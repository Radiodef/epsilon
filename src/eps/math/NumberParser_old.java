/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.app.*;
import eps.util.*;
import static eps.math.Math2.*;

import java.math.*;
import java.util.*;
import java.util.stream.*;
import static java.lang.Character.*;

// TODO:
//  delete this once we feel good about eps.eval.NumberParser
@Deprecated
interface NumberParser_old {
    Number parse(MathEnv env, String n);
    
    NumberParser_old BINARY_PARSER = new NumberParser_old() {
        @Override
        public Number parse(MathEnv env, String n) {
            int length = n.length();
            if (length > 2) {
                if (n.codePointAt(0) == '0' && n.codePointAt(1) == 'b') {
                    BigInteger result = BigInteger.ZERO;
                    
                    CodePoint separator = Settings.getDigitSeparator();
                    
                    int bit = 0;
                    int code;
                    for (int i = length; i > 2; i -= charCount(code)) {
                        code = n.codePointBefore(i);
                        
                        switch (code) {
                            case '0':
                                ++bit;
                                break;
                            case '1':
                                result = result.setBit(bit);
                                ++bit;
                                break;
                            default :
                                if ((separator == null) || (code != separator.intValue()))
                                    return null;
                                break;
                        }
                    }
                    
                    if (bit == 0) {
                        // No digits: something like 0b_
                        return null;
                    }
                    
                    return env.valueOf(result);
                }
            }
            return null;
        }
    };
    
    NumberParser_old HEX_PARSER = new NumberParser_old() {
        @Override
        public Number parse(MathEnv env, String n) {
            int length = n.length();
            if (length > 2) {
                if (n.codePointAt(0) == '0' && n.codePointAt(1) == 'x') {
                    BigInteger result = BigInteger.ZERO;
                    
                    CodePoint separator = Settings.getDigitSeparator();
                    
                    BigInteger coef = BigInteger.ONE;
                    int code;
                    for (int i = length; i > 2; i -= charCount(code)) {
                        code = n.codePointBefore(i);
                        
                        int index;
                        if (inRangeClosed(code, '0', '9')) {
                            index = code - '0';
                        } else if (inRangeClosed(code, 'A', 'F')) {
                            index = 10 + (code - 'A');
                        } else if (inRangeClosed(code, 'a', 'f')) {
                            index = 10 + (code - 'a');
                        } else if ((separator != null) && (code == separator.intValue())) {
                            continue;
                        } else {
                            return null;
                        }
                        
                        result = result.add(digits[index].multiply(coef));
                        coef   = coef.shiftLeft(4);
                    }
                    
                    if (coef.equals(BigInteger.ONE)) {
                        // No digits: something like 0x_
                        return null;
                    }
                    
                    return env.valueOf(result);
                }
            }
            return null;
        }
        private final BigInteger[] digits =
            IntStream.range(0, 16)
                     .mapToObj(BigInteger::valueOf)
                     .toArray(BigInteger[]::new);
        @Deprecated
        private int toDigit(char c) {
            switch (c) {
                case '0': return 0x0;
                case '1': return 0x1;
                case '2': return 0x2;
                case '3': return 0x3;
                case '4': return 0x4;
                case '5': return 0x5;
                case '6': return 0x6;
                case '7': return 0x7;
                case '8': return 0x8;
                case '9': return 0x9;
                case 'a':
                case 'A': return 0xA;
                case 'b':
                case 'B': return 0xB;
                case 'c':
                case 'C': return 0xC;
                case 'd':
                case 'D': return 0xD;
                case 'e':
                case 'E': return 0xE;
                case 'f':
                case 'F': return 0xF;
                default : return -1;
            }
        }
    };
    
    NumberParser_old DECIMAL_PARSER = new NumberParser_old() {
        @Override
        public Number parse(MathEnv env, String input) {
            int length = input.length();
            if (length > 0) {
                int       decimalMark = Settings.getDecimalMark();
                CodePoint separator   = Settings.getDigitSeparator();
                
                int start = 0;
                int end   = length;
                
                if (separator != null) {
                    for (int c; (c = input.codePointAt(start)) == separator.intValue();) {
                        start += charCount(c);
                        if (start == end) {
                            // Only separators.
                            return null;
                        }
                    }
                }
                
                if (input.codePointAt(start) == '0') {
                    if (isZero(input, decimalMark, separator)) {
                        return env.zero();
                    }
                    for (int i = start + 1; i < length; ++i) {
                        int code = input.codePointAt(i);
                        if (code != '0' && (separator == null || code != separator.intValue())) {
                            start = i;
                            break;
                        }
                    }
                }
                // Note: Removing all trailing 0s is wrong. (e.g. 100 turns to 1.)
                //       Would need to check only to the right of the decimal
                //       mark, but frankly trailing zeroes don't really cause
                //       any problem.
//                for (int c; end > start; end -= charCount(c)) {
//                    if ((c = input.codePointBefore(end)) != '0' && c != separator)
//                        break;
//                }
                
                BigInteger result   = BigInteger.ZERO;
                BigInteger scale    = BigInteger.ONE;
                boolean    dotFound = false;
                
                for (int i = start, c; i < end; i += charCount(c)) {
                    c = input.codePointAt(i);
                    
                    if (c == decimalMark) {
                        if (dotFound) {
                            return null; // error
                        } else {
                            dotFound = true;
                        }
                        
                    } else if (inRangeClosed(c, '0', '9')) {
                        if (dotFound) {
                            scale = scale.multiply(BigInteger.TEN);
                        }
                        
                        result = result.multiply(BigInteger.TEN);
                        
                        if (c != '0') {
                            result = result.add(digits[c - '0']);
                        }
                        
                    } else if ((separator == null) || (c != separator.intValue())) {
                        return null;
                    }
                }
                
                if (scale.equals(BigInteger.ONE)) {
                    return env.valueOf(result);
                } else {
                    return Fraction.create(env.valueOf(result), env.valueOf(scale), env);
                }
            }
            return null;
        }
        private boolean isZero(String input, int decimalMark, CodePoint separator) {
            int     length   = input.length();
            boolean dotFound = false;
            
            for (int i = 0, c; i < length; i += charCount(c)) {
                c = input.codePointAt(i);
                
                if (c != '0' && (separator == null || c != separator.intValue())) {
                    if (c == decimalMark) {
                        if (dotFound) {
                            return false; // error
                        } else {
                            dotFound = true;
                        }
                    } else {
                        return false;
                    }
                }
            }
            
            return true;
        }
        private final BigInteger[] digits =
            IntStream.range(0, 10)
                     .mapToObj(BigInteger::valueOf)
                     .toArray(BigInteger[]::new);
    };
    
    List<NumberParser_old> DEFAULTS = Collections.unmodifiableList(Misc.collectConstants(NumberParser_old.class));
    
    static Number parseDefault(MathEnv env, String input) {
        for (NumberParser_old parser : DEFAULTS) {
            Number n = parser.parse(env, input);
            if (n != null)
                return n;
        }
        return null;
    }
}