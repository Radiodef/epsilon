/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.util.*;
import static eps.util.Misc.*;

import java.math.*;
import java.util.function.*;
import java.io.PrintStream;
import java.io.Serializable;

public final class Math2 {
    private Math2() {}
    
    public static final int INT_MIN = java.lang.Integer.MIN_VALUE;
    public static final int INT_MAX = java.lang.Integer.MAX_VALUE;
    
    public static final long LONG_MIN = java.lang.Long.MIN_VALUE;
    public static final long LONG_MAX = java.lang.Long.MAX_VALUE;
    
    public static final double TAU = 6.283185307179586;
    
    public static final double SQRT_2 = 1.4142135623730951;
    
    public static int signum(int n) {
        return (n < 0) ? -1 : (n > 0) ? +1 : 0;
    }
    
    public static int signum(double n) {
        return (n < 0) ? -1 : (n > 0) ? +1 : 0;
    }
    
    public static int lshift32(int n, int shift) {
        return ((shift & 0b1_1111) == 0) ? 0 : (n << shift);
    }
    
    public static int rshift32(int n, int shift) {
        return ((shift & 0b1_1111) == 0) ? 0 : (n >>> shift);
    }
    
    private static final int MAX_POW_2 = 30;
    
    public static int pow2(int n) {
        if (n < 0 || MAX_POW_2 < n) {
            throw Errors.newArithmetic(n);
        }
        return 1 << n;
    }
    
    public static int next2(int n) {
        --n;
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        return n + 1;
    }
    
    public static long next2(long n) {
        --n;
        n |= n >> 1;
        n |= n >> 2;
        n |= n >> 4;
        n |= n >> 8;
        n |= n >> 16;
        n |= n >> 32;
        return n + 1L;
    }
    
    public static int floorLog10(int n) {
        if (n >= 1_000_000_000) return 9;
        if (n >=   100_000_000) return 8;
        if (n >=    10_000_000) return 7;
        if (n >=     1_000_000) return 6;
        if (n >=       100_000) return 5;
        if (n >=        10_000) return 4;
        if (n >=         1_000) return 3;
        if (n >=           100) return 2;
        if (n >=            10) return 1;
        if (n <= 0) throw Errors.newArithmetic(n);
        return 0;
    }
    
    public static int floorLog10(long n) {
        if (n >= 1_000_000_000_000_000_000L) return 18;
        if (n >=   100_000_000_000_000_000L) return 17;
        if (n >=    10_000_000_000_000_000L) return 16;
        if (n >=     1_000_000_000_000_000L) return 15;
        if (n >=       100_000_000_000_000L) return 14;
        if (n >=        10_000_000_000_000L) return 13;
        if (n >=         1_000_000_000_000L) return 12;
        if (n >=           100_000_000_000L) return 11;
        if (n >=            10_000_000_000L) return 10;
        if (n >=             1_000_000_000L) return  9;
        if (n >=               100_000_000L) return  8;
        if (n >=                10_000_000L) return  7;
        if (n >=                 1_000_000L) return  6;
        if (n >=                   100_000L) return  5;
        if (n >=                    10_000L) return  4;
        if (n >=                     1_000L) return  3;
        if (n >=                       100L) return  2;
        if (n >=                        10L) return  1;
        if (n <= 0) throw Errors.newArithmetic(n);
        return 0;
    }
    
    // Generates the above two floorLog10 methods to System.out.
    private static void generateFloorLog10s() {
        PrintStream out = System.out;
        
        for (String type : new String[] {"int", "long"}) {
            out.printf("    public static int floorLog10(%s n) {%n", type);
            out.printf("        if (n <= 0) throw Errors.newArithmetic(n);%n");
            
            long max      = type.equals("int") ? java.lang.Integer.MAX_VALUE : Long.MAX_VALUE;
            int  max10    = (int) Math.log10(max);
            int  maxWidth = 1 + max10 + (max10 / 3);
            
            for (int i = max10; i > 0; --i) {
                int width = 1 + i + (i / 3);
                
                out.printf("        if (n >= ");
                for (int j = 0; j < (maxWidth - width); ++j) {
                    out.printf(" ");
                }
                out.printf("1");
                
                for (int j = i; j > 0; --j) {
                    if (i >= 3 && (j % 3) == 0) {
                        out.printf("_");
                    }
                    out.printf("0");
                }
                
                if (type.equals("long")) {
                    out.printf("L");
                }
                out.printf(") return %s%d;%n", (max10 >= 10 && i < 10) ? " " : "", i);
            }
            
            out.printf("        return 0;%n");
            out.printf("    }%n");
        }
    }
    
    public static int ceilDiv(int dividend, int divisor) {
        long lDivisor  = divisor;
        long roundOffs = (divisor < 0 ? -lDivisor : lDivisor) - 1L;
        long lDividend = (long) dividend + (dividend < 0 ? -roundOffs : roundOffs);
        return (int) (lDividend / lDivisor);
    }
    
    public static boolean isReal(double d) {
        // NaN
        if (d != d)
            return false;
        // Infinity
        if (d == Double.POSITIVE_INFINITY)
            return false;
        if (d == Double.NEGATIVE_INFINITY)
            return false;
        return true;
    }
    
    public static int bitHash(double d) {
        return Long.hashCode(Double.doubleToLongBits(d));
    }
    
    public static boolean bitEquals(double a, double b) {
        return Double.doubleToLongBits(a) == Double.doubleToLongBits(b);
    }
    
    public static boolean isInteger(double d) {
        return isReal(d) && (d == Math.floor(d));
    }
    
    public static boolean inRange(int n, int min, int max) {
        return min <= n && n < max;
    }
    
    public static boolean inRangeClosed(int n, int min, int max) {
        return min <= n && n <= max;
    }
    
    public static boolean inRange(double n, double min, double max) {
        return min <= n && n < max;
    }
    
    public static boolean inRangeClosed(double n, double min, double max) {
        return min <= n && n <= max;
    }
    
    public static long divideExact(long dividend, long divisor) {
        long quotient = dividend / divisor;
        
        if ((quotient * divisor) != dividend) {
            throw new ArithmeticException(dividend + " is not divisible by " + divisor);
        }
        
        return quotient;
    }
    
    private static final double DOUBLE_TO_LONG_MAX =  9223372036854774784.0;
    private static final double DOUBLE_TO_LONG_MIN = -9223372036854775808.0;
    
    public static long toLongExact(double d) {
        if (Math2.isInteger(d)) {
            if (d < 0.0) {
                if (DOUBLE_TO_LONG_MIN <= d)
                    return (long) d;
            } else if (d > 0.0) {
                if (d <= DOUBLE_TO_LONG_MAX)
                    return (long) d;
            } else {
                return 0L;
            }
        }
        throw new ArithmeticException(f("(double) %s = (long) %s", d, (long) d));
    }
    
    private static final long LONG_TO_DOUBLE_MAX =  9007199254740992L;
    private static final long LONG_TO_DOUBLE_MIN = -9007199254740992L;
    
    // Note/TODO: We could add more cases, for example (2^53)+2 actually has
    //            an exact double representation.
    
    public static double toDoubleExact(long l) {
        if (l < 0) {
            if (LONG_TO_DOUBLE_MIN <= l)
                return (double) l;
        } else if (l > 0) {
            if (l <= LONG_TO_DOUBLE_MAX)
                return (double) l;
        } else {
            return 0.0;
        }
        throw new ArithmeticException(f("(long) %s = (double) %s", l, (double) l));
    }
    
    private static long squareExact_throws(long n) {
        // TODO: replace this to not use an exception
        try {
            return Math.multiplyExact(n, n);
        } catch (ArithmeticException x) {
            return -1;
        }
    }
    
    public static long squareExact(long n) {
        long sq = n * n;
        if (sq < 0) {
            return -1;
        }
        long abs = Math.abs(n);
        if ((abs >>> 31) != 0 && (sq / n) != n) {
            return -1;
        }
        return sq;
    }
    
    public static int multiplyUnsigned(int a, int b) {
        if ((a | b) < 0) {
            throw new IllegalArgumentException(a + " * " + b);
        }
        long result = (long) a * (long) b;
        if (result > java.lang.Integer.MAX_VALUE) {
            return -1;
        }
        return (int) result;
    }
    
    public static long multiplyUnsigned(long a, long b) {
        if ((a | b) < 0) {
            throw new IllegalArgumentException(a + " * " + b);
        }
        try {
            return Math.multiplyExact(a, b);
        } catch (ArithmeticException x) {
            return -1;
        }
    }
    
    /**
     * Returns {@code min(a, b)}, unless either a or b is negative. If either
     * a or b is negative, then the result is whichever of a or b is positive.
     * If both a and b are negative, then the result is unspecified.
     * <p>
     * This method is primarily useful for working with Strings, for example
     * {@code pmin("X,Y".indexOf(","), "X,Y".length())} returns the index of
     * the end of the first element in the list {@code "X,Y"} or the end of
     * the String if there's only one element.
     * 
     * @param   a   the first number.
     * @param   b   the second number.
     * @return  {@code min(a, b)}, but preferring a positive result.
     */
    public static int pmin(int a, int b) {
        return (a < b) ? ((a < 0) ? b : a) : ((b < 0) ? a : b);
    }
    
    public static int trailingZeroCount(int n) {
        if (n == java.lang.Integer.MIN_VALUE) // -2_147_483_648
            return 0;
        n = Math.abs(n);
        
        int count = 0;
        
        for (;;) {
            if (n == 0)
                break;
            int div = (n / 10);
            int mod = n - (div * 10);
            if (mod != 0)
                break;
            ++count;
            n = div;
        }
        
        return count;
    }
    
    public static int trailingZeroCount(long n) {
        if (n == java.lang.Long.MIN_VALUE) // -9_223_372_036_854_775_808
            return 0;
        n = Math.abs(n);
        
        int count = 0;
        
        for (;;) {
            if (n == 0L)
                break;
            long div = (n / 10L);
            long mod = n - (div * 10L);
            if (mod != 0L)
                break;
            ++count;
            n = div;
        }
        
        return count;
    }
    
    private static final LongBinaryOperator ADDING_LONGS      = (LongBinaryOperator & Serializable) (a, b) -> a + b;
    private static final LongBinaryOperator SUBTRACTING_LONGS = (LongBinaryOperator & Serializable) (a, b) -> a - b;
    private static final LongBinaryOperator MULTIPLYING_LONGS = (LongBinaryOperator & Serializable) (a, b) -> a * b;
    private static final LongBinaryOperator DIVIDING_LONGS    = (LongBinaryOperator & Serializable) (a, b) -> a / b;
    private static final LongBinaryOperator MODDING_LONGS     = (LongBinaryOperator & Serializable) (a, b) -> a % b;
    
    private static final LongBinaryOperator ADDING_EXACT_LONGS      = (LongBinaryOperator & Serializable) Math::addExact;
    private static final LongBinaryOperator SUBTRACTING_EXACT_LONGS = (LongBinaryOperator & Serializable) Math::subtractExact;
    private static final LongBinaryOperator MULTIPLYING_EXACT_LONGS = (LongBinaryOperator & Serializable) Math::multiplyExact;
    private static final LongBinaryOperator DIVIDING_EXACT_LONGS    = (LongBinaryOperator & Serializable) Math2::divideExact;
    // Note: long mod is always exact; see https://docs.oracle.com/javase/specs/jls/se9/html/jls-15.html#jls-15.17.3
    private static final LongBinaryOperator MODDING_EXACT_LONGS     = MODDING_LONGS;
    
    private static final DoubleBinaryOperator ADDING_DOUBLES      = (DoubleBinaryOperator & Serializable) (a, b) -> a + b;
    private static final DoubleBinaryOperator SUBTRACTING_DOUBLES = (DoubleBinaryOperator & Serializable) (a, b) -> a - b;
    private static final DoubleBinaryOperator MULTIPLYING_DOUBLES = (DoubleBinaryOperator & Serializable) (a, b) -> a * b;
    private static final DoubleBinaryOperator DIVIDING_DOUBLES    = (DoubleBinaryOperator & Serializable) (a, b) -> a / b;
    private static final DoubleBinaryOperator MODDING_DOUBLES     = (DoubleBinaryOperator & Serializable) (a, b) -> a % b;
    
    private static final BinaryOperator<BigInteger> ADDING_BIG_INTEGERS      = (BinaryOperator<BigInteger> & Serializable) BigInteger::add;
    private static final BinaryOperator<BigInteger> SUBTRACTING_BIG_INTEGERS = (BinaryOperator<BigInteger> & Serializable) BigInteger::subtract;
    private static final BinaryOperator<BigInteger> MULTIPLYING_BIG_INTEGERS = (BinaryOperator<BigInteger> & Serializable) BigInteger::multiply;
    private static final BinaryOperator<BigInteger> DIVIDING_BIG_INTEGERS    = (BinaryOperator<BigInteger> & Serializable) BigInteger::divide;
    private static final BinaryOperator<BigInteger> MODDING_BIG_INTEGERS     = (BinaryOperator<BigInteger> & Serializable) BigInteger::mod;
    
    public static LongBinaryOperator addingLongs()      { return ADDING_LONGS;      }
    public static LongBinaryOperator subtractingLongs() { return SUBTRACTING_LONGS; }
    public static LongBinaryOperator multiplyingLongs() { return MULTIPLYING_LONGS; }
    public static LongBinaryOperator dividingLongs()    { return DIVIDING_LONGS;    }
    public static LongBinaryOperator moddingLongs()     { return MODDING_LONGS;     }
    
    public static LongBinaryOperator addingExactLongs()      { return ADDING_EXACT_LONGS;      }
    public static LongBinaryOperator subtractingExactLongs() { return SUBTRACTING_EXACT_LONGS; }
    public static LongBinaryOperator multiplyingExactLongs() { return MULTIPLYING_EXACT_LONGS; }
    public static LongBinaryOperator dividingExactLongs()    { return DIVIDING_EXACT_LONGS;    }
    public static LongBinaryOperator moddingExactLongs()     { return MODDING_EXACT_LONGS;     }
    
    public static DoubleBinaryOperator addingDoubles()      { return ADDING_DOUBLES;      }
    public static DoubleBinaryOperator subtractingDoubles() { return SUBTRACTING_DOUBLES; }
    public static DoubleBinaryOperator multiplyingDoubles() { return MULTIPLYING_DOUBLES; }
    public static DoubleBinaryOperator dividingDoubles()    { return DIVIDING_DOUBLES;    }
    public static DoubleBinaryOperator moddingDoubles()     { return MODDING_DOUBLES;     }
    
    public static BinaryOperator<BigInteger> addingBigIntegers()      { return ADDING_BIG_INTEGERS;      }
    public static BinaryOperator<BigInteger> subtractingBigIntegers() { return SUBTRACTING_BIG_INTEGERS; }
    public static BinaryOperator<BigInteger> multiplyingBigIntegers() { return MULTIPLYING_BIG_INTEGERS; }
    public static BinaryOperator<BigInteger> dividingBigIntegers()    { return DIVIDING_BIG_INTEGERS;    }
    public static BinaryOperator<BigInteger> moddingBigIntegers()     { return MODDING_BIG_INTEGERS;     }
    
    private static final DoubleUnaryOperator FLOORING_DOUBLES = (DoubleUnaryOperator & Serializable) Math::floor;
    private static final DoubleUnaryOperator CEILING_DOUBLES  = (DoubleUnaryOperator & Serializable) Math::ceil;
    
    public static DoubleUnaryOperator flooringDoubles() { return FLOORING_DOUBLES; }
    public static DoubleUnaryOperator ceilingDoubles()  { return CEILING_DOUBLES;  }
}