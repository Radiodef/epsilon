/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.job.*;
import eps.app.*;
import static eps.util.Misc.*;

import java.math.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import java.io.IOException;

public abstract class Primes implements Iterable<Integer> {
    protected final MathEnv env;
    
    private final List<Integer> primes;
    
    private volatile int cacheSize;
    
    private volatile boolean cacheIsFull = false;
    
    public Primes(MathEnv env) {
        this.env = Objects.requireNonNull(env, "env");
        
        App.whenAvailable(app -> app.getSettings().addChangeListener(Setting.PRIME_CACHE_SIZE, new CacheSizeListener(this)));
        
        cacheSize = Settings.getOrDefault(Setting.PRIME_CACHE_SIZE);
        
        synchronized (this) {
            primes = new ArrayList<>();
            init(primes);
        }
    }
    
    private static final class CacheSizeListener extends Settings.WeakListener<java.lang.Integer, Primes> {
        private CacheSizeListener(Primes primes) {
            super(primes);
        }
        @Override
        protected void settingChanged(Primes                     primes,
                                      Settings                   settings,
                                      Setting<java.lang.Integer> setting,
                                      java.lang.Integer          oldSize,
                                      java.lang.Integer          cacheSize) {
            primes.cacheSize = cacheSize;
        }
    }
    
    protected void init(List<Integer> primes) {
    }
    
    protected int getNextIndex(List<Integer> primes, Integer n, MathEnv env) {
        assert Thread.holdsLock(this);
        int index = Collections.binarySearch(primes, n, env.comparingIntegers());
        
        int next;
        if (index < 0) {
            int insertion = -(index + 1);
            next = insertion;
        } else {
            next = index + 1;
        }
        
        return next;
    }
    
    public Integer getNextAfter(long n) {
        return getNextAfter(env.valueOf(n));
    }
    
    public Integer getNextAfter(Integer n) {
        Objects.requireNonNull(n, "n");
        
        MathEnv env = this.env;
        
        if (n.relateTo(env.one(), env) <= 0) {
            return env.valueOf(2);
        }
        
        int cacheSize = this.cacheSize;
        List<Integer> primes = this.primes;
        
        synchronized (this) {
            int next = getNextIndex(primes, n, env);
            
            for (;;) {
                Worker.executing();
                int size = primes.size();
                
                if (next < size) {
                    Integer last = primes.get(next);
                    if (n.relateTo(last, env) < 0)
                        return last;
                }
                
                if (size >= cacheSize) {
                    boolean cacheIsFull = this.cacheIsFull;
                    if (!cacheIsFull) {
                        this.cacheIsFull = true;
                        Log.low(Primes.class, "getNextAfter", "cache is full at ", size, "; computing next after ", n);
                    }
                    
                    return computeNextAfter(n);
                }
                
                addNextAfterLast(primes);
                next = getNextIndex(primes, n, env);
            }
        }
    }
    
//    @Deprecated
//    public Integer getNextAfter_old(Integer n) {
//        Objects.requireNonNull(n, "n");
//        
//        MathEnv env = this.env;
//        
//        if (n.relateTo(env.one(), env) <= 0) {
//            return env.valueOf(2);
//        }
//        
//        int cacheSize = this.cacheSize;
//        List<Integer> primes = this.primes;
//        
//        synchronized (this) {
//            int index = Collections.binarySearch(primes, n, env.comparingIntegers());
//            
//            int next;
//            if (index < 0) {
//                int insertion = -(index + 1);
//                next = insertion;
//            } else {
//                next = index + 1;
//            }
//            
//            for (;;) {
//                Worker.executing();
//                int size = primes.size();
//                
//                if (next < size) {
//                    // TODO: this should probably be another binary search?
//                    do {
//                        Integer last = primes.get(next);
//                        if (n.relateTo(last, env) < 0)
//                            return last;
//                        ++next;
//                    } while (next < size);
//                }
//                
//                if (size >= cacheSize) {
//                    boolean cacheIsFull = this.cacheIsFull;
//                    if (!cacheIsFull) {
//                        this.cacheIsFull = true;
//                        Log.low(Primes.class, "getNextAfter", "cache is full at ", size, "; computing next after ", n);
//                    }
//                    
//                    return computeNextAfter(n);
//                }
//                
//                addNextAfterLast(primes);
//            }
//        }
//    }
    
    protected void addNextAfterLast(List<Integer> primes) {
        Integer last = primes.get(primes.size() - 1);
        Integer next = computeNextAfter(last);
        primes.add(next);
    }
    
    protected Integer computeNextAfter(Integer n) {
        BigInteger p = computeNextAfter(n.toBigIntegerExact(env));
        try {
            return env.valueOf(p).toIntegerExact(env);
        } catch (ArithmeticException x) {
            throw UnsupportedException.overflow(p).initCause(x);
        }
    }
    
    public boolean isPrime(Integer n) {
        return isPrime(n.toBigIntegerExact(env));
    }
    
    protected BigInteger computeNextAfter(BigInteger n) {
        do {
            n = n.nextProbablePrime();
        } while (!isPrime(n));
        return n;
    }
    
    private static final BigInteger ONE   = BigInteger.ONE;
    private static final BigInteger TWO   = ONE.add(ONE);
    private static final BigInteger THREE = TWO.add(ONE);
    
    protected boolean isPrime(BigInteger n) {
        if (n.isProbablePrime(3)) {
            if (n.compareTo(BigInteger.ONE) <= 0)
                return false;
            if (n.equals(TWO))
                return true;
            if (n.mod(TWO).signum() == 0)
                return false;
            
            for (BigInteger i = THREE; i.multiply(i).compareTo(n) <= 0; i = i.add(TWO)) {
                Worker.executing();
                
                if (n.mod(i).signum() == 0)
                    return false;
            }
            return true;
        }
        return false;
    }
    
    @Override
    public Iterator<Integer> iterator() {
        return new PrimeIterator(this);
    }
    
    protected static class PrimeIterator implements Iterator<Integer> {
        protected final Primes primes;
        
        protected Integer next;
        
        protected PrimeIterator(Primes primes) {
            this.primes = Objects.requireNonNull(primes, "primes");
            this.next   = primes.env.one();
        }
        
        @Override
        public boolean hasNext() {
            return true;
        }
        
        @Override
        public Integer next() {
            Integer prev = this.next;
            Integer next = primes.getNextAfter(prev);
            this.next    = next;
            return next;
        }
    }
    
    @Override
    public Spliterator<Integer> spliterator() {
        return new PrimeSpliterator(this);
    }
    
    public Stream<Integer> stream() {
        return StreamSupport.stream(spliterator(), false);
    }
    
    protected static class PrimeSpliterator implements Spliterator<Integer> {
        protected final Primes primes;
        
        protected Integer next;
        
        protected PrimeSpliterator(Primes primes) {
            this.primes = Objects.requireNonNull(primes, "primes");
            this.next   = primes.env.one();
        }
        
        @Override
        public boolean tryAdvance(Consumer<? super Integer> action) {
            Integer next = primes.getNextAfter(this.next);
            action.accept(next);
            this.next    = next;
            return true;
        }
        
        @Override
        public long estimateSize() {
            return Long.MAX_VALUE;
        }
        
        @Override
        public int characteristics() {
            return CONCURRENT | DISTINCT | IMMUTABLE | NONNULL | ORDERED | SORTED;
        }
        
        @Override
        public Comparator<Integer> getComparator() {
            return primes.env.comparingIntegers();
        }
        
        @Override
        public Spliterator<Integer> trySplit() {
            return null;
        }
    }
    
    // see https://oeis.org/A000040
    private static final int[] PRIMES_100 = {
          2,   3,   5,   7,  11,  13,  17,  19,  23,  29,  31,  37,  41,
         43,  47,  53,  59,  61,  67,  71,  73,  79,  83,  89,  97, 101,
        103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167,
        173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239,
        241, 251, 257, 263, 269, 271
    };
    
    public static final List<java.lang.Integer> FIRST_100_PRIMES;
    static {
        List<java.lang.Integer> list = new ArrayList<>(100);
        for (int p : PRIMES_100)
            list.add(p);
        FIRST_100_PRIMES = Collections.unmodifiableList(list);
    }
    
    protected final boolean assertions() {
        if (!EpsilonMain.isDeveloper()) {
            return true;
        }
        
        try {
            int count = 0;
            
            try {
                java.net.URL url = new java.net.URL("http://www.primos.mat.br/primeiros_10000_primos.txt");
                
                try (Scanner in = new Scanner(url.openStream())) {
                    Log.note(Primes.class, "assertions", "running assertions with ", url);
                    
                    Integer next = env.one();
                    Integer read;
                    
                    while (in.hasNextLong()) {
                        ++count;
                        next = getNextAfter(next);
                        read = env.valueOf(in.nextLong());
                        
                        if (!next.equals(read)) {
                            throw new AssertionError(f("next = %s, read = %s", next, read));
                        }
                    }
                    
                    assert count == 10_000 : count;
                }
                
            } catch (IOException | NoSuchElementException x) {
                Log.caught(Primes.class, "assertions", x, false);
                Log.note(Primes.class, "assertions", "running assertions statically");
                
                count = 0;
                
                Integer next = env.one();
                Integer elem;
                
                for (int e : PRIMES_100) {
                    ++count;
                    next = getNextAfter(next);
                    elem = env.valueOf(e);
                    
                    if (!next.equals(elem)) {
                        throw new AssertionError(f("next = %s, elem = %s", next, elem));
                    }
                }
            }
            
            Log.note(Primes.class, "assertions", "assertions passed for first ", count, " primes");
            
        } catch (RuntimeException x) {
            Log.caught(Primes.class, "assertions", x, false);
        }
        
        return true;
    }
}