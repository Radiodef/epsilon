/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.app.*;

import java.math.*;
import java.util.*;
import java.util.function.*;

public interface Integer extends Number.Generic<Integer> {
    @Override
    default Number.Kind getNumberKind() {
        return Number.Kind.INTEGER;
    }
    
    @Override
    default boolean isInteger(MathEnv env) {
        return true;
    }
    
    @Override
    default boolean isApproximate() {
        return false;
    }
    
    BigInteger toBigIntegerExact(MathEnv env);
    
    int toPrimitiveIntExact(MathEnv env);
    
    @Override
    default Integer toInteger(MathEnv env) {
        return this;
    }
    
    @Override
    default Integer toIntegerInexact(MathEnv env) {
        return this;
    }
    
    boolean isDivisibleBy(Integer divisor, MathEnv env);
    
    Integer divideAsInt(Integer divisor, MathEnv env);
    
    default Number divideAsInt(Integer that, int roundingSign, MathEnv env) {
        if (roundingSign == 0) {
            return this.divideAsInt(that, env);
        }
        
        if (that.isZero(env)) {
            throw UndefinedException.divisionByZero(this, that);
        }
        
        BigInteger dividend = this.toBigIntegerExact(env).abs();
        BigInteger divisor  = that.toBigIntegerExact(env).abs();
        int        sign     = this.signum() * that.signum();
        
        BigInteger[] results   = dividend.divideAndRemainder(divisor);
        BigInteger   quotient  = results[0];
        BigInteger   remainder = results[1];
        
        BigInteger result = quotient;
        
        if (remainder.signum() != 0) {
            if (( roundingSign < 0 ) ? ( sign < 0 ) : ( sign > 0 )) {
                result = result.add(BigInteger.ONE);
            }
        }
        
        return env.valueOf(sign < 0 ? result.negate() : result);
    }
    
    default Number floorDiv(Integer divisor, MathEnv env) {
        return this.divideAsInt(divisor, -1, env);
    }
    default Number ceilDiv(Integer divisor, MathEnv env) {
        return this.divideAsInt(divisor, +1, env);
    }
    
//    @Override
//    Integer negate(MathEnv env);
//    @Override
//    Integer abs(MathEnv env);
    
    @Override
    default Integer floor(MathEnv env) {
        return this;
    }
    
    @Override
    default Integer ceil(MathEnv env) {
        return this;
    }
    
    @Override
    default Number promoteTo(Number toType, MathEnv env) {
        switch (toType.getNumberKind()) {
            case FLOAT:
                return toFloatInexact(env);
            case BOOLEAN:
            case INTEGER:
                return this;
            case FRACTION:
                return Fraction.create(this, env.one(), env);
            default:
                throw UnsupportedException.operandTypes(this, toType, "numeric promotion");
        }
    }
    
    default Tuple factor(MathEnv env) {
        Objects.requireNonNull(env, "env");
        
        int sign = signum();
        if (sign == 0) {
            return Tuple.empty();
        }
        
        Tuple.Builder b = Tuple.builder();
        
        Integer n = this;
        if (sign < 0) {
            b.append(env.negativeOne());
            
            Number m = negate(env);
            if (m.getNumberKind().isInteger()) {
                n = (Integer) m;
            } else {
                throw UnsupportedException.overflow("negation of " + this);
            }
//            n = negate(env);
        }
        
        if (n.isOne(env)) {
            if (sign > 0) {
                b.append(env.one());
            }
            return b.build();
        }
        
        for (Integer prime : env.primes()) {
            while (n.isDivisibleBy(prime, env)) {
                b.append(prime);
                n = n.divideAsInt(prime, env);
            }
            if (n.isOne(env)) {
                break;
            }
        }
        
        Tuple factors = b.build();
        
        assert new BiPredicate<Integer, Tuple>() {
            @Override
            public boolean test(Integer n, Tuple t) {
                try {
                    Number product = env.one();
                    
                    for (Operand op : t) {
                        Number lhs = product;
                        Number rhs = (Number) op;
                        product    = env.multiply(lhs, rhs);
                        
                        assert product.getNumberKind().isInteger()
                             : lhs + " * " + rhs + " = " + product;
                    }
                    
                    return env.isEqual(n, product);
                } catch (AssertionError x) {
                    Log.note(Integer.class, "factor$test", "n = ", n, ", t = ", t);
                    throw x;
                } catch (RuntimeException x) {
                    Log.caught(Integer.class, "factor$test", x, false);
                    return false;
                }
            }
        }.test(this, factors) : this + " = " + factors;
        
        return factors;
    }
}