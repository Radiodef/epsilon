/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.json.*;
import static eps.util.Misc.*;

import java.util.*;

public final class UndefinedException extends eps.eval.EvaluationException {
    static {
        JsonObjectAdapter.put(UndefinedException.class,
                              UndefinedException::toMap,
                              UndefinedException::new);
    }
    
    public UndefinedException(String description) {
        super(replnull(description, "undefined"));
    }
    
    public UndefinedException(Map<?, ?> map) {
        super(map);
    }
    
    public static double requireReal(double val) {
        if (Double.isNaN(val)) {
            throw ieeeNaN();
        }
        if (Double.isInfinite(val)) {
            throw ieeeInfinity();
        }
        return val;
    }
    
    public static UndefinedException fmt(String fmt, Object... args) {
        return new UndefinedException(String.format(fmt, args));
    }
    
    public static UndefinedException ieeeInfinity() {
        return new UndefinedException("IEEE infinity");
    }
    
    public static UndefinedException ieeeNaN() {
        return new UndefinedException("IEEE NaN");
    }
    
    public static UndefinedException divisionByZero() {
        return new UndefinedException("division by 0");
    }
    
    public static UndefinedException divisionByZero(Object dividend, Object divisor) {
        return new UndefinedException("division by 0: " + dividend + "/" + divisor);
    }
    
    public static UndefinedException negativeRoot() {
        return new UndefinedException("root of a negative number");
    }
    
    public static UndefinedException negativeRoot(Number n) {
        return new UndefinedException("root of a negative number: " + n);
    }
    
    public static UndefinedException negativeFactorial() {
        return new UndefinedException("factorial of a negative number");
    }
    
    public static UndefinedException fractionalFactorial() {
        return new UndefinedException("factorial of a non-integer");
    }
    
    public static UndefinedException discontinuity() {
        return new UndefinedException("function discontinuity");
    }
    
    public static UndefinedException negativeOrZeroLog(Number n) {
        return new UndefinedException("logarithm of zero or negative number: " + n);
    }
    
    public static UndefinedException negativeOrZeroLog(Number base, Number n) {
        return new UndefinedException("base " + base + " logarithm of zero or negative number: " + n);
    }
    
    public static UndefinedException baseOneLog(Number n) {
        return new UndefinedException("base 1 logarithm (of " + n + ")");
    }
}