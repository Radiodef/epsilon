/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.app.*;
import eps.eval.*;
import eps.util.*;
import eps.block.*;

import java.util.*;

public interface Number extends Operand {
    boolean isInteger(MathEnv env);
    boolean isDefined(MathEnv env);
    boolean isOne(MathEnv env);
    
    int signum();
    
    default boolean isZero(MathEnv env) {
        return this.signum() == 0;
    }
    
    Number promoteTo(Number toType, MathEnv env);
    
    Number abs(MathEnv env);
    Number negate(MathEnv env);
    
    Number floor(MathEnv env);
    Number ceil(MathEnv env);
    
    default Number reciprocal(MathEnv env) {
        return env.divide(env.one(), this);
    }
    
    boolean isApproximate();
    
    @Override
    default boolean isTruthy(Context context) {
        return this.isTruthy(context.getMathEnvironment());
    }
    
    default boolean isTruthy(MathEnv env) {
        return !this.isZero(env);
    }
    
    default Boolean toBoolean(MathEnv env) {
        return Boolean.valueOf(this.isTruthy(env));
    }
    
    Float toFloat(MathEnv env);
    Integer toInteger(MathEnv env);
    
    Float toFloatInexact(MathEnv env);
    Integer toIntegerInexact(MathEnv env);
    
    default Float toFloatExact(MathEnv env) {
        Float f = this.toFloat(env);
        if (f == null) {
            throw Errors.newArithmetic("could not convert " + this + " to an exact integer");
        }
        return f;
    }
    
    default Integer toIntegerExact(MathEnv env) {
        Integer i = this.toInteger(env);
        if (i == null) {
            throw Errors.newArithmetic("could not convert " + this + " to an exact float");
        }
        return i;
    }
    
    interface Generic<N extends Generic<N>> extends Number {
        Number add(N rhs, MathEnv env);
        Number subtract(N rhs, MathEnv env);
        Number multiply(N rhs, MathEnv env);
        Number divide(N rhs, MathEnv env);
        Number mod(N rhs, MathEnv env);
        
        int relateTo(N that, MathEnv env);
    }
    
    @Override
    default Operand.Kind getOperandKind() {
        return Operand.Kind.NUMBER;
    }
    
    @Override
    default boolean isNumber() {
        return true;
    }
    
    default boolean isScalar() {
        return this.getNumberKind().isScalar();
    }
    
    Number.Kind getNumberKind();
    
    @Override
    default StringBuilder toResultString(StringBuilder b, Context context, AnswerFormatter fmt, MutableBoolean approx) {
        if (fmt == null) {
            return this.toResultString(b, context, approx);
        } else {
            return fmt.toResultString(this, b, context, approx);
        }
    }
    
    StringBuilder toResultString(StringBuilder b, Context context, MutableBoolean approx);
    
    @Override
    default Block toResultBlock(StringBuilder temp, Context context, AnswerFormatter fmt, MutableBoolean approx) {
        temp.setLength(0);
        Block block = Block.of(this.toResultString(temp, context, fmt, approx));
        return new StyleBlock(block).set(Setting.NUMBER_STYLE);
    }
    
    @Override
    default Block toSourceBlock(StringBuilder temp, Context context, Node node) {
        Block block = Operand.super.toSourceBlock(temp, context, node);
        return new StyleBlock(block).set(Setting.NUMBER_STYLE);
    }
    
    enum Kind {
        BOOLEAN,
        INTEGER,
        FLOAT,
        FRACTION;
        
        private final String lowerName;
        
        Kind() {
            lowerName = name().toLowerCase(Locale.ROOT);
        }
        
        public boolean isBoolean()  { return this == BOOLEAN;  }
        public boolean isInteger()  { return this == INTEGER;  }
        public boolean isFloat()    { return this == FLOAT;    }
        public boolean isFraction() { return this == FRACTION; }
        
        public boolean isScalar() {
            switch (this) {
                case BOOLEAN:
                case INTEGER:
                case FLOAT:
                    return true;
                default:
                    return false;
            }
        }
        
        public String toLowerCase() {
            return lowerName;
        }
    }
}