/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.util.*;

public final class Boolean implements Number.Generic<Boolean> {
    private final boolean booleanValue;
    
    private Boolean(boolean booleanValue) {
        this.booleanValue = booleanValue;
    }
    
    public static final Boolean TRUE  = new Boolean(true);
    public static final Boolean FALSE = new Boolean(false);
    
    public static Boolean valueOf(boolean booleanValue) {
        return booleanValue ? TRUE : FALSE;
    }
    
    @Override
    public Number.Kind getNumberKind() {
        return Number.Kind.BOOLEAN;
    }
    
    @Override
    public boolean isApproximate() {
        return false;
    }
    
    public boolean booleanValue() {
        return this.booleanValue;
    }
    
    public int intValue() {
        return this.booleanValue ? 1 : 0;
    }
    
    @Override
    public int signum() {
        return this.intValue();
    }
    
    @Override
    public Integer floor(MathEnv env) {
        return this.toIntegerExact(env);
    }
    
    @Override
    public Integer ceil(MathEnv env) {
        return this.toIntegerExact(env);
    }
    
    @Override
    public int relateTo(Boolean that, MathEnv env) {
        if (this.booleanValue) {
            return that.booleanValue ? 0 : 1;
        } else {
            return that.booleanValue ? -1 : 0;
        }
    }
    
    @Override
    public Number promoteTo(Number toType, MathEnv env) {
        switch (toType.getNumberKind()) {
            case BOOLEAN:  return this;
            case FLOAT:    return toFloatInexact(env);
            case INTEGER:  return toIntegerExact(env);
            case FRACTION: return Fraction.create(this, env.one(), env);
            default: throw UnsupportedException.operandTypes(this, toType, "numeric promotion");
        }
    }
    
    @Override
    public boolean isOne(MathEnv env) {
        return this.booleanValue;
    }
    
    @Override
    public boolean isZero(MathEnv env) {
        return !this.booleanValue;
    }
    
    @Override
    public boolean isDefined(MathEnv env) {
        return true;
    }
    
    @Override
    public boolean isInteger(MathEnv env) {
        return true;
    }
    
    @Override
    public boolean isTruthy(MathEnv env) {
        return this.booleanValue;
    }
    
    @Override
    public Boolean toBoolean(MathEnv env) {
        return this;
    }
    
    @Override
    public Number reciprocal(MathEnv env) {
        return TRUE.divide(this, env);
    }
    
    @Override
    public Number add(Boolean that, MathEnv env) {
        return env.valueOf(this.intValue() + that.intValue());
    }
    
    @Override
    public Number subtract(Boolean that, MathEnv env) {
        return env.valueOf(this.intValue() - that.intValue());
    }
    
    @Override
    public Number multiply(Boolean that, MathEnv env) {
        return env.valueOf(this.intValue() * that.intValue());
    }
    
    @Override
    public Number divide(Boolean that, MathEnv env) {
        if (that.booleanValue) {
            return this.booleanValue ? env.one() : env.zero();
        }
        throw UndefinedException.divisionByZero(this, that);
    }
    
    @Override
    public Number mod(Boolean that, MathEnv env) {
        if (that.booleanValue) {
            return env.zero();
        }
        throw UndefinedException.divisionByZero(this, that);
    }
    
    @Override
    public Number abs(MathEnv env) {
        return this.toIntegerExact(env);
    }
    
    @Override
    public Number negate(MathEnv env) {
        return this.booleanValue ? env.negativeOne() : env.zero();
    }
    
    @Override public Integer toInteger(MathEnv env)        { return env.valueOf(this.intValue()); }
    @Override public Integer toIntegerExact(MathEnv env)   { return env.valueOf(this.intValue()); }
    @Override public Integer toIntegerInexact(MathEnv env) { return env.valueOf(this.intValue()); }
    @Override public Float toFloat(MathEnv env)        { return this.toIntegerExact(env).toFloatExact(env); }
    @Override public Float toFloatExact(MathEnv env)   { return this.toIntegerExact(env).toFloatExact(env); }
    @Override public Float toFloatInexact(MathEnv env) { return this.toIntegerExact(env).toFloatExact(env); }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context c, MutableBoolean a) {
        return b.append(this.toString());
    }
    
    @Override
    public String toString() {
        return java.lang.Boolean.toString(this.booleanValue);
    }
}