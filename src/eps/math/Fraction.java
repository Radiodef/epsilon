/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.util.*;
import eps.app.*;
import static eps.util.Misc.*;

import java.util.*;
import java.util.function.*;

public final class Fraction implements Number.Generic<Fraction> {
    private final Number numerator;
    private final Number denominator;
    
    private Fraction(Number numerator, Number denominator) {
        this.numerator   = numerator;
        this.denominator = denominator;
    }
    
    static Fraction createUnchecked(Number numerator, Number denominator) {
        Objects.requireNonNull(numerator,   "numerator");
        Objects.requireNonNull(denominator, "denominator");
        
        return new Fraction(numerator, denominator);
    }
    
    public static Fraction create(Number numerator, Number denominator, MathEnv env) {
        Objects.requireNonNull(numerator,   "numerator");
        Objects.requireNonNull(denominator, "denominator");
        
        boolean nIsFraction = numerator.getNumberKind().isFraction();
        boolean dIsFraction = denominator.getNumberKind().isFraction();
        
        if (nIsFraction || dIsFraction) {
            Number result = env.divide(numerator, denominator);
            
            if (result.getNumberKind().isFraction()) {
                return (Fraction) result;
            }
            return create(result, env.one(), env);
        }
        
        int nSignum = numerator.signum();
        int dSignum = denominator.signum();
        
        if (dSignum == 0) {
            throw UndefinedException.divisionByZero(numerator, denominator);
        }
        // Don't reduce floats and such.
        if (!numerator.isApproximate() && !denominator.isApproximate()) {
            if (nSignum == 0) {
                return env.zeroOverOne();
            }
            if (numerator.equals(denominator)) {
                return env.oneOverOne();
            }
        }
        
        if (numerator.isScalar() && denominator.isScalar()) {
            // Normalize scalar signs to always be on the numerator.
            if (dSignum < 0) {
                numerator   = numerator.negate(env);
                denominator = denominator.negate(env);
                nSignum     = numerator.signum();
                dSignum     = denominator.signum();
            }
        }
        
        Integer numeratorInt   = numerator.toInteger(env);
        Integer denominatorInt = denominator.toInteger(env);
        
        if (numeratorInt != null && denominatorInt != null) {
            Number numeratorAbs   = numeratorInt.abs(env);
            Number denominatorAbs = denominatorInt.abs(env);
            if (numeratorAbs.getNumberKind().isInteger() && denominatorAbs.getNumberKind().isInteger()) {
                numeratorInt   = (Integer) numeratorAbs;
                denominatorInt = (Integer) denominatorAbs;
                
                Integer gcd = env.gcd(numeratorInt, denominatorInt);
                
                if (!gcd.isOne(env)) {
                    numeratorInt   = numeratorInt.divideAsInt(gcd, env);
                    denominatorInt = denominatorInt.divideAsInt(gcd, env);
                }
                
                numerator   = nSignum < 0 ? numeratorInt.negate(env)   : numeratorInt;
                denominator = dSignum < 0 ? denominatorInt.negate(env) : denominatorInt;
            }
        }
        
//        if (denominator.getNumberKind().isFloat()) {
//            throw new RuntimeException(numerator + " / " + denominator);
//        }
        
        return new Fraction(numerator, denominator);
    }
    
    public static Fraction create(long numerator, long denominator, MathEnv env) {
        return create(env.valueOf(numerator), env.valueOf(denominator), env);
    }
    
    public Number collapse(MathEnv env) {
        Number result = collapseIfInteger(env);
        if (result == this) {
            result = collapseIfFloat(env);
        }
        return result;
    }
    
    public Number collapseIfInteger(MathEnv env) {
        Objects.requireNonNull(env, "env");
        Integer n = numerator.toInteger(env);
        Integer d = denominator.toInteger(env);
        if (n != null && d != null && n.isDivisibleBy(d, env)) {
            return n.divide(d, env);
        } else {
            return this;
        }
    }
    
    public Number collapseIfFloat(MathEnv env) {
        Objects.requireNonNull(env, "env");
        Number n = numerator;
        Number d = denominator;
        if (n.getNumberKind().isFloat() || d.getNumberKind().isFloat()) {
            return env.divide(n, d);
        } else {
            return this;
        }
    }
    
    @Override
    public Number.Kind getNumberKind() {
        return Number.Kind.FRACTION;
    }
    
    @Override
    public Number promoteTo(Number toType, MathEnv env) {
        switch (toType.getNumberKind()) {
            case BOOLEAN:
            case INTEGER:
            case FRACTION:
                return this;
            case FLOAT:
                return toFloatInexact(env);
            default:
                throw UnsupportedException.operandTypes(this, toType, "numeric promotion");
        }
    }
    
    @Override
    public Integer toInteger(MathEnv env) {
        Objects.requireNonNull(env, "env");
        Number n = numerator;
        Number d = denominator;
        // for a case like (1/3)/(3), we want to return 1 instead
        // of trying to do (int)(1/3)/(3) and returning null.
        if (n.getNumberKind().isFraction() || d.getNumberKind().isFraction()) {
            return env.divide(n, d).toInteger(env);
        }
        // note: we aren't really handling floats, but in some cases
        //       perhaps we could. (say if both n and d are not integers,
        //       but also not approximate, such as a case like 3.0/1.5)
        Integer dividend = n.toInteger(env);
        if (dividend == null) {
            return null;
        }
        Integer divisor = d.toInteger(env);
        if (divisor == null) {
            return null;
        }
        if (dividend.isDivisibleBy(divisor, env)) {
            return dividend.divideAsInt(divisor, env);
        }
        return null;
    }
    
    @Override
    public Integer toIntegerInexact(MathEnv env) {
        Objects.requireNonNull(env, "env");
        // see above (toInteger).
        // this is less important, because we don't necessarily have
        // to return any particular value
        Number n = numerator;
        Number d = denominator;
        
        Number.Kind nKind = n.getNumberKind();
        Number.Kind dKind = d.getNumberKind();
        
        if (!nKind.isFraction() && !dKind.isFraction() && !nKind.isFloat() && !dKind.isFloat()) {
            Integer divisor = d.toIntegerInexact(env);
            
            // note: divisor could be rounded to 0
            if (!divisor.isZero(env)) {
                Integer dividend = n.toIntegerInexact(env);
                
                return dividend.divideAsInt(divisor, env);
            }
        }
        
        return env.divide(n, d).toIntegerInexact(env);
    }
    
    @Override
    public Float toFloat(MathEnv env) {
        Objects.requireNonNull(env, "env");
        Float numerator = this.numerator.toFloat(env);
        if (numerator == null) {
            return null;
        }
        Float denominator = this.denominator.toFloat(env);
        if (denominator == null) {
            return null;
        }
        return numerator.divideAsFloat(denominator, env);
    }
    
    @Override
    public Float toFloatInexact(MathEnv env) {
        Objects.requireNonNull(env, "env");
        Float numerator   = this.numerator.toFloatInexact(env);
        Float denominator = this.denominator.toFloatInexact(env);
        
        return numerator.divideAsFloat(denominator, env);
    }
    
    @Override
    public int signum() {
        return numerator.signum() * denominator.signum();
    }
    
    public Number getNumerator() {
        return numerator;
    }
    
    public Number getDenominator() {
        return denominator;
    }
    
    @Override
    public Number negate(MathEnv env) {
        Objects.requireNonNull(env, "env");
        return Fraction.create(numerator.negate(env), denominator, env).collapse(env);
    }
    
    @Override
    public Number abs(MathEnv env) {
        Objects.requireNonNull(env, "env");
        // This logic looks weird, but here is the reason:
        //  Sometimes the method might return something that's
        //  not a fraction (such as the negation of, say, MathEnv64 Long.MIN_VALUE / N),
        //  so we want to be able to collapse it.
        //  Even in cases which are not that, we call this.collapse(...) to make
        //  sure that the collapsing behavior is consistent in all cases.
        Fraction abs = this;
        // We do this instead of this.signum() < 0, because we also want
        // to normalize the signs (negate does this, so we want it to be
        // consistent).
        if (numerator.signum() < 0 || denominator.signum() < 0) {
            abs = Fraction.create(numerator.abs(env), denominator.abs(env), env);
        }
        return abs.collapse(env);
    }
    
    @Override
    public Number floor(MathEnv env) {
        Objects.requireNonNull(env, "env");
        Integer n = numerator.toInteger(env);
        Integer d = denominator.toInteger(env);
        
        if (n == null || d == null) {
            return this.toFloatInexact(env).floor(env);
        }
        
        return n.floorDiv(d, env);
    }
    
    @Override
    public Number ceil(MathEnv env) {
        Objects.requireNonNull(env, "env");
        Integer n = numerator.toInteger(env);
        Integer d = denominator.toInteger(env);
        
        if (n == null || d == null) {
            return this.toFloatInexact(env).ceil(env);
        }
        
        return n.ceilDiv(d, env);
    }
    
    @Override
    public Number reciprocal(MathEnv env) {
        return env.valueOf(denominator, numerator);
    }
    
    @Override
    public int relateTo(Fraction that, MathEnv env) {
        Objects.requireNonNull(that, "that");
        Objects.requireNonNull(env,  "env");
        
        Integer numeratorL   = this.numerator.toInteger(env);
        Integer denominatorL = this.denominator.toInteger(env);
        Integer numeratorR   = that.numerator.toInteger(env);
        Integer denominatorR = that.denominator.toInteger(env);
        
        if (nullCount(numeratorL, denominatorL, numeratorR, denominatorR) != 0) {
            return this.toFloatInexact(env).relateTo(that.toFloatInexact(env), env);
        }
        
        if (env.isEqual(denominatorL, denominatorR)) {
            return numeratorL.relateTo(numeratorR, env);
        }
        
        Integer crossL = numeratorL.multiply(denominatorR, env).toIntegerExact(env);
        Integer crossR = numeratorR.multiply(denominatorL, env).toIntegerExact(env);
        
        return crossL.relateTo(crossR, env);
    }
    
    private Number addImpl(Fraction rhs, MathEnv env, BinaryOperator<Number> op) {
        Objects.requireNonNull(rhs, "rhs");
        Objects.requireNonNull(env, "env");
        
        Number numeratorL   = this.numerator;
        Number denominatorL = this.denominator;
        Number numeratorR   = rhs.numerator;
        Number denominatorR = rhs.denominator;
        
        if (env.isEqual(denominatorL, denominatorR)) {
            return Fraction.create(op.apply(numeratorL, numeratorR), denominatorL, env).collapse(env);
        }
        
        Number numerator   = op.apply(env.multiply(numeratorL, denominatorR), env.multiply(numeratorR, denominatorL));
        Number denominator = env.multiply(denominatorL, denominatorR);
        
        return Fraction.create(numerator, denominator, env).collapse(env);
    }
    
    @Override
    public Number add(Fraction rhs, MathEnv env) {
        return addImpl(rhs, env, env.add());
    }
    
    @Override
    public Number subtract(Fraction rhs, MathEnv env) {
        return addImpl(rhs, env, env.subtract());
    }
    
    @Override
    public Number multiply(Fraction rhs, MathEnv env) {
        Objects.requireNonNull(rhs, "rhs");
        Objects.requireNonNull(env, "env");
        
        Number numerator   = env.multiply(this.numerator,   rhs.numerator);
        Number denominator = env.multiply(this.denominator, rhs.denominator);
        
        return Fraction.create(numerator, denominator, env).collapse(env);
    }
    
    @Override
    public Number divide(Fraction rhs, MathEnv env) {
        Objects.requireNonNull(rhs, "rhs");
        Objects.requireNonNull(env, "env");
        
        Number numerator   = env.multiply(this.numerator,   rhs.denominator);
        Number denominator = env.multiply(this.denominator, rhs.numerator);
        
        return Fraction.create(numerator, denominator, env).collapse(env);
    }
    
    @Override
    public Number mod(Fraction rhs, MathEnv env) {
        Objects.requireNonNull(rhs, "rhs");
        Objects.requireNonNull(env, "env");
        
        Number quotient = divide(rhs, env);
        int    sign     = quotient.signum();
        if (sign == 0) {
            return env.zero();
        }
        
        try {
            if (sign < 0) {
                quotient = quotient.negate(env);
            }
            
            Integer floorQ = quotient.floor(env).toInteger(env);
            if (floorQ != null) {
                Number floorD = env.multiply(rhs, floorQ);
                if (sign < 0) {
                    floorD = floorD.negate(env);
                }
                Number remain = env.subtract(this, floorD);
                return remain;
            }
        } catch (UnsupportedException x) {
            // catch anything like integer overflow exceptions or whatever
            Log.caught(getClass(), "mod", x, true);
        }
        
        return toFloatInexact(env).mod(rhs.toFloatInexact(env), env);
    }
    
    @Override
    public boolean isTruthy(MathEnv env) {
        Objects.requireNonNull(env, "env");
        return numerator.isTruthy(env) && denominator.isTruthy(env);
    }
    
    @Override
    public boolean isOne(MathEnv env) {
        Objects.requireNonNull(env, "env");
        if (isDefined(env)) {
            if (numerator.equals(denominator))
                return true;
            if (env.relate(numerator, denominator) == 0)
                return true;
        }
        return false;
    }
    
    @Override
    public boolean isZero(MathEnv env) {
        Objects.requireNonNull(env, "env");
        return isDefined(env) && numerator.isZero(env);
    }
    
    @Override
    public boolean isDefined(MathEnv env) {
        Objects.requireNonNull(env, "env");
        return numerator.isDefined(env) && denominator.isDefined(env) && !denominator.isZero(env);
    }
    
    @Override
    public boolean isInteger(MathEnv env) {
        Objects.requireNonNull(env, "env");
        if (numerator.isInteger(env) && denominator.isInteger(env)) {
            return ((Integer) numerator).isDivisibleBy((Integer) denominator, env);
        }
        return false;
    }
    
    @Override
    public boolean isApproximate() {
        return numerator.isApproximate() || denominator.isApproximate();
    }
    
    // TODO:
    //  * should fractions get collapsed to float when displayed inside a tuple result?
    //  * should the numerator and denominator get formatted by the AnswerFormatter, if any?
    @Override
    public StringBuilder toResultString(StringBuilder b, Context context, MutableBoolean approx) {
        MathEnv env = context.getMathEnvironment();
        
        if (isZero(env)) {
            return b.append('0');
        }
        Integer intVal = this.toInteger(env);
        if (intVal != null) {
            return intVal.toResultString(b, context, approx);
        }
        
        if (numerator.isScalar() && denominator.isScalar()) {
            int sign = signum();
            if (sign < 0) {
                b.append('-');
            }
            
            numerator.abs(env).toResultString(b, context, approx);
            b.append("/");
            denominator.abs(env).toResultString(b, context, approx);
        } else {
            numerator.toResultString(b, context, approx);
            b.append("/");
            denominator.toResultString(b, context, approx);
        }
        
        return b;
    }
    
    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }
    
    @Override
    public int hashCode() {
        return (31 * numerator.hashCode()) + denominator.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Fraction) {
            Fraction that = (Fraction) obj;
            return this.numerator.equals(that.numerator)
                && this.denominator.equals(that.denominator);
        }
        return false;
    }
}