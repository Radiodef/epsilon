/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.util.*;

import java.math.*;
import java.util.function.*;

public final class Integer64 implements Integer {
    public static final Integer64 ZERO = new Integer64(0);
    public static final Integer64 ONE  = new Integer64(1);
    
    private final long value;
    
    public Integer64(long value) {
        this.value = value;
    }
    
    public long longValue() {
        return value;
    }
    
    @Override
    public BigInteger toBigIntegerExact(MathEnv env) {
        return BigInteger.valueOf(value);
    }
    
    @Override
    public int toPrimitiveIntExact(MathEnv env) {
        return Math.toIntExact(value);
    }
    
    @Override
    public Float toFloat(MathEnv env) {
        long   longValue   = this.value;
        double doubleValue = longValue;
        
        if (BigInteger.valueOf(longValue).equals(new BigDecimal(doubleValue).toBigIntegerExact())) {
            return Float64.exact(doubleValue);
//            return new Float64(doubleValue, false);
        }
        
        return null;
    }
    
    @Override
    public Float toFloatInexact(MathEnv env) {
        Float fl = this.toFloat(env);
        if (fl != null) {
            return fl;
        } else {
            return Float64.approx(this.value);
//            return new Float64(this.value, true);
        }
    }
    
    @Override
    public int signum() {
        long value = this.value;
        return (value < 0) ? -1 : (value > 0) ? +1 : 0;
    }
    
    public static final Float64 APPROX_ABS_MIN_VALUE = Float64.approx(Math.pow(2, 63));
    
    @Override
    public Number negate(MathEnv env) {
        long value = this.value;
        if (value == Long.MIN_VALUE) {
            return APPROX_ABS_MIN_VALUE;
//            throw UnsupportedException.overflow(BigInteger.valueOf(value).negate());
        }
        return new Integer64(-value);
    }
    
    @Override
    public Number abs(MathEnv env) {
        if (value < 0) {
            return negate(env);
        }
        return this;
    }
    
    @Override
    public boolean isTruthy(MathEnv env) {
        return value != 0;
    }
    
    @Override
    public boolean isZero(MathEnv env) {
        return value == 0;
    }
    
    @Override
    public boolean isOne(MathEnv env) {
        return value == 1;
    }
    
    @Override
    public boolean isInteger(MathEnv env) {
        return true;
    }
    
    @Override
    public boolean isDefined(MathEnv env) {
        return true;
    }
    
    @Override
    public boolean isDivisibleBy(Integer rhs, MathEnv env) {
        return (value % ((Integer64) rhs).value) == 0;
    }
    
    @Override
    public int relateTo(Integer that, MathEnv env) {
        long lhs = this.value;
        long rhs = ((Integer64) that).value;
        return (lhs < rhs) ? -1 : (lhs > rhs) ? +1 : 0;
    }
    
    private static Number applyExact(Integer64 self,
                                     Integer   rhs,
                                     MathEnv   env,
                                     LongBinaryOperator         longOp,
                                     DoubleBinaryOperator       doubleOp,
                                     BinaryOperator<BigInteger> bigOp) {
        long valL = self.value;
        long valR = ((Integer64) rhs).value;
        try {
            long result = longOp.applyAsLong(valL, valR);
            return env.valueOf(result);
        } catch (ArithmeticException x) {
            double result = doubleOp.applyAsDouble(valL, valR);
            if (Math2.isReal(result)) {
                return Float64.approx(result);
            } else {
                BigInteger exact = bigOp.apply(BigInteger.valueOf(valL), BigInteger.valueOf(valR));
                throw UnsupportedException.overflow(exact);
            }
        }
    }
    
    @Override
    public Number add(Integer rhs, MathEnv env) {
        return applyExact(this, rhs, env, Math2.addingExactLongs(),
                                          Math2.addingDoubles(),
                                          Math2.addingBigIntegers());
    }
    
    @Override
    public Number subtract(Integer rhs, MathEnv env) {
        return applyExact(this, rhs, env, Math2.subtractingExactLongs(),
                                          Math2.subtractingDoubles(),
                                          Math2.subtractingBigIntegers());
    }
    
    @Override
    public Number multiply(Integer rhs, MathEnv env) {
        return applyExact(this, rhs, env, Math2.multiplyingExactLongs(),
                                          Math2.multiplyingDoubles(),
                                          Math2.multiplyingBigIntegers());
    }
    
    @Override
    public Number divide(Integer rhs, MathEnv env) {
        long dividend = value;
        long divisor  = ((Integer64) rhs).value;
        
        if (divisor == 0) {
            throw UndefinedException.divisionByZero(this, rhs);
        }
        
        long quotient = dividend / divisor;
        
        if ((quotient * divisor) == dividend) {
            return env.valueOf(quotient);
        } else {
            return Fraction.create(this, rhs, env).collapse(env);
        }
    }
    
    @Override
    public Integer divideAsInt(Integer rhs, MathEnv env) {
        long dividend = value;
        long divisor  = ((Integer64) rhs).value;
        if (divisor == 0) {
            throw UndefinedException.divisionByZero(this, rhs);
        }
        
        long quotient = dividend / divisor;
        
        return env.valueOf(quotient);
    }
    
    @Override
    public Number mod(Integer rhs, MathEnv env) {
        long dividend = value;
        long divisor  = ((Integer64) rhs).value;
        
        if (divisor == 0) {
            throw UndefinedException.divisionByZero(this, rhs);
        }
        
        long remainder = dividend % divisor;
        return env.valueOf(remainder);
    }
    
    @Override
    public String toString() {
        return Long.toString(value);
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context context, MutableBoolean approx) {
        return b.append(value);
    }
    
    @Override
    public int hashCode() {
        return Long.hashCode(value);
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Integer64) && value == ((Integer64) obj).value;
    }
}