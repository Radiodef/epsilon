/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.app.*;
import eps.job.*;
import eps.util.*;
import static eps.util.Misc.*;

import java.util.*;
import java.math.*;

public final class MathEnv64 extends AbstractMathEnv {
    public static final Float64 E  = Float64.approx(Math.E);
    public static final Float64 PI = Float64.approx(Math.PI);
    
    MathEnv64() {
    }
    
    @Override
    protected Primes64 createPrimes() {
        return new Primes64(this);
    }
    
    @Override
    public Number e() {
        return E;
    }
    
    @Override
    public Number pi() {
        return PI;
    }
    
    @Override
    public Decimal toDecimal(Number n) {
        switch (n.getNumberKind()) {
            case BOOLEAN:
                return this.toDecimal(n.toIntegerExact(this));
            case INTEGER:
                return Decimal.valueOf(((Integer64) n).longValue());
            case FLOAT:
                return Decimal.valueOf(((Float64) n).doubleValue());
            case FRACTION:
                Fraction f = (Fraction) n;
                
                Number numerator   = f.getNumerator();
                Number denominator = f.getDenominator();
                
                Integer intNumerator   = numerator.toInteger(this);
                Integer intDenominator = denominator.toInteger(this);
                
                if (intNumerator != null && intDenominator != null) {
                    return Decimal.valueOf(((Integer64) intNumerator).longValue(),
                                           ((Integer64) intDenominator).longValue());
                }
        }
        Float asFloat = n.toFloat(this);
        if (asFloat != null) {
            return this.toDecimal(asFloat);
        }
        throw UnsupportedException.operandType(n, "conversion to decimal");
    }
    
    @Override
    protected Integer newInteger(long n) {
        return new Integer64(n);
    }
    
    @Override
    protected Float newFloat(double n, boolean approx) {
        return new Float64(n, approx);
    }
    
    @Override
    public Number valueOf(BigInteger n) {
        try {
            return this.valueOf(n.longValueExact());
        } catch (ArithmeticException x) {
            try {
                return Float64.approx(n.doubleValue());
            } catch (EvaluationException y) {
                throw UnsupportedException.overflow(n);
            }
        }
    }
    
    @Override
    public Integer gcd(Integer a, Integer b) {
        long c = ((Integer64) a).longValue();
        long d = ((Integer64) b).longValue();
        
        while (d != 0) {
            Worker.executing();
            long t = d;
            d = c % d;
            c = t;
        }
        
        return this.valueOf(c);
    }
    
    @Override
    public Number factorial(Number n) {
        if (n.isInteger(this)) {
            long longVal = ((Integer64) n.toIntegerExact(this)).longValue();
            if (longVal < 0) {
                // TODO
                throw UndefinedException.negativeFactorial();
            }
            
            long result = 1;
            
            for (; longVal > 1; --longVal) {
                Worker.executing();
                try {
                    result = Math.multiplyExact(result, longVal);
                    
                } catch (ArithmeticException x) {
                    double dresult = result;
                    
                    for (; longVal > 1; --longVal) {
                        Worker.executing();
                        double product = dresult * (double) longVal;
                        
                        if (Double.isInfinite(product)) {
                            BigDecimal big = BigDecimal.valueOf(dresult).multiply(new BigDecimal(longVal));
                            throw UnsupportedException.overflow(big.toBigInteger());
                        }
                        
                        dresult = product;
                    }
                    
                    return Float64.approx(dresult);
                }
            }
            
            return this.valueOf(result);
        }
        // TODO
        throw UndefinedException.fractionalFactorial();
    }
    
    private Number floatSqrt(double n) {
        return Float64.approx(Math.sqrt(n));
    }
    
    long integerSqrt(long in) {
        return integerSqrt(in, null);
    }
    
    // Computes floor(sqrt(n)) in log(n) time.
    long integerSqrt(long in, MutableInt iterationsOut) {
        if (in < 0) {
            throw new UndefinedException("square root of a negative number: " + in);
        }
        if (in == 0 || in == 1) {
            return in;
        }
        
        Log.low(MathEnv64.class, "integerSqrt", "Attempting integer square root for ", in);
        
        long lo = 1;
        long hi = in >> 1; // start at one half the input.
        
        int iterations = 1;
        
        long result;
        
        // This is just a straight binary search. I'm sure there's
        // a much better method, but this seems fine for now.
        for (;;) {
            Worker.executing();
            long distance = hi - lo;
            
            if (distance <= 1) {
                // Do not oscillate.
                result = ((hi * hi) == in) ? hi : lo;
                break;
            }
            
            long mid = lo + (distance >> 1);
            long sq  = Math2.squareExact(mid);
//            long sq  = mid * mid;
//            long sq;
//            try {
//                sq = Math.multiplyExact(mid, mid);
//            } catch (ArithmeticException x) {
//                sq = -1;
//            }
            
            if (sq == in) {
                result = mid;
                break;
            }
            // testing for overflow
            //             vvvvvvv
            if (sq < in && sq > 0) {
                lo = mid;
            } else {
                hi = mid;
            }
    
            // noinspection ConstantConditions
            assert (lo < hi) && (lo > 0) && (hi > 0) : f("lo = %d, hi = %d", lo, hi);
            ++iterations;
        }
        
        assert (result <= in) && (result > 0) && (result * result > 0)
             : f("in = %d, result = %d, result*result = ", in, result, result * result);
        
        // Around 60 seems to be the upper bound. That seems
        // correct since that's around log2(Long.MAX_VALUE).
        Log.low(MathEnv64.class, "integerSqrt", "Sqrt is ", result, " in ", iterations, " iterations.");
        if (iterationsOut != null) {
            iterationsOut.value = iterations;
        }
        return result;
    }
    
    @Override
    public Number sqrt(Number n) {
        int sign = n.signum();
        if (sign == 0 && n.isInteger(this)) {
            return zero();
        }
        if (sign < 0) {
            // TODO
            throw UndefinedException.negativeRoot(n);
        }
        
        switch (n.getNumberKind()) {
            case BOOLEAN:
                return n.toIntegerExact(this); // (sqrt 1) == 1; (sqrt 0) == 0;
                
            case INTEGER:
                long longVal = ((Integer64) n).longValue();
                long intSqrt = integerSqrt(longVal);
                
                if ((intSqrt * intSqrt) == longVal) {
                    return this.valueOf(intSqrt);
                }
                
//                assert false : longVal + ", " + intSqrt;
                return this.floatSqrt(longVal);
                
            case FLOAT:
                return this.floatSqrt(((Float64) n).doubleValue());
                
            case FRACTION:
                Fraction f = (Fraction) n;
                
                Number numerator   = f.getNumerator();
                Number denominator = f.getDenominator();
                
                return this.valueOf(sqrt(numerator), sqrt(denominator));
        }
        
        throw UnsupportedException.operandType(n, "square root");
    }
    
    long integerNrt(long rt, long n) {
        assert rt > 0 : rt + " root of " + n;
        assert n >= 0 || (rt & 1) == 1 : rt + " root of " + n;
        
        if (n  == 0) return 0;
        if (n  == 1) return 1;
        if (rt == 1) return n;
        
        long init = n;
        long est  = n / rt;
        
        long lo = 1;
        long hi = est;
        
        for (;;) {
            Worker.executing();
            long dist = hi - lo;
            
            if (dist <= 1) {
                try {
                    if (powBySquaring(hi, rt) == init) {
                        return hi;
                    }
                } catch (ArithmeticException ignored) {
                }
                return lo;
            }
            
            long mid = lo + (dist >>> 1);
            try {
                long pow = powBySquaring(mid, rt);
                
                if (pow == init)
                    return mid;
                if (pow < init)
                    lo = mid;
                if (pow > init)
                    hi = mid;
            } catch (ArithmeticException overflow) {
                Log.caught(MathEnv64.class, "integerNrt", overflow, true);
                hi = mid;
            }
        }
    }
    
    private Number integerNrt(Integer index, Integer radicand) {
        long longIndex    = ((Integer64) index).longValue();
        long longRadicand = ((Integer64) radicand).longValue();
        
        if (longIndex == 0) {
            throw UndefinedException.fmt("0th root of %d is undefined", longRadicand);
        }
        if (longIndex == 1) {
            return radicand;
        }
        if (longRadicand == 0 || longRadicand == 1) {
            return radicand;
        }
        
        try {
            boolean negativeIndex = longIndex < 0;
            if (negativeIndex) {
                longIndex = Math.negateExact(longIndex);
            }
            
            boolean negativeRadicand = longRadicand < 0;
            if (negativeRadicand) {
                if ((longIndex & 1L) == 0L) {
                    throw UndefinedException.fmt("found root %d of %d; complex numbers unsupported",
                                                 longIndex, longRadicand);
                }

                longRadicand = Math.negateExact(longRadicand);
            }
            
            long longRoot = integerNrt(longIndex, longRadicand);
            
            if (powBySquaring(longRoot, longIndex) == longRadicand) {
                if (negativeRadicand) {
                    longRoot = Math.negateExact(longRoot);
                }
                
                Number result = valueOf(longRoot);
                if (negativeIndex) {
                    result = result.reciprocal(this);
                }
                
                return result;
            }
        } catch (ArithmeticException x) {
            Log.caught(MathEnv64.class, "nrt", x, true);
        }
        
        return floatNrt(index, radicand);
    }
    
    private Float64 floatNrt(Number index, Number radicand) {
        Float64 indexAsFloat    = (Float64) index.toFloatInexact(this);
        Float64 radicandAsFloat = (Float64) radicand.toFloatInexact(this);
        
        double indexAsDouble    = indexAsFloat.doubleValue();
        double radicandAsDouble = radicandAsFloat.doubleValue();
        
        if (radicandAsDouble < 0) {
            throw UndefinedException.fmt("cannot determine approximate root of a negative number: %s", radicand);
        }
        
        if (indexAsDouble == 0) {
            throw UndefinedException.fmt("0th root of %s is undefined", radicand);
        }
        if (indexAsDouble == 1) {
            return radicandAsFloat;
        }
        
        if (radicandAsDouble == 0) {
            return Float64.APPROX_ZERO;
        }
        if (radicandAsDouble == 1) {
            return Float64.APPROX_ONE;
        }
        
        double base = radicandAsDouble;
        double exp  = 1 / indexAsDouble;
        
        double pow = Math.pow(base, exp);
        return Float64.approx(pow);
    }
    
    @Override
    public Number nrt(Number index, Number radicand) {
        Objects.requireNonNull(index,    "index");
        Objects.requireNonNull(radicand, "radicand");
        
        if (index.isOne(this) && !index.isApproximate()) {
            return radicand;
        }
        
        switch (radicand.getNumberKind()) {
            case INTEGER:
            case BOOLEAN:
                Integer intIndex = index.toInteger(this);
                if (intIndex != null) {
                    return integerNrt(intIndex, radicand.toIntegerExact(this));
                }
                if (index.getNumberKind().isFraction()) {
                    Fraction fraction = (Fraction) index;
                    Number   numer    = fraction.getNumerator();
                    Number   denom    = fraction.getDenominator();
                    
                    return nrt(numer, pow(radicand, denom));
                }
                break;
                
            case FRACTION:
                Fraction fraction = (Fraction) radicand;
                Number   numer    = fraction.getNumerator();
                Number   denom    = fraction.getDenominator();
                
                return divide(nrt(index, numer), nrt(index, denom));
                
            default:
                break;
        }
        
        return floatNrt(index, radicand);
    }
    
    private double powAsDouble(Number base, double val, Number exponent) {
        switch (exponent.getNumberKind()) {
            case BOOLEAN:
                return this.powAsDouble(base.toIntegerExact(this), val, exponent);
                
            case INTEGER:
                return Math.pow(val, ((Integer64) exponent).longValue());
                
            case FLOAT:
                if (exponent.isInteger(this)) {
                    return this.powAsDouble(base, val, exponent.toIntegerExact(this));
                }
                
                // Approximate values are indeterminate.
                if (base.signum() < 0) {
                    throw UndefinedException.fmt("possible root of a negative number: %s", base);
                }
                
                return Math.pow(val, ((Float64) exponent).doubleValue());
                
            case FRACTION:
                // TODO?
                return this.powAsDouble(base, val, exponent.toFloatInexact(this));
        }
        
        throw UnsupportedException.operandTypes(base, exponent, "exponentiation");
    }
    
    private Float powToFloat(Number base, double val, Number exponent) {
        return Float64.approx(this.powAsDouble(base, val, exponent));
    }
    
    // note: tested and works
    long powBySquaring_recursive(long base, long exponent) {
        if (exponent < 0) {
            // note: IllegalArgumentException could make more sense,
            //       but we throw ArithmeticException for the sake
            //       of conformity with the other thrown exceptions
            throw new ArithmeticException(base + "^" + exponent);
        }
        Worker.executing();
        
        if (exponent == 0) return 1;
        if (exponent == 1) return base;
        
        long half   = exponent >>> 1;
        long result = this.powBySquaring(base, half);
        
        result = Math.multiplyExact(result, result);
        
        if ((exponent & 1) == 1) {
            result = Math.multiplyExact(result, base);
        }
        
        return result;
    }
    
    long powBySquaring(long base, long exponent) {
        if (exponent < 0) {
            // note: see above in recursive version
            throw new ArithmeticException(base + "^" + exponent);
        }
        
        if (exponent == 0) return 1;
        if (exponent == 1) return base;
        
        long odds = 1;
        
        for (;;) {
            Worker.executing();
            
            if ((exponent & 1) == 1) {
                odds = Math.multiplyExact(odds, base);
                --exponent;
            }
            
            exponent >>>= 1;
            base = Math.multiplyExact(base, base);
            
            if (exponent == 1) {
                return Math.multiplyExact(base, odds);
            }
        }
    }
    
    @Override
    public Number pow(Number base, Number exponent) {
        int baseSign = base.signum();
        int expSign  = exponent.signum();
        if (baseSign == 0 && expSign == 0) {
            throw new UndefinedException("0 to the 0th power");
        }
        if (baseSign == 0) {
            if (expSign < 0) {
                throw UndefinedException.fmt("%s raised to the power of %s", base, exponent);
            }
            return (base.isApproximate() || exponent.isApproximate()) ? Float64.APPROX_ZERO : Integer64.ZERO;
        }
        if (expSign == 0) {
            return (base.isApproximate() || exponent.isApproximate()) ? Float64.APPROX_ONE : Integer64.ONE;
        }
        // TODO: special case for x^1?
        
        boolean negative = false;
        
        if (expSign < 0) {
            negative = true;
            exponent = exponent.negate(this);
        }
        
        Number result;
        
        switch (base.getNumberKind()) {
            case BOOLEAN:
                result = this.pow(base.toIntegerExact(this), exponent);
                break;
            
            case FRACTION:
                Fraction f = (Fraction) base;
                
                try {
                    Number numerator   = this.pow(f.getNumerator(),   exponent);
                    Number denominator = this.pow(f.getDenominator(), exponent);
                    
                    result = this.valueOf(numerator, denominator);
                } catch (EvaluationException x) {
                    result = this.pow(f.toFloatInexact(this), exponent);
                }
                break;
                
            case INTEGER:
                if (exponent.getNumberKind().isFraction()) {
                    Fraction fraction = (Fraction) exponent;
                    Number   numer    = fraction.getNumerator();
                    Number   denom    = fraction.getDenominator();
                    
                    result = this.nrt(denom, this.pow(base, numer));
                    break;
                }
                
                long longBase = ((Integer64) base).longValue();
                
                if (exponent.isInteger(this)) {
                    Integer64 intExp  = (Integer64) exponent.toIntegerExact(this);
                    long      longExp = intExp.longValue();
                    
                    // Note: Exponentiation by squaring is log(n) time so we
                    //       could go much larger, except for the fact of overflow.
                    //     ~~It could also cause a StackOverflowError, potentially.~~ <- not anymore
                    //       Something to think about. An iterative approach would
                    //       be better.
                    final long powBySquaringMaxExponent = 62;
                    
                    if (longExp <= powBySquaringMaxExponent) {
                        Log.low(MathEnv64.class, "pow", "exponentiation by squaring on ", base, " and ", exponent, ".");
                        
                        try {
                            result = this.valueOf(powBySquaring(longBase, longExp));
                            break;
                        } catch (ArithmeticException overflow) {
                            Log.caught(MathEnv64.class, "during exponentiation by squaring", overflow, true);
                            // use floating point
                        }
                    }
                }
                
                result = this.powToFloat(base, longBase, exponent);
                break;
            case FLOAT:
                result = this.powToFloat(base, ((Float64) base).doubleValue(), exponent);
                break;
                
            default:
                throw UnsupportedException.operandTypes(base, exponent, "exponentiation");
        }
        
        if (negative) {
            return result.reciprocal(this);
        } else {
            return result;
        }
    }
    
    private Float64 applyAsFloat(Number n, java.util.function.DoubleUnaryOperator op) {
        Float64 fl = (Float64) n.toFloatInexact(this);
        return Float64.approx(op.applyAsDouble(fl.doubleValue()));
    }
    
    @Override
    public Number sin(Number theta) {
        return this.applyAsFloat(theta, Math::sin);
    }
    
    @Override
    public Number cos(Number theta) {
        return this.applyAsFloat(theta, Math::cos);
    }
    
    @Override
    public Number tan(Number theta) {
        // TODO:
        //  check for pi / 2? Float64 constructor already throws...
        Float64 tan = this.applyAsFloat(theta, Math::tan);
        return tan;
    }
    
    @Override
    public Number csc(Number theta) {
        return this.applyAsFloat(theta, t -> 1 / Math.sin(t));
    }
    
    @Override
    public Number sec(Number theta) {
        return this.applyAsFloat(theta, t -> 1 / Math.cos(t));
    }
    
    @Override
    public Number cot(Number theta) {
        return this.applyAsFloat(theta, t -> 1 / Math.tan(t));
    }
    
    @Override
    public Number asin(Number sin) {
        return this.applyAsFloat(sin, Math::asin);
    }
    
    @Override
    public Number acos(Number cos) {
        return this.applyAsFloat(cos, Math::acos);
    }
    
    @Override
    public Number atan(Number tan) {
        return this.applyAsFloat(tan, Math::atan);
    }
    
    @Override
    public Number atan2(Number y, Number x) {
        double doubleY = ((Float64) y.toFloatInexact(this)).doubleValue();
        double doubleX = ((Float64) x.toFloatInexact(this)).doubleValue();
        return Float64.approx(Math.atan2(doubleY, doubleX));
    }
    
    @Override
    public Number ln(Number n) {
        if (n.signum() <= 0) {
            throw UndefinedException.negativeOrZeroLog(n);
        }
        if (n.isInteger(this) && n.isOne(this)) {
            return zero();
        }
        
        Float64 floatVal = (Float64) n.toFloatInexact(this);
        
        return Float64.approx(Math.log(floatVal.doubleValue()));
    }
    
    @Override
    public Number log10(Number n) {
        if (n.signum() <= 0) {
            throw UndefinedException.negativeOrZeroLog(n);
        }
        Integer intVal = n.toInteger(this);
        if (intVal != null) {
            long longVal = ((Integer64) intVal).longValue();
            if (longVal == 1)
                return this.zero();
            int log10 = Math2.floorLog10(longVal);
            if (this.powBySquaring(10, log10) == longVal) {
                return this.valueOf(log10);
            }
        }
        
        Float64 floatVal = (Float64) n.toFloatInexact(this);
        
        return Float64.approx(Math.log10(floatVal.doubleValue()));
    }
    
    @Override
    public Number log(Number base, Number n) {
        if (n.signum() <= 0) {
            throw UndefinedException.negativeOrZeroLog(base, n);
        }
        if (base.signum() <= 0) {
            throw UndefinedException.fmt("negative or zero logarithm base: %s", base);
        }
        
        Integer64 intBase = (Integer64) base.toInteger(this);
        Integer64 intVal  = (Integer64) n.toInteger(this);
        if (intVal != null) {
            long longVal = intVal.longValue();
            if (intBase != null) {
                long longBase = intBase.longValue();
                
                if (longBase == 1) {
                    throw UndefinedException.baseOneLog(n);
                }
                
                long result = 0;
                for (;;) {
                    if (longVal == 1) {
                        return valueOf(result);
                    }
                    if (longVal % longBase != 0) {
                        break;
                    }
                    longVal /= longBase;
                    ++result;
                }
            } else if (longVal == 1) {
                return Float64.APPROX_ZERO;
            }
        }
        
        Float64 floatBase = (Float64) base.toFloatInexact(this);
        Float64 floatVal  = (Float64) n.toFloatInexact(this);
        
        if (floatBase.doubleValue() == 1) {
            throw UndefinedException.baseOneLog(n);
        }
        
        return Float64.approx(Math.log(floatVal.doubleValue()) / Math.log(floatBase.doubleValue()));
    }
}