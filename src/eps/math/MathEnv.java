/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.app.*;
import eps.util.*;

import java.math.*;
import java.util.*;
import java.util.function.*;

public interface MathEnv {
    // Numbers:
    
    Decimal toDecimal(Number n);
    
    Integer valueOf(int n);
    Integer valueOf(long n);
    Number valueOf(BigInteger n);
    
    default Float valueOf(double d) {
        return approxValueOf(d);
    }
    
    Float exactValueOf(double d);
    Float approxValueOf(double d);
    
    default Number valueOf(Number numerator, Number denominator) {
        if (!(numerator.isApproximate() || denominator.isApproximate())) {
            // use relateTo?
            if (numerator.equals(denominator)) {
                return one();
            }
            if (numerator.isZero(this) && !denominator.isZero(this)) {
                return zero();
            }
        }
        if (numerator.isInteger(this) && denominator.isInteger(this)) {
            return Fraction.create(numerator, denominator, this).collapseIfInteger(this);
        }
        return divide(numerator, denominator);
    }
    
    default Number valueOf(long numerator, long denominator) {
        return this.valueOf(this.valueOf(numerator), this.valueOf(denominator));
    }
    
    default Integer one() {
        return valueOf(1);
    }
    
    default Integer zero() {
        return valueOf(0);
    }
    
    default Integer negativeOne() {
        return valueOf(-1);
    }
    
    default Fraction oneOverOne() {
        return Fraction.createUnchecked(one(), one());
    }
    
    default Fraction zeroOverOne() {
        return Fraction.createUnchecked(zero(), one());
    }
    
    Comparator<Integer> comparingIntegers();
    
    Primes primes();
    
    Number e();
    Number pi();
    
    // Operations:
    
    Integer gcd(Integer lhs, Integer rhs);
    
    int relate(Number lhs, Number rhs);
    
    default boolean isEqual(Number lhs, Number rhs) {
        return this.relate(lhs, rhs) == 0;
    }
    
    // round
    
    Number add(Number lhs, Number rhs);
    Number subtract(Number lhs, Number rhs);
    Number multiply(Number lhs, Number rhs);
    Number divide(Number lhs, Number rhs);
    Number mod(Number lhs, Number rhs);
    
    default BinaryOperator<Number> add()      { return this::add;      }
    default BinaryOperator<Number> subtract() { return this::subtract; }
    default BinaryOperator<Number> multiply() { return this::multiply; }
    default BinaryOperator<Number> divide()   { return this::divide;   }
    default BinaryOperator<Number> mod()      { return this::mod;      }
    
    default Boolean or(Number lhs, Number rhs) {
        return Boolean.valueOf(lhs.toBoolean(this).booleanValue() || rhs.toBoolean(this).booleanValue());
    }
    
    default Boolean and(Number lhs, Number rhs) {
        return Boolean.valueOf(lhs.toBoolean(this).booleanValue() && rhs.toBoolean(this).booleanValue());
    }
    
    default Boolean xor(Number lhs, Number rhs) {
        return Boolean.valueOf(lhs.toBoolean(this).booleanValue() ^ rhs.toBoolean(this).booleanValue());
    }
    
    default Boolean not(Number n) {
        return Boolean.valueOf(!n.toBoolean(this).booleanValue());
    }
    
    Number factorial(Number n);
    Number sqrt(Number n);
    Number nrt(Number index, Number radicand);
    Number pow(Number base, Number exponent);
    
    Number sin(Number theta);
    Number cos(Number theta);
    Number tan(Number theta);
    
    Number asin(Number sin);
    Number acos(Number cos);
    Number atan(Number tan);
    Number atan2(Number y, Number x);
    
    Number csc(Number theta);
    Number sec(Number theta);
    Number cot(Number theta);
    
    Number ln(Number n);
    Number log10(Number n);
    Number log(Number base, Number n);
    
    // Static stuff:
    
    static MathEnv instance() {
        return instance(Settings.getOrDefault(Setting.MATH_ENV_CLASS));
    }
    
    static MathEnv instance(Class<?> clazz) {
        if (clazz == MathEnv64.class) {
            return MathEnvPrivate.mathEnv64Instance();
        }
        throw Errors.newIllegalArgument(clazz);
    }
    
    static boolean isEnvironment(Class<?> clazz) {
        return clazz == MathEnv64.class;
    }
    
    static void poke() {
        instance(MathEnv64.class);
    }
}

final class MathEnvPrivate {
    private MathEnvPrivate() {}
    
    private static volatile MathEnv64 MATH_ENV_64 = new MathEnv64();
    
    static MathEnv64 mathEnv64Instance() {
        return MATH_ENV_64;
    }
    
    static {
        App.whenAvailable(app -> {
            Runnable reinstantiator = () -> {
                MATH_ENV_64 = new MathEnv64();
            };
            
            Settings settings = app.getSettings();
            
            for (Setting<?> setting : Arrays.asList(Setting.INTEGER_CACHE_SIZE)) {
                settings.addChangeListener(setting, reinstantiator);
            }
        });
    }
}