/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.app.*;
import eps.job.*;

import java.math.*;
import java.util.*;

// TODO: convert computeNextAfter(...) to use the segmented sieve
public final class Primes64 extends Primes {
    private static final long NOT_PRIME = -1;
    
    private final int rangeLength;
    
    private final long[] temp;
    
    public Primes64(MathEnv64 env) {
        super(env);
        
        int cacheSize = Settings.getOrDefault(Setting.PRIME_CACHE_SIZE);
        rangeLength   = Math.min(Math.max(cacheSize / 2, 128), Math2.pow2(24));
        
        Log.note(Primes64.class, "<init>", "rangeLength is ", rangeLength);
        
        temp = new long[rangeLength];
        
        assert true || assertions();
    }
    
    @Override
    protected void init(List<Integer> primes) {
        primes.add(env.valueOf(2));
        primes.add(env.valueOf(3));
    }
    
    private long[] computeRange(long start, List<Integer> primes) {
        assert start >= 0 : start;
        assert ( start & 1 ) == 1 : start;
        
        long[] range = temp;
        int    len   = range.length;
        
        for (int i = 0; i < len; ++i) {
            range[i] = start + ( i << 1 );
        }
        
//        System.out.printf("before = %s%n", Arrays.toString(range));
        
        int  count = primes.size();
        long p     = NOT_PRIME;
        int  i     = 1;
        
        for (;;) {
            Worker.executing();
            
            if (i < count) {
                p = ((Integer64) primes.get(i)).longValue();
                ++i;
                
            } else {
                assert p != NOT_PRIME;
                long q = NOT_PRIME;
                
                long index = ( p - start ) >>> 1;
                if (index < 0 || len <= index)
                    index = 0;
                
                for (int j = (int) index; j < len; ++j) {
                    long r = range[j];
                    
                    if (r != NOT_PRIME && r > p) {
                        q = r;
                        break;
                    }
                }
                
                if (q == NOT_PRIME)
                    break;
                p = q;
            }
            
            long n = ( start / p ) * p;
            if (n < start)
                n += p;
            if (n == p)
                n += p;
            if ((n & 1) == 0)
                n += p;
            
            long inc = p << 1;
            if (inc < 0) {
                throw new ArithmeticException("p = " + p + ", inc = " + inc);
            }
            
            for (;;) {
                long next = ( n - start ) >>> 1;
                if (next >= len)
                    break;
//                System.out.printf("p = %s, n = %s, next = %s%n", p, n, range[(int) next]);
                
                range[(int) next] = NOT_PRIME;
                
                n += inc;
            }
        }
        
//        System.out.printf("after = %s%n", Arrays.toString(range));
        
        return range;
    }
    
    @Override
    protected void addNextAfterLast(List<Integer> primes) {
        int     sizeIn  = primes.size();
        Integer last    = primes.get(sizeIn - 1);
        long    longVal = ((Integer64) last).longValue();
        
        try {
            long start = Math.addExact(longVal, 1);
            if ((start & 1) == 0) {
                start = Math.addExact(start, 1);
            }
            
            for (;;) {
                Worker.executing();
                Log.low(Primes64.class, "addNextAfterLast", "start = ", start);
                
                long[] range = computeRange(start, primes);
                int    len   = range.length;
                long   next  = NOT_PRIME;
                
                for (int i = 0; i < len; ++i) {
                    long e = range[i];
                    if (e != NOT_PRIME) {
                        if (next == NOT_PRIME)
                            next = e;
                        primes.add(env.valueOf(e));
                    }
                }
                
                if (next != NOT_PRIME)
                    break;
                start = Math.addExact(start, len << 1);
            }
        } catch (ArithmeticException x) {
            Log.caught(Primes64.class, "fillInNextAfter", x, true);
            
            if (sizeIn >= primes.size()) {
                primes.add(computeNextAfter(last));
            }
        }
    }
    
//    private long[] computeRange(long start, List<Integer> primes) {
//        assert start >= 0 : start;
//        assert ( start & 1 ) == 1 : start;
//        
//        long[] range = temp;
//        int    len   = range.length;
//        
//        for (int i = 0; i < len; ++i) {
//            range[i] = start + i;
//        }
//        
//        int  count = primes.size();
//        long p     = NOT_PRIME;
//        
//        for (int i = 0;;) {
//            if (i < count) {
//                p = ((Integer64) primes.get(i)).longValue();
//                ++i;
//                
//            } else {
//                assert p != NOT_PRIME;
//                long q = NOT_PRIME;
//                
//                long index = p - start;
//                if (index < 0 || len <= index)
//                    index = 0;
//                
//                for (int j = (int) index; j < len; ++j) {
//                    long r = range[j];
//                    
//                    if (r != NOT_PRIME && r > p) {
//                        q = r;
//                        break;
//                    }
//                }
//                
//                if (q == NOT_PRIME)
//                    break;
//                p = q;
//            }
//            
//            long n = ( start / p ) * p;
//            if (n < start)
//                n += p;
//            if (n == p)
//                n += p;
//            
//            for (;;) {
//                long next = ( n - start );
//                if (next >= len)
//                    break;
//                
//                range[(int) next] = NOT_PRIME;
//                
//                n += p;
//            }
//        }
//        
//        return range;
//    }
//    
//    @Override
//    protected void addNextAfterLast(List<Integer> primes) {
//        int     sizeIn  = primes.size();
//        Integer last    = primes.get(sizeIn - 1);
//        long    longVal = ((Integer64) last).longValue();
//        
//        try {
//            long start = Math.addExact(longVal, 1);
//            if ((start & 1) == 0) {
//                start = Math.addExact(start, 1);
//            }
//            
//            for (;;) {
//                long[] range = computeRange(start, primes);
//                int    len   = range.length;
//                long   next  = NOT_PRIME;
//                
//                for (int i = 0; i < len; ++i) {
//                    long e = range[i];
//                    if (e != NOT_PRIME) {
//                        if (next == NOT_PRIME)
//                            next = e;
//                        primes.add(env.valueOf(e));
//                    }
//                }
//                
//                if (next != NOT_PRIME)
//                    break;
//                start = Math.addExact(start, len);
//            }
//        } catch (ArithmeticException x) {
//            Log.caught(Primes64.class, "fillInNextAfter", x, true);
//            
//            if (sizeIn >= primes.size()) {
//                primes.add(computeNextAfter(last));
//            }
//        }
//    }
    
    @Override
    public Integer computeNextAfter(Integer n) {
        return env.valueOf(computeNextAfterNaively(((Integer64) n).longValue()));
    }
    
    private long computeNextAfterNaively(long n) {
        if (n <= 1) {
            return 2;
        }
        
        for (;;) {
            long next = n + 1;
            if (next < 0) {
                throw UnsupportedException.overflow(BigInteger.valueOf(n).add(BigInteger.ONE));
            }
            if (isPrime(next)) {
                return next;
            }
            n = next;
        }
    }
    
    @Override
    public boolean isPrime(Integer n) {
        return isPrime(((Integer64) n).longValue());
    }
    
    private boolean isPrime(long n) {
        if (n <= 1)
            return false;
        if (n == 2)
            return true;
        if ((n & 1) == 0)
            return false;
        
        for (long f = 3; (f * f) <= n; f += 2) {
            Worker.executing();
            
            if ((n % f) == 0) {
//                System.out.println("f = "+f);
                return false;
            }
        }
        
        return true;
    }
}