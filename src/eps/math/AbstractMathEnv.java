/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.app.*;

import java.util.*;
import java.util.function.*;

public abstract class AbstractMathEnv implements MathEnv {
    private final Integer[] integerCache;
    
    private final Integer negativeOne;
    private final Integer zero;
    private final Integer one;
    private final Fraction oneOverOne;
    private final Fraction zeroOverOne;
    
    private final BinaryOperator<Number> addOp      = this::add;
    private final BinaryOperator<Number> subtractOp = this::subtract;
    private final BinaryOperator<Number> multiplyOp = this::multiply;
    private final BinaryOperator<Number> divideOp   = this::divide;
    private final BinaryOperator<Number> modOp      = this::mod;
    
    private final Comparator<Integer> comparingIntegers = (lhs, rhs) -> lhs.relateTo(rhs, this);
    
    private final Primes primes;
    
    private static final Map<Class<?>, MathEnv> INSTANCES =
        new java.util.concurrent.ConcurrentHashMap<>();
    
    public AbstractMathEnv() {
        if (INSTANCES.putIfAbsent(getClass(), this) != null) {
            throw new IllegalStateException(getClass().toString());
        }
        
        int       cacheSize    = Settings.getOrDefault(Setting.INTEGER_CACHE_SIZE);
        Integer[] integerCache = new Integer[cacheSize];
        
        int half = cacheSize / 2;
        
        for (int i = 0; i < cacheSize; ++i) {
            integerCache[i] = newInteger(i - half);
        }
        
        this.integerCache = integerCache;
        
        this.negativeOne    = valueOf(-1);
        this.zero           = valueOf(0);
        this.one            = valueOf(1);
        this.oneOverOne     = Fraction.createUnchecked(one, one);
        this.zeroOverOne    = Fraction.createUnchecked(zero, one);
        
        this.primes = createPrimes();
    }
    
    protected abstract Integer newInteger(long n);
    protected abstract Float newFloat(double n, boolean approx);
    
    @Override
    public Integer valueOf(int n) {
        Integer[] integerCache = this.integerCache;
        int       cacheSize    = integerCache.length;
        int       index        = n + (cacheSize / 2);
        if (0 <= index && index < cacheSize) {
            return integerCache[index];
        }
        return newInteger(n);
    }
    
    @Override
    public Integer valueOf(long n) {
        if (java.lang.Integer.MIN_VALUE <= n && n <= java.lang.Integer.MAX_VALUE) {
            return valueOf((int) n);
        } else {
            return newInteger(n);
        }
    }
    
    @Override
    public Float approxValueOf(double n) {
        return newFloat(n, true);
    }
    
    @Override
    public Float exactValueOf(double n) {
        return newFloat(n, false);
    }
    
    @Override
    public Integer one() {
        return one;
    }
    
    @Override
    public Integer zero() {
        return zero;
    }
    
    @Override
    public Integer negativeOne() {
        return negativeOne;
    }
    
    @Override
    public Fraction oneOverOne() {
        return oneOverOne;
    }
    
    @Override
    public Fraction zeroOverOne() {
        return zeroOverOne;
    }
    
    @Override
    public Comparator<Integer> comparingIntegers() {
        return comparingIntegers;
    }
    
    protected abstract Primes createPrimes();
    
    @Override
    public Primes primes() {
        return primes;
    }
    
    protected static abstract class BinaryOverloads {
        private final String name;
        
        protected BinaryOverloads(String name) {
            this.name = name;
        }
        
        protected abstract Number apply(Boolean  lhs, Boolean  rhs, MathEnv env);
        protected abstract Number apply(Integer  lhs, Integer  rhs, MathEnv env);
        protected abstract Number apply(Fraction lhs, Fraction rhs, MathEnv env);
        protected abstract Number apply(Float    lhs, Float    rhs, MathEnv env);
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    protected Number promoteAndApply(Number lhs, Number rhs, BinaryOverloads op) {
        lhs = lhs.promoteTo(rhs, this);
        rhs = rhs.promoteTo(lhs, this);
        
        try {
            switch (lhs.getNumberKind()) {
                case BOOLEAN:
                    return op.apply((Boolean) lhs, (Boolean) rhs, this);
                case INTEGER:
                    return op.apply((Integer) lhs, (Integer) rhs, this);
                case FRACTION:
                    return op.apply((Fraction) lhs, (Fraction) rhs, this);
                case FLOAT:
                    return op.apply((Float) lhs, (Float) rhs, this);
            }
        } catch (ClassCastException x) {
            Log.caught(AbstractMathEnv.class, "In promoteAndApply.", x, false);
        }
        throw UnsupportedException.operandTypes(lhs, rhs, op.toString());
    }
    
    protected static final BinaryOverloads RELATE_OVERLOADS = new BinaryOverloads("relation") {
        @Override
        protected Integer apply(Boolean lhs, Boolean rhs, MathEnv env) {
            return env.valueOf(lhs.relateTo(rhs, env));
        }
        @Override
        protected Integer apply(Integer lhs, Integer rhs, MathEnv env) {
            return env.valueOf(lhs.relateTo(rhs, env));
        }
        @Override
        protected Integer apply(Fraction lhs, Fraction rhs, MathEnv env) {
            return env.valueOf(lhs.relateTo(rhs, env));
        }
        @Override
        protected Integer apply(Float lhs, Float rhs, MathEnv env) {
            return env.valueOf(lhs.relateTo(rhs, env));
        }
    };
    
    @Override
    public int relate(Number lhs, Number rhs) {
        Number relation = promoteAndApply(lhs, rhs, RELATE_OVERLOADS);
        return ((Integer) relation).toPrimitiveIntExact(this);
    }
    
    protected static final BinaryOverloads ADD_OVERLOADS = new BinaryOverloads("addition") {
        @Override
        protected Number apply(Boolean lhs, Boolean rhs, MathEnv env) {
            return lhs.add(rhs, env);
        }
        @Override
        protected Number apply(Integer lhs, Integer rhs, MathEnv env) {
            return lhs.add(rhs, env);
        }
        @Override
        protected Number apply(Fraction lhs, Fraction rhs, MathEnv env) {
            return lhs.add(rhs, env);
        }
        @Override
        protected Number apply(Float lhs, Float rhs, MathEnv env) {
            return lhs.add(rhs, env);
        }
    };
    
    @Override
    public Number add(Number lhs, Number rhs) {
        return promoteAndApply(lhs, rhs, ADD_OVERLOADS);
    }
    
    @Override
    public BinaryOperator<Number> add() {
        return addOp;
    }
    
    protected static final BinaryOverloads SUBTRACT_OVERLOADS = new BinaryOverloads("subtraction") {
        @Override
        protected Number apply(Boolean lhs, Boolean rhs, MathEnv env) {
            return lhs.subtract(rhs, env);
        }
        @Override
        protected Number apply(Integer lhs, Integer rhs, MathEnv env) {
            return lhs.subtract(rhs, env);
        }
        @Override
        protected Number apply(Fraction lhs, Fraction rhs, MathEnv env) {
            return lhs.subtract(rhs, env);
        }
        @Override
        protected Number apply(Float lhs, Float rhs, MathEnv env) {
            return lhs.subtract(rhs, env);
        }
    };
    
    @Override
    public Number subtract(Number lhs, Number rhs) {
        return promoteAndApply(lhs, rhs, SUBTRACT_OVERLOADS);
    }
    
    @Override
    public BinaryOperator<Number> subtract() {
        return subtractOp;
    }
    
    protected static final BinaryOverloads MULTIPLY_OVERLOADS = new BinaryOverloads("multiplication") {
        @Override
        protected Number apply(Boolean lhs, Boolean rhs, MathEnv env) {
            return lhs.multiply(rhs, env);
        }
        @Override
        protected Number apply(Integer lhs, Integer rhs, MathEnv env) {
            return lhs.multiply(rhs, env);
        }
        @Override
        protected Number apply(Fraction lhs, Fraction rhs, MathEnv env) {
            return lhs.multiply(rhs, env);
        }
        @Override
        protected Number apply(Float lhs, Float rhs, MathEnv env) {
            return lhs.multiply(rhs, env);
        }
    };
    
    @Override
    public Number multiply(Number lhs, Number rhs) {
        return promoteAndApply(lhs, rhs, MULTIPLY_OVERLOADS);
    }
    
    @Override
    public BinaryOperator<Number> multiply() {
        return multiplyOp;
    }
    
    protected static final BinaryOverloads DIVIDE_OVERLOADS = new BinaryOverloads("division") {
        @Override
        protected Number apply(Boolean lhs, Boolean rhs, MathEnv env) {
            return lhs.divide(rhs, env);
        }
        @Override
        protected Number apply(Integer lhs, Integer rhs, MathEnv env) {
            return lhs.divide(rhs, env);
        }
        @Override
        protected Number apply(Fraction lhs, Fraction rhs, MathEnv env) {
            return lhs.divide(rhs, env);
        }
        @Override
        protected Number apply(Float lhs, Float rhs, MathEnv env) {
            return lhs.divide(rhs, env);
        }
    };
    
    @Override
    public Number divide(Number lhs, Number rhs) {
        return promoteAndApply(lhs, rhs, DIVIDE_OVERLOADS);
    }
    
    @Override
    public BinaryOperator<Number> divide() {
        return divideOp;
    }
    
    protected static final BinaryOverloads MOD_OVERLOADS = new BinaryOverloads("remainder") {
        @Override
        protected Number apply(Boolean lhs, Boolean rhs, MathEnv env) {
            return lhs.mod(rhs, env);
        }
        @Override
        protected Number apply(Integer lhs, Integer rhs, MathEnv env) {
            return lhs.mod(rhs, env);
        }
        @Override
        protected Number apply(Fraction lhs, Fraction rhs, MathEnv env) {
            return lhs.mod(rhs, env);
        }
        @Override
        protected Number apply(Float lhs, Float rhs, MathEnv env) {
            return lhs.mod(rhs, env);
        }
    };
    
    @Override
    public Number mod(Number lhs, Number rhs) {
        return promoteAndApply(lhs, rhs, MOD_OVERLOADS);
    }
    
    @Override
    public BinaryOperator<Number> mod() {
        return modOp;
    }
}