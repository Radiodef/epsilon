/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.util.*;
import eps.app.*;
import eps.eval.*;
import static eps.math.Math2.*;
import static eps.util.Misc.*;

import java.util.*;
import java.math.*;

/**
 * This should possibly be refactored to somewhere more public.
 * <p>
 * For example, the obvious design is that Decimal should be
 * <code>interface Decimal {...}</code> which the Builder also implements,
 * and the <code>valueOf(...)</code> factories would return a private
 * implementation class. The issue with that is that it's somewhat problematic
 * WRT mutability. A method accepting the interface could receive either
 * an immutable Decimal or a mutable Builder.
 */
interface DecimalCommon {
    int getDigit(int column);
    int width();
    int scale();
    int signum();
    boolean isNegative();
    boolean isApproximate();
    
    default int minColumn() {
        return Math.min(0, scale());
//        return scale();
    }
    
    default int maxColumn() {
        int width = width();
        int scale = scale();
        if (width == 0) {
            return Math.max(-1, minColumn());
        }
        return Math.max(-1, scale + width - 1);
//        return (scale() + width()) - 1;
    }
    
    default Decimal.DigitIterator digits() {
        return new Decimal.DigitIterator(this);
    }
}

/**
 * Decimal is essentially just a tool for converting Numbers to Strings,
 * although it could be adapted to implement Number itself if there was
 * a good reason to do so.
 * <p>
 * Decimal performs conversions of fractions to floating-point decimal
 * values while keeping track of whether the result is an approximation
 * or not. For example, {@code valueOf(1, 4)} would create an exact Decimal
 * {@code 0.25}, while {@code valueOf(1, 3)} would create an approximate
 * Decimal {@code 0.3333333333...}.
 * 
 * @see     Decimal.Builder
 * @author  David
 */
// TODO:
//  More important:
//  * Work on good behavior for number of fractional digits.
//  * Detect repeating decimal places?
//  Less important:
//  * Implement division and subtraction for non-integer Decimals.
//  * Should Decimal implement eps.math.Number? (probably not!!!)
//  * Consider storing an int signum instead of boolean isNegative.
public final class Decimal implements DecimalCommon, Comparable<Decimal> {
    private final int[]   digits;
    private final int     width;
    private final int     scale;
    private final boolean isNegative;
    private final boolean isApproximate;
    
    private Decimal() {
        this(Misc.EMPTY_INT_ARRAY, 0, 0, false, false);
    }
    
    private Decimal(int[] digits, int width, int scale, boolean isNegative, boolean isApproximate) {
        this.digits        = digits;
        this.width         = width;
        this.scale         = scale;
        this.isNegative    = width != 0 && isNegative;
        this.isApproximate = isApproximate;
        assert assertions();
    }
    
    private static final ToString<Decimal> TO_DEBUG_STRING =
        ToString.builder(Decimal.class)
                .add("value",  Decimal::toString)
                .add("width",  Decimal::width)
                .add("scale",  Decimal::scale)
                .add("digits", decimal -> toDigitString(decimal.digits))
                .build();
    
    public String toDebugString() {
//        return f("width = %d, scale = %d, digits = %s", width, scale, toDigitString(digits));
        return TO_DEBUG_STRING.apply(this);
    }
    
    private AssertionError newAssertionError() {
        return new AssertionError(toDebugString());
    }
    
    private boolean assertions() {
        int[] digits = this.digits;
        int   width  = this.width;
        int   scale  = this.scale;
        // Negative number of digits...what could that possibly mean?
        if (width < 0) {
            throw newAssertionError();
        }
        // We should never have extra space in the array.
        if (digits.length != ceilDiv(width, DIGITS_PER_ELEMENT)) {
            throw newAssertionError();
        }
        // This is just here because of Math.abs(scale).
        if (scale == java.lang.Integer.MIN_VALUE) {
            throw newAssertionError();
        }
        // We should never have too few digits
        // e.g. .XX1 where scale = -3 and digits = {0x1000_0000}.
        if (Math.abs(scale) > width) {
            throw newAssertionError();
        }
        // Or e.g. 1XX. where scale = 2 and digits = {0x1000_0000}.
        if (scale > 0) {
            throw newAssertionError();
        }
        // We should never have non-fractional leading zeroes e.g. 0500.
        if ((width + scale) > 0 && Builder.countLeadingZeroes(digits, width) != 0) {
            throw newAssertionError();
        }
        // We should never have fractional trailing zeroes e.g. 0.123000.
        if (scale < 0 && Builder.countTrailingZeroes(digits, width, scale) != 0) {
            throw newAssertionError();
        }
        // No -0
        if (isZero() && isNegative()) {
            throw newAssertionError();
        }
        return true;
    }
    
    private static final int DIGIT_SIZE = 4;
    private static final int DIGITS_PER_ELEMENT = java.lang.Integer.SIZE / DIGIT_SIZE;
    
    private static int[] newDigitsArray(int width) {
        return new int[ceilDiv(width, DIGITS_PER_ELEMENT)];
    }
    
    // 0
    public static final Decimal ZERO = new Decimal();
    // 0.0
    public static final Decimal APPROX_ZERO = ZERO.setApproximate(true);
    // 1
    public static final Decimal ONE = new Decimal(new int[] {0x1000_0000}, 1, 0, false, false);
    // 1.0
    public static final Decimal APPROX_ONE = ONE.setApproximate(true);
    // -2147_4836_48xx_xxxx
    private static final Decimal INTEGER_MIN_VALUE = new Decimal(new int[] {0x2147_4836, 0x4800_0000}, 10, 0, true, false);
    // -9223_3720_3685_4775_808x_xxxx
    private static final Decimal LONG_MIN_VALUE = new Decimal(new int[] {0x9223_3720, 0x3685_4775, 0x8080_0000}, 19, 0, true, false);
    
    public static Decimal valueOf(long n) {
        if (n == 0) {
            return ZERO;
        }
        
        boolean isNegative = n < 0;
        if (isNegative) {
            if (n == Long.MIN_VALUE) {
                // Avoid overflow.
                return LONG_MIN_VALUE;
            } // else
            n = -n;
        }
        
        int   width  = 1 + floorLog10(n);
        int   scale  = 0;
        int[] digits = newDigitsArray(width);
        
        for (int column = 0; n != 0; ++column, n /= 10) {
            int digit = (int) (n % 10);
            
            int index = columnToIndex(column, width, scale);
            int group = groupIndexOf(index);
            int shift = digitShiftOf(index);
            
            digits[group] |= digit << shift;
        }
        
        return new Decimal(digits, width, scale, isNegative, false);
    }
    
    public static Builder builderOf(long n) {
        Builder b = builder(1 + floorLog10(n));
        if (n == Long.MIN_VALUE) {
            return b.copy(LONG_MIN_VALUE, LONG_MIN_VALUE.digits);
        }
        boolean isNegative = n < 0;
        if (isNegative) {
            n = -n;
        }
        
        while (n != 0) {
            b.append((int) (n % 10));
            n /= 10;
        }
        
        return b.reverse().makeExact();
    }
    
    public static Decimal valueOf(long numerator, long denominator) {
        if (denominator == 0) {
            throw UndefinedException.divisionByZero(numerator, denominator);
        }
        if (numerator == 0) {
            return ZERO;
        }
        if (numerator == denominator) {
            return ONE;
        }
        // (Avoid pesky overflow issues.)
        if (numerator == Long.MIN_VALUE || denominator == Long.MIN_VALUE) {
            return valueOf(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
        }
        
        int sign = ((numerator < 0) == (denominator < 0)) ? +1 : -1;
        
        Builder dividend = builderOf(Math.abs(numerator));
        Builder divisor  = builderOf(Math.abs(denominator));
        
        return divide(dividend, divisor).setNegative(sign < 0).buildWithNoArrayCopy();
    }
    
    public static Decimal valueOf(BigInteger n) {
        Objects.requireNonNull(n, "n");
        
        int bitLength = n.bitLength();
        if (bitLength < Long.SIZE) {
            return valueOf(n.longValueExact());
        }
        
        return builderOf(n).buildWithNoArrayCopy();
    }
    
    public static Builder builderOf(BigInteger n) {
        Objects.requireNonNull(n, "n");
        
        if (n.equals(BigInteger.ZERO)) {
            Log.low(Decimal.class, "builderOf(BigInteger)", "returning zero");
            return builder().makeExact();
        }
        
        boolean isNegative = n.signum() < 0;
        if (isNegative) {
            n = n.negate();
        }
        
        String s = n.toString(10);
        
        int length = s.length();
        int width  = s.codePointCount(0, length);
        
        Builder b = new Builder(width);
        
        // TODO: don't use Strings?
        for (int i = 0, c; i < length; i += Character.charCount(c)) {
            c = s.codePointAt(i);
            b.append(Character.digit(c, 10));
        }
        
        return b.setNegative(isNegative).makeExact();
    }
    
    public static Decimal valueOf(BigInteger numerator, BigInteger denominator) {
        Objects.requireNonNull(numerator,   "numerator");
        Objects.requireNonNull(denominator, "denominator");
        
        if (denominator.equals(BigInteger.ZERO)) {
            throw UndefinedException.divisionByZero(numerator, denominator);
        }
        if (numerator.equals(BigInteger.ZERO)) {
            return ZERO;
        }
        
        int signN = numerator.signum();
        if (signN < 0) {
            numerator = numerator.negate();
        }
        int signD = denominator.signum();
        if (signD < 0) {
            denominator = denominator.negate();
        }
        
        Builder dividend = builderOf(numerator);
        Builder divisor  = builderOf(denominator);
        
        return divide(dividend, divisor).setNegative(signN * signD < 0).buildWithNoArrayCopy();
    }
    
    public static Decimal valueOf(double n) {
        UndefinedException.requireReal(n);
        
        return valueOf(BigDecimal.valueOf(n));
    }
    
    public static Decimal valueOf(BigDecimal n) {
        Objects.requireNonNull(n, "n");
        
        BigInteger unscaled = n.unscaledValue();
        int        scale    = -n.scale();
        
        Builder b = builderOf(unscaled);
        
        Log.note(Decimal.class, "valueOf", "unscaled = ", unscaled, ", scale = ", scale);
        if (scale > 0) {
            b.append(0, scale);
            
        } else if (scale < 0) {
            b.setScale(scale);
        }
        
        return b.makeApproximate().buildWithNoArrayCopy();
    }
    
    @Override
    public boolean isNegative() {
        return isNegative;
    }
    
    public boolean isZero() {
        return width == 0;
    }
    
    public boolean isPositive() {
        return !(isNegative() || isZero());
    }
    
    @Override
    public int signum() {
        return isNegative() ? -1 : isZero() ? 0 : +1;
    }
    
    @Override
    public boolean isApproximate() {
        return isApproximate;
    }
    
    public Decimal setApproximate(boolean isApproximate) {
        if (isApproximate == this.isApproximate) {
            return this;
        }
        return new Decimal(digits, width, scale, isNegative, isApproximate);
    }
    
    public Decimal negate() {
        if (isZero()) {
            return this;
        }
        return new Decimal(digits, width, scale, !isNegative, isApproximate);
    }
    
    public boolean isInteger() {
        return (scale == 0);
    }
    
    @Override
    public int width() {
        return width;
    }
    
    @Override
    public int scale() {
        return scale;
    }
    
    private static int columnToIndex(int column, int width, int scale) {
        int normal  = column - scale;
        int reverse = (width - 1) - normal;
        return reverse;
    }
    
    private static int groupIndexOf(int index) {
        return index >> 3;
    }
    
    private static int digitShiftOf(int index) {
        return (7 - (index & 0b111)) << 2;
    }
    
    private static int digitAt(int column, int[] digits, int width) {
        return digitAt(column, digits, width, 0);
    }
    
    private static int digitAt(int column, int[] digits, int width, int scale) {
        int min = scale;
        int max = (width + scale) - 1;
        
        if (inRangeClosed(column, min, max)) {
            int index = columnToIndex(column, width, scale);
            assert inRange(index, 0, width) : index;
            
            int group = digits[groupIndexOf(index)];
            int digit = (group >>> digitShiftOf(index)) & 0xF;
            assert inRangeClosed(digit, 0, 9) : digit;
            
            return digit;
        } // else
        
        // Throwing an exception IMO makes more sense but this return 0
        // here makes the DigitIterator work more easily with the Builder.
        return 0;
//        throw Errors.newIndexOutOfBounds("digit column", column, min, max);
    }
    
    @Override
    public int getDigit(int column) {
        return digitAt(column, digits, width, scale);
    }
    
    private static int compare(DecimalCommon decimalL, DecimalCommon decimalR) {
        int sign = decimalL.signum();
        int comp = java.lang.Integer.compare(sign, decimalR.signum());
        if (comp != 0) {
            return comp;
        }
        if (sign == 0) {
            return 0;
        }
        
        int widthL = decimalL.width();
        int widthR = decimalR.width();
        int scaleL = decimalL.scale();
        int scaleR = decimalR.scale();
        
        int wholeL = widthL + scaleL;
        int wholeR = widthR + scaleR;
        
        int colL = decimalL.maxColumn();
        int colR = decimalR.maxColumn();
        
        // Skip leading zeroes
        for (; colL >= 0; --colL, --wholeL)
            if (decimalL.getDigit(colL) != 0)
                break;
        for (; colR >= 0; --colR, --wholeR)
            if (decimalR.getDigit(colR) != 0)
                break;
        
        if (wholeL < wholeR) return sign * -1;
        if (wholeL > wholeR) return sign * +1;
        
        int minL = decimalL.minColumn();
        int minR = decimalR.minColumn();
        for (;;) {
            if (colL < minL) {
                return (colR < minR) ? 0 : sign * -1;
            }
            if (colR < minR) {
                return sign * +1;
            }
            comp = java.lang.Integer.compare(decimalL.getDigit(colL--), decimalR.getDigit(colR--));
            if (comp != 0) {
                return sign * comp;
            }
        }
    }
    
//    private static int compare(DecimalCommon decimalL, DecimalCommon decimalR) {
//        int comp = java.lang.Integer.compare(decimalL.signum(), decimalR.signum());
//        if (comp != 0) {
//            return comp;
//        }
//        
//        int widthL = decimalL.width();
//        int widthR = decimalR.width();
//        int scaleL = decimalL.scale();
//        int scaleR = decimalR.scale();
//        
//        int wholeL = widthL + scaleL;
//        int wholeR = widthR + scaleR;
//        
//        int colL = decimalL.maxColumn();
//        int colR = decimalR.maxColumn();
//        
//        // Skip leading zeroes
//        for (; colL >= 0; --colL, --wholeL)
//            if (decimalL.getDigit(colL) != 0)
//                break;
//        for (; colR >= 0; --colR, --wholeR)
//            if (decimalR.getDigit(colR) != 0)
//                break;
//        
//        if (wholeL < wholeR) return -1;
//        if (wholeL > wholeR) return +1;
//        
//        int minL = decimalL.minColumn();
//        int minR = decimalR.minColumn();
//        for (;;) {
//            if (colL < minL) {
//                return (colR < minR) ? 0 : -1;
//            }
//            if (colR < minR) {
//                return +1;
//            }
//            comp = java.lang.Integer.compare(decimalL.getDigit(colL--), decimalR.getDigit(colR--));
//            if (comp != 0) {
//                return comp;
//            }
//        }
//    }
    
    // Note: This method does not currently consider cases where either
    //       argument has extraneous leading or trailing zeroes. The
    //       method should therefore be used carefully when comparing
    //       Builders.
    //       (Decimal instances should never have extraneous zeroes.)
//    private static int compare_old(DecimalCommon decimalL, DecimalCommon decimalR) {
//        int comp = java.lang.Integer.compare(decimalL.signum(), decimalR.signum());
//        if (comp != 0) {
//            return comp;
//        }
//        
//        int widthL = decimalL.width();
//        int widthR = decimalR.width();
//        int scaleL = decimalL.scale();
//        int scaleR = decimalR.scale();
//        
//        int wholeL = widthL + scaleL;
//        int wholeR = widthR + scaleR;
//        if (wholeL < wholeR) return -1;
//        if (wholeL > wholeR) return +1;
//        
//        int colL = decimalL.maxColumn();
//        int colR = decimalR.maxColumn();
//        int minL = decimalL.minColumn();
//        int minR = decimalR.minColumn();
//        for (;;) {
//            if (colL < minL) {
//                return (colR < minR) ? 0 : -1;
//            }
//            if (colR < minR) {
//                return +1;
//            }
//            comp = java.lang.Integer.compare(decimalL.getDigit(colL--), decimalR.getDigit(colR--));
//            if (comp != 0) {
//                return comp;
//            }
//        }
//    }
    
    @Override
    public int compareTo(Decimal that) {
        return compare(this, that);
    }
    
    @Override
    public int hashCode() {
        int hash = Arrays.hashCode(digits);
        hash = 17 * hash + width;
        hash = 17 * hash + scale;
        return isNegative ? ~hash : hash; // Hash isApproximate too? Probably no reason.
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Decimal) {
            Decimal that = (Decimal) obj;
            if (this.width != that.width)
                return false;
            if (this.scale != that.scale)
                return false;
            if (this.isNegative != that.isNegative)
                return false;
            if (this.isApproximate != that.isApproximate)
                return false;
            return Arrays.equals(this.digits, that.digits);
        }
        return false;
    }
    
    private static <T> T getSetting(Context local, Setting<T> key) {
        if (local != null) {
            return local.getSetting(key);
        } else {
            return Settings.getOrDefault(key);
        }
    }
    
    public static final String ELLIPSE = "...";
    
    @Override
    public String toString() {
        Context context = Context.local();
        int decimalMark = getSetting(context, Setting.DECIMAL_MARK).intValue();
        int maxDigits   = getSetting(context, Setting.MAX_FRACTION_DIGITS);
        return toString(decimalMark, maxDigits, ELLIPSE);
    }
    
    private String toString(int decimalMark, int maxDigits, String ellipse) {
        // TODO: preconditions if this method should be public
        
        if (isZero()) {
            if (isApproximate()) {
                if (decimalMark == '.') {
                    return "0.0";
                }
                return new StringBuilder(3).append('0')
                                           .appendCodePoint(decimalMark)
                                           .append('0').toString();
            }
            return "0";
        }
        
        int width = this.width;
        StringBuilder b = new StringBuilder(5 + width);
        if (isNegative()) {
            b.append('-');
        }
        
        if (this.scale == -width) {
            // (All the digits are fractional, so append a leading 0.)
            b.append('0');
        }
        
        int fracDigits = 0;
        
        for (DigitIterator it = digits(); it.hasNext();) {
            if (it.isTenthsColumn()) {
                if (maxDigits <= 0)
                    break;
                b.appendCodePoint(decimalMark);
                ++fracDigits;
            } else if (fracDigits != 0) {
                if (fracDigits >= maxDigits)
                    break;
                ++fracDigits;
            }
            
            b.append(it.nextInt());
        }
        
        if (isApproximate()) {
            if (isInteger()) {
                b.appendCodePoint(decimalMark).append('0');
            } else {
                b.append(ellipse);
            }
        }
        
        return b.toString();
    }
    
    private static String toDigitString(int[] digits) {
        return Arrays.stream(digits)
                     .mapToObj(Misc::toHexLiteral)
                     .collect(java.util.stream.Collectors.joining(", ", "[", "]"));
    }
    
    /**
     * Iterator over the digits of a Decimal. Iteration order is left-to-right,
     * or in other words highest order to lowest order. If the decimal is e.g.
     * 2012, then the iterator returns the digits in the order: 2, 0, 1, 2.
     */
    public static final class DigitIterator implements PrimitiveIterator.OfInt {
        private final DecimalCommon self;
        
        private final int minColumn;
        private       int column;
        
        DigitIterator(DecimalCommon self) {
            this.self      = Objects.requireNonNull(self, "self");
            this.minColumn = self.minColumn();
            this.column    = self.maxColumn();
        }
        
        @Override
        public boolean hasNext() {
            return column >= minColumn;
        }
        
        @Override
        public int nextInt() {
            if (hasNext()) {
                return self.getDigit(column--);
            } else {
                throw Errors.newNoSuchElement(column);
            }
        }
        
        /**
         * Returns {@code true} if the digit returned by the next invocation
         * of {@code nextInt()} would return the digit in the tenths column
         * and {@code false} otherwise.
         * <p>
         * In other words, for the number 3.14, this method would return
         * {@code true} when the next digit is the 1.
         * <p>
         * See the source code for {@link Decimal#toString()} for an example
         * usage.
         * 
         * @return whether or not the next digit is in the tenths column.
         */
        public boolean isTenthsColumn() {
            return column == -1;
        }
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static Builder builder(int expectedWidth) {
        return new Builder(expectedWidth);
    }
    
    /**
     * <p>Builder appends digits from left-to-right.</p>
     * 
     * <p>For example, the following builds the number "3.14159...":</p>
     * 
     *<pre><code>Decimal.builder()
     *    .append(3) // Note: could also use appendAll(3, 1, 4, 1, 5, 9)
     *    .append(1)
     *    .append(4)
     *    .append(1)
     *    .append(5)
     *    .append(9)
     *    .setScale(-5)
     *    .makeApproximate()
     *    .build();
     *</code></pre>
     * 
     * <p>To build a number from right-to-left, build the number backwards and
     * then call {@link Decimal.Builder#reverse()}. (See {@link Decimal#builderOf(long)}
     * for an example of this.)</p>
     */
    public static final class Builder implements DecimalCommon, Cloneable {
        private int[] digits = Misc.EMPTY_INT_ARRAY;
        private int   width  = 0;
        private int   scale  = 0;
        
        private boolean isNegative    = false;
        private boolean isApproximate = false;
        
        private Builder() {}
        
        private Builder(int width) {
            this.digits = newDigitsArray(width);
        }
        
        private Builder(int[] digits, int width) {
            this.digits = digits;
            this.width  = width;
            assert width >= 0 : width;
        }
        
        @Override
        public int width() {
            return width;
        }
        
        @Override
        public int scale() {
            return scale;
        }
        
        @Override
        public boolean isNegative() {
            return isNegative;
        }
        
        @Override
        public boolean isApproximate() {
            return isApproximate;
        }
        
        @Override
        public int signum() {
            int width = this.width;
            if (width == 0) {
                return 0;
            }
            int[] digits = this.digits;
            for (int i = 0; i < width; ++i) {
                if (Decimal.digitAt(i, digits, width) != 0)
                    return isNegative ? -1 : +1;
            }
            return 0;
        }
        
        public Builder copy(Builder that) {
            Objects.requireNonNull(that, "that");
            return copy(that, that.digits);
        }
        
        private Builder copy(DecimalCommon that, int[] that_digits) {
            int[] this_digits = this.digits;
            int   this_length = this_digits.length;
            int   that_length = that_digits.length;
            
            if (this_length >= that_length) {
                System.arraycopy(that_digits, 0, this_digits, 0, that_length);
                Arrays.fill(this_digits, that_length, this_length, 0);
            } else {
                this.digits = that_digits.clone();
            }
            
            this.width         = that.width();
            this.scale         = that.scale();
            this.isNegative    = that.isNegative();
            this.isApproximate = that.isApproximate();
            return this;
        }
        
        public Builder trim() {
            Decimal d = buildWithNoArrayCopy();
            return copy(d, d.digits);
        }
        
        public Builder resetToZero() {
//            int width = this.width;
            this.width         = 0;
            this.scale         = 0;
            this.isNegative    = false;
            this.isApproximate = false;
//            if (width > 0) {
                Arrays.fill(digits, 0);
//            }
            return this;
        }
        
        public Builder reverse() {
            reverse(digits, width);
            return this;
        }
        
        private static void reverse(int[] digits, int width) {
            int half = width >> 1;
            
            int columnA, columnB,
                 indexA,  indexB,
                 groupA,  groupB,
                 shiftA,  shiftB,
                digitsA, digitsB,
                 digitA,  digitB;
            
            for (int i = 0; i < half; ++i) {
                columnA = i;
                columnB = (width - 1) - i;
                indexA  = columnToIndex(columnA, width, 0);
                indexB  = columnToIndex(columnB, width, 0);
                groupA  = groupIndexOf(indexA);
                groupB  = groupIndexOf(indexB);
                shiftA  = digitShiftOf(indexA);
                shiftB  = digitShiftOf(indexB);
                
                digitsA = digits[groupA];
                digitsB = digits[groupB];
                
                digitA = (digitsA >>> shiftA) & 0xF;
                digitB = (digitsB >>> shiftB) & 0xF;
                
                if (groupA == groupB) {
                    digitsA ^= (digitA << shiftA) | (digitB << shiftB);
                    digits[groupA] = digitsA | (digitA << shiftB) | (digitB << shiftA);
                } else {
                    digits[groupA] = (digitsA ^ (digitA << shiftA)) | (digitB << shiftA);
                    digits[groupB] = (digitsB ^ (digitB << shiftB)) | (digitA << shiftB);
                }
            }
        }
        
        public Builder appendAll(int... digits) {
            if (digits != null) {
                for (int digit : digits) {
                    append(digit);
                }
            }
            return this;
        }
        
        public Builder appendAll(Builder digitsToCopy) {
            int min = digitsToCopy.minColumn();
            
            for (int col = digitsToCopy.maxColumn(); col >= min; --col) {
                append(digitsToCopy.getDigit(col));
            }
            
            return this;
        }
        
        public Builder appendAll(Decimal digitsToCopy) {
            for (DigitIterator it = digitsToCopy.digits(); it.hasNext();) {
                append(it.nextInt());
            }
            return this;
        }
        
        public Builder append(int digit, int count) {
            while (count > 0) {
                append(digit);
                --count;
            }
            return this;
        }
        
        // note: do not depend on scale in this method!
        //       elsewhere depends on this fact.
        public Builder append(int digit) {
            if (!inRangeClosed(digit, 0, 9)) {
                throw Errors.newIllegalArgument("digit", digit);
            }
            
            int   width  = this.width + 1;
            int[] digits = this.digits;
            int   length = digits.length;
            
            if (ceilDiv(width, DIGITS_PER_ELEMENT) > length) {
                this.digits = digits = Arrays.copyOf(digits, length + 1);
            }
            
//            int index = columnToIndex(minColumn(), width, scale);
            int index = columnToIndex(0, width, 0);
            int group = groupIndexOf(index);
            int shift = digitShiftOf(index);
            
            digits[group] |= digit << shift;
            
            this.width = width;
            return this;
        }
        
        public Builder delete(int column) {
            if (!inRangeClosed(column, minColumn(), maxColumn())) {
                throw Errors.newIndexOutOfBoundsClosed("column", column, minColumn(), maxColumn());
            }
            
            int[] digits = this.digits;
            int   width  = this.width;
            int   scale  = this.scale;
            
            if (scale < 0) {
                // happens if we have something like
                // Builder b = builder();
                // b.append(1)
                //  .setScale(-2) // 0.01 at this point,
                //  .delete(-1);  //   ^ and attempt to delete 0 which is OOB
                if (column >= (width + scale)) {
                    // do nothing
                    return this;
                }
            } else if (scale > 0) {
                // happens if we have something like
                // Builder b = builder();
                // b.append(1)
                //  .setScale(2) // 100 at this point,
                //  .delete(0);  //   ^ and attempt to delete 0 which is OOB
                if (column < scale) {
                    return setScale(scale - 1);
                }
            }
            // happens (in general) if we have something like
            // builder().delete(0), in other words attempting to
            // delete the 0 from a 0.
            if (width == 0) {
                // do nothing
                return this;
            }
            
            int delIndex = columnToIndex(column, width, scale);
            int delGroup = groupIndexOf(delIndex);
            int delShift = digitShiftOf(delIndex);
            
            int group = digits[delGroup];
            int upper = group & lshift32(0xFFFF_FFFF, delShift + DIGIT_SIZE);
            int lower = group & ~(0xFFFF_FFFF << delShift);
            digits[delGroup] = upper | (lower << DIGIT_SIZE);
            
            int next = ((delIndex + DIGITS_PER_ELEMENT) / DIGITS_PER_ELEMENT) * DIGITS_PER_ELEMENT;
            
            for (; next < width; next += DIGITS_PER_ELEMENT) {
                int nextGroup = groupIndexOf(next);
                
                group = digits[nextGroup];
                digits[nextGroup - 1] |= group >>> (7 * DIGIT_SIZE);
                digits[nextGroup] = (group << DIGIT_SIZE);
            }
            
            this.width = width - 1;
            return this;
        }
        
        @Override
        public int getDigit(int column) {
            return Decimal.digitAt(column, digits, width, scale);
        }
        
        public Builder setDigit(int column, int digit) {
            if (inRangeClosed(column, minColumn(), maxColumn())) {
                int width = this.width;
                int scale = this.scale;
                
                if (scale > 0) {
                    int dif = scale - column;
                    if (dif > 0) {
                        append(0, dif);
                        setScale(scale - dif);
                        width = this.width;
                        scale = this.scale;
                    }
                } else if (scale < 0) {
                    int lead = scale + width;
                    if (lead < 0) {
                        int dif = column - lead + 1;
                        if (dif > 0) {
                            reverse(this.digits, width);
                            append(0, dif);
                            width = this.width;
                            reverse(this.digits, width);
                        }
                    }
                } else {
                    if (width == 0) {
                        return append(digit);
                    }
                }
                
                int index = columnToIndex(column, width, scale);
                int group = groupIndexOf(index);
                int shift = digitShiftOf(index);
                
                int[]  digits = this.digits;
                digits[group] = (digits[group] & ~(0xF << shift)) | (digit << shift);
                return this;
            }
            throw Errors.newIndexOutOfBoundsClosed("column", column, minColumn(), maxColumn());
        }
        
        public Builder incrementScale() {
            return setScale(scale + 1);
        }
        
        public Builder decrementScale() {
            return setScale(scale - 1);
        }
        
        public Builder setScale(int scale) {
            this.scale = scale;
            return this;
        }
        
        public Builder setWidth(int width) {
            if (width < 0) {
                throw new IllegalArgumentException("width = " + width);
            }
            
            int[] digits   = this.digits;
            int   oldWidth = this.width;
            
            if (width == 0) {
                if (width != oldWidth) {
                    Arrays.fill(digits, 0);
                }
            } else {
                if (oldWidth < width) {
                    int requiredLength = ceilDiv(width, DIGITS_PER_ELEMENT);
                    
                    if (digits.length < requiredLength) {
                        this.digits = Arrays.copyOf(digits, requiredLength);
                    }
                } else if (oldWidth > width) {
                    do {
                        int last  = columnToIndex(0, oldWidth, 0);
                        int group = groupIndexOf(last);
                        int shift = digitShiftOf(last);
                        
                        digits[group] &= ~(0xF << shift);
                    } while (--oldWidth > width);
                }
            }
            
            this.width = width;
            return this;
        }
        
        public Builder makeNegative() {
            return setNegative(true);
        }
        
        public Builder makePositive() {
            return setNegative(false);
        }
        
        public Builder setNegative(boolean isNegative) {
            this.isNegative = isNegative;
            return this;
        }
        
        public Builder makeApproximate() {
            return setApproximate(true);
        }
        
        public Builder makeExact() {
            return setApproximate(false);
        }
        
        public Builder setApproximate(boolean isApproximate) {
            this.isApproximate = isApproximate;
            return this;
        }
        
        public Decimal build() {
            return repairZeroesThenBuild(true);
        }
        
        private Decimal buildWithNoArrayCopy() {
            return repairZeroesThenBuild(false);
        }
        
        private static int countLeadingZeroes(int[] digits, int width) {
            int count = 0;
            for (int i = (width - 1); i >= 0; --i) {
                if (Decimal.digitAt(i, digits, width) != 0)
                    break;
                ++count;
            }
            return count;
        }
        
//        private static int countTrailingZeroes(int[] digits, int width) {
//            int count = 0;
//            for (int i = 0; i < width; ++i) {
//                if (Decimal.digitAt(i, digits, width) != 0)
//                    break;
//                ++count;
//            }
//            return count;
//        }
        
        private static int countTrailingZeroes(int[] digits, int width, int scale) {
            int count = 0;
            for (int i = scale; i < 0; ++i) {
                if (Decimal.digitAt(i, digits, width, scale) != 0)
                    break;
                ++count;
            }
            return count;
        }
        
        private Decimal repairZeroesThenBuild(boolean arrayCopy) {
            int   scale  = this.scale;
            int   width  = this.width;
            int[] digits = this.digits;
            
            if (width > 0) {
                if (scale >= 0) {
                    int leadingZeroes = countLeadingZeroes(digits, width);
                    int missingZeroes = scale;
                    
                    if (leadingZeroes > 0 || missingZeroes > 0) {
                        digits    = digits.clone();
                        arrayCopy = false;
                    }
                    
                    if (leadingZeroes > 0) {
                        reverse(digits, width);
                        width -= leadingZeroes;
                        reverse(digits, width);
                    }
                    
                    if (missingZeroes > 0) {
                        Builder temp = new Builder(digits, width).append(0, missingZeroes);
                        digits = temp.digits;
                        width  = temp.width;
                    }
                    
                    scale = 0;
                    
                } else if (scale < 0) {
//                    int trailingZeroes = countTrailingZeroes(digits, width);
                    int trailingZeroes = countTrailingZeroes(digits, width, scale);
                    
//                    Log.low(getClass(), "repairZeroesThenBuild", "trailingZeroes = ", trailingZeroes);
                    
                    if (trailingZeroes >= width) {
                        // this happens if e.g. scale was negative,
                        // abs(scale) was greater than the width
                        // and the digits were all 0s
                        width = 0;
                        scale = 0;
                    } else {
                        width -= trailingZeroes;
                        scale += trailingZeroes;
                    }
                    
                    int missingZeroes = -(width + scale);
                    
//                    Log.low(getClass(), "repairZeroesThenBuild", "missingZeroes = ", missingZeroes);
                    
                    if (missingZeroes > 0) {
                        digits    = digits.clone();
                        arrayCopy = false;
                        reverse(digits, width);
                        
                        Builder temp = new Builder(digits, width).append(0, missingZeroes);
                        digits = temp.digits;
                        width  = temp.width;
                        
                        reverse(digits, width);
                    } else {
                        int leadingZeroes = countLeadingZeroes(digits, width);
                        int wholePart     = width + scale;
                        int extraZeroes   = Math.min(leadingZeroes, wholePart);
                        
//                        Log.low(getClass(), "repairZeroesThenBuild", "extraZeroes = ", extraZeroes);
                        
                        if (extraZeroes > 0) {
                            digits    = digits.clone();
                            arrayCopy = false;
                            
                            reverse(digits, width);
                            width -= extraZeroes;
                            reverse(digits, width);
                        }
                    }
                }
            }
            
            if (width == 0) {
                scale = 0;
            }
            
            return build(digits, width, scale, isNegative, isApproximate, arrayCopy);
        }
        
        private static Decimal build(int[]   digits,
                                     int     width,
                                     int     scale,
                                     boolean isNegative,
                                     boolean isApproximate,
                                     boolean arrayCopy) {
            int length = digits.length;
            int min    = Math.min(length, ceilDiv(width, DIGITS_PER_ELEMENT));
            if (min != length) {
                digits = Arrays.copyOf(digits, min);
            } else if (arrayCopy) {
                digits = digits.clone();
            }
            
//            Log.low(Builder.class, "build", "digits = ", toDigitString(digits));
            return new Decimal(digits, width, scale, isNegative, isApproximate);
        }
        
        @Override
        public Builder clone() {
            try {
                Builder clone = (Builder) super.clone();
                clone.digits = clone.digits.clone();
                return clone;
            } catch (CloneNotSupportedException x) {
                throw new AssertionError(getClass().toString(), x);
            }
        }
        
        @Override
        public String toString() {
            int width = this.width;
//            if (width == 0) {
//                return "empty";
//            }
            
            StringBuilder b = new StringBuilder(5 + width);
            if (isNegative()) {
                b.append('-');
            }
            
//            if (scale == -width) {
                // (All the digits are fractional, so append a leading 0.)
//                b.append('0');
//            }
            
            boolean hadFraction = false;
            
            for (DigitIterator it = digits(); it.hasNext();) {
                if (it.isTenthsColumn()) {
                    hadFraction = true;
                    b.append('.');
                }
                
                b.append(it.nextInt());
            }
            
            if (isApproximate()) {
                b.append(hadFraction ? ELLIPSE : ".0");
            }
            
            return b.toString();
        }
    }
    
    // Note: Division may mutate the dividend. The actual result is
    //       returned in the quotient.
    
    private static Builder divide(Builder dividend, Builder divisor) {
        return divide(dividend, divisor, null, Settings.getMaxFractionDigits());
    }
    
    private static Builder divide(Builder dividend, Builder divisor, Builder quotient) {
        return divide(dividend, divisor, quotient, Settings.getMaxFractionDigits());
    }
    
    private static Builder divide(Builder dividend, Builder divisor, Builder quotient, int maxFractionDigits) {
        if (maxFractionDigits < 0) {
            throw Errors.newIllegalArgument("maxFractionDigits", maxFractionDigits);
        }
        int dividendSignum = dividend.signum();
        if (dividendSignum == 0) {
            return quotient.resetToZero();
        }
        int divisorSignum = divisor.signum();
        if (divisorSignum == 0) {
            throw UndefinedException.divisionByZero(dividend, divisor);
        }
        
        Log.low(Decimal.class, "divide", "dividing ", dividend, " by ", divisor);
        
        int quotientSignum = dividendSignum * divisorSignum;
        quotient = (quotient == null) ? builder() : quotient.resetToZero();
        
        Builder temp = builder(dividend.width());
        
        boolean isZero = true;
        
        for (int i = (dividend.width() - 1); i >= 0; --i) {
            temp.append(dividend.getDigit(i));
            
            int comparison = compare(divisor, temp);
//            Log.low(Decimal.class, "divide", "compare(", divisor, ", ", temp, ") = ", comparison);
            
            if (comparison <= 0) {
                int digit = 0;
                do {
                    subtract(temp, divisor);
                    ++digit;
                    comparison = compare(divisor, temp);
//                    Log.low(Decimal.class, "divide", "compare(", divisor, ", ", temp, ") = ", comparison);
                } while (comparison <= 0);
                
                assert digit > 0 : digit;
                quotient.append(digit);
                isZero = false;
                
            } else if ((maxFractionDigits == 0) || !isZero) {
                quotient.append(0);
            }
        }
        
        Log.low(Decimal.class, "divide", "quotient = ", quotient, ", remainder = ", temp);
        
        // temp is the remainder.
        if (temp.signum() != 0) {
            if (maxFractionDigits != 0) {
                int remainWidth = temp.width();
                // TODO: calculate this width precisely?
                int extraZeroes = maxFractionDigits; // - remainWidth;
                temp.append(0, extraZeroes);
                
                dividend.copy(temp);
                Builder fraction = divide(dividend, divisor, temp, 0);
                
                int fractionWidth = fraction.width();
                if (fractionWidth != 0) {
                    // delete the leading zeroes in columns > the tenths column
                    if (fractionWidth == 1) {
                        fractionWidth = fraction.delete(fractionWidth - 1).width();
                    } else {
                        fractionWidth = fraction.reverse().setWidth(fractionWidth - remainWidth).reverse().width();
                    }
                }
                
                Log.low(Decimal.class, "divide", "quotient = ", quotient, ", fraction = ", fraction);
                Log.low(Decimal.class, "divide", "quotient: width = ", quotient.width(), ", scale = ", quotient.scale());
                Log.low(Decimal.class, "divide", "fraction: width = ", fraction.width(), ", scale = ", fraction.scale());

                quotient.appendAll(fraction)
                        .setScale(-fractionWidth);
                
                if (fraction.isApproximate()) {
                    quotient.makeApproximate();
                }
            } else {
                quotient.makeApproximate();
            }
        }
        
        return quotient.setNegative(quotientSignum < 0);
    }
    
    // Note: Subtraction mutates the left-hand-side to be the result.
    //       (These are not intended to be called directly.)
    
    private static Builder subtract(Builder lhs, Builder rhs) {
        return subtract(lhs, rhs, null);
    }
    
    private static Builder subtract(Builder lhs, Builder rhs, Builder temp) {
        int signL = lhs.signum();
        int signR = rhs.signum();
        if (signL == 0) {
            return lhs.copy(rhs).setNegative(signR > 0);
        }
        if (signR == 0) {
            return lhs;
        }
        
        assert signL >= 0 && signR >= 0 : f("%s - %s", lhs, rhs);
        assert lhs.scale() == 0 && rhs.scale() == 0 : f("%s - %s", lhs, rhs);
        
        int comp = compare(lhs, rhs);
        if (comp == 0) {
            Log.low(Decimal.class, "subtract", "subtracted ", lhs, " - ", rhs, " = 0");
            return lhs.resetToZero();
        }
        if (comp < 0) {
            temp = (temp == null) ? rhs.clone() : temp.copy(rhs);
            return lhs.copy(subtract(temp, lhs).setNegative(true));
        }
        
        Log.low(Decimal.class, "subtract", "subtracting ", lhs, " - ", rhs);
        
        int maxL = lhs.maxColumn();
        int maxR = rhs.maxColumn();
        
        for (int col = rhs.minColumn(); col <= maxR; ++col) {
            int digitL = lhs.getDigit(col);
            int digitR = rhs.getDigit(col);
            
            if (digitL < digitR) {
                for (int next = (col + 1); next <= maxL; ++next) {
                    int borrow = lhs.getDigit(next);
//                    Log.low(Decimal.class, "subtract", "borrowing from ", borrow, " in ", lhs);
                    if (borrow != 0) {
                        digitL += 10;
                        lhs.setDigit(next, borrow - 1);
                        break;
                    } else {
                        lhs.setDigit(next, 9);
                    }
                }
            }
            
            assert digitL >= digitR : f("%s - %s in %s - %s", digitL, digitR, lhs, rhs);
            lhs.setDigit(col, digitL - digitR);
        }
        
        int leading0s = Builder.countLeadingZeroes(lhs.digits, lhs.width);
        if (leading0s > 0) {
            lhs.reverse().setWidth(lhs.width - leading0s).reverse();
        }
        
        Log.low(Decimal.class, "subtract", "result is ", lhs);
        
        return lhs;
    }
}