/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.app.*;
import eps.eval.*;
import eps.util.*;

import java.math.*;
import java.util.function.*;

public final class Float64 implements Float {
    public static final Float64 APPROX_ZERO = new Float64(0, true);
    public static final Float64 APPROX_ONE  = new Float64(1, true);
    
    private final double  value;
    private final boolean isApproximate;
    
    public Float64(double value, boolean isApproximate) {
        UndefinedException.requireReal(value);
        this.value         = (value == 0.0) ? 0.0 : value; // no -0.0
        this.isApproximate = isApproximate;
    }
    
    public static Float64 approx(double value) {
        return new Float64(value, true);
    }
    
    public static Float64 exact(double value) {
        return new Float64(value, false);
    }
    
    public double doubleValue() {
        return value;
    }
    
    @Override
    public BigDecimal toBigDecimalExact() {
        return new BigDecimal(value); // valueOf?
    }
    
    @Override
    public boolean isApproximate() {
        return isApproximate;
    }
    
    @Override
    public boolean isOne(MathEnv env) {
        return isDefined(env) /*&& !isApproximate*/ && (value == 1.0);
    }
    
    @Override
    public boolean isZero(MathEnv env) {
        return isDefined(env) /*&& !isApproximate*/ && (value == 0.0);
    }
    
    @Override
    public boolean isDefined(MathEnv env) {
        boolean isDefined = Math2.isReal(value);
        assert isDefined : this;
        return isDefined;
    }
    
    @Override
    public boolean isInteger(MathEnv env) {
        if (isApproximate || !isDefined(env)) {
            return false;
        }
        double value = this.value;
        return Math.rint(value) == value;
    }
    
    @Override
    public Integer toInteger(MathEnv env) {
        if (isInteger(env)) {
            return env.valueOf((long) value);
        }
        return null;
    }
    
    @Override
    public Integer toIntegerInexact(MathEnv env) {
        return env.valueOf(Math.round(value));
    }
    
    @Override
    public int signum() {
        double value = this.value;
        return (value < 0) ? -1 : (value > 0) ? +1 : 0;
    }
    
    @Override
    public Number negate(MathEnv env) {
        return new Float64(-value, isApproximate);
    }
    
    @Override
    public Number abs(MathEnv env) {
        if (value < 0) {
            return negate(env);
        }
        return this;
    }
    
    @Override
    public Number reciprocal(MathEnv env) {
        double value = this.value;
        if (value == 0) {
            throw UndefinedException.divisionByZero();
        }
        return approx(1 / value);
    }
    
    private Number round(MathEnv env, DoubleUnaryOperator op) {
        double  doubleValue   = op.applyAsDouble(value);
        boolean isApproximate = this.isApproximate;
        
        if (!isApproximate) {
            try {
                long longValue = Math2.toLongExact(doubleValue);
                return env.valueOf(longValue);
            } catch (ArithmeticException x) {
                isApproximate = true;
            }
        }
        
        return new Float64(doubleValue, isApproximate);
    }
    
    @Override
    public Number floor(MathEnv env) {
        return this.round(env, Math2.flooringDoubles());
    }
    
    @Override
    public Number ceil(MathEnv env) {
        return this.round(env, Math2.ceilingDoubles());
    }
    
    @Override
    public boolean isTruthy(MathEnv env) {
        return /*isApproximate ||*/ value != 0.0;
    }
    
    @Override
    public int relateTo(Float that, MathEnv env) {
        double lhs = this.value;
        double rhs = ((Float64) that).value;
        return (lhs < rhs) ? -1 : (lhs > rhs) ? +1 : 0;
    }
    
    private Number apply(Float rhs, MathEnv env, DoubleBinaryOperator doubleOp, LongBinaryOperator longOp, boolean isDivision) {
        Float64 rh64 = (Float64) rhs;
        
        double valL = this.value;
        double valR = rh64.value;
        
        if (isDivision && valR == 0) {
            throw UndefinedException.divisionByZero(this, rhs);
        }
        
        if (!this.isApproximate && !rh64.isApproximate) {
            try {
                long longL = Math2.toLongExact(valL);
                long longR = Math2.toLongExact(valR);
                
                long   longResult   = longOp.applyAsLong(longL, longR);
                double doubleResult = Math2.toDoubleExact(longResult);
                
                return exact(doubleResult);
                
            } catch (ArithmeticException x) {
                Log.caught(getClass(), "while attempting exact result", x, true);
            }
        }
        
        double result = doubleOp.applyAsDouble(valL, valR);
        return approx(result);
    }
    
    @Override
    public Number add(Float rhs, MathEnv env) {
        return apply(rhs, env, Math2.addingDoubles(), Math2.addingExactLongs(), false);
    }
    
    @Override
    public Number subtract(Float rhs, MathEnv env) {
        return apply(rhs, env, Math2.subtractingDoubles(), Math2.subtractingExactLongs(), false);
    }
    
    @Override
    public Number multiply(Float rhs, MathEnv env) {
        return apply(rhs, env, Math2.multiplyingDoubles(), Math2.multiplyingExactLongs(), false);
    }
    
    @Override
    public Number divide(Float rhs, MathEnv env) {
        return apply(rhs, env, Math2.dividingDoubles(), Math2.dividingExactLongs(), true);
    }
    
    @Override
    public Float divideAsFloat(Float rhs, MathEnv env) {
        double dividend = this.value;
        double divisor  = ((Float64) rhs).value;
        
        if (divisor == 0) {
            throw UndefinedException.divisionByZero(this, rhs);
        }
        
        return approx(dividend / divisor);
    }
    
    @Override
    public Number mod(Float rhs, MathEnv env) {
        return apply(rhs, env, Math2.moddingDoubles(), Math2.moddingExactLongs(), true);
    }
    
    @Override
    public Number promoteTo(Number rhs, MathEnv env) {
        switch (rhs.getNumberKind()) {
            case BOOLEAN:
            case INTEGER:
            case FLOAT:
            case FRACTION:
                return this;
            default:
                throw UnsupportedException.operandTypes(this, rhs, "numeric promotion");
        }
    }
    
    // Note: Perhaps we should consider isApproximate for hashCode and equals.
    //       However, we shouldn't be calling these methods to evaluate numerical
    //       equivalence during evaluation. (i.e., the MathEnv should not call
    //       these methods. It should use use relateTo(....) == 0, if at all.)
    
    @Override
    public int hashCode() {
        return Double.hashCode(value);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Float64) {
            return Double.doubleToLongBits(this.value)
                == Double.doubleToLongBits(((Float64) obj).value);
        }
        return false;
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context context, MutableBoolean approx) {
        if (approx != null) {
            approx.or(isApproximate);
        }
        return b.append(context.getMathEnvironment().toDecimal(this));
    }
    
    @Override
    public String toString() {
        return Double.toString(value);
    }
}