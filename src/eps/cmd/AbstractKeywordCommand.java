/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.ui.*;

import java.util.*;
import java.util.regex.*;

abstract class AbstractKeywordCommand implements Command {
    private final List<String> keywords;
    private final Pattern      pattern;
    
    protected AbstractKeywordCommand(String... keywords) {
        List<String> list = new ArrayList<>(0);
        
        StringBuilder pattern = new StringBuilder("(\\s*(");
        
        if (keywords != null && keywords.length != 0) {
            for (int i = 0; i < keywords.length; ++i) {
                String keyword = keywords[i];
                list.add(Objects.requireNonNull(keyword, "keyword"));
                
                if (i != 0) {
                    pattern.append('|');
                }
                pattern.append(Pattern.quote(keyword));
            }
        }
        
        // (\\s*(wordA|wordB)\\s+)\\S?.*
        // \\s*(wordA|wordB)(\\s+(?:\\S.*)?)?
        // (\\s*(wordA|wordB))(\\s+(\\S.*)?)?
        pattern.append("))(\\s+(\\S.*)?)?");
        
        this.keywords = Collections.unmodifiableList(list);
        this.pattern  = Pattern.compile(pattern.toString(), Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    }
    
    @Override
    public List<String> getKeywordsHandled() {
        return keywords;
    }
    
    @Override
    public boolean isCommand(String text) {
        return pattern.matcher(text).matches();
    }
    
    protected int getKeywordEnd(String text) {
        Matcher mat = pattern.matcher(text);
        if (mat.matches()) {
            return mat.end(2);
        }
        return -1;
    }
    
    @Override
    public boolean execute(String text, Item item) {
        Matcher mat = pattern.matcher(text);
        
        if (mat.matches()) {
            String keyword = mat.group(2);
            String substr  = mat.group(4);
            
            return executeImpl(text, keyword, (substr == null) ? "" : substr.trim(), item);
        }
        
        return false;
    }
    
    protected TextAreaCell newTextArea() {
        return App.app().getComponentFactory().newTextArea().followTextStyle(Setting.COMMAND_STYLE);
    }
    
    protected TextAreaCell newTextArea(String text) {
        TextAreaCell cell = this.newTextArea();
        cell.setText(text);
        return cell;
    }
    
    protected TextAreaCell newTextArea(String format, Object... args) {
        return this.newTextArea(String.format(format, args));
    }
    
    protected abstract boolean executeImpl(String full, String cmd, String text, Item item);
}