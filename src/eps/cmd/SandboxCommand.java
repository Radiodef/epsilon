/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.ui.*;
import eps.ui.swing.*;
import eps.util.*;
import eps.json.*;
import eps.eval.*;
import eps.block.*;
import static eps.app.App.*;

import java.util.*;
import java.util.List;
import java.lang.reflect.*;
import javax.swing.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.util.function.*;
import static java.util.stream.Collectors.*;

final class SandboxCommand extends AbstractKeywordCommand {
    private final boolean enabled = EpsilonMain.isDeveloper();
    
    SandboxCommand() {
        super("sandbox");
    }
    
    @Override
    public String getHelpText(String text) {
        if (this.enabled && "sandbox".equalsIgnoreCase(text)) {
            return "Enter \"sandbox\" to run the developer sandbox routine.";
        }
        return null;
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String text, Item item) {
        if (this.enabled) {
            String echo = "sandbox";
            if (!(text = text.trim()).isEmpty()) {
                echo += " " + text;
            }
            item.addCell(this.newTextArea(echo));
            try {
                sandbox(text, item);
            } catch (Exception x) {
                Log.caught(SandboxCommand.class, "executeImpl", x, true);
            }
            return true;
        }
        return false;
    }
    
    
    
    private void sandbox(String text, Item item) throws Exception {
        for (Method m : java.awt.Desktop.class.getMethods()) {
            Log.note(m);
        }
    }
}

//@JsonSerializable
//class Test {
//    @JsonProperty
//    String foo;
//    @JsonProperty
//    String bar;
//
//    @JsonDefault
//    static String fooDefault = "FOO";
//    @JsonDefault
//    static String barDefault = "BAR";
//
//    @JsonConstructor
//    Test(String foo, String bar) {
//        this.foo = foo;
//        this.bar = bar;
//    }
//
//    @Override
//    public String toString() {
//        return Json.stringify(this, true);
//    }
//}