/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.util.*;
import static eps.util.Misc.*;

import javax.swing.*;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;

final class UiDefaultCommand extends AbstractKeywordCommand {
    UiDefaultCommand() {
        super("uidef");
    }
    
    @Override
    public String getHelpText(String cmd) {
        if ("uidef".equalsIgnoreCase(cmd)) {
            return "Prints UIDefaults.";
        }
        return null;
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String text, Item item) {
        String str;
        
        if (text.isEmpty()) {
            str = toString(e -> true);
        } else if (text.startsWith("with")) {
            String key = text.substring("with".length()).trim();
            str = toString(e -> String.valueOf(e.getKey()).contains(key)
                             || String.valueOf(e.getValue()).contains(key));
        } else {
            str = toString(text, 0, UIManager.getDefaults().get(text));
        }
        
        item.addCell(newTextArea(str));
        
        return true;
    }
    
    private String toString(Predicate<? super Map.Entry<?, ?>> p) {
        List<Pair<String, String>> list = new ArrayList<>();
        
        defaults().filter(p).forEach(e -> {
            String key = String.valueOf(e.getKey());
            String val = String.valueOf(e.getValue());
            list.add(Pair.of(key, val));
        });
        
        int width = 0;
        for (Pair<String, String> def : list) {
            width = Math.max(width, def.left.length());
        }
        
        int max = width;
        return j(list, "\n", def -> toString(def, max));
    }
    
    private Stream<Map.Entry<?, ?>> defaults() {
        return Stream.concat(UIManager.getDefaults().entrySet().stream(),
                             UIManager.getLookAndFeelDefaults().entrySet().stream());
    }
    
    private String toString(Pair<String, String> def, int max) {
        return toString(def.left, max - def.left.length(), def.right);
    }
    
    private String toString(Object key, int s, Object val) {
        return String.format("\"%s\" %s= \"%s\"", key, space(s), val);
    }
    
    private String space(int width) {
        if (width == 0)
            return "";
        char[] chars = new char[width];
        Arrays.fill(chars, ' ');
        return new String(chars);
    }
}