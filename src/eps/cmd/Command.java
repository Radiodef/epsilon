/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;

import java.util.*;

public interface Command {
    boolean isCommand(String text);
    boolean execute(String text, Item item);
    
    default List<String> getKeywordsHandled() {
        return Collections.emptyList();
    }
    
    default String getHelpText(String keyword) {
        return null;
    }
    
    default int getCommandEnd(String text) {
        if (this.isCommand(text)) {
            return text.length();
        } else {
            return -1;
        }
    }
    
    List<Command> ALL_COMMANDS = CommandPrivate.ALL_COMMANDS;
    
    static Command getCommand(String text) {
        List<Command> commands = ALL_COMMANDS;
        int           size     = commands.size();
        for (int i = 0; i < size; ++i) {
            Command cmd = commands.get(i);
            if (!(cmd instanceof NoExplicitCommand) && cmd.isCommand(text))
                return cmd;
        }
        return null;
    }
    
    static boolean isAnyCommand(String text) {
        return Command.getCommand(text) != null;
    }
    
    static void executeFirst(String text, Item item) {
        List<Command> commands = ALL_COMMANDS;
        int           size     = commands.size();
        for (int i = 0; i < size; ++i)
            if (commands.get(i).execute(text, item))
                break;
    }
}

final class CommandPrivate {
    private CommandPrivate() {
    }
    
    static final List<Command> ALL_COMMANDS;
    static {
        List<Command> all = new ArrayList<>();
        
        // Add new commands here...
        all.add(new SetCommand());
        all.add(new HelpCommand());
        all.add(new DebugCommand());
        all.add(new SettingsCommand());
        all.add(new SymbolsCommand());
        
        all.add(new AnswerFormatCommand());
        all.add(new FactorCommand());
        all.add(new ParseCommand());
        all.add(new AstCommand());
        
        if (EpsilonMain.isDeveloper()) {
            all.add(new EchoCommand());
            all.add(new ThemeCommand());
            all.add(new SandboxCommand());
            
            if (EpsilonMain.getUserInterfaceType() == EpsilonMain.UserInterfaceType.SWING) {
                all.add(new UiDefaultCommand());
            }
        }
        
        // NoExplicitCommand must always be at the bottom.
        // (It accepts any command.)
        all.add(new NoExplicitCommand());
        
        ALL_COMMANDS = Collections.unmodifiableList(all);
    }
}