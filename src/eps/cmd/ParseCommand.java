/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.eval.*;
import eps.job.*;
import eps.ui.*;
import eps.block.*;
import static eps.util.Misc.*;
import static eps.app.App.*;

import java.util.*;
import java.util.stream.*;

final class ParseCommand extends AbstractKeywordCommand {
    ParseCommand() {
        super("parse");
    }
    
    @Override
    public String getHelpText(String command) {
        if ("parse".equalsIgnoreCase(command)) {
            return "Parses an expression and displays information about the tokens rather than evaluating it.";
        }
        return null;
    }
    
    @Override
    public int getCommandEnd(String command) {
        return this.getKeywordEnd(command);
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String expr, Item item) {
        TextAreaCell comp = app().getComponentFactory().newTextArea();
        comp.setText(new StyleBlock(full).set(Setting.COMMAND_STYLE));
        
        item.addCell(comp);
        
        ParseJob job = new ParseJob(expr, item, comp);
        app().getWorker().enqueue(job, false);
        
        return true;
    }
    
    private final class ParseJob extends AbstractJob {
        private final Item item;
        private final TextAreaCell comp;
        private final String expr;
        
        private volatile List<Token> tokens;
        private volatile Block tokensBlock;
        private volatile Block compBlock;
        
        ParseJob(String expr, Item item, TextAreaCell comp) {
            this.item = Objects.requireNonNull(item, "item");
            this.comp = Objects.requireNonNull(comp, "comp");
            this.expr = Objects.requireNonNull(expr, "expr");
        }
        
        @Override
        protected void executeImpl() {
            App app = app();
            try (Settings sets = app.getSettings().block();
                 Symbols  syms = app.getSymbols().block()) {
                Context     context = new Context(expr);
                List<Token> tokens  = Parser.tokenize(context);
                this.tokens         = tokens;
                
                JoinBlock lines = new JoinBlock().setDelimiter(Block.ln());
                JoinBlock comp  = new JoinBlock();
                
                comp.addBlock(new StyleBlock(getKeywordsHandled().get(0)).set(Setting.COMMAND_STYLE));
                
                StringBuilder b = new StringBuilder();
            
                int      count  = tokens.size();
                String[] labels = new String[count];
                String[] values = new String[count];
                
                for (int i = 0; i < count; ++i) {
                    Token token = tokens.get(i);
                    
                    b.setLength(0);
                    String label = "Token";
                    
                    switch (token.getTokenKind()) {
                        case UNRESOLVED:
                            label = "Unresolved Token";
                            b.append(token.getSource());
                            break;
                            
                        case AMBIGUOUS:
                            label = "Ambiguous Token";
                            b.append(token.getSource());
                            
                            String symbols =
                                ((Token.OfAmbiguous) token).getGroup().stream()
                                    .map(Symbol::getSymbolKind)
                                    .map(Symbol.Kind::toShortString)
                                    .collect(Collectors.joining(", "));

                            b.append(" (").append(symbols).append(")");
                            break;
                            
                        case SYMBOL:
                            label = "Symbol";
                            
                            Symbol sym  = (Symbol) token.getValue();
                            String kind = sym.getSymbolKind().toLowerCase(token);
                            
                            b.append(sym.getName()).append(" (").append(kind).append(")");
                            break;
                            
                        case NUMBER:
                            label = "Number";
                            b.append(token.getValue());
                            break;
                            
                        case STRING_LITERAL:
                            label = "String Literal";
                            b.append(token.getValue());
                            break;
                            
                        case EMPTY:
                            label = "Empty Operand";
                            break;
                            
                        default:
                            assert false : token.toDebugString();
                            label = "Other Token";
                            b.append(token.toDebugString());
                            break;
                    }
                    
                    labels[i] = label;
                    values[i] = b.toString();
                }
                
                boolean xSpace = false;
                
                int maxWidth = 0;
                if (count > 0) {
                    maxWidth = labels[0].length();
                    for (int i = 1; i < count; ++i) {
                        int len = labels[i].length();
                        if (len != maxWidth) {
                            xSpace   = true;
                            maxWidth = Math.max(maxWidth, len);
                        }
                    }
                    xSpace = false;
                }
                
                String indent = unquote(replnull(context.getSetting(Setting.EDITOR_INDENT),
                                                 Setting.EDITOR_INDENT.getDefaultValue()));
                
                for (int i = 0; i < count; ++i) {
                    Token          token = tokens.get(i);
                    String         label = labels[i];
                    String         value = values[i];
                    Setting<Style> style = token.getStyleSetting();
                    
                    b.setLength(0);
                    b.append(indent).append(label);
                    
                    if (!token.isEmpty()) {
                        for (int s = (maxWidth - label.length()); s > 0; --s) {
                            b.append(' ');
                        }
                        
                        if (xSpace) {
                            b.append(' ');
                        }
                        
                        b.append(": ").append(value);
                    }
                    
                    Block lineBlock  = Block.of(b.toString());
                    Block tokenBlock = Block.of(token.getSource());
                    
                    if (style != null) {
                        lineBlock  = new StyleBlock(lineBlock).set(style);
                        tokenBlock = new StyleBlock(tokenBlock).set(style);
                    }
                    
                    lines.addBlock(lineBlock);
                    
                    String space = " ";
                    
                    Token prev = token.getPrev();
                    if (prev != null) {
                        space = null;
                        
                        int before = prev.getSource().getSourceEnd();
                        int after  = token.getSource().getSourceStart();
                        
                        if (before < after) {
                            space = token.getSource().getFullSource().substring(before, after);
                        }
                    }
                    
                    if (space != null) {
                        comp.addBlock(Block.of(space));
                    }
                    
                    comp.addBlock(tokenBlock);
                }
                
                tokensBlock = lines;
                compBlock   = comp;
            }
        }
        
        @Override
        protected void doneImpl() {
            Block compBlock = this.compBlock;
            if (compBlock != null) {
                comp.setText(compBlock);
            }
            
            List<Token> tokens = this.tokens;
            if (tokens == null)
                return;
            Block tokensBlock = this.tokensBlock;
            if (tokensBlock == null)
                return;
            TextAreaCell cell = app().getComponentFactory().newTextArea();
            
            cell.setText(tokensBlock);
            item.addCell(cell);
        }
    }
}