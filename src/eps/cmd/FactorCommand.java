/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.eval.*;
import eps.ui.*;
import eps.block.*;

final class FactorCommand extends AbstractKeywordCommand {
    FactorCommand() {
        super("factor");
    }
    
    @Override
    public String getHelpText(String command) {
        if ("factor".equalsIgnoreCase(command)) {
            return "Displays the prime factorization of the absolute value of the integer "
                 + "result of a computation. If the result of the computation is not an "
                 + "integer, the result is rounded to one before being factored.";
        }
        return null;
    }
    
    @Override
    public int getCommandEnd(String command) {
        return this.getKeywordEnd(command);
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String expr, Item item) {
        ComputationCell cell = App.app().getComponentFactory().newComputation();
        item.addCell(cell);
        
        cell.setText(new StyleBlock(full + " = ...").set(Setting.COMMAND_STYLE));
        
        App.evaluate(expr, item, cell, AnswerFormatter.DECIMAL, PostComputation.PRIME_FACTORS);
        return true;
    }
}