/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.eval.*;
import eps.ui.*;
import eps.block.*;

// TODO: should an arbitrary radix be possible? "radix2 2 + 3" or "radix3 2 + 3"
final class AnswerFormatCommand extends AbstractKeywordCommand {
    AnswerFormatCommand() {
        super("bin", "hex", "sci");
    }
    
    @Override
    public String getHelpText(String command) {
        boolean isCommand = false;
        String  lower     = "";
        if ("bin".equalsIgnoreCase(command)) {
            isCommand = true;
            command   = "bin";
            lower     = "binary";
        }
        if ("hex".equalsIgnoreCase(command)) {
            isCommand = true;
            command   = "hex";
            lower     = "hexadecimal";
        }
        if ("sci".equalsIgnoreCase(command)) {
            isCommand = true;
            command   = "sci";
            lower     = "scientific notation";
        }
        if (isCommand) {
            return "Formats the result of a computation as " + lower + "."
                 + "\n\n"
                 + "To use this command, enter \"" + command + " [expression]\" where "
                 + "[expression] is the computation whose result ought to be formatted. "
                 + "The expression is evaluated normally."
                 + "\n\n"
                 + "For example, \"" + command + " 11 + 12\" displays the result of "
                 + "\"11 + 12\" in " + lower + ".";
        }
        return null;
    }
    
    @Override
    public int getCommandEnd(String command) {
        return this.getKeywordEnd(command);
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String expr, Item item) {
        ComputationCell cell = App.app().getComponentFactory().newComputation();
        item.addCell(cell);
        
        AnswerFormatter fmt;
        if ("bin".equalsIgnoreCase(cmd)) {
            fmt = AnswerFormatter.BINARY;
        } else if ("hex".equalsIgnoreCase(cmd)) {
            fmt = AnswerFormatter.HEXADECIMAL;
        } else if ("sci".equalsIgnoreCase(cmd)) {
            fmt = AnswerFormatter.SCIENTIFIC;
        } else {
            assert false : cmd;
            fmt = AnswerFormatter.DECIMAL;
        }
        
        cell.setText(new StyleBlock(full + " = ...").set(Setting.COMMAND_STYLE));
        
        App.evaluate(expr, item, cell, fmt, PostComputation.IDENTITY);
        return true;
    }
}