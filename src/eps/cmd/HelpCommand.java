/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.ui.*;
import static eps.util.Literals.*;
import static eps.app.App.*;

import java.util.*;
import java.util.regex.*;

final class HelpCommand extends AbstractKeywordCommand {
    HelpCommand() {
        super("help");
    }
    
    public static final char BULLET = '•';
    
    @Override
    public String getHelpText(String text) {
        if ("help".equalsIgnoreCase(text)) {
            return "Enter \"help\" followed by a whitespace-delimited list of zero or more topics.";
        }
        return null;
    }
    
    private static final List<Topic> TOPICS =
        ListOf(new HelpTopic(),
               new SettingsTopic(),
               new CommandsTopic(),
               new FontsTopic(),
               new StylesTopic());
    
    @Override
    protected TextAreaCell newTextArea() {
        return app().getComponentFactory().newTextArea();
    }
    
    @Override
    protected boolean executeImpl(String full, String _help, String text, Item item) {
        for (Topic topic : TOPICS) {
            if (topic.isTopic(text)) {
                topic.print(text, item);
                return true;
            }
        }
        
        for (Command cmd : Command.ALL_COMMANDS) {
            String help = cmd.getHelpText(text);
            if (help != null) {
                item.addCell(this.newTextArea(help));
                return true;
            }
        }
        
        item.addCell(this.newTextArea("Unknown help topic \"%s\". Enter \"help\" to see a list of available topics.", text));
        return true;
    }
    
    private static abstract class Topic {
        private Topic() {}
        abstract boolean isTopic(String topic);
        abstract void    print(String text, Item item);
    }
    
    private static abstract class SimpleTopic extends Topic {
        private final String keyword;
        
        private SimpleTopic(String keyword) {
            this.keyword = Objects.requireNonNull(keyword, "keyword");
        }
        
        @Override
        boolean isTopic(String topic) {
            return topic.equalsIgnoreCase(keyword);
        }
    }
    
    private static abstract class SimpleTextAreaTopic extends SimpleTopic {
        private SimpleTextAreaTopic(String keyword) {
            super(keyword);
        }
        
        @Override
        void print(String text, Item item) {
            TextAreaCell cell = app().getComponentFactory().newTextArea();
            cell.setText(this.getText());
            item.addCell(cell);
        }
        
        abstract CharSequence getText();
    }
    
    private static final class HelpTopic extends SimpleTextAreaTopic {
        private HelpTopic() {
            super("");
        }
        
        private final CharSequence text =
            new StringBuilder()
                .append("Enter \"help [topic]\" where [topic] is one of the following:")
                .append("\n • commands")
                .append("\n • settings")
                .append("\n • fonts")
                .append("\n • styles");
        @Override
        CharSequence getText() {
            return text;
        }
    }
    
//    @Deprecated
//    private static final class SettingsTopic_old extends SimpleTextAreaTopic {
//        private SettingsTopic_old() {
//            super("settings");
//        }
//        
//        @Override
//        CharSequence getText() {
//            StringBuilder b = new StringBuilder(64);
//            b.append("Available settings:");
//            
//            for (Setting<?> setting : Setting.values()) {
//                b.append("\n • ").append(setting);
//            }
//            
//            b.append("\n\nEnter \"help settings [settingName]\" where [settingName] is one ") // TODO
//             .append("of the preceding setting names for a description of that setting.");
//            return b;
//        }
//    }
    
    private static final class SettingsTopic extends Topic {
        private SettingsTopic() {
        }
        
        private static final Pattern PAT =
            Pattern.compile("settings(\\s+(" + Setting.NAME_PATTERN + "))?", Pattern.CASE_INSENSITIVE);
        
        @Override
        boolean isTopic(String text) {
            return PAT.matcher(text).matches();
        }
        
        @Override
        void print(String text, Item item) {
            Matcher mat = PAT.matcher(text);
            if (mat.matches()) {
                String name = mat.group(2);
                
                TextAreaCell cell = app().getComponentFactory().newTextArea();
                if (name != null) {
                    try {
                        Setting<?> setting = Setting.valueOfIgnoreCase(name);
                        cell.formatText("%s:\n%s", setting.getName(), setting.getDescription());
                    } catch (IllegalArgumentException x) {
                        Log.caught(SettingsTopic.class, "print", x, true);
                        cell.formatText("'%s' is not the name of a setting. Enter \"help settings\" for a list of available settings.", name);
                    }
                } else {
                    StringBuilder b = new StringBuilder(64);
                    b.append("Available settings:");
                    
                    for (Setting<?> setting : Setting.values()) {
                        b.append("\n • ").append(setting);
                    }
                    
                    b.append("\n\nEnter \"help settings [settingName]\" where [settingName] is one ")
                     .append("of the preceding setting names for a description of that setting.");
                    cell.setText(b);
                }
                
                item.addCell(cell);
            }
        }
    }
    
    private static final class CommandsTopic extends SimpleTextAreaTopic {
        private CommandsTopic() {
            super("commands");
        }
        
        @Override
        CharSequence getText() {
            StringBuilder b = new StringBuilder(64);
            b.append("Available commands:");
            
            Command.ALL_COMMANDS.stream()
                   .map(Command::getKeywordsHandled)
                   .flatMap(List::stream)
                   .sorted()
                   .forEachOrdered(cmd -> b.append("\n • ").append(cmd));
            
            b.append("\n\nEnter \"help [command]\" where [command] is one of the preceding ")
             .append("commands for information about a specific command.");
            return b;
        }
    }
    
    private static final class FontsTopic extends SimpleTextAreaTopic {
        private FontsTopic() {
            super("fonts");
        }
        
        @Override
        CharSequence getText() {
            StringBuilder b = new StringBuilder();
            b.append("Available system font family names:");
            
            for (String font : SystemInfo.getAvailableFonts()) {
                b.append("\n • ").append(font);
            }
            
            return b;
        }
    }
    
    private static final class StylesTopic extends SimpleTextAreaTopic {
        private StylesTopic() {
            super("styles");
        }
        
        @Override
        CharSequence getText() {
            return "Text styles support the following properties:"
                 + "\n • foreground: the color of the text itself."
                 + "\n • background: the color of the highlight behind the text."
                 + "\n • family: the font family name. Enter \"help fonts\" for a list of available font families."
                 + "\n • size: a number specifying the size of the font. If the number is prefixed with either - or +, "
                 + "the size is relative to (smaller or larger than, respectively) the basic style font size. The "
                 + "basic style may not have a relative size."
                 + "\n • bold: true if the text is bold and false otherwise."
                 + "\n • italic: true if the text is italicized and false otherwise."
                 + "\n • underline: true if the text is underlined and false otherwise."
                 + "\n In all cases, the property value may be specified as \"null\" if the style should inherit "
                 + "that property from the basic style.";
        }
    }
}