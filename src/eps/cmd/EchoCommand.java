/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;

final class EchoCommand extends AbstractKeywordCommand {
    EchoCommand() {
        super("echo");
    }
    
    @Override
    public String getHelpText(String cmd) {
        if ("echo".equalsIgnoreCase(cmd)) {
            return "Enter \"echo\" followed by at least one space and any text to print the text to the output window.";
        }
        return null;
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String text, Item item) {
        if (text.isEmpty()) {
            text = "ECHO...ECHo...ECho...Echo...echo...ech...ec...e...";
        }
        item.addCell(this.newTextArea(text));
        return true;
    }
}