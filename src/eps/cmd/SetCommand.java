/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.ui.*;
import eps.util.*;
import static eps.app.App.*;

import java.util.*;
import java.util.regex.*;

final class SetCommand extends AbstractKeywordCommand {
    private static final String SET    = "set";
    private static final String RESET  = "reset";
    private static final String SHOW   = "show";
    private static final String ADD    = "add";
    private static final String REMOVE = "remove";
    private static final String ALL    = "all";
    
    private static boolean isSet   (String cmd) { return SET.equalsIgnoreCase(cmd);    }
    private static boolean isReset (String cmd) { return RESET.equalsIgnoreCase(cmd);  }
    private static boolean isShow  (String cmd) { return SHOW.equalsIgnoreCase(cmd);   }
    private static boolean isAdd   (String cmd) { return ADD.equalsIgnoreCase(cmd);    }
    private static boolean isRemove(String cmd) { return REMOVE.equalsIgnoreCase(cmd); }
    private static boolean isAll   (String val) { return ALL.equalsIgnoreCase(val);    }
    
    SetCommand() {
        super(SET, RESET, SHOW);
    }
    
    @Override
    public String getHelpText(String text) {
        if (isReset(text)) {
            return "Enter \"reset [settingName]\" where [settingName] is the name of a "
                 + "setting to reset it to its default value."
                 + "\n\n"
                 + "To reset all settings at once, enter \"reset all\" and confirm the action."
                 + "\n\n"
                 + "Enter \"help settings\" for a list of available settings.";
        }
        if (isSet(text)) {
            return "Enter \"set [settingName] [settingValue]\" where [settingName] is "
                 + "the name of a setting and [settingValue] is the new value to be parsed."
                 + "\n\n"
                 + "If the setting is a list of elements, then the set command may be "
                 + "of either the form \"set [settingName] add [elementValue]\" or "
                 + "\"set [settingName] remove [elementValue]\", where [elementValue] is "
                 + "the element value to add or remove."
                 + "\n\n"
                 + "If the setting is a style, then the set command may be of the form "
                 + "\"set [settingName] [propertyName] [propertyValue] where [propertyName] "
                 + "is the name of a style property and [propertyValue] is the new value to be "
                 + "parsed for that property. For example, \"set basicStyle foreground red\" "
                 + "changes the base style for text to be red. Enter \"help styles\" for a "
                 + "list of style properties."
                 + "\n\n"
                 + "Enter \"help settings\" for a list of available settings.";
        }
        if (isShow(text)) {
            return "Enter \"show [settingName]\" where [settingName] is the name "
                 + "of a setting to show its current value."
                 + "\n\n"
                 + "Enter \"help settings\" for a list of available settings.";
        }
        return null;
    }
    
    private static final Pattern SETTING_KEY = Pattern.compile("\\S+");
    
    @Override
    protected boolean executeImpl(String full, String cmd, String text, Item item) {
        Settings settings = app().getSettings();
        
        item.addCell(this.newTextArea(full));
        
        Matcher mat = SETTING_KEY.matcher(text);
        if (mat.find()) {
            String settingName = mat.group();
            
            if (isReset(cmd) && isAll(settingName)) {
                resetAll();
                return true;
            }
            
            Setting<?> setting;
            try {
                setting = Setting.valueOfIgnoreCase(settingName);
            } catch (IllegalArgumentException x) {
                item.formatNote("'%s' is not a setting. Enter \"help settings\" for a list of available settings.", settingName);
                Log.caught(SetCommand.class, "executeImpl", x, true);
                return true;
            }
            
            String val = text.substring(mat.end()).trim();
            
            if (setting.isTransient() && (isReset(cmd) || isSet(cmd))) {
                item.formatNote("Warning: %s is transient, meaning its value is computed from another "
                              + "setting rather than set directly. The specified value may be lost.", setting);
            }
            
            if (isReset(cmd)) {
                // reset
                reset(settings, setting);
            } else if (isSet(cmd)) {
                // set
                set(settings, setting, val, item);
            } else if (isShow(cmd)) {
                // show
                show(settings, setting, item);
            } else {
                throw new AssertionError(cmd);
            }
        } else {
            noSettingName(cmd, item);
        }
        
        return true;
    }
    
    private void noSettingName(String cmd, Item item) {
        if (isSet(cmd)) {
            item.formatNote("The %s command requires a setting name and value. Enter \"help %s\" for instructions.", cmd, cmd);
        } else {
            item.formatNote("The %s command requires a setting name. Enter \"help %s\" for instructions.", cmd, cmd);
        }
    }
    
    private static final Pattern ADD_REMOVE_PATTERN =
        Pattern.compile("(" + ADD + "|" + REMOVE + ")(\\s+(.*))?", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    
    private static <T> void set(Settings settings, Setting<T> setting, String val, Item item) {
        if (val.isEmpty()) {
            item.addNote("The set command requires a value. Enter \"help set\" for instructions.");
            return;
        }
        
        if (setting.getType() == Style.class) {
            setStyle(settings, setting.as(Style.class), val, item);
            return;
        }
        
        setNoStyle(settings, setting, val, item);
    }
    
    private static <T> void setNoStyle(Settings settings, Setting<T> setting, String val, Item item) {
        Matcher mat = ADD_REMOVE_PATTERN.matcher(val);
        if (mat.matches()) {
            String cmd  = mat.group(1);
            String elem = mat.group(3);
            
            if (elem == null || (elem = elem.trim()).isEmpty()) {
                item.formatNote("The %s subcommand requires a value. Enter \"help set\" for instructions.", cmd);
                return;
            }
            
            addOrRemove(cmd, item, settings, setting, elem);
            return;
        }
        
        Throwable caught = null;
        
        try {
            settings.set(setting, setting.parse(val));
            refresh();
        } catch (ObjectParser.BadStringException x) {
            item.addNote(x.getMessage());
            caught = x;
        } catch (IllegalArgumentException | NullPointerException | ClassCastException x) {
            item.formatNote("Specified value '%s' is invalid for %s.", val, setting.getName());
            caught = x;
        }
        
        if (caught != null) {
            Log.caught(SetCommand.class, "set", caught, true);
        }
    }
    
    private static final Pattern STYLE_PROP_PAT =
        Pattern.compile("\\s*([a-zA-Z_-]+)(\\s+(.*))?", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    
    private static void setStyle(Settings settings, Setting<Style> setting, String text, Item item) {
        Matcher mat = STYLE_PROP_PAT.matcher(text);
        if (mat.matches()) {
            String prop = mat.group(1);
            String val  = mat.group(3);
            
            if (val == null || (val = val.trim()).isEmpty()) {
                item.formatNote("To set a style property, a value must be provided after the property name.");
                return;
            }
            
            if (!Style.isProperty(prop)) {
                item.formatNote("'%s' is not a valid style property name. Enter \"help styles\" for a list of style properties.", prop);
                return;
            }
            
            try {
                Style style = settings.get(setting);
                
                style = Style.replnull(style);
                style = style.parseByName(prop, val);
                
                settings.set(setting, style);
                refresh();
                
            } catch (IllegalArgumentException | NullPointerException | ClassCastException x) {
                Log.caught(SetCommand.class, "setStyle", x, true);
                item.formatNote("Specified value '%s' is invalid for the specified style property.", val);
            }
            return;
        }
        
        // all else
        setNoStyle(settings, setting, text, item);
    }
    
    private static <T> void addOrRemove(String cmd, Item item, Settings settings, Setting<T> setting, String elemText) {
        final class NotCollectionException extends RuntimeException {
            private NotCollectionException(Object val) {
                super(String.valueOf(val));
            }
        }
        
        Throwable caught = null;
        
        try {
            settings.compute(setting, val -> {
                if (!(val instanceof Collection<?>)) {
                    throw new NotCollectionException(val);
                }
                
                Object elem   = setting.parseElement(elemText);
                T      common = setting.toCommonValue(val);
                
                @SuppressWarnings("unchecked")
                Collection<Object> unchecked = (Collection<Object>) common;
                
                if (isAdd(cmd)) {
                    if (!unchecked.add(elem)) {
                        item.addNote("Specified setting already contained that element; nothing added.");
                        return val;
                    }
                } else if (isRemove(cmd)) {
                    if (!unchecked.remove(elem)) {
                        item.addNote("Specified setting didn't contain that element; nothing removed.");
                        return val;
                    }
                } else {
                    throw new AssertionError(cmd);
                }
                
                return common;
            });
            refresh();
            
            show(settings, setting, item);
            
        } catch (NotCollectionException x) {
            item.formatNote("The %s subcommand is only applicable to settings with multiple elements.", cmd);
            caught = x;
        } catch (ObjectParser.BadStringException x) {
            item.addNote(x.getMessage());
            caught = x;
        } catch (IllegalArgumentException x) {
            item.formatNote("Specified element value '%s' is invalid for %s.", elemText, setting.getName());
            caught = x;
        }
        
        if (caught != null) {
            Log.caught(SetCommand.class, "add", caught, true);
        }
    }
    
    private static <T> void reset(Settings settings, Setting<T> setting) {
        settings.set(setting, setting.getDefaultValue());
        refresh();
    }
    
    private static void resetAll() {
        app().addItemOnInterfaceThread(item -> {
            TextAreaCell confirm = app().getComponentFactory().newTextArea();
            confirm.followTextStyle(Setting.COMMAND_STYLE);
            confirm.setText("You are about to reset all settings to their default values.\n"
                          + "Enter 'yes' to confirm the reset or 'no' to cancel it.");
            item.addCell(confirm);
            
            app().addCommandFilter(cmd -> {
                int response = App.CommandFilter.CONSUME;
                
                if ("yes".equalsIgnoreCase(cmd)) {
                    response |= App.CommandFilter.DONE;
                    
                    app().getSettings().doSynchronized(settings -> {
                        for (Setting<?> key : Setting.values()) {
                            settings.reset(key);
                        }
                    });
                    refresh();
                    
                    item.addNote("Reset confirmed; all settings reset.");
                } else if ("no".equalsIgnoreCase(cmd)) {
                    response |= App.CommandFilter.DONE;
                    item.addNote("Reset cancelled.");
                } else {
                    item.formatNote("'%s' unrecognized. Enter 'yes' to confirm or 'no' to cancel.", cmd);
                }
                
                return response;
            });
        });
    }
    
    private static <T> void show(Settings settings, Setting<T> setting, Item item) {
        app().addItemOnInterfaceThread(showItem -> {
            TextAreaCell cell = app().getComponentFactory().newTextArea();
            
            cell.setText(setting.toString(settings.get(setting)));
            
            showItem.addCell(cell);
        });
    }
    
    private static void refresh() {
        app().ifSettingsWindowInitialized(Window::refresh);
    }
}