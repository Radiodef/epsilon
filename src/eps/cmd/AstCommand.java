/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.eval.*;
import eps.math.Number;
import eps.job.*;
import eps.ui.*;
import eps.block.*;
import static eps.util.Misc.*;
import static eps.app.App.*;

import java.util.*;

final class AstCommand extends AbstractKeywordCommand {
    AstCommand() {
        super("ast");
    }
    
    @Override
    public String getHelpText(String command) {
        if ("ast".equalsIgnoreCase(command)) {
            return "Parses an expression and displays information about "
                 + "the abstract syntax tree rather than evaluating it.";
        }
        return null;
    }
    
    @Override
    public int getCommandEnd(String command) {
        return this.getKeywordEnd(command);
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String expr, Item item) {
        TextAreaCell comp = app().getComponentFactory().newTextArea();
        comp.setText(new StyleBlock(full).set(Setting.COMMAND_STYLE));
        
        item.addCell(comp);
        
        TreeJob job = new TreeJob(expr, item, comp);
        app().getWorker().enqueue(job, false);
        
        return true;
    }
    
    private final class TreeJob extends AbstractJob {
        private final Item item;
        private final TextAreaCell comp;
        private final String expr;
        
        private volatile EvaluationException caught;
        
        private volatile Block compBlock;
        private volatile Block astBlock;
        
        TreeJob(String expr, Item item, TextAreaCell comp) {
            this.item = Objects.requireNonNull(item, "item");
            this.comp = Objects.requireNonNull(comp, "comp");
            this.expr = Objects.requireNonNull(expr, "expr");
        }
        
        @Override
        protected void executeImpl() {
            App app = app();
            try (Settings sets = app.getSettings().block();
                 Symbols  syms = app.getSymbols().block()) {
                Context context = new Context(expr);
                Tree    tree    = Tree.create(context);
                Node    root    = tree.getRoot(true);
                
                Block cmdBlock  = new StyleBlock(getKeywordsHandled().get(0)).set(Setting.COMMAND_STYLE);
                Block exprBlock = root.toBlock(context);
                compBlock = Block.join(' ', cmdBlock, exprBlock);
                astBlock  = makeAstBlock(context, root);
                
            } catch (EvaluationException x) {
                caught = x;
                // TODO: something we could do is call Parser.toTokens
                //       or whatever the method is that just tokenizes
                //       whatever without complaining. then we can at
                //       least format the cell for the command.
            }
        }
        
        @Override
        protected void doneImpl() {
            if (compBlock != null) {
                comp.setText(compBlock);
            }
            
            if (caught != null) {
                List<String> messages = new ArrayList<>();
                caught.collectAllMessages(messages::add);
                messages.forEach(item::addNote);
                
            } else if (astBlock != null) {
                TextAreaCell cell = app().getComponentFactory().newTextArea();
                cell.setText(astBlock);
                item.addCell(cell);
                
            } else {
                // we were probably cancelled
                Log.note(getClass(), "doneImpl", "no error nor AST produced for ", expr);
            }
        }
        
        private Block makeAstBlock(Context context, Node root) {
            List<Node> flattened = flatten(new ArrayList<>(), root);
            
            int count = flattened.size();
            
            List<Block> lines = new ArrayList<>(count);
            for (Node node : flattened) {
                lines.add(makeBlock(context, node));
            }
            
            List<Block> prefixes = makeTreePrefixes(context, root, flattened);
            for (int i = 0; i < count; ++i) {
                lines.set(i, Block.join(prefixes.get(i), lines.get(i)));
            }
            
            return new JoinBlock(lines).setDelimiter(new NewLineBlock());
        }
        
        private List<Node> flatten(List<Node> list, Node current) {
            switch (current.getNodeKind()) {
                case UNARY_OP:
                case BINARY_OP:
                case SPAN_OP:
                case NARY_OP:
                    list.add(current);
                    for (Node op : ((Node.OfOperator) current).getOperands()) {
                        flatten(list, op);
                    }
                    break;
                case OPERAND:
                    list.add(current);
                    break;
                default:
                    assert false : current;
                    list.add(current);
                    break;
            }
            return list;
        }
        
        private Block makeBlock(Context context, Node node) {
            Node.Kind kind = node.getNodeKind();
            
            Block  block;
            String label;
            
            switch (kind) {
                case OPERAND: {
                    Operand op = ((Node.OfOperand) node).get();
                    
                    if (op.getOperandKind().isEmpty()) {
                        Setting<Style> style = Setting.BASIC_STYLE;
                        Node p = node.getParent();
                        if (p.isSpanOp()) {
                            SpanOp span = ((Node.OfSpanOp) p).getOperator();
                            if (span.isLiteral() || span.isParenthesis()) {
                                style = span.getStyleSetting();
                            }
                        }
                        block = new StyleBlock("empty").set(style);
                    } else {
                        block = node.toBlock(context);
                    }
                    
                    label = getOperandLabel(context, op);
                    break;
                }
                
                case UNARY_OP:
                case BINARY_OP:
                case SPAN_OP:
                case NARY_OP: {
                    Operator op = ((Node.OfOperator) node).getOperator();
                    
                    Symbol.Kind symKind = op.getSymbolKind();
                    switch (symKind) {
                        case BINARY_OP:
                            label = "binary operator";
                            break;
                        case UNARY_OP:
                            UnaryOp unary = (UnaryOp) op;
                            label  = unary.isFunctionLike() ? "function-like " : "";
                            label += (unary.isPrefix() ? "prefix" : "suffix") + " unary operator";
                            break;
                        case SPAN_OP:
                            label = "span operator";
                            break;
                        case NARY_OP:
                            label = "n-ary operator";
                            break;
                        default:
                            assert false : symKind;
                            label = symKind.toLowerCase();
                            break;
                    }
                    
                    String text;
                    if (node.isSpanOp()) {
                        SpanOp span = ((SpanOp) op).getParent();
                        text = span.getLeftName() + " " + span.getRightName();
                    } else {
                        text = op.getName();
                    }
                    
                    block = new StyleBlock(text).set(node.getToken().getStyleSetting());
                    break;
                }
                
                default: {
                    block = new TextBlock("Unknown");
                    label = kind.toLowerCase();
                }
            }
            
            if (label != null) {
                block = Block.join(' ', block, new StyleBlock("(" + label + ")").set(Setting.COMMAND_STYLE));
            }
            return block;
        }
        
        private String getOperandLabel(Context context, Operand op) {
            Operand.Kind opKind = op.getOperandKind();
            switch (opKind) {
                case EMPTY:
                    return "operand";
                case NUMBER:
                    Number.Kind numKind = ((Number) op).getNumberKind();
                    switch (numKind) {
                        case FRACTION:
                            return "real number";
                        case FLOAT:
                            return "floating-point number";
                        default:
                            return numKind.toLowerCase();
                    }
                case STRING_LITERAL:
                    return "string literal";
                case VARIABLE:
                    String value;
                    try {
                        value = getOperandLabel(context, ((Variable) op).evaluate(context));
                    } catch (EvaluationException x) {
                        value = "error evaluating";
                    }
                    return "variable, " + value;
                default:
                    return opKind.toLowerCase();
            }
        }
        
        private List<Block> makeTreePrefixes(Context context, Node root, List<Node> flat) {
            int count = flat.size();
            
            String indent = unquote(replnull(context.getSetting(Setting.EDITOR_INDENT),
                                             Setting.EDITOR_INDENT.getDefaultValue()));
            
            List<StringBuilder> lines = new ArrayList<>(count);
            for (int i = 0; i < count; ++i) {
                lines.add(new StringBuilder(32).append(indent));
            }
            
            fillInPrefixes(root, 0, flat, lines);
            
            @SuppressWarnings("unchecked")
            List<Object> linesAsObj = (List<Object>) (List<?>) lines;
            linesAsObj.replaceAll(o -> new TextBlock(o));
            @SuppressWarnings("unchecked")
            List<Block> linesAsBlocks = (List<Block>) (List<?>) linesAsObj;
            return linesAsBlocks;
        }
        
        private void fillInPrefixes(Node current, int index, List<Node> flat, List<StringBuilder> lines) {
            int nodeCount = flat.size();
            assert 0 <= index && index < nodeCount : index + " of " + nodeCount;
            
            Node.Kind kind = current.getNodeKind();
            switch (kind) {
                case OPERAND:
                    break;
                case UNARY_OP:
                case BINARY_OP:
                case SPAN_OP:
                case NARY_OP:
                    Node.OfOperator opNode = (Node.OfOperator) current;
                    List<Node>    operands = opNode.getOperands();
                    int            opCount = operands.size();
                    boolean      lastFound = false;
                    
                    for (int i = index + 1; i < nodeCount; ++i) {
                        Node node    = flat.get(i);
                        int  indexOf = operands.indexOf(node);
                        
                        if (lastFound && !opNode.isAncestorOf(node)) {
                            break;
                        }
                        
                        String chars;
                        
                        if (0 <= indexOf && indexOf < opCount) {
                            if (indexOf == (opCount - 1)) {
                                chars = "└ ";
                                lastFound = true;
                            } else {
                                chars = "├ ";
                            }
                        } else {
                            if (lastFound) {
                                chars = "  ";
                            } else {
                                chars = "│ ";
                            }
                        }
                        
                        lines.get(i).append(chars);
                    }
                    
                    for (int i = 0; i < opCount; ++i) {
                        Node op = operands.get(i);
                        fillInPrefixes(op, flat.indexOf(op), flat, lines);
                    }
                    
                    break;
            }
        }
    }
}