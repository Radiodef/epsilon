/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import static eps.app.App.*;

final class DebugCommand extends AbstractKeywordCommand {
    DebugCommand() {
        super("debug");
    }
    
    @Override
    public String getHelpText(String cmd) {
        if ("debug".equalsIgnoreCase(cmd)) {
            return "Shows the debug window, if it's not already visible.";
        }
        return null;
    }
    
    @Override
    protected boolean executeImpl(String full, String cmd, String val, Item item) {
        item.addCell(this.newTextArea(full));
        
        if (!val.isEmpty()) {
            item.formatNote("Note: argument '%s' was ignored. The debug command does not take an argument.", val);
        }
        
        App.runOnInterfaceThread(() -> app().getDebugWindow().show());
        return true;
    }
}