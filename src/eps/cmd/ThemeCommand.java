/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.cmd;

import eps.app.*;
import eps.ui.*;
import static eps.app.App.*;

import java.util.regex.*;

final class ThemeCommand extends AbstractKeywordCommand {
    ThemeCommand() {
        super("theme");
    }
    
    @Override
    public String getHelpText(String cmd) {
        if ("theme".equalsIgnoreCase(cmd)) {
            StringBuilder b = new StringBuilder(128);
            b.append("Enter \"theme [name]\" where [name] is the name of a theme "
                   + "to change the visual style of the application.");
            b.append("\n\nEnter \"theme [name] ws\" to preserve the current text "
                   + "sizes while setting the theme.");
            b.append("\n\nThe following themes are available:");
            
            for (String name : Theme.names()) {
                b.append("\n • ").append(name);
            }
            
            return b.toString();
        }
        return null;
    }
    
    private static final Pattern WITHOUT_SIZE_PAT = Pattern.compile("(.*\\S+)\\s+ws\\s*", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    
    @Override
    protected boolean executeImpl(String full, String cmd, String val, Item item) {
        item.addCell(this.newTextArea("%s %s", cmd, val));
        
        boolean ws  = false;
        Matcher mat = WITHOUT_SIZE_PAT.matcher(val);
        if (mat.matches()) {
            ws  = true;
            val = mat.group(1);
        }
        
        Theme theme;
        try {
            theme = Theme.valueOfIgnoreCase(val);
        } catch (IllegalArgumentException x) {
            item.formatNote("'%s' is not the name of a theme. Enter 'help theme' for a list of available themes.", val);
            return true;
        }
        
        theme.setAll(app().getSettings(), ws);
        app().ifSettingsWindowInitialized(Window::refresh);
        return true;
    }
}