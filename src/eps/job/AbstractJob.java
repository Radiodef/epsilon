/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.job;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

public abstract class AbstractJob implements Job {
    private final AtomicReference<JobCancellationException> cancellation = new AtomicReference<>();
    
    private final List<Consumer<? super Job>> onDone = new CopyOnWriteArrayList<>();
    
    private volatile boolean isDone = false;
    
    protected AbstractJob() {
    }
    
    @Override
    public final synchronized void execute() {
        executeImpl();
    }
    
    protected abstract void executeImpl();
    
    @Override
    public final synchronized void done() {
        Throwable caught = null;
        try {
            doneImpl();
        } catch (Throwable x) {
            caught = x;
            throw x;
        } finally {
            isDone = true;
            try {
                for (Consumer<? super Job> action : onDone) {
                    action.accept(this);
                }
            } catch (Throwable x) {
                if (caught != null) {
                    x.addSuppressed(caught);
                }
                throw x;
            }
        }
    }
    
    protected void doneImpl() {
    }
    
    @Override
    public boolean isDone() {
        return isDone;
    }
    
    final JobCancellationException getCancellation() {
        return cancellation.get();
    }
    
    @Override
    public final boolean isCancelled() {
        return cancellation.get() != null;
    }
    
    private enum CancellationAccumulator implements UnaryOperator<JobCancellationException> {
        INSTANCE;
        @Override
        public JobCancellationException apply(JobCancellationException x) {
            if (x == null) {
                return new JobCancellationException();
            }
            JobCancellationException y = new JobCancellationException();
            y.addSuppressed(x);
            return y;
        }
    }
    
    @Override
    public final void cancel() {
        cancellation.updateAndGet(CancellationAccumulator.INSTANCE);
    }
    
    @Override
    public final AbstractJob onDone(Consumer<? super Job> action) {
        Objects.requireNonNull(action, "action");
        onDone.add(action);
        return this;
    }
}