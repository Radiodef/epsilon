/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.job;

import eps.app.*;
import eps.ui.*;
import eps.util.*;

import java.util.*;

public class Worker implements AutoCloseable {
    private final Object monitor = new Object();
    
    private final Watcher watcher;
    private final Thread  thread;
    
    private final Deque<Job> queue = new ArrayDeque<>();
    
    private boolean isOpen = true;
    
    private volatile Job currentJob = null;
    
    private Runnable threadDoneCallback = null;
    
    private final AsyncExecutor async;
    
    public Worker(String name, AsyncExecutor async) {
        this(name, async, Thread.NORM_PRIORITY);
    }
    
    public Worker(String name, AsyncExecutor async, int priority) {
        Log.constructing(getClass());
        this.async = Objects.requireNonNull(async, "async");
        
        thread = App.newThread(this::process);
        thread.setName(name);
        thread.setDaemon(false);
        thread.setPriority(priority);
        configureThread(thread);
        
        watcher = new Watcher();
        thread.start();
    }
    
    protected void configureThread(Thread thread) {
    }
    
    protected final Thread getThread() {
        return thread;
    }
    
    public AsyncExecutor getAsyncExecutor() {
        return async;
    }
    
    private static final ThreadLocal<Worker> LOCAL_WORKER = new ThreadLocal<>();
    
    public static Worker local() {
        return LOCAL_WORKER.get();
    }
    
    public static void sleep(long millis) {
        final long increment = 100;
        while (millis > 0) {
            try {
                Worker.executing();
                long next = Math.min(millis, increment);
                millis -= increment;
                Thread.sleep(next);
            } catch (InterruptedException x) {
                Worker local = local();
                if (local != null) {
                    Job currentJob = local.currentJob;
                    if (currentJob != null) {
                        currentJob.cancel();
                        continue;
                    }
                }
                Log.caught(Worker.class, "sleep", x, false);
            }
        }
    }
    
    public static void executing() {
        Worker local = local();
        if (local != null) {
            Job currentJob = local.currentJob;
            
            if ((currentJob != null) && currentJob.isCancelled()) {
                if (currentJob instanceof AbstractJob) {
                    JobCancellationException cancellation = ((AbstractJob) currentJob).getCancellation();
                    if (cancellation != null) {
                        throw new JobCancellationException(cancellation);
                    }
                }
                throw new JobCancellationException();
            }
        }
    }
    
    public static boolean defer() {
        Worker local = local();
        if (local != null) {
            synchronized (local.monitor) {
                Job currentJob = local.currentJob;

                if ((currentJob != null) && !local.queue.isEmpty()) {
                    local.queue.addLast(currentJob);

                    local.currentJob = null;
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public Job getCurrentJob() {
        synchronized (monitor) {
            return currentJob;
        }
    }
    
    public boolean isOpen() {
        synchronized (monitor) {
            return isOpen;
        }
    }
    
    public void exhaust() {
        if (Thread.currentThread() == thread) {
            return;
        }
        class NotifyJob extends AbstractJob {
            volatile boolean isExausted = false;
            final    Object  monitor    = new Object();
            @Override
            protected void executeImpl() {
                synchronized (monitor) {
                    isExausted = true;
                    monitor.notifyAll();
                }
            }
        }
        NotifyJob job = new NotifyJob();
        synchronized (job.monitor) {
            enqueue(job, true);
            while (!job.isExausted) {
                Misc.wait(job.monitor);
            }
        }
    }
    
    @Override
    public void close() {
        close((Runnable) null);
    }
    
    public void close(Runnable threadDoneCallback) {
        Log.entering(Worker.class, "close");
        
        synchronized (monitor) {
            if (isOpen) {
                this.threadDoneCallback = threadDoneCallback;
                isOpen = false;
                
                if (currentJob != null) {
                    currentJob.cancel();
                }
                queue.forEach(Job::cancel);
                
                monitor.notify();
            }
        }
    }
    
    public void join() {
        Misc.join(thread);
    }
    
    public boolean enqueue(Job job, boolean mandatory) {
        Log.entering(Worker.class, "enqueue");
        
        synchronized (monitor) {
            if (isOpen) {
                queue.addLast(job);
                monitor.notify();
                return true;
            } else if (mandatory) {
                this.execute(job);
                try {
                    async.executeNow(monitor, job::done);
                } catch (AsyncExecutor.AsyncException x) {
                    Log.caught(Worker.class, "from job::done", x, true);
                }
                return true;
            }
            return false;
        }
    }
    
    private void process() {
        Log.entering(Worker.class, "process");
        LOCAL_WORKER.set(this);
        watcher.start();
        
        try {
            executeJobs:
            for (;;) {
                Job job;
                synchronized (monitor) {
                    Job prevJob = currentJob;
                    if (prevJob != null) {
                        try {
                            async.executeNow(monitor, prevJob::done);
                        } catch (AsyncExecutor.AsyncException x) {
                            Log.caught(Worker.class, "from prevJob::done", x, true);
                        }
                        currentJob = null;
                    }
                    
                    for (;;) {
                        if (!isOpen)
                            break executeJobs;
                        if (!queue.isEmpty())
                            break;
                        Log.note(Worker.class, "process", "Waiting.");
                        Misc.wait(monitor);
                    }
                    
                    currentJob = job = queue.removeFirst();
                }
                
                watcher.mark();
                try {
                    this.execute(job);
                } finally {
                    watcher.unmark();
                }
            }
            
            synchronized (monitor) {
                queue.forEach(Job::cancel);
                
                for (Job job : queue) {
                    try {
                        async.executeNow(monitor, job::done);
                    } catch (AsyncExecutor.AsyncException x) {
                        Log.caught(Worker.class, "from job::done", x, true);
                    }
                }
                
                queue.clear();
            }
        } finally {
            LOCAL_WORKER.set(null);
            watcher.stop();
        }
        // This should maybe go in a finally block.
        synchronized (monitor) {
            Log.note(Worker.class, "process", "Worker terminated.");
            
            if (threadDoneCallback != null) {
                threadDoneCallback.run();
                threadDoneCallback = null;
            }
        }
    }
    
    private void execute(Job job) {
        try {
            Log.note(Worker.class, "process", "Executing a job.");
            job.execute();
        } catch (JobCancellationException x) {
            Log.caught(Worker.class, "Job cancelled.", x, true);
        } catch (Throwable x) {
            Log.caught(Worker.class, "Unexpected exception while executing " + job + ".", x, false);
        }
    }
    
    // Note: we can't use AsyncExecutor.executeNow(...) because
    //       we need to relinquish this.monitor while waiting.
    //       (fixed -- see AsyncExecutor.executeNow(monitor, task))
    @Deprecated // since AsyncExecutor.executeNow(monitor, task)
    private final class SyncTask implements Runnable {
        private final Runnable actualTask;
        
        private boolean actualTaskComplete = false;
        
        private SyncTask(Runnable actualTask) {
            this.actualTask = actualTask;
        }
        
        void sendAndWaitForCompletion() {
            Log.entering(SyncTask.class, "sendAndWaitForCompletion");
            
            synchronized (monitor) {
                async.execute(this);
                
                while (!actualTaskComplete) {
                    Misc.wait(monitor);
                }
            }
        }
        
        @Override
        public void run() {
            Log.entering(SyncTask.class, "run");
            
            synchronized (monitor) {
                actualTask.run();
                actualTaskComplete = true;
                monitor.notify();
            }
        }
    }
    
    private final class Watcher implements Runnable {
        private final   long   interval;
        private final   Thread thread;
        
        private boolean isRunning = false;
        private boolean isMarked  = false;
        private long    mark      = -1;
        private int     count     = 0;
        
        private long timeout = Long.MAX_VALUE;
        
        private Watcher() {
            interval = 1000L;
            thread   = App.newThread(this);
            thread.setName(Worker.this.thread.getName() + ".Watcher");
            thread.setDaemon(true);
            thread.setPriority(Thread.MIN_PRIORITY);
            
            App.whenAvailable(app -> {
                Settings s = app.getSettings();
                s.addChangeListener(Setting.WORKER_HALTING_TIMEOUT,
                                    timeout -> this.timeout = (int) timeout);
                this.timeout = (int) s.get(Setting.WORKER_HALTING_TIMEOUT);
            });
        }
        
        synchronized void start() {
            isRunning = true;
            thread.start();
        }
        
        synchronized void stop() {
            isRunning = false;
            notify();
        }
        
        synchronized void mark() {
            isMarked = true;
            mark     = System.currentTimeMillis();
            count    = 0;
            notify();
        }
        
        synchronized void unmark() {
            isMarked = false;
        }
        
        @Override
        public synchronized void run() {
            do {
                if (isMarked) {
                    long current = System.currentTimeMillis();
                    if ((current - mark) >= timeout) {
                        workerHalting();
                        ++count;
                        mark = current;
                    }
                }
                Misc.wait(this, interval);
            } while (isRunning);
        }
        
        private void workerHalting() {
            String              message = Worker.this.thread.getName() + " may be halting";
            StackTraceElement[] trace   = Worker.this.thread.getStackTrace();
            WorkerHalting       x       = new WorkerHalting(message, trace);
            Log.caught(Watcher.class, "workerHalting", x, true);
            
            if (count == 0) {
                App.whenAvailable(app -> app.addItemOnInterfaceThread(item -> {
                    TextAreaCell cell = app.getComponentFactory().newTextArea();
                    cell.followTextStyle(Setting.NOTE_STYLE);
                    
//                    StringBuilder b = new StringBuilder(128);
//                    b.append(message);
//                    for (StackTraceElement e : trace) {
//                        b.append("\n  at ").append(e);
//                    }
//                    b.append("\nsee the log for additional periodic stack traces");
                    
                    cell.setText("A worker thread may be halting. See the log for periodic stack traces.");
                    item.addCell(cell);
                }));
            }
        }
    }
    
    private static final class WorkerHalting extends Throwable {
        private WorkerHalting(String message, StackTraceElement[] trace) {
            super(message);
            setStackTrace(trace);
        }
    }
}