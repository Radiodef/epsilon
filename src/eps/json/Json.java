/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import eps.io.*;
import static eps.util.Literals.*;

import java.util.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.charset.StandardCharsets;

public final class Json {
    private Json() {
    }
    
    /*
    private static void test() {
        System.out.println(java.math.BigInteger.ONE.equals(new java.math.BigInteger("1")));
        Test t = new Test(100, "foo", true, "hello, \"world\"!", new Test.AdaptedPoint(6, Math.PI), "A", "B", "C", "1", "2", "3", java.util.concurrent.TimeUnit.SECONDS);
        System.out.println("t = " + t);
        
        String json = stringify(t, true);
        System.out.println(json);
        
        JsonValue val = new JsonParser().parse(json);
        System.out.println(val);
        
        t = (Test) objectify(json);
        System.out.println(t);
    }
    */
    
    public static String stringify(Object obj, boolean multiLine) {
        return new JsonJavaConverter().toJson(obj).toString(multiLine);
    }
    
    public static Object objectify(String json) {
        return new JsonJavaConverter().fromJson(new JsonParser().parse(json));
    }
    
    public static Object thru(Object obj, boolean multiline) {
        String str = stringify(obj, multiline);
        Object res = objectify(str);
        return res;
    }
    
    public static JsonValue parse(String json) {
        return new JsonParser().parse(json);
    }
    
    public static Object fromFile(Path path) throws IOException {
        byte[] bytes = FileSystem.instance().readAllBytes(path);
        return fromBytes(bytes);
    }
    
    public static Object fromBytes(byte[] bytes) {
        return objectify(new String(bytes, StandardCharsets.UTF_8));
    }
    
    public static byte[] toBytes(Object obj, boolean multiLine) {
        return stringify(obj, multiLine).getBytes(StandardCharsets.UTF_8);
    }
    
    public static String toString(JsonValue json) {
        return (json == null) ? NULL : json.toString();
    }
    
    public static boolean isNull(JsonValue json) {
        return json == null || json.isNull();
    }
    
    public static boolean toBoolean(JsonValue json) {
        if (json == null)
            return false;
        if (json.isNull())
            return false;
        if (json instanceof JsonBoolean)
            return ((JsonBoolean) json).booleanValue();
        if (json instanceof JsonNumber)
            return ((JsonNumber) json).doubleValue() != 0;
        return true;
    }
    
    public static String quote(Object obj) {
        return "\"" + obj + "\"";
    }
    
    /*
    public static String unquote(String s) {
        String t = s.trim();
        if (t.startsWith("\"") && t.endsWith("\"")) {
            return t.substring(1, t.length() - 1);
        } else {
            throw Errors.newIllegalArgument(s);
        }
    }
    */
    
    public static final String CLASS = "$class";
    public static final String REF   = "$ref";
    public static final String PROPS = "$props";
    public static final String KEY   = "$key";
    public static final String VAL   = "$val";
    public static final String ENUM  = "$enum";
    public static final String NAME  = "$name";
    public static final String ELEMS = "$elems";
    
    public static final Set<String> SPECIAL_FIELDS =
        Collections.unmodifiableSet(SetOf(Json.CLASS,
                                           Json.REF,
                                           Json.PROPS,
                                           Json.ENUM,
                                           Json.NAME,
                                           Json.KEY,
                                           Json.VAL,
                                           Json.ELEMS));
    
    public static final String INDENT = "  ";
    
    public static final String NULL  = "null";
    public static final String TRUE  = "true";
    public static final String FALSE = "false";
    
    public static final List<String> LITERALS = Collections.unmodifiableList(ListOf(NULL, TRUE, FALSE));
}