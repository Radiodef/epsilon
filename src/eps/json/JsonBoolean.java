/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

public final class JsonBoolean extends AbstractJsonValue {
    private final boolean booleanValue;
    
    public JsonBoolean(boolean booleanValue) {
        this.booleanValue = booleanValue;
    }
    
    public static JsonBoolean valueOf(boolean b) {
        return b ? TRUE : FALSE;
    }
    
    public boolean booleanValue() {
        return booleanValue;
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, int indents, boolean multiLine, boolean indentFirst) {
        return indentIf(b, indents, multiLine && indentFirst).append(booleanValue ? Json.TRUE : Json.FALSE);
    }
    
    @Override
    public int hashCode() {
        return Boolean.hashCode(booleanValue);
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof JsonBoolean) && ((JsonBoolean) obj).booleanValue == this.booleanValue;
    }
    
    public static final JsonBoolean TRUE  = new JsonBoolean(true);
    public static final JsonBoolean FALSE = new JsonBoolean(false);
}