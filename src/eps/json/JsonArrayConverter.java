/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.lang.reflect.*;

/**
 * Quick note: this should not be used for TreeSets with a custom Comparator,
 * at least not at the moment. The Comparator will not be retained through
 * serialization.
 * 
 * @author David
 */
final class JsonArrayConverter {
    private JsonArrayConverter() {
    }
    
    static final class ConstructorInfo {
        final Class<?>       type;
        final Constructor<?> ctor;
        final boolean        isPublic;
        
        private ConstructorInfo(Class<?> type) {
            this.type = Objects.requireNonNull(type, "type");
            
            if (!Modifier.isPublic(type.getModifiers())) {
                this.isPublic = false;
                this.ctor     = null;
            } else {
                Constructor<?> ctor;
                try {
                    ctor = type.getConstructor(Collection.class);
                } catch (ReflectiveOperationException x) {
                    // fail
                    ctor = null;
                }
                if (ctor != null) {
                    this.isPublic = true;
                    this.ctor     = ctor;
                } else {
                    this.isPublic = false;
                    this.ctor     = null;
                }
            }
        }
        
        private static final ToString<ConstructorInfo> TO_STRING =
            ToString.builder(ConstructorInfo.class)
                    .add("type",     i -> i.type)
                    .add("ctor",     i -> i.ctor)
                    .add("isPublic", i -> i.isPublic)
                    .build();
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
    }
    
    private static final Map<Class<?>, ConstructorInfo> CONSTRUCTORS =
        new ConcurrentHashMap<>();
    
    private static final Function<Class<?>, ConstructorInfo> NEW_CONSTRUCTOR_INFO =
        ConstructorInfo::new;
    
    static void resetCache() {
        CONSTRUCTORS.clear();
        CONSTRUCTORS.put(List.class,       getConstructorInfo(ArrayList.class));
        CONSTRUCTORS.put(Set.class,        getConstructorInfo(LinkedHashSet.class));
        CONSTRUCTORS.put(SortedSet.class,  getConstructorInfo(TreeSet.class));
        CONSTRUCTORS.put(Collection.class, getConstructorInfo(List.class));
    }
    static {
        resetCache();
    }
    
    static ConstructorInfo getConstructorInfo(Class<?> clazz) {
        ConstructorInfo info = CONSTRUCTORS.computeIfAbsent(clazz, NEW_CONSTRUCTOR_INFO);
        return info;
    }
    
    static Constructor<?> getConstructor(Class<?> clazz) {
        ConstructorInfo info = getConstructorInfo(clazz);
        
        if (info.isPublic) {
            return info.ctor;
        } else {
            throw Errors.newIllegalArgument(clazz);
        }
    }
    
    static boolean isConvertible(Object obj) {
        Objects.requireNonNull(obj, "obj");
        return JsonArray.isConvertible(obj) && getConstructorInfo(obj.getClass()).isPublic;
    }
}