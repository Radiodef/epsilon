/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.util.Literals.*;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;

public interface JsonObjectAdapter<T> {
    Class<T> getType();
    Map<?, ?> toMap(T obj);
    T toObject(Map<?, ?> map);
    
    static <T> JsonObjectAdapter<T> create(Class<T>                                 type,
                                           Function<? super T, ? extends Map<?, ?>> serializer,
                                           Function<? super Map<?, ?>, ? extends T> deserializer) {
        return new JsonObjectAdapter.FromFunction<>(type, serializer, deserializer);
    }
    static <T> JsonObjectAdapter<T> put(Class<T>                                 type,
                                        Function<? super T, ? extends Map<?, ?>> serializer,
                                        Function<? super Map<?, ?>, ? extends T> deserializer) {
        return put(create(type, serializer, deserializer));
    }
    
    final class FromFunction<T> implements JsonObjectAdapter<T> {
        private final Class<T>                                 type;
        private final Function<? super T, ? extends Map<?, ?>> serializer;
        private final Function<? super Map<?, ?>, ? extends T> deserializer;
        
        public FromFunction(Class<T>                                 type,
                            Function<? super T, ? extends Map<?, ?>> serializer,
                            Function<? super Map<?, ?>, ? extends T> deserializer) {
            this.type         = Objects.requireNonNull(type,         "type");
            this.serializer   = Objects.requireNonNull(serializer,   "serializer");
            this.deserializer = Objects.requireNonNull(deserializer, "deserializer");
        }
        
        @Override
        public Class<T> getType() {
            return type;
        }
        
        @Override
        public Map<?, ?> toMap(T obj) {
            return serializer.apply(obj);
        }
        
        @Override
        public T toObject(Map<?, ?> map) {
            return deserializer.apply(map);
        }
    }
    
    static <T> JsonObjectAdapter<T> put(JsonObjectAdapter<T> ja) {
        return JsonObjectAdapterPrivate.put(ja);
    }
    
    static <T> JsonObjectAdapter<T> get(Class<T> type) {
        return JsonObjectAdapterPrivate.get(type);
    }
    
    enum StackTraceAdapter implements JsonObjectAdapter<StackTraceElement> {
        INSTANCE;
        
        public static final String CLASS_NAME  = "className";
        public static final String METHOD_NAME = "methodName";
        public static final String FILE_NAME   = "fileName";
        public static final String LINE_NUMBER = "lineNumber";
        
        @Override
        public Class<StackTraceElement> getType() {
            return StackTraceElement.class;
        }
        
        @Override
        public Map<?, ?> toMap(StackTraceElement e) {
            return LinkedHashMapOf(CLASS_NAME,  e.getClassName(),
                                   METHOD_NAME, e.getMethodName(),
                                   FILE_NAME,   e.getFileName(),
                                   LINE_NUMBER, e.getLineNumber());
                                   
        }
        
        @Override
        public StackTraceElement toObject(Map<?, ?> m) {
            return new StackTraceElement((String)  m.get(CLASS_NAME),
                                         (String)  m.get(METHOD_NAME),
                                         (String)  m.get(FILE_NAME),
                                         (Integer) m.get(LINE_NUMBER));
        }
    }
}

final class JsonObjectAdapterPrivate {
    private JsonObjectAdapterPrivate() {
    }
    
    private static final Map<Class<?>, JsonObjectAdapter<?>> REGISTRY = new ConcurrentHashMap<>();
    
    static void resetRegistry() {
        REGISTRY.clear();
        put(JsonObjectAdapter.create(ConcurrentHashMap.class,
                                     concurrentHashMap -> concurrentHashMap,
                                     ConcurrentHashMap::new));
        put(JsonObjectAdapter.StackTraceAdapter.INSTANCE);
    }
    static {
        resetRegistry();
    }
    
    @SuppressWarnings("unchecked")
    static <T> JsonObjectAdapter<T> put(JsonObjectAdapter<T> ja) {
        Objects.requireNonNull(ja, "ja");
        return (JsonObjectAdapter<T>) REGISTRY.put(ja.getType(), ja);
    }
    
    @SuppressWarnings("unchecked")
    static <T> JsonObjectAdapter<T> get(Class<T> type) {
        Objects.requireNonNull(type, "type");
        return (JsonObjectAdapter<T>) REGISTRY.get(type);
    }
}