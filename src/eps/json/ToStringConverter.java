/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;

import java.math.*;
import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;

public interface ToStringConverter<T> {
    Class<T> getType();
    String serialize(T object);
    T deserialize(String string);
    
    List<ToStringConverter<?>> DEFAULT_CONVERTERS = JsonSerializerPrivate.DEFAULT_SERIALIZERS;
    
    static <T> ToStringConverter<T> create(Class<T>                              type,
                                           Function<? super T, ? extends String> serializer,
                                           Function<? super String, ? extends T> deserializer) {
        return new ToStringConverter.FromFunction<>(type, serializer, deserializer);
    }
    
    final class FromFunction<T> implements ToStringConverter<T> {
        private final Class<T>                              type;
        private final Function<? super T, ? extends String> serializer;
        private final Function<? super String, ? extends T> deserializer;
        
        public FromFunction(Class<T>                              type,
                            Function<? super T, ? extends String> serializer,
                            Function<? super String, ? extends T> deserializer) {
            this.type         = Objects.requireNonNull(type,         "type");
            this.serializer   = Objects.requireNonNull(serializer,   "serializer");
            this.deserializer = Objects.requireNonNull(deserializer, "deserializer");
        }
        
        @Override
        public Class<T> getType() {
            return type;
        }
        
        @Override
        public String serialize(T obj) {
            return serializer.apply(obj);
        }
        
        @Override
        public T deserialize(String str) {
            return deserializer.apply(str);
        }
    }
    
    static <T> ToStringConverter<T> put(ToStringConverter<T> js) {
        return JsonSerializerPrivate.put(js);
    }
    
    static <T> ToStringConverter<T> get(Class<T> type) {
        return JsonSerializerPrivate.get(type);
    }
    
    static String toString(Object obj) {
        return JsonSerializerPrivate.toString(obj);
    }
    
    static <T> T toObject(Class<T> type, String str) {
        return JsonSerializerPrivate.toObject(type, str);
    }
}

final class JsonSerializerPrivate {
    private JsonSerializerPrivate() {}
    
    private static <T> ToStringConverter<T> createSimpleConverter(Class<T> clazz) {
        Function<T, String> toString = createInstanceFunction(clazz, "toString", String.class);
        Function<String, T> ctor     = createConstructorFunction(clazz, String.class);
        return ToStringConverter.create(clazz, toString, ctor);
    }
    
    static final List<ToStringConverter<?>> DEFAULT_SERIALIZERS;
    static {
        List<ToStringConverter<?>> defaults = new ArrayList<>();
        
        for (Class<?> simple : ListOf(Byte.class,
                                      Short.class,
                                      Integer.class,
                                      Long.class,
                                      Float.class,
                                      Double.class,
                                      BigInteger.class,
                                      BigDecimal.class)) {
            defaults.add(createSimpleConverter(simple));
        }
        
        defaults.add(ToStringConverter.create(
            Misc.WILD_CLASS,
            Class::getName,
            s -> {
                try {
                    return Class.forName(s);
                } catch (ClassNotFoundException x) {
                    throw new IllegalArgumentException(s, x);
                }
            }));
        
        defaults.add(ToStringConverter.create(
            Character.class,
            Object::toString,
            s -> {
                if (s.length() == 1) {
                    return s.charAt(0);
                }
                throw Errors.newIllegalArgument(s);
            }));
        
        DEFAULT_SERIALIZERS = Collections.unmodifiableList(defaults);
    }
    
    static final ConcurrentMap<Class<?>, ToStringConverter<?>> REGISTRY = new ConcurrentHashMap<>();
    static {
        for (ToStringConverter<?> ser : DEFAULT_SERIALIZERS) {
            REGISTRY.put(ser.getType(), ser);
        }
    }
    
    @SuppressWarnings("unchecked")
    static <T> ToStringConverter<T> put(ToStringConverter<T> js) {
        Objects.requireNonNull(js, "js");
        js = (ToStringConverter<T>) REGISTRY.put(js.getType(), js);
        return js;
    }
    
    @SuppressWarnings("unchecked")
    static <T> ToStringConverter<T> get(Class<T> type) {
        Objects.requireNonNull(type, "type");
        ToStringConverter<T> js = (ToStringConverter<T>) REGISTRY.get(type);
        return js;
    }
    
    @SuppressWarnings("unchecked")
    static <T> String toString(T obj) {
        Objects.requireNonNull(obj, "obj");
        ToStringConverter<T> js = (ToStringConverter<T>) get(obj.getClass());
        
        if (js != null) {
            return js.serialize(obj);
        }
        
        for (ToStringConverter<?> v : REGISTRY.values()) {
            if (v.getType().isInstance(obj)) {
                return ((ToStringConverter<? super T>) v).serialize(obj);
            }
        }
        
        return null;
    }
    
    static <T> T toObject(Class<T> type, String str) {
        Objects.requireNonNull(str, "str");
        ToStringConverter<T> js = get(type);
        
        if (js != null) {
            return js.deserialize(str);
        }
        
        for (ToStringConverter<?> v : REGISTRY.values()) {
            if (type.isAssignableFrom(v.getType())) {
                return type.cast(v.deserialize(str));
            }
        }
        
        return null;
    }
}