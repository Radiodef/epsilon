/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;

import java.util.*;
import java.util.Map.*;
import java.util.function.*;

public abstract class JsonDestructurer {
    protected abstract String mapKey(String key);
    protected abstract Iterable<String> keys();
    
    protected int minimumSize() {
        return 0;
    }
    
    protected int maximumSize() {
        return Integer.MAX_VALUE;
    }
    
    protected boolean isOptional(String key) {
        return false;
    }
    
    public Map<String, JsonValue> destructure(String text) {
        return this.destructure(Json.parse(text), () -> text);
    }
    
    public Map<String, JsonValue> destructure(JsonValue json) {
        return this.destructure(json, () -> Json.toString(json));
    }
    
    private Map<String, JsonValue> destructure(JsonValue json, Supplier<String> text) {
        Objects.requireNonNull(json, "json");
        if (!(json instanceof JsonObject)) {
            throw Errors.newIllegalArgumentF("not a JSON object: \"%s\"", text.get());
        }
        
        JsonObject obj = (JsonObject) json;
        
        int size = obj.count();
        int min  = minimumSize();
        int max  = maximumSize();
        if (size < min || max < size) {
            throw Errors.newIllegalArgumentF("found %s properties; required %s-%s; in \"%s\"", size, min, max, text.get());
        }
        
        Map<String, JsonValue> map = new LinkedHashMap<>();
        
        for (Entry<String, JsonValue> e : obj.asMap().entrySet()) {
            String from = e.getKey();
            String to   = mapKey(from);
            
            if (to == null) {
                throw Errors.newIllegalArgumentF("found erroneous key \"%s\" in \"%s\"", from, text.get());
            }
            
            JsonValue old = map.put(to, e.getValue());
            
            // Note: This will only happen if mapKey(...) has synonymns.
            //       For example, we were considering case-insensitive
            //       property names and this method was fed an object like
            //       { "A": 0, "a": 1 }.
            if (old != null) {
                throw Errors.newIllegalArgumentF("found duplicate key for \"%s\" in \"%s\"", to, text.get());
            }
        }
        
        for (String key : keys()) {
            if (!map.containsKey(key) && !isOptional(key)) {
                throw Errors.newIllegalArgumentF("missing property \"%s\" in \"%s\"", key, text.get());
            }
        }
        
        return map;
    }
    
    protected static TreeMap<String, String> TreeMapOf(String... entries) {
        return JsonDestructurer.TreeMapOf((Comparator<? super String>) null, entries);
    }
    
    protected static TreeMap<String, String> TreeMapOf(Comparator<? super String> comp, String... entries) {
        int length = (entries == null) ? 0 : entries.length;
        if ((length & 1) == 1) {
            throw Errors.newIllegalArgumentF("odd number of entry elements %d", length);
        }
        
        TreeMap<String, String> map = (comp == null) ? new TreeMap<>() : new TreeMap<>(comp);
        
        for (int i = 0; i < length;) {
            map.put(Objects.requireNonNull(entries[i++], "key"),
                    Objects.requireNonNull(entries[i++], "val"));
        }
        
        return map;
    }
}