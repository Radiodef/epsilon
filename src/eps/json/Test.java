/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.util.Literals.*;

import java.util.*;
import java.math.*;

/*
@JsonSerializable
class Test {
    @JsonProperty
    private final int num;
    @JsonProperty
    private final String name;
    @JsonProperty
    private final Object nil;
    @JsonProperty
    private final boolean b;
    @JsonProperty
    private final Nested message;
    @JsonProperty
    private final Nested message2;
    @JsonProperty
    private final AdaptedPoint pt;
    @JsonProperty
    private final List<?> list;
    
    Test(int num, String name, boolean b, String message, AdaptedPoint pt, Object... elems) {
        this.num      = num;
        this.name     = name;
        this.nil      = null;
        this.b        = b;
        this.message  = new Nested(message);
        this.message2 = this.message;
        this.pt       = pt;
        this.list     = Arrays.asList(elems);
    }
    
    @JsonConstructor
    Test(int num, String name, Object nil, boolean b, Nested message, Nested message2, AdaptedPoint pt, List<?> list) {
        this.num      = num;
        this.name     = name;
        this.nil      = nil;
        this.b        = b;
        this.message  = message;
        this.message2 = message2;
        this.pt       = pt;
        this.list     = list;
    }
    
    @Override
    public String toString() {
        return String.format("Test{num = %d, name = %s, nil = %s, b = %s, message = %s, pt = %s, list = %s}",
                             num, name, nil, b, message, pt, list);
    }
    
    @JsonSerializable
    static class Nested {
        @JsonProperty
        final String message;
        
        @JsonConstructor
        Nested(String message) {
            this.message = message;
        }
        
        @Override
        public String toString() {
            return "(" + message + ")";
        }
    }
    
    static class AdaptedPoint {
        BigDecimal x, y;
        
        AdaptedPoint(double x, double y) {
            this(new BigDecimal(x), new BigDecimal(y));
        }
        
        AdaptedPoint(BigDecimal x, BigDecimal y) {
            this.x = x;
            this.y = y;
        }
        
        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }
    }
    
    static {
        JsonObjectAdapter<AdaptedPoint> ja =
            JsonObjectAdapter.create(
                AdaptedPoint.class,
                ap -> MapOf("x", ap.x, "y", ap.y),
                map -> new AdaptedPoint((BigDecimal) map.get("x"), (BigDecimal) map.get("y")));
        
        JsonObjectAdapter.put(ja);
    }
}
*/