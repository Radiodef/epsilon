/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import java.util.*;
import static java.lang.Character.*;

public final class JsonString extends AbstractJsonValue {
    private final String chars;
    
    public JsonString(String chars) {
        this.chars = Objects.requireNonNull(chars, "chars");
    }
    
    public static JsonString valueOf(Object obj) {
        return new JsonString(String.valueOf(obj));
    }
    
    public String getChars() {
        return chars;
    }
    
    public static String getChars(JsonValue val) {
        if (val instanceof JsonString)
            return ((JsonString) val).getChars();
        else {
            return Json.toString(val);
        }
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, int indents, boolean multiLine, boolean indentFirst) {
        indentIf(b, indents, multiLine && indentFirst);
        
        b.append('"');
        
        String chars = this.chars;
        int    len   = chars.length();
        
        b.ensureCapacity(b.length() + len);
        
        for (int i = 0; i < len; ++i) {
            char   ch  = chars.charAt(i);
            String esc = escape(ch);
            
            if (esc != null) {
                b.append(esc);
            } else {
                b.append(ch);
            }
        }
        
        return b.append('"');
    }
    
    public static String createEscape(char c) {
        char[] chars = new char[6];
        chars[0] = '\\';
        chars[1] = 'u';
        
        for (int i = 3; i >= 0; --i) {
            int nibble = (c >>> (4*i)) & 0xF;
            
            if (nibble < 10) {
                chars[5 - i] = (char) ('0' + nibble);
            } else {
                chars[5 - i] = (char) ('A' + (nibble - 10));
            }
        }
        
        return new String(chars);
    }
    
    private static final String   QUOTE_ESCAPE     = createEscape('"');
    private static final String   BACKSLASH_ESCAPE = createEscape('\\');
    private static final String[] CONTROL_ESCAPES  = new String[0x001F + 1];
    
    public static final List<String> ESCAPED_CHARACTERS;
    
    static {
        Arrays.setAll(CONTROL_ESCAPES, i -> createEscape((char) i));
        
        List<String> escapedChars = new ArrayList<>(2 + CONTROL_ESCAPES.length);
        escapedChars.add("\"");
        escapedChars.add("\\");
        for (char c = 0x0; c <= 0x1F; ++c) {
            escapedChars.add(Character.toString(c));
        }
        ESCAPED_CHARACTERS = Collections.unmodifiableList(escapedChars);
    }
    
    public static String escape(char c) {
        if (c == '"') {
            return QUOTE_ESCAPE;
        }
        if (c == '\\') {
            return BACKSLASH_ESCAPE;
        }
        if (c <= 0x1F) {
            return CONTROL_ESCAPES[c];
        }
        return null;
    }
    
    public static String escape(String s) {
        StringBuilder b = null;
        
        int prev = 0;
        int len  = s.length();
        for (int i = 0; i < len; ++i) {
            char   c = s.charAt(i);
            String e = escape(c);
            if (e != null) {
                if (b == null) {
                    b = new StringBuilder(2 * len);
                }
                b.append(s, prev, i).append(e);
                prev = i + 1;
            }
        }
        
        if (b != null) {
            b.append(s, prev, s.length());
            return b.toString();
        }
        return s;
    }
    
    private static boolean isHexChar(int c) {
        return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
    }
    
    public static boolean isHexadecimalEscape(String chars, int i) {
        int len = chars.length();
        if (i < len && chars.codePointAt(i++) == '\\') {
            if (i < len && chars.codePointAt(i++) == 'u') {
                int end = i + 4;
                if (end <= len) {
                    for (; i < end; ++i) {
                        if (!isHexChar(chars.codePointAt(i))) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    public static JsonString unescape(String chars) {
        return JsonString.valueOf(unescapeChars(chars));
    }
    
    public static String unescapeChars(String chars) {
        Objects.requireNonNull(chars, "chars");
        
        StringBuilder buf = null;
        
        int len  = chars.length();
        int prev = 0;
        
        for (int i = prev, ch; i < len;) {
            ch = chars.codePointAt(i);
            
            if (ch == '\\') {
                if (buf == null) {
                    buf = new StringBuilder(len);
                }
                
                if (isHexadecimalEscape(chars, i)) {
                    buf.append(chars, prev, i);
                    i += 2;
                    int code = 0;
                    
                    for (int d = 0; d < 4; ++d) {
                        int digit = chars.codePointAt(i + d);
                        int nibble;
                        if ('0' <= digit && digit <= '9') {
                            nibble = digit - '0';
                        } else if ('a' <= digit && digit <= 'f') {
                            nibble = 10 + (digit - 'a');
                        } else if ('A' <= digit && digit <= 'F') {
                            nibble = 10 + (digit - 'A');
                        } else {
                            throw new AssertionError(chars);
                        }
                        code |= nibble << (4*(3 - d));
                    }
                    
                    buf.appendCodePoint(code);
                    prev = (i += 4);
                }
                
                int ip1 = i + 1;
                if (ip1 == len) {
                    buf.append(chars, prev, i);
                    break;
                }
                
            } else {
                i += charCount(ch);
            }
        }
        
        if (buf != null) {
            if (prev != len) {
                buf.append(chars, prev, len);
            }
            return buf.toString();
        } else {
            return chars;
        }
    }
    
    @Override
    public int hashCode() {
        return chars.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof JsonString) && chars.equals(((JsonString) obj).chars);
    }
}