/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.util.Errors.*;

import java.util.*;
import static java.lang.Character.*;

final class JsonParser {
    JsonParser() {
    }
    
    JsonValue parse(String json) {
        Objects.requireNonNull(json, "json");
        
        int       len = json.length();
        int[]     end = new int[1];
        JsonValue val = parseRange(json, 0, len, end);
        
        int end0 = end[0];
        if (end0 != len) {
            if (findWhitespaceEnd(json, end0, len) != len) {
                throw newIllegalArgumentF("found disconnected trailing tokens '%s'", json.substring(end0));
            }
        }
        
        return val;
    }
    
    private JsonValue parseRange(String json, int start, int end, int[] valueEndIndex) {
        start = findWhitespaceEnd(json, start, end);
        
        if (start == end) {
            throw newIllegalArgument("empty String; nothing to parse");
        }
        
        int code0 = json.codePointAt(start);
        switch (code0) {
            case '{':
                return parseJsonObject(json, start, end, valueEndIndex);
            case '[':
                return parseJsonArray(json, start, end, valueEndIndex);
            case '"':
                return parseJsonString(json, start, end, valueEndIndex);
        }
        
        JsonValue v;
        if ((v = tryParseLiteral(json, start, end, valueEndIndex)) != null)
            return v;
        if ((v = tryParseJsonNumber(json, start, end, valueEndIndex)) != null)
            return v;
        
        throw newIllegalArgumentF("unable to parse range '%s' starting at '%s'", json.substring(start, end), json.substring(start));
    }
    
    private final String[]    LITERAL_KEYS = {Json.NULL,      Json.TRUE,        Json.FALSE};
    private final JsonValue[] LITERAL_VALS = {JsonValue.NULL, JsonBoolean.TRUE, JsonBoolean.FALSE};
    
    private JsonValue tryParseLiteral(String json, int start, int end, int[] litEndIndex) {
        String[] lits  = LITERAL_KEYS;
        int      count = lits.length;
        for (int i = 0; i < count; ++i) {
            String lit = lits[i];
            int    len = lit.length();
            if (json.regionMatches(false, start, lit, 0, len)) {
                litEndIndex[0] = start + len;
                return LITERAL_VALS[i];
            }
        }
        return null;
    }
    
    private JsonNumber tryParseJsonNumber(String json, int start, int end, int[] numEndIndex) {
        int numStart = start;
        int numEnd   = -1;
        
        numSearch:
        for (int i = start, ch; i <= end; i += charCount(ch)) {
            if (i == end) {
                numEnd = end;
                break;
            }
            ch = json.codePointAt(i);
            if (isWhitespace(ch)) {
                numEnd = i;
                break;
            }
            switch (ch) {
                case ',':
                case '[':
                case ']':
                case '{':
                case '}':
                case '"':
                case ':':
                    numEnd = i;
                    break numSearch;
            }
        }
        
        if (numEnd != -1) {
            int code0 = json.codePointAt(numStart + 0);
            do {
                // parseDouble accepts a form like +1 or +2.5, but JSON does not
                if (code0 == '+') {
                    break;
                }
                // this is just setup for the next condition
                int len  = numEnd - numStart;
                int lead = code0;
                if (lead == '-') {
                    if (len > 1) {
                        lead = json.codePointAt(numStart + 1);
                    }
                }
                // parseDouble accepts leading 0s, but JSON does not
                if (lead == '0') {
                    int next = (code0 == '-' ? 2 : 1);
                    if (len > next) {
                        int code1 = json.codePointAt(numStart + next);
                        if (isDigit(code1)) {
                            break;
                        }
                    }
                }
                try {
                    double d = Double.parseDouble(json.substring(numStart, numEnd));
                    numEndIndex[0] = numEnd;
                    return JsonNumber.valueOf(d);
                } catch (NumberFormatException x) {
                }
            } while (false);
        }
        
        return null;
    }
    
    private JsonObject parseJsonObject(String json, int start, int end, int[] objEndIndex) {
        assert json.codePointAt(start) == '{' : json;
        
        end = findBracketEnd(json, '{', '}', start, end);
        if (end < 0) {
            throw newIllegalArgumentF("missing end to object starting at '%s'", json.substring(start));
        }
        
        JsonObject obj = new JsonObject();
        
        int[] valEndIndex = new int[1];
        
        int last = end - 1;
        for (int i = findWhitespaceEnd(json, start + 1, last); i < last;) {
            int code = json.codePointAt(i);
            if (code != '"') {
                throw newIllegalArgumentF("should be a quote at '%s'", json.substring(i));
            }
            
            int nameEnd = findQuoteEnd(json, i, last);
            if (nameEnd < 0) {
                throw newIllegalArgumentF("no end to name starting at '%s'", json.substring(i));
            }
            
            String name = JsonString.unescapeChars(json.substring(i + 1, nameEnd - 1));
            
            int colon = findWhitespaceEnd(json, nameEnd, last);
            if (colon == last || json.codePointAt(colon) != ':') {
                throw newIllegalArgumentF("missing value starting at '%s'", json.substring(nameEnd));
            }
            
            int valStart = findWhitespaceEnd(json, colon + 1, last);
            if (valStart == last) {
                throw newIllegalArgumentF("missing value starting at '%s'", json.substring(colon + 1));
            }
            
            JsonValue val = parseRange(json, valStart, last, valEndIndex);
            obj.put(name, val);
            
            i = findWhitespaceEnd(json, valEndIndex[0], last);
            if (i == last)
                break;
            
            if (json.codePointAt(i) != ',') {
                throw newIllegalArgumentF("expected ',' starting at '%s'", json.substring(i));
            }
            
            i = findWhitespaceEnd(json, i + 1, last);
        }
        
        objEndIndex[0] = end;
        return obj;
    }
    
    private JsonArray parseJsonArray(String json, int start, int end, int[] arrEndIndex) {
        assert json.codePointAt(start) == '[' : json;
        
        end = findBracketEnd(json, '[', ']', start, end);
        if (end < 0) {
            throw newIllegalArgumentF("missing end to array starting at '%s'", json.substring(start));
        }
        
        JsonArray arr = new JsonArray();
        
        int[] elemEndIndex = new int[1];
        
        int elemsEnd = end - 1;
        for (int i = findWhitespaceEnd(json, start + 1, elemsEnd); i < elemsEnd;) {
            JsonValue elem = parseRange(json, i, elemsEnd, elemEndIndex);
            arr.add(elem);
            
            i = findWhitespaceEnd(json, elemEndIndex[0], elemsEnd);
            if (i == elemsEnd) {
                break;
            }
            if (json.codePointAt(i) != ',') {
                throw newIllegalArgumentF("missing comma before array element starting at '%s'", json.substring(i));
            }
            
            i = findWhitespaceEnd(json, i + 1, elemsEnd);
        }
        
        arrEndIndex[0] = end;
        return arr;
    }
    
    private JsonString parseJsonString(String json, int start, int end, int[] valueEndIndex) {
        assert json.codePointAt(start) == '"' : json;
        
        int quoteEnd = findQuoteEnd(json, start, end);
        if (quoteEnd < 0) {
            throw newIllegalArgumentF("missing end to string starting at '%s'", json.substring(start, end));
        }
        
        valueEndIndex[0] = quoteEnd;
        return JsonString.unescape(json.substring(start + 1, quoteEnd - 1));
    }
    
    private static int findWhitespaceEnd(String s, int start, int end) {
        for (int i = start; i < end;) {
            int code = s.codePointAt(i);
            
            if (!isWhitespace(code))
                return i;
            
            i += charCount(code);
        }
        return end;
    }
    
    private static int findBracketEnd(String s, int open, int shut, int start, int end) {
        assert s.codePointAt(start) == open : s.substring(start, end);
        
        int count = 1;
        
        for (int i = (start + charCount(open)); i < end;) {
            int code = s.codePointAt(i);
            
            if (code == open) {
                ++count;
            } else if (code == shut) {
                --count;
                if (count == 0) {
                    return i + charCount(shut);
                }
            } else if (code == '"') {
                int q = findQuoteEnd(s, i, end);
                if (q < 0) {
                    break;
                } else {
                    i = q;
                    continue;
                }
            }
            
            i += charCount(code);
        }
        
        return -1;
    }
    
    private static int findQuoteEnd(String s, int start, int end) {
        assert s.codePointAt(start) == '"' : s.substring(start, end);
        
        for (int i = start + 1; i < end;) {
            int code = s.codePointAt(i);
            if (code == '"') {
                return i + 1;
            }
            i += charCount(code);
        }
        
        return -1;
    }
}