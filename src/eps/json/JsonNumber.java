/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;

public final class JsonNumber extends AbstractJsonValue {
    private final double doubleValue;
    
    public JsonNumber(double doubleValue) {
        if (!eps.math.Math2.isReal(doubleValue)) {
            throw Errors.newIllegalArgument(doubleValue);
        }
        this.doubleValue = doubleValue;
    }
    
    public double doubleValue() {
        return doubleValue;
    }
    
    public int intValue() {
        return (int) doubleValue;
    }
    
    public <N> N as(Class<N> type) {
        java.util.Objects.requireNonNull(type, "type");
        Object result;
        
        type = Misc.box(type);
        
        if (type == Double.class)
            result = doubleValue;
        else if (type == Integer.class)
            result = (int) doubleValue;
        else if (type == Float.class)
            result = (float) doubleValue;
        else if (type == Byte.class)
            result = (byte) doubleValue;
        else if (type == Short.class)
            result = (short) doubleValue;
        else {
            throw Errors.newIllegalArgument(type);
        }
        
        return type.cast(result);
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, int indents, boolean multiLine, boolean indentFirst) {
        return indentIf(b, indents, multiLine && indentFirst).append(doubleValue);
    }
    
    public static boolean isExactlyConvertible(Object obj) {
        if (obj == null) return false;
        Class<?> c = obj.getClass();
        return c == Integer.class
            || c == Double.class
            || c == Float.class
            || c == Byte.class
            || c == Short.class;
    }
    
    public static JsonNumber valueOf(Number n) {
        double d;
        
        Class<?> c = n.getClass();
        
        if (c == Integer.class)
            d = (Integer) n;
        else if (c == Double.class)
            d = (Double) n;
        else if (c == Float.class)
            d = (Float) n;
        else if (c == Byte.class)
            d = (Byte) n;
        else if (c ==  Short.class)
            d = (Short) n;
        else {
            throw Errors.newIllegalArgument("probable lossy conversion from " + n);
        }
        
        return new JsonNumber(d);
    }
    
    @Override
    public int hashCode() {
        return Double.hashCode(doubleValue);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JsonNumber) {
            JsonNumber that = (JsonNumber) obj;
            // Note: doubleValue should/can never be NaN (see constructor).
            return this.doubleValue == that.doubleValue;
        }
        return false;
    }
}