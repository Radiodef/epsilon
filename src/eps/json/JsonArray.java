/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.util.Misc.*;

import java.util.*;

public final class JsonArray extends AbstractJsonValue implements Iterable<JsonValue> {
    private final List<JsonValue> elements;
    
    public JsonArray() {
        elements = new ArrayList<>();
    }
    
    public JsonArray(int initialCapacity) {
        elements = new ArrayList<>(initialCapacity);
    }
    
    public static boolean isConvertible(Object obj) {
        return (obj == null) || obj.getClass().isArray() || (obj instanceof Iterable<?>);
    }
    
    static Iterator<?> getIterator(Object obj) {
        if (obj.getClass().isArray()) {
            return new AnyArrayIterator(obj);
        } else if (obj instanceof Iterable<?>) {
            return ((Iterable<?>) obj).iterator();
        } else {
            throw Errors.newIllegalArgument(f("object of type %s can not be converted to a JSON array: %s", obj.getClass(), obj));
        }
    }
    
    public boolean isEmpty() {
        return elements.isEmpty();
    }
    
    public int size() {
        return elements.size();
    }
    
    public JsonValue get(int i) {
        return elements.get(i);
    }
    
    public JsonValue set(int i, JsonValue val) {
        Objects.requireNonNull(val, "val");
        JsonValue old = elements.set(i, val);
        return old;
    }
    
    public void add(JsonValue val) {
        elements.add(Objects.requireNonNull(val, "val"));
    }
    
    @Override
    public Iterator<JsonValue> iterator() {
        return elements.iterator();
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, int indents, boolean multiLine, boolean indentFirst) {
        indentIf(b, indents, multiLine && indentFirst);
        
        if (isEmpty()) {
            return b.append("[]");
        }
        
        b.append('[').append(multiLine ? '\n' : ' ');
        ++indents;
        
        int last = elements.size() - 1;
        for (int i = 0; i <= last; ++i) {
            if (multiLine) {
                JsonValue.indent(b, indents);
            }
            
            elements.get(i).toString(b, indents, multiLine, false);
            
            if (i != last) {
                b.append(',');
            }
            b.append(multiLine ? '\n' : ' ');
        }
        
        --indents;
        if (multiLine) {
            JsonValue.indent(b, indents);
        }
        
        return b.append(']');
    }
    
    @Override
    public int hashCode() {
        return elements.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JsonArray) {
            JsonArray that = (JsonArray) obj;
            return this.elements.equals(that.elements);
        }
        return false;
    }
}