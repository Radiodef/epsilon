/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.app.*;
import eps.util.*;
import static eps.util.Misc.*;

import java.util.*;
import java.util.Map.*;
import java.math.*;
import java.lang.reflect.*;
import java.util.function.*;

final class JsonJavaConverter {
    private final Map<Object, BigInteger> refsByObj = new IdentityHashMap<>();
    private final Map<BigInteger, Object> objsByRef = new HashMap<>();
    
    private BigInteger nextRef = BigInteger.ONE;
    
    JsonJavaConverter() {
    }
    
    private void reset() {
        refsByObj.clear();
        objsByRef.clear();
        nextRef = BigInteger.ONE;
    }
    
    // JAVA TO JSON
    
    JsonValue toJson(Object obj) {
        reset();
        JsonValue result;
        try {
            result = this.valueOf(obj, null);
        } finally {
            reset();
        }
        return result;
    }
    
    private JsonValue valueOf(Object obj, Class<?> fieldType) {
        if (obj == null) {
            return JsonValue.NULL;
        }
        if (obj instanceof Boolean) {
            return JsonBoolean.valueOf((Boolean) obj);
        }
        
        BigInteger ref = refsByObj.get(obj);
        if (ref != null) {
            return this.toReference(ref);
        }
        
        if (obj instanceof String) {
            return JsonString.valueOf(obj);
        }
        
//        if (JsonNumber.isExactlyConvertible(obj)) {
//            return JsonNumber.valueOf((Number) obj);
//        }
        
        if (JsonArrayConverter.isConvertible(obj) || obj.getClass().isArray()) {
            return this.toJsonArrayWrapper(obj, fieldType);
        }
        
        return this.toJsonObject(obj);
    }
    
    private JsonObject toReference(BigInteger ref) {
        JsonObject obj = new JsonObject();
        
        obj.put(Json.REF, JsonString.valueOf(ref));
        
        return obj;
    }
    
    private JsonObject toJsonArrayWrapper(Object src, Class<?> fieldType) {
        JsonObject obj = nextJsonObject(fieldType, src);
        
        obj.put(Json.ELEMS, toJsonArray(src));
        
        return obj;
    }
    
    private JsonArray toJsonArray(Object obj) {
        assert JsonArray.isConvertible(obj) : obj;
        
        JsonArray arr = new JsonArray();
        
        Iterator<?> it = JsonArray.getIterator(obj);
        
        while (it.hasNext()) {
            arr.add( this.valueOf(it.next(), null) );
        }
        
        return arr;
    }
    
    private JsonObject toJsonObject(Object obj) {
        if (obj instanceof Enum<?>) {
            return toJsonFromEnum((Enum<?>) obj);
        }
        
        Class<?> clazz = obj.getClass();
        
        ToStringConverter<?> sc = ToStringConverter.get(clazz);
        if (sc != null) {
            return toJsonFromString(obj, sc);
        }
        
        JsonObjectAdapter<?> adapter = JsonObjectAdapter.get(clazz);
        if (adapter != null) {
            return toJsonFromAdapterObject(obj, adapter);
        }
        
        if (clazz.isAnnotationPresent(JsonSerializable.class)) {
            return toJsonFromAnnotatedObject(obj);
        }
        
        throw Errors.newIllegalArgument(f("unable to convert object of %s to JSON: %s", obj.getClass(), obj));
    }
    
    private JsonObject nextJsonObject(Class<?> clazz, Object src) {
        if (clazz == null) {
            clazz = src.getClass();
        }
        
        JsonObject obj = new JsonObject();
        BigInteger ref = this.nextRef;
        
        obj.put(Json.CLASS, JsonString.valueOf(clazz.getName()));
        obj.put(Json.REF,   JsonString.valueOf(ref));
        
        this.refsByObj.put(src, ref);
        this.nextRef = ref.add(BigInteger.ONE);
        
        return obj;
    }
    
    private JsonObject toJsonFromAnnotatedObject(Object obj) {
        Class<?>     clazz = obj.getClass();
        ClassInfo<?> info  = ClassInfo.get(clazz);
        
        JsonObject result = nextJsonObject(clazz, obj);
        
        List<Field> props = info.getPropertyFields();
        
        for (Field field : props) {
            try {
                Object prop = field.get(obj);
                
                JsonProperty annotation = field.getAnnotation(JsonProperty.class);
                String       converter  = annotation.interimConverter();
                if (!converter.isEmpty()) {
                    InterimConverter<?, ?> ic = InterimConverter.forName(converter);
                    prop = ic.toInterim(prop);
                }
                
                result.put(field.getName(), this.valueOf(prop, prop == null ? field.getType() : null));
                
            } catch (ReflectiveOperationException x) {
                throw new IllegalArgumentException(x);
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private JsonObject toJsonFromAdapterObject(Object src, JsonObjectAdapter<?> adapter) {
        Map<?, ?> map = ((JsonObjectAdapter<Object>) adapter).toMap(src);
        
        JsonObject obj = nextJsonObject(adapter.getType(), src);
        
        JsonArray props = new JsonArray();
        
        for (Entry<?, ?> e : map.entrySet()) {
            JsonValue key = this.valueOf(e.getKey(), null);
            JsonValue val = this.valueOf(e.getValue(), null);
            
            JsonObject entry = new JsonObject();
            entry.put(Json.KEY, key);
            entry.put(Json.VAL, val);
            
            props.add(entry);
        }
        
        obj.put(Json.PROPS, props);
        
        return obj;
    }
    
    private JsonObject toJsonFromEnum(Enum<?> e) {
        JsonObject obj = new JsonObject();
        
        obj.put(Json.ENUM, JsonString.valueOf(e.getDeclaringClass().getName()));
        obj.put(Json.NAME, JsonString.valueOf(e.name()));
        
        return obj;
    }
    
    @SuppressWarnings("unchecked")
    private JsonObject toJsonFromString(Object src, ToStringConverter<?> sc) {
        JsonObject obj = new JsonObject();
        
        obj.put(Json.CLASS, JsonString.valueOf(sc.getType().getName()));
        obj.put(Json.VAL,   JsonString.valueOf(((ToStringConverter<Object>) sc).serialize(src)));
        
        return obj;
    }
    
    // JSON TO JAVA
    
    Object fromJson(JsonValue json) {
        reset();
        Object result;
        try {
            result = toObject(json, null);
            return result;
        } finally {
            reset();
        }
    }
    
    private Object toObject(JsonValue json, Class<?> type) {
        Objects.requireNonNull(json, "json");
//        Objects.requireNonNull(type, "type");
        // if (type == null) type = Object.class; ?
        
        if (json.isNull()) {
            return null;
        }
        if (json instanceof JsonBoolean) {
            return ((JsonBoolean) json).booleanValue();
        }
        if (json instanceof JsonString) {
            return ((JsonString) json).getChars();
        }
        
        // Note: serialization doesn't/shouldn't use these next 2 paths.
        if (json instanceof JsonNumber) {
            return ((JsonNumber) json).doubleValue();
        }
        if (json instanceof JsonArray) {
            return fromSimpleArray((JsonArray) json);
        }
        
        if (json instanceof JsonObject) {
            try {
                return toObject((JsonObject) json, type);
            } catch (ClassCastException x) {
                throw new IllegalArgumentException(json.toString(), x);
            }
        }
        
        throw Errors.newIllegalArgument(json);
    }
    
    private List<Object> fromSimpleArray(JsonArray json) {
        List<Object> result = new ArrayList<>(json.size());
        
        for (JsonValue val : json) {
            result.add( this.toObject(val, null) );
        }
        
        return result;
    }
    
    private Object toObject(JsonObject json, Class<?> type) {
        do {
            int count = json.count();
            
            JsonString $ref = (JsonString) json.get(Json.REF);
            
            if ($ref != null && count == 1) {
                return fromReference(json, $ref);
            }
            
            JsonString $enum = (JsonString) json.get(Json.ENUM);
            JsonString $name = (JsonString) json.get(Json.NAME);
            
            if ($enum != null && $name != null && count == 2) {
                return toEnum(json, $enum, $name);
            }
            if ($enum != null || $name != null) {
                break;
            }
            
            JsonString $class = (JsonString) json.get(Json.CLASS);
            
            if ($class == null) {
                break;
            }
            
            Class<?> clazz = getClass(json, $class);
            
            if ($ref == null) {
                ToStringConverter<?> converter = ToStringConverter.get(clazz);
                if (converter != null) {
                    JsonString $val = (JsonString) json.get(Json.VAL);
                    
                    if (json.count() == 2) {
                        return converter.deserialize($val.getChars());
                    }
                } // else
                break;
            }
            
            BigInteger ref = getReference(json, $ref);
            
            JsonArray $elems = (JsonArray) json.get(Json.ELEMS);
            
            if ($elems != null) {
                return put(fromArray(json, $class, $elems), ref);
            }

            JsonObjectAdapter<?> adapter = JsonObjectAdapter.get(clazz);
            if (adapter != null) {
                return put(fromAdapter(json, adapter), ref);
            }

            return put(fromAnnotatedClass(json, clazz), ref);
            
        } while (false);
        
        throw Errors.newIllegalArgument(json);
    }
    
    private Object put(Object obj, BigInteger ref) {
        objsByRef.put(ref, obj);
        return obj;
    }
    
    private BigInteger getReference(JsonValue json, JsonString $ref) {
        try {
            return new BigInteger($ref.getChars());
        } catch (NumberFormatException x) {
            throw new IllegalArgumentException(json.toString(), x);
        }
    }
    
    private Object fromReference(JsonObject json, JsonString $ref) {
        Objects.requireNonNull($ref, "$ref");
        BigInteger ref = getReference(json, $ref);
        Object     obj = objsByRef.get(ref);
        if (obj == null) {
            throw Errors.newIllegalArgument(f("no prior object exists with $ref %s. source: %s, objsByRef: %s", $ref, json, objsByRef));
        }
        return obj;
    }
    
    private static Class<?> getClass(JsonObject json, JsonString $class) {
        try {
            return Class.forName($class.getChars());
        } catch (ClassNotFoundException x) {
            throw new IllegalArgumentException(json.toString(), x);
        }
    }
    
    private Enum<?> toEnum(JsonObject json, JsonString $enum, JsonString $name) {
        Class<?> clazz = getClass(json, $enum);
        return Misc.getEnumConstant(clazz, $name.getChars());
    }
    
    private Object fromArray(JsonObject json, JsonString $class, JsonArray $elems) {
        Class<?> clazz = getClass(json, $class);
        int      size  = $elems.size();
        
        if (clazz.isArray()) {
            Object array = Array.newInstance(clazz.getComponentType(), size);
            
            for (int i = 0; i < size; ++i) {
                Object val = this.toObject($elems.get(i), null);
                try {
                    Array.set(array, i, val);
                } catch (IllegalArgumentException x) {
                    throw new IllegalArgumentException("clazz = " + clazz + ", elem[i] = " + $elems.get(i), x);
                }
            }
            
            return array;
        } // else
        
        List<Object> list = new ArrayList<>($elems.size());
        for (int i = 0; i < size; ++i) {
            list.add( this.toObject($elems.get(i), null) );
        }
        
        Constructor<?> ctor = JsonArrayConverter.getConstructor(clazz);
        
        try {
            return ctor.newInstance(list);
        } catch (ReflectiveOperationException x) {
            throw new IllegalArgumentException(json.toString(), x);
        }
    }
    
    private Object fromAnnotatedClass(JsonObject json, Class<?> clazz) {
        ClassInfo<?> info = ClassInfo.get(clazz);
        
        if (info == null) {
            throw Errors.newIllegalArgument(f("%s is not @JsonSerializable in %s", clazz, json));
        }
        
        List<Pair<String, Object>> args = new ArrayList<>(json.count());
        
        for (Entry<String, JsonValue> e : json.asMap().entrySet()) {
            String    key = e.getKey();
            JsonValue val = e.getValue();
            
            if (JsonProperty.BANNED_FIELD_NAMES.contains(key))
                continue;
            Field field = info.getPropertyField(key);
            
            Object obj;
            try {
                obj = this.toObject(val, field.getType());
            } catch (RuntimeException x) {
                Supplier<?> def = info.getDefault(field);
                if (def != null) {
                    Log.caught(JsonJavaConverter.class, "fromAnnotatedClass", x, true);
                    obj = def.get();
                } else {
                    throw x;
                }
            }
            
            JsonProperty annotation = field.getAnnotation(JsonProperty.class);
            String       converter  = annotation.interimConverter();
            if (!converter.isEmpty() && !annotation.deferInterimToConstructor()) {
                InterimConverter<?, ?> ic = InterimConverter.forName(converter);
                obj = ic.fromInterim(obj);
            }
            
            args.add(Pair.of(key, obj));
        }
        
        Constructor<?> ctor = info.getConstructor();
        
        if (args.size() < ctor.getParameterCount()) {
            List<Field> props = info.getPropertyFields();
            int         count = props.size();
            for (int i = 0; i < count; ++i) {
                Field  prop = props.get(i);
                String name = prop.getName();
                if (args.stream().noneMatch(p -> p.left.equals(name))) {
                    Supplier<?> def = info.getDefault(prop);
                    if (def != null) {
                        args.add(Pair.of(name, def.get()));
                    }
                }
            }
        }
        
        args.sort(Comparator.comparing(Pair.left(), Comparator.comparingInt(info::indexOfPropertyField)));
        
        Object[] ctorArgs = new Object[args.size()];
        for (int i = 0; i < ctorArgs.length; ++i) {
            ctorArgs[i] = args.get(i).right;
        }
        
        try {
            Object result = ctor.newInstance(ctorArgs);
            return result;
        } catch (ReflectiveOperationException | IllegalArgumentException x) {
            throw new IllegalArgumentException(json.toString(), x);
        }
    }
    
    private Object fromAdapter(JsonObject json, JsonObjectAdapter<?> adapter) {
        JsonArray props = (JsonArray) json.get(Json.PROPS);
        int       size  = props.size();
        
        Map<Object, Object> map = new LinkedHashMap<>(size);
        
        for (int i = 0; i < size; ++i) {
            JsonObject e = (JsonObject) props.get(i);
            
            JsonValue key = e.get(Json.KEY);
            JsonValue val = e.get(Json.VAL);
            
            map.put(this.toObject(key, null), this.toObject(val, null));
        }
        
        try {
            return adapter.toObject(map);
        } catch (ClassCastException | NullPointerException | IllegalArgumentException x) {
            throw new IllegalArgumentException(json.toString(), x);
        }
    }
}