/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

public interface JsonValue {
    
    default String toString(int indents) {
        return toString(new StringBuilder(), indents, true).toString();
    }
    
    default String toString(boolean multiLine) {
        return toString(new StringBuilder(), 0, multiLine).toString();
    }
    
    default StringBuilder toString(StringBuilder b, int indents, boolean multiLine) {
        return toString(b, indents, multiLine, true);
    }
    
    StringBuilder toString(StringBuilder b, int indents, boolean multiLine, boolean indentFirst);
    
    static StringBuilder indent(StringBuilder b, int indents) {
        for (; indents > 0; --indents) {
            b.append(Json.INDENT);
        }
        
        return b;
    }
    
    static StringBuilder indentIf(StringBuilder b, int indents, boolean doIndent) {
        if (doIndent) {
            indent(b, indents);
        }
        return b;
    }
    
    JsonValue NULL = new AbstractJsonValue() {
        @Override
        public StringBuilder toString(StringBuilder b, int indents, boolean multiLine, boolean indentFirst) {
            return indentIf(b, indents, multiLine && indentFirst).append(Json.NULL);
        }
    };
    
    default boolean isNull() {
        return this == JsonValue.NULL;
    }
    
    static JsonValue valueOf(Object obj) {
        return new JsonJavaConverter().toJson(obj);
    }
}