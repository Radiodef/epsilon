/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import java.util.*;
import java.util.Map.*;

public final class JsonObject extends AbstractJsonValue {
    private final LinkedHashMap<String, JsonValue> values = new LinkedHashMap<>();
    
    public JsonObject() {
    }
    
    public int count() {
        return values.size();
    }
    
    public Map<String, JsonValue> asMap() {
        return Collections.unmodifiableMap(values);
    }
    
    public JsonValue get(String key) {
        return values.get(Objects.requireNonNull(key, "key"));
    }
    
    public JsonValue put(String key, JsonValue val) {
        return values.put(Objects.requireNonNull(key, "key"),
                          Objects.requireNonNull(val, "val"));
    }
    
    public JsonValue remove(String key) {
        return values.remove(Objects.requireNonNull(key, "key"));
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, int indents, boolean multiLine, boolean indentFirst) {
        indentIf(b, indents, multiLine && indentFirst);
        
        if (values.isEmpty()) {
            return b.append("{}");
        }
        
        b.append("{");
        b.append(multiLine ? '\n' : ' ');
        
        Iterator<Entry<String, JsonValue>> it = values.entrySet().iterator();
        ++indents;
        
        while (it.hasNext()) {
            Entry<String, JsonValue> e = it.next();
            String    key = e.getKey();
            JsonValue val = e.getValue();
            
            if (multiLine) {
                JsonValue.indent(b, indents);
            }
            
//            b.append('"').append(key).append("\": ");
            new JsonString(key).toString(b, 0, false, false);
            b.append(": ");
            
            val.toString(b, indents, multiLine, false);
            
            if (it.hasNext()) {
                b.append(',');
            }
            
            b.append(multiLine ? '\n' : ' ');
        }
        
        --indents;
        if (multiLine) {
            JsonValue.indent(b, indents);
        }
        
        return b.append("}");
    }
    
    @Override
    public int hashCode() {
        return values.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JsonObject) {
            JsonObject that = (JsonObject) obj;
            return this.values.equals(that.values);
        }
        return false;
    }
}