/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;

import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;

public abstract class InterimConverter<T, U> {
    private final Class<T> sourceType;
    private final Class<U> interimType;
    
    protected InterimConverter(Class<T> sourceType, Class<U> interimType) {
        this.sourceType  = Objects.requireNonNull(sourceType,  "sourceType");
        this.interimType = Objects.requireNonNull(interimType, "interimType");
    }
    
    public Class<T> getSourceType() {
        return sourceType;
    }
    
    public Class<U> getInterimType() {
        return interimType;
    }
    
    public abstract U toInterim(Object obj);
    public abstract T fromInterim(Object obj);
    
    @Override
    public String toString() {
        return "InterimConverter{sourceType = " + sourceType + ", interimType = " + interimType + "}";
    }
    
    @SuppressWarnings("unchecked")
    public static final Class<InterimConverter<?, ?>> WILD_CLASS =
        (Class<InterimConverter<?, ?>>) (Class<? super InterimConverter<?, ?>>) InterimConverter.class;
    
    private static final Map<String, InterimConverter<?, ?>> CACHE = new ConcurrentHashMap<>();
    
    private static InterimConverter<?, ?> computeConverter(String className) {
        try {
            Class<? extends InterimConverter<?, ?>>       clazz = Misc.getClass(className).asSubclass(WILD_CLASS);
            Constructor<? extends InterimConverter<?, ?>> ctor  = clazz.getDeclaredConstructor();
            ctor.setAccessible(true);
            return ctor.newInstance();
        } catch (RuntimeException | ReflectiveOperationException x) {
            throw new IllegalArgumentException(className, x);
        }
    }
    
    private static final Function<String, InterimConverter<?, ?>> COMPUTE_CONVERTER = InterimConverter::computeConverter;
    
    public static InterimConverter<?, ?> forName(String className) {
        Objects.requireNonNull(className, "className");
        return CACHE.computeIfAbsent(className, COMPUTE_CONVERTER);
    }
    
    public static final class EmptySetConverter extends InterimConverter<Set<?>, Set<?>> {
        public EmptySetConverter() {
            super(Misc.WILD_SET, Misc.WILD_SET);
        }
        @Override
        public Set<?> toInterim(Object obj) {
            Set<?> set = (Set<?>) obj;
            if (set.isEmpty()) {
                return new HashSet<>(0);
            }
            return set;
        }
        @Override
        public Set<?> fromInterim(Object obj) {
            Set<?> set = (Set<?>) obj;
            if (set.isEmpty()) {
                return Collections.emptySet();
            }
            return set;
        }
    };
}