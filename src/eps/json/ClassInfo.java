/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.util.Errors.*;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.lang.reflect.*;

final class ClassInfo<T> {
    private final Class<T>                 type;
    private final List<Field>              props;
    private final Constructor<T>           ctor;
    private final Map<String, Supplier<?>> defaults;
    
    ClassInfo(Class<T> type) {
        this.type     = requireValidClass(type);
        this.props    = Collections.unmodifiableList(getFields(type, new ArrayList<>(), new HashSet<>()));
        this.ctor     = getConstructor(type, props);
        this.defaults = getDefaults(type, props);
    }
    
    private static <T> Class<T> requireValidClass(Class<T> type) {
        Objects.requireNonNull(type, "type");
        if (type.isAnnotation())
            throw newIllegalArgumentF("%s is an annotation", type);
        if (type.isInterface())
            throw newIllegalArgumentF("%s is an interface", type);
        if (type.isEnum())
            throw newIllegalArgumentF("%s is an enum", type);
        if (type.isAnonymousClass())
            throw newIllegalArgumentF("%s is an anonymous class", type);
        if (type.getEnclosingClass() != null && !Modifier.isStatic(type.getModifiers()))
            throw newIllegalArgumentF("%s is an inner class", type);
        if (!type.isAnnotationPresent(JsonSerializable.class))
            throw newIllegalArgumentF("%s is not JSON serializable", type);
        return type;
    }
    
    private static List<Field> getFields(Class<?> type, List<Field> fields, Set<String> names) {
        Class<?> supertype = type.getSuperclass();
        
        if ((supertype != null) && supertype.isAnnotationPresent(JsonSerializable.class)) {
            getFields(type.getSuperclass(), fields, names);
        }
        
        for (Field field : getDeclaredFields(type)) {
            if (!field.isAnnotationPresent(JsonProperty.class))
                continue;
            int mods = field.getModifiers();
            if (Modifier.isStatic(mods)) {
                throw newIllegalArgumentF("Found static property field '%s'", field);
            }
            String name = field.getName();
            if (JsonProperty.BANNED_FIELD_NAMES.contains(name)) {
                throw newIllegalArgumentF("'%s' may not be used as a field name", name);
            }
            if (!names.add(name)) {
                throw newIllegalArgumentF("Found duplicate field name '%s'", name);
            }
            field.setAccessible(true);
            fields.add(field);
        }
        
        return fields;
    }
    
    @SuppressWarnings("unchecked")
    private static <T> Constructor<T> getConstructor(Class<T> type, List<Field> props) {
        if (props.isEmpty()) {
            try {
                Constructor<T> ctor = type.getDeclaredConstructor();
                ctor.setAccessible(true);
                return ctor;
            } catch (ReflectiveOperationException x) {
                throw new IllegalArgumentException(x);
            }
        }
        
        int        paramCount = props.size();
        Class<?>[] propParams = new Class<?>[paramCount];
        for (int i = 0; i < paramCount; ++i) {
            Field        field      = props.get(i);
            JsonProperty annotation = field.getAnnotation(JsonProperty.class);
            Class<?>     fieldType  = field.getType();
            if (annotation.deferInterimToConstructor()) {
                try {
                    fieldType = InterimConverter.forName(annotation.interimConverter()).getInterimType();
                } catch (IllegalArgumentException x) {
                    throw new IllegalArgumentException(type.toString(), x);
                }
            }
            propParams[i] = fieldType;
        }
        
        eachConstructor:
        for (Constructor<?> ctor : type.getDeclaredConstructors()) {
            if (!ctor.isAnnotationPresent(JsonConstructor.class))
                continue;
            
            Class<?>[] params = ctor.getParameterTypes();
            
            if (params.length != paramCount)
                continue;
            for (int i = 0; i < paramCount; ++i) {
                if (!params[i].isAssignableFrom(propParams[i]))
                    continue eachConstructor;
            }
            
            ctor.setAccessible(true);
            return (Constructor<T>) ctor;
        }
        
        throw newIllegalArgumentF("no constructor for %s with parameters applicable to properties %s", type, props);
    }
    
    private static Map<String, Supplier<?>> getDefaults(Class<?> type, List<Field> props) {
        Map<String, Supplier<?>> defaults = new LinkedHashMap<>();
        
        forEachProperty:
        for (Field prop : props) {
            JsonDefault ann = null;
            
            for (Field field : getDeclaredFields(prop.getDeclaringClass())) {
                if ((ann = field.getAnnotation(JsonDefault.class)) == null)
                    continue;
                if (!Modifier.isStatic(field.getModifiers()))
                    continue;
                if (!prop.getType().isAssignableFrom(field.getType()))
                    continue;
                boolean isDefault =
                       ann.value().equals(prop.getName())
                    || field.getName().equals(prop.getName() + "Default");
                if (isDefault) {
                    field.setAccessible(true);
                    defaults.put(prop.getName(), () -> {
                        try {
                            return field.get(null);
                        } catch (ReflectiveOperationException x) {
                            throw new AssertionError(prop.getName(), x);
                        }
                    });
                    continue forEachProperty;
                }
            }
            for (Method method : getDeclaredMethods(prop.getDeclaringClass())) {
                if ((ann = method.getAnnotation(JsonDefault.class)) == null)
                    continue;
                if (!Modifier.isStatic(method.getModifiers()))
                    continue;
                if (method.getParameterCount() != 0)
                    continue;
                if (!prop.getType().isAssignableFrom(method.getReturnType()))
                    continue;
                boolean isDefault =
                       ann.value().equals(prop.getName())
                    || method.getName().equals(prop.getName() + "Default");
                if (isDefault) {
                    method.setAccessible(true);
                    defaults.put(prop.getName(), () -> {
                        try {
                            return method.invoke(null, (Object[]) null);
                        } catch (ReflectiveOperationException x) {
                            throw new AssertionError(prop.getName(), x);
                        }
                    });
                    continue forEachProperty;
                }
            }
        }
        
        return defaults;
    }
    
    Class<T> getType() {
        return type;
    }
    
    List<Field> getPropertyFields() {
        return props;
    }
    
    Constructor<T> getConstructor() {
        return ctor;
    }
    
    Supplier<?> getDefault(Field prop) {
        return getDefault(Objects.requireNonNull(prop, "prop").getName());
    }
    
    Supplier<?> getDefault(String name) {
        return defaults.get(Objects.requireNonNull(name, "name"));
    }
    
    public Map<String, Object> getDefaults() {
        Map<String, Object> defaults = new LinkedHashMap<>(this.defaults);
        defaults.replaceAll((k, v) -> ((Supplier<?>) v).get());
        return defaults;
    }
    
    Field getPropertyField(String name) {
        List<Field> props = this.props;
        int         size  = props.size();
        for (int i = 0; i < size; ++i) {
            Field p = props.get(i);
            if (p.getName().equals(name))
                return p;
        }
        Objects.requireNonNull(name, "name");
        throw newIllegalArgumentF("unknown property name '%s' for %s", name, this);
    }
    
    int indexOfPropertyField(String name) {
        List<Field> props = this.props;
        int         size  = props.size();
        for (int i = 0; i < size; ++i) {
            Field p = props.get(i);
            if (p.getName().equals(name))
                return i;
        }
        Objects.requireNonNull(name, "name");
        return -1;
    }
    
    @SuppressWarnings("unchecked")
    private static final Class<ClassInfo<?>> WILD_CLASS =
        (Class<ClassInfo<?>>) (Class<? super ClassInfo<?>>) ClassInfo.class;
    
    private static final ToString<ClassInfo<?>> TO_STRING =
        ToString.builder(ClassInfo.WILD_CLASS)
                .add("type",     ClassInfo::getType)
                .add("props",    ClassInfo::getPropertyFields)
                .add("ctor",     ClassInfo::getConstructor)
                .add("defaults", ClassInfo::getDefaults)
                .build();
    
    @Override
    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean multiline) {
        return TO_STRING.apply(this, multiline);
    }
    
    private static final Map<Class<?>, ClassInfo<?>> STORE = new ConcurrentHashMap<>();
    
    private static final Function<Class<?>, ClassInfo<?>> NEW_CLASS_INFO =
        type -> {
            if (type.isAnnotationPresent(JsonSerializable.class)) {
                return new ClassInfo<>(type);
            } else {
                return null;
            }
        };
    
    @SuppressWarnings("unchecked")
    static <T> ClassInfo<T> get(Class<T> type) {
        return (ClassInfo<T>) STORE.computeIfAbsent(type, NEW_CLASS_INFO);
    }
    
    private static final Map<Class<?>, Field[]> DECLARED_FIELDS = new ConcurrentHashMap<>();
    
    private static Field[] getDeclaredFields(Class<?> type) {
        return DECLARED_FIELDS.computeIfAbsent(type, Class::getDeclaredFields);
    }
    
    private static final Map<Class<?>, Method[]> DECLARED_METHODS = new ConcurrentHashMap<>();
    
    private static Method[] getDeclaredMethods(Class<?> type) {
        return DECLARED_METHODS.computeIfAbsent(type, Class::getDeclaredMethods);
    }
}