/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import java.util.*;

public final class SystemInfo {
    private SystemInfo() {}
    
    private static final String CL_INIT = "<clinit>";
    
    public static final String OS_NAME = System.getProperty("os.name");
    public static final String OS_VERS = System.getProperty("os.version");
    
    static {
        Log.note(SystemInfo.class,
                 CL_INIT,
                 "os.name is \"",
                 OS_NAME,
                 "\", which is ",
                 isMacOsx() ? "Mac OS X" : isWindows() ? "Windows" : "unknown", ".");
        Log.note(SystemInfo.class,
                 CL_INIT,
                 "os.version is ",
                 OS_VERS);
        Log.note(SystemInfo.class,
                 CL_INIT,
                 "java.version is ",
                 System.getProperty("java.version"));
        Log.note(SystemInfo.class,
                 CL_INIT,
                 "java.specification.version is ",
                 System.getProperty("java.specification.version"));
    }
    
    public static boolean isMacOsx() {
        // see https://developer.apple.com/library/content/technotes/tn2002/tn2110.html
        // (archive: http://archive.is/w6JC0)
        return OS_NAME.contains("OS X");
    }
    
    public static boolean isMacOsxElCapitan() {
        return isMacOsx() && OS_VERS.startsWith("10.11");
    }
    
    public static boolean isWindows() {
        return OS_NAME.startsWith("Windows");
    }
    
    private static final boolean IS_JAVA_9_OR_GREATER;
    static {
        boolean isJava9OrGreater = false;
        
        try {
            // noinspection JavaReflectionMemberAccess
            Object version = Runtime.class.getMethod("version").invoke(null);
            int    major   = (int) version.getClass().getMethod("major").invoke(version);
            
            isJava9OrGreater = major >= 9;
            
        } catch (ReflectiveOperationException ignored) {
        }
        
        IS_JAVA_9_OR_GREATER = isJava9OrGreater;
        Log.note(SystemInfo.class, CL_INIT, "is Java 9 or greater is ", isJava9OrGreater);
    }
    
    public static boolean isJava9OrGreater() {
        return IS_JAVA_9_OR_GREATER;
    }
    
    public static boolean isFont(String family) {
        return getAvailableFonts().contains(family);
    }
    
    private static volatile List<String> availableFonts;
    
    public static List<String> getAvailableFonts() {
        List<String> availableFonts = SystemInfo.availableFonts;
        if (availableFonts == null) {
            availableFonts = SystemInfo.availableFonts = getAvailableFonts0();
        }
        return availableFonts;
    }
    
    private static List<String> getAvailableFonts0() {
        switch (EpsilonMain.getUserInterfaceType()) {
            case SWING:
                try {
                    Class<?> graphicsEnv    = Class.forName("java.awt.GraphicsEnvironment");
                    Object   local          = graphicsEnv.getMethod("getLocalGraphicsEnvironment").invoke(null);
                    String[] availableNames = (String[]) graphicsEnv.getMethod("getAvailableFontFamilyNames").invoke(local);
                    
                    return Collections.unmodifiableList(Arrays.asList(availableNames));
                    
                } catch (ReflectiveOperationException x) {
                    Log.caught(SystemInfo.class, "during getAvailableFonts0", x, false);
                }
                break;
            case FX:
                try {
                    @SuppressWarnings("unchecked")
                    List<String> list =
                        (List<String>) Class.forName("javafx.scene.text.Font")
                                            .getMethod("getFamilies")
                                            .invoke(null);
                    return Collections.unmodifiableList(list);
                } catch (ReflectiveOperationException x) {
                    Log.caught(SystemInfo.class, "during getAvailableFonts0", x, false);
                }
                break;
            default:
                break;
        }
        return Collections.emptyList();
    }
}