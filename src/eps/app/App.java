/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import eps.eval.*;
import eps.ui.*;
import eps.job.*;
import eps.math.*;
import eps.util.*;
import eps.cmd.*;
import eps.io.*;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

public final class App implements MiniSettings {
    private static final AtomicReference<App> INSTANCE = new AtomicReference<>();
    
    static App initialize(ComponentFactory componentFactory) {
        return new App(componentFactory);
    }
    
    public static App app() {
        App app = INSTANCE.get();
        if (app == null) {
            throw new IllegalStateException("App not initialized");
        }
        return app;
    }
    
    static App getInstance() {
        return INSTANCE.get();
    }
    
    // this is e.g. SwingUtilities::invokeLater
    private static final PreinitialConsumer<Runnable> ASYNC_TASK_CONSUMER = new PreinitialConsumer<>();
    
    public static void runOnInterfaceThread(Runnable r) {
        ASYNC_TASK_CONSUMER.accept(r);
    }
    
    private static final PreinitialConsumer<Consumer<? super App>> WHEN_AVAILABLE = new PreinitialConsumer<>();
    
    public static void whenAvailable(Consumer<? super App> action) {
        WHEN_AVAILABLE.accept(action);
    }
    
    private static final List<Runnable> ON_EXIT = new CopyOnWriteArrayList<>();
    
    public static void onExit(Runnable task) {
        Objects.requireNonNull(task, "task");
        boolean doRun = false;
        synchronized (EXIT_MONITOR) {
            if (IS_EXITING.get()) {
                doRun = true;
            } else {
                ON_EXIT.add(task);
            }
        }
        if (doRun) {
            task.run();
        }
    }
    
    public static void offExit(Runnable task) {
        Objects.requireNonNull(task, "task");
        synchronized (EXIT_MONITOR) {
            if (!IS_EXITING.get()) {
                ON_EXIT.remove(task);
            }
        }
    }
    
    private static final Object EXIT_MONITOR = new Object();
    
    private static final AtomicBoolean IS_EXITING = new AtomicBoolean(false);
    
    private static final Map<Thread, Long> THREADS = Collections.synchronizedMap(new WeakHashMap<>());
    
    private static Thread putThread(Thread t) {
        synchronized (THREADS) {
            THREADS.put(t, System.currentTimeMillis());
            return t;
        }
    }
    
    public static Thread newThread(Runnable r) {
        synchronized (THREADS) {
            return App.putThread(new Thread(r));
        }
    }
    
    public static List<Thread> getThreads() {
        synchronized (THREADS) {
            List<Thread> threads = new ArrayList<>(THREADS.keySet());
            threads.sort(Comparator.comparing(THREADS::get));
            return threads;
        }
    }
    
    // instance stuff below
    
    private final Settings.Concurrent settings;
    
    private final ComponentFactory componentFactory;
    private final AsyncExecutor asyncExecutor;
    
    private final MainWindow window;
    
    private final Lazy<DebugWindow> debugWindow;
    private final Lazy<SettingsWindow> settingsWindow;
    private final Lazy<SymbolsWindow> symbolsWindow;
    
    private final Worker worker;
    private final Worker worker2;
    
    private final Symbols.Concurrent symbols;
    
    private final ListDeque<Item> items     = new ListDeque<>();
    private final ItemChangeTimer itemTimer = new ItemChangeTimer();
    
    private final PreinitialConsumer<String> commandsSent = new PreinitialConsumer<>();
    private final PreinitialConsumer<Consumer<? super Item>> itemsAdded = new PreinitialConsumer<>();
    
    private final Deque<CommandFilter> commandFilters = new ArrayDeque<>();
    
    private volatile int historyCursor = -1;
    
    private volatile boolean initialized = false;
    private volatile boolean loading     = true;
    
    private App(ComponentFactory componentFactory) {
        Log.constructing(App.class);
        Objects.requireNonNull(componentFactory, "componentFactory");
        
        if (!INSTANCE.compareAndSet(null, this)) {
            throw Errors.newIllegalState("App already initialized");
        }
        
        FileSystem.setInstance(RealFileSystem.INSTANCE);
        LogFileWriter.createInstance();
        PersistentFiles.readAndPutThemes();
        
        componentFactory.setApp(this);
        AsyncExecutor executor = componentFactory.newAsyncExecutor();
        this.asyncExecutor = executor;
        requireInterfaceThread();
        
        App.putThread(Thread.currentThread());
        
        this.settings         = PersistentFiles.readSettings();
        this.componentFactory = componentFactory;
        this.window           = componentFactory.newMainWindow();
        this.debugWindow      = Lazy.from(componentFactory::newDebugWindow);
        this.settingsWindow   = Lazy.from(componentFactory::newSettingsWindow);
        this.symbolsWindow    = Lazy.from(componentFactory::newSymbolsWindow);
        this.worker           = new Worker("App.Worker",  executor, Thread.NORM_PRIORITY);
        this.worker2          = new Worker("App.Worker2", executor, Thread.MIN_PRIORITY);
        // Some symbols need a reference to the app Symbols instance
        // during deserialization, so it needs to be assigned beforehand.
        this.symbols          = Symbols.Concurrent.empty();
        PersistentFiles.readSymbols(this.symbols);
        
        window.addEventCallback(new MainWindow.EventCallback() {
            @Override
            public void commandSent(MainWindow source, String text) {
                source.setCommandText("");
                commandsSent.accept(text);
            }
            @Override
            public void historyPrevious(MainWindow source) {
                App.this.historyPrevious();
            }
            @Override
            public void historyNext(MainWindow source) {
                App.this.historyNext();
            }
            @Override
            public void windowBoundsChanged(MainWindow source, BoundingBox bounds) {
                App.this.mainWindowBoundsChanged(bounds);
            }
            @Override
            public void windowMaximizedChanged(MainWindow source, boolean isMaximized) {
                App.this.mainWindowMaximizedChanged(isMaximized);
            }
            @Override
            public void windowFullScreenChanged(MainWindow source, boolean isFullScreen) {
                App.this.mainWindowFullScreenChanged(isFullScreen);
            }
            @Override
            public void windowFirstShown(MainWindow source) {
                ASYNC_TASK_CONSUMER.set(executor::execute);
            }
        });
        
        window.setTitleText(settings.get(Setting.APPLICATION_NAME));
        window.setBounds(settings.get(Setting.MAIN_WINDOW_BOUNDS));
        window.setMaximized(settings.get(Setting.MAIN_WINDOW_MAXIMIZED));
        window.setFullScreen(settings.get(Setting.MAIN_WINDOW_FULL_SCREEN));
        window.setAlwaysOnTop(settings.get(Setting.MAIN_WINDOW_ALWAYS_ON_TOP));
        
        MathEnv.poke();
        
        initialized = true;
        
        WHEN_AVAILABLE.set(action -> action.accept(this));
        
        worker.enqueue(new ItemLoader(), true);
    }
    
    private final class ItemLoader extends AbstractJob {
        private static final long INTERVAL = 20;
        
        @Override
        protected void executeImpl() {
            Log.entering(ItemLoader.class, "executeImpl");
            for (Item.Info info : PersistentFiles.readItems()) {
                Worker.executing();
                try {
                    asyncExecutor.executeNow(() -> {
                        Item item = createAndAddItem(info.getCommandText());
                        item.set(info);
                    });
                } catch (AsyncExecutor.AsyncException x) {
                    Log.caught(ItemLoader.class, "executeImpl", x, true);
                }
                Misc.sleep(INTERVAL);
            }
        }
        
        @Override
        protected void doneImpl() {
            Log.entering(ItemLoader.class, "doneImpl");
            if (!isCancelled()) {
                loadingDone();
            }
        }
    }
    
    private void loadingDone() {
        Log.entering(App.class, "loadingDone");
        requireInterfaceThread();
        historyCursor = items.size();
        itemTimer.start();
        loading = false;
        itemsAdded.set(this::addItemOnInterfaceThreadImpl);
        commandsSent.set(this::commandSent);
    }
    
    private static <T> T requirePostInitial(String name, T obj) {
        if (obj == null) {
            throw new IllegalStateException(name + " not initialized yet");
        }
        return obj;
    }
    
    public void requireInterfaceThread() {
        if (getAsyncExecutor().isInterfaceThread())
            return;
        throw new IllegalStateException();
    }
    
    public Settings getSettings() {
        return requirePostInitial("settings", settings);
    }
    
    @Override
    public <T> T getSetting(Setting<T> key) {
        return getSettings().get(key);
    }
    
    public Symbols getSymbols() {
        return requirePostInitial("symbols", symbols);
    }
    
    public void writeSymbols() {
        PersistentFiles.writeSymbols((Symbols.Concurrent) getSymbols());
    }
    
    public void writeThemes() {
        PersistentFiles.writeThemes();
    }
    
    private ListDeque<Item> getItems() {
        return requirePostInitial("items", items);
    }
    
    private void writeItems() {
        try {
            List<Item.Info> l = getAsyncExecutor().executeNow(() -> {
                List<Item>      items = getItems();
                int             count = items.size();
                List<Item.Info> infos = new ArrayList<>(count);
                for (int i = 0; i < count; ++i) {
                    infos.add(items.get(i).getInfo());
                }
                return infos;
            });
            PersistentFiles.writeItems(l);
        } catch (AsyncExecutor.AsyncException x) {
            Log.caught(getClass(), "while getting item info", x, true);
        }
    }
    
    public void itemsChanged() {
        itemTimer.reset();
    }
    
    public MathEnv getMathEnvironment() {
        return MathEnv.instance(getSettings().get(Setting.MATH_ENV_CLASS));
    }
    
    public ComponentFactory getComponentFactory() {
        return requirePostInitial("component factory", componentFactory);
    }
    
    public AsyncExecutor getAsyncExecutor() {
        return requirePostInitial("async executor", asyncExecutor);
    }
    
    public MainWindow getMainWindow() {
        return requirePostInitial("main window", window);
    }
    
    public DebugWindow getDebugWindow() {
        return getWindow("debug window", debugWindow);
    }
    
    public SettingsWindow getSettingsWindow() {
        return getWindow("settings window", settingsWindow);
    }
    
    public SymbolsWindow getSymbolsWindow() {
        return getWindow("symbols window", symbolsWindow);
    }
    
    private <W extends Window> W getWindow(String name, Lazy<W> lazy) {
        if (IS_EXITING.get()) {
            throw new IllegalStateException();
        }
        return requirePostInitial(name, lazy).get();
    }
    
    public Optional<DebugWindow> getDebugWindowIfInitialized() {
        return getWindowIfInitialized(debugWindow);
    }
    
    public Optional<SettingsWindow> getSettingsWindowIfInitialized() {
        return getWindowIfInitialized(settingsWindow);
    }
    
    public Optional<SymbolsWindow> getSymbolsWindowIfInitialized() {
        return getWindowIfInitialized(symbolsWindow);
    }
    
    private <W extends Window> Optional<W> getWindowIfInitialized(Lazy<W> lazy) {
        return IS_EXITING.get() ? Optional.empty() : lazy.getIfInitialized();
    }
    
    public void ifDebugWindowInitialized(Consumer<? super DebugWindow> action) {
        debugWindow.ifInitialized(action);
    }
    
    public void ifSettingsWindowInitialized(Consumer<? super SettingsWindow> action) {
        settingsWindow.ifInitialized(action);
    }
    
    public void ifSymbolsWindowInitialized(Consumer<? super SymbolsWindow> action) {
        symbolsWindow.ifInitialized(action);
    }
    
    public Worker getWorker() {
        return requirePostInitial("worker", worker);
    }
    
    public Worker getWorker2() {
        return requirePostInitial("worker2", worker2);
    }
    
    public void exit() {
        exit((Runnable) null);
    }
    
    private boolean isPreinitial(String caller) {
        if (!initialized) {
            Log.note(App.class, "isPreinitial", caller, " invoked before initialization");
            return true;
        }
        return false;
    }
    
    // should this be static? (for exiting before the App is initialized)
    public void exit(Runnable exitCallback) {
        Log.entering(App.class, "exit");
        if (isPreinitial("exit"))
            return;
        
        boolean        doExit = false;
        List<Runnable> tasks;
        synchronized (EXIT_MONITOR) {
            if (IS_EXITING.compareAndSet(false, true)) {
                doExit = true;
                tasks  = new ArrayList<>(ON_EXIT);
                ON_EXIT.clear();
            } else {
                tasks = Collections.emptyList();
            }
        }
        
        if (doExit) {
            Thread timeout = newThread(() -> {
                long millis = getSetting(Setting.EXIT_TIMEOUT);
                Misc.sleepSilently(millis);
                class ExitTimedOutException extends Exception {
                    ExitTimedOutException(Thread t) {
                        super(t.getName());
                        setStackTrace(t.getStackTrace());
                    }
                }
                for (Thread t : getThreads()) {
                    if (t.isAlive()) {
                        Log.caught(App.class, "exit", new ExitTimedOutException(t), false);
                    }
                }
                PersistentFiles.exitingAbruptly();
                System.exit(0xBAD_10_000);
            });
            timeout.setName("App.exitTimeout");
            timeout.setDaemon(true);
            timeout.start();
            asyncExecutor.executeNow(componentFactory::prepareToExit);
            asyncExecutor.executeLater(() -> exitImpl(tasks, exitCallback));
        } else {
            Log.note(App.class, "exit", Thread.currentThread(), " attempted to exit");
        }
    }
    
    private void exitImpl(List<Runnable> tasks, Runnable exitCallback) {
        for (Runnable task : tasks) {
            try {
                task.run();
            } catch (RuntimeException x) {
                Log.caught(App.class, "exit", x, false);
            }
        }
        itemTimer.stop();
        
        asyncExecutor.executeNow(() -> {
            window.close();
            debugWindow.ifInitialized(DebugWindow::close);
            settingsWindow.ifInitialized(SettingsWindow::close);
            
            componentFactory.close();
        });
        
        worker2.close(() -> {
            worker.close(() -> {
                Log.note(App.class, "exit$Runnable.run", "Exiting.");
                
                settings.forceUnsafeFlush();
                if (!loading) {
                    writeItems();
                }
                
                Log.note(App.class, "exit$Runnable.run", "Really exiting.");
                if (exitCallback != null) {
                    exitCallback.run();
                }
            });
        });
    }
    
    private Item createAndAddItem(String commandText) {
        assert !isPreinitial("createAndAddItem");
        requireInterfaceThread();
        
        Item item = new Item(commandText);
        addItem(item);
        
        return item;
    }
    
    private void addItem(Item item) {
        assert !isPreinitial("addItem");
        requireInterfaceThread();
        
        int maxItems = settings.get(Setting.MAX_ITEMS);
        
        while (items.size() >= maxItems) {
            try (Item del = items.removeFirst()) {
                window.removeCell(del.getContainer());
            }
        }
        
        items.addLast(item);
        window.addCell(item.getContainer());
    }
    
    void removeItemOnInterfaceThread(Item item) {
        runOnInterfaceThread(() -> {
            int index = items.indexOf(item);
            if (index >= 0) {
                items.remove(index);
                window.removeCell(item.getContainer());
                item.close();
                
                if (index < historyCursor) {
                    --historyCursor;
                }
            }
        });
    }
    
    public void addItemOnInterfaceThread(Consumer<? super Item> itemConsumer) {
        itemsAdded.accept(itemConsumer);
    }
    
    private void addItemOnInterfaceThreadImpl(Consumer<? super Item> itemConsumer) {
        runOnInterfaceThread(() -> {
            itemConsumer.accept(this.createAndAddItem(null));
        });
    }
    
    @FunctionalInterface
    public interface CommandFilter {
        int CONSUME = 1 << 0;
        int DONE    = 1 << 1;
        
        static boolean isDone(int response) {
            return (response & DONE) == DONE;
        }
        static boolean isConsumed(int response) {
            return (response & CONSUME) == CONSUME;
        }
        
        int filter(String command);
    }
    
    public void addCommandFilter(CommandFilter filter) {
        Objects.requireNonNull(filter, "filter");
        runOnInterfaceThread(() -> commandFilters.addLast(filter));
    }
    
    private void commandSent(String commandText) {
        Log.entering(App.class, "commandSent");
        Objects.requireNonNull(commandText, "commandText");
        requireInterfaceThread();
        if (isPreinitial("commandSent"))
            return;
        Log.note(App.class, "commandSent", "commandText = \"", commandText, "\"");
        
        commandText = commandText.trim();
        if (commandText.isEmpty())
            return;
        
        Iterator<CommandFilter> it = commandFilters.iterator();
        while (it.hasNext()) {
            int response = it.next().filter(commandText);
            if (CommandFilter.isDone(response))
                it.remove();
            if (CommandFilter.isConsumed(response))
                return;
        }
        
        Item item = createAndAddItem(commandText);
        historyCursor = items.size();
        
        Command.executeFirst(commandText, item);
    }
    
    private int historySeek(int start, int increment) {
        requireInterfaceThread();
        String prevCmd = null;
        
        if (Math2.inRange(start, 0, items.size())) {
            prevCmd = items.get(start).getCommandText();
        }
        
        for (;;) {
            int next = start + increment;
            
            if (!Math2.inRange(next, 0, items.size())) {
                return next;
            }
            String nextCmd = items.get(next).getCommandText();
            if (nextCmd != null && (prevCmd == null || !prevCmd.equals(nextCmd))) {
                return next;
            }
            
            start = next;
        }
    }
    
    private void historyPrevious() {
        Log.entering(App.class, "historyPrevious");
        requireInterfaceThread();
        if (isPreinitial("historyPrevious") || loading)
            return;
        
        int historyCursor = this.historyCursor;
        if (historyCursor == 0)
            return;
        
        int prev = historySeek(historyCursor, -1);
        
        if (prev >= 0) {
            getMainWindow().setCommandText(items.get(prev).getCommandText());
            
            this.historyCursor = prev;
        }
    }
    
    private void historyNext() {
        Log.entering(App.class, "historyNext");
        requireInterfaceThread();
        if (isPreinitial("historyNext") || loading)
            return;
        
        int historyCursor = this.historyCursor;
        if (historyCursor == items.size())
            return;
        
        int next = historySeek(historyCursor, +1);
        
        if (next < items.size()) {
            getMainWindow().setCommandText(items.get(next).getCommandText());
        } else {
            getMainWindow().setCommandText("");
        }
        this.historyCursor = next;
    }
    
    private void mainWindowBoundsChanged(BoundingBox bounds) {
        requireInterfaceThread();
        if (isPreinitial("mainWindowBoundsChanged"))
            return;
        getSettings().set(Setting.MAIN_WINDOW_BOUNDS, bounds);
    }
    
    private void mainWindowMaximizedChanged(boolean isMaximized) {
        requireInterfaceThread();
        if (isPreinitial("mainWindowMaximizedChanged"))
            return;
        getSettings().set(Setting.MAIN_WINDOW_MAXIMIZED, isMaximized);
    }
    
    private void mainWindowFullScreenChanged(boolean isFullScreen) {
        requireInterfaceThread();
        if (isPreinitial("mainWindowFullScreenChanged"))
            return;
        getSettings().set(Setting.MAIN_WINDOW_FULL_SCREEN, isFullScreen);
    }
    
    public static void evaluate(String          text,
                                Item            item,
                                ComputationCell cell) {
        evaluate(text, item, cell, AnswerFormatter.DECIMAL, PostComputation.IDENTITY);
    }
    
    public static void evaluate(String          text,
                                Item            item,
                                ComputationCell cell,
                                AnswerFormatter fmt,
                                PostComputation post) {
        evaluate(text, item, cell, fmt, post, comp -> cell.setComputation(comp, true));
    }
    
    public static void evaluate(String                        text,
                                Item                          item0,
                                ComputationCell               cell,
                                AnswerFormatter               fmt,
                                PostComputation               post,
                                Consumer<? super Computation> cons) {
        App.runOnInterfaceThread(() -> {
            App app = app();
            
            Item item;
            if (item0 == null) {
                Item found = app.items.stream().filter(e -> e.contains(cell)).findFirst().orElse(null);
                if (found == null) {
                    Log.note(App.class, "evaluate", "could not evaluate '", text, "' because the specified cell has no item");
                    return;
                }
                item = found;
            } else {
                item = item0;
                assert item.contains(cell) : item;
            }
            
            cell.setComplete(false);
            
            ComputationJob job = new ComputationJob(app, text, item, cell, fmt, post, cons);
            
            cell.addDeleteListener(job);
            cell.addStopListener(job::cancel);
            
            app.getWorker().enqueue(job, false);
        });
    }
    
    private static final class ComputationJob extends AbstractJob implements Consumer<Cell> {
        private final App                           app;
        private final String                        text;
        private final Item                          item;
        private final ComputationCell               cell;
        private final AnswerFormatter               fmt;
        private final PostComputation               post;
        private final Consumer<? super Computation> cons;
        
        private Symbols.ModRef      modRef      = null;
        private Computation         computation = null;
        private EvaluationException error       = null;
        
        private ComputationJob(App                           app,
                               String                        text,
                               Item                          item,
                               ComputationCell               cell,
                               AnswerFormatter               fmt,
                               PostComputation               post,
                               Consumer<? super Computation> cons) {
            this.app  = Objects.requireNonNull(app,  "app");
            this.text = Objects.requireNonNull(text, "text");
            this.item = Objects.requireNonNull(item, "item");
            this.cell = Objects.requireNonNull(cell, "cell");
            this.fmt  = Objects.requireNonNull(fmt,  "fmt");
            this.post = Objects.requireNonNull(post, "post");
            this.cons = Objects.requireNonNull(cons, "cons");
        }

        @Override
        protected void executeImpl() {
            Log.entering(ComputationJob.class, "execute");
            Context prev = Context.local();
            
            try (Settings settings = app.getSettings().block()) {
                Context context  = new Context(text, app);
                prev             = Context.setLocal(context);
                this.modRef      = context.getSymbols().getModificationRef();
                Tree    tree     = Tree.create(context);
                this.computation = Computation.create(item, context, tree, null, fmt, post);

                Operand result = tree.evaluate(context);
                result         = post.apply(result, context);

                if (result.isReference()) {
                    Symbol sym = ((Reference) result).get();
                    if (sym.isVariable()) {
                        result = ((Variable) sym).evaluate(context);
                    }
                }

                SpecialSyms.setAnswer(context, tree, result);
                context.success();
                this.computation = Computation.create(item, context, tree, result, fmt, post);

            } catch (EvaluationException x) {
                if (this.computation != null) {
                    this.computation = Computation.create(item,
                                                          computation.getContext(),
                                                          computation.getTree(),
                                                          computation.getResult(),
                                                          fmt,
                                                          post,
                                                          x);
                }
                this.error = x;
                Log.caught(ComputationJob.class, "While executing command", x, true);
            } finally {
                Context.setLocal(prev);
            }
        }

        @Override
        protected void doneImpl() {
            Log.entering(ComputationJob.class, "done");

            cell.removeDeleteListener(this);

            if (isCancelled()) {
                Log.note(ComputationJob.class, "doneImpl", "cancelled ", text);
                item.addNote("stopped");
            }

            if (computation != null) {
                cons.accept(computation);
//                cell.setComputation(computation, true);
            } else {
                cell.setText(text);
            }

            if (error != null) {
                Set<String> messages = new LinkedHashSet<>();
                error.collectAllMessages(messages::add);
                messages.forEach(item::addNote);
            }

            cell.setComplete(true);

            if (modRef != null && computation != null) {
                Context context = computation.getContext();
                if (context != null) {
                    // Note: basically always writes because of ans variable.
                    Symbols syms = context.getSymbols();
                    if (syms.modifiedSince(modRef)) {
                        app.writeSymbols();
                        app.ifSymbolsWindowInitialized(Window::refresh);
                    }
                }
            }
        }

        @Override
        public void accept(Cell cell) {
            this.cancel();
        }
    }
    
    private static final class ItemChangeTimer implements Runnable {
        private final Thread thread;
        private final long   interval = 500;
        
        private volatile long timeout = Long.MAX_VALUE;
        
        private boolean isRunning = false;
        private boolean isSet     = false;
        private long    start     = -1;
        
        private ItemChangeTimer() {
            this.thread = App.newThread(this);
            thread.setName("App.ItemChangeTimer");
            thread.setDaemon(true);
            thread.setPriority(Thread.MIN_PRIORITY);
            
            App.whenAvailable(app -> {
                app.getSettings().addChangeListener(Setting.ITEM_CHANGE_TIMEOUT,
                                                    timeout -> this.timeout = (int) timeout);
                this.timeout = (int) app.getSetting(Setting.ITEM_CHANGE_TIMEOUT);
            });
        }
        
        synchronized void start() {
            if (!isRunning) {
                isRunning = true;
                thread.start();
            }
        }
        
        synchronized void stop() {
            isRunning = isSet = false;
            notifyAll();
        }
        
        synchronized void reset() {
            if (isRunning) {
                start = System.currentTimeMillis();
                isSet = true;
            }
        }
        
        @Override
        public void run() {
            for (;;) {
                boolean write = false;
                
                synchronized (this) {
                    if (isSet) {
                        if ((System.currentTimeMillis() - start) >= timeout) {
                            isSet = false;
                            write = true;
                        }
                    }
                }
                // do not write while holding the monitor (prevents deadlock)
                if (write) {
                    app().writeItems();
                }
                
                synchronized (this) {
                    Misc.wait(this, interval);
                    
                    if (!isRunning)
                        break;
                }
            }
        }
    }
}