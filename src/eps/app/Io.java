/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import eps.json.*;
import eps.util.*;
import eps.eval.*;
import eps.job.*;
import eps.ui.*;
import static eps.util.Misc.*;
import static eps.app.App.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import java.util.concurrent.atomic.*;
import java.time.*;
import java.time.format.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.LinkOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.InvalidPathException;
import java.nio.file.Files;
import java.nio.file.FileAlreadyExistsException;

@Deprecated
public final class Io {
    private Io() {
    }
    
    private static final Object MONITOR = new Object();
    
    private static final Object WRITERS_MONITOR = new Object();
    private static volatile long WRITERS = 0;
    
    static void exitingAbruptly() {
        synchronized (WRITERS_MONITOR) {
            while (WRITERS > 0) {
                try {
                    WRITERS_MONITOR.wait();
                } catch (InterruptedException ignored) {
                }
            }
            WRITERS = -1;
        }
    }
    
    private static final class Writer implements AutoCloseable {
        private final boolean doRun;
        private Writer() {
            synchronized (WRITERS_MONITOR) {
                long writers = WRITERS;
                if (writers < 0) {
                    doRun   = false;
                } else {
                    doRun   = true;
                    WRITERS = writers + 1;
                }
            }
        }
        @Override
        public void close() {
            synchronized (WRITERS_MONITOR) {
                if (--WRITERS <= 0) {
                    WRITERS_MONITOR.notifyAll();
                }
            }
        }
        static void run(Runnable task) {
            try (Writer w = new Writer()) {
                if (w.doRun) {
                    task.run();
                }
            }
        }
    }
    
    private static final class FileInfo {
        final    String  name;
        final    String  ext;
        volatile Path    path;
        volatile boolean backup;
        volatile boolean error;
        
        volatile IllegalStateException trace;
        
        private FileInfo(String name, String ext, boolean backup) {
            this.name   = Objects.requireNonNull(name, "name");
            this.ext    = ext;
            this.path   = null;
            this.backup = backup;
            this.error  = false;
            this.trace  = null;
        }
        
        FileInfo error() {
            this.error = true;
            
            IllegalStateException prev  = this.trace;
            IllegalStateException trace = new IllegalStateException();
            if (prev != null) {
                trace.addSuppressed(prev);
            }
            this.trace = trace;
            
            return this;
        }
        
        private static final ToString<FileInfo> TO_STRING =
            ToString.builder(FileInfo.class)
                    .add("name",  info -> info.name)
                    .add("ext",   info -> info.ext)
                    .add("path",  info -> info.path)
                    .add("error", info -> info.error)
                    .add("trace", info -> info.trace)
                    .build();
        
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
    }
    
    private static final String OBJECT_FILE_EXT = "epsilonobj";
    
    private static final FileInfo APP_DIRECTORY   = new FileInfo("EpsilonApplicationFiles", null, false);
    private static final FileInfo LOG_DIRECTORY   = new FileInfo("epsilon_logs",            null, false);
    private static final FileInfo THEME_DIRECTORY = new FileInfo("epsilon_themes",          null, false);
    private static final FileInfo MISC_DIRECTORY  = new FileInfo("epsilon_misc",            null, false);
    
    private static final FileInfo SETTINGS_FILE = new FileInfo("epsilon_settings", OBJECT_FILE_EXT, true);
    private static final FileInfo SYMBOLS_FILE  = new FileInfo("epsilon_symbols",  OBJECT_FILE_EXT, true);
    private static final FileInfo ITEMS_FILE    = new FileInfo("epsilon_items",    OBJECT_FILE_EXT, true);
    
    private static final String LOG_FILE_NAME = "epsilon_log";
    private static final String LOG_FILE_EXT  = "txt";
    
    private static volatile boolean LOG_FILE_FAILURE = false;
    
    private enum NoFile { INSTANCE }
    
    private static String abs(Path path) {
        return path.toAbsolutePath().toString();
    }
    
    public static boolean isObjectFile(Path path) {
        if (Files.isDirectory(path)) {
            return false;
        }
        return isObjectFile(path.getFileName().toString());
    }
    
    public static boolean isObjectFile(String name) {
        if (name.endsWith(OBJECT_FILE_EXT)) {
            int dot = name.length() - OBJECT_FILE_EXT.length();
            if (dot > 0) {
                int code = name.codePointBefore(dot);
                if (code == '.') {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static String toObjectFile(String name) {
        if (!isObjectFile(name)) {
            name += "." + OBJECT_FILE_EXT;
        }
        return name;
    }
    
    public static String fromObjectFile(String name) {
        if (isObjectFile(name)) {
            name = name.substring(0, name.length() - OBJECT_FILE_EXT.length() - 1);
        }
        return name;
    }
    
    public static String getSymbolsFileName() {
        if (!Log.caughtError()) {
            FileInfo info = SYMBOLS_FILE;
            if (!info.error) {
                Path path = info.path;
                if (path != null) {
                    Path file = path.getFileName();
                    if (file != null) {
                        return file.toString();
                    }
                }
            }
        }
        return null;
    }
    
    private static Path getDirectory(Path parent, FileInfo info, String description) {
        synchronized (MONITOR) {
            if (Log.caughtError())
                return null;
            if (info.error)
                return null;
            Path path = info.path;
            if (path != null)
                return path;
            path = getDirectory0(parent, info.name, description);
            info.path = path;
            if (path == null)
                info.error();
            return path;
        }
    }
    
    private static Path getDirectory0(Path parent, String directoryName, String description) {
        Path path;
        
        for (int suffix = 0;; ++suffix) {
            String pathName = directoryName;
            
            if (suffix > 0) {
                pathName += suffix;
            }
            
            try {
                path = (parent != null) ? parent.resolve(pathName) : Paths.get(pathName);
            } catch (InvalidPathException x) {
                Log.caught(Io.class, f("in getDirectory, while getting Path from \"%s\"", pathName), x, false);
                return null;
            }
            
            if (Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
                Log.note(Io.class, "getDirectory", description, " is ", abs(path));
                return path;
            }
            
            try {
                Log.note(Io.class, "getDirectory", "attempting to create ", description, " at ", abs(path));
                return Files.createDirectory(path);
                
            } catch (FileAlreadyExistsException x) {
                Log.caught(Io.class, "getAppDirectory", x, true);
            } catch (IOException | UnsupportedOperationException x) {
                Log.caught(Io.class, f("in getDirectory, while creating %s at %s", description, abs(path)), x, false);
                return null;
            }
        }
    }
    
    public static Path getAppDirectory() {
        return getDirectory(null, APP_DIRECTORY, "application directory");
    }
    
    public static Path getMiscDirectory() {
        return getDirectory(getAppDirectory(), MISC_DIRECTORY, "miscellaneous directory");
    }
    
    private static Object readOrCreateNewFile(FileInfo info, Predicate<Object> test) {
        synchronized (MONITOR) {
            if (Log.caughtError() || info.error) {
                return NoFile.INSTANCE;
            }
            
            Pair<Object, Path> result = readOrCreateNewFile0(info.path, info.name, info.ext, test);
            
            Path path = result.right;
            if (path == null) {
                info.error();
            } else {
                info.path = path;
            }
            
            return result.left;
        }
    }
    
    private static Pair<Object, Path> readOrCreateNewFile0(Path path, String fileName, String fileExt, Predicate<Object> test) {
        Pair<Object, Path> result = Pair.of(NoFile.INSTANCE, null);
        
        if (path == null) {
            Path appDir = getAppDirectory();
            
            if (appDir == null) {
                return result;
            }
            
            for (int suffix = 0;; ++suffix) {
                String name = fileName + ((suffix > 0) ? suffix : "") + "." + fileExt;
                
                try {
                    path = appDir.resolve(name);
                } catch (InvalidPathException x) {
                    Log.caught(Io.class, "readOrCreateNewFile0", x, false);
                    return result;
                }
                
                if (Files.exists(path)) {
                    Log.note(Io.class, "readOrCreateNewFile0", "reading ", abs(path));
                    try {
                        byte[] bytes = Files.readAllBytes(path);
                        Object obj   = Json.fromBytes(bytes);
                        
                        if (test.test(obj)) {
                            return result.set(obj, path);
                        } else {
                            Log.note(Io.class, "readOrCreateNewFile0", coals(obj, Object::getClass), " failed: ", obj);
                        }
                        
                    } catch (IOException x) {
                        Log.caught(Io.class, "readOrCreateNewFile0", x, false);
                        return result;
                        
                    } catch (ClassCastException | IllegalArgumentException x) {
                        // We catch the IllegalArgumentException here because
                        // it may denote a file which is corrupt or not of our
                        // format.
                        // In that case we want to create a new file or see
                        // if there's another one.
                        Log.caught(Io.class, "readOrCreateNewFile0", x, false);
                    }
                } else {
                    break;
                }
            }
        }
        
        assert path != null;
        return result.set(NoFile.INSTANCE, path);
    }
    
    private static Path createBackup(FileInfo info) {
        if (!info.backup) {
            return null;
        }
        
        Path path = info.path;
        if (!Files.exists(path)) {
            return null;
        }
        
        Path file = path.getFileName();
        if (file == null) {
            return null;
        }
        
        String fileName = file.toString();
        Path   parent   = path.getParent();
        Path   backup;
        
        for (int suffix = 0;; ++suffix) {
            String backupName = fileName + ((suffix > 0) ? suffix : "") + ".backup";
            
            try {
                backup = parent.resolve(backupName);
            } catch (InvalidPathException x) {
                info.backup = false;
                Log.caught(Io.class, f("creating backup of %s at %s", abs(path), backupName), x, false);
                return null;
            }
            
            if (!Files.exists(backup)) {
                break;
            }
        }
        
        try {
            Log.note(Io.class, "createBackup", "backup up ", path, " at ", backup);
            Files.copy(path, backup);
        } catch (IOException x) {
            info.backup = false;
            Log.caught(Io.class, f("while copying from %s to %s", abs(path), abs(backup)), x, false);
            try {
                Files.deleteIfExists(backup);
            } catch (IOException y) {
                Log.caught(Io.class, "while deleting " + abs(backup), y, false);
            }
            return null;
        }
        
        return backup;
    }
    
    private static void write(Object obj, Gate gate, FileInfo info, Function<App, ?> appFn) {
        if (!Log.caughtError() && !info.error) {
            App.whenAvailable(app -> Io.write0(app, obj, gate, info, appFn));
        }
    }
    private static void write0(App app, Object obj, Gate gate, FileInfo info, Function<App, ?> appFn) {
        if ((appFn != null) && (appFn.apply(app) != obj)) {
            return;
        }
        
        Path path = info.path;
        
        if (path == null) {
            Log.note(Io.class, "write0", "no path; skipped writing instance of ", coals(obj, Object::getClass));
            return;
        }
        
        final class WriteJob extends AbstractJob {
            private final Object obj;
            private final Gate gate;
            
            private WriteJob(Object obj, Gate gate) {
                this.obj  = obj;
                this.gate = gate;
            }
            
            @Override
            protected void executeImpl() {
                write();
            }
            
            private void writeDirectly() {
                try {
                    write();
                } catch (RuntimeException x) {
                    Log.caught(WriteJob.class, "write", x, false);
                }
            }
            
            private void write() {
                Writer.run(this::write0);
            }
            
            private void write0() {
                if (Log.caughtError() || info.error) {
                    return;
                }
                Path      backup = createBackup(info);
                Throwable caught = null;
                try {
                    write1();
                } catch (Throwable x) {
                    caught = x;
                    try {
                        info.error();
                    } catch (Throwable y) {
                        info.error = true; // just in case
                        x.addSuppressed(y);
                    }
                    throw x;
                } finally {
                    if (backup != null) {
                        if (caught == null) {
                            try {
                                Files.deleteIfExists(backup);
                            } catch (IOException x) {
                                Log.caught(Io.class, "deleting backup at " + abs(backup), x, false);
                                info.backup = false;
                            }
                        } else {
                            // TODO: we could just switch the files automatically.
                            //       probably not though; what if we caught OOM?
                            App.whenAvailable(app -> app.addItemOnInterfaceThread(item -> {
                                String msg = f("An error occurred while writing to the file %s. "
                                              + "A backup of its previous value has been created at %s "
                                              + "in case the error caused the file to be corrupted. "
                                              + "No further attempts to write to this file will be "
                                              + "performed.",
                                               path.getFileName(), backup.toAbsolutePath());

                                TextAreaCell err = app.getComponentFactory().newTextArea(msg);
                                item.addCell(err.followTextStyle(Setting.UNEXPECTED_ERROR_STYLE));
                            }));
                        }
                    }
                }
            }
            
            private void write1() {
                if (obj instanceof Settings) {
                    ((Settings) obj).doSynchronized(this::write2);
                } else {
                    write2(obj);
                }
            }
            
            private void write2(Object obj) {
                if (gate != null) {
                    try (Gate gate0 = gate.block()) {
                        write3(obj);
                    }
                } else {
                    write3(obj);
                }
            }
            
            private void write3(Object obj) {
                // We don't catch the IllegalArgumentException here
                // because it would be abnormal.
                // In that case we want to crash/exit the program.
                byte[] bytes = Json.toBytes(obj, true);
                
                Log.note(Io.class, "write", "writing ", path.toAbsolutePath());
                try {
                    Files.write(path, bytes);
                } catch (IOException x) {
                    Log.caught(Io.class, "write", x, false);
                }
            }
            
            @Override
            protected void doneImpl() {
                if (isCancelled()) {
                    writeDirectly();
                }
            }
        }
        
        if (!Log.caughtError() && !info.error) {
            WriteJob job = new WriteJob(obj, gate);
            app().getWorker2().enqueue(job, true);
        }
    }
    
    static Settings.Concurrent readOrCreateNewSettingsFile() {
        Object result = Io.readOrCreateNewFile(SETTINGS_FILE, Settings.Concurrent.class::isInstance);
        
        Settings.Concurrent settings;
        
        if (result == NoFile.INSTANCE) {
            settings = new Settings.Concurrent();
            write(settings);
        } else {
            settings = (Settings.Concurrent) result;
            
            int newCount = 0;
            
            for (Setting<?> setting : Setting.values()) {
                if (!settings.contains(setting)) {
                    settings.putDefault(setting);
                    
                    if (!setting.isTransient()) {
                        ++newCount;
                    }
                }
            }
            
            if (newCount != 0) {
                write(settings);
            }
        }
        
        return settings;
    }
    
    static Symbols readOrCreateNewSymbolsFile(Symbols.Concurrent symbols) {
        Object result = Io.readOrCreateNewFile(SYMBOLS_FILE, Symbols.Concurrent.class::isInstance);
        
        if (result == NoFile.INSTANCE) {
            symbols.clear();
            symbols.putAll(Symbols.Concurrent.ofDefaults());
            write(symbols);
        } else {
            Symbols read = (Symbols) result;
            
            // If new intrinsics were added since the symbols were written,
            // we add the new aliases.
            List<Symbol> toAdd =
                symbols.stream()
                       .filter(sym -> !read.contains(sym))
                       .collect(Collectors.toList());
            for (Symbol intrinsic : toAdd)
                for (Symbol alias : intrinsic.createPreferredAliases())
                    for (Symbol sym : alias.getSymbols())
                        symbols.put(sym);
            
            symbols.putAll(read);
            
            if (!toAdd.isEmpty()) {
                write(symbols);
            }
        }
        
        return symbols;
    }
    
    private static boolean isListOfItems(Object obj) {
        if (obj instanceof List<?>) {
            for (Object e : (List<?>) obj) {
                if (!(e instanceof Item.Info)) {
                    Log.note(Io.class, "isListOfItems", "found ", coals(obj, Object::getClass), ": ", obj);
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    static List<Item.Info> readOrCreateNewItemsFile() {
        Object result = Io.readOrCreateNewFile(ITEMS_FILE, Io::isListOfItems);
        
        List<Item.Info> items;
        
        if (result == NoFile.INSTANCE) {
            items = new ArrayList<>(0);
            write(items);
        } else {
            items = (List<Item.Info>) result;
        }
        
        return items;
    }
    
    static void write(Settings.Concurrent settings) {
        write(settings, settings, SETTINGS_FILE, App::getSettings);
    }
    
    static void write(Symbols.Concurrent syms) {
        write(syms, syms, SYMBOLS_FILE, App::getSymbols);
    }
    
    public static Job copySymbolsTo(Path path, Consumer<? super String> onDone) {
        Objects.requireNonNull(path, "path");
        
        Job job = new AbstractJob() {
            private volatile String message;
            
            @Override
            protected void executeImpl() {
                FileInfo info = SYMBOLS_FILE;
                try {
                    if (info.error) {
                        message = "There was an error in a past attempt to read or write the symbols file.";
                        return;
                    }
                    if (info.path == null) {
                        message = "No symbols file to copy.";
                        return;
                    }
                    if (!Files.notExists(path)) {
                        message = "Specified file path may already exist.";
                        return;
                    }
                    try {
                        Files.copy(info.path, path);
                    } catch (IOException x) {
                        Log.caught(getClass(), "while copying symbols file", x, false);
                        message = "Copy failed: \"" + Misc.createSimpleMessage(x) + "\". "
                                + "The application log may have addition details.";
                    }
                } catch (Throwable x) {
                    message = Misc.createSimpleMessage(x);
                    throw x;
                }
            }
            
            @Override
            protected void doneImpl() {
                if (onDone != null) {
                    onDone.accept(message);
                }
            }
        };
        
        app().getWorker2().enqueue(job, true);
        return job;
    }
    
    static void write(ListDeque<Item> items) {
        try {
            write(app().getAsyncExecutor().executeNow(() -> {
                int             count = items.size();
                List<Item.Info> infos = new ArrayList<>(count);
                for (int i = 0; i < count; ++i) {
                    infos.add(items.get(i).getInfo());
                }
                return infos;
            }));
        } catch (AsyncExecutor.AsyncException fail) {
            Log.caught(Io.class, "write(ListDeque<Item>)", fail, true);
        }
    }
    
    static void write(List<Item.Info> items) {
        write(items, null, ITEMS_FILE, null);
    }
    
    static void createNewLogFile() {
        synchronized (MONITOR) {
            createNewLogFile0();
        }
    }
    
    private static void logFileFailure(String method, Throwable x) {
        Log.caught(Io.class, method, x, false);
        LOG_FILE_FAILURE = true;
    }
    
    private static final AtomicReference<LogWriter> LOG_WRITER = new AtomicReference<>(null);
    
    private static void createNewLogFile0() {
        if (EpsilonMain.isNoLogging())
            return;
        if (LOG_FILE_FAILURE)
            return;
        Path directory = getLogDirectory();
        if (directory == null)
            return;
        Log.note(Io.class, "createNewLogFile", "log directory is ", directory.toAbsolutePath());
        
        LocalDateTime     now  = LocalDateTime.now();
        DateTimeFormatter fmt  = DateTimeFormatter.ofPattern("yyyy-MM-dd-kk-mm-ss");
        String            date = now.format(fmt);
        
        Path path;
        
        for (int suffix = 0;; ++suffix) {
            String name = LOG_FILE_NAME + "_" + date;
            if (suffix > 0) {
                name += "_" + suffix;
            }
            name += "." + LOG_FILE_EXT;
            
            try {
                path = directory.resolve(Paths.get(name));
            } catch (InvalidPathException x) {
                logFileFailure("createNewFile0", x);
                return;
            }
            try {
                Path file = Files.createFile(path);
                path      = file;
                break;
            } catch (FileAlreadyExistsException x) {
                Log.caught(Io.class, "createNewFile0", x, true);
            } catch (IOException | UnsupportedOperationException x) {
                logFileFailure("createNewFile0", x);
                return;
            }
        }
        
        Log.note(Io.class, "createNewFile0", "log file is ", path.toAbsolutePath());
        
        BufferedWriter bufferedWriter;
        try {
            bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND);
        } catch (IOException | UnsupportedOperationException x) {
            logFileFailure("createNewFile0", x);
            return;
        }
        
        LogWriter logWriter = new LogWriter(bufferedWriter);
        if (LOG_WRITER.compareAndSet(null, logWriter)) {
            Runtime.getRuntime().addShutdownHook(new Thread(logWriter::close));
            logWriter.start();
        } else {
            Log.caught(Io.class, "createNewFile0", new Throwable("LOG_WRITER previously set"), false);
            logWriter.close();
        }
        
        notifyIfManyLogFiles(directory, path);
    }
    
    private static final class LogWriter implements Consumer<ListDeque<Log.Line>>, Runnable, AutoCloseable {
        private final PrintWriter writer;
        private final Thread      thread;
        
        private final Log.LineReceiver receiver;
        
        private LogWriter(BufferedWriter writer) {
            this.writer = new PrintWriter(Objects.requireNonNull(writer, "writer"));
            this.thread = App.newThread(this);
            
            thread.setDaemon(true);
            thread.setName(this.toString());
            
            receiver = Log.newLineReceiver(256);
        }
        
        private void start() {
            thread.start();
        }
        
        @Override
        public void close() {
            thread.interrupt();
            receiver.receive(this);
            writer.close();
        }
        
        @Override
        public void accept(ListDeque<Log.Line> lines) {
            PrintWriter writer = this.writer;
            
            int size = lines.size();
            for (int i = 0; i < size; ++i) {
                writer.println(lines.get(i).getLine());
            }
        }
        
        @Override
        public void run() {
            final long interval = 5000L;
            for (;;) {
                receiver.receive(this);
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException x) {
                    break;
                }
            }
        }
    }
    
    private static Path getLogDirectory() {
        Path path = getDirectory(getAppDirectory(), LOG_DIRECTORY, "log directory");
        // TODO?
//        if (path == null) {
//            LOG_FILE_FAILURE = true;
//        }
        return path;
    }
    
    private static void notifyIfManyLogFiles(Path directory, Path current) {
        App.whenAvailable(app -> {
            int max = app.getSetting(Setting.MAX_LOG_COUNT_BEFORE_NOTICE);
            try (Stream<Path> files = Files.list(directory)) {
                long count = files.count();
                if (count > max) {
                    app.addItemOnInterfaceThread(item -> {
                        eps.ui.TextAreaCell cell = app.getComponentFactory().newTextArea();
                        String text =
                            String.format("Note: The log directory '%s' has %d files. "
                                        + "The log files (with names matching %s_DATE-TIME.%s) may be safely deleted, "
                                        + "except for '%s' which is currently in use.",
                                        directory.toAbsolutePath(), count, LOG_FILE_NAME, LOG_FILE_EXT, current.getFileName());
                        
                        ComponentFactory fact = app.getComponentFactory();
                        if (fact.isOpenSupported()) {
                            text += " Enter 'open' as the next command to open the log directory.";
                            
                            app.addCommandFilter(cmd -> {
                                if ("open".equalsIgnoreCase(cmd)) {
                                    fact.openDirectory(directory);
                                    return App.CommandFilter.CONSUME | App.CommandFilter.DONE;
                                }
                                return App.CommandFilter.DONE;
                            });
                        }
                        
                        cell.setText(text);
                        cell.followTextStyle(Setting.NOTE_STYLE);
                        
                        item.addCell(cell);
                    });
                }
            } catch (IOException | UncheckedIOException x) {
                Log.caught(Io.class, "while counting the log files", x, false);
            }
        });
    }
    
    private static Path getThemeDirectory() {
        return getDirectory(getAppDirectory(), THEME_DIRECTORY, "theme directory");
    }
    
    static void readThemes() {
        List<Theme> themes;
        
        synchronized (MONITOR) {
            themes = readThemes0();
        }
        
        themes.forEach(Theme::put);
    }
    
    private static List<Theme> readThemes0() {
        List<Theme> themes = new ArrayList<>();
        
        Path dir = getThemeDirectory();
        if (dir != null) {
            try (Stream<Path> files = Files.list(dir)) {
                files.forEachOrdered(path -> {
                    Theme theme = readTheme(path);
                    if (theme != null) {
                        themes.add(theme);
                    }
                });
            } catch (IOException | UncheckedIOException x) {
                Log.caught(Io.class, "readThemes0", x, false);
            }
        }
        
        return themes;
    }
    
    private static Theme readTheme(Path path) {
        Path file = path.getFileName();
        
        if (file.toString().endsWith(OBJECT_FILE_EXT)) {
            try {
                Log.note(Io.class, "readTheme", "reading Theme from ", path.toAbsolutePath());
                Theme theme = (Theme) Json.fromFile(path);
                
                if (theme == null) {
                    Log.note(Io.class, "readTheme", "found null Theme in ", path.toAbsolutePath());
                }
                
                return theme;
                
            } catch (IOException | RuntimeException x) {
                Log.caught(Io.class, "while reading Theme file " + file, x, false);
            }
        } else {
            Log.note(Io.class, "readTheme", "found ", path.toAbsolutePath());
        }
        
        return null;
    }
    
    static void writeThemes() {
        Path dir = getThemeDirectory();
        if (dir != null) {
            Set<Path> files = new LinkedHashSet<>();
            
            for (Theme theme : Theme.values()) {
                if (!theme.isPreset()) {
                    Path path = writeTheme(theme);
                    if (path != null && (path = path.getFileName()) != null) {
                        files.add(path);
                    }
                }
            }
            
            try (Stream<Path> list = Files.list(dir)) {
                list.forEach(path -> {
                    Path file = path.getFileName();
                    if (files.contains(file))
                        return;
                    if (readTheme(path) == null)
                        return;
                    Log.note(Io.class, "writeThemes", "deleting unused Theme in ", path.toAbsolutePath());
                    try {
                        Files.delete(path);
                    } catch (IOException x) {
                        Log.caught(Io.class, "writeThemes", x, false);
                    }
                });
            } catch (IOException | RuntimeException x) {
                Log.caught(Io.class, "writeThemes", x, false);
            }
        }
    }
    
    private static Path writeTheme(Theme theme) {
        Path dir = getThemeDirectory();
        if (dir != null) {
            String name = theme.getName();
            String file = name + "." + OBJECT_FILE_EXT;
            
            Path path;
            try {
                path = dir.resolve(file);
            } catch (InvalidPathException x) {
                Log.caught(Io.class, "resolving " + file, x, false);
                return null;
            }
            
            Log.note(Io.class, "writeTheme", "writing Theme to ", path.toAbsolutePath());
            
            try {
                byte[] bytes = Json.toBytes(theme, true);
                Files.write(path, bytes);
                return path;
                
            } catch (IOException x) {
                Log.caught(Io.class, "writing " + file, x, false);
            }
        }
        return null;
    }
}