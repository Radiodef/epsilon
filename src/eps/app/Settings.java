/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import eps.util.*;
import eps.json.*;
import eps.ui.*;
import eps.io.*;
import static eps.app.App.*;

import java.util.*;
import java.util.Map.*;
import java.util.concurrent.*;

public interface Settings extends Gate, Copyable<Settings>, MiniSettings {
    boolean isEmpty();
    
    default <T> T reset(Setting<T> key) {
        return set(key, key.getDefaultValue());
    }
    
    <T> T set(Setting<T> key, T val);
    <T> T get(Setting<T> key);
    
    @Override
    default <T> T getSetting(Setting<T> key) {
        return get(key);
    }
    
    <T> T compute(Setting<T> key, java.util.function.Function<? super T, ? extends T> fn);
    
    boolean contains(Setting<?> key);
    
    Map<Setting<?>, Object> toMap();
    
    @Override
    default Settings copy() {
        return new Settings.CompactCopy(this, this.toMap());
    }
    
    Settings copyOf(Setting.Category... categories);
    
    void doSynchronized(java.util.function.Consumer<? super Settings> action);
    
    @Override
    Settings block();
    @Override
    Settings unblock();
    @Override
    Settings flush();
    @Override
    Settings forceUnsafeFlush();
    
    <T> Settings addChangeListener(Setting<T> key, ChangeListener<T> listener);
    
    default Settings addChangeListener(Setting<?> key, Runnable listener) {
        Objects.requireNonNull(key,      "key");
        Objects.requireNonNull(listener, "listener");
        
        return this.addChangeListener(key, (s, k, o, n) -> listener.run());
    }
    
    default <T> Settings addChangeListener(Setting<T> key, java.util.function.Consumer<? super T> listener) {
        Objects.requireNonNull(key,      "key");
        Objects.requireNonNull(listener, "listener");
        
        return this.addChangeListener(key, (s, k, o, n) -> listener.accept(n));
    }
    
    Settings removeChangeListener(Setting<?> key, ChangeListener<?> listener);
    
    @FunctionalInterface
    interface ChangeListener<T> {
        void settingChanged(Settings settings, Setting<T> key, T oldValue, T newValue);
    }
    
    @FunctionalInterface
    interface Function {
        <T> T get(Setting<T> key);
    }
    
    final class Concurrent implements Settings {
        private final ConcurrentMap<Setting<?>, Object> map = new ConcurrentHashMap<>();
        
        private final ConcurrentMap<Setting<?>, Set<ChangeListener<?>>> listeners = new ConcurrentHashMap<>();
        
        private final Gate.Consumer gate = new Gate.Consumer();
        
        // ConcurrentHashMap doesn't allow null values, so we put this in the map instead.
        private enum Null {
            INSTANCE;
            static Object wrap  (Object obj) { return obj == null ? INSTANCE : obj; }
            static Object unwrap(Object obj) { return obj == INSTANCE ? null : obj; }
        }
        
        public <T> void putDefault(Setting<T> setting) {
            T defaultVal = setting.getDefaultValue();
            map.put(setting, Null.wrap(defaultVal));
            
            for (Setting<?> depend : setting.getDependants()) {
                updateDependant(setting, depend, defaultVal, false);
            }
        }
        
        Concurrent() {
            for (Setting<?> key : Setting.values()) {
                putDefault(key);
            }
        }
        
        private Concurrent(Void $private) {
        }
        
        public static Concurrent empty() {
            return new Concurrent((Void) null);
        }
        
        public static Concurrent ofDefaults() {
            return new Concurrent();
        }
        
        static {
            JsonObjectAdapter.put(Settings.Concurrent.class,
                                  Settings.Concurrent::toJsonMap,
                                  Settings.Concurrent::new);
        }
        
        private static <T> Object toStorageValue(Setting<T> setting, Object val) {
            return setting.toStorageValue(setting.cast(Null.unwrap(val)));
        }
        
        private Map<?, ?> toJsonMap() {
            synchronized (gate.monitor()) {
                try (Gate gate0 = gate.block()) {
                    Map<Setting<?>, Object> src = map;
                    Map<Setting<?>, Object> dst = new LinkedHashMap<>(src.size());
                    
                    for (Entry<Setting<?>, Object> e : src.entrySet()) {
                        Setting<?> key = e.getKey();
                        Object     val = e.getValue();
                        
                        if (!key.isTransient()) {
                            dst.put(key, toStorageValue(key, val));
                        }
                    }
                    
                    return dst;
                }
            }
        }
        
        // Constructor for JsonObjectAdapter
        private Concurrent(Map<?, ?> map) {
            @SuppressWarnings("unchecked")
            Set<? extends Entry<? extends Setting<?>, ?>> entrySet =
                ((Map<? extends Setting<?>, ?>) map).entrySet();
            
            for (Entry<? extends Setting<?>, ?> e : entrySet) {
                this.putFromStorage(e.getKey(), e.getValue());
            }
            
            for (Entry<? extends Setting<?>, ?> e : entrySet) {
                Setting<?> key = e.getKey();
                for (Setting<?> depend : key.getDependants()) {
                    if (!this.contains(depend)) {
                        this.updateDependant(key, depend, this.get(key), false);
                    }
                }
            }
        }
        
        private <T> void putFromStorage(Setting<T> key, Object val0) {
            val0 = Null.unwrap(val0);
            try {
                this.map.put(key, Null.wrap( key.fromStorageValue(val0) ));
            } catch (IllegalArgumentException | NullPointerException | ClassCastException x) {
                Log.caught(Settings.class, "putFromStorage", x, true);
                this.map.put(key, Null.wrap( key.getDefaultValue() ));
            }
        }
        
        private boolean updateDependants(Setting<?> key, Object val, boolean notify) {
            boolean modified = false;
            
            for (Setting<?> depend : key.getDependants()) {
                if (updateDependant(key, depend, val, notify))
                    modified = true;
            }
            
            return modified;
        }
        
        private <T, U> boolean updateDependant(Setting<T> key, Setting<U> depend, Object val, boolean notify) {
            U newVal = key.toDependantValue(depend, key.cast(val));
            U oldVal = setDirect(depend, newVal);
            if (notify) {
                notifyListeners(depend, oldVal, newVal);
            }
            return !Objects.equals(oldVal, newVal);
        }
        
        @Override
        public boolean hasActionsWaiting() {
            return gate.hasActionsWaiting();
        }
        
        @Override
        public boolean isBlocked() {
            return gate.isBlocked();
        }
        
        @Override
        public Settings block() {
            gate.block();
            return this;
        }
        
        @Override
        public Settings unblock() {
            gate.unblock();
            return this;
        }
        
        @Override
        public Settings flush() {
            gate.flush();
            return this;
        }
        
        @Override
        public Settings forceUnsafeFlush() {
            gate.forceUnsafeFlush();
            return this;
        }
        
        @Override
        public boolean contains(Setting<?> key) {
            return key != null && map.containsKey(key);
        }
        
        @Override
        public void doSynchronized(java.util.function.Consumer<? super Settings> action) {
            synchronized (gate.monitor()) {
                action.accept(this);
            }
        }
        
        @Override
        public boolean isEmpty() {
            return map.isEmpty();
        }
        
        @Override
        public <T> T get(Setting<T> key) {
            Objects.requireNonNull(key, "key");
            // Lock-free.
            return key.cast( Null.unwrap( map.get(key) ) );
        }
        
        private <T> T setDirect(Setting<T> key, T val) {
            return key.cast( Null.unwrap( map.put(key, Null.wrap(val)) ) );
        }
        
        private <T> T setAndNotify(Setting<T> key, T val) {
            val = key.toImmutableValue(val);
            
            synchronized (gate.monitor()) {
                T old = setDirect(key, val);
                notifyListeners(key, old, val);
                
                boolean dependantsUpdated =
                    updateDependants(key, val, true);
                
                if (!gate.hasActionsWaiting() && (dependantsUpdated || !Objects.equals(old, val))) {
                    PersistentFiles.writeSettings(this);
                }
                
                return old;
            }
        }
        
        @Override
        public <T> T set(Setting<T> key, T val) {
            Objects.requireNonNull(key, "key");
            key.requireValidValue(val);
            
            synchronized (gate.monitor()) {
                T old = get(key);
                
                gate.accept(() -> this.setAndNotify(key, val));
                
                return old;
            }
        }
        
        @Override
        public <T> T compute(Setting<T> key, java.util.function.Function<? super T, ? extends T> fn) {
            Objects.requireNonNull(key, "key");
            Objects.requireNonNull(fn,  "fn");
            
            synchronized (gate.monitor()) {
                T old = get(key);
                T val = fn.apply(old);
                
                if (old != val) {
                    key.requireValidValue(val);
                    gate.accept(() -> this.setAndNotify(key, val));
                }
                
                return old;
            }
        }
        
        @Override
        public Map<Setting<?>, Object> toMap() {
            synchronized (gate.monitor()) {
                Map<Setting<?>, Object> copy = new HashMap<>(map.size());
                
                for (Entry<Setting<?>, Object> e : map.entrySet()) {
                    copy.put(e.getKey(), Null.unwrap( e.getValue() ));
                }
                
                return copy;
            }
        }
        
        @Override
        public Settings copyOf(Setting.Category... categories) {
            if (categories == null || categories.length == 0) {
                return new Settings.CompactCopy(this, Collections.emptyMap());
            }
            
            synchronized (gate.monitor()) {
                Map<Setting<?>, Object> copy = new HashMap<>();
                
                for (Entry<Setting<?>, Object> e : map.entrySet()) {
                    Setting<?> key = e.getKey();
                    if (key.isInAnyOf(categories)) {
                        copy.put(key, Null.unwrap( e.getValue() ));
                    }
                }
                
                return new Settings.CompactCopy(this, copy);
            }
        }
        
        private transient List<Runnable> changes = null;
        private transient boolean isNotifying = false;
        
        @Override
        public <T> Settings addChangeListener(Setting<T> key, ChangeListener<T> listener) {
            Objects.requireNonNull(key,      "key");
            Objects.requireNonNull(listener, "listener");
            
            synchronized (gate.monitor()) {
                if (isNotifying) {
                    if (changes == null) {
                        changes = new ArrayList<>(2);
                    }
                    changes.add(() -> this.addChangeListener(key, listener));
                } else {
                    // Note: don't actually need a CopyOnWriteArrayList with the synchronization.
                    Set<ChangeListener<?>> set = listeners.computeIfAbsent(key, k -> new CopyOnWriteArraySet<>());
                    set.add(listener);
                }
            }
            return this;
        }
        
        @Override
        public Settings removeChangeListener(Setting<?> key, ChangeListener<?> listener) {
            Objects.requireNonNull(key,      "key");
            Objects.requireNonNull(listener, "listener");
            
            synchronized (gate.monitor()) {
                if (isNotifying) {
                    if (changes == null) {
                        changes = new ArrayList<>(2);
                    }
                    changes.add(() -> this.removeChangeListener(key, listener));
                } else {
                    Set<ChangeListener<?>> list = listeners.get(key);
                    
                    if (list != null) {
                        list.remove(listener);
                    }
                }
            }
            
            return this;
        }
        
        @SuppressWarnings("unchecked")
        private <T> void notifyListeners(Setting<T> key, T oldVal, T newVal) {
            synchronized (gate.monitor()) {
                isNotifying = true;
                Throwable flight = null;
                try {
                    Set<ChangeListener<?>> list = listeners.get(key);
                    
                    if (list != null) {
                        for (ChangeListener<?> listener : list) {
                            ((ChangeListener<T>) listener).settingChanged(this, key, oldVal, newVal);
                        }
                    }
                } catch (Throwable x) {
                    flight = x;
                    throw x;
                } finally {
                    isNotifying = false;
                    try {
                        if (changes != null) {
                            changes.forEach(Runnable::run);
                            changes = null;
                        }
                    } catch (Throwable x) {
                        if (flight != null) {
                            x.addSuppressed(flight);
                        }
                        throw x;
                    }
                }
            }
        }
        
        @Override
        public String toString() {
            return map.toString();
        }
    }
    
    // immutable
    final class CompactCopy implements Settings {
        private final Settings settings;
        
        private final Map<Setting<?>, Object> map;
        
        private CompactCopy(Settings settings, Map<Setting<?>, Object> map) {
            this.settings = Objects.requireNonNull(settings, "settings");
            this.map      = Objects.requireNonNull(map,      "map");
        }
        
        static {
            JsonObjectAdapter.put(CompactCopy.class,
                                  CompactCopy::toJsonMap,
                                  CompactCopy::fromJsonMap);
        }
        
        private enum TransientDependant { INSTANCE }
        
        private static <T> Object toStorage(Setting<T> key, Object val) {
            return key.toStorageValue(key.cast(val));
        }
        
        private Map<?, ?> toJsonMap() {
            Map<Object, Object> adapted = new LinkedHashMap<>();
            
            if (settings != app().getSettings()) {
                adapted.put("settings", settings);
            }
            for (Entry<Setting<?>, Object> e : map.entrySet()) {
                Setting<?> key = e.getKey();
                Object     val = e.getValue();
                if (key.isTransient()) {
                    Setting<?> indy = key.getIndependant();
                    if (map.containsKey(indy)) {
                        adapted.put(key, TransientDependant.INSTANCE);
                        continue;
                    }
                }
                adapted.put(key, toStorage(key, val));
            }
            
            return adapted;
        }
        
        private static <T, U> U toDependant(Setting<T> indy, Setting<U> dep, Object val) {
            return indy.toDependantValue(dep, indy.cast(val));
        }
        
        private static CompactCopy fromJsonMap(Map<?, ?> adapted) {
            Settings settings = (Settings) adapted.remove("settings");
            if (settings == null) {
                settings = app().getSettings();
            }
            
            Map<Setting<?>, Object> map = new LinkedHashMap<>(adapted.size());
            for (Entry<?, ?> e : adapted.entrySet()) {
                Setting<?> key = (Setting<?>) e.getKey();
                Object     val = e.getValue();
                if (val != TransientDependant.INSTANCE) {
                    try {
                        val = key.fromStorageValue(val);
                    } catch (IllegalArgumentException | NullPointerException | ClassCastException x) {
                        val = key.getDefaultValue();
                        Log.caught(CompactCopy.class, "fromJsonMap", x, true);
                    }
                }
                map.put(key, val);
            }
            map.replaceAll((key, val) -> {
                if (val == TransientDependant.INSTANCE) {
                    Setting<?> indy = key.getIndependant();
                    Object     val0 = map.get(indy);
                    try {
                        return toDependant(indy, key, val0);
                    } catch (IllegalArgumentException | NullPointerException | ClassCastException x) {
                        Log.caught(CompactCopy.class, "fromJsonMap", x, true);
                        return key.getDefaultValue();
                    }
                }
                return val;
            });
            
            return new CompactCopy(settings, map);
        }
        
        @Override
        public Map<Setting<?>, Object> toMap() {
            Map<Setting<?>, Object> copy = settings.toMap();
            copy.putAll(map);
            return copy;
        }
        
        @Override
        public Settings copyOf(Setting.Category... cats) {
            Map<Setting<?>, Object> copy = new HashMap<>();
            
            for (Entry<Setting<?>, Object> e : map.entrySet()) {
                if (e.getKey().isInAnyOf(cats)) {
                    copy.put(e.getKey(), e.getValue());
                }
            }
            
            return new CompactCopy(settings, copy);
        }
        
        @Override
        public boolean contains(Setting<?> key) {
            return (key != null) && (map.containsKey(key) || settings.contains(key));
        }
        
        @Override
        public boolean isEmpty() {
            return map.isEmpty() && settings.isEmpty();
        }
        
        @Override
        public <T> T get(Setting<T> key) {
            T val = key.cast( map.get(key) );
            if (val == null) {
                if (!map.containsKey(key)) {
                    return settings.get(key);
                }
            }
            return val;
        }
        
        @Override
        public <T> T set(Setting<T> key, T val) {
            throw new UnsupportedOperationException("<T> T set(Setting<T>, T)");
        }
        
        @Override
        public <T> T compute(Setting<T> key, java.util.function.Function<? super T, ? extends T> fn) {
            throw new UnsupportedOperationException("<T> T compute(Setting<T>, Function<? super T, ? extends T>)");
        }
        
//        @Override
//        public void write() {
//            throw new UnsupportedOperationException("void write()");
//        }
        
        @Override
        public boolean hasActionsWaiting() {
            return false;
        }
        
        @Override
        public boolean isBlocked() {
            return false;
        }
        
        @Override
        public Settings block() {
            return this;
        }
        
        @Override
        public Settings unblock() {
            return this;
        }
        
        @Override
        public Settings flush() {
            return this;
        }
        
        @Override
        public Settings forceUnsafeFlush() {
            return this;
        }
        
        @Override
        public void doSynchronized(java.util.function.Consumer<? super Settings> action) {
            action.accept(this);
        }
        
        @Override
        public <T> Settings addChangeListener(Setting<T> key, ChangeListener<T> cl) {
            return this;
        }
        
        @Override
        public Settings removeChangeListener(Setting<?> key, ChangeListener<?> cl) {
            return this;
        }
        
        @Override
        public String toString() {
            return map.toString();
        }
    }
    
    abstract class WeakListener<T, R> implements ChangeListener<T> {
        private final java.lang.ref.WeakReference<R> ref;
        
        protected WeakListener(R ref) {
            Objects.requireNonNull(ref, "ref");
            this.ref = new java.lang.ref.WeakReference<>(ref);
        }
        
        protected final R ref() {
            R ref = this.ref.get();
            if (ref == null) {
                throw new IllegalStateException(this.toString());
            }
            return ref;
        }
        
        @Override
        public final void settingChanged(Settings settings, Setting<T> key, T oldValue, T newValue) {
            R referant = ref.get();
            if (referant != null) {
                settingChanged(referant, settings, key, oldValue, newValue);
            } else {
                Log.note(WeakListener.class, "settingChanged", this, " was never removed");
                settings.removeChangeListener(key, this);
            }
        }
        
        protected abstract void settingChanged(R          referant,
                                               Settings   settings,
                                               Setting<T> key,
                                               T          oldValue,
                                               T          newValue);
    }
    
    final class Listeners implements AutoCloseable {
        private final Map<Setting<?>, Set<ChangeListener<?>>> map = new LinkedHashMap<>();
        private final Settings settings;
        
        public Listeners() {
            this(app());
        }
        
        public Listeners(App app) {
            this(Objects.requireNonNull(app, "app").getSettings());
        }
        
        public Listeners(Settings settings) {
            this.settings = Objects.requireNonNull(settings, "settings");
        }
        
        public synchronized <T> Listeners add(Setting<T> setting, ChangeListener<T> listener) {
            if (map.computeIfAbsent(setting, key -> new ArraySet<>(2)).add(listener)) {
                settings.addChangeListener(setting, listener);
            }
            return this;
        }
        
        public synchronized <T> Listeners addAndSet(Setting<T> setting, ChangeListener<T> listener) {
            this.add(setting, listener);
            T val = settings.get(setting);
            listener.settingChanged(settings, setting, val, val);
            return this;
        }
        
        public synchronized Listeners remove(Setting<?> setting, ChangeListener<?> listener) {
            Set<ChangeListener<?>> set = map.get(setting);
            if (set != null && set.remove(listener)) {
                settings.removeChangeListener(setting, listener);
            }
            return this;
        }
        
        public synchronized void removeAll(Setting<?> setting) {
            Set<ChangeListener<?>> set = map.remove(setting);
            if (set != null) {
                set.forEach(listener -> settings.removeChangeListener(setting, listener));
            }
        }
        
        public synchronized void removeAll() {
            map.forEach((key, list) -> list.forEach(listener -> settings.removeChangeListener(key, listener)));
            map.clear();
        }
        
        @Override
        public synchronized void close() {
            this.removeAll();
        }
        
        @Override
        public synchronized String toString() {
            return map.toString();
        }
    }
    
    final class AppSettingsInterimConverter extends InterimConverter<Settings, Settings> {
        public static final String NAME = "eps.app.Settings$AppSettingsInterimConverter";
        public AppSettingsInterimConverter() {
            super(Settings.class, Settings.class);
        }
        @Override
        public Settings toInterim(Object obj) {
            return (obj == app().getSettings()) ? null : (Settings) obj;
        }
        @Override
        public Settings fromInterim(Object obj) {
            return (obj == null) ? app().getSettings() : (Settings) obj;
        }
    }
    
    static <T> T getOrDefault(Setting<T> key) {
        Objects.requireNonNull(key, "key");
        return MiniSettings.instance().getSetting(key);
//        try {
//            return app().getSetting(key);
//        } catch (IllegalStateException x) {
//            return key.getDefaultValue();
//        }
    }
    
    static int getDecimalMark() {
        return getOrDefault(Setting.DECIMAL_MARK).intValue();
    }
    
    static CodePoint getDigitSeparator() {
        return getOrDefault(Setting.DIGIT_SEPARATOR);
    }
    
    static int getMaxFractionDigits() {
        return getOrDefault(Setting.MAX_FRACTION_DIGITS);
    }
    
    static String getEditorIndent() {
        String indent = getOrDefault(Setting.EDITOR_INDENT);
        return Misc.unquote(indent);
    }
    
    static int getMinorMargin() {
        return getOrDefault(Setting.MINOR_MARGIN);
    }
    
    static int getMajorMargin() {
        return getOrDefault(Setting.MAJOR_MARGIN);
    }
    
    static float getBasicStyleSize() {
        Style style = getBasicStyle();
        
        if (!style.isSizeSet() || !style.getSize().isAbsolute()) {
            style = app().getComponentFactory().getDefaultStyle();
        }
        
        return style.getSize().getAbsoluteSize();
    }
    
    static float getAbsoluteSize(Setting<Style> setting) {
        Objects.requireNonNull(setting, "setting");
        Style style = getOrDefault(setting);
        if (style != null) {
            Size size = style.getSize();
            if (size != null) {
                return size.getAbsoluteSize(Settings::getBasicStyleSize);
            }
        }
        return getBasicStyleSize();
    }
    
    static float getAbsoluteSize(Style style) {
        if (style != null && style.isSizeSet()) {
            return style.getSize().getAbsoluteSize(Settings::getBasicStyleSize);
        } else {
            return getBasicStyleSize();
        }
    }
    
    static Style getBasicStyle() {
        return getOrDefault(Setting.BASIC_STYLE);
    }
    
    static <T> T getAbsolute(Setting<Style> setting, java.util.function.Function<Style, T> fn) {
        Objects.requireNonNull(setting, "setting");
        Objects.requireNonNull(fn,      "fn");
        
        Style style = getOrDefault(setting);
        
        if (style != null) {
            T val = fn.apply(style);
            if (val != null) {
                return val;
            }
        }
        
        if (setting == Setting.BASIC_STYLE) {
            return fn.apply(app().getComponentFactory().getDefaultStyle());
        } else {
            return getAbsolute(Setting.BASIC_STYLE, fn);
        }
    }
    
    static <T> T getAbsolute(Style style, java.util.function.Function<Style, T> fn) {
        Objects.requireNonNull(fn, "fn");
        
        if (style != null) {
            T val = fn.apply(style);
            if (val != null) {
                return val;
            }
        }
        
        return getAbsolute(Setting.BASIC_STYLE, fn);
    }
}