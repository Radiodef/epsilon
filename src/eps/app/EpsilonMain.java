/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import eps.ui.*;
import eps.util.*;

import java.util.*;
import java.io.PrintStream;

public final class EpsilonMain {
    private EpsilonMain() {}
    
    private static volatile Set<String> args = Collections.emptySet();
    
    public static Set<String> getArguments() {
        Set<String> args = EpsilonMain.args;
        assert args != null;
        return args;
    }
    
    public static boolean isNoLogging() {
        return getArguments().contains("nolog");
    }
    
    public static boolean isSandbox() {
        return getArguments().contains("sandbox");
    }
    
    public static boolean isDeveloper() {
        return getArguments().contains("dev");
    }
    
//    @Deprecated
//    public static String getLogFilter() {
//        Iterator<String> it = getArguments().iterator();
//        while (it.hasNext()) {
//            if ("filter".equals(it.next())) {
//                if (it.hasNext()) {
//                    return it.next();
//                }
//            }
//        }
//        return null;
//    }
    
    private static volatile boolean appLaunchedSuccessfully = true;
    
    public static boolean appLaunchedSuccessfully() {
        return appLaunchedSuccessfully;
    }
    
    public enum UserInterfaceType {
        NONE, SWING, FX;
    }
    
    private static volatile UserInterfaceType userInterfaceType = UserInterfaceType.NONE;
    
    public static UserInterfaceType getUserInterfaceType() {
        return userInterfaceType;
    }
    
    public static boolean isSwing() {
        return userInterfaceType == UserInterfaceType.SWING;
    }
    
    public static boolean isFx() {
        return userInterfaceType == UserInterfaceType.FX;
    }
    
    private static volatile boolean mainEntered = false;
    
    public static boolean mainEntered() {
        return mainEntered;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        mainEntered = true;
        
        Set<String> argsSet = new LinkedHashSet<>(2 * args.length);
        Collections.addAll(argsSet, args);
        EpsilonMain.args = Collections.unmodifiableSet(argsSet);
        
        // Note: Log <clinit> sets the Thread.UncaughtExceptionHandler
        Log.note(EpsilonMain.class, "main", "Program starting...");
        Log.note(EpsilonMain.class, "main", "arguments = ", argsSet);
        
        try {
            if (isSandbox()) {
                sandboxMain();
            } else if (argsSet.contains("swingui")) {
                swingMain();
            } else if (argsSet.contains("fxui")) {
                fxMain();
            } else {
                defaultMain();
            }
        } catch (Throwable x) {
            appLaunchedSuccessfully = false;
            throw x;
        }
    }
    
    private static void defaultMain() {
        Log.entering(EpsilonMain.class, "defaultMain");
        swingMain();
    }
    
    private static void sandboxMain() {
        Log.entering(EpsilonMain.class, "sandboxMain");
        PrintStream out = System.out;
        //
        // put whatever here to run instead of the app
        //
    }
    
    private static final class Launcher implements Runnable {
        private final Class<?> componentFactoryClass;
        
        private Launcher(Class<?> componentFactoryClass) {
            this.componentFactoryClass = Objects.requireNonNull(componentFactoryClass);
        }
        
        private Launcher(String componentFactoryClassName) throws ClassNotFoundException {
            this(Class.forName(componentFactoryClassName));
        }
        
        @Override
        public void run() {
            Log.entering(getClass(), "run");
            try {
                App app = App.initialize(newComponentFactory());
                app.getMainWindow().setVisible(true);
            } catch (Throwable x) {
                appLaunchedSuccessfully = false;
                throw x;
            }
        }
        
        private ComponentFactory newComponentFactory() {
            try {
                return (ComponentFactory) componentFactoryClass.getConstructor().newInstance();
            } catch (ReflectiveOperationException x) {
                throw new UncheckedReflectiveOperationException(x);
            }
        }
    }
    
    private static void fxMain() {
        Log.entering(EpsilonMain.class, "fxMain");
        userInterfaceType = UserInterfaceType.FX;
        
        try {
            // FxApplication.launch(new Launcher(FxComponentFactory.class));
            Class.forName("eps.ui.fx.FxApplication")
                 .getMethod("launch", Runnable.class)
                 .invoke(null, new Launcher("eps.ui.fx.FxComponentFactory"));
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
    
    private static void swingMain() {
        Log.entering(EpsilonMain.class, "swingMain");
        userInterfaceType = UserInterfaceType.SWING;
        
        try {
            // SwingMisc.preconfigure();
            Class.forName("eps.ui.swing.SwingMisc")
                 .getMethod("preconfigure")
                 .invoke(null);
            // SwingUtilities.invokeLater(new Launcher(SwingComponentFactory.class));
            Class.forName("javax.swing.SwingUtilities")
                 .getMethod("invokeLater", Runnable.class)
                 .invoke(null, new Launcher("eps.ui.swing.SwingComponentFactory"));
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
}