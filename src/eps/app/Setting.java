/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import eps.util.*;
import eps.eval.*;
import eps.json.*;
import eps.ui.*;
import eps.ui.Style;
import eps.math.MathEnv;
import eps.math.MathEnv64;
import static eps.util.Misc.*;
import static eps.math.Math2.*;

import java.util.*;
import java.util.Map.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.util.stream.*;
import java.lang.reflect.*;

public final class Setting<T> implements Comparable<Setting<?>> {
    private static final Object           MONITOR     = new Object();
    private static       int              nextOrdinal = 0;
    private static final List<Setting<?>> VALUES      = new CopyOnWriteArrayList<>();
    private static final List<Setting<?>> VALUES_VIEW = Collections.unmodifiableList(VALUES);
    private static final ConcurrentMap<String, Setting<?>> BY_NAME = new ConcurrentHashMap<>();
    
    private static final BiPredicate<? extends Setting<?>, ?> DEFAULT_VALIDATOR =
        (setting, obj) -> setting.type.isInstance(obj);
    @SuppressWarnings("unchecked")
    private static <T> BiPredicate<Setting<T>, Object> defaultValidator() {
        return (BiPredicate<Setting<T>, Object>) DEFAULT_VALIDATOR;
    }
    
    @SuppressWarnings("unchecked")
    public static final Class<Setting<?>> TYPE =
        (Class<Setting<?>>) (Class<? super Setting<?>>) Setting.class;
    
    static {
        ToStringConverter<Setting<?>> converter =
            ToStringConverter.create(Setting.TYPE,
                                     Setting::getName,
                                     Setting::valueOfOrDeclare);
        ToStringConverter.put(converter);
    }
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    
    public enum Category {
        APP,
        UI,
        EDIT,
        EVAL,
        MATH,
        ERROR;
        
        public List<Setting<?>> settings() {
            return Setting.values().stream().filter(s -> s.isIn(this)).collect(Collectors.toList());
        }
    }
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    
    public static final Setting<String> APPLICATION_VERSION =
        builder().setName("applicationVersion")
                 .setType(String.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofString())
                 .setDefaultValue("0.8")
                 .setDescription("The current application version.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<String> APPLICATION_NAME =
        builder().setName("applicationName")
                 .setType(String.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofString())
                 .setDefaultValue("Epsilon")
                 .setDescription("The name of the application which appears in the window title bar.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Double> DEFAULT_WINDOW_AREA_RATIO =
        builder().setName("defaultWindowAreaRatio")
                 .setType(Double.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.inClosed(0.125, 1.0))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Double.class))
                 .setDefaultValue(0.25)
                 .setDescription("The application window is initially sized using this ratio in proportion to the available screen space.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<BoundingBox> MAIN_WINDOW_BOUNDS =
        builder().setName("mainWindowBounds")
                 .setType(BoundingBox.class)
                 .allowNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(BoundingBox.class, "parse"))
                 .setDefaultValue(null)
                 .setDescription("The location and size of the application window.")
                 .addCategory(Category.UI)
                 .setToStringFunction(BoundingBox::toJsonString)
                 .declare();
    
    public static final Setting<Boolean> MAIN_WINDOW_MAXIMIZED =
        builder().setName("mainWindowMaximized")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(false)
                 .setDescription("True if the application window is maximized and false if it's windowed or full screen.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Boolean> MAIN_WINDOW_FULL_SCREEN =
        builder().setName("mainWindowFullScreen")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(false)
                 .setDescription("True if the application window is full screen and false if it's windowed or maximized.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Boolean> MAIN_WINDOW_ALWAYS_ON_TOP =
        builder().setName("mainWindowAlwaysOnTop")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(true)
                 .setDescription("True if the application window is always on top of all other windows and false otherwise.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Integer> MINOR_MARGIN =
        builder().setName("minorMargin")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.in(0, 128))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(8)
                 .setDescription("The size of the UI minor margin, used for example as the space between adjacent cells.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Integer> MAJOR_MARGIN =
        builder().setName("majorMargin")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.in(0, 128))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(12)
                 .setDescription("The size of the UI major margin, used for example as the space between cells and the "
                               + "edge of the scroll pane.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Boolean> MULTILINE_MODE =
        builder().setName("multilineMode")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(false)
                 .setDescription("True if the entry text area allows multiple lines and false if it allows only a single line.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Double> MULTILINE_ENTRY_HEIGHT =
        builder().setName("multilineEntryHeight")
                 .setType(Double.class)
                 .allowNull()
                 .setValidator(RangeValidator.in(0.0, (double) Short.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Double.class).nullable())
                 .setDefaultValue(null)
                 .setDescription("The height of the entry text area in multiline mode.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Integer> MAX_ITEMS =
        builder().setName("maxItems")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.in(1, Integer.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(100)
                 .setDescription("The maximum number of cells (for example, calculations) remembered by the "
                               + "application and displayed in the history pane. Larger values will require "
                               + "more memory and disk space.")
                 .addCategory(Category.UI)
                 .addCategory(Category.APP)
                 .declare();
    
    public static final Setting<Integer> MAX_LOG_COUNT_BEFORE_NOTICE =
        builder().setName("maxLogCountBeforeNotice")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.in(1, Integer.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(100)
                 .setDescription("The maximum number of log files in the log directory before the user gets a notice when the program starts.")
                 .addCategory(Category.APP)
                 .declare();
    
    public static final Setting<Integer> EXIT_TIMEOUT =
        builder().setName("exitTimeout")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.in(1, Integer.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(20_000)
                 .setDescription("If the application hangs for some reason while exiting, it will be terminated after this many milliseconds.")
                 .addCategory(Category.APP)
                 .declare();
    
    public static final Setting<Integer> WORKER_HALTING_TIMEOUT =
        builder().setName("workerHaltingTimeout")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.inClosed(1, Integer.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(15_000)
                 .setDescription("The time in milliseconds which a single job (for example a calculation) may run "
                               + "for before a notification is shown warning that the job may be halting.")
                 .addCategory(Category.APP)
                 .declare();
    
    public static final Setting<Integer> ITEM_CHANGE_TIMEOUT =
        builder().setName("itemChangeTimeout")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.inClosed(0, Integer.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(3_000)
                 .setDescription("The time in milliseconds which the application waits before writing the items "
                               + "file to disk. This timeout prevents multiple writes from occurring when multiple "
                               + "changes are made within a narrow timescale.")
                 .addCategory(Category.APP)
                 .declare();
    
    public static final Setting<Integer> LO_FREQ_LOG_INTERVAL =
        builder().setName("loFreqLogInterval")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.inClosed(0, Integer.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(10_000)
                 .setDescription("How often the debug window (if it's open) checks for more log messages "
                               + "when there hasn't been a recent message.")
                 .addCategory(Category.APP)
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Integer> HI_FREQ_LOG_INTERVAL =
        builder().setName("hiFreqLogInterval")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.inClosed(0, Integer.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(500)
                 .setDescription("How often the debug window (if it's open) checks for more log messages "
                               + "when there's been a recent message.")
                 .addCategory(Category.APP)
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<IntSet> BOUNDARY_CHARS_AS_INT =
        builder().setName("boundaryCharsAsInt")
                 .setType(IntSet.class)
                 .requireNonNull()
                 .defaultValidator()
                 .setCommonFunction(IntHashSet::new)
                 .setImmutableFunction(Parser.Stuff::toImmutableIntSet)
                 .identityStorage()
                 .setParser(ObjectParser.ofSimple(IntHashSet.class))
                 .setDefaultValue(Parser.Stuff.DEFAULT_BOUNDARIES_AS_INT)
//                 .setDefaultValue(IntSet.empty())
                 .setDescription("The set of boundary characters in a table for optimized lookup. This setting "
                               + "should not be set directly because it's computed automatically when the boundaryChars "
                               + "setting is changed.")
                 .addCategory(Category.EVAL)
                 .setToStringFunction(chars -> j(chars, " ", Object::toString))
                 .setTransient(true)
                 .declare();
    
    public static final Setting<Set<CodePoint>> BOUNDARY_CHARS =
        builder().setName("boundaryChars")
                 .setType(Misc.SET_OF_CODE_POINT)
                 .requireNonNull()
                 .setValidator((self, obj) -> {
                     if (obj instanceof Set<?>) {
                         for (Object e : (Set<?>) obj)
                             if (!(e instanceof CodePoint))
                                 return false;
                         return true;
                     }
                     return false;
                 })
                 .setCommonFunction(LinkedHashSet::new)
                 .setImmutableFunction(Parser.Stuff::toImmutableCodePointSet)
                 .setToStorageFunction(Parser.Stuff::toString)
                 .setFromStorageFunction(Parser.Stuff::toCodePointSet)
                 .setParser(ObjectParser.ofSimple(CodePoint.class).asCollection(LinkedHashSet::new))
                 .setDefaultValue(Parser.Stuff.DEFAULT_BOUNDARIES)
                 .setDescription("The set of Unicode code points which the parser uses to initially split "
                               + "a mathematical expression in to tokens. For example, the expression 2+2 is "
                               + "interpreted as the three tokens 2, + and 2 by default, because + is a boundary "
                               + "character. Were + not a boundary character, the expression would be interpreted "
                               + "as a single token 2+2. In such a case, whitespace would be necessary for the "
                               + "expression to be interpreted in the expected way. Generally speaking, operators "
                               + "should be boundary characters.")
                 .addCategory(Category.EVAL)
                 .addDependant(Setting.BOUNDARY_CHARS_AS_INT, (self, to, from) -> Parser.Stuff.toIntSet(from))
                 .setElementParser(ObjectParser.ofSimple(CodePoint.class))
                 .setToStringFunction(chars -> j(chars, " ", CodePoint::toString))
                 .declare();
    
    public static final Setting<CodePoint> LEFT_PARENTHESIS =
        builder().setName("leftParenthesis")
                 .setType(CodePoint.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(CodePoint.class))
                 .setDefaultValue(CodePoint.valueOf('('))
                 .setDescription("The character inserted as a left parenthesis (bracket) by some display routines. "
                               + "Note that this setting only has an effect on how some expressions are displayed, "
                               + "such as \"sin -1\" which is displayed as \"sin(-1)\".")
                 .addCategory(Category.EVAL)
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<CodePoint> RIGHT_PARENTHESIS =
        builder().setName("rightParenthesis")
                 .setType(CodePoint.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(CodePoint.class))
                 .setDefaultValue(CodePoint.valueOf(')'))
                 .setDescription("The character inserted as a left parenthesis (bracket) by some display routines. "
                               + "Note that this setting only has an effect on how some expressions are displayed, "
                               + "such as \"sin -1\" which is displayed as \"sin(-1)\".")
                 .addCategory(Category.EVAL)
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Integer> INTEGER_CACHE_SIZE =
        builder().setName("integerCacheSize")
                 .setType(Integer.class)
                 .requireNonNull()
                 // TODO: check Runtime free memory?
                 .setValidator(RangeValidator.inClosed(0, pow2(24)))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(2048)
                 .setDescription("The size of the math environment integer cache. This setting may have a minor "
                               + "impact on performance, but larger values will consume more memory. Very large "
                               + "values may cause the Java Virtual Machine to run out of memory, in which case "
                               + "the application will show a notification and recommend it be restarted.")
                 .addCategory(Category.MATH)
                 .declare();
    
    public static final Setting<Integer> PRIME_CACHE_SIZE =
        builder().setName("primeCacheSize")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.inClosed(0, pow2(24)))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(1024)
                 .setDescription("The size of the math environment prime number cache. This setting may have an "
                               + "impact on performance for operations such as factorization, but larger values "
                               + "will consume more memory. Very large values may cause the Java Virtual Machine "
                               + "to run out of memory, in which case the application will show a notification "
                               + "and recommend it be restarted.")
                 .addCategory(Category.MATH)
                 .declare();
    
    public static final Setting<Class<?>> MATH_ENV_CLASS =
        builder().setName("mathEnvironmentClass")
                 .setType(Misc.WILD_CLASS)
                 .requireNonNull()
                 .setValidator((self, obj) -> (obj instanceof Class<?>) && MathEnv.isEnvironment((Class<?>) obj))
                 .identityConversion()
                 .setParser(ObjectParser.ofSubtypesOf(MathEnv.class).requiring(MathEnv::isEnvironment))
                 .setDefaultValue(MathEnv64.class)
                 .setDescription("The name of the math environment, which affects speed and precision of calculations.")
                 .addCategory(Category.MATH)
                 .setToStringFunction(Class::getCanonicalName)
                 .declare();
    
    public static final Setting<Boolean> EMPTY_MULTIPLY =
        builder().setName("emptyMultiply")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(true)
                 .setDescription("Multiplies adjacent operands in certain cases, for example '2pi' is equivalent to '2*pi'.")
                 .addCategory(Category.MATH)
                 .addCategory(Category.EVAL)
                 .declare();
    
    public static final Setting<Integer> MAX_RECURSION_DEPTH =
        builder().setName("maxRecursionDepth")
                 .setType(Integer.class)
                 .requireNonNull()
                 // TODO: check runtime max stack depth somehow?
                 .setValidator(RangeValidator.inClosed(1, pow2(16)))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(32)
                 .setDescription("The maximum number of recursive function calls. A recursive function is "
                               + "a function which calls itself. Larger values may risk causing a stack overflow "
                               + "which the application cannot recover from. In the case of a stack overflow, the "
                               + "application will show a notification and recommend it be restarted.")
                 .addCategory(Category.EVAL)
                 .declare();
    
    public static final Setting<CodePoint> DECIMAL_MARK =
        builder().setName("decimalMark")
                 .setType(CodePoint.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(CodePoint.class))
                 .setDefaultValue(CodePoint.valueOf('.'))
                 .setDescription("The character used to separate the integer part of a number from its fraction digits.")
                 .addCategory(Category.EVAL)
                 .declare();
    
    public static final Setting<CodePoint> DIGIT_SEPARATOR =
        builder().setName("digitSeparator")
                 .setType(CodePoint.class)
                 .allowNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(CodePoint.class).nullable())
                 .setDefaultValue(CodePoint.valueOf('_'))
                 .setDescription("A character used to separate digits for readability when entering in large numbers. "
                               + "By default this is the underscore, for example 1_000_000.")
                 .addCategory(Category.EVAL)
                 .declare();
    
    public static final Setting<CodePoint> BOUNDARY_JOINER =
        builder().setName("boundaryJoiner")
                 .setType(CodePoint.class)
                 .allowNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(CodePoint.class).nullable())
                 .setDefaultValue(CodePoint.valueOf('_'))
                 .setDescription("A character used to join two adjacent boundary characters to force the parser "
                               + "to interpret them as one token. For example, (a)*_*(b) := a^b defines a new "
                               + "exponentiation operator **. Without the boundary joiner, the parser will in "
                               + "general interpret the expression ** as two separate tokens * and *. The parser "
                               + "is greedy and will automatically join adjacent boundary characters if a symbol "
                               + "exists with the joined name, so explicit use of the boundary joiner is in general "
                               + "only necessary for declarations. For example, after the preceding operator ** is "
                               + "defined, it will suffice to simply write 2**8. The parser will interpret ** as a "
                               + "single token because a symbol now exists with that name.")
                 .addCategory(Category.EVAL)
                 .declare();
    
    public static final Setting<Integer> MAX_FRACTION_DIGITS =
        builder().setName("maxFractionDigits")
                 .setType(Integer.class)
                 .requireNonNull()
                 .setValidator(RangeValidator.inClosed(0, (int) Short.MAX_VALUE))
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Integer.class))
                 .setDefaultValue(30)
                 .setDescription("The maximum number of fraction digits to show in the answer to a computation before truncating.")
                 .addCategory(Category.MATH)
                 .declare();
    
    // TODO: This should be changed to an enum eventually.
    //       There are 3 options now (exact, plain text, html).
    public static final Setting<Boolean> ECHO_EXACT_INPUT =
        builder().setName("echoExactInput")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(false)
                 .setDescription("True if the output window should display the exact plain text entered for a calculation "
                               + "and false if it should format it to look nicer.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<Boolean> DO_NOT_NEST_HTML_SUPERSCRIPTS =
        builder().setName("doNotNestHtmlSuperscripts")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(true)
                 .setDescription("True if the formatted output should not attempt to have multiple levels of superscripts "
                               + "and subscripts, for example as may arise by an expression such as 2^3^4.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<String> EDITOR_INDENT =
        builder().setName("editorIndent")
                 .setType(String.class)
                 .requireNonNull()
                 .setValidator((self, obj) -> {
                     if (obj instanceof String) {
                         String text = (String) obj;
                         int    len  = text.length();
                         return len >= 2 && text.codePointAt(0) == '"' && text.codePointBefore(len) == '"';
                     }
                     return false;
                 })
                 .identityConversion()
                 .setParser(ObjectParser.ofString())
                 .setDefaultValue("\"    \"")
                 .setDescription("The sequence of characters used as an indent by the editor.")
                 .addCategory(Category.UI)
                 .addCategory(Category.EDIT)
                 .declare();
    
    public static final Setting<Boolean> EDITOR_WRAP =
        builder().setName("editorWrap")
                 .setType(Boolean.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofBoolean())
                 .setDefaultValue(false)
                 .setDescription("Whether or not the editor in the main window has line wrap when it's in multiline mode.")
                 .addCategory(Category.UI)
                 .addCategory(Category.EDIT)
                 .declare();
    
    public static final Setting<String> THEME_NAME =
        builder().setName("themeName")
                 .setType(String.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofString())
                 .setDefaultValue(Theme.DEFAULT.getName())
                 .setDescription("The name of the theme.")
                 .addCategory(Category.UI)
                 .declare();
    
    public static final Setting<ArgbColor> BACKGROUND_COLOR =
        builder().setName("backgroundColor")
                 .setType(ArgbColor.class)
                 .requireNonNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(ArgbColor.class))
                 .setDefaultValue(Theme.DEFAULT.getBackgroundColor())
                 .setDescription("The background color of the cell history pane and entry text area.")
                 .addCategory(Category.UI)
                 .setToStringFunction(ArgbColor::toJsonString)
                 .declare();
    
    public static final Setting<ArgbColor> CARET_COLOR =
        builder().setName("caretColor")
                 .setType(ArgbColor.class)
                 .allowNull()
                 .defaultValidator()
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(ArgbColor.class, "parse"))
                 .setDefaultValue(Theme.DEFAULT.getCaretColor())
                 .setDescription("The color of the caret in the entry text area.")
                 .addCategory(Category.UI)
                 .setToStringFunction(color -> (color == null) ? Json.NULL : color.toJsonString())
                 .declare();
    
    public static final Setting<Style> BASIC_STYLE =
        builder().setName("basicStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getBasicStyle())
                 .setDescription("The style of regular text from which all other styles are derived.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> NOTE_STYLE =
        builder().setName("noteStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getNoteStyle())
                 .setDescription("The style of notes which appear under calculations, for example such as "
                               + "would appear to warn of division by 0 in an expression such as 1/0.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> NUMBER_STYLE =
        builder().setName("numberStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getNumberStyle())
                 .setDescription("The style of numbers and variable names.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> LITERAL_STYLE =
        builder().setName("literalStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getLiteralStyle())
                 .setDescription("The style of string literals.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> BRACKET_STYLE =
        builder().setName("bracketStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getBracketStyle())
                 .setDescription("The style of parentheses and brackets.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> FUNCTION_STYLE =
        builder().setName("functionStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getFunctionStyle())
                 .setDescription("The style of function names.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> COMMAND_STYLE =
        builder().setName("commandStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getCommandStyle())
                 .setDescription("The style of commands, such as \"help commands\" and \"set boundaryChars add x\".")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> BUTTON_STYLE =
        builder().setName("buttonStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getButtonStyle())
                 .setDescription("The text style of the buttons in the main window toolbar.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> BUTTON_SELECTED_STYLE =
        builder().setName("buttonSelectedStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getButtonSelectedStyle())
                 .setDescription("The text style of the buttons in the main window toolbar while selected.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    public static final Setting<Style> UNEXPECTED_ERROR_STYLE =
        builder().setName("unexpectedErrorStyle")
                 .setType(Style.class)
                 .requireNonNull()
                 .setValidator(StyleValidator.INSTANCE)
                 .identityConversion()
                 .setParser(ObjectParser.ofSimple(Style.class))
                 .setDefaultValue(Theme.DEFAULT.getUnexpectedErrorStyle())
                 .setDescription("The style of notifications which warn of unexpected exceptions, likely "
                               + "due to internal program errors.")
                 .addCategory(Category.UI)
                 .setToStringFunction(Style::toJsonString)
                 .declare();
    
    // ADD NEW SETTINGS HERE.
    
    static {
        for (Field field : Setting.class.getFields()) {
            if (field.getModifiers() != Misc.PUBLIC_STATIC_FINAL)
                continue;
            if (field.getType() != Setting.class)
                continue;
            try {
                Setting<?> val = (Setting<?>) field.get(null);
                if (BY_NAME.put(field.getName(), val) != null) {
                    throw new AssertionError(field + " = " + val);
                }
            } catch (ReflectiveOperationException x) {
                throw new AssertionError("field is public static", x);
            }
        }
    }
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    
    private final int      ordinal;
    private final String   name;
    private final Class<T> type;
    private final T        defaultVal;
    private final boolean  noNull;
    private final boolean  isTransient;
    
    private final Set<Category> categories;
    
    private final String description;
    
    private final BiPredicate<Setting<T>, Object>   validator;
    private final Function<? super T, ? extends T>  toCommon;
    private final Function<? super T, ? extends T>  toImmutable;
    private final Function<? super T, ?>            toStorage;
    private final Function<?, ? extends T>          fromStorage;
    private final ObjectParser<? extends T>         parser;
    private final ObjectParser<?>                   elemParser;
    private final Function<? super T, String>       toStringFn;
    
    private final Set<Setting<?>> dependants;
    private final Map<Setting<?>, DependantValueFunction<? super T, ?>> dependantFns;
    
    public static final String NAME_PATTERN = "[a-zA-Z0-9_]+";
    
    private Setting(int ordinal, BuilderImpl<T> builder) {
        Objects.requireNonNull(builder, "builder");
        
        this.ordinal     = ordinal;
        this.name        = Objects.requireNonNull(builder.name, "name");
        this.type        = Objects.requireNonNull(builder.type, "type");
        this.noNull      = builder.noNull;
        this.isTransient = builder.isTransient;
        
        if (type.isPrimitive()) {
            throw new IllegalArgumentException(type.toString());
        }
        
        if (!name.matches(NAME_PATTERN)) {
            throw new IllegalArgumentException("illegal name \"" + name + "\"");
        }
        
        Set<Category> categories = EnumSet.noneOf(Category.class);
        if (builder.categories != null) {
            categories.addAll(builder.categories);
        }
        
        this.categories  = Collections.unmodifiableSet(categories);
        this.description = builder.description;
        
        this.validator   = Objects.requireNonNull(builder.validator,   "validator");
        this.toCommon    = Objects.requireNonNull(builder.toCommon,    "toCommon");
        this.toImmutable = Objects.requireNonNull(builder.toImmutable, "toImmutable");
        this.toStorage   = Objects.requireNonNull(builder.toStorage,   "toStorage");
        this.fromStorage = Objects.requireNonNull(builder.fromStorage, "fromStorage");
        this.parser      = Objects.requireNonNull(builder.parser,      "parser");
        this.elemParser  = builder.elemParser; // may be null
        this.toStringFn  = Objects.requireNonNull(builder.toStringFn,  "toStringFn");
        
        Map<Setting<?>, DependantValueFunction<T, ?>> dependantsIn = builder.dependants;
        
        if (dependantsIn == null || dependantsIn.isEmpty()) {
            this.dependants   = Collections.emptySet();
            this.dependantFns = Collections.emptyMap();
        } else {
            Set<Setting<?>> dependants = new ArraySet<>(dependantsIn.size());
            Map<Setting<?>, DependantValueFunction<T, ?>> dependantFns = new HashMap<>(2*dependantsIn.size());
            
            for (Entry<Setting<?>, DependantValueFunction<T, ?>> e : dependantsIn.entrySet()) {
                Setting<?> key = Objects.requireNonNull(e.getKey(), "dependant");
                DependantValueFunction<T, ?> fn = Objects.requireNonNull(e.getValue(), "dependant function");
                
                dependants.add(key);
                dependantFns.put(key, fn);
            }
            
            this.dependants   = Collections.unmodifiableSet(dependants);
            this.dependantFns = Collections.unmodifiableMap(dependantFns);
        }
        
        this.defaultVal = toImmutable.apply( this.requireValidValue(builder.defaultVal) );
    }
    
    private static <T> Setting<T> declare(BuilderImpl<T> builder) {
        synchronized (MONITOR) {
            int nextOrdinal = Setting.nextOrdinal;
            assert nextOrdinal != Integer.MIN_VALUE : "overflow of nextOrdinal shouldn't happen unless there's a problem";
            
            String name = builder.name;
            
            if (BY_NAME.containsKey(name)) {
                throw new IllegalArgumentException(name + " already defined");
            }
            
            Setting<T> s = new Setting<>(nextOrdinal, builder);
            
            Setting.nextOrdinal = nextOrdinal + 1;
            VALUES.add(s);
            BY_NAME.put(name, s);
            
            return s;
        }
    }

    public static List<Setting<?>> values() {
        return VALUES_VIEW;
    }

    public static Setting<?> valueOf(String name) {
        Objects.requireNonNull(name, "name");
        Setting<?> s = BY_NAME.get(name);
        if (s == null) {
            throw new IllegalArgumentException(name);
        }
        return s;
    }
    
    public static Setting<?> valueOfIgnoreCase(String name) {
        Objects.requireNonNull(name, "name");
        synchronized (MONITOR) {
            Setting<?> s = BY_NAME.get(name);
            if (s != null) {
                return s;
            }
            for (Entry<String, Setting<?>> e : BY_NAME.entrySet()) {
                if (e.getKey().equalsIgnoreCase(name)) {
                    return e.getValue();
                }
            }
            throw new IllegalArgumentException(name);
        }
    }
    
    public static Setting<?> valueOfOrDeclare(String name) {
        Objects.requireNonNull(name, "name");
        synchronized (MONITOR) {
            Setting<?> s = BY_NAME.get(name);
            if (s == null) {
                return builder().setName(name)
                                .setType(Object.class)
                                .allowNull()
                                .defaultValidator()
                                .identityConversion()
                                .setParser(ObjectParser.alwaysNull())
                                .setDefaultValue(null)
                                .setDescription("Unknown setting, likely from a file.")
                                .addCategory(Category.ERROR)
//                                .setTransient(true) // yes/no? this will delete it from future settings files
                                .declare();
            }
            return s;
        }
    }

    @Override
    public int compareTo(Setting<?> that) {
        Objects.requireNonNull(that, "that");
        return Integer.compare(this.ordinal, that.ordinal);
    }

    public int getOrdinal() {
        return ordinal;
    }
    
    public String getName() {
        return name;
    }
    
    public boolean isTransient() {
        return isTransient;
    }
    
    public String getDescription() {
        return description;
    }
    
    public Set<Category> getCategories() {
        return categories;
    }
    
    public boolean isIn(Category cat) {
        return cat != null && categories.contains(cat);
    }
    
    public boolean isInAnyOf(Category... cats) {
        if (cats != null) {
            for (Category cat : cats) {
                if (this.isIn(cat)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Class<T> getType() {
        return type;
    }

    public T getDefaultValue() {
        return defaultVal;
    }
    
    public T toCommonValue(T immutable) {
        return toCommon.apply(immutable);
    }
    
    public T toImmutableValue(T common) {
        return toImmutable.apply(common);
    }
    
    public Object toStorageValue(T val) {
        return toStorage.apply(val);
    }
    
    @SuppressWarnings("unchecked")
    public T fromStorageValue(Object obj) {
        try {
            return requireValidValue(((Function<Object, ? extends T>) fromStorage).apply(obj));
        } catch (ClassCastException | NullPointerException | IllegalArgumentException x) {
            throw Misc.setMessage(x, "while converting " + obj);
        }
    }
    
    public T parse(String val) {
        try {
            return parser.parse(val);
        } catch (ObjectParser.BadStringException x) {
            throw x.setMessage("specified value \"%s\" is invalid for setting %s", this);
        }
    }
    
    public Object parseElement(String elem) {
        ObjectParser<?> elemParser = this.elemParser;
        if (elemParser == null) {
            throw new UnsupportedOperationException("Object parseElement(String)");
        } else {
            try {
                return elemParser.parse(elem);
            } catch (ObjectParser.BadStringException x) {
                throw x.setMessage("specified value \"%s\" is invalid for an element of setting %s", this);
            }
        }
    }
    
    public Set<Setting<?>> getDependants() {
        return dependants;
    }
    
    public <U> U toDependantValue(Setting<U> dependant, T val) {
        @SuppressWarnings("unchecked")
        DependantValueFunction<T, U> fn = (DependantValueFunction<T, U>) dependantFns.get(dependant);
        if (fn != null) {
            return dependant.toImmutableValue( fn.apply(this, dependant, val) );
        }
        throw new IllegalArgumentException(dependant.getName());
    }
    
    public Setting<?> getIndependant() {
        if (!this.isTransient())
            return this;
        for (Setting<?> setting : values())
            if (setting.getDependants().contains(this))
                return setting;
        throw new AssertionError(this); // somebody screwed up a declaration, or method called during clinit
    }
    
    public BiPredicate<Setting<T>, Object> getValidator() {
        return validator;
    }
    
    @SuppressWarnings("unchecked")
    public T requireValidValue(Object obj) {
        if (obj == null) {
            if (this.noNull) {
                String msg = f("setting %s requires non-null values", this);
                throw new NullPointerException(msg);
            } else {
                return null;
            }
        }
        if (!this.type.isInstance(obj)) {
            String msg = f("setting %s requires %s; found %s", this, this.type, obj.getClass());
            throw new ClassCastException(msg);
        }
        if (this.isValue(obj)) {
            return (T) obj;
        }
        String msg = f("invalid value for setting %s: %s", this, obj);
        throw new IllegalArgumentException(msg);
    }
    
    public boolean isValue(Object obj) {
        return (obj == null) ? !this.noNull : this.validator.test(this, obj);
//        return (!this.noNull || obj != null) && this.validator.test(this, obj);
    }
    
    public boolean typeEquals(Class<?> type) {
        return this.type == Objects.requireNonNull(type, "type");
    }

    public T cast(Object obj) {
        try {
            return type.cast(obj);
        } catch (ClassCastException cause) {
            // Capture a little more information than just the types involved.
            throw initCause(new ClassCastException(this + " casting " + obj), cause);
        }
    }
    
    @SuppressWarnings("unchecked")
    public <U> Setting<U> as(Class<U> type) {
        if (type == this.type) {
            if (type.getTypeParameters().length == 0) {
                return (Setting<U>) this;
            }
        }
        throw new ClassCastException(type + " to " + this.type);
    }
    
    public String toString(T val) {
        return toStringFn.apply(val);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return ordinal;
    }

    @Override
    public boolean equals(Object that) {
        return this == that;
    }
    
    private static Builder.NameStep builder() {
        return new BuilderImpl<>();
    }
    
    private interface Builder {
        interface NameStep {
            TypeStep setName(String name);
        }
        interface TypeStep {
            <T> NoNullStep<T> setType(Class<T> type);
        }
        interface NoNullStep<T> {
            default ValidatorStep<T> requireNonNull() { return this.setNoNull(true);  }
            default ValidatorStep<T> allowNull()      { return this.setNoNull(false); }
            
            ValidatorStep<T> setNoNull(boolean noNull);
        }
        interface ValidatorStep<T> {
            CommonFunctionStep<T> setValidator(BiPredicate<Setting<T>, Object> validator);
            
            default CommonFunctionStep<T> defaultValidator() {
                return this.setValidator(Setting.defaultValidator());
            }
        }
        interface CommonFunctionStep<T> {
            ImmutableFunctionStep<T> setCommonFunction(Function<? super T, ? extends T> toCommon);
            
            default ImmutableFunctionStep<T> identityCommon() {
                return this.setCommonFunction(Function.identity());
            }
            
            default ParserStep<T> identityConversion() {
                return this.identityCommon().identityImmutable().identityStorage();
            }
        }
        interface ImmutableFunctionStep<T> {
            ToStorageFunctionStep<T> setImmutableFunction(Function<? super T, ? extends T> toImmutable);
            
            default ToStorageFunctionStep<T> identityImmutable() {
                return this.setImmutableFunction(Function.identity());
            }
        }
        interface ToStorageFunctionStep<T> {
            <C> FromStorageFunctionStep<T, C> setToStorageFunction(Function<? super T, ? extends C> toCompact);
            
            ParserStep<T> identityStorage();
        }
        interface FromStorageFunctionStep<T, C> {
            ParserStep<T> setFromStorageFunction(Function<? super C, ? extends T> fromCompact);
        }
        interface ParserStep<T> {
            DefaultValueStep<T> setParser(ObjectParser<? extends T> parser);
        }
        interface DefaultValueStep<T> {
            DescriptionStep<T> setDefaultValue(T defaultVal);
        }
        interface DescriptionStep<T> {
            DeclarationStep<T> setDescription(String description);
        }
        interface DeclarationStep<T> {
            DeclarationStep<T> addCategory(Category cat);
            
            default DeclarationStep<T> addCategories(Category... cats) {
                for (Category cat : cats)
                    this.addCategory(cat);
                return this;
            }
            
            DeclarationStep<T> setTransient(boolean isTransient);
            
            <U> DeclarationStep<T> addDependant(Setting<U> dependant, DependantValueFunction<T, U> fn);
            
            DeclarationStep<T> setToStringFunction(Function<? super T, String> fn);
            
            DeclarationStep<T> setElementParser(ObjectParser<?> elemParser);
            
            Setting<T> declare();
        }
    }
    
    private static final class BuilderImpl<T>
    implements  Builder,
                Builder.NameStep,
                Builder.TypeStep,
                Builder.DescriptionStep<T>,
                Builder.NoNullStep<T>,
                Builder.ValidatorStep<T>,
                Builder.CommonFunctionStep<T>,
                Builder.ImmutableFunctionStep<T>,
                Builder.ToStorageFunctionStep<T>,
                Builder.FromStorageFunctionStep<T, Object>,
                Builder.ParserStep<T>,
                Builder.DefaultValueStep<T>,
                Builder.DeclarationStep<T> {
        private String   name;
        private Class<T> type;
        private T        defaultVal;
        private boolean  noNull;
        private boolean  isTransient = false;
        
        private Set<Category> categories;
        private String        description;
        
        private BiPredicate<Setting<T>, Object>     validator;
        private Function<? super T, ? extends T>    toCommon;
        private Function<? super T, ? extends T>    toImmutable;
        private Function<? super T, ?>              toStorage;
        private Function<?, ? extends T>            fromStorage;
        private ObjectParser<? extends T>           parser;
        private ObjectParser<?>                     elemParser;
        
        private Map<Setting<?>, DependantValueFunction<T, ?>> dependants;
        
        private Function<? super T, String> toStringFn = String::valueOf;
        
        @Override
        public TypeStep setName(String name) {
            this.name = Objects.requireNonNull(name, "name");
            return this;
        }
        
        @Override
        @SuppressWarnings("unchecked")
        public <U> NoNullStep<U> setType(Class<U> type) {
            if (this.type != null) {
                throw new IllegalStateException("type already set to " + this.type);
            }
            this.type = (Class<T>) Objects.requireNonNull(type, "type");
            return (NoNullStep<U>) this;
        }
        
        @Override
        public ValidatorStep<T> setNoNull(boolean noNull) {
            this.noNull = noNull;
            return this;
        }
        
        @Override
        public CommonFunctionStep<T> setValidator(BiPredicate<Setting<T>, Object> validator) {
            this.validator = Objects.requireNonNull(validator, "validator");
            return this;
        }
        
        @Override
        public ImmutableFunctionStep<T> setCommonFunction(Function<? super T, ? extends T> toCommon) {
            this.toCommon = Objects.requireNonNull(toCommon, "toCommon");
            return this;
        }
        
        @Override
        public ToStorageFunctionStep<T> setImmutableFunction(Function<? super T, ? extends T> toImmutable) {
            this.toImmutable = Objects.requireNonNull(toImmutable, "toImmutable");
            return this;
        }
        
        @Override
        @SuppressWarnings("unchecked")
        public <C> FromStorageFunctionStep<T, C> setToStorageFunction(Function<? super T, ? extends C> toStorage) {
            this.toStorage = Objects.requireNonNull(toStorage, "toStorage");
            return (FromStorageFunctionStep<T, C>) this;
        }
        
        @Override
        public ParserStep<T> setFromStorageFunction(Function<? super Object, ? extends T> fromStorage) {
            this.fromStorage = Objects.requireNonNull(fromStorage, "fromStorage");
            return this;
        }
        
        @Override
        public ParserStep<T> identityStorage() {
            this.toStorage   = this.toCommon;
            this.fromStorage = this.toImmutable;
            return this;
        }
        
        @Override
        public DefaultValueStep<T> setParser(ObjectParser<? extends T> parser) {
            this.parser = Objects.requireNonNull(parser, "parser");
            return this;
        }
        
        @Override
        public DeclarationStep<T> setElementParser(ObjectParser<?> elemParser) {
            this.elemParser = Objects.requireNonNull(elemParser, "elemParser");
            return this;
        }
        
        @Override
        public DescriptionStep<T> setDefaultValue(T defaultVal) {
            this.defaultVal = defaultVal;
            return this;
        }
        
        @Override
        public DeclarationStep<T> setDescription(String description) {
            this.description = Objects.requireNonNull(description, "description");
            return this;
        }
        
        @Override
        public DeclarationStep<T> addCategory(Category cat) {
            Set<Category> categories = this.categories;
            if (categories == null) {
                this.categories = categories = EnumSet.noneOf(Category.class);
            }
            categories.add(cat);
            return this;
        }
        
        @Override
        public DeclarationStep<T> setTransient(boolean isTransient) {
            this.isTransient = isTransient;
            return this;
        }
        
        @Override
        public DeclarationStep<T> setToStringFunction(Function<? super T, String> toStringFn) {
            this.toStringFn = Objects.requireNonNull(toStringFn, "toStringFn");
            return this;
        }
        
        @Override
        public <U> DeclarationStep<T> addDependant(Setting<U> dependant, DependantValueFunction<T, U> fn) {
            Objects.requireNonNull(dependant, "dependant");
            Objects.requireNonNull(fn,        "fn");
            Map<Setting<?>, DependantValueFunction<T, ?>> dependants = this.dependants;
            if (dependants == null) {
                this.dependants = dependants = new LinkedHashMap<>(4);
            }
            dependants.put(dependant, fn);
            return this;
        }
        
        @Override
        public Setting<T> declare() {
            return Setting.declare(this);
        }
    }
    
    @FunctionalInterface
    public interface DependantValueFunction<T, U> {
        U apply(Setting<T> self, Setting<U> dependant, T source);
    }
    
    private enum StyleValidator implements BiPredicate<Setting<Style>, Object> {
        INSTANCE;
        @Override
        public boolean test(Setting<Style> self, Object obj) {
            if (obj instanceof Style) {
                Style style = (Style) obj;
                if (style.isFamilySet()) {
                    if (!SystemInfo.isFont(style.getFamily())) {
                        return false;
                    }
                }
                if (style.isSizeSet()) {
                    Size size = style.getSize();
                    if (self == Setting.BASIC_STYLE && size.isRelative()) {
                        return false;
                    }
                    if (!inRangeClosed(size.getAbsoluteValue(), 0f, 512f)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
    
    public static <N extends Number & Comparable<? super N>> N getMinimum(Setting<N> set) {
        try {
            return ((RangeValidator<N>) set.getValidator()).getMinimum();
        } catch (ClassCastException x) {
            Log.caught(Setting.class, "getMinimum", x, false);
            return null;
        }
    }
    
    public static <N extends Number & Comparable<? super N>> N getMaximum(Setting<N> set) {
        try {
            return ((RangeValidator<N>) set.getValidator()).getMaximum();
        } catch (ClassCastException x) {
            Log.caught(Setting.class, "getMinimum", x, false);
            return null;
        }
    }
    
    public static <N extends Number & Comparable<? super N>> boolean isClosed(Setting<N> set) {
        try {
            return ((RangeValidator<N>) set.getValidator()).isClosed();
        } catch (ClassCastException x) {
            Log.caught(Setting.class, "isClosed", x, false);
            return false;
        }
    }
    
    public static final class RangeValidator<N extends Number & Comparable<? super N>>
    implements BiPredicate<Setting<N>, Object> {
        private final Class<N> type;
        private final N        min;
        private final N        max;
        private final boolean  isClosed;
        
        private RangeValidator(Class<N> type, N min, N max, boolean isClosed) {
            this.type     = Objects.requireNonNull(type, "type");
            this.min      = Objects.requireNonNull(min,  "min");
            this.max      = Objects.requireNonNull(max,  "max");
            this.isClosed = isClosed;
        }
        
        private static <N extends Number & Comparable<? super N>> RangeValidator<N> in(N min, N max) {
            return in(min, max, false);
        }
        
        private static <N extends Number & Comparable<? super N>> RangeValidator<N> inClosed(N min, N max) {
            return in(min, max, true);
        }
        
        @SuppressWarnings("unchecked")
        private static <N extends Number & Comparable<? super N>> RangeValidator<N> in(N min, N max, boolean isClosed) {
            return new RangeValidator<>((Class<N>) min.getClass(), min, max, isClosed);
        }
        
        public Class<N> getType() {
            return type;
        }
        
        public N getMinimum() {
            return min;
        }
        
        public N getMaximum() {
            return max;
        }
        
        public boolean isClosed() {
            return isClosed;
        }
        
        @Override
        public boolean test(Setting<N> self, Object obj) {
            if (type.isInstance(obj)) {
                @SuppressWarnings("unchecked")
                N val = (N) obj;
                if (isClosed) {
                    return min.compareTo(val) <= 0 && val.compareTo(max) <= 0;
                } else {
                    return min.compareTo(val) <= 0 && val.compareTo(max) < 0;
                }
            }
            return false;
        }
    }
}