/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import java.util.*;

@FunctionalInterface
public interface MiniSettings {
    <T> T getSetting(Setting<T> key);
    
    class OfValue<T> implements MiniSettings {
        private final Setting<T> key;
        private final T val;
        private final MiniSettings orElse;
        
        public OfValue(Setting<T> key, T val, MiniSettings orElse) {
            this.key    = Objects.requireNonNull(key, "key");
            this.val    = val;
            this.orElse = Objects.requireNonNull(orElse, "orElse");
        }
        
        public OfValue<T> orElse(MiniSettings orElse) {
            return new OfValue<>(key, val, orElse);
        }
        
        @Override
        public <T> T getSetting(Setting<T> key) {
            if (this.key == key) {
                return key.cast(val);
            } else {
                return orElse.getSetting(key);
            }
        }
    }
    
    static <T> MiniSettings.OfValue<T> of(Setting<T> key, T val) {
        return new OfValue<>(key, val, DEFAULT);
    }
    
    enum Default implements MiniSettings {
        INSTANCE;
        @Override
        public <T> T getSetting(Setting<T> key) {
            return key.getDefaultValue();
        }
    }
    
    MiniSettings DEFAULT = Default.INSTANCE;
    
    static MiniSettings instance() {
        MiniSettings local = MiniSettingsPrivate.getLocal();
        if (local != null) {
            return local;
        }
        App app = App.getInstance();
        if (app != null) {
            return app.getSettings();
        } else {
            return DEFAULT;
        }
    }
    
    static void setLocal(MiniSettings local) {
        MiniSettingsPrivate.setLocal(local);
    }
    
    static <T> void setLocal(Setting<T> key, T val) {
        setLocal(of(key, val));
    }
    
    static MiniSettings getLocal() {
        return MiniSettingsPrivate.getLocal();
    }
}

final class MiniSettingsPrivate {
    private MiniSettingsPrivate() {
    }
    
    private static final ThreadLocal<MiniSettings> LOCAL = new ThreadLocal<>();
    
    static MiniSettings getLocal() {
        return LOCAL.get();
    }
    
    static void setLocal(MiniSettings local) {
        if (local == null) {
            LOCAL.remove();
        } else {
            LOCAL.set(local);
        }
    }
}