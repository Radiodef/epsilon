/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import eps.ui.*;
import eps.util.*;
import static eps.app.App.*;

import java.util.*;
import java.util.logging.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.function.*;
import java.text.*;
import java.io.PrintStream;

public final class Log {
    private Log() {}
    
    public static final class Line {
        private final String name;
        private final String line;
        private final int    flags;
        
        public Line(String name, String line) {
            this(name, line, 0);
        }
        
        private Line(String name, String line, int flags) {
            this.name  = Objects.requireNonNull(name, "name");
            this.line  = Objects.requireNonNull(line, "line");
            this.flags = flags;
        }
        
        public String getName() { return name; }
        public String getLine() { return line; }
        
        public boolean isStackTraceElement() { return (flags & STACK_TRACE_ELEMENT) != 0; }
        
        private static final ToString<Line> TO_STRING =
            ToString.builder(Line.class)
                    .add("name",  Line::getName)
                    .add("flags", ln -> Integer.toBinaryString(ln.flags))
                    .add("line",  Line::getLine)
                    .build();
        
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
        
        static final int STACK_TRACE_ELEMENT = 1;
    }
    
    
    private static final Logger ROOT_LOGGER = Logger.getLogger("eps");
    
    private static final boolean ENABLED = !EpsilonMain.isNoLogging();
    
    private static final PrintStream STACK_TRACE_OUT = System.out; // ENABLED ? System.out : System.err;
    
    
    private static final    int             LOG_LINE_COUNT = 1024;
    private static final    ListDeque<Line> LINES          = new ListDeque<>(LOG_LINE_COUNT);
//    private static volatile long            LINES_OFFSET   = 0;
    
    
//    private static final String FILTER_STRING = EpsilonMain.getLogFilter();
//    
//    private static final Filter FILTER = new Filter() {
//        @Override
//        public boolean isLoggable(LogRecord rec) {
//            String filter = FILTER_STRING;
//            if (filter != null) {
//                String name = rec.getLoggerName();
//                return "eps".equals(name) || name.contains(filter);
//            }
//            return true;
//        }
//    };
    
    @FunctionalInterface
    public interface ConsumerConsumer extends Consumer<Consumer<? super ListDeque<Line>>> {
    }
    
    public static final class LineReceiver implements AutoCloseable, ConsumerConsumer {
        private final ListDeque<Line> lines;
        private volatile boolean isOpen = true;
        
        private LineReceiver(int capacity) {
            synchronized (this) {
                lines = new ListDeque<>(capacity);
            }
        }
        
        private void send(Line line) {
            synchronized (this) {
                if (isOpen) {
                    lines.addLast(line);
                }
            }
        }
        
        private void sendAll(@SuppressWarnings("SameParameterValue")
                             Collection<? extends Line> coll) {
            synchronized (this) {
                if (isOpen) {
                    lines.addAll(coll);
                }
            }
        }
        
        private void sendAll(List<? extends Line> list, int start, int end) {
            synchronized (this) {
                if (isOpen) {
                    for (; start < end; ++start) {
                        lines.add(list.get(start));
                    }
                }
            }
        }
        
        @Override
        public void accept(Consumer<? super ListDeque<Line>> cons) {
            receive(cons);
        }
        
        public boolean receive(Consumer<? super ListDeque<Line>> cons) {
            ListDeque<Line> lines = this.lines;
            synchronized (this) {
                if (lines.isEmpty()) {
                    return false;
                }
                try {
                    cons.accept(lines);
                } catch (RuntimeException x) {
                    Log.caught(LineReceiver.class, "receive", x, false);
                }
                lines.clear();
            }
            return true;
        }
        
        @Override
        public void close() {
            LINE_RECEIVERS.updateAndGet(arr -> Misc.remove(arr, this));
            synchronized (this) {
                isOpen = false;
                lines.clear();
            }
        }
    }
    
    private static final AtomicReference<LineReceiver[]> LINE_RECEIVERS =
        new AtomicReference<>(new LineReceiver[0]);
    
    public static LineReceiver newLineReceiver(int capacity) {
        LineReceiver lr = new LineReceiver(capacity);
        synchronized (LINES) {
            lr.sendAll(LINES);
        }
        LINE_RECEIVERS.updateAndGet(arr -> Misc.append(arr, lr));
        return lr;
    }
    
    
    @SuppressWarnings("unchecked")
    private static final AtomicReference<Consumer<? super LogRecord>[]> RECORD_CONSUMERS =
        new AtomicReference<>((Consumer<? super LogRecord>[]) new Consumer<?>[0]);
    
    private static final Consumer<LogRecord> SYSTEM_OUT_CONSUMER =
            new Consumer<LogRecord>() {
        private final SimpleFormatter formatter = new SimpleFormatter();
        @Override
        public void accept(LogRecord rec) {
            System.out.print(formatter.format(rec));
        }
    };
    
    private static final Consumer<LogRecord> LINE_CONSUMER =
            new Consumer<LogRecord>() {
        private final DateFormat dateFmt = new SimpleDateFormat("HH:mm:ss.SSS");
        @Override
        public void accept(LogRecord rec) {
            LineReceiver[]  receivers = LINE_RECEIVERS.get();
            ListDeque<Line> lines     = LINES;
    
            // noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (lines) {
                String name = rec.getLoggerName();
                
                StringBuilder b = new StringBuilder();
                b.append(dateFmt.format(rec.getMillis()));
                
                b.append(' ').append(rec.getSourceClassName()).append('.').append(rec.getSourceMethodName());
                b.append(": ").append(rec.getMessage());
                
                Line ln = new Line(name, b.toString());
                lines.addLast(ln);
                for (LineReceiver lr : receivers)
                    lr.send(ln);
                
                Throwable x = rec.getThrown();
                if (x != null) {
                    PrintStream out = Misc.toStringStream();
                    x.printStackTrace(out);
                    String[] trace = out.toString().split("\r?\n");
                    
                    lines.ensureCapacity(trace.length);
                    int start = lines.size();
                    int end   = start;
                    
                    for (String line : trace) {
                        lines.addLast(new Line(name, line, Line.STACK_TRACE_ELEMENT));
                        ++end;
                    }
                    if (start < end) {
                        for (LineReceiver lr : receivers)
                            lr.sendAll(lines, start, end);
                    }
                }
                
                int size = lines.size();
                if (size > LOG_LINE_COUNT) {
                    int dif = Math.min((size - LOG_LINE_COUNT) + 256, size);
                    
                    while (dif > 0) {
                        lines.removeFirst();
                        --dif;
                    }
                }
            }
        }
    };
    
    public static void addLogConsumer(Consumer<? super LogRecord> consumer) {
        Objects.requireNonNull(consumer, "consumer");
        RECORD_CONSUMERS.updateAndGet(arr -> Misc.append(arr, consumer));
    }
    
    public static void removeLogConsumer(Consumer<? super LogRecord> consumer) {
        Objects.requireNonNull(consumer, "consumer");
        RECORD_CONSUMERS.updateAndGet(arr -> Misc.remove(arr, consumer));
    }
    
    static {
        if (EpsilonMain.isDeveloper() || !EpsilonMain.mainEntered()) {
            addLogConsumer(SYSTEM_OUT_CONSUMER);
        }
        addLogConsumer(LINE_CONSUMER);
    }
    
    static {
        ROOT_LOGGER.setLevel(Level.ALL);
        ROOT_LOGGER.setUseParentHandlers(false);
        
        ROOT_LOGGER.addHandler(new Handler() {
            @Override
            public void publish(LogRecord rec) {
                Consumer<? super LogRecord>[] consumers = RECORD_CONSUMERS.get();
                int len = consumers.length;
                for (int i = 0; i < len; ++i) {
                    consumers[i].accept(rec);
                }
            }
            @Override
            public void close() {
            }
            @Override
            public void flush() {
            }
        });
        
//        ROOT_LOGGER.setFilter(FILTER);
    }
    
    private static final Map<Class<?>, Logger> LOGGERS = new ConcurrentHashMap<>();
    
    private static Logger createLogger(Class<?> clazz) {
        Package pack   = clazz.getPackage();
        String  name   = pack.getName();
        if (!name.startsWith("eps.")) throw new IllegalArgumentException(clazz.toString());
        Logger  logger = Logger.getLogger(name);
        logger.setLevel(Level.ALL);
        logger.setUseParentHandlers(true);
//        logger.setFilter(FILTER);
        return logger;
    }
    
    private static final Function<Class<?>, Logger> CREATE_LOGGER = Log::createLogger;
    
    public static Logger getLogger(Class<?> clazz) {
        Objects.requireNonNull(clazz, "clazz");
        return LOGGERS.computeIfAbsent(clazz, CREATE_LOGGER);
    }
    
    private static void showUnexpected(Throwable x) {
        if (x == null) {
            assert false : "this shouldn't happen";
            return;
        }
        
        if (x instanceof Error) {
            caughtError((Error) x);
        }
        
        try {
            App.whenAvailable(app -> app.addItemOnInterfaceThread(item -> {
                TextAreaCell cell = app.getComponentFactory().newTextArea();
                cell.followTextStyle(Setting.UNEXPECTED_ERROR_STYLE);

                String name = Misc.getSimpleName(x.getClass());

                cell.setText("Unexpected " + name + ": see the application log for details.");
                item.addCell(cell);
            }));
        } catch (ComponentFactory.UnsupportedComponentException unsupported) {
            // allowing this to throw would result in an infinite loop
            // of uncaught exceptions attempting to add items and throwing
            // more uncaught exceptions
            note(Log.class, "showUnexpected", "caught ", unsupported);
        }
    }
    
    private static volatile boolean CAUGHT_ERROR = false;
    
    public static boolean caughtError() {
        return CAUGHT_ERROR;
    }
    
    private static void caughtError(Error x) {
        if (!(x instanceof AssertionError)) {
            CAUGHT_ERROR = true;
            
            App.runOnInterfaceThread(() -> {
                final String msg = "A fatal error has occurred. See the application log for details. "
                                 + "Epsilon may continue to run; however, it may be in an invalid "
                                 + "state. As such, Epsilon will no longer save your work until it's "
                                 + "been restarted.";
                app().getComponentFactory().displayError(Misc.getSimpleName(x.getClass()), msg);
            });
        }
    }
    
    private static final Thread.UncaughtExceptionHandler UNCAUGHT_HANDLER =
            new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread t, Throwable x) {
            if (!EpsilonMain.appLaunchedSuccessfully()) {
                x.printStackTrace(STACK_TRACE_OUT);
                System.exit(0xDEAD);
                return;
            }
            
            if (ENABLED) {
                ROOT_LOGGER.log(Level.SEVERE, x, () -> String.format("On Thread: %s (#%d)", t.getName(), t.getId()));
                showUnexpected(x);
            } else {
                x.printStackTrace(STACK_TRACE_OUT);
            }
        }
    };
    
    static {
        Thread.setDefaultUncaughtExceptionHandler(UNCAUGHT_HANDLER);
    }
    
    public static Thread.UncaughtExceptionHandler getUncaughtExceptionHandler() {
        return UNCAUGHT_HANDLER;
    }
    
    // STATIC INITIALIZATION END
    
    public static void constructing(Class<?> sender) {
        entering(sender, "<init>");
    }
    
    public static void entering(Class<?> sender, String method) {
        if (ENABLED) {
            String clazz = sender.getCanonicalName();
            if (clazz == null) {
                clazz = sender.getName();
            }
            getLogger(sender).entering(clazz, method);
        }
    }
    
    public static void caught(Class<?> sender, String message, Throwable x, boolean expected) {
        if (ENABLED) {
            getLogger(sender).logp(expected ? Level.INFO : Level.WARNING, sender.getName(), "..", message, x);
            if (!expected) {
                showUnexpected(x);
            }
        } else if (!expected) {
            x.printStackTrace(STACK_TRACE_OUT);
        }
    }
    
    public static void note(Class<?> sender, String method, java.util.function.Supplier<String> message) {
        log(Level.INFO, sender, method, message);
    }
    
    public static void low(Class<?> sender, String method, java.util.function.Supplier<String> message) {
        log(Level.FINE, sender, method, message);
    }
    
    private static void log(Level lvl, Class<?> sender, String method, java.util.function.Supplier<String> message) {
        if (ENABLED) {
            getLogger(sender).logp(lvl, sender.getName(), method, message);
        }
    }
    
    public static void note(Object message) {
        if (ENABLED) {
            StackTraceElement caller = Misc.getCaller(1);
            try {
                note(Class.forName(caller.getClassName()), caller.getMethodName(), message);
            } catch (ClassNotFoundException x) {
                caught(Log.class, "note", x, false);
            }
        }
    }
    
    static void generateOverloads() {
        generateOverloads("note", "INFO");
        generateOverloads("low",  "FINE");
    }
    
    static void generateOverloads(String name, String level) {
        Objects.requireNonNull(name,  "name");
        Objects.requireNonNull(level, "level");
        PrintStream out = System.out;
        
        int max = 12;
        for (int count = 1; count <= max; ++count) {
            out.printf("    public static void %s(Class<?> sender, String method", name);
            
            for (int i = 0; i < count; ++i) {
                out.printf(", Object msg%d", i);
            }
            
            out.printf(") {%n");
            out.printf("        if (ENABLED) {%n");
            out.printf("            String message = new StringBuilder(%d)", 8*count);
            
            for (int i = 0; i < count; ++i) {
                out.printf(".append(msg%d)", i);
            }
            
            out.printf(".toString();%n");
            out.printf("            getLogger(sender).logp(Level.%s, sender.getName(), method, message);%n", level);
            out.printf("        }%n");
            out.printf("    }%n");
        }
    }
    
    public static void note(Class<?> sender, String method, Object msg0) {
        if (ENABLED) {
            String message = new StringBuilder(8).append(msg0).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1) {
        if (ENABLED) {
            String message = new StringBuilder(16).append(msg0).append(msg1).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2) {
        if (ENABLED) {
            String message = new StringBuilder(24).append(msg0).append(msg1).append(msg2).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3) {
        if (ENABLED) {
            String message = new StringBuilder(32).append(msg0).append(msg1).append(msg2).append(msg3).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4) {
        if (ENABLED) {
            String message = new StringBuilder(40).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5) {
        if (ENABLED) {
            String message = new StringBuilder(48).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6) {
        if (ENABLED) {
            String message = new StringBuilder(56).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7) {
        if (ENABLED) {
            String message = new StringBuilder(64).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8) {
        if (ENABLED) {
            String message = new StringBuilder(72).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8, Object msg9) {
        if (ENABLED) {
            String message = new StringBuilder(80).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).append(msg9).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8, Object msg9, Object msg10) {
        if (ENABLED) {
            String message = new StringBuilder(88).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).append(msg9).append(msg10).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void note(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8, Object msg9, Object msg10, Object msg11) {
        if (ENABLED) {
            String message = new StringBuilder(96).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).append(msg9).append(msg10).append(msg11).toString();
            getLogger(sender).logp(Level.INFO, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0) {
        if (ENABLED) {
            String message = new StringBuilder(8).append(msg0).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1) {
        if (ENABLED) {
            String message = new StringBuilder(16).append(msg0).append(msg1).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2) {
        if (ENABLED) {
            String message = new StringBuilder(24).append(msg0).append(msg1).append(msg2).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3) {
        if (ENABLED) {
            String message = new StringBuilder(32).append(msg0).append(msg1).append(msg2).append(msg3).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4) {
        if (ENABLED) {
            String message = new StringBuilder(40).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5) {
        if (ENABLED) {
            String message = new StringBuilder(48).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6) {
        if (ENABLED) {
            String message = new StringBuilder(56).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7) {
        if (ENABLED) {
            String message = new StringBuilder(64).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8) {
        if (ENABLED) {
            String message = new StringBuilder(72).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8, Object msg9) {
        if (ENABLED) {
            String message = new StringBuilder(80).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).append(msg9).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8, Object msg9, Object msg10) {
        if (ENABLED) {
            String message = new StringBuilder(88).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).append(msg9).append(msg10).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
    public static void low(Class<?> sender, String method, Object msg0, Object msg1, Object msg2, Object msg3, Object msg4, Object msg5, Object msg6, Object msg7, Object msg8, Object msg9, Object msg10, Object msg11) {
        if (ENABLED) {
            String message = new StringBuilder(96).append(msg0).append(msg1).append(msg2).append(msg3).append(msg4).append(msg5).append(msg6).append(msg7).append(msg8).append(msg9).append(msg10).append(msg11).toString();
            getLogger(sender).logp(Level.FINE, sender.getName(), method, message);
        }
    }
}