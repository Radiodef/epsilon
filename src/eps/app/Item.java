/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.app;

import eps.ui.*;
import eps.json.*;
import static eps.app.App.*;

import java.util.*;

public class Item implements AutoCloseable {
    private final String commandText;
    
    private final ContainerCell container;
    private final ContainerCell cells;
    private final ContainerCell notes;
    
    private volatile boolean isClosed = false;
    
    Item(String commandText) {
        synchronized (this) {
            this.commandText = commandText;
            
            ComponentFactory fact = app().getComponentFactory();
            this.cells       = fact.newContainer();
            this.notes       = fact.newContainer();
            this.container   = fact.newContainer();
            
            container.addCell(cells);
        }
    }
    
    synchronized Info getInfo() {
        return new Info(this);
    }
    
    @JsonSerializable
    public static final class Info {
        @JsonProperty
        private final String cmd;
        @JsonProperty
        private final Packet cells;
        @JsonProperty
        private final Packet notes;
        
        private Info(Item item) {
            this.cmd   = item.commandText;
            this.cells = item.cells.isEmpty() ? null : item.cells.getPacket();
            this.notes = item.notes.isEmpty() ? null : item.notes.getPacket();
        }
        
        @JsonConstructor
        private Info(String cmd, Packet cells, Packet notes) {
            this.cmd   = cmd;
            this.cells = cells;
            this.notes = notes;
        }
        
        public String getCommandText() {
            return cmd;
        }
    }
    
    synchronized void set(Info info) {
        Log.entering(Item.class, "set");
        Objects.requireNonNull(info, "info");
        if (info.cells != null) {
            cells.configure(this, info.cells);
            cells.getCells().forEach(c -> c.addDeleteListener(() -> this.cellDeleted(c, false)));
        }
        if (info.notes != null) {
            notes.configure(this, info.notes);
            notes.getCells().forEach(c -> c.addDeleteListener(() -> this.cellDeleted(c, true)));
            if (!notes.isEmpty()) {
                container.addCell(notes);
            }
        }
    }
    
    public String getCommandText() {
        return commandText;
    }
    
    Cell getContainer() {
        return container;
    }
    
    private synchronized void cellDeleted(Cell cell, boolean isNote) {
        (isNote ? notes : cells).removeCell(cell);
        cell.close();
        
        if (isNote && notes.isEmpty()) {
            container.removeCell(notes);
        }
        if (cells.isEmpty()) {
            app().removeItemOnInterfaceThread(this);
        }
    }
    
    private synchronized void addCell(Cell cell, boolean isNote) {
        Log.entering(Item.class, "addCell");
        Objects.requireNonNull(cell, "cell");
        if (isClosed) {
            Log.note(Item.class, "addCell", "closed when attempting to add ", cell, ": ", cell.getLines());
            return;
        }
        
        cell.addDeleteListener(() -> this.cellDeleted(cell, isNote));
        
        if (isNote && notes.isEmpty()) {
            container.addCell(notes);
        }
        
        (isNote ? notes : cells).addCell(cell);
    }
    
    public synchronized void addCell(Cell cell) {
        addCell(cell, false);
    }
    
    public void formatNote(String format, Object... args) {
        addNote(String.format(format, args));
    }
    
    public synchronized void addNote(String text) {
        App app = app();
        
        TextAreaCell cell = app.getComponentFactory().newTextArea();
        
        cell.setText("↳ " + text);
        cell.setMargins(new BoundingBox(0, 0, 0, app.getSettings().get(Setting.MAJOR_MARGIN)));
        cell.followTextStyle(Setting.NOTE_STYLE);
        
        addCell(cell, true);
    }
    
    public synchronized boolean contains(Cell cell) {
        Objects.requireNonNull(cell, "cell");
        return cells.containsCell(cell) || notes.containsCell(cell);
    }
    
    @Override
    public synchronized void close() {
        if (!isClosed) {
            isClosed = true;
            container.close();
        }
    }
    
    @Override
    public String toString() {
        return "Item[" + commandText + "]";
    }
}

//public class Item implements AutoCloseable {
//    private final App owner;
//    
//    private final String commandText;
//    
//    private volatile Cell empty;
//    
//    private final List<Cell> cells;
//    private final List<Cell> notes;
//    
//    private volatile boolean isClosed = false;
//    
//    Item(App owner, String commandText) {
//        synchronized (this) {
//            this.owner       = Objects.requireNonNull(owner, "owner");
//            this.commandText = commandText;
//            this.cells       = new ArrayList<>(2);
//            this.notes       = new ArrayList<>(2);
////            this.empty       = owner.getComponentFactory().newEmpty();
////            this.addCell(empty);
//        }
//    }
//    
//    public Info getInfo() {
//        return new Info(this);
//    }
//    
//    @JsonSerializable
//    public static final class Info {
//        @JsonProperty
//        private final String cmd;
//        @JsonProperty
//        private final List<Packet> cells;
//        @JsonProperty
//        private final List<Packet> notes;
//        
//        private Info(Item item) {
//            this.cmd   = item.commandText;
//            this.cells = getPackets(item.cells, item.empty);
//            this.notes = getPackets(item.notes, null);
//        }
//        
//        private static List<Packet> getPackets(List<Cell> cells, Cell empty) {
//            int          count   = cells.size();
//            List<Packet> packets = new ArrayList<>(count);
//            
//            for (int i = 0; i < count; ++i) {
//                Cell cell = cells.get(i);
//                if (cell != empty) {
//                    packets.add(cell.getPacket());
//                }
//            }
//            
//            return packets;
//        }
//        
//        private Info(String cmd, List<Packet> cells, List<Packet> notes) {
//            this.cmd   = cmd;
//            this.cells = Misc.requireNonNull(cells, "cells");
//            this.notes = Misc.requireNonNull(notes, "notes");
//        }
//        
//        public String getCommandText() {
//            return cmd;
//        }
//    }
//    
//    public App getOwner() {
//        return owner;
//    }
//    
//    public String getCommandText() {
//        return commandText;
//    }
//    
//    private synchronized void addCell(Cell cell, boolean isNote) {
//        Objects.requireNonNull(cell, "cell");
//        if (isClosed)
//            return;
//        
//        MainWindow window = owner.getMainWindow();
//        
//        if (cells.isEmpty() && notes.isEmpty()) {
////            assert cell == empty : cell;
//            window.addCell(cell);
//            
//        } else {
//            int index;
//            
//            if (isNote) {
//                List<Cell> list   = notes.isEmpty() ? cells : notes;
//                Cell       before = list.get(list.size() - 1);
//                
//                index = window.indexOfCell(before) + 1;
//                
//            } else {
//                if (cells.isEmpty()) {
//                    Cell after = notes.get(0);
//                    index = window.indexOfCell(after);
//                    
//                } else {
//                    Cell before = cells.get(cells.size() - 1);
//                    index = window.indexOfCell(before) + 1;
//                }
//            }
//            
//            window.insertCell(index, cell);
//            
//            if (empty != null) {
//                window.removeCell(empty);
//                cells.remove(empty);
//                empty.close();
//                empty = null;
//            }
//        }
//        
//        cell.addDeleteListener(() -> {
//            if ((isNote ? notes : cells).remove(cell)) {
//                window.removeCell(cell);
//                cell.close();
//                
//                if (cells.isEmpty()) {
//                    owner.removeItem(this);
//                }
//            }
//        });
//        
//        (isNote ? notes : cells).add(cell);
//    }
//    
//    public synchronized void addCell(Cell cell) {
//        addCell(cell, false);
//    }
//    
//    public void formatNote(String format, Object... args) {
//        addNote(String.format(format, args));
//    }
//    
//    public synchronized void addNote(String text) {
//        TextAreaCell cell = owner.getComponentFactory().newTextArea();
//        
//        cell.setText("↳ " + text);
//        cell.setMargins(0, 0, 0, owner.getSettings().get(Setting.MAJOR_MARGIN));
//        cell.followTextStyle(Setting.NOTE_STYLE);
//        
//        addCell(cell, true);
//    }
//    
//    public synchronized List<Cell> getCells() {
//        return Collections.unmodifiableList(cells);
//    }
//    
//    public synchronized boolean contains(Cell cell) {
//        Objects.requireNonNull(cell, "cell");
//        return cells.contains(cell) || notes.contains(cell);
//    }
//    
//    @Override
//    public synchronized void close() {
//        if (!isClosed) {
//            isClosed = true;
//            closeAll(cells);
//            closeAll(notes);
//        }
//    }
//    
//    private void closeAll(List<Cell> list) {
//        MainWindow window = owner.getMainWindow();
//        
//        for (Cell cell : list) {
//            window.removeCell(cell);
//            cell.close();
//        }
//        
//        list.clear();
//    }
//    
//    @Override
//    public String toString() {
//        return "Item[" + commandText + "]";
//    }
//}