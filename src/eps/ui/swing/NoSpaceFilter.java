/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.util.*;

import javax.swing.text.*;

final class NoSpaceFilter extends DocumentFilter {
    @Override
    public void insertString(FilterBypass bypass, int offs, String str, AttributeSet atts)
            throws BadLocationException {
        bypass.insertString(offs, Misc.despace(str), atts);
    }
    
    @Override
    public void replace(FilterBypass bypass, int offs, int len, String str, AttributeSet atts)
            throws BadLocationException {
        bypass.replace(offs, len, Misc.despace(str), atts);
    }
    
    @Override
    public void remove(FilterBypass bypass, int offs, int len)
            throws BadLocationException {
        bypass.remove(offs, len);
    }
    
    static final NoSpaceFilter INSTANCE = new NoSpaceFilter();
}