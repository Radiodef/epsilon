/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;

import javax.swing.*;
import java.awt.*;

abstract class SwingSettingWeakListener<T, R> extends Settings.WeakListener<T, R> {
    SwingSettingWeakListener(R referant) {
        super(referant);
    }
    
    @Override
    protected final void settingChanged(R          referant,
                                        Settings   settings,
                                        Setting<T> setting,
                                        T          oldValue,
                                        T          newValue) {
        if (SwingUtilities.isEventDispatchThread()) {
            this.settingChangedOnEdt(referant, settings, setting, oldValue, newValue);
        } else {
            SwingUtilities.invokeLater(() -> this.settingChangedOnEdt(referant, settings, setting, oldValue, newValue));
        }
    }
    
    protected abstract void settingChangedOnEdt(R          referant,
                                                Settings   settings,
                                                Setting<T> setting,
                                                T          oldValue,
                                                T          newValue);
    
    static abstract class OfJComponent<T, R> extends SwingSettingWeakListener<T, R> {
        OfJComponent(R referant) {
            super(referant);
        }
        
        protected abstract JComponent getComponent(R referant);
        
        @Override
        public void settingChangedOnEdt(R          referant,
                                        Settings   settings,
                                        Setting<T> setting,
                                        T          oldValue,
                                        T          newValue) {
            JComponent comp = this.getComponent(referant);
            if (comp != null) {
                this.settingChangedOnEdt(referant, comp, settings, setting, oldValue, newValue);
            } else {
                Log.note(OfJComponent.class, "settingChanged", "found null component for ", referant);
            }
        }
        
        protected abstract void settingChangedOnEdt(R          referant,
                                                    JComponent comp,
                                                    Settings   settings,
                                                    Setting<T> setting,
                                                    T          oldValue,
                                                    T          newValue);
    }
    
    static abstract class OfFontName<R> extends OfJComponent<String, R> {
        OfFontName(R referant) {
            super(referant);
        }
        
        @Override
        public void settingChangedOnEdt(R               referant,
                                        JComponent      comp,
                                        Settings        settings,
                                        Setting<String> setting,
                                        String          oldName,
                                        String          newName) {
            if (newName == null) {
                newName = SwingMisc.Store.DEFAULT_FONT_NAME;
            }
            
            Font font = comp.getFont();
            comp.setFont(new Font(newName, font.getStyle(), font.getSize()));
            
            Component root = SwingUtilities.getRoot(comp);
            if (root != null) {
                root.revalidate();
                root.repaint();
            }
        }
    }
    
//    @Deprecated
//    static abstract class OfFontSize<R> extends OfJComponent<Float, R> {
//        OfFontSize(R referant) {
//            super(referant);
//        }
//        
//        @Override
//        public void settingChangedOnEdt(R              referant,
//                                        JComponent     comp,
//                                        Settings       settings,
//                                        Setting<Float> setting,
//                                        Float          oldSize,
//                                        Float          newSize) {
//            int intSize;
//            if (newSize == null) {
//                Font def = SwingMisc.getDefaultFont(comp);
//                if (def == null) {
//                    intSize = 12;
//                } else {
//                    intSize = def.getSize();
//                }
//            } else {
//                intSize = Math.round(newSize);
//            }
//            
//            Font font = comp.getFont();
//            comp.setFont(new Font(font.getName(), font.getStyle(), intSize));
//            
//            Component root = SwingUtilities.getRoot(comp);
//            if (root != null) {
//                root.revalidate();
//                root.repaint();
//            }
//        }
//    }
    
    static abstract class OfForegroundColor<R> extends OfJComponent<ArgbColor, R> {
        OfForegroundColor(R referant) {
            super(referant);
        }
        
        @Override
        public void settingChangedOnEdt(R                  referant,
                                        JComponent         comp,
                                        Settings           settings,
                                        Setting<ArgbColor> setting,
                                        ArgbColor          oldColor,
                                        ArgbColor          newColor) {
            comp.setForeground(SwingMisc.toAwtColor(newColor));
        }
    }
    
    static abstract class OfBackgroundColor<R> extends OfJComponent<ArgbColor, R> {
        OfBackgroundColor(R ref) {
            super(ref);
        }
        
        @Override
        public void settingChangedOnEdt(R                  referant,
                                        JComponent         comp,
                                        Settings           settings,
                                        Setting<ArgbColor> setting,
                                        ArgbColor          oldColor,
                                        ArgbColor          newColor) {
            comp.setBackground(SwingMisc.toAwtColor(newColor));
        }
    }
}