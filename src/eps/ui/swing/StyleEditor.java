/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.ui.Style;
import eps.app.*;
import eps.util.*;
import static eps.app.App.*;
import static eps.util.Literals.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import java.util.*;
import java.util.List;

final class StyleEditor extends JPanel {
    private ChangeListener[]  changeListeners = new ChangeListener[0];
    private final ChangeEvent changeEvent     = new ChangeEvent(this);
    private ActionListener[]  actionListeners = new ActionListener[0];
    private final ActionEvent actionEvent     = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "styleEdited");
    
    private Theme theme;
    private Theme.Key key;
    
    private final Style.Builder builder = new Style.Builder();
    
    private final JTextField familyField;
    private final JList<Family> familiesList;
    
    private final SizeField sizeField;
    private final JTextField absSizeField;
    
    private final ColorSelector foregroundColor;
    
    private final StyleBooleanOption boldOption;
    private final StyleBooleanOption italicOption;
    
    private final List<Component> componentsToEnable;
    
    StyleEditor() {
        int minor = Settings.getOrDefault(Setting.MINOR_MARGIN);
        setLayout(new BorderLayout(minor, minor));
        
        familyField = new JTextField();
        familyField.setEditable(false);
        familyField.setEnabled(false);
        
        familiesList = new JList<>();
        familiesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        familiesList.setCellRenderer(new FamilyCellRenderer());
        familiesList.setDragEnabled(false);
        
        DefaultListModel<Family> familiesModel = new DefaultListModel<>();
        familiesList.setModel(familiesModel);
        
        familiesModel.addElement(Family.INHERITED);
        for (String family : SystemInfo.getAvailableFonts()) {
            familiesModel.addElement(new Family(family));
        }
        
        JScrollPane familiesScroll =
            new JScrollPane(familiesList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                          JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        JPanel familiesPane = new JPanel(new BorderLayout());
        familiesPane.setOpaque(false);
        
        familiesPane.add(familyField, BorderLayout.NORTH);
        familiesPane.add(familiesScroll, BorderLayout.CENTER);
        
        add(familiesPane, BorderLayout.CENTER);
        
        JPanel optionsPane = new JPanel();
        optionsPane.setLayout(new BoxLayout(optionsPane, BoxLayout.Y_AXIS));
        optionsPane.setOpaque(false);
        
        sizeField = new SizeField(6);
        
        absSizeField = new JTextField(6);
        absSizeField.setEnabled(false);
        absSizeField.setEditable(false);
        
        JPanel sizePane = new JPanel(new GridLayout(1, 2)) {
            @Override
            public Dimension getMaximumSize() {
                Dimension max = super.getMaximumSize();
                max.height = getPreferredSize().height;
                return max;
            }
        };
        sizePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Size"));
        
        sizePane.add(sizeField);
        sizePane.add(absSizeField);
        
        optionsPane.add(sizePane);
        
        foregroundColor = new ColorSelector("Foreground", "Inherited", () -> {
            if (theme != null && key != null) {
                return theme.getAbsoluteStyle(key).getForeground();
            }
            return ArgbColor.BLACK;
        });
        
        optionsPane.add(foregroundColor);
        
//        ColorSelector altColor = new ColorSelector("Alternative", "Inherited", () -> ArgbColor.BLACK);
//        optionsPane.add(altColor);
//        altColor.setVisible(false);
        
        boldOption = new StyleBooleanOption("Bold");
        italicOption = new StyleBooleanOption("Italic");
        
        optionsPane.add(boldOption);
        optionsPane.add(italicOption);
        
        add(optionsPane, BorderLayout.WEST);
        
        componentsToEnable = ListOf(familiesScroll, sizeField, foregroundColor, boldOption, italicOption);
        
        addListeners();
        setToolTips();
    }
    
    private void addListeners() {
        familiesList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                familySelectionChanged();
                actionPerformed();
            }
        });
        
        sizeField.addChangeListener(e -> {
            Size prev = builder.getSize();
            sizeChanged();
            if (!Objects.equals(prev, builder.getSize())) {
                actionPerformed();
            }
        });
        
        foregroundColor.addActionListener(e -> {
            foregroundChanged();
            actionPerformed();
        });
        
        boldOption.addActionListener(e -> {
            boldChanged();
            actionPerformed();
        });
        
        italicOption.addActionListener(e -> {
            italicChanged();
            actionPerformed();
        });
    }
    
    private void setToolTips() {
        sizeField.setToolTipText("The font size. Prefix the size with + or - to specify a relative size. "
                               + "The basic style may not use a relative size.");
        absSizeField.setToolTipText("The actual font size, taking in to account any relative sizes involved.");
        
        foregroundColor.getButton().setToolTipText("Choose the font foreground color.");
        foregroundColor.getCheckBox().setToolTipText("Select 'Inherited' to use the font foreground color from the basic or default style.");
        
        for (StyleBooleanOption b : ListOf(boldOption, italicOption)) {
            String label = (b == boldOption) ? "bold" : "italic";
            b.getOnButton().setToolTipText("Enable " + label + " for the selected style.");
            b.getOffButton().setToolTipText("Disable " + label + " for the selected style.");
            b.getInheritedButton().setToolTipText("Use the " + label + " property from the basic or default style.");
        }
        
        familiesList.setToolTipText("<html>Select the font family for the selected style. If no selection is made or "
                                  + " <i>(Inherited)</i> is selected, the font family will be inherited from the "
                                  + "basic or default style.</html>");
        familyField.setToolTipText("The actual font family used.");
    }
    
    public Theme getTheme() {
        return theme;
    }
    
    public void setTheme(Theme theme) {
        if (!Objects.equals(this.theme, theme)) {
            this.theme = theme;
            updateBuilder();
            updateEditorComponents();
            stateChanged();
        }
    }
    
    public Theme.Key getKey() {
        return key;
    }
    
    public void setKey(Theme.Key key) {
        if (!Objects.equals(this.key, key)) {
            this.key = key;
            updateBuilder();
            updateEditorComponents();
            stateChanged();
        }
    }
    
    private void updateBuilder() {
        if (theme != null && key != null) {
            builder.set(theme.getStyle(key));
        }
    }
    
    private void updateTheme() {
        if (theme != null && key != null && builder != null) {
            theme = theme.derive(key, builder.build());
        }
    }
    
    public Style getStyle() {
        return (theme == null || key == null) ? null : builder.build();
    }
    
    public void addChangeListener(ChangeListener listener) {
        changeListeners = Misc.append(changeListeners, Objects.requireNonNull(listener, "listener"));
    }
    
    public void removeChangeListener(ChangeListener listener) {
        changeListeners = Misc.remove(changeListeners, Objects.requireNonNull(listener, "listener"));
    }
    
    public ChangeListener[] getChangeListeners() {
        return changeListeners.clone();
    }
    
    private void stateChanged(Object previous, Object current) {
        if (!Objects.equals(previous, current))
            stateChanged();
    }
    
    private void stateChanged() {
        for (ChangeListener listener : changeListeners)
            listener.stateChanged(changeEvent);
    }
    
    public void addActionListener(ActionListener listener) {
        actionListeners = Misc.append(actionListeners, Objects.requireNonNull(listener, "listener"));
    }
    
    public void removeActionListener(ActionListener listener) {
        actionListeners = Misc.remove(actionListeners, Objects.requireNonNull(listener, "listener"));
    }
    
    public ActionListener[] getActionListeners() {
        return actionListeners.clone();
    }
    
    private void actionPerformed() {
        for (ActionListener listener : actionListeners)
            listener.actionPerformed(actionEvent);
    }
    
    private void updateEditorComponents() {
        if (theme == null || key == null) {
            setEnabled(false);
            
            setFamiliesListSelectedValue(null);
            updateFamilyField();
            sizeField.setText("");
            updateAbsoluteSizeField();
            foregroundColor.setColor(ArgbColor.BLACK);
            boldOption.setBooleanValue(null);
            italicOption.setBooleanValue(null);
            
        } else {
            setEnabled(true);
            
            setFamiliesListSelectedValue(builder.getFamily());
            updateFamilyField();
            sizeField.setText(builder.isSizeSet() ? builder.getSize().toString() : "");
            updateAbsoluteSizeField();
            foregroundColor.setColor(builder.getForeground());
            boldOption.setBooleanValue(builder.isBold());
            italicOption.setBooleanValue(builder.isItalic());
        }
    }
    
    @Override
    public void setEnabled(boolean e) {
        super.setEnabled(e);
        for (Component comp : componentsToEnable)
            comp.setEnabled(e);
    }
    
    private void setFamiliesListSelectedValue(String fam) {
        ListSelectionListener[] listeners = familiesList.getListSelectionListeners();
        for (ListSelectionListener listener : listeners)
            familiesList.removeListSelectionListener(listener);
        try {
            if (fam != null) {
                familiesList.setSelectedValue(new Family(fam), true);
            } else {
                familiesList.setSelectedValue(Family.INHERITED, true);
//                familiesList.clearSelection();
            }
        } finally {
            for (ListSelectionListener listener : listeners)
                familiesList.addListSelectionListener(listener);
        }
    }
    
    private void familySelectionChanged() {
        String fam  = Family.getName(familiesList.getSelectedValue());
        String prev = builder.getFamily();
        builder.setFamily(fam);
        
        updateTheme();
        updateFamilyField();
        stateChanged(prev, fam);
    }
    
    private void updateFamilyField() {
        String fam = Family.getName(familiesList.getSelectedValue());
        
        if (fam != null) {
            familyField.setText(fam);
        } else if (theme != null && key != null) {
            fam = theme.getAbsoluteStyle(key).getFamily();
            familyField.setText(fam + " (Inherited)");
        } else {
            familyField.setText("");
        }
    }
    
    private void sizeChanged() {
        Size size = null;
        try {
            String text = sizeField.getText();
            if (!text.isEmpty()) {
                size = new Size(text);
            }
        } catch (IllegalArgumentException x) {
            Log.caught(StyleEditor.class, "sizeChanged", x, true);
        }
        
        Size prev = builder.getSize();
        builder.setSize(size);
        
        updateTheme();
        updateAbsoluteSizeField();
        stateChanged(prev, size);
    }
    
    private void updateAbsoluteSizeField() {
        Size size;
        if (theme != null && key != null) {
            size = theme.getAbsoluteStyle(key).getSize();
        } else {
            size = builder.getSize();
        }
        
        if (size != null) {
            absSizeField.setText(size.toString());
        } else {
            absSizeField.setText("");
        }
    }
    
    private void foregroundChanged() {
        ArgbColor prev  = builder.getForeground();
        ArgbColor color = foregroundColor.getColor();
        
        builder.setForeground(color);
        updateTheme();
        stateChanged(prev, color);
    }
    
    private void boldChanged() {
        Boolean prev = builder.isBold();
        Boolean curr = boldOption.getBooleanValue();
        
        builder.setBold(curr);
        updateTheme();
        stateChanged(prev, curr);
    }
    
    private void italicChanged() {
        Boolean prev = builder.isItalic();
        Boolean curr = italicOption.getBooleanValue();
        
        builder.setItalic(curr);
        updateTheme();
        stateChanged(prev, curr);
    }
    
    private final class SizeField extends JTextField {
        private final DocumentFilter   documentFilter;
        private final DocumentListener documentListener;
        
        private boolean sendEvents = false;
        
        private final ChangeEvent    changeEvent = new ChangeEvent(this);
        private ChangeListener[] changeListeners = new ChangeListener[0];
        
        private int decimalMark = Settings.getOrDefault(Setting.DECIMAL_MARK).intValue();
        
        SizeField() {
            this(0);
        }
        
        SizeField(int columns) {
            super(columns);
            app().getSettings().addChangeListener(Setting.DECIMAL_MARK, new DecimalMarkListener(this));
            
            documentFilter = new DocumentFilter() {
                @Override
                public void insertString(FilterBypass bypass, int offs, String str, AttributeSet atts)
                        throws BadLocationException {
                    filter(bypass, offs, str, atts);
                }
                @Override
                public void remove(FilterBypass bypass, int offs, int len)
                        throws BadLocationException {
                    bypass.remove(offs, len);
                }
                @Override
                public void replace(FilterBypass bypass, int offs, int len, String str, AttributeSet atts)
                        throws BadLocationException {
                    bypass.remove(offs, len);
                    filter(bypass, offs, str, atts);
                }
                
                private void filter(FilterBypass bypass, int offs, String str, AttributeSet atts)
                        throws BadLocationException {
                    try {
                        filter0(bypass, offs, str, atts);
                    } catch (BadLocationException x) {
                        Log.caught(StyleEditor.class, "filter", x, false);
                        throw x;
                    }
                }
                
                private void filter0(FilterBypass bypass, int offs, String str, AttributeSet atts)
                        throws BadLocationException {
                    
                    if (str.indexOf(decimalMark) >= 0) {
                        Document doc = bypass.getDocument();
                        
                        int del;
                        del   = removeAnyDots(bypass, doc.getText(0, offs), 0);
                        offs -= del;
                        del   = removeAnyDots(bypass, doc.getText(offs, doc.getLength() - offs), offs);
                    }
                    
                    bypass.insertString(offs, filter(offs, str), atts);
                    postProcess(bypass);
                }
                
                private int removeAnyDots(FilterBypass bypass, String str, int offs)
                        throws BadLocationException {
                    int len = str.length();
                    int del = 0;
                    
                    for (int src = 0, dst = offs; src < len;) {
                        int code  = str.codePointAt(src);
                        int count = Character.charCount(code);
                        
                        if (code == decimalMark) {
                            bypass.remove(dst, count);
                            del += count;
                        } else {
                            dst += count;
                        }
                        
                        src += count;
                    }
                    
                    return del;
                }
                
                private String filter(int offs, String str) {
                    StringBuilder b = null;
                    
                    int len  = str.length();
                    int prev = 0;
                    
                    for (int i = 0; i < len;) {
                        int code  = str.codePointAt(i);
                        int count = Character.charCount(code);
                        
                        boolean isValid =
                               ('0' <= code && code <= '9')
                            || (code == decimalMark)
                            || ((code == '+' || code == '-') && offs == 0 && i == 0 && key != Theme.Key.BASIC);
                        if (!isValid) {
                            if (b == null) {
                                b = new StringBuilder(len);
                            }
                            b.append(str, prev, i);
                            prev = i + count;
                        }
                        
                        i += count;
                    }
                    
                    return (b == null) ? str : b.append(str, prev, len).toString();
                }
                
                private void postProcess(FilterBypass bypass) throws BadLocationException {
                    Document doc  = bypass.getDocument();
                    String   text = doc.getText(0, doc.getLength());
                    int      len  = text.length();
                    
                    for (int src = 0, dst = 0; src < len;) {
                        int code  = text.codePointAt(src);
                        int count = Character.charCount(code);
                        
                        if (code == '-' || code == '+') {
                            if (src != 0 || key == Theme.Key.BASIC) {
                                bypass.remove(dst, count);
                                dst -= count;
                            }
                        }
                        
                        src += count;
                        dst += count;
                    }
                }
            };
            
            documentListener = new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    stateChanged();
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    stateChanged();
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            };
            
            addPropertyChangeListener(SwingMisc.Store.DOCUMENT, e -> {
                Object o = e.getOldValue();
                Object n = e.getNewValue();
                if (n instanceof AbstractDocument) {
                    ((AbstractDocument) n).setDocumentFilter(documentFilter);
                }
                if (o instanceof Document) {
                    ((Document) o).removeDocumentListener(documentListener);
                }
                if (n instanceof Document) {
                    ((Document) n).addDocumentListener(documentListener);
                }
            });
            
            setDocument(new PlainDocument());
            sendEvents = true;
        }
        
        @Override
        public void setText(String text) {
            sendEvents = false;
            try {
                super.setText(text);
            } finally {
                sendEvents = true;
            }
        }
        
        public void addChangeListener(ChangeListener listener) {
            changeListeners = Misc.append(changeListeners, Objects.requireNonNull(listener, "listener"));
        }
        
        public void removeChangeListener(ChangeListener listener) {
            changeListeners = Misc.remove(changeListeners, Objects.requireNonNull(listener, "listener"));
        }
        
        public ChangeListener[] getChangeListeners() {
            return changeListeners.clone();
        }
        
        private void stateChanged() {
            if (sendEvents) {
                for (ChangeListener listener : changeListeners)
                    listener.stateChanged(changeEvent);
            }
        }
    }
    
    private static final class DecimalMarkListener extends SwingSettingWeakListener<CodePoint, SizeField> {
        private DecimalMarkListener(SizeField field) {
            super(field);
        }
        
        @Override
        protected void settingChangedOnEdt(SizeField          field,
                                           Settings           sets,
                                           Setting<CodePoint> set,
                                           CodePoint          old,
                                           CodePoint          mark) {
            field.sendEvents = false;
            try {
                String text = field.getText();
                field.setText("");
                field.decimalMark = mark.intValue();
                field.setText(text.replace(old.toString(), mark.toString()));
            } finally {
                field.sendEvents = true;
            }
            field.stateChanged();
        }
    }
    
    private static final class StyleBooleanOption extends JComponent {
        final OptionButton[] buttons = {
            new OptionButton("On",        true ),
            new OptionButton("Off",       false),
            new OptionButton("Inherited", null ),
        };
        
        JComponent getOnButton() {
            return buttons[0];
        }
        
        JComponent getOffButton() {
            return buttons[1];
        }
        
        JComponent getInheritedButton() {
            return buttons[2];
        }
        
        private final ActionEvent actionEvent     = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "selected");
        private ActionListener[]  actionListeners = new ActionListener[0];
        
        StyleBooleanOption(String label) {
            setLayout(new FlowLayout(FlowLayout.LEFT));
            setOpaque(false);
            
            if (label != null) {
                setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), label));
            } else {
                setBorder(BorderFactory.createEtchedBorder());
            }
            
            ButtonGroup group = new ButtonGroup();
            
            for (OptionButton btn : buttons) {
                group.add(btn);
                add(btn);
                
                btn.addActionListener(e -> {
                    if (btn.isSelected()) {
                        actionPerformed();
                    }
                });
            }
        }
        
        public void addActionListener(ActionListener listener) {
            actionListeners = Misc.append(actionListeners, Objects.requireNonNull(listener, "listener"));
        }
        
        public void removeActionListener(ActionListener listener) {
            actionListeners = Misc.remove(actionListeners, Objects.requireNonNull(listener, "listener"));
        }
        
        public ActionListener[] getActionListeners() {
            return actionListeners.clone();
        }
        
        private void actionPerformed() {
            for (ActionListener listener : actionListeners)
                listener.actionPerformed(actionEvent);
        }
        
        @Override
        public Dimension getMaximumSize() {
            Dimension max = super.getMaximumSize();
            max.height = super.getPreferredSize().height;
            return max;
        }
        
        @Override
        public void setEnabled(boolean enabled) {
            super.setEnabled(enabled);
            for (OptionButton btn : buttons) {
                btn.setEnabled(enabled);
            }
        }
        
        Boolean getBooleanValue() {
            for (OptionButton btn : buttons)
                if (btn.isSelected())
                    return btn.opt;
            assert false : this;
            return null;
        }
        
        void setBooleanValue(Boolean value) {
            for (OptionButton btn : buttons) {
                if (Objects.equals(btn.opt, value)) {
                    btn.setSelected(true);
                    break;
                }
            }
        }
        
        final class OptionButton extends JRadioButton {
            final Boolean opt;
            
            OptionButton(String label, Boolean opt) {
                super(label);
                this.opt = opt;
            }
        }
    }
    
    private static final class Family {
        static final Family INHERITED = new Family(null);
        
        private final String name;
        
        Family(String name) {
            this.name = name;
        }
        
        String getName() {
            return name;
        }
        
        static String getName(Family fam) {
            return fam == null ? null : fam.name;
        }
        
        boolean isInherited() {
            return name == null;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(name);
        }
        
        @Override
        public boolean equals(Object obj) {
            return (obj instanceof Family) && Objects.equals(this.name, ((Family) obj).name);
        }
        
        @Override
        public String toString() {
            return name == null ? "(Inherited)" : name;
        }
    }
    
    private static final class FamilyCellRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                                                      Object   value,
                                                      int      index,
                                                      boolean  isSelected,
                                                      boolean  cellHasFocus) {
            String text = (value instanceof Family) ? value.toString() : null;
            Component comp = super.getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
            if (value instanceof Family && ((Family) value).isInherited()) {
                Font font = comp.getFont();
                comp.setFont(font.deriveFont(font.getStyle() | Font.ITALIC));
            }
            return comp;
        }
    }
}