/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.util.*;
import static eps.util.Literals.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import java.util.*;
import java.util.List;

final class ParsingEditor extends JPanel implements SwingSettingsWindow.Tab {
    private final SwingSettingsWindow window;
    
    private final CharTextField decimalMark;
    private final CharTextField digitSeparator;
    private final CharTextField boundaryJoiner;
    
    private final List<CharTextField> charFields;
    
    private final JList<CodePoint> boundaryChars;
    private final BoundaryCharModel boundaryCharsModel;
    
    private final JButton addBoundary;
    private final JButton removeBoundary;
    private final JButton resetBoundaries;
    
    ParsingEditor(SwingSettingsWindow window) {
        this.window = Objects.requireNonNull(window, "window");
        
        int minor = Settings.getOrDefault(Setting.MINOR_MARGIN);
        setLayout(new GridLayout(1, 2, minor, minor));
        
        JPanel left = new JPanel();
        JPanel right = new JPanel();
        
        left.setOpaque(false);
        right.setOpaque(false);
        
        left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
        right.setLayout(new BorderLayout(minor, minor));
        
        add(left);
        add(right);
        
        decimalMark    = new CharTextField(Setting.DECIMAL_MARK,    "Decimal Mark");
        digitSeparator = new CharTextField(Setting.DIGIT_SEPARATOR, "Digit Separator");
        boundaryJoiner = new CharTextField(Setting.BOUNDARY_JOINER, "Boundary Joiner");
        
        charFields = ListOf(decimalMark, digitSeparator, boundaryJoiner);
        
        charFields.forEach(cf -> left.add(cf.createPanel()));
        
        boundaryChars = new JList<>();
        boundaryCharsModel = new BoundaryCharModel();
        
        boundaryChars.setModel(boundaryCharsModel);
        boundaryChars.setCellRenderer(new CodePointRenderer());
        boundaryChars.setDragEnabled(false);
        boundaryChars.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        boundaryChars.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        boundaryChars.setVisibleRowCount(0);
        
        boundaryChars.setToolTipText("<html><body style='width:500'>" + Setting.BOUNDARY_CHARS.getDescription() + "</body></html>");
        
        JScrollPane boundaryCharsScroll =
            new JScrollPane(boundaryChars, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                           JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        JToolBar boundaryTools = new JToolBar(JToolBar.HORIZONTAL);
        boundaryTools.setFloatable(false);
        boundaryTools.setOpaque(false);
        
        addBoundary = new JButton("Add");
        removeBoundary = new JButton("Remove");
        resetBoundaries = new JButton("Reset");
        
        addBoundary.setToolTipText("Add one more boundary characters.");
        removeBoundary.setToolTipText("Remove the selected boundary character.");
        resetBoundaries.setToolTipText("Reset the boundary characters to the default set.");
        
        addBoundary.addActionListener(e -> addBoundary());
        removeBoundary.addActionListener(e -> removeBoundary());
        resetBoundaries.addActionListener(e -> resetBoundaries());
        
        boundaryChars.addListSelectionListener(e -> updateBoundaryButtons(null));
        
        boundaryTools.add(resetBoundaries);
        boundaryTools.addSeparator();
        boundaryTools.add(addBoundary);
        boundaryTools.add(removeBoundary);
        
        JPanel boundaryCharsPane = new JPanel(new BorderLayout(minor, minor));
        boundaryCharsPane.setOpaque(false);
        boundaryCharsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Boundary Characters"));
        
        boundaryCharsPane.add(boundaryTools, BorderLayout.NORTH);
        boundaryCharsPane.add(boundaryCharsScroll, BorderLayout.CENTER);
        
        right.add(boundaryCharsPane, BorderLayout.CENTER);
    }
    
    @Override
    public void refresh() {
        charFields.forEach(CharTextField::update);
        boundaryCharsModel.reset(false);
    }
    
    private void addBoundary() {
        String input =
            JOptionPane.showInputDialog(this,
                                        "Enter one or more characters to add to the boundary characters.",
                                        "Add Boundary Characters",
                                        JOptionPane.PLAIN_MESSAGE);
        if (input != null && !input.isEmpty()) {
            boundaryCharsModel.addElements(input);
            boundariesChanged();
        }
    }
    
    private void removeBoundary() {
        CodePoint sel = boundaryChars.getSelectedValue();
        if (sel != null) {
            boundaryCharsModel.removeElement(sel);
            boundariesChanged();
        }
    }
    
    private void resetBoundaries() {
        String message =
              "<html><body>"
            + "This will reset the set of boundary characters back to the default,"
            + "<br/>losing any customizations."
            + "<br/><br/>"
            + "Are you sure you want to perform this action?"
            + "</body></html>";
        int choice =
            JOptionPane.showConfirmDialog(this,
                                          message,
                                          "Confirm Reset",
                                          JOptionPane.YES_NO_OPTION,
                                          JOptionPane.QUESTION_MESSAGE);
        if (choice == JOptionPane.YES_OPTION) {
            boundaryCharsModel.reset(true);
            boundariesChanged();
        }
    }
    
    private void boundariesChanged() {
        Set<CodePoint> bounds = new LinkedHashSet<>();
        int            size   = boundaryCharsModel.getSize();
        for (int i = 0; i < size; ++i) {
            bounds.add(boundaryCharsModel.getElementAt(i));
        }
        window.putSetting(Setting.BOUNDARY_CHARS, bounds);
        updateBoundaryButtons(bounds);
    }
    
    private void updateBoundaryButtons(Set<CodePoint> set) {
        if (false) {
            if (set != null) {
                resetBoundaries.setEnabled(!Setting.BOUNDARY_CHARS.getDefaultValue().equals(set));
            }
            removeBoundary.setEnabled(boundaryChars.getSelectedValue() != null);
        }
    }
    
    private final class CharTextField extends JTextField {
        private final DocumentListener listener;
        private final DocumentFilter filter;
        
        private final String label;
        private final Setting<CodePoint> set;
        
        CharTextField(Setting<CodePoint> set, String label) {
            super(1);
            
            this.label = label;
            this.set   = Objects.requireNonNull(set, "set");
            
            setHorizontalAlignment(SwingConstants.CENTER);
            setToolTipText("<html><body style='width:500'><p>" + set.getDescription() + "</p></body></html>");
            
            listener = new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    charChanged();
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    charChanged();
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                }
            };
            
            filter = new DocumentFilter() {
                @Override
                public void remove(FilterBypass fb, int offs, int len)
                        throws BadLocationException {
                    fb.remove(offs, len);
                }
                @Override
                public void replace(FilterBypass fb, int offs, int len, String str, AttributeSet atts)
                        throws BadLocationException {
                    fb.remove(offs, len);
                    insertString(fb, offs, str, atts);
                }
                @Override
                public void insertString(FilterBypass fb, int offs, String str, AttributeSet atts)
                        throws BadLocationException {
                    if (!str.isEmpty()) {
                        int code0 = str.codePointAt(0);
                        int count = Character.charCount(code0);
                        if (count < str.length()) {
                            str = str.substring(0, count);
                        }
                        
                        Document doc = fb.getDocument();
                        int      len = doc.getLength();
                        if (len != 0) {
                            fb.remove(0, len);
                            offs = 0;
                        }
                        
                        fb.insertString(offs, str, atts);
                    }
                }
            };
            
            addPropertyChangeListener(SwingMisc.Store.DOCUMENT, e -> {
                Object o = e.getOldValue();
                Object n = e.getNewValue();
                if (o instanceof Document)
                    ((Document) o).removeDocumentListener(listener);
                if (n instanceof Document)
                    ((Document) n).addDocumentListener(listener);
                if (n instanceof AbstractDocument)
                    ((AbstractDocument) n).setDocumentFilter(filter);
            });
            
            setDocument(new PlainDocument());
        }
        
        private void charChanged() {
            String    text = getText();
            CodePoint code = null;
            if (!text.isEmpty()) {
                code = CodePoint.valueOf(text.codePointAt(0));
            }
            
            if (set.isValue(code)) {
                window.putSetting(set, code);
            }
        }
        
        void update() {
            getDocument().removeDocumentListener(listener);
            try {
                CodePoint code = window.getSetting(set);
                setText(code == null ? "" : code.toString());
            } finally {
                getDocument().addDocumentListener(listener);
            }
        }
        
        JPanel createPanel() {
            JPanel pane = new JPanel(new BorderLayout()) {
                @Override
                public Dimension getMaximumSize() {
                    Dimension max = super.getMaximumSize();
                    max.height = getPreferredSize().height;
                    return max;
                }
            };
            
            pane.setOpaque(false);
            
            if (label == null) {
                pane.setBorder(BorderFactory.createEtchedBorder());
            } else {
                pane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), label));
            }
            
            pane.add(this, BorderLayout.CENTER);
            return pane;
        }
    }
    
    private final class BoundaryCharModel extends DefaultListModel<CodePoint> {
        public void reset(boolean toDefault) {
            removeAllElements();
            
            Set<CodePoint> elems;
            if (toDefault) {
                elems = Setting.BOUNDARY_CHARS.getDefaultValue();
            } else {
                elems = window.getSetting(Setting.BOUNDARY_CHARS);
            }
            
            addElements(elems);
            updateBoundaryButtons(elems);
        }
        
        @Override
        public void addElement(CodePoint code) {
            if (!contains(code)) {
                super.addElement(code);
            }
        }
        
        public void addElements(String elems) {
            int len = elems.length();
            for (int i = 0; i < len;) {
                int code = elems.codePointAt(i);
                addElement(CodePoint.valueOf(code));
                i += Character.charCount(code);
            }
        }
        
        public void addElements(Iterable<? extends CodePoint> elems) {
            for (CodePoint code : elems)
                addElement(code);
        }
    }
    
    private static final class CodePointRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                                                      Object   value,
                                                      int      index,
                                                      boolean  isSelected,
                                                      boolean  cellHasFocus) {
            String str = (value != null) ? ((CodePoint) value).toString() : null;
            return super.getListCellRendererComponent(list, str, index, isSelected, cellHasFocus);
        }
    }
}