/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.ui.Style;
import static eps.ui.swing.SwingMisc.*;
import static eps.app.App.*;

import javax.swing.*;
import javax.swing.text.*;

abstract class AbstractSwingTextCell extends SwingCell implements GeneralTextCell {
    private Setting<Style> toFollow;
    
    private final BasicStyleListener basicListener = new BasicStyleListener(this);
    private final TextStyleListener  follower      = new TextStyleListener(this);
    
    AbstractSwingTextCell() {
    }
    
    protected void afterTextComponentInitialization() {
        settingsListeners.addAndSet(Setting.BASIC_STYLE, basicListener);
    }
    
    protected abstract JComponent getTextComponent();
    
    @Override
    public Setting<Style> getFollowedStyle() {
        return this.toFollow;
    }
    
    @Override
    public GeneralTextCell followTextStyle(Setting<Style> toFollow) {
        requireEdt();
        
        Setting<Style> prev = this.toFollow;
        if (prev != null) {
            settingsListeners.remove(prev, follower);
        }
        
        settingsListeners.add(toFollow, follower);
//        System.err.println(settingsListeners);
        
        this.toFollow = toFollow;
        this.setTextStyle(toFollow);
        return this;
    }
    
    @Override
    public String getText() {
        requireEdt();
        
        JComponent comp = getTextComponent();
        
        if (comp instanceof JLabel) {
            return ((JLabel) comp).getText();
        } else if (comp instanceof JTextComponent) {
            return ((JTextComponent) comp).getText();
        } else {
            throw new AssertionError(comp);
        }
    }
    
    @Override
    public void setText(String text) {
        requireEdt();
        if (text == null) {
            text = "";
        }
        
        JComponent comp = getTextComponent();
        
        if (comp instanceof JLabel) {
            ((JLabel) comp).setText(text);
        } else if (comp instanceof JTextComponent) {
            ((JTextComponent) comp).setText(text);
        } else {
            throw new AssertionError(comp);
        }
        
        this.setToolTipText(text);
        app().itemsChanged();
    }
    
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions", "ParameterCanBeLocal"})
    protected void setToolTipText(String text) {
        // this probably isn't actually good
        if (true) {
            return;
        }
        // Clip multi-line text to just the first line.
        int n = text.indexOf("\n");
        int r = text.indexOf("\r");
        int i = (n < 0) ? ((r < 0) ? -1 : r) : ((r >= 0) ? Math.min(r, n) : n);
        
        if (i >= 0) {
            text = text.substring(0, i) + " ...";
        }
        
        getComponent().setToolTipText(text);
    }
    
    @Override
    public ArgbColor getTextColor() {
        requireEdt();
        return fromAwtColor(getTextComponent().getForeground());
    }
    
    @Override
    public void setTextColor(ArgbColor color) {
        requireEdt();
        getTextComponent().setForeground(toAwtColor(color));
        app().itemsChanged();
    }
    
    @Override
    public void setTextStyle(Setting<Style> setting) {
        requireEdt();
        Style abs = Theme.getAbsoluteStyle(setting);
        
        JComponent comp = getTextComponent();
        comp.setFont(SwingMisc.toAwtFontAbs(abs));
        
        this.setTextColor(abs.getForeground());
        comp.revalidate();
        comp.repaint();
        
        app().itemsChanged();
    }
    
    /*
    @Override
    public void setTextStyle(Style style) {
        requireEdt();
        JComponent comp = getTextComponent();
        comp.setFont(SwingMisc.toAwtFont(style));
        
//        Log.note(AbstractSwingTextCell.class, "setTextStyle", comp.getFont(), ", ", style);
        
        ArgbColor color = Settings.getAbsolute(style, Style::getForeground);
        assert color != null : style;
        
        this.setTextColor(color);
        comp.revalidate();
        comp.repaint();
        
        app().itemsChanged();
    }
    */
    
    private static final class BasicStyleListener extends SwingSettingWeakListener<Style, AbstractSwingTextCell> {
        private BasicStyleListener(AbstractSwingTextCell cell) {
            super(cell);
        }
        @Override
        protected void settingChangedOnEdt(AbstractSwingTextCell cell,
                                           Settings              settings,
                                           Setting<Style>        setting,
                                           Style                 oldValue,
                                           Style                 newValue) {
            if (cell.toFollow != null) {
                cell.setTextStyle(cell.toFollow);
            }
        }
    }
    
    private static final class TextStyleListener extends SwingSettingWeakListener<Style, AbstractSwingTextCell> {
        private TextStyleListener(AbstractSwingTextCell cell) {
            super(cell);
        }
        @Override
        protected void settingChangedOnEdt(AbstractSwingTextCell cell,
                                           Settings              settings,
                                           Setting<Style>        setting,
                                           Style                 oldValue,
                                           Style                 newValue) {
            cell.setTextStyle(setting);
        }
    }
}