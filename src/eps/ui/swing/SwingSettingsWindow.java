/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.app.*;
import eps.util.*;
import static eps.app.App.*;
import static eps.ui.swing.SwingMisc.*;
import static eps.util.Literals.*;

import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;

// To Add:
//  * leftParenthesis/rightParenthesis?
final class SwingSettingsWindow implements SettingsWindow, DialogChild {
    private final JDialog dialog;
    private final JTabbedPane tabPane;
    
    private final JButton cancel;
    private final JButton apply;
    private final JButton ok;
    
    private final GeneralTab generalTab;
    private final ParsingEditor parsingTab;
    private final ThemeEditor appearanceTab;
    
    private final List<Tab> tabs;
    
    private ActionListener[] cancelListeners = new ActionListener[0];
    private ActionListener[] applyListeners  = new ActionListener[0];
    
    private final Map<Setting<?>, Object> changes = new LinkedHashMap<>();
    
    private boolean initialized = false;
    
    SwingSettingsWindow() {
        requireEdt();
        
        dialog = new JDialog(getFrame());
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.setModal(false);
        dialog.setTitle("Epsilon Settings");
        
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // TODO: ask for confirmation when unsaved changes?
                cancel(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, "cancel"));
            }
        });
        
        SwingMisc.onFirstOpen(dialog, () -> dialog.setMinimumSize(dialog.getPreferredSize()));
        
        JPanel content = new JPanel(new BorderLayout());
        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        
        cancel = new JButton("Cancel");
        apply  = new JButton("Apply");
        ok     = new JButton("OK");
        
        cancel.setActionCommand("cancel");
        apply.setActionCommand("apply");
        ok.setActionCommand("apply");
        
        for (JButton btn : ListOf(cancel, apply, ok)) {
            buttons.add(btn);
        }
        
        tabPane = new JTabbedPane(JTabbedPane.TOP);
        
        generalTab    = decorate(new GeneralTab());
        parsingTab    = decorate(new ParsingEditor(this));
        appearanceTab = decorate(new ThemeEditor(this));
        
        tabs = ListOf(generalTab, parsingTab, appearanceTab);
        
        tabPane.addTab("General",    generalTab);
        tabPane.addTab("Parsing",    parsingTab);
        tabPane.addTab("Appearance", appearanceTab);
        
        content.add(tabPane, BorderLayout.CENTER);
        content.add(buttons, BorderLayout.SOUTH);
        
        dialog.setContentPane(content);
        dialog.getRootPane().setDefaultButton(ok);
        setBounds(null);
        
        cancel.addActionListener(this::cancel);
        apply.addActionListener(this::apply);
        ok.addActionListener(this::ok);
        
        tabPane.addChangeListener(e -> selectedTabActivated());
        
        initialized = true;
        assert changes.isEmpty() : changes;
        
        tabPane.setSelectedComponent(generalTab);
//        tabs.setSelectedComponent(appearanceTab);
    }
    
    private <C extends JComponent> C decorate(C tab) {
        int minor = Settings.getOrDefault(Setting.MINOR_MARGIN);
        tab.setBorder(BorderFactory.createEmptyBorder(minor, minor, minor, minor));
        tab.setOpaque(false);
        return tab;
    }
    
    private JFrame getFrame() {
        return ((SwingMainWindow) app().getMainWindow()).getFrame();
    }
    
    @Override
    public JDialog getDialog() {
        return dialog;
    }
    
    @Override
    public void refresh() {
        tabs.forEach(Tab::refresh);
    }
    
    @Override
    public void close() {
        dialog.setVisible(false);
        dialog.dispose();
    }
    
    @Override
    public boolean isVisible() {
        return dialog.isVisible();
    }
    
    @Override
    public void setVisible(boolean isVisible) {
        dialog.setVisible(isVisible);
        if (isVisible) {
            selectedTabActivated();
        }
    }
    
    @Override
    public BoundingBox getBounds() {
        return SwingMisc.getBounds(dialog);
    }
    
    @Override
    public void setBounds(BoundingBox bounds) {
        if (bounds != null) {
            SwingMisc.setBounds(dialog, bounds);
            dialog.revalidate();
            dialog.repaint();
        } else {
            dialog.pack();
            dialog.setLocationRelativeTo(getFrame());
        }
    }
    
    private void cancel(ActionEvent e) {
        changes.clear();
        modified();
        
        for (ActionListener al : cancelListeners)
            al.actionPerformed(e);
        
        dialog.setVisible(false);
    }
    
    private void apply(ActionEvent e) {
        Settings settings = app().getSettings();
        
        changes.forEach((key, val) -> set(settings, key, val));
        changes.clear();
        modified();
        
        waitForSettings(settings);
        
        for (ActionListener al : applyListeners)
            al.actionPerformed(e);
    }
    
    private void waitForSettings(Settings settings) {
        if (settings.hasActionsWaiting()) {
            String title   = "Settings In Use";
            String message = "Waiting while the application settings are in use...";
            SwingMisc.await(dialog, title, message, settings::hasActionsWaiting);
        }
    }
    
    private <T> void set(Settings settings, Setting<T> key, Object val) {
        try {
            settings.set(key, key.cast(val));
        } catch (IllegalArgumentException | ClassCastException | NullPointerException x) {
            Log.caught(SwingSettingsWindow.class, "while setting " + key, x, false);
        }
    }
    
    private void ok(ActionEvent e) {
        apply(e);
        dialog.setVisible(false);
    }
    
    private void selectedTabActivated() {
        Component comp = tabPane.getSelectedComponent();
        if (comp instanceof Tab) {
            ((Tab) comp).activated();
        }
    }
    
    <T> boolean putSetting(Setting<T> key, T val) {
        boolean changed = false;
        if (initialized) {
            changes.remove(key); // preserve ordering
            
            T prev = app().getSetting(key);
            
            if (!Objects.equals(prev, val)) {
                changes.put(key, val);
                changed = true;
            }
            
            modified();
        }
        return changed;
    }
    
    void removeSetting(Setting<?> key) {
        if (initialized) {
            changes.remove(key);
            modified();
        }
    }
    
    private void modified() {
        modified(!changes.isEmpty());
    }
    
    private void modified(boolean modified) {
        SwingMisc.setModified(dialog, modified);
    }
    
    <T> T getSetting(Setting<T> key) {
        if (changes.containsKey(key)) {
            return key.cast(changes.get(key));
        }
        return app().getSetting(key);
    }
    
    void addCancelListener(ActionListener al) {
        cancelListeners = Misc.append(cancelListeners, Objects.requireNonNull(al, "al"));
    }
    
    void removeCancelListener(ActionListener al) {
        cancelListeners = Misc.remove(cancelListeners, Objects.requireNonNull(al, "al"));
    }
    
    void addApplyListener(ActionListener al) {
        applyListeners = Misc.append(applyListeners, Objects.requireNonNull(al, "al"));
    }
    
    void removeApplyListener(ActionListener al) {
        applyListeners = Misc.remove(applyListeners, Objects.requireNonNull(al, "al"));
    }
    
    interface Tab {
        default void activated() {
            refresh();
        }
        void refresh();
    }
    
    private final class GeneralTab extends JPanel implements Tab {
        private final NumberPanel<?> maxItems;
        private final NumberPanel<?> maxDigits;
        private final NumberPanel<?> intCacheSize;
        private final NumberPanel<?> primeCacheSize;
        private final NumberPanel<?> recursionDepth;
        
        private final JTextField editorIndent;
        
        private final JCheckBox editorLineWrap;
        
        private final List<NumberPanel<?>> numPanes;
        
        GeneralTab() {
            setOpaque(false);
            setLayout(new GridLayout(1, 2));
            
            JPanel left = new JPanel();
            JPanel right = new JPanel();
            left.setOpaque(false);
            right.setOpaque(false);
            left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
            right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
            
            add(left);
            add(right);
            
            maxItems       = new NumberPanel<>(Setting.MAX_ITEMS,           "Maximum History Items");
            maxDigits      = new NumberPanel<>(Setting.MAX_FRACTION_DIGITS, "Maximum Fraction Digits Displayed");
            intCacheSize   = new NumberPanel<>(Setting.INTEGER_CACHE_SIZE,  "Integer Cache Size");
            primeCacheSize = new NumberPanel<>(Setting.PRIME_CACHE_SIZE,    "Prime Number Cache Size");
            recursionDepth = new NumberPanel<>(Setting.MAX_RECURSION_DEPTH, "Maximum Recursion Depth");
            
            String recursionTip =
                  "<p>" + Setting.MAX_RECURSION_DEPTH.getDescription() + "</p>"
                + "<br/>"
                + "<p>As an example, the following is a simple recursive factorial function:</p>"
                + "<br/>"
                + "<pre>f(x) := for({x <= 0}: 1, x * f(x - 1))</pre>";
            
            recursionDepth.setTip(recursionTip);
            
            numPanes = ListOf(maxItems, maxDigits, intCacheSize, primeCacheSize, recursionDepth);
            numPanes.forEach(left::add);
            
            editorIndent = new JTextField();
            
            editorIndent.setToolTipText("<html><body>"
                                      + "The indent inserted by a tab keystroke in the entry text field."
                                      + "<br/><br/>"
                                      + "Use the character sequence \\t to represent the tab character."
                                      + "</body></html>");
            
            JPanel editorIndentPane = new JPanel(new BorderLayout()) {
                @Override
                public Dimension getMaximumSize() {
                    Dimension max = super.getMaximumSize();
                    max.height = getPreferredSize().height;
                    return max;
                }
            };
            editorIndentPane.setOpaque(false);
            editorIndentPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Editor Indent"));
            editorIndentPane.add(editorIndent, BorderLayout.CENTER);
            
            left.add(editorIndentPane);
            
            DocumentListener editorListener = new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    put();
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    put();
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                }
                private void put() {
                    String indent = editorIndent.getText().replace("\\t", "\t");
                    putSetting(Setting.EDITOR_INDENT, '"' + indent + '"');
                }
            };
            editorIndent.getDocument().addDocumentListener(editorListener);
            editorIndent.addPropertyChangeListener(SwingMisc.Store.DOCUMENT, e -> {
                Object o = e.getOldValue();
                Object n = e.getNewValue();
                if (o instanceof Document)
                    ((Document) o).removeDocumentListener(editorListener);
                if (n instanceof Document)
                    ((Document) n).addDocumentListener(editorListener);
            });
            
            editorLineWrap = new JCheckBox("Multiline Editor Line Wrap");
            editorLineWrap.addActionListener(e -> {
                putSetting(Setting.EDITOR_WRAP, editorLineWrap.isSelected());
            });
            
            editorLineWrap.setToolTipText("Sets whether or not the multiline entry text fields have line wrap.");
            
            right.add(editorLineWrap);
        }
        
        @Override
        public void refresh() {
            numPanes.forEach(NumberPanel::update);
            
            String indent = getSetting(Setting.EDITOR_INDENT);
            int    length = indent.length();
            if (length >= 2 && indent.codePointAt(0) == '"' && indent.codePointBefore(length) == '"') {
                indent = indent.substring(1, length - 1);
            }
            
            indent = indent.replace("\t", "\\t");
            
            editorIndent.setText(indent);
            
            editorLineWrap.setSelected(getSetting(Setting.EDITOR_WRAP));
        }
    }
    
    private class NumberPanel<N extends Number & Comparable<N>> extends JPanel {
        private final Setting<N> set;
        private final NumberTextField<N> field;
        
        NumberPanel(Setting<N> set, String title) {
            super(new BorderLayout());
            
            this.set = Objects.requireNonNull(set, "set");
            
            if (title == null) {
                title = set.getName();
            }
            
            setOpaque(false);
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
            
            field = NumberTextField.of(set.getType(),
                                       Setting.getMinimum(set),
                                       Setting.getMaximum(set),
                                       Setting.isClosed(set));
            
            add(field, BorderLayout.CENTER);
            
            field.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                    field.setValue(getSetting(set));
                }
            });
            
            field.addChangeListener(e -> {
                N val = field.getValue();
                
                if (val == null) {
                    removeSetting(set);
                } else {
                    putSetting(set, val);
                }
            });
            
            setTip("<p>" + set.getDescription() + "</p>");
        }
        
        @Override
        public Dimension getMaximumSize() {
            Dimension max = super.getMaximumSize();
            max.height = getPreferredSize().height;
            return max;
        }
        
        void setTip(String tip) {
            N       min    = Setting.getMinimum(set);
            N       max    = Setting.getMaximum(set);
            boolean closed = Setting.isClosed(set);
            String  range  = "The supported range for this setting is " + min + " (inclusive) to " + max + " (" + (closed ? "in" : "ex") + "clusive).";
            field.setToolTipText("<html><body style='width:500'>" + tip + "<br/><p>" + range + "</p></body></html>");
        }
        
        void update() {
            field.setValue(getSetting(set));
        }
    }
}