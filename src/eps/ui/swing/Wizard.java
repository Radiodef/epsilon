/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import static eps.app.App.*;
import static eps.util.Literals.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.List;
import java.util.*;

class Wizard implements AutoCloseable {
    public static final String CANCEL   = "Cancel";
    public static final String PREVIOUS = "Previous";
    public static final String NEXT     = "Next";
    public static final String DONE     = "Done";
    
    protected final JDialog diag;
    protected final JLabel title;
    protected final JPanel cards;
    
    protected final JButton cancel;
    protected final JButton prev;
    protected final JButton next;
    protected final JPanel buttons;
    
    protected final List<Step> steps;
    protected boolean wasCancelled;
    protected boolean isDone;
    
    protected final int minorMargin;
    protected final int majorMargin;
    protected final int outerMargin;
    
    private Map<String, Object> results;

    Wizard(Window parent) {
        minorMargin = getMinorMargin();
        majorMargin = getMajorMargin();
        outerMargin = getOuterMargin();
        
        diag = new JDialog(parent);
//        diag.setAlwaysOnTop(parent.isAlwaysOnTop());
        diag.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        diag.setUndecorated(true);
        
        SwingMisc.onFirstOpen(diag, diag::toFront);
        
        title   = new JLabel();
        buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        cards   = new JPanel(new CardLayout());
        steps   = new ArrayList<>();
        
        cancel = new JButton(CANCEL);
        prev   = new JButton(PREVIOUS);
        next   = new JButton(NEXT);
        
        cancel.addActionListener(e -> run(this::cancel));
        prev.addActionListener(e -> run(this::prev));
        next.addActionListener(e -> run(this::next));
        
        forEach(cancel, prev, next, btn -> {
            btn.setActionCommand(btn.getText());
            buttons.add(btn);
        });
        
        diag.getRootPane().setDefaultButton(next);

        JPanel content = new JPanel(new BorderLayout());
        JPanel titlePane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        titlePane.add(title);
        content.add(titlePane, BorderLayout.NORTH);
        content.add(cards, BorderLayout.CENTER);
        content.add(buttons, BorderLayout.SOUTH);

        int sm = minorMargin;
        int lg = outerMargin;

        cards.setBorder(BorderFactory.createEmptyBorder(lg, lg, lg, lg));
        titlePane.setBorder(BorderFactory.createEmptyBorder(sm, sm, 0, sm));
        
        cards.add("qwertuiopasdfghjklzxcvbnm", new BoxDescriptionStep("card_layout_min_sizer"));
        
        diag.setContentPane(content);
        new MouseMover(diag);
    }
    
    public JDialog getDialog() {
        return diag;
    }
    
    public void showErrorMessage(String title, String format, Object... args) {
        showErrorMessage(diag, title, format, args);
    }
    
    public static void showErrorMessage(Window parent, String title, String format, Object... args) {
        String message = String.format(format, args);
        JOptionPane.showMessageDialog(parent,
                                      message,
                                      title,
                                      JOptionPane.ERROR_MESSAGE);
    }
    
    protected static int getMinorMargin() {
        return app().getSetting(Setting.MINOR_MARGIN);
    }
    
    protected static int getMajorMargin() {
        return app().getSetting(Setting.MAJOR_MARGIN);
    }
    
    protected static int getOuterMargin() {
        return getMajorMargin() * 4;
    }
    
    public Wizard addStep(Step step) {
        Objects.requireNonNull(step, "step");
        step.setWizard(this);
        steps.add(step);
        cards.add(step.getTitle(), step);
        return this;
    }
    
    public int indexOf(Step step) {
        return steps.indexOf(step);
    }
    
    public int stepCount() {
        return steps.size();
    }
    
    public Map<String, Object> getCurrentResults() {
        if (results == null) {
            throw new IllegalStateException();
        } else {
            return Collections.unmodifiableMap(results);
        }
    }
    
    public Map<String, Object> show() {
        if (results != null) {
            throw new IllegalStateException(results.toString());
        }
        Step first = null;
        for (Step step : steps) {
            if (step.isFirstStep() && (first == null))
                first = step;
            step.reset();
        }
        if (first != null) {
            try {
                results = new LinkedHashMap<>();
                show(first, NEXT);
                diag.pack();
                diag.setLocationRelativeTo(diag.getOwner());
                diag.setVisible(true);
                if (isDone()) {
                    return results;
                }
            } finally {
                results = null;
            }
        }
        return null;
    }
    
    protected void setTitle(String text) {
        if (text == null)
            text = "";
        title.setText(text);
        diag.setTitle(text);
    }
    
    protected void show(Step step, String cmd) {
        Objects.requireNonNull(step, "step");
        if (steps.contains(step)) {
            setTitle(step.getTitle());
            
            prev.setEnabled(!step.isFirstStep());
            next.setText(step.isLastStep() ? DONE : NEXT);
            
            ((CardLayout) cards.getLayout()).show(cards, step.getTitle());
            step.stepShown(cmd);
        } else {
            throw new IllegalArgumentException(step.toString());
        }
    }
    
    public int getCurrentStepIndex() {
        int count = steps.size();
        for (int i = 0; i < count; ++i) {
            if (steps.get(i).isVisible()) {
                return i;
            }
        }
        return -1;
    }
    
    public Step getCurrentStep() {
        int index = getCurrentStepIndex();
        return (index < 0) ? null : steps.get(index);
    }
    
    public Step getStep(int index) {
        return steps.get(index);
    }
    
    public Step getStep(String title) {
        for (Step step : steps)
            if (Objects.equals(step.title, title))
                return step;
        return null;
    }
    
    @Override
    public void close() {
        diag.setVisible(false);
        diag.dispose();
    }
    
    public boolean isDone() {
        return isDone;
    }
    
    private void run(Runnable r) {
        try {
            r.run();
        } catch (IllegalStateException x) {
            Log.caught(getClass(), "run", x, true);
        }
    }
    
    private void cancel() {
        run(this::cancelImpl);
    }
    
    private void cancelImpl() {
        isDone = false;
        
        Step current = getCurrentStep();
        if (current != null) {
            current.stepHidden(CANCEL);
        }
        
        close();
    }
    
    private void next() {
        run(this::nextImpl);
    }
    
    private boolean nextImpl() {
        Step current = getCurrentStep();
        if (current == null) {
            return assertFalse("No Current Step");
        }
        if (!current.canAdvance()) {
            return false;
        }
        String title = current.getNextStep();
        if (title != null) {
            Step next = getStep(title);
            if (next == null) {
                return assertFalse(title);
            }
            current.stepHidden(NEXT);
            results.put(current.getTitle(), current.getValue());
            next.setPreviousStep(current.getTitle());
            show(next, NEXT);
        } else {
            if (!current.isLastStep()) {
                return assertFalse(current.getTitle());
            }
            isDone = true;
            current.stepHidden(NEXT);
            results.put(current.getTitle(), current.getValue());
            close();
        }
        return true;
    }
    
    private void prev() {
        run(this::prevImpl);
    }
    
    private boolean prevImpl() {
        Step current = getCurrentStep();
        if (current == null) {
            return assertFalse("No Current Step");
        }
        if (current.isFirstStep()) {
            return assertFalse(current.getTitle());
        }
        String title = current.getPreviousStep();
        if (title == null) {
            return assertFalse(current.getTitle());
        }
        Step prev = getStep(title);
        if (prev == null) {
            return assertFalse(title);
        }
        current.stepHidden(PREVIOUS);
        results.remove(prev.getTitle());
        show(prev, PREVIOUS);
        return true;
    }
    
    private boolean assertFalse(String message) {
        assert false : message;
        return false;
    }
    
    public static class Step extends JPanel {
        protected Wizard wizard;
        
        protected final String title;
        
        protected final int minorMargin = getMinorMargin();
        protected final int majorMargin = getMajorMargin();
        protected final int outerMargin = getOuterMargin();
        
        protected boolean shownAtLeastOnce = false;
        
        protected String previousStep;
        
        public Step(String title) {
            this.title = Objects.requireNonNull(title, "title");
        }
        
        protected void setWizard(Wizard wizard) {
            this.wizard = wizard;
        }
        
        public boolean isWizardSet() {
            return wizard != null;
        }
        
        public Wizard getWizard() {
            if (wizard == null)
                throw new IllegalStateException();
            return wizard;
        }
        
        public JDialog getDialog() {
            return getWizard().getDialog();
        }
        
        public String getTitle() {
            return title;
        }
        
        protected void setPreviousStep(String previousStep) {
            this.previousStep = previousStep;
        }
        
        public String getPreviousStep() {
            return isFirstStep() ? null : previousStep;
        }
        
        public String getNextStep() {
            if (wizard != null) {
                int index = wizard.indexOf(this);
                if (0 <= index && (index + 1) < wizard.stepCount()) {
                    return wizard.getStep(index + 1).getTitle();
                }
            }
            return null;
        }
        
        public boolean isFirstStep() {
            return (wizard != null) && (wizard.indexOf(this) == 0);
        }
        
        public boolean isLastStep() {
            return (wizard != null) && (wizard.indexOf(this) == (wizard.stepCount() - 1));
        }
        
        public Object getValue() {
            return null;
        }
        
        public void setValue(Object value) {
        }
        
        protected void setNextEnabled(boolean enabled) {
            if (wizard != null) {
                wizard.next.setEnabled(enabled);
            }
        }
        
        protected boolean isNextEnabled() {
            return wizard != null && wizard.next.isEnabled();
        }
        
        protected void reset() {
            shownAtLeastOnce = false;
        }
        
        protected boolean canAdvance() {
            return (wizard == null) || isNextEnabled();
        }
        
        protected void stepShown(String cmd) {
            if (!shownAtLeastOnce) {
                onFirstShow();
            }
            shownAtLeastOnce = true;
        }
        
        protected void onFirstShow() {
        }
        
        protected void stepHidden(String cmd) {
        }
        
        protected IllegalStateException newIllegalStateException(Object message) {
            return new IllegalStateException(String.valueOf(message));
        }
        
        protected void showErrorMessage(String title, String format, Object... args) {
            if (wizard != null) {
                wizard.showErrorMessage(title, format, args);
            } else {
                Wizard.showErrorMessage(null, title, format, args);
            }
        }
    }
    
    public static class BoxDescriptionStep extends Step {
        protected final JComboBox<String> options;
        protected final JTextArea description;
        
        public BoxDescriptionStep(String title) {
            super(title);
            int gap = minorMargin;
            setLayout(new BorderLayout(gap, gap));
            
            options = new JComboBox<>();
            
            description = new JTextArea(10, 0);
            description.setEditable(false);
            description.setLineWrap(true);
            description.setWrapStyleWord(true);
            description.setBorder(BorderFactory.createEmptyBorder(gap, gap, gap, gap));
            
            Dimension pref = description.getPreferredSize();
            pref.width = pref.height * 3;
            description.setPreferredSize(pref);
            
            JScrollPane scroll =
                new JScrollPane(description, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                             JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            
            add(options, BorderLayout.NORTH);
            add(scroll, BorderLayout.CENTER);
            
            options.addItemListener(e -> updateView());
        }
        
        @Override
        protected void stepShown(String cmd) {
            super.stepShown(cmd);
            updateView();
        }
        
        protected void updateView() {
            setDescription();
            setNextEnabled(getSelectedOption() != null);
        }
        
        protected void addOption(String option) {
            Objects.requireNonNull(option, "option");
            options.addItem(option);
        }
        
        protected String getSelectedOption() {
            return (String) options.getSelectedItem();
        }
        
        protected void setSelectedOption(String option) {
            options.setSelectedItem(option);
            updateView();
        }
        
        @Override
        public Object getValue() {
            return getSelectedOption();
        }
        
        @Override
        public void setValue(Object value) {
            if (value instanceof String) {
                setSelectedOption((String) value);
            }
        }
        
        protected void setDescription() {
            String option = getSelectedOption();
            String text   = "";
            if (option != null) {
                text = getDescription(option);
                if (text == null) {
                    text = "";
                }
            }
            description.setText(text);
        }
        
        protected String getDescription(String option) {
            return "";
        }
    }
    
    private static final class MouseMover extends MouseAdapter implements ContainerListener {
        private final JDialog diag;
        
        private final Set<Component> comps = Collections.newSetFromMap(new IdentityHashMap<>());
        
        private Point mouseDown;
        private Point diagDown;
        
        MouseMover(JDialog diag) {
            this.diag = diag;
            attach(diag.getContentPane());
        }
        
        @Override
        public void mousePressed(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                mouseDown = e.getLocationOnScreen();
                diagDown  = diag.getLocation();
            }
        }
        
        @Override
        public void mouseReleased(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                mouseDown = null;
                diagDown  = null;
            }
        }
        
        @Override
        public void mouseDragged(MouseEvent e) {
            if (mouseDown != null) {
                Point loc = e.getLocationOnScreen();
                loc.x -= mouseDown.x;
                loc.y -= mouseDown.y;
                loc.x += diagDown.x;
                loc.y += diagDown.y;
                diag.setLocation(loc);
            }
        }
        
        @Override
        public void componentRemoved(ContainerEvent e) {
            detach(e.getComponent());
        }
        
        @Override
        public void componentAdded(ContainerEvent e) {
            attach(e.getComponent());
        }
        
        private void attach(Component comp) {
            if (!comps.add(comp)) {
                return;
            }
            if (comp instanceof Container) {
                Container cont = (Container) comp;
                cont.addContainerListener(this);
                for (Component child : cont.getComponents())
                    attach(child);
            }
            if (isMoveComponent(comp)) {
                comp.addMouseListener(this);
                comp.addMouseMotionListener(this);
            }
        }
        
        private void detach(Component comp) {
            if (!comps.remove(comp)) {
                return;
            }
            if (comp instanceof Container) {
                Container cont = (Container) comp;
                cont.removeContainerListener(this);
                for (Component child : cont.getComponents())
                    detach(child);
            }
            if (isMoveComponent(comp)) {
                comp.removeMouseListener(this);
                comp.removeMouseMotionListener(this);
            }
        }
        
        private boolean isMoveComponent(Component comp) {
            return comp instanceof JPanel
                || comp instanceof JLabel;
        }
    }
}