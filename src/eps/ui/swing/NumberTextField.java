/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.util.*;

import javax.swing.text.*;
import javax.swing.event.*;
import java.util.*;
import java.math.*;

abstract class NumberTextField<N extends Number & Comparable<N>> extends SafeChangeTextField {
    private final boolean allowNegative;
    
    private ChangeListener[] listeners = new ChangeListener[0];
    private final ChangeEvent event = new ChangeEvent(this);
    
    private final Class<N> type;
    private final N min;
    private final N max;
    
    private N value;
    
    private NumberTextField(Class<N> type, N min, N max, boolean allowNegative) {
        this.type = Objects.requireNonNull(type, "type");
        
        this.min = min;
        this.max = max;
        
        this.allowNegative = allowNegative;
        
        DocumentPropertyChangeListener.add(this, new DocListener(), new IntegerFilter(allowNegative));
//        setDocument(new PlainDocument());
    }
    
    public N getValue() {
        return value;
    }
    
    public void setValue(N value) {
        value = limit(value);
        if (!Objects.equals(getValue(), value)) {
            setText((value == null) ? "" : value.toString());
        }
    }
    
    private N limit(N value) {
        if (value != null) {
            if (min != null && value.compareTo(min) < 0)
                value = min;
            if (max != null && value.compareTo(max) > 0)
                value = max;
        }
        return value;
    }
    
    protected abstract boolean isNegative(N val);
    
    protected abstract N parse(String text);
    
    public void addChangeListener(ChangeListener cl) {
        listeners = Misc.append(listeners, Objects.requireNonNull(cl, "cl"));
    }
    
    public void removeChangeListener(ChangeListener cl) {
        listeners = Misc.remove(listeners, Objects.requireNonNull(cl, "cl"));
    }
    
    public ChangeListener[] getChangeListeners() {
        return listeners.clone();
    }
    
    static final class IntegerFilter extends DocumentFilter {
        private final boolean allowNegative;
        
        IntegerFilter(boolean allowNegative) {
            this.allowNegative = allowNegative;
        }
        
        private void noNegativeZero(FilterBypass bypass) throws BadLocationException {
            Document doc  = bypass.getDocument();
            String   text = doc.getText(0, doc.getLength());
            if ("-0".equals(text)) {
                bypass.remove(0, 1);
            }
        }
        
        @Override
        public void insertString(FilterBypass bypass, int offs, String str, AttributeSet atts)
                throws BadLocationException {
            insert(bypass, offs, str, atts);
//            noNegativeZero(bypass);
        }
        @Override
        public void replace(FilterBypass bypass, int offs, int len, String str, AttributeSet atts)
                throws BadLocationException {
            bypass.remove(offs, len);
            insert(bypass, offs, str, atts);
//            noNegativeZero(bypass);
        }
        @Override
        public void remove(FilterBypass bypass, int offs, int len)
                throws BadLocationException {
            bypass.remove(offs, len);
//            noNegativeZero(bypass);
        }
        
        private void insert(FilterBypass bypass, int offs, String str, AttributeSet atts)
                throws BadLocationException {
//            eps.app.Log.low(NumberTextField.class, "insert", "str = ", str);
            StringBuilder b = null;
            
            int len  = str.length();
            int prev = 0;
            
            for (int i = 0; i < len;) {
                int code  = str.codePointAt(i);
                int count = Character.charCount(code);
                
                if (!isValid(bypass, code, offs + i)) {
                    if (b == null) {
                        b = new StringBuilder(len);
                    }
                    b.append(str, prev, i);
                    prev = i + count;
                }
                
                i += count;
            }
            
            if (b != null) {
                bypass.insertString(offs, b.append(str, prev, len).toString(), atts);
            } else {
                bypass.insertString(offs, str, atts);
            }
        }
        
        private boolean isValid(FilterBypass bypass, int code, int offs)
                throws BadLocationException {
            if ('0' <= code && code <= '9')
                return true;
            if (code != '-' || offs != 0 || !allowNegative)
                return false;
            Document doc = bypass.getDocument();
            int      len = doc.getLength();
            return (len == 0) || !"-".equals(doc.getText(0, 1));
        }
    }
    
    private final class DocListener implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent e) {
            parseValue();
        }
        @Override
        public void removeUpdate(DocumentEvent e) {
            parseValue();
        }
        @Override
        public void changedUpdate(DocumentEvent e) {
        }
        
        private void parseValue() {
            N prev = value;
            try {
                value = limit(parse(getText()));
            } catch (NumberFormatException x) {
                value = null;
            }
            if (!Objects.equals(prev, value)) {
                for (ChangeListener cl : listeners)
                    cl.stateChanged(event);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    static <N extends Number & Comparable<N>> NumberTextField<N> of(Class<N> type, N min, N max, boolean isClosed) {
        if ((Class<?>) type == Integer.class) {
            return (NumberTextField<N>) new OfInteger((Integer) min, (Integer) max, isClosed);
        }
        throw new IllegalArgumentException(type.toString());
    }
    
    static class OfInteger extends NumberTextField<Integer> {
        OfInteger(Integer min, Integer max, boolean isClosed) {
            super(Integer.class, min, (isClosed || max == null) ? max : Math.subtractExact(max, 1), min < 0);
        }
        OfInteger(boolean allowNegative) {
            super(Integer.class, null, null, allowNegative);
        }
        OfInteger() {
            super(Integer.class, null, null, true);
        }
        
        @Override
        protected boolean isNegative(Integer val) {
            return val < 0;
        }
        
        private static final BigInteger BIG_MIN = BigInteger.valueOf(Integer.MIN_VALUE);
        private static final BigInteger BIG_MAX = BigInteger.valueOf(Integer.MAX_VALUE);
        
        @Override
        protected Integer parse(String text) {
            BigInteger bigVal = new BigInteger(text);
            if (bigVal.compareTo(BIG_MIN) < 0)
                return Integer.MIN_VALUE;
            if (bigVal.compareTo(BIG_MAX) > 0)
                return Integer.MAX_VALUE;
            return bigVal.intValue();
        }
    }
}