/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.util.*;
import static eps.ui.swing.SwingMisc.*;
import static eps.app.App.*;

import java.util.*;
import java.util.function.*;
import javax.swing.*;
import java.awt.event.*;

abstract class SwingCell implements Cell {
    protected final Settings.Listeners settingsListeners;
    
    private final ArraySet<Consumer<? super Cell>> deleteListeners = new ArraySet<>(2);
    
    private final JPopupMenu popup;
    
    private BoundingBox margins;
    
    SwingCell() {
        requireEdt();
        this.settingsListeners = new Settings.Listeners(app());
        
        this.popup = new JPopupMenu();
        
        JMenuItem delete = new JMenuItem("Remove");
        delete.addActionListener(e -> deleteListeners.forEach(listener -> listener.accept(SwingCell.this)));
        popup.add(delete);
        
        SwingUtilities.invokeLater(() -> {
            JComponent comp = getPopupComponent();
            if (comp != null) comp.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    showPopup(e);
                }
                @Override
                public void mouseReleased(MouseEvent e) {
                    showPopup(e);
                }
                private void showPopup(MouseEvent e) {
                    if (e.isPopupTrigger()) {
                        popup.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            });
        });
    }
    
    protected final SwingMainWindow getMainWindow() {
        return (SwingMainWindow) app().getMainWindow();
    }
    
    protected JComponent getPopupComponent() {
        return getComponent();
    }
    
    protected abstract JComponent getComponent();
    
    protected JPopupMenu getPopupMenu() {
        return popup;
    }
    
    @Override
    public BoundingBox getMargins() {
        return margins;
    }
    
    @Override
    public void setMargins(BoundingBox margins) {
        requireEdt();
        this.margins = margins;
        if (margins != null) {
            int north = (int) Math.round(margins.north);
            int west  = (int) Math.round(margins.west);
            int south = (int) Math.round(margins.south);
            int east  = (int) Math.round(margins.east);
            getComponent().setBorder(BorderFactory.createEmptyBorder(north, west, south, east));
        } else {
            getComponent().setBorder(BorderFactory.createEmptyBorder());
        }
    }
    
    @Override
    public void addDeleteListener(Consumer<? super Cell> listener) {
        requireEdt();
        deleteListeners.add(Objects.requireNonNull(listener, "listener"));
    }
    
    @Override
    public void removeDeleteListener(Consumer<? super Cell> listener) {
        requireEdt();
        deleteListeners.remove(Objects.requireNonNull(listener, "listener"));
    }
    
    @Override
    public void close() {
        requireEdt();
        settingsListeners.close();
    }
}