/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.ui.Style;
import eps.cmd.*;
import eps.job.*;
import eps.eval.*;
import static eps.util.Misc.*;
import static eps.app.App.*;

import java.util.*;
import java.util.List;
import java.util.function.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.plaf.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import static java.lang.Character.*;

// TODO:
//  * fix the bug with text containing line breaks before
//    switching to single line mode. the user can perform
//    an 'undo' to bring them back. (this is not a disaster,
//    but it really shouldn't happen.)
//  * find a way to grab the styled text and send it to the
//    ComputationCell when a command is sent. this way the
//    text in the ComputationCell is always styled. Maybe
//    we should always store the last list of tokens from
//    the parser.
final class MultilineEditor extends JTextPane implements AutoCloseable {
    private final Settings.Listeners        listeners;
    private final DocumentSyntaxHighlighter highlighter;
    private final TextEditUndoer            editUndoer;
    private final KeyHandler                keyHandler   = new KeyHandler();
    private final KeyboardFocusManager      focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
    
    private final ScrollPane scrollPane;
    
    private boolean isMultilineMode  = true;
    private boolean hasLineWrap      = false;
    private boolean isParsingEnabled = true;
    
    MultilineEditor() {
        App app = app();
        
        this.listeners = new Settings.Listeners(app);
        
        Settings sets = app.getSettings();
        
        focusManager.addKeyEventDispatcher(keyHandler);
        
        this.setCaretColor(sets.get(Setting.CARET_COLOR));
        this.putClientProperty(JTextPane.HONOR_DISPLAY_PROPERTIES, true);
        
        listeners.add(Setting.BASIC_STYLE,      new BasicStyleListener(this))
                 .add(Setting.BACKGROUND_COLOR, new BackgroundColorListener(this))
                 .add(Setting.CARET_COLOR,      new CaretColorListener(this))
                 .add(Setting.EDITOR_WRAP,      new LineWrapListener(this));
        addAttributeSetCreators(this);
        
        int gap = sets.get(Setting.MAJOR_MARGIN);
        this.setBorder(BorderFactory.createEmptyBorder(gap, gap, gap, gap));
        
        this.highlighter = new DocumentSyntaxHighlighter(this);
        
        Document doc = this.getDocument();
        doc.addDocumentListener(highlighter);
        if (doc instanceof AbstractDocument) {
            ((AbstractDocument) doc).setDocumentFilter(SingleLineFilter.INSTANCE);
        } else {
            Log.note(MultilineEditor.class, "<init>", "doc.getClass() = ", doc.getClass());
        }
        
        this.addPropertyChangeListener(SwingMisc.Store.DOCUMENT, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent e) {
                Document oldDoc = (Document) e.getOldValue();
                Document newDoc = (Document) e.getNewValue();
                Log.note(MultilineEditor.class, "propertyChange",
                         (oldDoc == null) ? "null" : oldDoc.getClass(),
                         " to ",
                         (newDoc == null) ? "null" : newDoc.getClass());
                if (oldDoc != null) {
                    oldDoc.removeDocumentListener(highlighter);
                }
                if (newDoc != null) {
                    newDoc.addDocumentListener(highlighter);
                    if (newDoc instanceof AbstractDocument) {
                        ((AbstractDocument) newDoc).setDocumentFilter(isMultilineMode? null : SingleLineFilter.INSTANCE);
                    } else {
                        Log.note(MultilineEditor.class, "<init>", "newDoc.getClass() = ", newDoc.getClass());
                    }
                }
            }
        });
        
        this.setContentType("text/plain");
        
        this.scrollPane = new ScrollPane();
        this.scrollPane.setViewportView(this);
        
        this.hasLineWrap = app.getSetting(Setting.EDITOR_WRAP);
        
        this.setMultilineMode(this.isMultilineMode);
        this.setStyle(null, null);
        
        this.editUndoer = new TextEditUndoer(this);
    }
    
    @Override
    public void close() {
        focusManager.removeKeyEventDispatcher(keyHandler);
        listeners.close();
    }
    
    JScrollPane getScrollPane() {
        return this.scrollPane;
    }
    
    void setLineWrap(boolean hasLineWrap) {
        if (this.hasLineWrap != hasLineWrap) {
            this.hasLineWrap = hasLineWrap;
            if (this.isMultilineMode) {
                setMultilineMode(true);
            }
        }
    }
    
    void setMultilineMode(boolean isMultilineMode) {
        this.isMultilineMode = isMultilineMode;
        
        StyledDocument doc = this.getStyledDocument();
        if (doc instanceof AbstractDocument) {
            ((AbstractDocument) doc).setDocumentFilter(isMultilineMode ? null : SingleLineFilter.INSTANCE);
        } else {
            Log.note(MultilineEditor.class, "<init>", "doc.getClass() = ", doc.getClass());
        }
        
        if (isMultilineMode) {
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            scrollPane.setHorizontalScrollBarPolicy(hasLineWrap ? JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
                                                                : JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        } else {
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            setText(getText());
        }
        
        this.revalidate();
        this.repaint();
    }
    
    boolean isParsingEnabled() {
        return isParsingEnabled;
    }
    
    void setParsingEnabled(boolean isParsingEnabled) {
        this.isParsingEnabled = isParsingEnabled;
        if (!isParsingEnabled) {
            highlighter.cancel();
        }
    }
    
    void clearEdits() {
        editUndoer.discardAllEdits();
    }
    
    private Dimension getUiSize(BiFunction<ComponentUI, JComponent, Dimension> fn, String name) {
        Dimension dim = fn.apply(this.getUI(), this);
        if (dim == null) {
            Log.note(MultilineEditor.class, "getUiSize", "null ", name, " size");
        }
        return dim;
    }
    
    private Integer getFixedMinimumHeight() {
        Dimension min = this.getUiSize(ComponentUI::getMinimumSize, "minimum");
        return (min == null) ? null : min.height;
    }
    
    private Dimension restrictHeight(Dimension dim) {
        if (!this.isMultilineMode) {
            Integer height = this.getFixedMinimumHeight();
            if (height != null) {
                dim.height = height;
            }
        }
        return dim;
    }
    
    @Override
    public Dimension getPreferredSize() {
        return this.restrictHeight(super.getPreferredSize());
    }
    
    @Override
    public Dimension getMaximumSize() {
        return this.restrictHeight(super.getMaximumSize());
    }
    
    @Override
    public boolean getScrollableTracksViewportWidth() {
        if (!this.isMultilineMode || !this.hasLineWrap) {
            Container parent = this.getParent();
            if (parent instanceof JViewport) {
                Dimension size = this.getUiSize(ComponentUI::getPreferredSize, "preferred");
                if (size != null) {
                    return size.width <= parent.getWidth();
                }
            }
        }
        return true;
    }
    
    private void addIndent() {
        String   indent = this.getIndent();
        if (indent.isEmpty())
            return;
        Document doc    = this.getDocument();
        Caret    caret  = this.getCaret();
        int      dot    = caret.getDot();
        int      mark   = caret.getMark();
        
        try {
            if (dot == mark) {
                doc.insertString(dot, indent, null);
                
            } else {
                int    selStart   = Math.min(dot, mark);
                int    selEnd     = Math.max(dot, mark);
                int    docStart   = doc.getStartPosition().getOffset();
                String text       = doc.getText(docStart, selEnd);
                String ln         = this.getLineSeparator();
                
                int    rangeStart = 0;
                int    rangeEnd   = text.length();
                
                if (rangeBeforeEquals(text, rangeEnd, ln)) {
                    rangeEnd -= ln.length();
                }
                
                for (int i = rangeEnd; i >= rangeStart;) {
                    boolean insert = (i == 0) || rangeBeforeEquals(text, i, ln);
                    
                    if (insert) {
                        doc.insertString(docStart + i, indent, null);
                        if (i <= selStart)
                            break;
                        i -= ln.length();
                    } else {
                        i -= charCount(text.codePointBefore(i));
                    }
                }
            }
        } catch (BadLocationException x) {
            Log.caught(MultilineEditor.class, "while adding indent", x, false);
        }
    }
    
    private void removeIndent() {
        String   indent = this.getIndent();
        if (indent.isEmpty())
            return;
        Document doc    = this.getDocument();
        Caret    caret  = this.getCaret();
        int      dot    = caret.getDot();
        int      mark   = caret.getMark();
        String   ln     = this.getLineSeparator();
        
        try {
            String text       = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset());
            int    length     = text.length();
            int    selStart   = Math.min(dot, mark);
            int    selEnd     = Math.max(dot, mark);
            
            int    rangeStart = selStart;
            int    rangeEnd   = selEnd;
            
            while (rangeEnd < length) {
                int code = text.codePointAt(rangeEnd);
                if (indent.indexOf(code) < 0)
                    break;
                rangeEnd += charCount(code);
            }
            
            if (rangeStart > 0 && rangeBeforeEquals(text, rangeStart, ln)) {
                rangeStart -= ln.length();
            }
            
            while (rangeStart > 0) {
                if (rangeBeforeEquals(text, rangeStart, ln))
                    break;
                int code = text.codePointBefore(rangeStart);
                if (selStart == selEnd && indent.indexOf(code) < 0)
                    break;
                rangeStart -= charCount(code);
            }
            
            int max = rangeEnd;
            
            while (rangeEnd >= rangeStart) {
                if ((rangeEnd == rangeStart) || rangeBeforeEquals(text, rangeEnd, ln)) {
                    for (int i = rangeEnd; i < max;) {
                        if (rangeEquals(text, i, indent)) {
                            doc.remove(i, indent.length());
                            break;
                        } else {
                            i += charCount(text.codePointAt(i));
                        }
                    }
                    rangeEnd = max = (rangeEnd - ln.length());
                } else {
                    rangeEnd -= charCount(text.codePointBefore(rangeEnd));
                }
            }
        } catch (BadLocationException x) {
            Log.caught(MultilineEditor.class, "while adding indent", x, false);
        }
    }
    
    private String getIndent() {
        String indent = app().getSetting(Setting.EDITOR_INDENT);
        int    len    = indent.length();
        if (len >= 2 && indent.codePointAt(0) == '"' && indent.codePointBefore(len) == '"') {
            indent = indent.substring(1, len - 1);
        }
        return indent;
    }
    
    private String getLineSeparator() {
        // see https://docs.oracle.com/javase/8/docs/api/javax/swing/text/DefaultEditorKit.html
        return "\n";
//        return System.getProperty("line.separator");
    }
    
    private final class KeyHandler implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (!MultilineEditor.this.hasFocus()) {
                return false;
            }
            
            int keyCode = e.getKeyCode();
            
            switch (keyCode) {
                case KeyEvent.VK_TAB:
                    if (e.getID() == KeyEvent.KEY_PRESSED) {
                        switch (e.getModifiersEx()) {
                            case 0:
                                MultilineEditor.this.addIndent();
                                break;
                            case KeyEvent.SHIFT_DOWN_MASK:
                                MultilineEditor.this.removeIndent();
                                break;
                            default:
                                return false;
                        }
                    }
                    return true;
            }
            
            return false;
        }
    }
    
    private static final class ScrollPane extends JScrollPane {
        ScrollPane() {
//            super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
//                  JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        }
        
        private Dimension restrict(Dimension dim) {
            Component view = this.getViewport().getView();
            
            if (view instanceof MultilineEditor) {
                MultilineEditor editor = (MultilineEditor) view;
                
                if (!editor.isMultilineMode) {
                    Integer height = editor.getFixedMinimumHeight();
                    
                    if (height != null) {
                        dim.height  = height;
                        Insets ins  = this.getInsets();
                        dim.height += ins.top + ins.bottom;
                    }
                }
            }
            return dim;
        }
        
        @Override
        public Dimension getMinimumSize() {
            return this.restrict(super.getMinimumSize());
        }
        @Override
        public Dimension getPreferredSize() {
            return this.restrict(super.getPreferredSize());
        }
        @Override
        public Dimension getMaximumSize() {
            return this.restrict(super.getMaximumSize());
        }
    }
    
    private void setStyle(Style style, ArgbColor background) {
        Settings settings = app().getSettings();
        
        if (style == null) {
            style = settings.get(Setting.BASIC_STYLE);
        }
        
        ArgbColor foreground = Settings.getAbsolute(style, Style::getForeground);
        String    family     = Settings.getAbsolute(style, Style::getFamily);
        float     size       = Settings.getAbsoluteSize(style);
        int       awtStyle   = (style.isBoldSet()   && style.isBold()   ? Font.BOLD   : 0)
                             | (style.isItalicSet() && style.isItalic() ? Font.ITALIC : 0);
        
        this.setFont(new Font(family, awtStyle, Math.round(size)));
        
        if (background == null) {
            background = settings.get(Setting.BACKGROUND_COLOR);
            if (background == null) {
                background = ArgbColor.WHITE;
            }
        }
        
        ArgbColor opposite = background.getOpposite();
        
        Color fg = SwingMisc.toAwtColor(foreground.equals(background) ? opposite : foreground);
        Color bg = SwingMisc.toAwtColor(background);
        
        this.setForeground(fg);
        this.setBackground(bg);
        this.setSelectionColor(fg);
        this.setSelectedTextColor(bg);
        
        ArgbColor caret = settings.get(Setting.CARET_COLOR);
        if (caret == null) {
            this.setCaretColor(opposite);
        }
        
        // We have to do this because the scroll pane size actually
        // depends on the size of the text pane.
        if (!this.isMultilineMode) {
            if (scrollPane.getViewport().getView() == this) {
                Container c = scrollPane.getParent();
                if (c != null) {
                    c.revalidate();
                    c.repaint();
                }
            } else {
                Log.note(MultilineEditor.class, "setStyle", "unexpected UI state: ", scrollPane);
            }
        }
        
        this.revalidate();
        this.repaint();
    }
    
    private void setCaretColor(ArgbColor color) {
        if (color == null) {
            color = app().getSetting(Setting.BACKGROUND_COLOR).getOpposite();
        }
        this.setCaretColor(SwingMisc.toAwtColor(color));
    }
    
    private static final class BasicStyleListener extends SwingSettingWeakListener<Style, MultilineEditor> {
        private BasicStyleListener(MultilineEditor editor) {
            super(editor);
        }
        @Override
        protected void settingChangedOnEdt(MultilineEditor editor,
                                           Settings        settings,
                                           Setting<Style>  setting,
                                           Style           oldValue,
                                           Style           newValue) {
            editor.setStyle(newValue, null);
        }
    }
    
    private static final class BackgroundColorListener extends SwingSettingWeakListener<ArgbColor, MultilineEditor> {
        private BackgroundColorListener(MultilineEditor editor) {
            super(editor);
        }
        @Override
        protected void settingChangedOnEdt(MultilineEditor    editor,
                                           Settings           settings,
                                           Setting<ArgbColor> setting,
                                           ArgbColor          oldValue,
                                           ArgbColor          newValue) {
            editor.setStyle(null, newValue);
        }
    }
    
    private static final class CaretColorListener extends SwingSettingWeakListener<ArgbColor, MultilineEditor> {
        private CaretColorListener(MultilineEditor editor) {
            super(editor);
        }
        @Override
        protected void settingChangedOnEdt(MultilineEditor    editor,
                                           Settings           settings,
                                           Setting<ArgbColor> setting,
                                           ArgbColor          oldValue,
                                           ArgbColor          newValue) {
            editor.setCaretColor(newValue);
        }
    }
    
    private static final class LineWrapListener extends SwingSettingWeakListener<Boolean, MultilineEditor> {
        private LineWrapListener(MultilineEditor editor) {
            super(editor);
        }
        @Override
        protected void settingChangedOnEdt(MultilineEditor  editor,
                                           Settings         settings,
                                           Setting<Boolean> setting,
                                           Boolean          oldValue,
                                           Boolean          newValue) {
            editor.setLineWrap(newValue);
        }
    }
    
    private static final class SingleLineFilter extends DocumentFilter {
        private static final SingleLineFilter INSTANCE = new SingleLineFilter();
        @Override
        public void insertString(DocumentFilter.FilterBypass fb, int offs, String text, AttributeSet atts) {
            try {
                fb.insertString(offs, this.filterNewLines(text), atts);
            } catch (BadLocationException x) {
                Log.caught(SingleLineFilter.class, "insertString", x, false);
            }
        }
        @Override
        public void replace(DocumentFilter.FilterBypass fb, int offs, int len, String text, AttributeSet atts) {
            try {
                fb.replace(offs, len, this.filterNewLines(text), atts);
            } catch (BadLocationException x) {
                Log.caught(SingleLineFilter.class, "remove", x, false);
            }
        }
        private String filterNewLines(String text) {
            int len = text.length();
            StringBuilder b = null;
            int prev = 0;
            for (int i = 0; i < len;) {
                int code = text.codePointAt(i);
                int next = i + charCount(code);
                switch (code) {
                    case '\r':
                    case '\n':
                        if (code == '\r') {
                            if ((i + 1) < len) {
                                if (text.codePointAt(i + 1) == '\n') {
                                    ++i;
                                }
                            }
                        }
                        if (b == null) {
                            b = new StringBuilder(len);
                        }
                        b.append(text, prev, i);
                        b.append(' ');
                        prev = next;
                        break;
                    default:
                        break;
                }
                i = next;
            }
            return (b == null) ? text : b.append(text, prev, len).toString();
        }
    }
    
    @Override
    public void setText(String text) {
        highlighter.cancel(); // whatever it is is probably invalid now
        super.setText(text);
    }
    
    private static final class DocumentSyntaxHighlighter implements DocumentListener {
        private final MultilineEditor editor;
        
        private Job current;
        private Job next;
        
        private DocumentSyntaxHighlighter(MultilineEditor editor) {
            this.editor = Objects.requireNonNull(editor, "editor");
        }
        
        @Override
        public void changedUpdate(DocumentEvent e) {
        }
        
        @Override
        public void insertUpdate(DocumentEvent e) {
            this.textChanged();
        }
        
        @Override
        public void removeUpdate(DocumentEvent e) {
            this.textChanged();
        }
        
        private void cancel() {
            if (current != null) {
                current.cancel();
            }
            if (next != null) {
                next.cancel(); // not strictly necessary; next should never be enqueued
            }
            current = next = null;
        }
        
        private void textChanged() {
            SwingMisc.requireEdt();
            if (!editor.isParsingEnabled()) {
                return;
            }
            
            String text = editor.getText();
            
            if (text.isEmpty()) {
                editor.getStyledDocument().setCharacterAttributes(0, 0, NO_ATTRIBUTES, true);
                return;
            }
            
            Job job = new SyntaxHighlightJob(editor, text).onDone(this::jobDone);
            
            if (current == null) {
                current = job;
                app().getWorker2().enqueue(job, false);
            } else {
                next = job;
            }
        }
        
        private void jobDone(Job job) {
            SwingMisc.requireEdt();
            if (job == current) {
                current = next;
                
                if (current != null) {
                    next = null;
                    
                    if (!app().getWorker2().enqueue(current, false)) {
                        current = next = null;
                    }
                }
            } else if (job == next) {
                // We shouldn't ever be running the next job; only the current.
                assert false : job;
                next = null;
            }
        }
    }
    
    private AttributeSet commandAtts;
    private AttributeSet numberAtts;
    private AttributeSet functionAtts;
    private AttributeSet literalAtts;
    private AttributeSet bracketAtts;
    
    private static final AttributeSet NO_ATTRIBUTES = new SimpleAttributeSet();
    
    private static final class AttributeSetCreator extends SwingSettingWeakListener<Style, MultilineEditor> {
        private final BiConsumer<MultilineEditor, AttributeSet> setter;
        private AttributeSetCreator(MultilineEditor editor, BiConsumer<MultilineEditor, AttributeSet> setter) {
            super(editor);
            this.setter = Objects.requireNonNull(setter, "setter");
        }
        @Override
        protected void settingChangedOnEdt(MultilineEditor editor,
                                           Settings        settings,
                                           Setting<Style>  setting,
                                           Style           oldValue,
                                           Style           newValue) {
            setter.accept(editor, newValue == null ? NO_ATTRIBUTES : SwingMisc.toAttributeSet(newValue));
        }
    }
    
    private static void addAttributeSetCreators(MultilineEditor editor) {
        editor.listeners
              .addAndSet(Setting.COMMAND_STYLE,  new AttributeSetCreator(editor, (e, s) -> e.commandAtts  = s))
              .addAndSet(Setting.NUMBER_STYLE,   new AttributeSetCreator(editor, (e, s) -> e.numberAtts   = s))
              .addAndSet(Setting.FUNCTION_STYLE, new AttributeSetCreator(editor, (e, s) -> e.functionAtts = s))
              .addAndSet(Setting.LITERAL_STYLE,  new AttributeSetCreator(editor, (e, s) -> e.literalAtts  = s))
              .addAndSet(Setting.BRACKET_STYLE,  new AttributeSetCreator(editor, (e, s) -> e.bracketAtts  = s));
    }
    
    private static final class SyntaxHighlightJob extends AbstractJob {
        private final MultilineEditor editor;
        private final String          text;
        
        private List<Token> tokens;
        private Command     command;
        private String      commandText;
        
        private SyntaxHighlightJob(MultilineEditor editor, String text) {
            this.editor = Objects.requireNonNull(editor, "editor");
            this.text   = Objects.requireNonNull(text,   "text"); // editor.getText();
        }
        
        @Override
        protected void executeImpl() {
            String text = this.text;
            
            if ((command = Command.getCommand(text)) != null) {
                int end = command.getCommandEnd(text);
                if (end == text.length()) {
                    commandText = text;
                    return;
                }
                commandText = text.substring(0, end);
                text        = text.substring(end);
                if (text.trim().isEmpty()) {
                    return;
                }
            }
            
            Context context = new Context(text, app());
            try {
                tokens = Parser.tokenize(context);
            } catch (EvaluationException x) {
                Log.caught(SyntaxHighlightJob.class, "execute", x, true);
            }
        }
        
        @Override
        protected void doneImpl() {
            StyledDocument doc     = editor.getStyledDocument();
            String         current = editor.getText();
            
            if (command != null) {
                if (rangeEquals(current, 0, commandText)) {
                    doc.setCharacterAttributes(0, commandText.length(), editor.commandAtts, true);
                }
            }
            
            int offset = (command == null) ? 0 : commandText.length();
            if (offset < current.length()) {
                doc.setCharacterAttributes(offset, current.length(), NO_ATTRIBUTES, true);
            }
            
            if (tokens == null || tokens.isEmpty()) {
                return;
            }
            
            AttributeSet numberAtts   = editor.numberAtts;
            AttributeSet functionAtts = editor.functionAtts;
            AttributeSet literalAtts  = editor.literalAtts;
            AttributeSet bracketAtts  = editor.bracketAtts;
            
            for (Token token : tokens) {
                Substring src = token.getSource();
                if (!src.subEquals(current, offset)) {
                    continue;
                }
                
                AttributeSet atts = null;
                
                switch (token.getTokenKind()) {
                case SYMBOL:
                    Symbol sym = (Symbol) token.getValue();
                    
                    switch (sym.getSymbolKind()) {
                    case VARIABLE:
                        atts = numberAtts;
                        break;
                        
                    case UNARY_OP:
                        if (((UnaryOp) sym).isFunctionLike()) {
                            atts = functionAtts;
                        }
                        break;
                        
                    case SPAN_OP:
                        SpanOp span = (SpanOp) sym;
                        
                        if (span.isParenthesis()) {
                            atts = bracketAtts;
                            
                        } else if (span.isLiteral()) {
                            atts = literalAtts;
                        }
                        
                        break;
                    }
                    break;
                    
                case STRING_LITERAL:
                    atts = literalAtts;
                    break;
                    
                case NUMBER:
                    atts = numberAtts;
                    break;
                }
                
                if (atts != null) {
                    // Note: we don't use src.length() here because in cases
                    //       using the boundary joiner _ the length is wrong.
                    //       The source e.g. +_+ will be parsed to the token ++
                    //       which has a length of 2, but the difference between
                    //       the end and start source positions is 3.
                    int length = src.getSourceEnd() - src.getSourceStart();
                    doc.setCharacterAttributes(offset + src.getSourceStart(), length, atts, true);
                }
            }
        }
    }
}