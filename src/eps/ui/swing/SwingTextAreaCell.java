/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.ui.Style;
import eps.block.*;
import static eps.ui.swing.SwingMisc.*;
import static eps.app.App.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.util.*;

class SwingTextAreaCell extends AbstractSwingTextCell implements TextAreaCell {
    private final Area area;
    
    private String text  = "";
    private Block  block = null;
    
    SwingTextAreaCell(String text) {
        area = new Area();
        
        area.setEditable(false);
        area.setOpaque(false);
        area.setForeground(Color.BLACK);
        area.setAlignmentX(SwingConstants.LEFT);
        area.setBorder(BorderFactory.createEmptyBorder());
        
        area.addFocusListener(new OneAreaFocusListener(this));
        
        area.setCaret(new NoUpdateCaret());
        area.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
        
        JPopupMenu popup = getPopupMenu();
        popup.addSeparator();
        
        JMenuItem copy = new JMenuItem("Copy");
        popup.add(copy);
        
        copy.addActionListener((ActionEvent e) -> {
            String sel = area.getSelectedText();
            if (text != null) {
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(sel), null);
            }
        });
        
        JMenuItem selall = new JMenuItem("Select All");
        popup.add(selall);
        
        selall.addActionListener((ActionEvent e) -> {
            area.requestFocus();
            area.selectAll();
        });
        
        this.setText(text);
        
        settingsListeners.add(Setting.BACKGROUND_COLOR, new BackgroundColorListener(this));
        
        this.refreshing(Setting.NUMBER_STYLE)
            .refreshing(Setting.FUNCTION_STYLE)
            .refreshing(Setting.LITERAL_STYLE)
            .refreshing(Setting.BRACKET_STYLE)
            .refreshing(Setting.BASIC_STYLE);
        
        this.afterTextComponentInitialization();
        this.followTextStyle(Setting.BASIC_STYLE);
    }
    
    protected <T> SwingTextAreaCell refreshing(Setting<T> set) {
        settingsListeners.add(set, new BlockStyleRefresher<>(this));
        return this;
    }
    
    @Override
    public void close() {
        super.close();
    }
    
    @Override
    protected JTextComponent getTextComponent() {
        return area;
    }
    
    @Override
    protected JComponent getComponent() {
        requireEdt();
        return this.getTextComponent();
    }
    
    @Override
    public void setTextColor(ArgbColor color) {
        requireEdt();
        super.setTextColor(color);
        area.setSelectionColor(toAwtColor(color));
        area.setSelectedTextColor(toAwtColor(app().getSetting(Setting.BACKGROUND_COLOR)));
        app().itemsChanged();
    }
    
    @Override
    public TextAreaCell followTextStyle(Setting<Style> setting) {
        super.followTextStyle(setting);
        return this;
    }
    
    @Override
    public Block getBlock() {
        requireEdt();
        return this.block;
    }
    
    @Override
    public String getText() {
        requireEdt();
        String text = this.text;
        if (text == null)
            text = super.getText();
        return text;
    }
    
    @Override
    public void setText(String text) {
        requireEdt();
//        area.setContentType("text/plain");
//        super.setText(text);
        // All of this bullshit is because I discovered that the plain text
        // editor is not rendering bold text for certain fonts (such as Monaco),
        // but the HTML editor does. It could be that this is not a problem with
        // the editor per se (could be the font itself has no bold...?), but the
        // HTML editor seems to be able to do it.
        this.text  = text;
        this.block = null;
        if (text == null) {
            area.setText("");
            app().itemsChanged();
            return;
        }
//        area.setContentType("text/html");
        
        Style.Builder style = fromAwtStyle(area.getForeground(), area.getFont());
        
        StringBuilder b = new StringBuilder(64 + text.length());
        b.append("<html><body><pre style=\"");
        style.toCssString(b);
        b.append("\">");
        
        Block.escapeHtml(b, text);
        
        b.append("</pre></body></html>");
        area.setText(b.toString());
        app().itemsChanged();
        
//        this.setToolTipText(text);
    }
    
    @Override
    public void setTextStyle(Setting<Style> setting) {
        requireEdt();
        super.setTextStyle(setting);
        // reformat the "plain text" to HTML
        if (this.text != null) {
            this.setText(this.text);
        }
        area.revalidate();
        area.repaint();
        app().itemsChanged();
    }
    
    /*
    @Override
    public void setTextStyle(Style style) {
        requireEdt();
        super.setTextStyle(style);
        // reformat the "plain text" to HTML
        if (this.text != null) {
            this.setText(this.text);
        }
        area.revalidate();
        area.repaint();
        app().itemsChanged();
    }
    */
    
    protected eps.eval.Context getBlockContext() {
        return null;
    }
    
    @Override
    public void setText(Block block) {
        requireEdt();
        this.text  = null;
        this.block = block;
        if (block == null) {
            area.setText("");
            return;
        }
//        area.setContentType("text/html");
        
        StringBuilder b = new StringBuilder(64);
        b.append("<html><body><pre style =\"");
        fromAwtStyle(area.getForeground(), area.getFont()).toCssString(b);
        b.append("\">");
        block.toHtml(b, this.getBlockContext());
        b.append("</pre></body></html>");
        
        area.setText(b.toString());
        app().itemsChanged();
    }
    
    private static final class OneAreaFocusListener extends FocusAdapter {
        private final java.lang.ref.WeakReference<SwingTextAreaCell> cell;
        
        private OneAreaFocusListener(SwingTextAreaCell cell) {
            this.cell = new java.lang.ref.WeakReference<>(Objects.requireNonNull(cell, "cell"));
        }
        
        @Override
        public void focusGained(FocusEvent e) {
        }
        
        @Override
        public void focusLost(FocusEvent e) {
            Component lost   = e.getComponent();
            Component gained = e.getOppositeComponent();
            
            SwingTextAreaCell cell = this.cell.get();
            if (cell == null) {
                Log.note(OneAreaFocusListener.class, "focusLost", "cell garbage collected");
                lost.removeFocusListener(this);
                return;
            }
            
            Area area = cell.area;
            
            if (lost != area) {
                lost.removeFocusListener(this);
            }
            
            if (gained != area) {
                if (gained instanceof Area) {
                    area.setSelectionStart(0);
                    area.setSelectionEnd(0);
                } else if (gained != null) {
                    gained.addFocusListener(this);
                }
            }
        }
    }
    
    private static final class BlockStyleRefresher<T> extends SwingSettingWeakListener<T, SwingTextAreaCell> {
        private BlockStyleRefresher(SwingTextAreaCell cell) {
            super(cell);
        }
        @Override
        protected void settingChangedOnEdt(SwingTextAreaCell cell,
                                           Settings   settings,
                                           Setting<T> setting,
                                           T          oldValue,
                                           T          newValue) {
            if (cell.block != null) {
                cell.setText(cell.block);
            }
        }
    }
    
    private static final class Area extends JTextPane {
        private Area() {
            this.setContentType("text/html");
            this.setEditorKit(new HTMLEditorKit() {
                private final ViewFactory viewFactory = new HTMLFactory() {
                    @Override
                    public View create(Element elem) {
                        AttributeSet attrs = elem.getAttributes();
                        Object elementName = attrs.getAttribute(AbstractDocument.ElementNameAttribute);
                        Object   styleName = (elementName != null) ? null : attrs.getAttribute(StyleConstants.NameAttribute);
                        if (styleName instanceof HTML.Tag) {
                            HTML.Tag kind = (HTML.Tag) styleName;
                            if (kind == HTML.Tag.IMPLIED) {
                                if ("pre".equals(attrs.getAttribute(CSS.Attribute.WHITE_SPACE))) {
                                    return new javax.swing.text.html.ParagraphView(elem) {
                                        // TODO: For some reason this doesn't work to fix the line wrapping.
//                                        @Override
//                                        public float getMinimumSpan(int axis) {
//                                            return (axis == View.X_AXIS) ? 0 : super.getMinimumSpan(axis);
//                                        }
                                    };
                                }
                            }
//                            if (kind == HTML.Tag.CONTENT) {
////                                return new InlineView(elem) {
//////                                    @Override
//////                                    public float getMinimumSpan(int axis) {
//////                                        return (axis == View.X_AXIS) ? 0 : super.getMinimumSpan(axis);
//////                                    }
////                                };
//                            }
                        }
                        return super.create(elem);
                    }
                };
                @Override
                public ViewFactory getViewFactory() {
                    return this.viewFactory;
                }
            });
            // We don't really need this if we are really rendering
            // all text as HTML.
            this.putClientProperty(Area.HONOR_DISPLAY_PROPERTIES, true);
            
            this.setAlignmentX(Component.LEFT_ALIGNMENT);
            this.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        }
        
        @Override
        public void updateUI() {
            super.updateUI();
            this.putClientProperty(Area.HONOR_DISPLAY_PROPERTIES, true);
        }
        
        @Override
        public Dimension getMaximumSize() {
            Dimension max = super.getMaximumSize();
            max.height = this.getPreferredSize().height;
            return max;
        }
    }
    
    private static final class BackgroundColorListener extends SwingSettingWeakListener<ArgbColor, SwingTextAreaCell> {
        private BackgroundColorListener(SwingTextAreaCell cell) {
            super(cell);
        }
        @Override
        protected void settingChangedOnEdt(SwingTextAreaCell  cell,
                                           Settings           settings,
                                           Setting<ArgbColor> setting,
                                           ArgbColor          oldValue,
                                           ArgbColor          newValue) {
            cell.setTextColor(cell.getTextColor());
        }
    }
}