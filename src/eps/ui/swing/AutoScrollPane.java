/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import javax.swing.*;
import java.awt.event.*;

final class AutoScrollPane extends JScrollPane {
    AutoScrollPane(JComponent comp, int vstyle, int hstyle) {
        super(comp, vstyle, hstyle);
        new AutoScroller().init(this.getVerticalScrollBar());
    }
    
    private final class AutoScroller implements AdjustmentListener {
        private int     prevValue = -1;
        private int     prevVis   = -1;
        private int     prevMax   = -1;
        private boolean isInitial = true;
        
        private void set(int val, int vis, int max) {
            this.prevValue = val;
            this.prevVis   = vis;
            this.prevMax   = max;
        }
        
        private void init(JScrollBar bar) {
            if (!init0(bar)) {
                addHierarchyListener(new HierarchyListener() {
                    @Override
                    public void hierarchyChanged(HierarchyEvent e) {
                        boolean displayabilityChanged =
                            (e.getChangeFlags() & HierarchyEvent.DISPLAYABILITY_CHANGED) == HierarchyEvent.DISPLAYABILITY_CHANGED;
                        if (displayabilityChanged) {
                            if (init0(bar)) {
                                removeHierarchyListener(this);
                            }
                        }
                    }
                });
            }
        }
        
        private boolean init0(JScrollBar bar) {
            if (isDisplayable()) {
                bar.addAdjustmentListener(this);
                
                int max = bar.getMaximum();
                int vis = bar.getVisibleAmount();
                this.set(max - vis, vis, max);
                this.isInitial = false;
                
                return true;
            } else {
                return false;
            }
        }
        
        @Override
        public void adjustmentValueChanged(AdjustmentEvent e) {
            if (!this.isInitial) {
                JScrollBar bar = (JScrollBar) e.getAdjustable();
                int        val = bar.getValue();
                int        vis = bar.getVisibleAmount();
                int        max = bar.getMaximum();
                
                boolean isAtMaximum  = (val + vis) == max;
                boolean wasAtMaximum = Math.abs(this.prevValue + this.prevVis - this.prevMax) <= 4;
                
                if (this.prevMax != max || this.prevVis != vis) {
                    if (!isAtMaximum) {
                        if (wasAtMaximum) {
                            val = max - vis;
                            
                            this.set(val, vis, max);
                            bar.setValue(val);
                            return;
                        }
                    }
//                    else {
//                        if (!wasAtMaximum) {
//                            val = Math.min(this.prevValue, max - vis);
//                            
//                            this.set(val, vis, max);
//                            bar.setValue(val);
//                        }
//                    }
                }
                this.set(val, vis, max);
            }
        }
    }
}