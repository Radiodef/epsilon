/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import java.awt.*;
import javax.swing.*;

final class Swatch extends JComponent {
    private final Insets insets = new Insets(0, 0, 0, 0);
    
    Swatch() {
        this(Color.WHITE);
    }
    
    Swatch(Color foreground) {
        setBorder(BorderFactory.createLoweredBevelBorder());
        setOpaque(true);
        setForeground(foreground);
    }
    
    @Override
    public void setForeground(Color foreground) {
        super.setForeground(foreground);
        String hex = SwingMisc.fromAwtColor(foreground).toHexString(true);
        setToolTipText(hex);
    }
    
    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
    
    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
    
    @Override
    public Dimension getPreferredSize() {
        int    size   = getFontMetrics(getFont()).getMaxAscent();
        Insets insets = getInsets(this.insets);
        int    width  = size + insets.left + insets.right;
        int    height = size + insets.top  + insets.bottom;
        return new Dimension(width, height);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(getForeground());
        Insets insets = getInsets(this.insets);
        int    x      = insets.left;
        int    y      = insets.top;
        int    width  = getWidth()  - x - insets.right;
        int    height = getHeight() - y - insets.bottom;
        g.fillRect(x, y, width, height);
    }
}