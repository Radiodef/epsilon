/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.eval.*;
import eps.app.*;
import eps.job.*;
import eps.util.*;
import eps.json.*;
import eps.io.*;
import eps.io.FileSystem;
import static eps.app.App.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.InvalidPathException;

final class Wizards {
    private Wizards() {
    }
    
    static class SymbolsWindowWizard extends Wizard {
        private final SwingSymbolsWindow window;
        
        SymbolsWindowWizard(SwingSymbolsWindow window) {
            super(window.getDialog());
            
            this.window = window;
        }
        
        SwingSymbolsWindow getSymbolsWindow() {
            return window;
        }
    }
    
    private static SwingSymbolsWindow getWindow(Wizard.Step step) {
        if (step.isWizardSet()) {
            Wizard wiz = step.getWizard();
            if (wiz instanceof SymbolsWindowWizard) {
                return ((SymbolsWindowWizard) wiz).getSymbolsWindow();
            }
        }
        return null;
    }
    
    private static boolean showCaughtErrorMessage(Wizard.Step step) {
        JOptionPane.showMessageDialog(step.getDialog(),
                                      "The application must be restarted first.",
                                      "Application In Error State",
                                      JOptionPane.ERROR_MESSAGE);
        return false;
    }
    
    private static final int FIELD_COLS = 50;
    
    static final Symbols DEFAULT_SYMBOLS = Symbols.Concurrent.ofDefaults();
    
    private static final List<Symbol> INTRINSICS =
        DEFAULT_SYMBOLS.stream()
                       .filter(Symbol::isIntrinsic)
                       .sorted(SwingSymbolsWindow.SYMBOL_COMPARATOR)
                       .collect(Collectors.toList());
    private static final List<Symbol> ALIASES =
        DEFAULT_SYMBOLS.stream()
                       .filter(Symbol::isAlias)
                       .sorted(SwingSymbolsWindow.SYMBOL_COMPARATOR)
                       .collect(Collectors.toList());
    
    static final class RestoreDefaultsWizard extends SymbolsWindowWizard {
        RestoreDefaultsWizard(SwingSymbolsWindow window) {
            super(window);
            
            addStep(new ChooseRestoreModeStep());
            addStep(new ChooseRestoreSymbolsStep());
            addStep(new ChooseBackupStep());
        }
    }
    
    static final class ChooseRestoreModeStep extends Wizard.BoxDescriptionStep {
        static final String TITLE = "Choose Restore Mode";
        
        static final String RESTORE_EVERYTHING = "Restore Everything";
        static final String RESTORE_SELECT     = "Restore Select Symbols";
        
        ChooseRestoreModeStep() {
            super(TITLE);
            
            addOption(RESTORE_SELECT);
            addOption(RESTORE_EVERYTHING);
        }
        
        @Override
        protected void onFirstShow() {
            super.onFirstShow();
            setSelectedOption(RESTORE_SELECT);
        }
        
        @Override
        protected void stepShown(String cmd) {
            super.stepShown(cmd);
            setNextEnabled(true);
        }
        
        @Override
        protected String getDescription(String option) {
            switch (option) {
            case RESTORE_EVERYTHING:
                return "Restores all symbols back to the default state, as they "
                     + "are when Epsilon is opened for the very first time. This operation "
                     + "will automatically delete all user-defined symbols and restore "
                     + "any default aliases which are missing."
                     + "\n\n"
                     + "Note: once the wizard is completed by pressing the '" + Wizard.DONE + "' "
                     + "button on the next page, this action can not be undone."
                     + "\n\n"
                     + "The next page will provide an option to create a backup of "
                     + "the current symbols file before restoring.";
            case RESTORE_SELECT:
                return "Select specific default symbols to restore from a list. "
                     + "Symbols which are restored will automatically overwrite any "
                     + "existing symbols with the same name and type."
                     + "\n\n"
                     + "The last page will provide an option to create a backup of "
                     + "the current symbols file before restoring.";
            default:
                return "";
            }
        }
        
        @Override
        protected boolean canAdvance() {
            return super.canAdvance() && getSelectedOption() != null;
        }
        
        @Override
        public String getNextStep() {
            String sel = getSelectedOption();
            if (sel != null) {
                switch (sel) {
                case RESTORE_EVERYTHING:
                    return ChooseBackupStep.TITLE;
                case RESTORE_SELECT:
                    return ChooseRestoreSymbolsStep.TITLE;
                }
            }
            throw newIllegalStateException(sel);
        }
        
        @Override
        public boolean isFirstStep() {
            return true;
        }
        
        @Override
        public boolean isLastStep() {
            return false;
        }
    }
    
    static final class ChooseRestoreSymbolsStep extends Wizard.Step {
        static final String TITLE = "Choose Default Symbols To Restore";
        
        private final JList<Symbol> intrinsics;
        private final JList<Symbol> aliases;
        
        private final DefaultListModel<Symbol> aliasModel;
        
        ChooseRestoreSymbolsStep() {
            super(TITLE);
            
            intrinsics = createList(INTRINSICS.toArray(new Symbol[0]));
            aliases    = createList(new Symbol[0]);
            
            aliasModel = new DefaultListModel<>();
            aliases.setModel(aliasModel);
            
            intrinsics.addListSelectionListener(e -> {
                List<Symbol> intrinsicsSel = intrinsics.getSelectedValuesList();
                List<Symbol> aliasesSel    = aliases.getSelectedValuesList();
                
                aliasModel.removeAllElements();
                for (Symbol a : ALIASES) {
                    if (intrinsicsSel.contains(a.getDelegate())) {
                        aliasModel.addElement(a);
                    }
                }
                
                SwingMisc.selectAll(aliases, aliasesSel);
            });
            
            int gap = minorMargin;
            JPanel listSplit = new JPanel(new GridLayout(1, 2, gap, gap));
            
            listSplit.add(createListPane(intrinsics, "Select Intrinsic Symbols To Filter By"));
            listSplit.add(createListPane(aliases,    "Select Default Aliases To Restore"));
            
            setLayout(new BorderLayout());
            add(listSplit, BorderLayout.CENTER);
        }
        
        private JList<Symbol> createList(Symbol[] syms) {
            JList<Symbol> list = new JList<>(syms);
            list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            list.setCellRenderer(new SwingSymbolsWindow.SymbolCellRenderer());
            return list;
        }
        
        private JPanel createListPane(JList<?> list, String title) {
            JScrollPane scroll =
                new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                      JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            JPanel panel = new JPanel(new BorderLayout());
            int m = minorMargin;
            Border border;
            border = BorderFactory.createEtchedBorder();
            border = BorderFactory.createTitledBorder(border, title);
            border = BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(m, m, m, m));
            panel.setBorder(border);
            panel.add(scroll, BorderLayout.CENTER);
            return panel;
        }
        
        @Override
        protected void onFirstShow() {
            super.onFirstShow();
            SwingMisc.selectAll(intrinsics);
        }
        
        @Override
        protected void stepShown(String cmd) {
            super.stepShown(cmd);
            setNextEnabled(true);
        }
        
        @Override
        public boolean isFirstStep() {
            return false;
        }
        
        @Override
        public boolean isLastStep() {
            return false;
        }
        
        @Override
        public String getNextStep() {
            return ChooseBackupStep.TITLE;
        }
        
        @Override
        public List<Symbol> getValue() {
            return aliases.getSelectedValuesList();
        }
    }
    
    static final class Backup {
        final String mode;
        final Path   path;
        
        private Backup(String mode, Path path) {
            this.mode = Objects.requireNonNull(mode, "mode");
            this.path = path;
        }
        
        @Override
        public int hashCode() {
            return 31 * Objects.hashCode(mode) + Objects.hashCode(path);
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Backup) {
                Backup that = (Backup) obj;
                return Objects.equals(this.mode, that.mode)
                    && Objects.equals(this.path, that.path);
            }
            return false;
        }
        
        private static final ToString<Backup> TO_STRING =
            ToString.builder(Backup.class)
                    .add("mode", b -> b.mode)
                    .add("path", b -> b.path == null ? null : b.path.toAbsolutePath())
                    .build();
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
    }
    
    static final class ChooseBackupStep extends Wizard.Step {
        static final String TITLE = "Choose Backup";
        
        static final String BACKUP    = "Backup Current Symbols";
        static final String NO_BACKUP = "No Backup";
        
        static final String DEFAULT_BACKUP_NAME = "symbols_backup";
        
        private final JRadioButton       backup;
        private final JRadioButton       none;
        private final List<JRadioButton> buttons;
        private final JTextField         name;
        private final JTextField         status;
        private final JButton            dest;
        
        private Path destPath;
        
        ChooseBackupStep() {
            super(TITLE);
            
            int min = minorMargin;
            
            JPanel buttonsPanel = new JPanel();
            buttonsPanel.setBorder(createBorder(null));
            buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
            
            backup  = createButton(BACKUP);
            none    = createButton(NO_BACKUP);
            buttons = ListOf(backup, none);
            
            ButtonGroup group = new ButtonGroup();
            for (JRadioButton btn : buttons) {
                group.add(btn);
                buttonsPanel.add(btn);
                btn.addItemListener(e -> buttonSelectionChanged());
            }
            
            SwingMisc.space(buttonsPanel, min);
            
            JPanel optionsPanel = new JPanel();
            optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));
            
            name = new BoxTextField(FIELD_COLS);
            name.setText(DEFAULT_BACKUP_NAME);
            DocumentPropertyChangeListener.add(name, (SimpleDocumentListener) e -> nameChanged());
                
            status = SwingMisc.setAsIfDisabled(new BoxTextField(FIELD_COLS));
                
            dest = new JButton("Open Destination Directory");
            dest.addActionListener(e -> destinationClicked());
            
            JPanel destPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            destPanel.add(dest);
            
            JPanel namePanel = SwingMisc.createTitledPanel("File Name", null, min);
            namePanel.setLayout(new BoxLayout(namePanel, BoxLayout.Y_AXIS));
            namePanel.add(name);
            namePanel.add(status);
            
            forEach(destPanel, namePanel, p -> p.setAlignmentX(Component.LEFT_ALIGNMENT));
            
            optionsPanel.add(namePanel);
            optionsPanel.add(destPanel);
            SwingMisc.space(optionsPanel, majorMargin);
                
            setLayout(new GridLayout(2, 1, 0, min));
            add(buttonsPanel);
            add(optionsPanel);
                
            backup.setSelected(true);
                
            setToolTips();
        }
        
        private void setToolTips() {
            backup.setToolTipText(
                  "<html><body style='width:500'>"
                + "Select this option to create a copy of the symbols file as "
                + "it currently exists on disk. To create a backup with more comprehensive "
                + "options, use the <i>Export</i> tool in the symbols editor."
                + "</body></html>");
            none.setToolTipText("Select this option to perform no backup.");
            name.setToolTipText(
                  "<html><body style='width:500'>"
                + "Set the name of the file to save a backup to."
                + "<br/><br/>"
                + "If the specified file name already exists, the backup tool "
                + "will change it to be unique. The backup tool will not overwrite "
                + "an existing file."
                + "</body></html>");
            dest.setToolTipText(
                  "<html><body style='width:500'>"
                + "Displays the full path of the file which will be created "
                + "for the backup, or an error message if there's an error."
                + "</body></html>");
        }
        
        private Border createBorder(String title) {
            int m = majorMargin;
            Border b = BorderFactory.createCompoundBorder(
                BorderFactory.createEtchedBorder(),
                BorderFactory.createEmptyBorder(m, m, m, m));
            if (title != null)
                b = BorderFactory.createTitledBorder(b, title);
            return b;
        }
        
        private JRadioButton createButton(String title) {
            JRadioButton btn = new JRadioButton(title);
            btn.setActionCommand(title);
            btn.setAlignmentX(Component.LEFT_ALIGNMENT);
            return btn;
        }
        
        private void updateViewAndControls() {
            updateViewAndControls(true);
        }
        
        private void updateViewAndControls(boolean doBeep) {
            updateViewAndControls(doBeep, getSelectedButton() == backup);
        }
        
        private void updateViewAndControls(boolean doBeep, boolean optionsEnabled) {
            name.setEnabled(optionsEnabled);
            name.getParent().setEnabled(optionsEnabled);
            dest.setEnabled(optionsEnabled);
            nameChanged(doBeep);
            setNextEnabled(isNextEnabled() && getSelectedButton() != null);
        }
        
        private void buttonSelectionChanged() {
            updateViewAndControls();
        }
        
        @Override
        protected void stepShown(String cmd) {
            super.stepShown(cmd);
            updateViewAndControls(false);
        }
            
        private void nameChanged() {
            nameChanged(true);
        }
        
        private void nameChanged(boolean doBeep) {
            boolean enableNext = true;
            
            destPath = null;
            
            createPath:
            if (name.isEnabled()) {
                destPath = getBackupFilePath();
                if (destPath == null) {
                    if (doBeep) {
                        SwingMisc.beep();
                    }
                } else {
                    status.setText(destPath.toAbsolutePath().toString());
                }
            }
            
            setNextEnabled(enableNext);
        }
        
        private Path getBackupFilePath() {
            Path dir = PersistentFiles.MISC_DIRECTORY.getPath();
            if (dir == null) {
                status.setText("Error retrieving destination directory. "
                             + "The application log may have additional details.");
                return null;
            }
            String text = PersistentObjectFile.fromObjectFile(name.getText());
            if (text.isEmpty()) {
                text = DEFAULT_BACKUP_NAME;
            }
            Suffixer sfxr = new Suffixer(text, 1_000);
            for (;;) {
                if (!sfxr.hasNext()) {
                    status.setText("There are too many files in the destination directory, "
                                 + "or there was a problem checking if the backup file already exists.");
                    break;
                }
                String fileName = PersistentObjectFile.toObjectFile(sfxr.next());
                try {
                    Path path = dir.resolve(fileName);
                    if (FileSystem.instance().notExists(path)) {
                        return path;
                    }
                } catch (InvalidPathException x) {
                    Log.caught(getClass(), "nameChanged", x, true);
                    status.setText("Error creating backup path: \"" + x.getMessage() + "\". "
                                 + "The application log may have additional details.");
                    break;
                }
            }
            return null;
        }
        
        private void destinationClicked() {
            Path dir = PersistentFiles.MISC_DIRECTORY.getPath();
            if (dir != null) {
                if (app().getComponentFactory().openDirectory(dir)) {
                    return;
                }
            }
            showErrorMessage("File System Error",
                             "<html>"
                           + "There was an error opening the destination directory."
                           + "<br/><br/>"
                           + "The application log may have additional details."
                           + "</html>");
        }
        
//        @Override
//        protected void onFirstShow() {
//            super.onFirstShow();
//            updateViewAndControls();
//        }
        
        @Override
        public boolean isFirstStep() {
            return false;
        }
        
        @Override
        public boolean isLastStep() {
            return true;
        }
        
        private JRadioButton getSelectedButton() {
            for (JRadioButton btn : buttons)
                if (btn.isSelected())
                    return btn;
            return null;
        }
        
        @Override
        public void setValue(Object value) {
            for (JRadioButton btn : buttons) {
                if (Objects.equals(btn.getActionCommand(), value)) {
                    btn.setSelected(true);
                    break;
                }
            }
        }
        
        @Override
        public Backup getValue() {
            updateViewAndControls(false);
            JRadioButton button = getSelectedButton();
            String       backup = (button == null) ? NO_BACKUP : button.getActionCommand();
            return new Backup(backup, destPath);
        }
        
        @Override
        protected boolean canAdvance() {
            if (super.canAdvance()) {
                JRadioButton button = getSelectedButton();
                if (button != null) {
                    if (button == backup) {
                        if (!backupSymbols(destPath)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
        }
        
        private boolean backupSymbols(Path path) {
            if (path == null) {
                return showBackupErrorMessage();
            }
            if (Log.caughtError()) {
                return showCaughtErrorMessage(this);
            }
            
            SymbolsCopyJob job = new SymbolsCopyJob(path);
            app().getWorker2().enqueue(job, true);
            
            String title   = "Backing Up";
            String message = "Backing up symbols file...";
            SwingMisc.await(getDialog(), title, message, () -> !job.isDone());
            
            String result = job.getMessage();
            if (result != null) {
                return showBackupErrorMessage(result);
            }
            
            return true;
        }
        
        static final class SymbolsCopyJob extends AbstractJob {
            private final Path destination;
            
            private volatile String message;
            
            SymbolsCopyJob(Path destination) {
                this.destination = Objects.requireNonNull(destination, "destination");
            }
            
            String getMessage() {
                return isDone() ? message : null;
            }
            
            @Override
            protected void executeImpl() {
                Path symsPath = PersistentFiles.SYMBOLS_FILE.getPath();
                if (symsPath == null) {
                    message = "There was an error in a past attempt to read or write the symbols file.";
                    return;
                }
                if (!FileSystem.instance().notExists(destination)) {
                    message = "Specified destination file may already exist.";
                    return;
                }
                try {
                    FileSystem.instance().copy(symsPath, destination);
                } catch (IOException x) {
                    Log.caught(getClass(), "while copying symbols file", x, false);
                    message = "Copy failed: \"" + Misc.createSimpleMessage(x) + "\". "
                            + "The application log may have addition details.";
                }
            }
        }
        
        private boolean showBackupErrorMessage() {
            return showBackupErrorMessage(null);
        }
        
        private boolean showBackupErrorMessage(String message) {
            String file = PersistentFiles.SYMBOLS_FILE.getActualFileName();
            String msg = "<html><body style='width:500'>"
                       + ((message == null) ? "There was an error creating the backup file. "
                                            : (
                                                  "There was an error creating the backup file:"
                                                + "<br/><br/>"
                                                + "<i>" + Misc.escapeHtml(message) + "</i>"
                                                + "<br/><br/>" )
                        )
                       + "The restore operation has been aborted. A backup can be made manually by navigating to the "
                       + "application folder and copying the symbols file"
                       + (file == null ? "." : (", <i>" + Misc.escapeHtml(file) + "</i>."))
                       + "<br/><br/>"
                       + "The <i>Export</i> tool in the symbols editor may also work, depending "
                       + "on what the problem was."
                       + "<br/><br/>"
                       + "The application log may have additional details."
                       + "</body></html>";
            showErrorMessage("Backup Error", msg);
            return false;
        }
    }
    
    static final class ImportSymbolsWizard extends SymbolsWindowWizard {
        ImportSymbolsWizard(SwingSymbolsWindow window) {
            super(window);
            
            addStep(new ChooseImportFileStep());
            addStep(new ChooseImportSymbolsStep());
        }
    }
    
    static final class ChooseImportFileStep extends Wizard.Step {
        static final String TITLE = "Choose Symbols File To Import From";
        
        private final JButton browse;
        private final JTextField pathField;
        private final JTextField statField;
        
        private Path path;
        private Job job;
        private Symbols syms;
        
        ChooseImportFileStep() {
            super(TITLE);
            
            browse = new JButton("Browse");
            
            pathField = new BoxTextField(FIELD_COLS);
            statField = SwingMisc.setAsIfDisabled(new BoxTextField("No file specified.", FIELD_COLS));
            
            JPanel pathPane = SwingMisc.createTitledPanel("Choose File", null, minorMargin);
            pathPane.setLayout(new BoxLayout(pathPane, BoxLayout.Y_AXIS));
                
            forEach(browse, pathField, statField, c -> {
                c.setAlignmentX(Component.LEFT_ALIGNMENT);
                pathPane.add(c);
            });
                
//            SwingMisc.space(pathPane, gap);
              
            setLayout(new BorderLayout());
            add(pathPane, BorderLayout.NORTH);
                
            browse.addActionListener(e -> browseClicked());
            DocumentPropertyChangeListener.add(pathField, (SimpleDocumentListener) e -> pathChanged());
            
            setToolTips();
        }
        
        private void setToolTips() {
            browse.setToolTipText("Opens a tool for choosing a symbols file to import.");
            pathField.setToolTipText(
                  "<html><body style='width:500'>"
                + "Displays the path to the chosen file. A file path may be specified directly "
                + "by editing the text field directly."
                + "</body></html>");
            statField.setToolTipText("Displays information about the status of reading the file.");
        }
        
        private void browseClicked() {
            Path path = this.path;
            if (path == null) {
                path = PersistentFiles.MISC_DIRECTORY.getPath();
            }
            
            path = SwingMisc.chooseFile(getDialog(),
                                        "Choose Symbols File",
                                        JFileChooser.OPEN_DIALOG,
                                        path,
                                        p -> FileSystem.instance().isDirectory(p)
                                          || PersistentObjectFile.isObjectFile(p),
                                        "Epsilon Object Files");
            if (path != null) {
                pathField.setText(path.toAbsolutePath().toString());
            }
        }
        
        private void pathChanged() {
            String text = pathField.getText();
            try {
                Path path = text.isEmpty() ? null : Paths.get(text);
                setPath(path);
            } catch (InvalidPathException x) {
                Log.caught(getClass(), "pathChanged", x, true);
                statField.setText("Specified path is invalid.");
            }
        }
        
        private void setPath(Path path) {
            this.path = path;
            read();
        }
        
        private void read() {
            setNextEnabled(false);
            this.syms = null;
            Path path = this.path;
            if (path == null) {
                statField.setText("No file specified.");
                return;
            }
            if (job != null) {
                job.cancel();
            }
            statField.setText("Reading the specified file.");
            job = new ReadSymbolsJob(path);
            app().getWorker2().enqueue(job, true);
        }
        
        private final class ReadSymbolsJob extends AbstractJob {
            private final    Path    path;
            private volatile Symbols result;
            private volatile String  message;
            
            private ReadSymbolsJob(Path path) {
                this.path = Objects.requireNonNull(path, "path");
            }
            
            @Override
            protected void executeImpl() {
                try {
                    try {
                        Object obj = eps.json.Json.fromFile(path);
                        if (obj instanceof Symbols) {
                            result  = (Symbols) obj;
                            message = "Symbols file read successfully.";
                        } else {
                            Log.note(getClass(), "executeImpl",
                                     "path = \"", path.toAbsolutePath(),
                                     "\"; contents = ", (obj == null) ? null : obj.getClass(), ": ", obj);
                            message = "Not a symbols file.";
                        }
                    } catch (IOException x) {
                        Log.caught(getClass(), "executeImpl", x, true);
                        message = "Error reading the specified file. "
                                + "The application log may have additional details.";
                    } catch (RuntimeException x) {
                        Log.caught(getClass(), "executeImpl", x, true);
                        message = "The file did not contain a valid object. "
                                + "The application log may have additional details.";
                    }
                } catch (Throwable x) {
                    message = Misc.createSimpleMessage(x);
                    throw x;
                }
            }
            
            @Override
            protected void doneImpl() {
                if (!isCancelled()) {
                    if (result != null) {
                        syms = result;
                    }
                    setNextEnabled(result != null);
                    statField.setText(message);
                }
                if (job == this) {
                    job = null;
                }
            }
        }
        
        @Override
        public String getNextStep() {
            if (syms == null) {
                throw newIllegalStateException(syms);
            }
            return ChooseImportSymbolsStep.TITLE;
        }
        
//        @Override
//        protected void onFirstShow() {
//            super.onFirstShow();
//            setNextEnabled(false);
//        }
        
        @Override
        protected void stepShown(String cmd) {
            super.stepShown(cmd);
            boolean nextEnabled = job == null && path != null && syms != null;
//            Log.note(getClass(), "stepShown",
//                     "nextEnabled = ", nextEnabled,
//                     ", job = ",       job,
//                     ", path = ",      path,
//                     ", syms = ",      syms);
            setNextEnabled(nextEnabled);
        }
        
        @Override
        public boolean isFirstStep() {
            return true;
        }
        
        @Override
        public boolean isLastStep() {
            return false;
        }
        
        @Override
        public Symbols getValue() {
            return syms;
        }
    }
    
    // TODO: use a JSplitPane?
    static abstract class ChooseSymbolsStep extends Wizard.Step implements ListSelectionListener, ChangeListener {
        protected final JList<Symbol> list;
        protected final DefaultListModel<Symbol> model;
        
        protected final SymbolEditor editor;
        
        protected Symbols symbols = Symbols.empty();
        
        ChooseSymbolsStep(String title, String selectTitle, String editTitle) {
            super(title);
            
            list = new JList<Symbol>() {
                @Override
                public Dimension getPreferredScrollableViewportSize() {
                    Dimension pref = super.getPreferredScrollableViewportSize();
                    Component comp =
                        getCellRenderer().getListCellRendererComponent(this,
                                                                       UnaryOp.SINE,
                                                                       0,
                                                                       false,
                                                                       false);
                    pref.width  = comp.getPreferredSize().width;
                    Insets  ins = getInsets();
                    pref.width += ins.left + ins.right;
                    return pref;
                }
            };
            
            model = new DefaultListModel<>();
            
            list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            list.setCellRenderer(new SwingSymbolsWindow.SymbolCellRenderer());
            list.setModel(model);
            
            editor = new SymbolEditor() {
                @Override
                protected boolean isCompact() {
                    return true;
                }
                @Override
                protected Iterable<Symbol> getSymbols() {
                    return () -> {
                        Iterator<Symbol> iterA = Misc.iteratorOf(model.elements());
                        Iterator<Symbol> iterB = symbols.stream().filter(Symbol::isIntrinsic).iterator();
                        return Misc.cat(iterA, iterB);
                    };
                }
            };
            
            int m = minorMargin;
            setLayout(new BorderLayout(m, m));
            
            JScrollPane scroll =
                new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                      JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            
            add(SwingMisc.createTitledPanel(selectTitle, scroll, m), BorderLayout.WEST);
            add(SwingMisc.createTitledPanel(editTitle, editor, m), BorderLayout.CENTER);
            
            list.addListSelectionListener(this);
            editor.addChangeListener(this);
        }
        
        private int lastSelectionIndex;
        
        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                int lastSelectionIndex = list.getSelectionModel().getLeadSelectionIndex();
                if (lastSelectionIndex != this.lastSelectionIndex) {
                    listSelectionChanged(lastSelectionIndex);
                }
            }
        }
        
        protected void listSelectionChanged(int lastSelectionIndex) {
            this.lastSelectionIndex = lastSelectionIndex;
            if (lastSelectionIndex >= 0) {
                editor.setSymbol(model.getElementAt(lastSelectionIndex));
            } else {
                editor.setSymbol(null);
            }
        }
        
        @Override
        public void stateChanged(ChangeEvent e) {
            Symbol after = editor.getSymbol();
            int    last  = lastSelectionIndex;
            
            boolean doSet = after != null && last >= 0;
            assert doSet : last + ": " + after;
            
            if (doSet) {
                Comparator<Symbol> comparator = SwingSymbolsWindow.SYMBOL_COMPARATOR;
                
                if (comparator.compare(model.get(last), after) == 0) {
                    model.set(last, after);
                    
                } else {
                    list.removeListSelectionListener(this);
                    try {
                        list.removeSelectionInterval(last, last);
                        model.remove(last);
                        
                        int size = model.getSize();
                        for (int i = 0; i <= size; ++i) {
                            if ((i < size) && (comparator.compare(model.get(i), after) < 0)) {
                                continue;
                            }
                            model.add(i, after);
                            list.addSelectionInterval(i, i);
                            list.ensureIndexIsVisible(i);
                            lastSelectionIndex = i;
                            break;
                        }
                    } finally {
                        list.addListSelectionListener(this);
                    }
                }
            }
        }
        
        @Override
        protected void stepShown(String cmd) {
            super.stepShown(cmd);
            setNextEnabled(true);
            
            if (!Wizard.NEXT.equals(cmd)) {
                return;
            }
            
            Symbols syms = loadSymbols();
            if (syms == null) {
                syms = Symbols.empty();
            }
            
//            if (syms == this.symbols) {
//                return;
//            }
            
            this.symbols = syms;
            
            list.removeListSelectionListener(this);
            try {
                model.removeAllElements();
                syms.stream()
                    .filter(not(Symbol::isIntrinsic))
                    .map(sym -> sym.isSpanOp() ? ((SpanOp) sym).getParent() : sym)
                    .distinct()
                    .sorted(SwingSymbolsWindow.SYMBOL_COMPARATOR)
                    .forEach(model::addElement);
                list.clearSelection();
            } finally {
                list.addListSelectionListener(this);
            }
            
            listSelectionChanged(-1);
        }
        
        protected abstract Symbols loadSymbols();
        
        @Override
        public List<Symbol> getValue() {
            return list.getSelectedValuesList();
        }
    }
    
    static final class ChooseImportSymbolsStep extends ChooseSymbolsStep {
        static final String TITLE = "Choose Symbols To Import";
        
        ChooseImportSymbolsStep() {
            super(TITLE, "Select Symbols To Import", "Edit Symbols Before Importing");
        }
        
        @Override
        protected Symbols loadSymbols() {
            return (Symbols) getWizard().getCurrentResults().get(ChooseImportFileStep.TITLE);
        }
        
        @Override
        protected boolean canAdvance() {
            if (!super.canAdvance()) {
                return false;
            }
            
            SwingSymbolsWindow window = getWindow(this);
            if (window != null) {
                List<Symbol> dupes = new ArrayList<>();
                
                for (Symbol sym : list.getSelectedValuesList()) {
                    for (Symbol exist : window.getSymbols()) {
                        if (Symbols.isOverwriteEqual(sym, exist)) {
                            dupes.add(sym);
                            break;
                        }
                    }
                }
                
                if (!dupes.isEmpty()) {
                    String msg = "<html><body style='width:500'>"
                               + "The following imported symbols will overwrite existing symbols:"
                               + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;"
                               + j(dupes, " ", sym -> Misc.escapeHtml(sym.getName()))
                               + "<br/><br/>"
                               + "Do you want to continue?"
                               + "</body></html>";
                    int choice =
                        JOptionPane.showConfirmDialog(getWizard().getDialog(),
                                                      msg,
                                                      "Confirm Overwrites",
                                                      JOptionPane.YES_NO_OPTION,
                                                      JOptionPane.QUESTION_MESSAGE);
                    if (choice != JOptionPane.YES_OPTION) {
                        return false;
                    }
                }
            }
            
            return true;
        }
        
        @Override
        public boolean isFirstStep() {
            return false;
        }
        
        @Override
        public boolean isLastStep() {
            return true;
        }
    }
    
    static final class ExportSymbolsWizard extends SymbolsWindowWizard {
        ExportSymbolsWizard(SwingSymbolsWindow window) {
            super(window);
            
            addStep(new ChooseExportSymbolsStep());
            addStep(new ChooseExportFileStep());
        }
    }
    
    static final class ChooseExportSymbolsStep extends ChooseSymbolsStep {
        static final String TITLE = "Choose Symbols To Export";
        
        ChooseExportSymbolsStep() {
            super(TITLE, "Select Symbols To Export", "Edit Symbols (Temporarily) Before Exporting");
        }
        
        @Override
        protected Symbols loadSymbols() {
            Symbols syms = Symbols.Concurrent.empty();
            
            SwingSymbolsWindow window = getWindow(this);
            if (window != null) {
                window.getSymbols().forEach(syms::put);
            }
            
            return syms;
        }
        
        @Override
        public boolean isFirstStep() {
            return true;
        }
        
        @Override
        public boolean isLastStep() {
            return false;
        }
    }
    
    static class ChooseExportFileStep extends Wizard.Step {
        static final String TITLE = "Choose File To Export To";
        
        private final JButton browse;
        
        private final JTextField pathField;
        private final JTextField statField;
        
        private final JButton openDir;
        
        private Path    path    = null;
        private boolean isValid = false;
        
        ChooseExportFileStep() {
            super(TITLE);
            
            browse = new JButton("Browse");
            
            pathField = new BoxTextField(FIELD_COLS);
            statField = SwingMisc.setAsIfDisabled(new BoxTextField("No file specified.", FIELD_COLS));
            
            JPanel pathPane = SwingMisc.createTitledPanel("Choose File", null, minorMargin);
            pathPane.setLayout(new BoxLayout(pathPane, BoxLayout.Y_AXIS));
            
            forEach(browse, pathField, statField, comp -> {
                comp.setAlignmentX(Component.LEFT_ALIGNMENT);
                pathPane.add(comp);
            });
            
            openDir = new JButton("Open Destination Directory");
            openDir.setEnabled(false);
            
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            
            forEach(pathPane, openDir, comp -> {
                comp.setAlignmentX(Component.LEFT_ALIGNMENT);
                add(comp);
            });
            
            SwingMisc.space(this, majorMargin);
            
            browse.addActionListener(e -> browseClicked());
            openDir.addActionListener(e -> openDirectoryClicked());
            DocumentPropertyChangeListener.add(pathField, (SimpleDocumentListener) e -> pathChanged());
            
            setToolTips();
        }
        
        private void setToolTips() {
            browse.setToolTipText("Opens a tool for choosing a file to export to.");
            pathField.setToolTipText(
                  "<html><body style='width:500'>"
                + "Displays the path to the chosen file. A file path may be specified directly "
                + "by editing the text field directly."
                + "</body></html>");
            statField.setToolTipText("Displays information about the status of the specified file.");
        }
        
        private void browseClicked() {
            Path path = this.path;
            if (path == null) {
                path = PersistentFiles.MISC_DIRECTORY.getPath();
            }
            
            path = SwingMisc.chooseFile(getDialog(),
                                        "Choose File",
                                        JFileChooser.SAVE_DIALOG,
                                        path,
                                        p -> FileSystem.instance().isDirectory(p)
                                          || PersistentObjectFile.isObjectFile(p),
                                        "Epsilon Object Files");
            if (path != null) {
                pathField.setText(PersistentObjectFile.toObjectFile(path.toAbsolutePath().toString()));
            }
        }
        
        private void openDirectoryClicked() {
            if (path == null)
                return;
            app().getComponentFactory().openDirectory(path);
        }
        
        private void pathChanged() {
            this.path    = null;
            this.isValid = false;
            
            String text = pathField.getText();
            if (text.isEmpty()) {
                statField.setText("No file specified.");
            } else {
                try {
                    Path path = Paths.get(text);
                    if (FileSystem.instance().isDirectory(path)) {
                        statField.setText("Specified path is a directory.");
                    } else {
                        this.path    = path;
                        this.isValid = true;
                        String  name = path.getFileName().toString();
                        if (FileSystem.instance().exists(path)) {
                            statField.setText("Specified file \"" + name + "\" will be overwritten.");
                        } else {
                            statField.setText("Symbols will be exported to \"" + name + "\".");
                        }
                    }
                } catch (InvalidPathException x) {
                    Log.caught(getClass(), "pathChanged", x, true);
                    statField.setText("Specified path is invalid.");
                }
            }
            
            setNextEnabled(isValid);
            openDir.setEnabled(isValid);
        }
        
        @Override
        protected void stepShown(String cmd) {
            super.stepShown(cmd);
            setNextEnabled(path != null && isValid);
        }
        
        @Override
        protected boolean canAdvance() {
            if (super.canAdvance()) {
                if (write()) {
                    return true;
                }
            }
            return false;
        }
        
        private boolean write() {
            if (Log.caughtError()) {
                return showCaughtErrorMessage(this);
            }
            Path path = this.path;
            if (path == null) {
                return false;
            }
            if (FileSystem.instance().isDirectory(path)) {
                return false;
            }
            if (FileSystem.instance().exists(path)) {
                String msg = "<html>"
                           + "The specified file will be overwritten."
                           + "<br/><br/>"
                           + "Do you want to continue?"
                           + "</html>";
                int choice = JOptionPane.showConfirmDialog(getDialog(),
                                                           msg,
                                                           "Confirm Overwrite",
                                                           JOptionPane.YES_NO_OPTION,
                                                           JOptionPane.QUESTION_MESSAGE);
                if (choice != JOptionPane.YES_OPTION) {
                    return false;
                }
            }
            
            ExportJob job = new ExportJob(path);
            app().getWorker2().enqueue(job, true);
            
            SwingMisc.await(getDialog(), "Writing", "Writing to file...", () -> !job.isDone);
            
            if (job.message != null) {
                String msg = "<html><body style='width:500'>"
                           + "There was an error while writing the symbols file:"
                           + "<br/><br/>"
                           + "<i>" + Misc.escapeHtml(job.message) + "</i>"
                           + "<br/><br/>"
                           + "The application log may have additional details."
                           + "</body></html>";
                JOptionPane.showMessageDialog(getDialog(),
                                              msg,
                                              "Write Error",
                                              JOptionPane.ERROR_MESSAGE);
                return false;
            } else {
                JOptionPane.showMessageDialog(getDialog(),
                                              "Export completed successfully.",
                                              "Export Complete",
                                              JOptionPane.INFORMATION_MESSAGE);
                return true;
            }
        }
        
        private final class ExportJob extends AbstractJob {
            private final Path    path;
            private final Symbols syms;
            
            private volatile boolean isDone  = false;
            private volatile String  message = null;
            
            private ExportJob(Path path) {
                this.path = Objects.requireNonNull(path, "path");
                
                Symbols syms = Symbols.Concurrent.empty();
                
                Map<String, Object> results = getWizard().getCurrentResults();
                if (results != null) {
                    List<?> list = (List<?>) results.get(ChooseExportSymbolsStep.TITLE);
                    if (list != null) {
                        for (Object obj : list) {
                            Symbol sym = (Symbol) obj;
                            syms.put(sym);
                            if (sym.isAlias()) {
                                syms.putIfAbsent(sym.getDelegate());
                            }
                        }
                    }
                }
                
                this.syms = syms;
            }
            
            @Override
            protected void executeImpl() {
                try {
                    byte[] bytes;
                    try {
                        bytes = Json.toBytes(syms, true);
                    } catch (RuntimeException x) {
                        Log.caught(getClass(), "executeImpl", x, false);
                        message = "Error during serialization.";
                        return;
                    }
                    try {
                        FileSystem.instance().write(path, bytes);
                    } catch (IOException | RuntimeException x) {
                        Log.caught(getClass(), "executeImpl", x, true);
                        message = x.getMessage();
                    }
                } catch (Throwable x) {
                    message = Misc.createSimpleMessage(x);
                    throw x;
                }
            }
            
            @Override
            protected void doneImpl() {
                isDone = true;
            }
        }
        
        @Override
        public boolean isFirstStep() {
            return false;
        }
        
        @Override
        public boolean isLastStep() {
            return true;
        }
    }
}