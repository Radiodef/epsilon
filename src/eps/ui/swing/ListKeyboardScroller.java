/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
import java.util.*;

abstract class ListKeyboardScroller implements KeyEventDispatcher, ActionListener, AutoCloseable {
    public static final int DEFAULT_TIMEOUT = 500;
    
    protected final JList<?> list;
    
    private final KeyboardFocusManager keyboardFocusManager;
    
    private final Timer timer;
    
    private String str = "";
    
    protected ListKeyboardScroller(JList<?> list) {
        this(list, DEFAULT_TIMEOUT);
    }
    
    protected ListKeyboardScroller(JList<?> list, int timeout) {
        this.list = Objects.requireNonNull(list, "list");
        
        keyboardFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        keyboardFocusManager.addKeyEventDispatcher(this);
        
        timer = new Timer(timeout, this);
        timer.setRepeats(false);
        timer.setCoalesce(false);
    }
    
    @Override
    public void close() {
        timer.stop();
        keyboardFocusManager.removeKeyEventDispatcher(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        str = "";
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        Component comp = e.getComponent();
            
        if (list.isAncestorOf(comp)) {
            if (e.getID() == KeyEvent.KEY_TYPED) {
                if (timer.isRunning()) {
                    timer.restart();
                } else {
                    timer.start();
                }
                
                str += e.getKeyChar();
                eps.app.Log.note(ListKeyboardScroller.class, "dispatchKeyEvent", "str = \"", str, "\"");
                
                Object val = stringToValue(str);
                
                if (val != null) {
                    list.setSelectedValue(val, true);
                }
            }
            return true;
        }
        
        return false;
    }
    
    protected abstract Object stringToValue(String str);
}