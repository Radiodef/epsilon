/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.eval.*;
import eps.util.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.util.*;
import java.util.List;
import java.util.function.*;

final class ParameterEditor extends JComponent {
    private final JToolBar tools;
    
    private final JButton add;
    private final JButton del;
    private final JButton tog;
    private final JButton rename;
    private final JButton up;
    private final JButton down;
    
    private final List<JButton> buttons;
    
    private final JTree tree;
    private final CustomModel model;
    
    private Operator op;
    
    private final TreeModelListener treeModelListener;
    
    private ChangeListener[] listeners = new ChangeListener[0];
    private final ChangeEvent changeEvent = new ChangeEvent(this);
    private boolean suppress = false;
    
    ParameterEditor() {
        this(5);
    }
    
    ParameterEditor(int initialCount) {
        setLayout(new BorderLayout());
        
        tools = new JToolBar(JToolBar.HORIZONTAL);
        tools.setFloatable(false);
        
        add    = new JButton("Add");
        del    = new JButton("Remove");
        tog    = new JButton("Toggle");
        rename = new JButton("Rename");
        up     = new JButton("↑");
        down   = new JButton("↓");
        
        tools.add(add);
        tools.add(del);
        tools.addSeparator();
        tools.add(tog);
        tools.add(rename);
        tools.addSeparator();
        tools.add(up);
        tools.add(down);
        
        buttons = ListOf(add, del, tog, rename, up, down);
        
        add.addActionListener(e -> add());
        del.addActionListener(e -> remove());
        tog.addActionListener(e -> toggle());
        rename.addActionListener(e -> rename());
        up.addActionListener(e -> up());
        down.addActionListener(e -> down());
        
        tree = new JTree() {
            @Override
            public Dimension getPreferredSize() {
                return super.getPreferredSize();
            }
            @Override
            public boolean getScrollableTracksViewportHeight() {
                return super.getScrollableTracksViewportHeight();
            }
        };
        
        model = new CustomModel();
        
        tree.setModel(model);
        tree.setCellRenderer(new ParameterCellRenderer());
        tree.setCellEditor(new ParameterCellEditor(tree));
        tree.setRootVisible(false);
        tree.setShowsRootHandles(true);
        tree.setDragEnabled(false);
        tree.setEditable(true);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.putClientProperty("JTree.lineStyle", "Angled");
        
        model.setRoot(Param.createDefault(initialCount));
        
        treeModelListener = new TreeModelListener() {
            @Override
            public void treeNodesInserted(TreeModelEvent e) {
                treeChanged(e);
            }
            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
                treeChanged(e);
            }
            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                treeChanged(e);
            }
            @Override
            public void treeStructureChanged(TreeModelEvent e) {
                treeChanged(e);
            }
            private void treeChanged(TreeModelEvent e) {
                if (e instanceof CustomModelEvent && !((CustomModelEvent) e).isLast)
                    return;
                fireStateChanged();
            }
        };
        
        model.addTreeModelListener(treeModelListener);
        
        tree.addTreeSelectionListener(this::selectionChanged);

        JScrollPane scroll =
            new JScrollPane(tree, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                  JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED) {
            JScrollPane setPreferredSize() {
                Dimension pref = getPreferredSize();
                Insets    ins  = getInsets();
                pref.height    = getViewport().getView().getPreferredSize().height;
                pref.height   += ins.top + ins.bottom;
                setPreferredSize(pref);
                return this;
            }
        }.setPreferredSize();
        
        add(tools, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);
        
        setToolTips();
    }
    
    private void setToolTips() {
        add.setToolTipText("Adds a new parameter to the currently-selected group.");
        del.setToolTipText("Deletes the currently-selected parameter.");
        tog.setToolTipText(
              "<html><body style='width:500'>"
            + "If the currently-selected parameter is a group, changes it to a "
            + "variable. Otherwise, if the currently-selected parameter is a "
            + "variable, changes it to a group."
            + "<br/><br/>"
            + "Parameter groups expect the argument passed to it to be a tuple, "
            + "and allow the elements of that tuple to be referenced by name. For "
            + "example, this allows the definition of an operator which adds vectors "
            + "together, like (1,2)plus(3,4)."
            + "</body></html>");
        rename.setToolTipText(
              "<html><body style='width:500'>"
            + "Starts editing the name of the currently-selected variable parameter. "
            + "Editing may be committed either by pressing this button again or by "
            + "pressing the 'Enter' key. Editing may be cancelled by starting editing "
            + "a different name or by pressing the 'Escape' key."
            + "<br/><br/>"
            + "Variable names may also be edited by double-clicking them in the tree view."
            + "<br/><br/>"
            + "Variable names must be unique."
            + "</body></html>");
        up.setToolTipText("Moves the currently-selected parameter to the position before it.");
        down.setToolTipText("Moves the currently-selected parameter to the position after it.");
    }
    
    private void setButtonsEnabled(boolean e) {
        for (JButton btn : buttons)
            btn.setEnabled(e);
    }
    
    private void up() {
        move(-1);
    }
    
    private void down() {
        move(+1);
    }
    
    private void move(int distance) {
        doChange(() -> moveImpl(distance));
    }
    
    private boolean moveImpl(int distance) {
        TreePath sel = tree.getSelectionPath();
        if (sel != null) {
            Param param = (Param) sel.getLastPathComponent();
            if (model.move(param, distance)) {
                validateParametersForOperand();
                select(param);
                return true;
            }
        }
        return false;
    }
    
    private void doChange(BooleanSupplier task) {
        suppress = true;
        boolean didChange;
        try {
            didChange = task.getAsBoolean();
        } finally {
            suppress = false;
        }
        if (didChange) {
            enableTools();
            fireStateChanged();
        }
    }
    
    private void add() {
        doChange(this::addImpl);
    }
    
    private boolean addImpl() {
        Object input =
            JOptionPane.showInputDialog(this,
                                        "Would you like to add a variable or a group?",
                                        "Choose Type",
                                        JOptionPane.QUESTION_MESSAGE,
                                        null,
                                        new Object[] { "Variable", "Group" },
                                        "Variable");
        if (input == null) {
            return false;
        }
        
        Param addition = new ParamVar(model.getUniqueName());
        if ("Group".equals(input)) {
            addition = new ParamGroup(null, addition);
        }
        
        TreePath sel = tree.getSelectionPath();
        if (sel != null) {
            int    index = Integer.MAX_VALUE;
            Param last   = (Param) sel.getLastPathComponent();
            if (last instanceof ParamVar) {
                ParamGroup parent = last.parent;
                index = parent.getIndex(last) + 1;
                last  = parent;
            }
            if (!(last instanceof ParamGroup)) {
                return false;
            }
            ParamGroup group = (ParamGroup) last;
            model.insert(group, index, addition);
        } else {
            model.append(addition);
        }
        
        expand(addition);
        validateParametersForOperand();
        return true;
    }
    
    private void remove() {
        doChange(this::removeImpl);
    }
    
    private boolean removeImpl() {
        TreePath sel = tree.getSelectionPath();
        if (sel != null) {
            if (model.remove((Param) sel.getLastPathComponent())) {
                validateParametersForOperand();
                return true;
            }
        }
        return false;
    }
    
    private boolean validateParametersForOperand() {
        boolean corrected = false;
        
        if (op != null) {
            ParamGroup root  = model.getRoot();
            int        count = root.count();
            
            switch (op.getSymbolKind()) {
                case UNARY_OP:
                    if (count == 0) {
                        model.append(Param.createDefault(op));
                        corrected = true;
                    }
                    break;
                case SPAN_OP:
                case NARY_OP:
                    if (count != 1) {
                        assert false : root;
                        if (count == 0) {
                            model.append(Param.createDefault(op));
                        } else {
                            while ((count = root.count()) > 1) {
                                model.remove(root.get(count - 1));
                            }
                        }
                        corrected = true;
                    }
                    break;
                case BINARY_OP:
                    if (count < 2) {
                        if (count == 0) {
                            model.setRoot((ParamGroup) Param.createDefault(op));
                        } else {
                            String name = model.getUniqueName("rhs");
                            model.append(new ParamVar(name));
                        }
                        corrected = true;
                    } else {
                        while ((count = root.count()) > 2) {
                            model.remove(root.get(count - 1));
                        }
                        corrected = true;
                    }
                    break;
            }
            
            List<ParamGroup> empty = new ArrayList<>(0);
            root.forEachGroup(group -> {
                if (group.count() == 0)
                    empty.add(group);
            });
            if (!empty.isEmpty()) {
                for (ParamGroup group : empty) {
                    String name = model.getUniqueName();
                    model.append(group, new ParamVar(name));
                }
                corrected = true;
            }
        }
        
        if (corrected) {
            SwingMisc.beep();
        }
        
        return corrected;
    }
    
    private void toggle() {
        doChange(this::toggleImpl);
    }
    
    private boolean toggleImpl() {
        Param sel = (Param) tree.getLastSelectedPathComponent();
        
        if (sel != null) {
            List<TreePath> expanded = getExpandedPaths();
            
            if ((sel = model.toggle(sel)) != null) {
                select(sel);
                expanded.forEach(tree::expandPath);
                return true;
            }
        }
        
        return false;
    }
    
    private void rename() {
        TreePath sel = tree.getSelectionPath();
        if (sel != null) {
            Param last = (Param) sel.getLastPathComponent();
            if (last instanceof ParamVar) {
                if (tree.isEditing()) {
                    tree.stopEditing();
                } else {
                    tree.startEditingAtPath(sel);
                }
            }
        }
    }
    
    private void selectionChanged(TreeSelectionEvent e) {
        enableTools();
    }
    
    private void enableTools() {
        boolean allowsStructuralChanges = op != null && (op.isUnaryOp() || op.isBinaryOp() || op.isSpanOp());
        if (!allowsStructuralChanges) {
            setButtonsEnabled(false);
            return;
        }
        
        TreePath path = tree.getSelectionPath();
        
        if (path != null) {
            tog.setEnabled(true);
            
            up.setEnabled(true);
            down.setEnabled(true);
            
            ParamGroup root = model.getRoot();
            Param      sel = (Param) path.getLastPathComponent();
            
            rename.setEnabled(sel instanceof ParamVar);
            
            if (root.contains(sel)) {
                add.setEnabled(op.isUnaryOp() || sel instanceof ParamGroup);
                del.setEnabled(op.isUnaryOp() && root.count() > 1);
            } else {
                add.setEnabled(true);
                del.setEnabled(sel.getParent().count() > 1);
            }
            
        } else {
            tog.setEnabled(false);
            del.setEnabled(false);
            rename.setEnabled(false);
            
            up.setEnabled(false);
            down.setEnabled(false);
            
            add.setEnabled(op.isUnaryOp());
        }
    }
    
    private boolean isValid(ParamGroup root) {
        if (root != null) {
            if (root.getParent() != null) {
                return false;
            }
            return isValid(root, Misc.newIdentityHashSet(), new HashSet<>());
        }
        return true;
    }
    
    private boolean isValid(Param param, Set<Param> seen, Set<String> names) {
        if (param != null && seen.add(param)) {
            if (param instanceof ParamVar) {
                return names.add(((ParamVar) param).name);
            }
            if (param instanceof ParamGroup) {
                ParamGroup group = (ParamGroup) param;
                Param[]    array = group.group;
                if (array != null) {
                    for (Param p : array) {
                        if (!isValid(p, seen, names)) {
                            return false;
                        }
                        if (p.getParent() != group) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    ParamGroup getParameters() {
        ParamGroup root = model.getRoot();
        assert isValid(root) : Param.toDebugString(root);
        if (root != null) {
            root = root.clone();
        }
        return root;
    }
    
    TreeAdapter<Param, String> getParametersAsTree() {
        ParamGroup root = getParameters();
        if (op != null && op.isUnaryOp()) {
            root = new ParamGroup(null, root);
        }
        return new TreeAdapter<Param, String>(root) {
            @Override
            protected boolean allowsValue(Param p) {
                return p instanceof ParamVar;
            }
            @Override
            protected boolean allowsChildren(Param p) {
                return p instanceof ParamGroup;
            }
            @Override
            protected String getValue(Param p) {
                return ((ParamVar) p).name;
            }
            @Override
            protected int getChildCount(Param p) {
                return ((ParamGroup) p).count();
            }
            @Override
            protected Param getChildAt(Param p, int i) {
                return ((ParamGroup) p).get(i);
            }
            @Override
            protected Node<Param, String> createNode(Param p) {
                return new Node<Param, String>(this, p) {
                    @Override
                    protected void toString(StringBuilder b) {
                        getObject().toString(b);
                    }
                };
            }
        };
    }
    
    boolean setParameters(Operator op) {
        model.removeTreeModelListener(treeModelListener);
        try {
            boolean result = setParametersImpl(op);
            enableTools();
            return result;
        } finally {
            model.addTreeModelListener(treeModelListener);
        }
    }
    
    private boolean setParametersImpl(Operator op) {
        this.op = op;
        
        if (op == null) {
            setButtonsEnabled(false);
            model.setRoot((ParamGroup) null);
            return false;
        }
        
        try {
            setParametersAsTree(op);
            return false;
        } catch (ClassCastException | IllegalArgumentException x) {
            Log.caught(getClass(), "setParameters", x, true);
            
            Param params = Param.createDefault(op);
            
            if (params == null) {
                return setParametersImpl(null);
            } else {
                setParameters(params);
                return true;
            }
        }
    }
    
    private void setParametersAsTree(Operator op) {
        try {
            TreeFunction fn = Trees.getTreeFunction(op);
            setParameters(op, fn);
        } catch (ClassCastException | IllegalArgumentException x) {
            if (op.isAlias()) {
                try {
                    setParametersAsTree((Operator) op.getDelegate());
                    return;
                } catch (ClassCastException | IllegalArgumentException y) {
                    x.addSuppressed(y);
                }
            }
            throw x;
        }
    }
    
    private void setParameters(Operator op, TreeFunction fn) {
        TreeFunction.Parameter fnParams = fn.getParameters();
        
        if (op.isUnaryOp()) {
            assert fnParams.count() == 1 : fn;
            fnParams = fnParams.get(0);
        }
        
        Param params = Param.create(null, fnParams);
        setParameters(params);
    }
    
    private void setParameters(Param params) {
        ParamGroup root;
        if (params instanceof ParamVar) {
            root = new ParamGroup(((ParamVar) params).name);
        } else {
            root = (ParamGroup) params;
        }
        model.setRoot(root);
        expandAll();
    }
    
    private void select(Param param) {
        if (param != null) {
            Object[] arr = model.pathTo(param);
            if (arr != null) {
                TreePath path = new TreePath(arr);
                tree.setSelectionPath(path);
                int row = tree.getRowForPath(path);
                if (row >= 0) {
                    tree.expandRow(row);
                }
            }
        } else {
            tree.clearSelection();
        }
    }
    
    private List<TreePath> getExpandedPaths() {
        List<TreePath> paths = new ArrayList<>(0);
        
        ParamGroup root = model.getRoot();
        if (root != null) {
            root.forEachGroup(group -> {
                Object[] arr = model.pathTo(group);
                if (arr != null) {
                    TreePath path = new TreePath(arr);
                    if (tree.isExpanded(path)) {
                        paths.add(path);
                    }
                }
            });
        }
        
        return paths;
    }
    
    private void expand(Param param) {
        if (param != null) {
            Object[] path = model.pathTo(param);
            if (path != null) {
                int row = tree.getRowForPath(new TreePath(path));
                if (row >= 0) {
                    tree.expandRow(row);
                }
            }
        }
    }
    
    private void expandAll() {
        JTree tree = this.tree;
        for (int i = 0; i < tree.getRowCount(); ++i) {
            tree.expandRow(i);
        }
    }
    
    public void addChangeListener(ChangeListener cl) {
        listeners = Misc.append(listeners, Objects.requireNonNull(cl, "cl"));
    }
    
    public void removeChangeListener(ChangeListener cl) {
        listeners = Misc.remove(listeners, Objects.requireNonNull(cl, "cl"));
    }
    
    public ChangeListener[] getChangeListeners() {
        return listeners.clone();
    }
    
    private void fireStateChanged() {
        if (!suppress) {
//            Log.low(getClass(), "fireStateChanged", "root = ", model.root);
            ChangeEvent changeEvent = this.changeEvent;
            for (ChangeListener cl : listeners) {
                cl.stateChanged(changeEvent);
            }
        }
    }
    
    static abstract class Param implements MutableTreeNode, Cloneable {
        private ParamGroup parent;
        
        private Param(ParamGroup parent) {
            this.parent = parent;
        }
        
        @Override
        public ParamGroup getParent() {
            return parent;
        }
        
        @Override
        public void setParent(MutableTreeNode parent) {
            if (parent instanceof ParamGroup) {
                this.parent = (ParamGroup) parent;
            }
        }
        
        @Override
        public abstract Enumeration<Param> children();
        
        Param getRoot() {
            Param param = this;
            for (Param parent; (parent = param.parent) != null;)
                param = parent;
            return param;
        }
        
        boolean isRoot() {
            return parent == null;
        }
        
        boolean contains(Param p) {
            return getIndex(p) >= 0;
        }
        
        static ParamGroup createDefault(int count) {
            String[] group = new String[count];
            for (int i = 0; i < count; ++i) {
                group[i] = (count <= 26) ? Character.toString((char) ('a' + i)) : Integer.toString(i);
            }
            return new ParamGroup(group);
        }
        
        static Param createDefault(Operator op) {
            if (op == null) {
                return null;
            }
            switch (op.getSymbolKind()) {
                case UNARY_OP:
                    UnaryOp unary = (UnaryOp) op;
                    int     count = unary.getTupleCount();
                    if (count > 0) {
                        String[] names = new String[count];
                        for (int i = 0; i < count; ++i) {
                            String name;
                            if (count <= 26) {
                                name = Character.toString((char) ('a' + i));
                            } else {
                                name = "arg" + i;
                            }
                            names[i] = name;
                        }
                        return new ParamGroup(names);
                    }
                    return new ParamVar("x");
                case BINARY_OP:
                    return new ParamGroup("lhs", "rhs");
                case SPAN_OP:
                    return new ParamVar("x");
                case NARY_OP:
                    return new ParamVar("args");
                default:
                    assert false : op;
                    return null;
            }
        }
        
        static Param create(ParamGroup parent, TreeFunction.Parameter param) {
            if (param instanceof TreeFunction.VariableParameter) {
                return new ParamVar(parent, (TreeFunction.VariableParameter) param);
            } else {
                return new ParamGroup(parent, param);
            }
        }
        
        String getLocationString() {
            StringBuilder b = new StringBuilder();
            getIndexString(b);
            return b.toString();
        }
        
        private void getIndexString(StringBuilder b) {
            Param[] group = parent.group;
            int     count = group.length;
            
            for (int i = 0; i < count; ++i) {
                if (this == group[i]) {
                    if (!parent.isRoot()) {
                        ((Param) parent).getIndexString(b);
                        b.append(", ");
                    }
                    b.append(i);
                    break;
                }
            }
        }
        
        List<Integer> getLocation() {
            if (isRoot()) {
                return new ArrayList<>(0);
            }
            List<Integer> list = new ArrayList<>(2);
            getIndexes(list);
            return list;
        }
        
        private void getIndexes(List<Integer> list) {
            Param[] group = parent.getGroup();
            int     count = group.length;

            for (int i = 0; i < count; ++i) {
                if (this == group[i]) {
                    if (!parent.isRoot()) {
                        ((Param) parent).getIndexes(list);
                    }
                    list.add(i);
                    break;
                }
            }
        }
        
        @Override
        public void removeFromParent() {
            ParamGroup parent = getParent();
            if (parent != null) {
                parent.remove(this);
            }
        }
        
        boolean isAncestorOf(Param p) {
            return this == p;
        }
        
        void forEachGroup(Consumer<? super ParamGroup> action) {
        }
        
        abstract void forEachVar(Consumer<? super ParamVar> action);
        
        abstract ParamVar findFirstVar(Predicate<? super ParamVar> test);
        
        abstract boolean isEqualTo(Param that);
        
        static boolean isEqual(Param lhs, Param rhs) {
            return (lhs == null) ? (rhs == null) : lhs.isEqualTo(rhs);
        }
        
        @Override
        public Param clone() {
            try {
                Param copy = (Param) super.clone();
                return copy;
            } catch (CloneNotSupportedException x) {
                throw new AssertionError("clone", x);
            }
        }
        
        void toString(StringBuilder b) {
            b.append(toString());
        }
        
        static void toString(Param p, StringBuilder b) {
            if (p == null) {
                b.append("null");
            } else {
                p.toString(b);
            }
        }
        
        String identityHashCode() {
            return Integer.toHexString(System.identityHashCode(this));
        }
        
        String toDebugString() {
            StringBuilder b = new StringBuilder();
            toDebugString(b, Misc.newIdentityHashSet());
            return b.toString();
        }
        
        static String toDebugString(Param p) {
            return (p == null) ? "null" : p.toDebugString();
        }
        
        abstract void toDebugString(StringBuilder b, Set<Param> seen);
        
        static void toDebugString(Param p, StringBuilder b, Set<Param> seen) {
            if (p == null) {
                b.append("null");
            } else {
                p.toDebugString(b, seen);
            }
        }
    }
    
    static final class ParamGroup extends Param {
        private Param[] group;
        
        private ParamGroup() {
            super(null);
            this.group = new Param[0];
        }
        
        private ParamGroup(ParamGroup parent, TreeFunction.Parameter param) {
            super(parent);
            
            int     count = param.count();
            Param[] group = new Param[count];
            
            for (int i = 0; i < count; ++i) {
                group[i] = create(this, param.get(i));
            }
            
            this.group = group;
        }
        
        private ParamGroup(ParamGroup parent, Param... group) {
            super(parent);
            this.group = Misc.requireNonNull(group, "group");
            for (Param p : group) {
                p.parent = this;
            }
        }
        
        private ParamGroup(String... vars) {
            super(null);
            
            int     count = vars.length;
            Param[] group = new Param[count];
            
            for (int i = 0; i < count; ++i) {
                group[i] = new ParamVar(this, vars[i]);
            }
            
            this.group = group;
        }
        
        Param[] getGroup() {
            return group.clone();
        }
        
        int count() {
            return group.length;
        }
        
        int varCount() {
            MutableInt count = new MutableInt();
            forEachVar(var -> ++count.value);
            return count.value;
        }
        
        Param get(int index) {
            return group[index];
        }
        
        @Override
        public int getChildCount() {
            return group.length;
        }
        
        @Override
        public Param getChildAt(int index) {
            return group[index];
        }
        
        @Override
        public int getIndex(TreeNode child) {
            Param[] group = this.group;
            int     count = group.length;
            for (int i = 0; i < count; ++i)
                if (group[i] == child)
                    return i;
            return -1;
        }
        
        @Override
        public Enumeration<Param> children() {
            return new Enumeration<Param>() {
                final Param[] group = ParamGroup.this.group;
                int i = 0;
                @Override
                public boolean hasMoreElements() {
                    return i < group.length;
                }
                @Override
                public Param nextElement() {
                    if (hasMoreElements()) {
                        return group[i++];
                    }
                    throw new NoSuchElementException();
                }
            };
        }
        
        @Override
        public void insert(MutableTreeNode node, int index) {
            if (node != null) {
                Param param  = (Param) node;
                assert !param.isAncestorOf(this) : param;
                this.group   = Misc.insert(this.group, index, param);
                param.parent = this;
            }
        }
        
        @Override
        public void remove(int index) {
            Param[] group = this.group;
            Param   param = group[index];
            this.group    = Misc.remove(group, index);
            param.parent  = null;
        }
        
        @Override
        public void remove(MutableTreeNode node) {
            Param param = (Param) node;
            if (param != null && this == param.parent) {
                this.group = Misc.remove(this.group, (Param) node);
                param.parent = null;
            }
        }
        
        @Override
        public void setUserObject(Object obj) {
            Log.entering(getClass(), "setUserObject(Object)");
        }
        
        @Override
        public boolean getAllowsChildren() {
            return true;
        }
        
        @Override
        public boolean isLeaf() {
            return false;
        }
        
        @Override
        boolean isAncestorOf(Param p) {
            if (super.isAncestorOf(p))
                return true;
            for (Param e : group)
                if (e.isAncestorOf(p))
                    return true;
            return false;
        }
        
        @Override
        void forEachGroup(Consumer<? super ParamGroup> action) {
            action.accept(this);
            for (Param p : group)
                p.forEachGroup(action);
        }
        
        @Override
        void forEachVar(Consumer<? super ParamVar> action) {
            for (Param p : group)
                p.forEachVar(action);
        }
        
        @Override
        ParamVar findFirstVar(Predicate<? super ParamVar> test) {
            for (Param p : group) {
                ParamVar var = p.findFirstVar(test);
                if (var != null)
                    return var;
            }
            return null;
        }
        
        @Override
        public String toString() {
            try {
                StringBuilder b = new StringBuilder();
                toString(b);
                return b.toString();
            } catch (StackOverflowError x) {
                throw Misc.initCause(new StackOverflowError(toDebugString()), x);
            }
        }
        
        @Override
        void toString(StringBuilder b) {
            b.append('(');
            Param[] group = this.group;
            int     count = group.length;
            for (int i = 0; i < count; ++i) {
                if (i != 0) {
                    b.append(", ");
                }
                Param.toString(group[i], b);
            }
            b.append(')');
        }
        
        @Override
        void toDebugString(StringBuilder b, Set<Param> seen) {
            String hash = identityHashCode();
            if (seen.add(this)) {
                b.append(hash).append('[');
                Param[] group = this.group;
                int     count = group.length;
                for (int i = 0; i < count; ++i) {
                    if (i != 0) {
                        b.append(", ");
                    }
                    Param.toDebugString(group[i], b, seen);
                }
                b.append(']');
            } else {
                b.append("CYCLE:").append(hash);
            }
        }
        
//        @Override
//        public int hashCode() {
//            return Arrays.hashCode(group);
//        }
//        
//        @Override
//        public boolean equals(Object obj) {
//            if (this == obj) {
//                return true;
//            }
//            if (obj instanceof ParamGroup) {
//                return Arrays.equals(this.group, ((ParamGroup) obj).group);
//            }
//            return false;
//        }
        
        @Override
        boolean isEqualTo(Param that) {
            if (this == that)
                return true;
            if (that instanceof ParamGroup) {
                Param[] groupL = this.group;
                Param[] groupR = ((ParamGroup) that).group;
                int     count  = groupL.length;
                if (count != groupR.length)
                    return false;
                for (int i = 0; i < count; ++i)
                    if (!Param.isEqual(groupL[i], groupR[i]))
                        return false;
                return true;
            }
            return false;
        }
        
        @Override
        public ParamGroup clone() {
            ParamGroup copy  = (ParamGroup) super.clone();
            Param[]    group = copy.group = this.group.clone();
            int        count = group.length;
            
            for (int i = 0; i < count; ++i) {
                Param p = group[i] = group[i].clone();
                p.parent = copy;
            }
            
            return copy;
        }
    }
    
    static final class ParamVar extends Param {
        private String name;
        
        private ParamGroup toggleGroup;
        
        private ParamVar(ParamGroup parent, TreeFunction.VariableParameter param) {
            this(parent, param.getVariable().getName());
        }
        
        private ParamVar(ParamGroup parent, String name) {
            super(parent);
            this.name = Objects.requireNonNull(name, "name");
        }
        
        private ParamVar(String name) {
            this(null, name);
        }
        
        String getName() {
            return name;
        }
        
        @Override
        public int getChildCount() {
            return 0;
        }
        
        @Override
        public Param getChildAt(int index) {
            throw new IndexOutOfBoundsException();
        }
        
        @Override
        public int getIndex(TreeNode node) {
            return -1;
        }
        
        @Override
        public Enumeration<Param> children() {
            return Collections.emptyEnumeration();
        }
        
        @Override
        public void insert(MutableTreeNode node, int index) {
            Log.entering(getClass(), "insert(MutableTreeNode, int)");
        }
        
        @Override
        public void remove(int index) {
            Log.entering(getClass(), "remove(int)");
        }
        
        @Override
        public void remove(MutableTreeNode node) {
            Log.entering(getClass(), "remove(MutableTreeNode)");
        }
        
        @Override
        public void setUserObject(Object obj) {
            if (!(obj instanceof String)) {
                Log.note(getClass(), "setUserObject", "obj = \"", obj, "\", class = ", (obj == null) ? null : obj.getClass().getName());
            }
            if (obj != null) {
                this.name = Misc.despace(obj.toString());
            }
        }
        
        @Override
        public boolean getAllowsChildren() {
            return false;
        }
        
        @Override
        public boolean isLeaf() {
            return true;
        }
        
        @Override
        void forEachVar(Consumer<? super ParamVar> action) {
            action.accept(this);
        }
        
        @Override
        ParamVar findFirstVar(Predicate<? super ParamVar> test) {
            return test.test(this) ? this : null;
        }
        
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        void toDebugString(StringBuilder b, Set<Param> seen) {
            String hash = identityHashCode();
            if (seen.add(this)) {
                b.append(hash).append('(').append(name).append(')');
            } else {
                b.append("DUPLICATE:").append(hash);
            }
        }
        
//        @Override
//        public int hashCode() {
//            return Objects.hashCode(name);
//        }
//        
//        @Override
//        public boolean equals(Object obj) {
//            return obj instanceof ParamVar && Objects.equals(name, ((ParamVar) obj).name);
//        }
        
        @Override
        boolean isEqualTo(Param that) {
            if (this == that)
                return true;
            return (that instanceof ParamVar) && Objects.equals(name, ((ParamVar) that).name);
        }
        
        @Override
        public ParamVar clone() {
            return (ParamVar) super.clone();
        }
    }
    
    private static final class CustomModelEvent extends TreeModelEvent {
        private volatile boolean isLast = true;
        
        CustomModelEvent(CustomModel src, Object[] path, int[] indices, Object[] children) {
            super(src, path, indices, children);
        }
        CustomModelEvent(CustomModel src, TreePath path) {
            super(src, path);
        }
        CustomModelEvent(CustomModel src, ParamGroup root) {
            super(src, new TreePath(root));
        }
        CustomModelEvent(CustomModel src) {
            this(src, src.root);
        }
        
        CustomModelEvent setLast(boolean isLast) {
            this.isLast = isLast;
            return this;
        }
    }
    
    private static final class CustomModel implements TreeModel, TreeWillExpandListener {
        private TreeModelListener[] listeners = new TreeModelListener[0];
        
        private ParamGroup root;
        
        CustomModel() {
        }
        
        @Override
        public ParamGroup getRoot() {
            return root;
        }
        
        void setRoot(ParamGroup root) {
            if (!Param.isEqual(this.root, root)) {
                this.root = root;
                treeStructureChanged();
            }
        }
        
        private void treeStructureChanged() {
            if (listeners.length == 0)
                return;
            TreeModelEvent e = new CustomModelEvent(this, new TreePath(root));
            for (TreeModelListener tml : listeners) {
                tml.treeStructureChanged(e);
            }
        }
        
        boolean contains(Param p) {
            return p != null && root != null && root.isAncestorOf(p);
        }
        
        @Override
        public void treeWillCollapse(TreeExpansionEvent e) throws ExpandVetoException {
            throw new ExpandVetoException(e);
        }
        
        @Override
        public void treeWillExpand(TreeExpansionEvent e) throws ExpandVetoException {
            throw new ExpandVetoException(e);
        }
        
        @Override
        public void addTreeModelListener(TreeModelListener tml) {
            listeners = Misc.append(listeners, Objects.requireNonNull(tml, "tml"));
        }
        
        @Override
        public void removeTreeModelListener(TreeModelListener tml) {
            listeners = Misc.remove(listeners, Objects.requireNonNull(tml, "tml"));
        }
        
        public TreeModelListener[] getTreeModelListeners() {
            return listeners.clone();
        }
        
        @Override
        public Param getChild(Object obj, int index) {
            return ((ParamGroup) obj).group[index];
        }
        
        @Override
        public int getChildCount(Object obj) {
            return ((ParamGroup) obj).group.length;
        }
        
        @Override
        public boolean isLeaf(Object obj) {
            return obj instanceof ParamVar;
        }
        
        @Override
        public int getIndexOfChild(Object parent, Object child) {
            return ((Param) parent).getIndex((Param) child);
        }
        
        boolean move(Param target, int distance) {
            int     moves   = 0;
            boolean success = true;
            while (distance != 0 && success) {
                success = false;
                if (distance < 0) {
                    success = moveUp(target);
                    ++distance;
                } else {
                    success = moveDown(target);
                    --distance;
                }
                if (success) {
                    ++moves;
                }
            }
            return moves > 0;
        }
        
        private boolean moveUp(Param target) {
            return moveNode(target, -1);
        }
        
        private boolean moveDown(Param target) {
            return moveNode(target, +1);
        }
        
        private boolean moveNode(Param target, int direction) {
            assert direction == -1 || direction == +1 : direction;
            
            if (contains(target)) {
                ParamGroup parent = target.getParent();
                if (parent != null) {
                    int index = parent.getIndex(target);
                    
                    if ((direction == -1) ? (index == 0) : (index == parent.count() - 1)) {
                        ParamGroup parent2 = parent.getParent();
                        if (parent2 == null) {
                            // cannot move outside the root
                            return false;
                        }
                        
                        index = parent2.getIndex(parent);
                        assert index >= 0 : parent2;
                        
                        parent = parent2;
                        index  = (direction == -1) ? index : (index + 1);
                        
                    } else {
                        index += direction;
                        
                        Param at = parent.get(index);
                        if (at instanceof ParamGroup) {
                            parent = (ParamGroup) at;
                            index  = (direction == -1) ? parent.count() : 0;
                        }
                    }
                    
                    moveNode(parent, index, target);
                    return true;
                }
            }
            
            return false;
        }
        
        private void moveNode(ParamGroup parent, int index, Param target) {
            removeNode(target);
            insertNode(parent, index, target);
        }
        
        boolean remove(Param target) {
            if (contains(target)) {
                removeNode(target);
                return true;
            }
            return false;
        }
        
        private void removeNode(Param target) {
            TreeModelListener[] listeners = this.listeners;
            
            if (listeners.length == 0) {
                target.removeFromParent();
                
            } else {
                Param    parent   = target.parent;
                Object[] pathArr  = pathTo(parent);
                int[]    indexArr = { parent.getIndex(target) };
                Object[] childArr = { target };
                
                assert indexArr[0] >= 0 : parent;
                
                target.removeFromParent();
                
                TreeModelEvent e = new CustomModelEvent(this, pathArr, indexArr, childArr).setLast(false);
                for (TreeModelListener tml : listeners)
                    tml.treeNodesRemoved(e);
                
                treeNodeChanged(parent);
            }
        }
        
        boolean append(Param param) {
            if (!contains(param)) {
                ParamGroup root = this.root;
                if (root == null) {
                    setRoot(new ParamGroup(null, param));
                } else {
                    insertNode(root, root.count(), param);
                }
                return true;
            }
            return false;
        }
        
        boolean append(ParamGroup parent, Param child) {
            return insert(parent, parent.count(), child);
        }
        
        boolean insert(ParamGroup parent, int index, Param child) {
            if (contains(parent) && !contains(child)) {
                index = Math.max(0, Math.min(index, parent.count()));
                insertNode(parent, index, child);
                return true;
            }
            return false;
        }
        
        private void insertNode(ParamGroup parent, int index, Param child) {
            parent.insert(child, index);
            
            TreeModelListener[] listeners = this.listeners;
            if (listeners.length > 0) {
                Object[] pathArr  = pathTo(parent);
                int[]    indexArr = { index };
                Object[] childArr = { child };
                
                TreeModelEvent e = new CustomModelEvent(this, pathArr, indexArr, childArr).setLast(false);
                
                for (TreeModelListener tml : listeners)
                    tml.treeNodesInserted(e);
                
                treeNodeChanged(parent);
            }
        }
        
        private Param toggle(Param p) {
            if (contains(p) && !p.isRoot()) {
                return toggleNode(p);
            }
            return null;
        }
        
        private Param toggleNode(Param p) {
            ParamGroup parent = p.getParent();
            int        index  = parent.getIndex(p);
            assert index >= 0 : f("p = %s, root = %s", p, root);
            
            Supplier<Param> elem;
            
            if (p instanceof ParamVar) {
                ParamVar   var    = (ParamVar) p;
                ParamGroup group  = var.toggleGroup;
                
                if (group == null) {
                    elem = () -> new ParamGroup(null, var);
                } else {
                    elem = () -> {
                        ParamGroup result = group;
                        ParamVar   first  = result.findFirstVar(v -> true);
                        if (first != null) {
                            first.name = var.name;
                        } else {
                            first = var;
                        }
                        return result;
                    };
                }
            } else {
                ParamGroup group = (ParamGroup) p;
                
                elem = () -> {
                    ParamVar result = group.findFirstVar(v -> true);
                    String   name   = (result == null) ? getUniqueName() : result.name;
                    
                    result = new ParamVar(name);
                    
                    result.toggleGroup = group;
                    return result;
                };
            }
            
            return setNode(parent, index, elem);
        }
        
        private Param setNode(ParamGroup parent, int index, Supplier<Param> elem) {
            Param before = parent.get(index);
            Param after  = elem.get();
            
            parent.group[index] = after;
            after.parent = parent;
            
            TreeModelListener[] listeners = this.listeners;
            if (listeners.length > 0) {
                TreeModelEvent e = new CustomModelEvent(this, new TreePath(pathTo(parent))).setLast(true);
                
                for (TreeModelListener tml : listeners)
                    tml.treeStructureChanged(e);
            }
            
            return after;
        }
        
        @Override
        public void valueForPathChanged(TreePath path, Object obj) {
            Object last = path.getLastPathComponent();
            
            if (last instanceof ParamVar) {
                ParamVar   var   = (ParamVar) last;
                ParamGroup group = var.getParent();
                int        index = group.getIndex(var);
                assert index >= 0 : path;
                
                String name = (obj == null) ? null : obj.toString();
                String uniq = getUniqueName(var, name);
                
                if (!Objects.equals(var.name, uniq)) {
                    if (!Objects.equals(name, uniq)) {
                        SwingMisc.beep();
                    }
                    
                    var.name = uniq;
                    treeNodeChanged(var);
                }
            } else {
                assert false : f("path = %s, obj = %s", path, obj);
            }
        }
        
        private void treeNodeChanged(Param changed) {
            treeNodeChanged(changed, true);
        }
        
        private void treeNodeChanged(Param changed, boolean entirePath) {
            if (listeners.length > 0 && changed != null) {
                do {
                    Param parent = changed.parent;
                    if (parent == null) {
                        // changed == root
                        break;
                    }
                    
                    Object[] pathArray    = pathTo(parent);
                    int[]    childIndexes = { parent.getIndex(changed) };
                    Object[] children     = { changed };
                    
                    changed = parent;
                    
                    boolean isLast = !entirePath || (changed.parent == null);
                    
                    TreeModelEvent e = new CustomModelEvent(this, pathArray, childIndexes, children).setLast(isLast);
                    treeNodeChanged(e);
                } while (entirePath);
            }
        }
        
        private void treeNodeChanged(TreeModelEvent e) {
            for (TreeModelListener tml : listeners) {
                tml.treeNodesChanged(e);
            }
        }
        
        private Object[] pathTo(Param target) {
            return pathTo(1, target);
        }
        
        private Object[] pathTo(int count, Param target) {
            Object[] path;
            if (target.isRoot()) {
                path = new Object[count];
            } else {
                path = pathTo(count + 1, target.parent);
            }
            path[path.length - count] = target;
            return path;
        }
        
        String getUniqueName() {
            return getUniqueName(null);
        }
        
        String getUniqueName(String name) {
            return getUniqueName(null, name);
        }
        
        String getDefaultName() {
            for (char c = 'a'; c <= 'z'; ++c) {
                String name = Character.toString(c);
                if (isUniqueName(name)) {
                    return name;
                }
            }
            return "var";
        }
        
        String getUniqueName(ParamVar var, String name) {
            if (name == null || name.isEmpty())
                name = getDefaultName();
            String uniq = name;
            
            for (int suffix = 0; !isUniqueName(var, uniq); ++suffix) {
                uniq = name + suffix;
            }
            
            return uniq;
        }
        
        private boolean isUniqueName(String name) {
            return isUniqueName(null, name);
        }
        
        private boolean isUniqueName(ParamVar var, String name) {
            return isUniqueName(root, var, name);
        }
        
        private boolean isUniqueName(Param curr, ParamVar var, String name) {
            if (curr != var && curr != null) {
                if (curr instanceof ParamVar) {
                    return !((ParamVar) curr).name.equals(name);
                } else {
                    for (Param e : ((ParamGroup) curr).group)
                        if (!isUniqueName(e, var, name))
                            return false;
                }
            }
            return true;
        }
    }
    
    private static final class ParameterCellRenderer extends DefaultTreeCellRenderer {
        private final Color foreground;
        private final Color disabled;
        
        ParameterCellRenderer() {
            setLeafIcon(null);
            setOpenIcon(null);
            setClosedIcon(null);
            
            foreground = UIManager.getColor("Label.foreground");
            disabled   = UIManager.getColor("Label.disabledForeground");
        }
        
        @Override
        public Component getTreeCellRendererComponent(JTree   tree,
                                                      Object  value,
                                                      boolean sel,
                                                      boolean expanded,
                                                      boolean leaf,
                                                      int     row,
                                                      boolean hasFocus) {
            String text = "";
            
            if (value != null) {
                text = Misc.escapeHtml(value.toString());
                if (leaf) {
                    text = "<html>" + value + "</html>";
                } else {
                    text = "<html><i>" + value + "</i></html>";
                }
            }
            
            super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
            
            if (foreground != null && disabled != null) {
                setForeground(leaf ? foreground : disabled);
            }
            
            return this;
        }
    }
    
    private static final class ParameterCellEditor extends DefaultTreeCellEditor {
        ParameterCellEditor(JTree tree) {
            super(tree, (DefaultTreeCellRenderer) tree.getCellRenderer());
        }
        
        @Override
        protected TreeCellEditor createTreeCellEditor() {
            javax.swing.border.Border b = UIManager.getBorder("Tree.editorBorder");
            DefaultTextField field = new DefaultTextField(b);
            DocumentPropertyChangeListener.add(field, new NoSpaceFilter());
            return new DefaultCellEditor(field);
        }
        
        @Override
        protected boolean canEditImmediately(EventObject e) {
            if (e instanceof MouseEvent) {
                MouseEvent me = (MouseEvent) e;
                boolean isLeft  = SwingUtilities.isLeftMouseButton(me);
                int     clicks  = me.getClickCount();
                boolean inHit   = inHitRegion(me.getX(), me.getY());
                boolean canEdit = isLeft && clicks >= 2 && inHit;
                Log.low(getClass(), "canEditImmediately",
                         "isLeft = ",    isLeft,
                         ", clicks = ",  clicks,
                         ", inHit = ",   inHit,
                         ", canEdit = ", canEdit);
                return canEdit && getParamAt(e) instanceof ParamVar;
            }
            return e == null;
        }
        
        @Override
        protected boolean inHitRegion(int x, int y) {
            if (lastRow >= 0 && tree != null) {
                Rectangle bounds = tree.getRowBounds(lastRow);
                return bounds.y <= y && y < bounds.y + bounds.height;
            }
            return super.inHitRegion(x, y);
        }
        
        @Override
        public boolean isCellEditable(EventObject e) {
            if (super.isCellEditable(e)) {
                Param param = getParamAt(e);
                return (param == null) || param instanceof ParamVar;
            }
            return false;
        }
        
        private Param getParamAt(EventObject e) {
            if (e instanceof MouseEvent && tree != null) {
                MouseEvent me = (MouseEvent) e;
                TreePath path = tree.getPathForLocation(me.getX(), me.getY());
                if (path != null) {
                    Object last = path.getLastPathComponent();
                    if (last instanceof DefaultMutableTreeNode)
                        last = ((DefaultMutableTreeNode) last).getUserObject();
                    return (Param) last;
                }
            }
            return null;
        }
        
        @Override
        protected void startEditingTimer() {
            // cancer
        }
    }
}

/*
final class Garbage {
    private static final class CrudeModel extends DefaultTreeModel {
        CrudeModel() {
            super(null, true);
        }
        
        void setRoot(Param param) {
            setRoot(toDefaultMutableTreeNode(param));
        }
        
        DefaultMutableTreeNode toDefaultMutableTreeNode(Param param) {
            if (param == null) {
                return null;
            }
            if (param instanceof ParamVar) {
                return new DefaultMutableTreeNode(param, false);
            }
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(param, true);
            for (Param child : ((ParamGroup) param).group) {
                node.add(toDefaultMutableTreeNode(child));
            }
            return node;
        }
    }
    
    private static final class ExtendedModel extends DefaultTreeModel {
        ExtendedModel() {
            super(null, true);
        }
        
        @Override
        public void valueForPathChanged(TreePath path, Object obj) {
            Object last = path.getLastPathComponent();
            
            if (last instanceof ParamVar) {
                ParamVar var  = (ParamVar) last;
                String   uniq = getUniqueName(var, obj.toString());
                
                if (Objects.equals(var.name, uniq)) {
                    return;
                }
                
                var.name = uniq;
                nodeChanged(var);
            } else {
                assert false : f("path = %s, val = %s", path, obj);
            }
        }
        
        String getUniqueName(String name) {
            return getUniqueName(null, name);
        }
        
        String getUniqueName(ParamVar var, String name) {
            if (name == null || name.isEmpty())
                name = "var";
            String uniq = name;
            
            for (int suffix = 0; !isUniqueName(var, uniq); ++suffix) {
                uniq = name + suffix;
            }
            
            return uniq;
        }
        
        private boolean isUniqueName(ParamVar var, String name) {
            return isUniqueName((Param) getRoot(), var, name);
        }
        
        private boolean isUniqueName(Param curr, ParamVar var, String name) {
            if (curr != var) {
                if (curr instanceof ParamVar) {
                    return !((ParamVar) curr).name.equals(name);
                } else {
                    for (Param e : ((ParamGroup) curr).group)
                        if (!isUniqueName(e, var, name))
                            return false;
                }
            }
            return true;
        }
    }
}
*/

/*
final class ParameterEditor_table extends JComponent {
    private static final String NAME = "Name";
    private static final String LOCATION = "Location";
    
    private final JToolBar tools;
    
    private final JButton add;
    private final JButton del;
    private final JButton up;
    private final JButton down;
    
    private final List<JButton> buttons;
    
    private final JTable table;
    
    private final DefaultTableModel model;
    
    private final TableModelListener tableModelListener;
    
    private Param params;
    
    ParameterEditor_table() {
        this(2);
    }
    
    ParameterEditor_table(int initialRowCount) {
        setLayout(new BorderLayout());
        
        tools = new JToolBar(JToolBar.HORIZONTAL);
        tools.setFloatable(false);
        
        add  = new JButton("Add");
        del  = new JButton("Remove");
        up   = new JButton("↑");
        down = new JButton("↓");
        
        up.addActionListener(e -> up());
        down.addActionListener(e -> down());
        
        tools.add(add);
        tools.add(del);
        tools.addSeparator();
        tools.add(up);
        tools.add(down);
        buttons = ListOf(add, del, up, down);
        
        model = new DefaultTableModel(new Object[] {NAME, LOCATION}, initialRowCount);
        
        for (int i = 0; i < initialRowCount; ++i) {
            String val = Integer.toString(i);
            model.setValueAt(val, i, 0);
            model.setValueAt(val, i, 1);
        }
        
        table = new JTable(model);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setRowSelectionAllowed(true);
        table.setDragEnabled(false);
        
        TableColumn nameCol = table.getColumn(NAME);
        nameCol.setCellRenderer(new NameCellRenderer());
        nameCol.setCellEditor(createNameEditor());
        
        TableColumn locCol = table.getColumn(LOCATION);
        locCol.setCellRenderer(new LocationCellRenderer());

        JScrollPane scroll =
            new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        add(tools, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);
        
        tableModelListener = e -> parametersChanged();
        model.addTableModelListener(tableModelListener);
    }
        
    private TableCellEditor createNameEditor() {
        try {
            Constructor<?> ctor = Class.forName("javax.swing.JTable$GenericEditor").getDeclaredConstructor();
            ctor.setAccessible(true);
            DefaultCellEditor editor = (DefaultCellEditor) ctor.newInstance();
            DocumentPropertyChangeListener.add((JTextField) editor.getComponent(), new NoSpaceFilter());
            return editor;
        } catch (ReflectiveOperationException | ClassCastException x) {
            Log.caught(getClass(), "createNameEditor", x, false);
            return new DefaultCellEditor(new JTextField() {
                {
                    DocumentPropertyChangeListener.add(this, new NoSpaceFilter());
                }
                @Override
                public void setBorder(Border b) {
                    if (b == null) super.setBorder(b);
                }
            });
        }
    }
    
    private void up() {
        move(-1);
    }
    
    private void down() {
        move(+1);
    }
    
    private void move(int dir) {
        int from  = table.getSelectedRow();
        if (from < 0)
            return;
        int count = table.getRowCount();
        int to    = (from + dir + count) % count;
        model.moveRow(from, from, to);
        table.getSelectionModel().setSelectionInterval(to, to);
    }
    
    private void clearTable() {
        while (model.getRowCount() > 0)
            model.removeRow(0);
    }
    
    private void setButtonsEnabled(boolean e) {
        for (JButton btn : buttons)
            btn.setEnabled(e);
    }
    
    Param getParameters() {
        return params;
    }
    
    boolean setParameters(Operator op) {
        model.removeTableModelListener(tableModelListener);
        try {
            return setParametersImpl(op);
        } finally {
            model.addTableModelListener(tableModelListener);
        }
    }
    
    private boolean setParametersImpl(Operator op) {
        params = null;
        clearTable();
        setButtonsEnabled(op != null && (op.isUnaryOp() || op.isBinaryOp()));
        if (op == null) {
            return false;
        }
        
        try {
            TreeFunction fn = Trees.getTreeFunction(op);
            setParameters(op, fn);
            return false;
        } catch (ClassCastException | IllegalArgumentException x) {
            Log.caught(getClass(), "setParameters", x, true);
            
            Param params;
            switch (op.getSymbolKind()) {
                case UNARY_OP:
                    params = new ParamVar(0, "x");
                    break;
                case BINARY_OP:
                    params = new ParamGroup("lhs", "rhs");
                    break;
                case SPAN_OP:
                    params = new ParamVar(0, "x");
                    break;
                case NARY_OP:
                    params = new ParamVar(0, "args");
                    break;
                default:
                    throw new AssertionError(op);
            }
            
            setParameters(params);
            return true;
        }
    }
    
    private void setParameters(Operator op, TreeFunction fn) {
        TreeFunction.Parameter fnParams = fn.getParameters();
        
        if (op.isUnaryOp()) {
            assert fnParams.count() == 1 : fn;
            fnParams = fnParams.get(0);
        }
        
        Param params = Param.create(null, new MutableInt(), fnParams);
        setParameters(params);
    }
    
    private void setParameters(Param params) {
        this.params = params;
        if (params != null) {
            params.forEachVar(this::add);
        }
    }
    
    private void add(ParamVar var) {
        model.addRow(var.getRow());
    }
    
    private void parametersChanged() {
        int count = model.getRowCount();
        
        List<Pair<String, List<Integer>>> rows = new ArrayList<>(count);
        
        boolean err = false;
        
        for (int i = 0; i < count; ++i) {
            try {
                String name = (String) model.getValueAt(i, 0);
                String loc  = (String) model.getValueAt(i, 1);
                
                List<Integer> indexes = toIndexes(loc);
                rows.add(Pair.of(name, indexes));
                
            } catch (ClassCastException | NumberFormatException x) {
                Log.caught(getClass(), "parametersChanged", x, true);
                err = true;
            }
        }
        
        if (err) {
            // ...
            return;
        }
        
        List<Pair<String, List<Integer>>> copy = process(rows);
        
        Object tree   = createTree(copy);
        Param  params = Param.createFromTree(null, rows, tree);
        
        this.params = params;
        table.repaint();
        
        // notify
    }
    
    private List<Pair<String, List<Integer>>> process(List<Pair<String, List<Integer>>> rows) {
        List<Pair<String, List<Integer>>> copy = new ArrayList<>(rows);
        //
        
        copy.sort(RowComparator.INSTANCE);
        processRange(copy, 0, copy.size(), 0);
        
        log("rows after process", copy);
        
        if (renameDuplicates(copy)) {
            log("rows after rename", copy);
        }
        
        return copy;
    }
    
    private void pad(List<Pair<String, List<Integer>>> rows) {
        int count = 0;
        for (int i = 0; i < count; ++i) {
            if (rows.get(i).right.isEmpty()) {
                break;
            }
        }
    }
    
    private void processRange(List<Pair<String, List<Integer>>> rows, int start, int end, int indexIndex) {
        if (start == end) {
            return;
        }
        
        assert start < end : f("start = %s, end = %s, indexIndex = %s, rows = %s", start, end, indexIndex, rows);
        
        int padEnd = -1;
        for (int i = start; i < end; ++i) {
            Pair<String, List<Integer>> row = rows.get(i);
            
            if (indexIndex < row.right.size()) {
                padEnd = i;
                break;
            }
        }
        
        int count = end - start;
        
        if (padEnd == -1) {
            if (indexIndex == 0) {
                for (int i = 0; i < count; ++i) {
                    rows.get(start + i).right.add(i);
                }
            }
        } else {
            int minIndex = rows.get(padEnd).right.get(indexIndex);
            int padCount = padEnd - start;
            
            for (int i = 0; i < padCount; ++i) {
                int distance = padCount - i;
                rows.get(start + i).right.add(minIndex - distance);
            }
            
            processRange(rows, padEnd, end, indexIndex + 1);
        }
    }
    
    private void processRange0(List<Pair<String, List<Integer>>> rows, int start, int end, int indexIndex) {
        int count = end - start;
        if (count == 0) {
            return;
        }
        
        int padEnd = -1;
        for (int i = start; i < end; ++i) {
            Pair<String, List<Integer>> row = rows.get(i);
            
            if (indexIndex < row.right.size()) {
                padEnd = i;
                break;
            }
        }
        
        if (padEnd == end) {
            for (int i = 0; i < count; ++i) {
                rows.get(start + i).right.add(i);
            }
        } else if (padEnd != -1) {
            int minIndex = rows.get(padEnd).right.get(indexIndex);
            int padCount = padEnd - start;
            
            for (int i = 0; i < padCount; ++i) {
                int distance = padCount - i;
                rows.get(start + i).right.add(minIndex - distance);
            }
            
            processRange0(rows, padEnd, end, indexIndex + 1);
        }
    }
    
    private boolean renameDuplicates(List<Pair<String, List<Integer>>> rows) {
        int rowCount = rows.size();
        Map<String, List<Pair<String, ?>>> byName = new LinkedHashMap<>(rowCount);
        
        boolean hasDuplicate = false;
        
        for (int i = 0; i < rowCount; ++i) {
            Pair<String, List<Integer>> row = rows.get(i);
            String name = row.left;
            
            List<Pair<String, ?>> list = byName.get(name);
            if (list == null) {
                byName.put(name, ListOf(row));
            } else {
                list.add(row);
                hasDuplicate = true;
            }
        }
        
        if (!hasDuplicate) {
            return false;
        }
        
        for (Map.Entry<String, List<Pair<String, ?>>> e : byName.entrySet()) {
            List<Pair<String, ?>> dupes = e.getValue();
            int dupeCount = dupes.size();
            
            if (dupeCount > 1) {
                String name = e.getKey();
                
                for (int i = 0; i < dupeCount; ++i) {
                    dupes.get(i).left = name + (1 + i);
                }
            }
        }
        
        return true;
    }
    
    private Object createTree(List<Pair<String, List<Integer>>> rows) {
        return createTree(rows, 0, rows.size(), 0);
    }
    
    private Object createTree(List<Pair<String, List<Integer>>> rows, int start, int end, int indexIndex) {
        int count = end - start;
        
        List<Object> result = new ArrayList<>(count);
        
        for (int i = 0; i < count;) {
            Pair<String, List<Integer>> first = rows.get(start + i);
            
            if ((indexIndex + 1) < first.right.size()) {
                int groupEnd = i + 1;
                
                for (; groupEnd < count; ++groupEnd) {
                    Pair<String, List<Integer>> last = rows.get(start + groupEnd);
                    
                    if ((indexIndex + 1) >= last.right.size()) {
                        break;
                    }
                }
                
                result.add(createTree(rows, i, groupEnd, indexIndex + 1));
                i = groupEnd;
                
            } else {
                result.add(first);
                ++i;
            }
        }
        
        return result;
    }
    
    private void log(String label, List<Pair<String, List<Integer>>> rows) {
        Log.note(getClass(), "log", label, " = ", rows);
    }
    
    private enum RowComparator implements Comparator<Pair<String, List<Integer>>> {
        INSTANCE;
        @Override
        public int compare(Pair<String, List<Integer>> rowL, Pair<String, List<Integer>> rowR) {
            List<Integer> indexesL = rowL.right;
            List<Integer> indexesR = rowR.right;
            
            int countL   = indexesL.size();
            int countR   = indexesR.size();
            int minCount = Math.min(countL, countR);
            
            for (int i = 0; i < minCount; ++i) {
                int comp = indexesL.get(i).compareTo(indexesR.get(i));
                if (comp != 0) {
                    return comp;
                }
            }
            
            if (countL != countR) {
                return Integer.compare(countL, countR);
            }
            
            return 0;
        }
    }
    
    private List<Integer> toIndexes(String str) {
        str = str.trim();
        if (str.isEmpty()) {
            return new ArrayList<>(0);
        }
        
        List<Integer> list = new ArrayList<>(2);
        
        NumberFormatException caught = null;
        
        for (String e : str.split(",")) {
            e = e.trim();
            
            try {
                Integer i = Integer.valueOf(e);
                list.add(i);
            } catch (NumberFormatException x) {
                if (caught != null)
                    x.addSuppressed(caught);
                caught = x;
            }
        }
        
        if (caught != null) {
            throw caught;
        }
        
        return list;
    }
    
    static abstract class Param {
        private final Param parent;
        
        private transient volatile String location;
        
        private Param(Param parent) {
            this.parent = parent;
        }
        
        Param getParent() {
            return parent;
        }
        
        Param getRoot() {
            Param param = this;
            for (Param parent; (parent = param.parent) != null;)
                param = parent;
            return param;
        }
        
        boolean isRoot() {
            return parent == null;
        }
        
        static Param create(Param parent, MutableInt row, TreeFunction.Parameter param) {
            if (param instanceof TreeFunction.VariableParameter) {
                return new ParamVar(parent, row.value++, (TreeFunction.VariableParameter) param);
            } else {
                return new ParamGroup(parent, row, param);
            }
        }
        
        static Param createFromTree(Param parent, List<?> rows, Object obj) {
            if (obj instanceof Pair<?, ?>) {
                return new ParamVar(parent, rows.indexOf(obj), (String) ((Pair<?, ?>) obj).left);
            } else {
                return new ParamGroup(parent, rows, (List<?>) obj);
            }
        }
        
        String getLocationString() {
            String loc = location;
            if (loc == null) {
                StringBuilder b = new StringBuilder();
                getIndexString(b);
                loc = location = b.toString();
            }
            return loc;
        }
        
        private void getIndexString(StringBuilder b) {
            if (parent instanceof ParamGroup) {
                Param[] group = ((ParamGroup) parent).group;
                int     count = group.length;
                
                for (int i = 0; i < count; ++i) {
                    if (this == group[i]) {
                        if (!parent.isRoot()) {
                            parent.getIndexString(b);
                            b.append(", ");
                        }
                        b.append(i);
                        break;
                    }
                }
            }
        }
        
        List<Integer> getLocation() {
            if (isRoot()) {
                return new ArrayList<>(0);
            }
            List<Integer> list = new ArrayList<>(2);
            getIndexes(list);
            return list;
        }
        
        private void getIndexes(List<Integer> list) {
            if (parent instanceof ParamGroup) {
                Param[] group = ((ParamGroup) parent).getGroup();
                int     count = group.length;
                
                for (int i = 0; i < count; ++i) {
                    if (this == group[i]) {
                        if (!parent.isRoot()) {
                            parent.getIndexes(list);
                        }
                        list.add(i);
                        break;
                    }
                }
            }
        }
        
        ParamVar getRowVar(int rowIndex) {
            return findFirstVar(p -> p.getRowIndex() == rowIndex);
        }
        
        abstract void forEachVar(Consumer<? super ParamVar> action);
        
        abstract ParamVar findFirstVar(Predicate<? super ParamVar> test);
    }
    
    static final class ParamGroup extends Param {
        private final Param[] group;
        
        private ParamGroup(Param parent, MutableInt row, TreeFunction.Parameter param) {
            super(parent);
            
            int     count = param.count();
            Param[] group = new Param[count];
            
            for (int i = 0; i < count; ++i) {
                group[i] = create(this, row, param.get(i));
            }
            
            this.group = group;
        }
        
        private ParamGroup(Param parent, List<?> rows, List<?> objs) {
            super(parent);
            
            int     count = objs.size();
            Param[] group = new Param[count];
            
            for (int i = 0; i < count; ++i) {
                group[i] = createFromTree(this, rows, objs.get(i));
            }
            
            this.group = group;
        }
        
        private ParamGroup(Param parent, Param... group) {
            super(parent);
            this.group = Misc.requireNonNull(group, "group");
        }
        
        private ParamGroup(String... vars) {
            super(null);
            
            int     count = vars.length;
            Param[] group = new Param[count];
            
            for (int i = 0; i < count; ++i) {
                group[i] = new ParamVar(this, i, vars[i]);
            }
            
            this.group = group;
        }
        
        Param[] getGroup() {
            return group.clone();
        }
        
        int count() {
            return group.length;
        }
        
        Param get(int index) {
            return group[index];
        }
        
        @Override
        void forEachVar(Consumer<? super ParamVar> action) {
            for (Param p : group)
                p.forEachVar(action);
        }
        
        @Override
        ParamVar findFirstVar(Predicate<? super ParamVar> test) {
            for (Param p : group) {
                ParamVar var = p.findFirstVar(test);
                if (var != null)
                    return var;
            }
            return null;
        }
        
        @Override
        public String toString() {
            return Arrays.toString(group);
        }
    }
    
    static final class ParamVar extends Param {
        private final int    row;
        private final String name;
        
        private ParamVar(Param parent, int row, TreeFunction.VariableParameter param) {
            this(parent, row, param.getVariable().getName());
        }
        
        private ParamVar(Param parent, int row, String name) {
            super(parent);
            this.row  = row;
            this.name = Objects.requireNonNull(name, "name");
        }
        
        private ParamVar(int row, String name) {
            this(null, row, name);
        }
        
        String getName() {
            return name;
        }
        
        String[] getRow() {
            return new String[] {getName(), getLocationString()};
        }
        
        int getRowIndex() {
            return row;
        }
        
        private int getRowIndex0() {
            MutableInt index = new MutableInt();
            getRowIndex(getRoot(), index);
            return index.value;
        }
        
        private boolean getRowIndex(Param curr, MutableInt index) {
            if (this == curr)
                return true;
            if (curr instanceof ParamVar) {
                ++index.value;
            } else {
                for (Param e : ((ParamGroup) curr).group) {
                    if (getRowIndex(e, index))
                        return true;
                }
            }
            return false;
        }
        
        @Override
        void forEachVar(Consumer<? super ParamVar> action) {
            action.accept(this);
        }
        
        @Override
        ParamVar findFirstVar(Predicate<? super ParamVar> test) {
            return test.test(this) ? this : null;
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    private final class NameCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable  table,
                                                       Object  value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int     row,
                                                       int     column) {
            String text   = null;
            Param  params = ParameterEditor_table.this.params;
            
            if (params != null) {
                ParamVar var = params.getRowVar(row);
                if (var != null) {
                    if (!var.name.equals(value)) {
                        text = "<html>" + value + " <i>(" + var.name + ")</i></html>";
                    }
                }
            }
            
            if (text == null) {
                text = "<html>" + value + "</html>";
            }
            
            return super.getTableCellRendererComponent(table, text, isSelected, hasFocus, row, column);
        }
    }
    
    private final class LocationCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable  table,
                                                       Object  value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int     row,
                                                       int     column) {
            String text   = null;
            Param  params = ParameterEditor_table.this.params;
            
            if (params != null) {
                ParamVar var = params.getRowVar(row);
                if (var != null) {
                    String loc = var.getLocationString();
                    if (!loc.equals(value)) {
                        text = "<html>" + value + " <i>(" + loc + ")</i></html>";
                    }
                }
            }
            
            if (text == null) {
                text = "<html>" + value + "</html>";
            }
            
            return super.getTableCellRendererComponent(table, text, isSelected, hasFocus, row, column);
        }
    }
}
*/