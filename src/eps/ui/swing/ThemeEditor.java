/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.util.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;
import static eps.app.App.*;

import java.util.*;
import java.util.regex.*;
import java.util.List;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

final class ThemeEditor extends JPanel implements SwingSettingsWindow.Tab {
    private final SwingSettingsWindow window;
    
    private final ThemeModel model;
    
    private final ThemeBox themeBox;
    private final JButton copy;
    private final JButton rename;
    private final JButton delete;
    
    private final ColorSelector backgroundColor;
    private final ColorSelector caretColor;
    
    private final StyleList styleList;
    private final StylePreviewPane stylePreview;
    private final StyleEditor styleEditor;
    
    private final List<Component> componentsToEnable;
    
    ThemeEditor(SwingSettingsWindow window) {
        this.window = Objects.requireNonNull(window, "window");
        
        int minor = getMinorMargin();
        
        setLayout(new BorderLayout(minor, minor));
        setOpaque(false);
        
        model    = new ThemeModel();
        themeBox = new ThemeBox(model);
        read();
        
        copy   = new JButton("Duplicate");
        delete = new JButton("Delete");
        rename = new JButton("Rename");
        
        JPanel themePane = new JPanel(new FlowLayout(FlowLayout.CENTER));
        themePane.setOpaque(false);
        
        themePane.add(themeBox);
        themePane.add(copy);
        themePane.add(delete);
        themePane.add(rename);
        
        backgroundColor = new ColorSelector("Background");
        caretColor      = new ColorSelector("Caret", "Auto", () -> {
            if (model.hasSelection()) {
                return model.theme.getComputedCaretColor();
            }
            return ArgbColor.BLACK;
        });
        
        JPanel general = new JPanel(new FlowLayout(FlowLayout.LEFT));
        general.setOpaque(false);
        
        general.add(backgroundColor);
        general.add(caretColor);
        
        JPanel topPane = new JPanel(new BorderLayout());
        topPane.setOpaque(false);
        
        topPane.add(themePane, BorderLayout.NORTH);
        topPane.add(general,   BorderLayout.CENTER);
        
        styleList = new StyleList();
        styleList.setDragEnabled(false);
        styleList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JScrollPane listScroll =
            new JScrollPane(styleList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                       JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        stylePreview = new StylePreviewPane();
        
        JScrollPane previewScroll =
            new JScrollPane(stylePreview, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                          JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED) {
            @Override
            public Dimension getMinimumSize() {
                return getPreferredSize();
            }
            private Dimension cachedPreferredSize;
            @Override
            public Dimension getPreferredSize() {
                Dimension pref = cachedPreferredSize;
                if (pref != null)
                    return new Dimension(pref);
                pref = super.getPreferredSize();
                if ((pref.width * pref.height) > 0 && isDisplayable())
                    cachedPreferredSize = new Dimension(pref);
                return pref;
            }
        };
        
        styleEditor = new StyleEditor();
        styleEditor.setOpaque(false);
        styleEditor.setBorder(BorderFactory.createEmptyBorder(minor, minor, minor, minor));
        
        JSplitPane styleSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        styleSplit.setTopComponent(previewScroll);
        styleSplit.setBottomComponent(styleEditor);
        styleSplit.setContinuousLayout(true);
        styleSplit.setResizeWeight(0);
        
        add(topPane,    BorderLayout.NORTH);
        add(listScroll, BorderLayout.WEST);
        add(styleSplit, BorderLayout.CENTER);
        
        componentsToEnable = ListOf(backgroundColor, caretColor, styleEditor);
        
        setToolTips();
        addListeners();
    }
    
    private void addListeners() {
        copy.addActionListener(e -> copy());
        delete.addActionListener(e -> delete());
        rename.addActionListener(e -> rename());
        themeBox.addActionListener(e -> themeSelectionChanged());
        
        backgroundColor.addActionListener(e -> backgroundColorChanged());
        caretColor.addActionListener(e -> caretColorChanged());
        
        styleList.addListSelectionListener(e -> styleSelected());
        styleEditor.addActionListener(e -> styleChanged());
        
        window.addCancelListener(e -> { read(); setModel(); });
        window.addApplyListener(e -> write());
    }
    
    private void setToolTips() {
        copy.setToolTipText("Duplicate the currently-selected theme.");
        rename.setToolTipText("Open an input dialog box to rename the currently-selected theme.");
        
        backgroundColor.getButton().setToolTipText("Choose the background color for the main window.");
        caretColor.getButton().setToolTipText("Choose the caret color, used in most text fields.");
        caretColor.getCheckBox().setToolTipText("Select 'Auto' if the caret color should be computed automatically.");
    }
    
    @Override
    public void refresh() {
        read();
        setModel();
    }
    
    private void themeSelectionChanged() {
        Theme sel = model.getSelectedItem();
        Log.note(ThemeEditor.class, "themeSelectionChanged", coals(sel, Theme::getName), " selected");
        
        setCopyEnabled();
        setDeleteAndRenameEnabled();
        
        stylePreview.setTheme(sel);
//        styleEditor.setTheme(sel);
    }
    
    private void read() {
        read(false);
    }
    
    private void read(boolean preservePreviousSelection) {
        Theme sel = model.getSelectedItem();
        
        model.clear();
        model.addAll(Theme.values());
        
        if (preservePreviousSelection && sel != null) {
            String name = sel.getName();
            sel = model.list.stream().filter(t -> t.getName().equals(name)).findFirst().orElse(null);
            
            if (sel == null && !model.list.isEmpty()) {
                sel = model.list.get(0);
            }
            
            model.setSelectedItem(sel, true);
        }
    }
    
    private void setModel() {
        styleList.setSelectedValue(Theme.Key.BASIC, true);
        
        Theme  theme = createThemeFromWindow();
        String name  = theme.getName();
        
        if (model.list.contains(theme)) {
            model.setSelectedItem(theme);
        } else {
            if (model.isDuplicateName(null, name)) {
                name  = model.getUniqueName(null, name);
                theme = theme.rename(name);
            }
            model.add(theme);
            model.setSelectedItem(theme);
            putSettings();
        }
        
        Log.note(ThemeEditor.class, "setModel", "name was ", name, "; selected is ",
                                    coals(model.getSelectedItem(), Theme::getName));
    }
    
    private Theme createThemeFromWindow() {
        Theme.FreeBuilder builder = new Theme.FreeBuilder(Theme.DEFAULT);
        
        builder.setName(window.getSetting(Setting.THEME_NAME))
               .setBackgroundColor(window.getSetting(Setting.BACKGROUND_COLOR))
               .setCaretColor(window.getSetting(Setting.CARET_COLOR));
        
        for (Theme.Key key : Theme.Key.VALUES) {
            builder.setStyle(key, window.getSetting(key.getSetting()));
        }
        
        return builder.build();
    }
    
    private void write() {
        if (model.themesChanged) {
            Theme.setAll(model.list);
            app().writeThemes();
            model.themesChanged = false;
        }
    }
    
    private void putSettings() {
        Theme theme = model.getSelectedItem();
        if (theme == null) {
            window.removeSetting(Setting.THEME_NAME);
            window.removeSetting(Setting.BACKGROUND_COLOR);
            window.removeSetting(Setting.CARET_COLOR);
            for (Theme.Key key : Theme.Key.VALUES) {
                window.removeSetting(key.getSetting());
            }
        } else {
            window.putSetting(Setting.THEME_NAME,       theme.getName());
            window.putSetting(Setting.BACKGROUND_COLOR, theme.getBackgroundColor());
            window.putSetting(Setting.CARET_COLOR,      theme.getCaretColor());
            for (Theme.Key key : Theme.Key.VALUES) {
                window.putSetting(key.getSetting(), key.apply(theme));
            }
        }
    }
    
    private void updateEditorComponents() {
        Log.entering(ThemeEditor.class, "updateEditorComponents");
        Theme sel = model.getSelectedItem();
        
        if (sel != null) {
            backgroundColor.setColor(sel.getBackgroundColor());
            
            ArgbColor caret = sel.getCaretColor();
            if (caret == null) {
                caretColor.setColor(sel.getComputedCaretColor());
            }
            caretColor.setColor(caret);
            
        } else {
            backgroundColor.setColor(ArgbColor.WHITE);
            caretColor.setColor(null);
        }
        
        for (Component comp : componentsToEnable) {
            comp.setEnabled(sel != null);
        }
        
        stylePreview.setTheme(sel);
        styleEditor.setTheme(sel);
    }
    
    private void setCopyEnabled() {
        copy.setEnabled(model.getSelectedItem() != null);
    }
    
    private void setDeleteAndRenameEnabled() {
        Theme   sel = model.getSelectedItem();
        boolean e   = sel != null && !Theme.isPreset(sel);
        rename.setEnabled(e);
        delete.setEnabled(e);
    }
    
    // Note: for copy and rename, there's no need to update the editor
    //       because the actual theme properties don't change.
    
    private void copy() {
        Log.note(ThemeEditor.class, "copy", "copying ", model.requireSelection().getName());
        model.copySelectedTheme();
    }
    
    private void delete() {
        Theme sel = model.getSelectedItem();
        if (sel != null) {
            int choice =
                JOptionPane.showConfirmDialog(this, "Are you sure you want to delete the selected theme '" + sel.getName() + "'?",
                                                    "Confirm Deletion",
                                                    JOptionPane.YES_NO_OPTION,
                                                    JOptionPane.QUESTION_MESSAGE);
            if (choice == JOptionPane.YES_OPTION) {
                Log.note(ThemeEditor.class, "delete", "deleting ", model.requireSelection().getName());
                model.deleteSelectedTheme();
            }
        }
    }
    
    private void rename() {
        Theme  theme = model.requireSelection();
        String name  = theme.getName();
        
        name = (String) JOptionPane.showInputDialog(this, "Enter a new, unique name for '" + name + "':",
                                                          "Rename Theme",
                                                          JOptionPane.PLAIN_MESSAGE,
                                                          null,
                                                          null,
                                                          name);
        if (name != null) {
            Log.note(ThemeEditor.class, "rename", "renaming to ", name);
            model.renameSelectedTheme(name);
        }
    }
    
    private void backgroundColorChanged() {
        model.builder().setBackgroundColor(backgroundColor.getColor());
        
        caretColor.setColor(caretColor.getColor());
        model.themeChanged();
    }
    
    private void caretColorChanged() {
        model.builder().setCaretColor(caretColor.getColor());
        model.themeChanged();
    }
    
    private void styleSelected() {
        Theme.Key key = styleList.getSelectedValue();
        Log.note(ThemeEditor.class, "styleSelected", key, " selected");
        
        stylePreview.setStyle(key);
        styleEditor.setKey(key);
    }
    
    private void styleChanged() {
        Theme   theme = styleEditor.getTheme();
        Theme.Key key = styleEditor.getKey();
        
        if (theme != null && key != null) {
            Style style = styleEditor.getStyle();
            
            model.builder().setStyle(key, style);
            model.themeChanged();
        }
    }
    
    private static final class ThemeBox extends JComboBox<Theme> {
        ThemeBox(ThemeModel model) {
            setRenderer(new ThemeCellRenderer());
            setEditable(false);
            
            setModel(model);
        }
        
        private Dimension cachedPreferredSize;
        
        @Override
        public Dimension getPreferredSize() {
            // We don't want the size of the combo box to
            // change when new items are added to it.
            Dimension pref = cachedPreferredSize;
            if (pref == null) {
                pref = super.getPreferredSize();
                pref.width *= 2;
                if (isDisplayable() && (pref.width * pref.height) > 0) {
                    cachedPreferredSize = pref;
                }
            } else {
                pref = new Dimension(pref);
            }
            return pref;
        }
    }
    
    private final class ThemeModel implements ComboBoxModel<Theme> {
        final List<Theme> list = new ArrayList<>();
        
        private ListDataListener[] listeners = new ListDataListener[0];
        
        Theme theme;
        Theme.FreeBuilder builder;
        
        boolean themesChanged = false;
        
        Theme requireSelection() {
            if (theme == null) {
                throw new IllegalStateException();
            }
            return theme;
        }
        
        Theme.FreeBuilder builder() {
            requireSelection();
            return builder;
        }
        
        void themeChanged() {
            themeChanged(false);
        }
        
        void themeChanged(boolean requiresSort) {
            Theme prev  = theme;
            int   index = list.indexOf(prev);
            
            if (Theme.isPreset(prev)) {
                copySelectedTheme();
                index = list.indexOf(theme);
            }
            
            theme = builder().build();
            
            assert index >= 0 : coals(prev, Theme::getName) + " in " + list;
            
            if (!Objects.equals(prev, theme)) {
                list.set(index, theme);
                
                int index0 = index;
                int index1 = index;
                
                if (requiresSort) {
                    sort();
                    index0 = 0;
                    index1 = list.size() - 1;
                }
                
                themesChanged = true;
                notify(ListDataEvent.CONTENTS_CHANGED, index0, index1);
                putSettings();
            }
        }
        
        void copySelectedTheme() {
            if (!hasSelection())
                return;
            Theme theme = this.theme;
            
            String name = theme.getName();
            if (name.matches(".* copy\\d*")) {
                name = name.substring(0, name.lastIndexOf(" copy"));
            }
            
            name = getUniqueName(null, name + " copy");
            
            theme = builder().setName(name).build();
            add(theme);
            setSelectedItem(theme, false);
        }
        
        void deleteSelectedTheme() {
            if (!hasSelection())
                return;
            Theme theme = this.theme;
            int   index = list.indexOf(theme);
            
            list.remove(index);
            notify(ListDataEvent.INTERVAL_REMOVED, index, index);
            
            Theme sel = list.get((index == list.size()) ? (list.size() - 1) : index);
            setSelectedItem(sel, true);
        }
        
        void renameSelectedTheme(String name) {
            Objects.requireNonNull(name, "name");
            if (!hasSelection())
                return;
            assert !Theme.isPreset(theme) : theme;
            
            String prev = theme.getName();
            
            if (isDuplicateName(prev, name)) {
                Matcher m = Pattern.compile("(.+)\\d+").matcher(name);
                
                if (m.matches()) {
                    name = m.group(1);
                }
                
                name = getUniqueName(prev, name);
            }
            
            builder().setName(name);
            themeChanged(true);
        }
        
        void clear() {
            if (!list.isEmpty()) {
                int last = list.size() - 1;
                list.clear();
                notify(ListDataEvent.INTERVAL_REMOVED, 0, last);
            }
        }
        
        void add(Theme theme) {
            Objects.requireNonNull(theme, "theme");
            
            for (int i = 0; i < list.size(); ++i) {
                Theme other = list.get(i);
                int   comp  = other.getName().compareTo(theme.getName());
                if (comp > 0) {
                    list.add(i, theme);
                    notify(ListDataEvent.INTERVAL_ADDED, i, i);
                    return;
                }
            }
            
            list.add(theme);
            int last = list.size() - 1;
            notify(ListDataEvent.INTERVAL_ADDED, last, last);
        }
        
        void addAll(Collection<? extends Theme> all) {
            list.addAll(all);
            sort();
            notify(ListDataEvent.CONTENTS_CHANGED, 0, list.size() - 1);
        }
        
        private void sort() {
            list.sort(Comparator.comparing(Theme::getName));
        }
        
        void set(int index, Theme theme) {
            Objects.requireNonNull(theme, "theme");
            
            if (index == list.indexOf(this.theme)) {
                this.theme = theme;
            }
            
            list.set(index, theme);
            sort();
            notify(ListDataEvent.CONTENTS_CHANGED, index, index);
        }
        
        int indexOf(Theme theme) {
            return list.indexOf(theme);
        }
        
        String getUniqueName(String prev, String name) {
            Objects.requireNonNull(name, "name");
            
            int suffix = 0;
            String actual;
            do {
                actual = (suffix > 0) ? (name + suffix) : name;
                ++suffix;
            } while (isDuplicateName(prev, actual));
            
            return actual;
        }
        
        boolean isDuplicateName(String prev, String name) {
            for (int i = 0; i < list.size(); ++i) {
                String other = list.get(i).getName();
                if (!Objects.equals(prev, other) && name.equals(other))
                    return true;
            }
            return false;
        }
        
        boolean hasSelection() {
            return this.theme != null;
        }
        
        @Override
        public Theme getSelectedItem() {
            return this.theme;
        }
        
        @Override
        public void setSelectedItem(Object selectedItem) {
            setSelectedItem((Theme) selectedItem, true);
        }
        
        void setSelectedItem(Theme selectedTheme, boolean updateEditorComponents) {
            Theme previousTheme = this.theme;
            this.theme          = selectedTheme;
            this.builder        = (selectedTheme == null) ? null : new Theme.FreeBuilder(selectedTheme);
            
            if (!Objects.equals(previousTheme, selectedTheme)) {
                notify(ListDataEvent.CONTENTS_CHANGED, 0, list.size() - 1);
                putSettings();
            }
            
            if (updateEditorComponents) {
                ThemeEditor.this.updateEditorComponents();
            }
        }
        
        @Override
        public int getSize() {
            return list.size();
        }
        
        @Override
        public Theme getElementAt(int index) {
            return list.get(index);
        }
        
        @Override
        public void addListDataListener(ListDataListener listener) {
            listeners = Misc.append(listeners, Objects.requireNonNull(listener, "listener"));
        }
        
        @Override
        public void removeListDataListener(ListDataListener listener) {
            listeners = Misc.remove(listeners, Objects.requireNonNull(listener, "listener"));
        }
        
        public ListDataListener[] getListDataListeners() {
            return listeners.clone();
        }
        
        private void notify(int type, int index0, int index1) {
            ListDataEvent e = new ListDataEvent(this, type, index0, index1);
            for (ListDataListener listener : listeners) {
                switch (type) {
                    case ListDataEvent.CONTENTS_CHANGED:
                        listener.contentsChanged(e);
                        break;
                    case ListDataEvent.INTERVAL_ADDED:
                        listener.intervalAdded(e);
                        break;
                    case ListDataEvent.INTERVAL_REMOVED:
                        listener.intervalRemoved(e);
                        break;
                    default:
                        assert false : type;
                        break;
                }
            }
        }
    }
    
    private static int getMinorMargin() {
        return Settings.getOrDefault(Setting.MINOR_MARGIN);
    }
    
    private static int getMajorMargin() {
        return Settings.getOrDefault(Setting.MAJOR_MARGIN);
    }
    
    private static final class StyleCellRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                                                      Object   value,
                                                      int      index,
                                                      boolean  isSelected,
                                                      boolean  cellHasFocus) {
            Theme.Key key  = (Theme.Key) value;
            String    name = (key == null) ? "" : VISIBLE_SETTING_NAMES.getOrDefault(key, key.name());
            Component comp = super.getListCellRendererComponent(list, name, index, isSelected, cellHasFocus);
            if (key != null && comp instanceof JComponent) {
                ((JComponent) comp).setToolTipText(key.getDescription());
            }
            return comp;
        }
    }
    
    private static final class ThemeCellRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                                                      Object   value,
                                                      int      index,
                                                      boolean  isSelected,
                                                      boolean  cellHasFocus) {
            String name = (value == null) ? "" : ((Theme) value).getName();
            return super.getListCellRendererComponent(list, name, index, isSelected, cellHasFocus);
        }
    }
    
    private static final class StyleList extends JList<Theme.Key> {
        StyleList() {
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            setCellRenderer(new StyleCellRenderer());
            
            DefaultListModel<Theme.Key> model = new DefaultListModel<>();
            setModel(model);
            
            for (Theme.Key key : Theme.Key.values()) {
                model.addElement(key);
            }
        }
    }
    
    private static final Map<Theme.Key, String> VISIBLE_SETTING_NAMES =
        MapOf(Theme.Key.BASIC,           "Basic Text",
              Theme.Key.NOTE,            "Notes",
              Theme.Key.COMMAND,         "Command Text",
              Theme.Key.NUMBER,          "Numbers and Variables",
              Theme.Key.FUNCTION,        "Function Names",
              Theme.Key.BRACKET,         "Brackets and Parentheses",
              Theme.Key.LITERAL,         "String Literals",
              Theme.Key.BUTTON,          "Button Text",
              Theme.Key.BUTTON_SELECTED, "Button Selected Text",
              Theme.Key.INTERNAL_ERROR,  "Internal Error Warnings");
}