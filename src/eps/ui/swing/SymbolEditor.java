/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.eval.*;
import eps.eval.Operator.*;
import eps.eval.UnaryOp.*;
import eps.app.*;
import eps.util.*;
import eps.job.*;
import eps.ui.swing.ParameterEditor.*;
import static eps.app.App.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import java.util.*;
import java.util.List;
import java.math.*;

// TODO: figure out custom->alias conversion when delegate is not
//       estranged for some reason. (seems fine...?)
//
//       echo format...?
//
class SymbolEditor extends JPanel implements AutoCloseable {
    private final SwingSymbolsWindow window;
    
    private ChangeListener[] listeners = new ChangeListener[0];
    
    private final ChangeEvent changeEvent = new ChangeEvent(this);
    
    private Symbol sym;
    
    private final JComboBox<Type> typeSelector;
    
    private final JPanel cardPane;
    
    private final List<Card> cards;
    
    SymbolEditor() {
        this(null);
    }
    
    SymbolEditor(SwingSymbolsWindow window) {
        this.window = window;
//        this.window = Objects.requireNonNull(window, "window");
        
        setOpaque(false);
        
        cardPane = new JPanel(new CardLayout());
        cardPane.setOpaque(false);
        
        cards = ListOf(new NullPanel(), new IntrinsicPanel(), new AliasPanel(), new CustomPanel());
        cards.forEach(c -> cardPane.add(c.key, c));
        
        typeSelector = new JComboBox<>(Type.values());
        
        typeSelector.setRenderer(new SymbolTypeRenderer(typeSelector.getRenderer()));
        typeSelector.setEditable(false);
        
        typeSelector.addItemListener((ItemSelectedListener) e -> {
            Type type = (Type) typeSelector.getSelectedItem();
            Log.note(getClass(), "itemSelected", "selected ", type);
            if (type == null) {
                ((CardLayout) cardPane.getLayout()).show(cardPane, Type.NONE.key());
                return;
            }
            
            boolean isValid = (sym == null) ? (type == Type.NONE)
                            : (sym.isIntrinsic() == (type == Type.INTRINSIC));
            if (!isValid) {
                Log.caught(SymbolEditor.class, "invalid state",
                           new AssertionError(f("type = %s, sym = %s", type, sym)), false);
                return;
            }
            
            Card sel = getSelectedCard();
            Card vis = getVisibleCard();
            
            Symbol before = this.sym;
            
            if (vis != null) {
                vis.leaving(false);
                before = replnull(vis.getSymbol(), before);
            }
            
            boolean notify = sel.selected(before);
            Symbol  after  = sel.getSymbol();
            
            ((CardLayout) cardPane.getLayout()).show(cardPane, type.key());
            
            if (notify) {
                symbolChanged(after);
            }
        });
        
        setLayout(new BorderLayout());
        add(typeSelector, BorderLayout.NORTH);
        add(cardPane, BorderLayout.CENTER);
        
        setSymbol(null);
        
        setToolTips();
    }
    
    private void setToolTips() {
        typeSelector.setToolTipText(
              "<html><body>"
            + "<p>Chooses the type of the symbol currently being edited.</p>"
            + "<ul>"
            + "<li><p>An intrinsic symbol is always present and cannot be edited.</p></li>"
            + "<li><p>An alias simply defines a second name for an already-existing symbol.</p></li>"
            + "<li><p>A custom symbol has any expression for its definition.</p></li>"
            + "</ul>"
            + "</body></html>");
    }
    
    protected boolean isCompact() {
        return false;
    }
    
    @Override
    public void close() {
        cards.forEach(Card::close);
    }
    
    Card getVisibleCard() {
        for (Card c : cards)
            if (c.isVisible())
                return c;
        return null;
    }
    
    Card getSelectedCard() {
        Type sel = (Type) typeSelector.getSelectedItem();
        for (Card c : cards)
            if (c.type == sel)
                return c;
        throw new AssertionError(sel);
    }
    
    private boolean contains(JComboBox<?> box, Object obj) {
        int count = box.getItemCount();
        for (int i = 0; i < count; ++i)
            if (Objects.equals(box.getItemAt(i), obj))
                return true;
        return false;
    }
    
    private void removeAll(JComboBox<?> box, Object obj) {
        while (contains(box, obj))
            box.removeItem(obj);
    }
    
    void setSymbol(Symbol sym) {
        typeSelector.setSelectedIndex(-1);
        cards.forEach(Card::reset);
        
        this.sym = sym;
        Log.note(SymbolEditor.class, "setSymbol", "sym set to ", sym);
        
        Type t = Type.valueOf(sym);
        
        removeAll(typeSelector, Type.INTRINSIC);
        removeAll(typeSelector, Type.NONE);
        if (!contains(typeSelector, t))
            typeSelector.addItem(t);
        
        typeSelector.setSelectedItem(t);
        setEnabled(t != Type.NONE && t != Type.INTRINSIC);
    }
    
    Symbol getSymbol() {
        Symbol sel = getSelectedCard().getSymbol();
        return (sel != null) ? sel : sym;
    }
    
    Symbol getOriginalSymbol() {
        return sym;
    }
    
    public void addChangeListener(ChangeListener cl) {
        listeners = Misc.append(listeners, Objects.requireNonNull(cl, "cl"));
    }
    
    public void removeChangeListener(ChangeListener cl) {
        listeners = Misc.remove(listeners, Objects.requireNonNull(cl, "cl"));
    }
    
    private void symbolChanged(Symbol after) {
        if (window != null) {
            window.symbolChanged(this.sym, after);
        }
        for (ChangeListener cl : listeners) {
            cl.stateChanged(changeEvent);
        }
    }
    
    private boolean isValidName(Symbol sym, String name) {
        return !name.isEmpty() && !Symbol.isIntrinsic(name) && !isDuplicateName(sym, name);
    }
    
    protected boolean isDuplicateName(Symbol sym, String name) {
        if (window != null) {
            return window.isDuplicateName(sym, name);
        }
        return Symbol.isDuplicate(getSymbols(), sym, name);
    }
    
    private boolean validateName(Symbol sym, String left, String right) {
        boolean valid  = true;
        boolean silent = true;
        if (left != null && !isValidName(sym, left)) {
            valid   = false;
            silent &= isEmpty(left);
        }
        if (right != null && !isValidName(sym, right)) {
            valid   = false;
            silent &= isEmpty(right);
        }
        if (!silent) {
            SwingMisc.beep();
        }
        return valid;
    }
    
    private boolean validateName_old(Symbol sym, String left, String right) {
        boolean valid = (left  == null || isValidName(sym, left))
                     && (right == null || isValidName(sym, right));
        if (!valid) {
//            Log.note(getClass(), "validateName", "left = ", left, ", right = ", right);
            if (!isEmpty(left) || !isEmpty(right)) {
                SwingMisc.beep();
            }
        }
        return valid;
    }
    
    protected Iterable<? extends Symbol> getSymbols() {
        if (window != null) {
            return window.getSymbols();
        }
        return Collections.emptyList();
    }
    
    @Override
    public void setEnabled(boolean e) {
        super.setEnabled(e);
        typeSelector.setEnabled(e);
    }
    
    void cancelling() {
        leavingAll(true);
    }
    
    void applying() {
        leavingAll(false);
    }
    
    private void leavingAll(boolean cancel) {
        cards.forEach(card -> card.leaving(cancel));
    }
    
    private void setNameToolTips(Symbol sym, JComponent left, JComponent right) {
        final String uniqueNameToolTip =
              "Two different symbols can share the same name as long as they're of "
            + "different types. For example, unary negation (-1) and binary subtraction "
            + "(3-2) share the same name but are of different types.";
        if (sym != null && sym.isSpanOp()) {
            boolean isLeft = false;
            do {
                String text =
                      "<html><body style='width:500'>"
                    + "Sets the " + (isLeft ? "left" : "right") + " name which this "
                    + "symbol is referenced by in an expression. For example if the "
                    + "symbol is a parenthesis, ( could be the left name and ) could "
                    + "be the right name."
                    + "<br/><br/>"
                    + "The left and right name can be identical, for example as is "
                    + "the case for an absolute value operator |x|."
                    + "<br/><br/>"
                    + uniqueNameToolTip
                    + "</body></html>";
                (isLeft ? left : right).setToolTipText(text);
            } while (isLeft = !isLeft);
        } else {
            left.setToolTipText(
                  "<html><body style='width:500'>"
                + "Sets the name which expressions use to refer to this symbol."
                + "<br/><br/>"
                + uniqueNameToolTip
                + "</body></html>");
        }
    }
    
    private void setFunctionLikeToolTip(JComponent comp) {
        comp.setToolTipText(
              "<html><body style='width:500'>"
            + "Sets whether or not this unary operator is a function."
            + "<br/><br/>"
            + "Function-like unary operators always bind to parenthesized expressions. "
            + "For example, the expression sin(x)! will always be grouped as (sin x)!, "
            + "even if the ! operator has a higher precedence than the sin function."
            + "<br/><br/>"
            + "This option also has an effect on the formatted display output."
            + "</body></html>");
    }
    
    private class Card extends JPanel implements AutoCloseable {
        private final Type   type;
        private final String key;
        
        protected Symbol sym;
        
        Card(Type type) {
            this.type = Objects.requireNonNull(type, "type");
            this.key  = type.key();
            
            setOpaque(false);
        }
        
        boolean isSymbol(Symbol sym) {
            return type.isSymbol(sym);
        }
        
        Symbol getSymbol() {
            return sym;
        }
        
        void reset() {
            sym = null;
        }
        
        void setSymbol(Symbol sym) {
            if (this.sym != sym) {
                this.sym = sym;
                
                if (active()) {
                    symbolChanged(sym);
                }
            }
        }
        
        boolean active() {
            return this == getSelectedCard();
        }
        
        boolean selected(Symbol sym) {
            this.sym = sym;
            return false;
        }
        
        void leaving(boolean cancel) {
        }
        
        @Override
        public void close() {
        }
    }
    
    private final class NullPanel extends Card {
        NullPanel() {
            super(Type.NONE);
        }
    }
    
    private final class IntrinsicPanel extends Card {
        IntrinsicPanel() {
            super(Type.INTRINSIC);
        }
    }
    
    private static final class Delegate {
        final Symbol  sym;
        final boolean estranged;
        private Delegate(Symbol sym, boolean estranged) {
            this.sym       = sym;
            this.estranged = estranged;
        }
        private Delegate(Symbol sym) {
            this.sym  = sym;
            estranged = false;
        }
        @Override
        public int hashCode() {
            return Objects.hashCode(sym);
        }
        @Override
        public boolean equals(Object obj) {
            return obj instanceof Delegate && Objects.equals(sym, ((Delegate) obj).sym);
        }
        private static final ToString<Delegate> TO_STRING =
            ToString.builder(Delegate.class)
                    .add("sym",       d -> d.sym)
                    .add("estranged", d -> d.estranged)
                    .build();
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
    }
    
    private static final class DelegateRenderer implements ListCellRenderer<Object> {
        private final ListCellRenderer<?> renderer;
        
        private DelegateRenderer(ListCellRenderer<?> renderer) {
            this.renderer = Objects.requireNonNull(renderer, "renderer");
        }
        
        @Override
        @SuppressWarnings("unchecked")
        public Component getListCellRendererComponent(JList<?> list,
                                                      Object   value,
                                                      int      index,
                                                      boolean  isSelected,
                                                      boolean  cellHasFocus) {
            Delegate del = (Delegate) value;
            String  text = del.sym.getName();
            if (del.estranged)
                text += " (estranged)";
            return ((ListCellRenderer<Object>) renderer).getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
        }
    }
    
    private final class AliasPanel extends Card implements SimpleDocumentListener, ItemSelectedListener {
        private final JComboBox<Delegate> delegates;
        
        private final JTextField name;
        private final JTextField rightName;
        
        private final JCheckBox functionLike;
        
        AliasPanel() {
            super(Type.ALIAS);
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            
            delegates = new JComboBox<>();
            delegates.setRenderer(new DelegateRenderer(delegates.getRenderer()));
            delegates.addItemListener(this);
            
            name = new SafeChangeTextField(10);
            rightName = new SafeChangeTextField(10);
            
            DocumentPropertyChangeListener.add(name, this, NoSpaceFilter.INSTANCE);
            DocumentPropertyChangeListener.add(rightName, this, NoSpaceFilter.INSTANCE);
            
            functionLike = new JCheckBox("Function-Like");
            functionLike.addActionListener(e -> functionLikeChanged());
            
            add(createBorderPanel(delegates, "Delegate"));
            add(createBorderPanel(name,      "Left Name"));
            add(createBorderPanel(rightName, "Right Name"));
            add(functionLike);
            
            for (Component comp : getComponents()) {
                ((JComponent) comp).setAlignmentX(Component.LEFT_ALIGNMENT);
            }
            
            setToolTips();
        }
        
        private void setToolTips() {
            setNameToolTips(SpanOp.PARENTHESIS, name, rightName);
            setFunctionLikeToolTip(functionLike);
            delegates.setToolTipText(
                  "<html><body style='width:500'>"
                + "Sets the symbol which this alias delegates to."
                + "<br/><br/>"
                + "If the view shows that the delegate is estranged, this means "
                + "the delegate does not otherwise exist in the symbol store. This "
                + "can happen if, for example, an alias is created for a symbol which "
                + "is later deleted. An estranged delegate will continue to exist as "
                + "long as the alias for it exists, but can only be referenced using "
                + "the name of the alias."
                + "<br/><br/>"
                + "If an estranged delegate was a custom symbol, the source code can "
                + "be retrieved by switching the type of the symbol to 'Custom'."
                + "</body></html>");
        }
        
        @Override
        boolean selected(Symbol sym) {
            boolean isAlias;
            if (sym.isAlias()) {
                isAlias = true;
            } else {
                isAlias = false;
                Symbol fwd = Trees.getAliasForward(sym);
                sym = ((fwd != null) ? fwd : sym).createAlias(sym);
            }
            
            super.selected(sym);
            setNameToolTips(sym, name, rightName);
            
            Symbol sym0 = sym;
            SwingMisc.unlisten(delegates, ItemListener.class,
                () -> {
                    delegates.removeAllItems();
                    boolean estranged = true;
                    
                    for (Symbol e : getSymbols()) {
                        if (e != sym0 && e.getSymbolKind() == sym0.getSymbolKind()) {
                            delegates.addItem(new Delegate(e, false));
                            
                            if (e == sym0.getDelegate()) {
                                estranged = false;
                            }
                        }
                    }
                    
                    if (estranged) {
                        delegates.insertItemAt(new Delegate(sym0.getDelegate(), true), 0);
                    }
                    
                    delegates.setSelectedItem(new Delegate(sym0.getDelegate()));
                }
            );
            
            if (sym.isSpanOp()) {
                rightName.getParent().setVisible(true);
                SwingMisc.setTitle(name.getParent(), "Left Name");
                
                SpanOp span = (SpanOp) sym;
                name.setText(span.getLeftName());
                rightName.setText(span.getRightName());
                
            } else {
                rightName.getParent().setVisible(false);
                SwingMisc.setTitle(name.getParent(), "Name");
                
                name.setText(sym.getName());
            }
            
            if (sym.isUnaryOp()) {
                functionLike.setVisible(true);
                functionLike.setSelected(((UnaryOp) sym).isFunctionLike());
            } else {
                functionLike.setVisible(false);
            }
            
            return !isAlias;
        }
        
        @Override
        public void itemSelected(ItemEvent e) {
            Symbol sel = ((Delegate) delegates.getSelectedItem()).sym;
            
            Symbol sym;
            
            if (sel.isSpanOp()) {
                sym = ((SpanOp) sel).createAlias(name.getText(), rightName.getText());
            } else if (sel.isUnaryOp() && functionLike.isVisible()) {
                sym = ((UnaryOp) sel).createAlias(name.getText(), functionLike.isSelected());
            } else {
                sym = sel.createAlias(name.getText());
            }
            
            setSymbol(sym);
        }
        
        private void functionLikeChanged() {
            if (sym.isUnaryOp()) {
                UnaryOp del = (UnaryOp) sym.getDelegate();
                
                setSymbol(del.createAlias(sym.getName(), functionLike.isSelected()));
                
            } else {
                assert false : sym;
            }
        }
        
        @Override
        public void documentChanged(DocumentEvent e) {
            nameChanged();
        }
        
        private void nameChanged() {
            setSymbolName(name.getText(), rightName.getText());
        }
        
        private void setSymbolName(String left, String right) {
            if (sym.isSpanOp()) {
                if (validateName(sym, left, right)) {
                    SpanOp span = (SpanOp) sym;
                    
                    if (!span.getLeftName().equals(left) || !span.getRightName().equals(right)) {
                        setSymbol(((SpanOp) span.getDelegate()).createAlias(left, right));
                    }
                }
            } else {
                String name = left;
                if (validateName(sym, name, null) && !sym.getName().equals(name)) {
                    setSymbol(sym.getDelegate().createAlias(name));
                }
            }
        }
        
        @Override
        public void changedUpdate(DocumentEvent e) {
        }
    }
    
    private final class CustomPanel extends Card implements SimpleDocumentListener, ChangeListener {
        private final MultilineEditor editor;
        
        private final JSplitPane splitPane;
        private final JPanel topPane;
        private final JPanel bottomPane;
        
        private final ParameterEditor params;
        
        private final JTextField name;
        private final JTextField rightName;
        
        private String nameText;
        private String rightNameText;
        
        private final PrecedenceComboBox precedence;
        private final JComboBox<Enum<?>> associativity;
        
        private final JCheckBox functionLike;
        private final JCheckBox literal;
        
        private volatile Builder builder = null;
        private final     Object monitor = new Object();
        
        CustomPanel() {
            super(Type.CUSTOM);
            
            topPane = new JPanel(new BorderLayout());
            topPane.setOpaque(false);
            
            int gap = app().getSetting(Setting.MINOR_MARGIN);
            bottomPane = new JPanel(new BorderLayout(gap, gap));
            bottomPane.setOpaque(false);
            
            splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
            splitPane.setContinuousLayout(true);
            splitPane.setResizeWeight(0);
            splitPane.setTopComponent(topPane);
            splitPane.setBottomComponent(bottomPane);
            
            setLayout(new BorderLayout());
            
            params = new ParameterEditor(isCompact() ? 2 : 6);
            params.setOpaque(false);
            params.addChangeListener(this);
            
            editor = new MultilineEditor();
//            editor.setPreferredSize(new Dimension(300, 300));
            JScrollPane editorScroll = editor.getScrollPane();
            
            DocumentPropertyChangeListener.add(editor, this);
            
            name = new SafeChangeTextField(10);
            rightName = new SafeChangeTextField(10);
            
            DocumentPropertyChangeListener.add(name, this::nameChanged, NoSpaceFilter.INSTANCE);
            DocumentPropertyChangeListener.add(rightName, this::nameChanged, NoSpaceFilter.INSTANCE);
            
            precedence = new PrecedenceComboBox();
            precedence.addChangeListener(this::precedenceChanged);
            
            associativity = new JComboBox<>();
            associativity.setEditable(false);
            associativity.setRenderer(new AssociativityCellRenderer(associativity.getRenderer()));
            associativity.addItemListener((ItemSelectedListener) this::associativityChanged);
            
            functionLike = new JCheckBox("Function-Like");
            functionLike.addActionListener(this::functionLikeChanged);
            
            literal = new JCheckBox("String Literal");
            literal.addActionListener(this::literalChanged);
            
            JPanel side = new JPanel();
            side.setOpaque(false);
            side.setLayout(new BoxLayout(side, BoxLayout.Y_AXIS));
            
            side.add(createBorderPanel(name, "Left Name"));
            side.add(createBorderPanel(rightName, "Right Name"));
            side.add(createBorderPanel(precedence, "Precedence"));
            side.add(createBorderPanel(associativity, "Associativity"));
            side.add(functionLike);
            side.add(literal);
            
            for (Component comp : side.getComponents()) {
                ((JComponent) comp).setAlignmentX(Component.LEFT_ALIGNMENT);
            }
            
            topPane.add(createBorderPanel(params, "Parameters"), BorderLayout.CENTER);
            
            bottomPane.add(side, BorderLayout.WEST);
            bottomPane.add(editorScroll, BorderLayout.CENTER);
            
            add(splitPane, BorderLayout.CENTER);
            setToolTips();
            
            setToMaximumSizeConfiguration();
            Dimension sidePref = side.getPreferredSize();
            side.setPreferredSize(sidePref);
            editorScroll.setPreferredSize(isCompact() ? sidePref : new Dimension(2*sidePref.width, sidePref.height));
        }
        
        private void setToMaximumSizeConfiguration() {
            // currently unary operator
            name.getParent().setVisible(true);
            rightName.getParent().setVisible(false);
            precedence.setVisible(true);
            associativity.setVisible(true);
            functionLike.setVisible(true);
        }
        
        private void setToolTips() {
            setToolTips(SpanOp.PARENTHESIS);
            setFunctionLikeToolTip(functionLike);
            editor.setToolTipText(
                  "<html><body style='width:500'>"
                + "Specifies the expression which this symbol evaluates."
                + "<br/><br/>"
                + "If the symbol is an operator, then the expression has access to "
                + "any parameters defined in the parameter editor."
                + "</body></html>");
            literal.setToolTipText(
                  "<html><body style='width:500'>"
                + "Specifies whether or not the argument to this span operator is parsed "
                + "and evaluated as an exact string of characters rather than as an expression."
                + "</body></html>");
            precedence.setToolTipText(
                  "<html><body style='width:500'>"
                + "Specifies the precedence for this operator. Operators with higher "
                + "precdence are evaluated before operators with lower precedence when "
                + "no ordering is specified with explicit parentheses. For example, by default "
                + "the expression a+b*c is evaluated first by multiplying b and c, then "
                + "by adding the result of that to a. This is because multiplication has a "
                + "higher precedence than addition by default."
                + "<br/><br/>"
                + "Precedence is specified by any integer in the range -2,147,483,648 to "
                + "2,147,483,647, both inclusive. Higher numbers specify higher precedence."
                + "</body></html>");
        }
        
        private void setToolTips(Symbol sym) {
            setNameToolTips(sym, name, rightName);
            if (sym != null && sym.isUnaryOp()) {
                associativity.setToolTipText(
                      "<html><body style='width:500'>"
                    + "Specifies whether this unary operator is a prefix or suffix."
                    + "<br/><br/>"
                    + "For example, negation (e.g. -1) is a prefix unary operator and "
                    + "factorial (e.g. 6!) is a suffix unary operator. Epsilon also "
                    + "treats functions (e.g. sin(x)) as unary prefix operators with a "
                    + "few special cases."
                    + "</body></html>");
            } else {
                associativity.setToolTipText(
                      "<html><body style='width:500'>"
                    + "Specifies the assoviativity for this operator. Associativity specifies "
                    + "how multiple operators of the same type are group when no grouping is "
                    + "provided with explicit parentheses. For example, by default the expression "
                    + "a*b*c is grouped as (a*b)*c because multiplication is left-to-right "
                    + "associative. In contrast, by default a^b^c is grouped as a^(b^c) because "
                    + "exponentiation is right-to-left associative by default."
                    + "</body></html>");
            }
        }
        
        private static final int REBUILD   = 0b001;
        private static final int FROM_TREE = 0b010;
        private static final int CONFIGURE = 0b100;
        
        private int setSource(Symbol sym) {
            editor.getStyledDocument().removeDocumentListener(this);
            try {
                return setSource(sym, false);
            } finally {
                editor.getStyledDocument().addDocumentListener(this);
                editor.clearEdits();
            }
        }
        
        private int setSource(Symbol sym, boolean recursive) {
            try {
                String src = Trees.getBody(sym);
                editor.setText(src);
                return FROM_TREE;
            } catch (ClassCastException x) {
                Log.caught(CustomPanel.class, "setting editor source", x, true);
                
                if (sym.isAlias()) {
                    Symbol del = sym.getDelegate();
                    if (setSource(del, true) == FROM_TREE) {
                        return CONFIGURE;
                    } else {
                        editor.setText(synthesizeBody(del));
                    }
                } else {
                    if (!recursive) {
                        editor.setText("");
                    }
                }
                
                return REBUILD;
            }
        }
        
        private SpanOp getBracket(Setting<CodePoint> set) {
            App       app  = app();
            CodePoint name = app.getSetting(set);
            if (name != null) {
                SpanOp op = app.getSymbols().get(name.toString(), SpanOp.class);
                if (op != null) {
                    boolean isValid = (set == Setting.LEFT_PARENTHESIS  && op.isLeft())
                                   || (set == Setting.RIGHT_PARENTHESIS && op.isRight());
                    if (isValid) {
                        return op.getParent();
                    }
                }
            }
            return SpanOp.PARENTHESIS;
        }
        
        private String getLeftBracket() {
            return getBracket(Setting.LEFT_PARENTHESIS).getLeftName();
        }
        
        private String getRightBracket() {
            return getBracket(Setting.RIGHT_PARENTHESIS).getRightName();
        }
        
        private String getComma() {
            NaryOp comma = NaryOp.TUPLE;
            
            Symbols.Group group = app().getSymbols().get(",");
            if (group != null) {
                NaryOp nary = group.get(NaryOp.class);
                if (nary != null) {
                    if (nary.getDelegate() == comma) {
                        comma = nary;
                    }
                }
            }
            
            return comma.getName();
        }
        
        private String synthesizeBody(Symbol sym) {
            if (sym.isVariable()) {
                return sym.getName();
            }
            ParamGroup params = this.params.getParameters();
            switch (sym.getSymbolKind()) {
                case UNARY_OP: {
                    UnaryOp unary = (UnaryOp) sym;
                    String  name  = sym.getName();
                    
                    StringBuilder b = new StringBuilder(24);
                    
                    if (unary.isPrefix()) {
                        b.append(name).append(' ');
                    }
                    
                    String lb = getLeftBracket();
                    String rb = getRightBracket();
                    
                    int count = params.count();
                    if (count == 1) {
                        b.append(lb)
                         .append(' ')
                         .append(params.get(0))
                         .append(' ')
                         .append(rb);
                    } else {
                        String comma = getComma();
                        
                        b.append(lb).append(' ');
                        
                        for (int i = 0; i < count; ++i) {
                            if (i > 0) {
                                b.append(' ')
                                 .append(comma)
                                 .append(' ');
                            }
                            b.append(lb)
                             .append(' ')
                             .append(params.get(i))
                             .append(' ')
                             .append(rb);
                        }
                        
                        b.append(' ').append(rb);
                    }
                    
                    if (unary.isSuffix()) {
                        b.append(' ').append(name);
                    }
                    
                    return b.toString();
                }
                case BINARY_OP: {
                    String name = sym.getName();
                    Param  lhs  = params.get(0);
                    Param  rhs  = params.get(1);
                    String lb   = getLeftBracket();
                    String rb   = getRightBracket();
                    return new StringBuilder(48).append(lb)
                                                .append(' ')
                                                .append(lhs)
                                                .append(' ')
                                                .append(rb)
                                                .append(' ')
                                                .append(name)
                                                .append(' ')
                                                .append(lb)
                                                .append(' ')
                                                .append(rhs)
                                                .append(' ')
                                                .append(rb).toString();
                }
                case SPAN_OP: {
                    SpanOp span = (SpanOp) sym;
                    Param  arg0 = params.get(0);
                    return new StringBuilder(32).append(span.getLeftName())
                                                .append(' ')
                                                .append(getLeftBracket())
                                                .append(' ')
                                                .append(arg0)
                                                .append(' ')
                                                .append(getRightBracket())
                                                .append(' ')
                                                .append(span.getRightName()).toString();
                }
                case NARY_OP: {
                    String name    = sym.getName();
                    Param  arg0    = params.get(0);
                    String ellipse = SpecialSyms.ELLIPSE.getName();
                    String lb      = getLeftBracket();
                    String rb      = getRightBracket();
                    return new StringBuilder(48).append(lb)
                                                .append(' ')
                                                .append(arg0)
                                                .append(' ')
                                                .append(rb)
                                                .append(' ')
                                                .append(name)
                                                .append(' ')
                                                .append(lb)
                                                .append(' ')
                                                .append(ellipse)
                                                .append(' ')
                                                .append(rb).toString();
                }
                default: {
                    assert false : f("%s: %s", sym.getSymbolKind(), sym);
                    break;
                }
            }
            return "";
        }
        
        @Override
        boolean selected(Symbol sym) {
            if (sym == this.sym) {
                return false;
            }
            synchronized (monitor) {
                if (builder != null) {
                    builder.cancel();
                }
            }
            
            super.selected(sym);
            setToolTips(sym);
            
            boolean didChange = false;
            
            removeAll();
            if (sym.isVariable()) {
                splitPane.setTopComponent(null);
                splitPane.setBottomComponent(null);
                add(bottomPane, BorderLayout.CENTER);
            } else {
                splitPane.setTopComponent(topPane);
                splitPane.setBottomComponent(bottomPane);
                add(splitPane, BorderLayout.CENTER);
            }
            
            if (sym.isSpanOp()) {
                rightName.getParent().setVisible(true);
                SwingMisc.setTitle(name.getParent(), "Left Name");
                
                SpanOp span = (SpanOp) sym;
                
                nameText      = span.getLeftName();
                rightNameText = span.getRightName();
                name.setText(nameText);
                rightName.setText(rightNameText);
                
                literal.setVisible(true);
                literal.setSelected(span.isLiteral());
                
            } else {
                rightName.getParent().setVisible(false);
                SwingMisc.setTitle(name.getParent(), "Name");
                
                nameText      = sym.getName();
                rightNameText = null;
                name.setText(nameText);
                
                literal.setVisible(false);
            }
            
            if (sym.isOperator() && !sym.isSpanOp()) {
                precedence.getParent().setVisible(true);
                SwingMisc.unlisten(precedence, ChangeListener.class,
                    () -> precedence.setSelectedItem(((Operator) sym).getPrecedence())
                );
            } else {
                precedence.getParent().setVisible(false);
            }
            
            if (sym.isOperator() && !sym.isNaryOp() && !sym.isSpanOp()) {
                associativity.getParent().setVisible(true);
                
                SwingMisc.unlisten(associativity, ItemListener.class,
                    () -> {
                        associativity.removeAllItems();
                        if (sym.isUnaryOp()) {
                            associativity.addItem(Affix.PREFIX);
                            associativity.addItem(Affix.SUFFIX);
                            associativity.setSelectedItem(((UnaryOp) sym).getAffix());
                            SwingMisc.setTitle(associativity.getParent(), "Affix");
                        } else {
                            associativity.addItem(Associativity.LEFT_TO_RIGHT);
                            associativity.addItem(Associativity.RIGHT_TO_LEFT);
                            associativity.setSelectedItem(((Operator) sym).getAssociativity());
                            SwingMisc.setTitle(associativity.getParent(), "Associativity");
                        }
                    }
                );
            } else {
                associativity.getParent().setVisible(false);
            }
            
            if (sym.isUnaryOp()) {
                functionLike.setVisible(true);
                functionLike.setSelected(((UnaryOp) sym).isFunctionLike());
            } else {
                functionLike.setVisible(false);
            }
            
            if (sym.isOperator()) {
                params.removeChangeListener(this);
                try {
                    if (params.setParameters((Operator) sym)) {
                        didChange = true;
                        // rebuild?
                    }
                } finally {
                    params.addChangeListener(this);
                }
            }
            
            switch (setSource(sym)) {
                case FROM_TREE:
                    break;
                case CONFIGURE:
                    Symbol sym2 = sym.getDelegate().configure(createBuilder());
                    super.selected(sym2);
                    didChange = true;
                    break;
                case REBUILD:
                    rebuild();
                    didChange = false; // notify later, when the build is done
                    break;
            }
            
            return didChange;
        }
        
        @Override
        public void documentChanged(DocumentEvent e) {
            changed(true);
        }
        
        private void nameChanged(DocumentEvent e) {
            Symbol sym       = this.sym;
            String leftName  = this.name.getText();
            String rightName = sym.isSpanOp() ? this.rightName.getText() : null;
            
            if (validateName(sym, leftName, rightName)) {
                this.nameText      = leftName;
                this.rightNameText = rightName;
                
                changed(false);
            }
        }
        
        private void precedenceChanged(ChangeEvent e) {
            Integer val = precedence.getSelectedPrecedence();
            Log.low(SymbolEditor.class, "precedenceChanged", "precedence = ", val);
            
            changed(false);
        }
        
        private void associativityChanged(ItemEvent e) {
            Object val = associativity.getSelectedItem();
            if (val instanceof Associativity) {
                Log.low(SymbolEditor.class, "associativityChanged", "associativity = ", val);
                // ...
            } else if (val instanceof Affix) {
                Log.low(SymbolEditor.class, "associativityChanged", "affix = ", val);
                // ...
            } else {
                Log.low(SymbolEditor.class, "associativityChanged", "val = ", val);
            }
            
            changed(false);
        }
        
        private void functionLikeChanged(ActionEvent e) {
            boolean isFunctionLike = functionLike.isSelected();
            Log.low(SymbolEditor.class, "functionLikeChanged", "isFunctionLike = ", isFunctionLike);
            
            changed(false);
        }
        
        private void literalChanged(ActionEvent e) {
            boolean isLiteral = literal.isSelected();
            Log.low(SymbolEditor.class, "literalChanged", "isLiteral = ", isLiteral);
            
            changed(false);
        }
        
        @Override
        public void stateChanged(ChangeEvent e) {
            ParameterEditor.ParamGroup params = this.params.getParameters();
            Log.low(SymbolEditor.class, "parametersChanged", "params = ", params);
            
            changed(true);
        }
        
        private void changed(boolean rebuild) {
            if (rebuild) {
                Log.note(getClass(), "changed", "rebuilding \"", editor.getText(), "\"");
                rebuild();
            } else {
                if (sym != null) {
                    setSymbol(sym.configure(createBuilder()));
                }
            }
        }
        
        private Symbol.Builder<?, ?> createBuilder() {
            return createBuilder(sym,
                                 nameText,
                                 rightNameText,
                                 functionLike.isSelected(),
                                 literal.isSelected(),
                                 associativity.getSelectedItem(),
                                 precedence.getSelectedPrecedence());
        }
        
        private Symbol.Builder<?, ?> createBuilder(Symbol  sym,
                                                   String  name,
                                                   String  rightName,
                                                   boolean isFunctionLike,
                                                   boolean isLiteral,
                                                   Object  associativity,
                                                   Integer precedence) {
            Symbol.Builder<?, ?> b = null;
            if (sym != null) {
                b = sym.createBuilder();
                b.setEchoFormatter(sym.getEchoFormatter());
                
                if (sym.isSpanOp()) {
                    ((SpanOp.Builder) b).setLeftName(name)
                                        .setRightName(rightName)
                                        .setLiteral(isLiteral);
                } else {
                    b.setName(name);
                }
                
                if (sym.isOperator()) {
                    if (sym.isUnaryOp()) {
                        ((UnaryOp.Builder) b).setAffix((Affix) associativity)
                                             .setFunctionLike(isFunctionLike);
                        if (isFunctionLike && ((UnaryOp) sym).isPrefix()) {
                            b.setEchoFormatter(EchoFormatter.OF_FUNCTION_LIKE);
                        }
                    } else {
                        ((Operator.Builder) b).setAssociativity((Associativity) associativity);
                    }
                    if (precedence != null) {
                        ((Operator.Builder) b).setPrecedence(precedence);
                    }
                }
            }
            return b;
        }
        
        private void rebuild() {
            synchronized (monitor) {
                if (builder != null) {
                    builder.cancel();
                }
                builder = new Builder();
                app().getWorker2().enqueue(builder, true);
            }
        }
        
        @Override
        void leaving(boolean cancel) {
            super.leaving(cancel);
            
            synchronized (monitor) {
                if (builder != null) {
                    if (cancel) {
                        builder.cancel();
                        builder = null;
                    } else {
                        Window  parent  = SwingUtilities.getWindowAncestor(this);
                        String  title   = "Building";
                        String  message = "Building '" + builder.name + "'...";
                        JDialog diag    = SwingMisc.createSimpleModalDialog(parent, title, message);
                        builder.diag    = diag;
                        diag.setVisible(true);
                    }
                }
            }
        }
        
        @Override
        Symbol getSymbol() {
            Symbol sym = super.getSymbol();
            return sym;
        }
        
        private class Builder extends AbstractJob {
            private final Symbol sym;
            
            private final String body;
            private final String name;
            private final String rightName;
            private final Object associativity;
            private final Integer precedence;
            private final boolean isFunctionLike;
            private final boolean isLiteral;
            
            private final TreeAdapter<?, String> params;
            
            private volatile Symbol result;
            private volatile JDialog diag;
            
            Builder() {
                sym            = CustomPanel.this.sym;
                body           = CustomPanel.this.editor.getText();
                name           = CustomPanel.this.nameText;
                rightName      = CustomPanel.this.rightNameText;
                associativity  = CustomPanel.this.associativity.getSelectedItem();
                precedence     = CustomPanel.this.precedence.getSelectedPrecedence();
                isFunctionLike = CustomPanel.this.functionLike.isSelected();
                isLiteral      = CustomPanel.this.literal.isSelected();
                params         = CustomPanel.this.params.getParametersAsTree();
            }
            
            @Override
            public String toString() {
                return ToString.builder(Builder.class)
                               .add("result",         b -> b.result)
                               .add("sym",            b -> b.sym)
                               .add("kind",           b -> b.sym == null ? "null" : b.sym.getSymbolKind())
                               .add("body",           b -> b.body)
                               .add("name",           b -> b.name)
                               .add("rightName",      b -> b.rightName)
                               .add("associativity",  b -> b.associativity)
                               .add("precedence",     b -> b.precedence)
                               .add("isFunctionLike", b -> b.isFunctionLike)
                               .add("isLiteral",      b -> b.isLiteral)
                               .add("params",         b -> b.params)
                               .add("diag",           b -> b.diag == null ? "null" : "JDialog")
                               .build()
                               .apply(this);
            }
            
            @Override
            protected void executeImpl() {
                try {
                    result = createSymbol();
//                    if (false) {
//                        Worker.sleep(4000);
//                    }
                } catch (JobCancellationException x) {
                    throw x;
                } catch (RuntimeException x) {
                    Log.note(getClass(), "executeImpl", this);
                    Log.caught(getClass(), "while building", x, false);
                }
            }
            private Symbol createSymbol() {
                if (sym != null) {
                    Symbol.Builder<?, ?> builder =
                        createBuilder(this.sym,
                                      this.name,
                                      this.rightName,
                                      this.isFunctionLike,
                                      this.isLiteral,
                                      this.associativity,
                                      this.precedence);
                    switch (sym.getSymbolKind()) {
                        case VARIABLE:
                            return new Variable.FromTree((Variable.Builder) builder, body);
                        case NARY_OP:
                            return new NaryOp.FromTree((NaryOp.Builder) builder, params, body);
                        case SPAN_OP:
                            return new SpanOp.FromTree((SpanOp.Builder) builder, params, body);
                        case UNARY_OP:
                            return new UnaryOp.FromTree((UnaryOp.Builder) builder, params, body);
                        case BINARY_OP:
                            return new BinaryOp.FromTree((BinaryOp.Builder) builder, params, body);
                        default:
                            assert false : this;
                            break;
                    }
                }
                return null;
            }
            
            @Override
            protected void doneImpl() {
                if (!isCancelled()) {
                    Symbol result = this.result;
                    if (result != null) {
                        if (this.sym == CustomPanel.this.sym) {
                            result = result.configure(createBuilder());
                            setSymbol(result);
                        }
                    }
                }
                synchronized (monitor) {
                    if (this == builder) {
                        builder = null;
                    }
                    if (diag != null) {
                        diag.setVisible(false);
                    }
                    monitor.notifyAll();
                }
            }
        }
        
        @Override
        public void close() {
            synchronized (monitor) {
                if (builder != null) {
                    builder.cancel();
                }
                builder = null;
            }
            editor.close();
        }
    }
    
    private enum Type {
        NONE,
        INTRINSIC,
        ALIAS,
        CUSTOM;
        
//        private static final Type NULL = null;
        
        private final String camel;
        
        private Type() {
            camel = name().substring(0, 1) + name().substring(1).toLowerCase(Locale.ROOT);
        }
        
        String toCamelCase() {
            return camel;
        }
        
        String key() {
            return name();
        }
        
        boolean isSymbol(Symbol sym) {
            switch (this) {
                case NONE:
                    return sym == null;
                case INTRINSIC:
                    return sym != null && sym.isIntrinsic();
                case ALIAS:
                    return sym != null && sym.isAlias();
                default:
                    assert this == CUSTOM : this;
                    return true;
            }
        }
        
//        static boolean isSymbol(Type t, Symbol sym) {
//            return (t == null) ? (sym == null) : t.isSymbol(sym);
//        }
        
        static Type valueOf(Symbol sym) {
            if (sym == null)
                return NONE;
            if (sym.isIntrinsic())
                return INTRINSIC;
            if (sym.isAlias())
                return ALIAS;
            return CUSTOM;
        }
        
//        static String keyFor(Type t) {
//            return (t == null) ? "null" : t.key();
//        }
    }
    
    private static final class SymbolTypeRenderer implements ListCellRenderer<Type> {
        private final ListCellRenderer<Object> del;
        
        @SuppressWarnings("unchecked")
        SymbolTypeRenderer(ListCellRenderer<?> del) {
            this.del = (ListCellRenderer<Object>) Objects.requireNonNull(del, "del");
        }
        
        @Override
        public Component getListCellRendererComponent(JList<? extends Type> list,
                                                      Type    val,
                                                      int     index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus) {
            String str = (val == null) ? null : val.toCamelCase();
            return del.getListCellRendererComponent(list, str, index, isSelected, cellHasFocus);
        }
    }
    
    private static JPanel createBorderPanel(String title) {
        JPanel pane = new JPanel() {
            @Override
            public Dimension getMaximumSize() {
                Dimension max = super.getMaximumSize();
                max.height = getPreferredSize().height;
                return max;
            }
        };
        pane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
        return pane;
    }
    
    private static JPanel createBorderPanel(Component comp, String title) {
        JPanel pane = createBorderPanel(title);
        pane.setLayout(new BorderLayout());
        pane.add(comp, BorderLayout.CENTER);
        return pane;
    }
    
    private static final class PrecedenceComboBox extends JComboBox<Integer> implements DocumentListener {
        private Integer sel;
        
        private ChangeListener[] listeners = new ChangeListener[0];
        private final ChangeEvent change = new ChangeEvent(this);
        
        PrecedenceComboBox() {
            setRenderer(new PrecedenceCellRenderer(getRenderer()));
            setEditable(true);
            Operator.PRECEDENCE_BY_VALUE.keySet().forEach(this::addItem);
            
            Component comp = getEditor().getEditorComponent();
            try {
                JTextComponent jtext = (JTextComponent) comp;
                DocumentPropertyChangeListener.add(jtext, this, new NumberTextField.IntegerFilter(true));
            } catch (ClassCastException x) {
                Log.caught(getClass(), "adding document listener", x, false);
            }
            
            addItemListener((ItemSelectedListener) e -> {
                Object item = getSelectedItem();
                if (item instanceof Integer) {
                    setSelectedPrecedence((Integer) item);
                }
            });
        }
        
        Integer getSelectedPrecedence() {
            return sel;
        }
        
        public void addChangeListener(ChangeListener cl) {
            listeners = Misc.append(listeners, Objects.requireNonNull(cl, "cl"));
        }
        
        public void removeChangeListener(ChangeListener cl) {
            listeners = Misc.remove(listeners, Objects.requireNonNull(cl, "cl"));
        }
        
        public ChangeListener[] getChangeListeners() {
            return listeners.clone();
        }
        
        void setSelectedPrecedence(Integer sel) {
            if (!Objects.equals(this.sel, sel)) {
                this.sel = sel;
                fireStateChanged();
            }
        }
        
        void fireStateChanged() {
            for (ChangeListener cl : listeners)
                cl.stateChanged(change);
        }
        
        private static final BigInteger MIN_INT = BigInteger.valueOf(Integer.MIN_VALUE);
        private static final BigInteger MAX_INT = BigInteger.valueOf(Integer.MAX_VALUE);
        
        private void docChanged(DocumentEvent e) {
            try {
                Document   doc  = e.getDocument();
                String     text = doc.getText(0, doc.getLength());
                BigInteger val  = new BigInteger(text);
                
                if (val.compareTo(MIN_INT) < 0)
                    val = MIN_INT;
                if (val.compareTo(MAX_INT) > 0)
                    val = MAX_INT;
                
                setSelectedPrecedence(val.intValueExact());
                
            } catch (BadLocationException x) {
                Log.caught(getClass(), "docChanged", x, false);
            } catch (NumberFormatException x) {
                Log.caught(getClass(), "docChanged", x, true);
            }
        }
        
        @Override
        public void insertUpdate(DocumentEvent e) {
            docChanged(e);
        }
        
        @Override
        public void removeUpdate(DocumentEvent e) {
            docChanged(e);
        }
        
        @Override
        public void changedUpdate(DocumentEvent e) {
        }
    }
    
    private static final class PrecedenceCellRenderer implements ListCellRenderer<Integer> {
        private final ListCellRenderer<Object> del;
        
        @SuppressWarnings("unchecked")
        PrecedenceCellRenderer(ListCellRenderer<?> del) {
            this.del = (ListCellRenderer<Object>) Objects.requireNonNull(del, "del");
        }
        
        @Override
        public Component getListCellRendererComponent(JList<? extends Integer> list,
                                                      Integer val,
                                                      int     index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus) {
            String str = null;
            if (val != null) {
                str = Operator.PRECEDENCE_BY_VALUE.get(val);
                if (str == null) {
                    str = val.toString();
                } else {
                    str += " (" + val + ")";
                }
            }
            return del.getListCellRendererComponent(list, str, index, isSelected, cellHasFocus);
        }
    }
    
    private static final class AssociativityCellRenderer implements ListCellRenderer<Enum<?>> {
        private final ListCellRenderer<Object> del;
        
        @SuppressWarnings("unchecked")
        AssociativityCellRenderer(ListCellRenderer<?> del) {
            this.del = (ListCellRenderer<Object>) Objects.requireNonNull(del, "del");
        }
        
        @Override
        public Component getListCellRendererComponent(JList<? extends Enum<?>> list,
                                                      Enum<?> val,
                                                      int     index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus) {
            String str = null;
            if (val instanceof Associativity) {
                switch ((Associativity) val) {
                    case LEFT_TO_RIGHT:
                        str = "Left-To-Right";
                        break;
                    case RIGHT_TO_LEFT:
                        str = "Right-To-Left";
                        break;
                }
            }
            if (val instanceof Affix) {
                switch ((Affix) val) {
                    case PREFIX:
                        str = "Prefix";
                        break;
                    case SUFFIX:
                        str = "Suffix";
                        break;
                }
            }
            return del.getListCellRendererComponent(list, str, index, isSelected, cellHasFocus);
        }
    }
}