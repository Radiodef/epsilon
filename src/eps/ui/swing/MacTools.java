/* MIT License
 *
 * Copyright (c) 2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.util.*;
import eps.app.*;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.*;
import java.lang.reflect.*;

/**
 * This class provides proxy utilities for jobs that were handled by com.apple.eawt
 * before Java 9 and are now handled by java.awt.Desktop.
 */
// see: https://docs.oracle.com/javase/9/migrate/toc.htm#JSMIG-GUID-97C1D0BB-D5D3-4CAD-B17D-03A87A0AAF3B
//      http://openjdk.java.net/jeps/272
enum MacTools {
    INSTANCE;
    
    private final List<Runnable> reOpenTasks = new CopyOnWriteArrayList<>();
    
    MacTools() {
        setQuitStrategy("CLOSE_ALL_WINDOWS");
        disableSuddenTermination();
        
        addQuitHandler(new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) {
                Log.note(getClass(), "invoke", method);
                
                if ("handleQuitRequestWith".equals(method.getName())) {
                    Object quitResponse = args[1];
                    try {
                        quitResponse.getClass().getMethod("performQuit").invoke(quitResponse);
                    } catch (ReflectiveOperationException x) {
                        // This exception should be logged (but it shouldn't happen).
                        Log.caught(getClass(), "during invocation of handleQuitRequestWith", x, false);
                        System.exit(0xBAD_C0DE);
                    }
                }
                return null;
            }
        });
        
        addPreferencesHandler(new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) {
                Log.note(getClass(), "invoke", method);
                
                if ("handlePreferences".equals(method.getName())) {
                    App.runOnInterfaceThread(() -> App.app().getSettingsWindow().show());
                }
                return null;
            }
        });
        
        addAboutHandler(new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) {
                Log.note(getClass(), "invoke", method);
                
                // noinspection StatementWithEmptyBody
                if ("handleAbout".equals(method.getName())) {
                    // TODO
                }
                return null;
            }
        });
        
        addHiddenHandler(new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) {
                Log.note(getClass(), "invoke", method);
                
                switch (method.getName()) {
                    case "appHidden":
                    case "appUnhidden":
                        // unused
                        break;
                }
                return null;
            }
        });
        
        addReOpenHandler(new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) {
                Log.note(getClass(), "invoke", method);
                
                App.runOnInterfaceThread(() -> reOpenTasks.forEach(Runnable::run));
                return null;
            }
        });
        
        addForegroundHandler(new InvocationHandler() {
            private Set<JDialog> visible;
            
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) {
                Log.note(getClass(), "invoke", method);
                
                App.runOnInterfaceThread(() -> {
                    switch (method.getName()) {
                        case "appMovedToBackground": {
                            App app = App.app();
                            
                            visible =
                                Stream.of(app.getSettingsWindowIfInitialized(),
                                          app.getSymbolsWindowIfInitialized(),
                                          app.getDebugWindowIfInitialized())
                                .filter(Optional::isPresent)
                                .map(opt -> ((DialogChild) opt.get()).getDialog())
                                .filter(JDialog::isVisible)
                                .collect(Collectors.toCollection(Misc::newIdentityHashSet));
                            
                            visible.forEach(diag -> diag.setVisible(false));
                            break;
                        }
                        case "appRaisedToForeground": {
                            if (visible != null) {
                                visible.forEach(diag -> diag.setVisible(true));
                                visible = null;
                                SwingMisc.getMainFrame().toFront();
                            }
                            break;
                        }
                    }
                });
                
                return null;
            }
        });
    }
    
    void addReOpenTask(Runnable task) {
        Log.entering(getClass(), "addReOpenTask");
        Objects.requireNonNull(task, "task");
        
        reOpenTasks.add(task);
    }
    
    private static final String RTFS_NAME = "requestToggleFullScreen";
    
    private final Method requestToggleFullScreen =
        getReflectively(RTFS_NAME, () -> getApplicationClass().getMethod(RTFS_NAME, Window.class));
    
    void requestToggleFullScreen(Window window) {
        if (requestToggleFullScreen != null) {
            try {
                requestToggleFullScreen.invoke(getApplication(), window);
                Log.note(getClass(), RTFS_NAME, "supposedly toggled");
            } catch (ReflectiveOperationException x) {
                Log.caught(getClass(), RTFS_NAME, x, false);
            }
        } else {
            Log.note(getClass(), RTFS_NAME, "no method to invoke");
        }
    }
    
    private void setQuitStrategy(@SuppressWarnings("SameParameterValue")
                                 String quitStrategyName) {
        getReflectively(
            "QuitStrategy class",
            () -> getMethod(APPLICATION_CLASS_NAME, "setQuitStrategy", "com.apple.eawt.QuitStrategy"),
            () -> getMethod(DESKTOP_CLASS_NAME, "setQuitStrategy", "java.awt.desktop.QuitStrategy")
        ).ifPresent(setQuitStrategy -> {
            try {
                Enum<?> quitStrategy = Misc.getEnumConstant(setQuitStrategy.getParameterTypes()[0], quitStrategyName);
                invoke(setQuitStrategy, quitStrategy);
                
                Log.note(getClass(), "setQuitStrategy", "QuitStrategy set to ", quitStrategyName);
                
            } catch (IllegalArgumentException | ReflectiveOperationException x) {
                Log.caught(getClass(), "while setting QuitStrategy", x, false);
            }
        });
    }
    
    private void disableSuddenTermination() {
        getReflectively(
            "disableSuddenTermination",
            () -> getMethod(APPLICATION_CLASS_NAME, "disableSuddenTermination"),
            () -> getMethod(DESKTOP_CLASS_NAME, "disableSuddenTermination")
        ).ifPresent(method -> {
            try {
                invoke(method);
                Log.note(getClass(), "disableSuddenTermination", "sudden termination disabled");
            } catch (ReflectiveOperationException x) {
                Log.caught(getClass(), "while disabling sudden termination", x, false);
            }
        });
    }
    
    private void addQuitHandler(InvocationHandler quitHandler) {
        addProxyHandler(
            "app quit handler",
            quitHandler,
            () -> {
                Class<?> type = Class.forName("com.apple.eawt.QuitHandler");
                Method method = getMethod(APPLICATION_CLASS_NAME, "setQuitHandler", type.getName());
                return Pair.of(method, type);
            },
            () -> {
                Class<?> type = Class.forName("java.awt.desktop.QuitHandler");
                Method method = getMethod(DESKTOP_CLASS_NAME, "setQuitHandler", type.getName());
                return Pair.of(method, type);
            }
        );
    }
    
    private void addPreferencesHandler(InvocationHandler prefsHandler) {
        addProxyHandler(
            "app preferences handler",
            prefsHandler,
            () -> {
                Class<?> type = Class.forName("com.apple.eawt.PreferencesHandler");
                Method method = getMethod(APPLICATION_CLASS_NAME, "setPreferencesHandler", type.getName());
                return Pair.of(method, type);
            },
            () -> {
                Class<?> type = Class.forName("java.awt.desktop.PreferencesHandler");
                Method method = getMethod(DESKTOP_CLASS_NAME, "setPreferencesHandler", type.getName());
                return Pair.of(method, type);
            }
        );
    }
    
    private void addAboutHandler(InvocationHandler aboutHandler) {
        addProxyHandler(
            "app about handler",
            aboutHandler,
            () -> {
                Class<?> type = Class.forName("com.apple.eawt.AboutHandler");
                Method method = getMethod(APPLICATION_CLASS_NAME, "setAboutHandler", type.getName());
                return Pair.of(method, type);
            },
            () -> {
                Class<?> type = Class.forName("java.awt.desktop.AboutHandler");
                Method method = getMethod(DESKTOP_CLASS_NAME, "setAboutHandler", type.getName());
                return Pair.of(method, type);
            }
        );
    }
    
    private void addHiddenHandler(InvocationHandler hiddenHandler) {
        addProxyHandler(
            "app hidden handler",
            hiddenHandler,
            () -> {
                Method method = getMethod(APPLICATION_CLASS_NAME, "addAppEventListener", "com.apple.eawt.AppEventListener");
                Class<?> type = Class.forName("com.apple.eawt.AppHiddenListener");
                return Pair.of(method, type);
            },
            () -> {
                Method method = getMethod(DESKTOP_CLASS_NAME, "addAppEventListener", "java.awt.desktop.SystemEventListener");
                Class<?> type = Class.forName("java.awt.desktop.AppHiddenListener");
                return Pair.of(method, type);
            }
        );
    }
    
    private void addReOpenHandler(InvocationHandler reOpenHandler) {
        addProxyHandler(
            "app re-open handler",
            reOpenHandler,
            () -> {
                Method method = getMethod(APPLICATION_CLASS_NAME, "addAppEventListener", "com.apple.eawt.AppEventListener");
                Class<?> type = Class.forName("com.apple.eawt.AppReOpenedListener");
                return Pair.of(method, type);
            },
            () -> {
                Method method = getMethod(DESKTOP_CLASS_NAME, "addAppEventListener", "java.awt.desktop.SystemEventListener");
                Class<?> type = Class.forName("java.awt.desktop.AppReopenedListener");
                return Pair.of(method, type);
            }
        );
    }
    
    private void addForegroundHandler(InvocationHandler foregroundHandler) {
        addProxyHandler(
            "app foreground handler",
            foregroundHandler,
            () -> {
                Method method = getMethod(APPLICATION_CLASS_NAME, "addAppEventListener", "com.apple.eawt.AppEventListener");
                Class<?> type = Class.forName("com.apple.eawt.AppForegroundListener");
                return Pair.of(method, type);
            },
            () -> {
                Method method = getMethod(DESKTOP_CLASS_NAME, "addAppEventListener", "java.awt.desktop.SystemEventListener");
                Class<?> type = Class.forName("java.awt.desktop.AppForegroundListener");
                return Pair.of(method, type);
            }
        );
    }
    
    private void addProxyHandler(String description,
                                 InvocationHandler invocationHandler,
                                 ReflectiveSupplier<Pair<Method, Class<?>>> before9,
                                 ReflectiveSupplier<Pair<Method, Class<?>>> after9) {
        getReflectively(description + " method and type", before9, after9).ifPresent(info -> {
            try {
                invoke(info.left, Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
                                                         new Class<?>[] { info.right },
                                                         invocationHandler));
                Log.note(getClass(), "addProxyHandler", description, " added");
            } catch (ReflectiveOperationException x) {
                Log.caught(SwingMisc.class, "while creating & adding re-open proxy", x, false);
            }
        });
    }
    
    private static Method getMethod(String className, String methodName, String... parameterTypeNames)
            throws ReflectiveOperationException {
        Class<?>[] parameterTypes = new Class<?>[parameterTypeNames.length];
        
        for (int i = 0; i < parameterTypes.length; ++i) {
            parameterTypes[i] = Class.forName(parameterTypeNames[i]);
        }
        
        return Class.forName(className).getMethod(methodName, parameterTypes);
    }
    
    private static final String DESKTOP_CLASS_NAME = "java.awt.Desktop";
    
    private static final String APPLICATION_CLASS_NAME = "com.apple.eawt.Application";
    
    private static volatile Class<?> APPLICATION_CLASS;
    
    private static Class<?> getApplicationClass() throws ClassNotFoundException {
        Class<?> applicationClass = APPLICATION_CLASS;
        return ( applicationClass != null ) ? ( applicationClass )
            : ( APPLICATION_CLASS = Class.forName(APPLICATION_CLASS_NAME) );
    }
    
    private static volatile Object APPLICATION;
    
    private static Object getApplication() throws ReflectiveOperationException {
        Object application = APPLICATION;
        return ( application != null ) ? ( application )
            : ( APPLICATION = getApplicationClass().getMethod("getApplication").invoke(null) );
    }
    
    private static void invoke(Method m, Object... args) throws ReflectiveOperationException {
        Class<?> decl = m.getDeclaringClass();
        
        Object target;
        
        if (Desktop.class.isAssignableFrom(decl))
            target = Desktop.getDesktop();
        else if (getApplicationClass().isAssignableFrom(decl))
            target = getApplication();
        else
            throw new IllegalArgumentException("m = " + m);
        
        m.invoke(target, args);
    }
    
    @FunctionalInterface
    private interface ReflectiveSupplier<T> {
        T get() throws ReflectiveOperationException;
    }
    
    private <T> T getReflectively(String description, ReflectiveSupplier<T> s) {
        try {
            return s.get();
        } catch (ReflectiveOperationException | SecurityException x) {
            Log.caught(getClass(), description, x, false);
            return null;
        }
    }
    
    private <T> Optional<T> getReflectively(String description,
                                            ReflectiveSupplier<T> before9,
                                            ReflectiveSupplier<T> after9) {
        try {
            return Optional.ofNullable(after9.get());
        } catch (ReflectiveOperationException | SecurityException x0) {
            Log.caught(getClass(), description, x0, true);
            try {
                return Optional.ofNullable(before9.get());
            } catch (ReflectiveOperationException | SecurityException x1) {
                Log.caught(getClass(), description, x1, false);
                return Optional.empty();
            }
        }
    }
}
