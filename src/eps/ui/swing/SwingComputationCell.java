/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.app.*;
import eps.util.*;
import eps.eval.*;
import eps.block.*;
import eps.job.*;
import static eps.ui.swing.SwingMisc.*;
import static eps.app.App.*;

import javax.swing.*;
import java.util.*;
import java.util.function.*;

final class SwingComputationCell extends SwingTextAreaCell implements ComputationCell {
    private boolean     isComplete  = false;
    private Computation computation = null;
    private Job         job         = null;
    
    private final JMenuItem stop;
    private final JMenuItem reevaluate;
    
    private final Set<Consumer<? super ComputationCell>> stopListeners   = new ArraySet<>(2);
    private final Set<Consumer<? super ComputationCell>> reevalListeners = new ArraySet<>(2);
    
    SwingComputationCell() {
        super("");
        JPopupMenu popup = getPopupMenu();
        popup.addSeparator();
        
        stop = new JMenuItem("Stop");
        stop.addActionListener(e -> notifyAll(stopListeners));
        popup.add(stop);
        
        reevaluate = new JMenuItem("Re-evaluate");
        reevaluate.addActionListener(e -> notifyAll(reevalListeners));
        popup.add(reevaluate);
        
        this.setComplete(false);
        
        /*
        this.refreshing(Setting.NUMBER_STYLE)
            .refreshing(Setting.FUNCTION_STYLE)
            .refreshing(Setting.LITERAL_STYLE)
            .refreshing(Setting.BRACKET_STYLE)
            .refreshing(Setting.ECHO_EXACT_INPUT)
            .refreshing(Setting.BASIC_STYLE);
        */
        
        this.refreshing(Setting.ECHO_EXACT_INPUT);
        
        this.addReevalListener(self -> {
            if (self.isComplete()) {
                Computation comp = self.getComputation();
                if (comp != null) {
                    App.evaluate(comp.getSource(), comp.getItem(), self, comp.getAnswerFormatter(), comp.getPostComputation());
                }
            }
        });
    }
    
    /*
    private <T> SwingComputationCell refreshing(Setting<T> setting) {
        settingsListeners.add(setting, new ResultRefresher<>(this));
        return this;
    }
    */
    
    @Override
    public SwingComputationCell followTextStyle(Setting<Style> setting) {
        super.followTextStyle(setting);
        return this;
    }
    
    @Override
    public eps.ui.Packet getPacket() {
        return ComputationCell.super.getPacket();
    }
    
    @Override
    protected Context getBlockContext() {
        if (computation != null && computation.isComplete()) {
            return computation.getContext();
        }
        return null;
    }
    
    @Override
    public Computation getComputation() {
        requireEdt();
        return this.computation;
    }
    
    @Override
    public void setComputation(Computation comp, boolean updateUi) {
        Log.entering(SwingComputationCell.class, "setComputation");
        requireEdt();
        this.computation = comp;
        
        if (updateUi) {
            if (comp == null) {
                this.setText("");
                
            } else {
                if (job != null) {
                    job.cancel();
                }
                job = new UpdateJob(this, comp);
                app().getWorker2().enqueue(job, true);
            }
        }
        
        app().itemsChanged();
    }
    
    @Override
    public boolean isComplete() {
        requireEdt();
        return this.isComplete;
    }
    
    @Override
    public void setComplete(boolean isComplete) {
        requireEdt();
        this.isComplete = isComplete;
        stop.setEnabled(!isComplete);
        reevaluate.setEnabled(isComplete);
    }
    
    @Override
    public void addStopListener(Consumer<? super ComputationCell> listener) {
        requireEdt();
        stopListeners.add(Objects.requireNonNull(listener, "listener"));
    }
    
    @Override
    public void removeStopListener(Consumer<? super ComputationCell> listener) {
        requireEdt();
        stopListeners.remove(Objects.requireNonNull(listener, "listener"));
    }
    
    @Override
    public void addReevalListener(Consumer<? super ComputationCell> listener) {
        requireEdt();
        reevalListeners.add(Objects.requireNonNull(listener, "listener"));
    }
    
    @Override
    public void removeReevalListener(Consumer<? super ComputationCell> listener) {
        requireEdt();
        reevalListeners.remove(Objects.requireNonNull(listener, "listener"));
    }
    
    private void notifyAll(Iterable<? extends Consumer<? super ComputationCell>> listeners) {
        requireEdt();
        Objects.requireNonNull(listeners, "listeners");
        listeners.forEach(listener -> listener.accept(this));
    }
    
    /*
    private static final class ResultRefresher<T> extends SwingSettingWeakListener<T, SwingComputationCell> {
        private ResultRefresher(SwingComputationCell cell) {
            super(cell);
        }
        @Override
        protected void settingChangedOnEdt(SwingComputationCell cell,
                                           Settings   settings,
                                           Setting<T> setting,
                                           T          oldValue,
                                           T          newValue) {
            if (cell.computation != null) {
//                cell.setComputation(cell.computation, true);
                Block block = cell.getBlock();
                if (block != null) {
                    cell.setText(block);
                }
            }
        }
    }
    */
    
    private static final class UpdateJob extends AbstractJob {
        private final SwingComputationCell cell;
        private final Computation          comp;
        
        private volatile Block block;
        
        private UpdateJob(SwingComputationCell cell, Computation comp) {
            this.cell = Objects.requireNonNull(cell, "cell");
            this.comp = Objects.requireNonNull(comp, "comp");
        }
        
        @Override
        protected void executeImpl() {
            Context context = comp.getContext();
            Context prev    = Context.setLocal(context);
            try {
                execute0(context);
            } finally {
                Context.setLocal(prev);
            }
        }
        private void execute0(Context context) {
            Tree    tree   = comp.getTree();
            String  source = context.getSource();
            
            Worker.executing();
            boolean isMultiline = source.contains("\n");
            
            Block echo;
            if (context.getSetting(Setting.ECHO_EXACT_INPUT)) {
                echo = new TextBlock(source);
            } else {
                echo = tree.toBlock(context);
                if (isMultiline) {
                    echo = Block.reindent(echo);
                }
            }
            
            // TODO
            String prefix = comp.getPostComputation().prefix();
            if (prefix != null) {
                Block prefixBk = new StyleBlock(Block.of(prefix)).set(Setting.COMMAND_STYLE);
                echo = Block.join(prefixBk, Block.space(), echo);
            }
            
            AnswerFormatter fmt = comp.getAnswerFormatter();
            if (fmt == AnswerFormatter.NONE) {
                this.block = echo;
                return;
            }
            
            Worker.executing();
            Block block = echo;
            
            MutableBoolean approx = new MutableBoolean(false);
            Block          result = fmt.toResultBlock(comp, approx);
            if (result != null) {
                JoinBlock join = new JoinBlock(6).addBlock(echo);

                if (isMultiline) {
                    join.addBlock(Block.ln())
                        .addBlock(Block.indent(context));
                } else {
                    join.addBlock(Block.space());
                }
                
                block = join.addBlock(Block.of(approx.value ? '≈' : '='))
                            .addBlock(Block.space())
                            .addBlock(result);
            }
            
            Worker.executing();
            this.block = block;
        }
        
        @Override
        protected void doneImpl() {
            if (!this.isCancelled()) {
                cell.setText(block);
                app().itemsChanged();
            }
            if (cell.job == this) {
                cell.job = null;
            }
        }
    }
}