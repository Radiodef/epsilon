/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.*;
import java.util.*;

class CompactButton extends JButton implements AutoCloseable {
    private Color textColor;
    private Color pressedColor;
    private Font textFont;
    private Font pressedFont;
    
    private boolean fontChanged = false;
    
    private boolean isInitialized = false;
    private boolean isToggleable = false;
    
    private final Settings.Listeners listeners;
    
    CompactButton(String text) {
        this(text, false);
    }
    
    CompactButton(String text, boolean isToggleable) {
        super(Objects.requireNonNull(text, "text"));
        
        StyleListener styleListener = new StyleListener(this);
        
        listeners = new Settings.Listeners();
        listeners.addAndSet(Setting.BASIC_STYLE, styleListener)
                 .add(Setting.BUTTON_STYLE, styleListener)
                 .add(Setting.BUTTON_SELECTED_STYLE, styleListener);
        
        setBorder(0);
        setBorderPainted(true);
        setOpaque(false);
        setHorizontalTextPosition(SwingConstants.CENTER);
        setVerticalTextPosition(SwingConstants.CENTER);
        
        setToggleable(isToggleable);
        
        isInitialized = true;
    }
    
    @Override
    public void updateUI() {
        setUI(new BasicButtonUI());
    }
    
    @Override
    public void setModel(ButtonModel model) {
        super.setModel(model);
        model.addChangeListener(e -> changed());
        changed();
    }
    
    @Override
    public void close() {
        listeners.close();
    }
    
    public void setToggleable(boolean isToggleable) {
        if (this.isToggleable != isToggleable || !isInitialized) {
            this.isToggleable = isToggleable;
            setModel(isToggleable ? new JToggleButton.ToggleButtonModel() : new DefaultButtonModel());
        }
    }
    
    public void setBorder(int size) {
        super.setBorder(BorderFactory.createEmptyBorder(size, size, size, size));
    }
    
    @Override
    public void setBorder(javax.swing.border.Border b) {
        super.setBorder(BorderFactory.createCompoundBorder(b, BorderFactory.createEmptyBorder(2, 2, 2, 2)));
    }
    
    private void changed() {
        ButtonModel model = getModel();
//        Log.note(CompactButton.class, "changed", getText(),
//                 ": isPressed = ", model.isPressed(),
//                 ", isSelected = ", model.isSelected(),
//                 ", isArmed = ", model.isArmed());
        
        boolean isPressed = model.isPressed() || model.isSelected();
        
        if (isPressed) {
            setForeground(pressedColor);
        } else {
            setForeground(textColor);
        }
        
        boolean setFont = textFont != pressedFont;
        
        if (setFont && Objects.equals(textFont, pressedFont)) {
            pressedFont = textFont;
            setFont     = false;
        }
        
        if (setFont || fontChanged) {
            if (fontChanged) {
                cachedPreferredSize = null;
            }
            fontChanged = false;
            setFont(isPressed ? pressedFont : textFont);
        }
        
        repaint();
    }
    
    private Dimension cachedPreferredSize;
    
    @Override
    public Dimension getPreferredSize() {
        Dimension size = cachedPreferredSize;
        if (size != null) {
            return new Dimension(size);
        }
        size = computePreferredSize();
        if (isDisplayable() && (size.width * size.height) > 0) {
            cachedPreferredSize = new Dimension(size);
        }
        return size;
    }
    
    private Dimension computePreferredSize() {
        String      text        = getText();
        Font        textFont    = this.textFont;
        Font        pressedFont = this.pressedFont;
        FontMetrics textFm      = getFontMetrics(textFont);
        FontMetrics pressedFm   = getFontMetrics(pressedFont);
        int         textArea    = textFm.stringWidth(text) * textFm.getAscent();
        int         pressedArea = pressedFm.stringWidth(text) * pressedFm.getAscent();
        FontMetrics fm          = textArea > pressedArea ? textFm : pressedFm;
        
        Rectangle iconRect = new Rectangle();
        Rectangle textRect = new Rectangle();
        Rectangle viewRect = new Rectangle(Short.MAX_VALUE, Short.MAX_VALUE);
        
        SwingUtilities.layoutCompoundLabel(
            this, fm, text, null,
            getVerticalAlignment(), getHorizontalAlignment(),
            getVerticalTextPosition(), getHorizontalTextPosition(),
            viewRect, iconRect, textRect, getIconTextGap()
        );
        
        Rectangle size = iconRect.union(textRect);
        Insets    ins  = getInsets();
        size.width  += ins.left + ins.right;
        size.height += ins.top  + ins.bottom;
        
        return size.getSize();
    }
    
    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }
    
    private static final class StyleListener extends SwingSettingWeakListener<Style, CompactButton> {
        private StyleListener(CompactButton btn) {
            super(btn);
        }
        
        @Override
        protected void settingChangedOnEdt(CompactButton  btn,
                                           Settings       settings,
                                           Setting<Style> setting,
                                           Style          oldStyle,
                                           Style          newStyle) {
            Style abs;
            
            abs = Theme.getAbsoluteStyle(Setting.BUTTON_STYLE);
            
            btn.textColor = SwingMisc.toAwtColor(abs.getForeground());
            btn.textFont  = SwingMisc.toAwtFontAbs(abs);
            
            abs = Theme.getAbsoluteStyle(Setting.BUTTON_SELECTED_STYLE);
            
            btn.pressedColor = SwingMisc.toAwtColor(abs.getForeground());
            btn.pressedFont  = SwingMisc.toAwtFontAbs(abs);
            
            btn.fontChanged = true;
            btn.changed();
            btn.revalidate();
        }
    }
}