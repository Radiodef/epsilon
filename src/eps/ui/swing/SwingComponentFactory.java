/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.util.*;
import eps.io.*;
import static eps.ui.swing.SwingMisc.*;
import static eps.app.App.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.Window;
import javax.swing.*;
import java.util.List;
import java.util.*;
import java.io.IOException;
import java.nio.file.Path;

public final class SwingComponentFactory extends AbstractComponentFactory {
    public SwingComponentFactory() {
        super(SwingMisc.Store.DEFAULT_STYLE);
    }
    
    @Override
    public AsyncExecutor newAsyncExecutor() {
        return new SwingAsyncExecutor();
    }
    
    @Override
    public void displayError(String title, String text) {
        requireEdt();
        
        JFrame  parent;
        Dimension size;
        try {
            parent = ((SwingMainWindow) app().getMainWindow()).getFrame();
            size   = SwingMisc.getAvailableScreenSize(parent);
        } catch (IllegalStateException | ClassCastException x) {
            Log.caught(SwingComponentFactory.class, "displayError", x, false);
            parent = null;
            size   = Toolkit.getDefaultToolkit().getScreenSize();
        }
        
        text = "<html><body style='width:" + (size.width / 4) + "px'>" + text + "</body></html>";
        
        JOptionPane.showMessageDialog(parent, new JLabel(text), title, JOptionPane.ERROR_MESSAGE);
    }
    
    @Override
    public MainWindow newMainWindow() {
        requireEdt();
        return new SwingMainWindow();
    }
    
    @Override
    public DebugWindow newDebugWindow() {
        requireEdt();
        return new SwingDebugWindow();
    }
    
    @Override
    public SettingsWindow newSettingsWindow() {
        requireEdt();
        return new SwingSettingsWindow();
    }
    
    @Override
    public SymbolsWindow newSymbolsWindow() {
        requireEdt();
        return new SwingSymbolsWindow();
    }
    
    @Override
    public EmptyCell newEmpty() {
        requireEdt();
        return new SwingEmptyCell();
    }
    
    @Override
    public ContainerCell newContainer() {
        requireEdt();
        return new SwingContainerCell();
    }
    
    @Override
    @Deprecated
    public LabelCell newLabel(String labelText) {
        requireEdt();
        return new SwingLabelCell(labelText);
    }
    
    @Override
    public TextAreaCell newTextArea(String areaText) {
        requireEdt();
        return new SwingTextAreaCell(areaText);
    }
    
    @Override
    public ComputationCell newComputation() {
        requireEdt();
        return new SwingComputationCell();
    }
    
    @Override
    public void prepareToExit() {
        SwingMisc.invokeNow(this::hideModalDialogs);
    }
    
    private void hideModalDialogs() {
        for (Window w : Window.getWindows()) {
            if (w instanceof Dialog && ((Dialog) w).isModal()) {
                w.setVisible(false);
            }
        }
    }
    
    public void prepareToExit_ugh() {
        SwingMisc.requireEdt();
        List<Window> modals = new ArrayList<>();
        
        for (Window w : Window.getWindows()) {
            if (w instanceof Dialog && ((Dialog) w).isModal() && w.isVisible()) {
                modals.add(w);
            }
        }
        
        if (!modals.isEmpty()) {
            SecondaryLoop  loop  = Toolkit.getDefaultToolkit().getSystemEventQueue().createSecondaryLoop();
            MutableBoolean enter = new MutableBoolean(true);
            
            for (Window w : modals) {
                w.addComponentListener(new ComponentAdapter() {
                    @Override
                    public void componentHidden(ComponentEvent e) {
                        modals.remove(w);
                        if (modals.isEmpty()) {
                            if (!loop.exit()) {
                                enter.value = false;
                            }
                        }
                    }
                });
                w.setVisible(false);
            }
            
            if (enter.value) {
                loop.enter();
            }
        }
    }
    
    @Override
    public void close() {
        SwingMisc.invokeNow(this::closeAll);
    }
    
    private void closeAll() {
        Log.entering(getClass(), "closeAll");
        int count = 0;
        
        for (Window window : Window.getWindows()) {
            if (window.isDisplayable()) {
                ++count;
                String title = (window instanceof Frame) ? ((Frame) window).getTitle()
                             : (window instanceof Dialog) ? ((Dialog) window).getTitle()
                             : "";
                Log.note(SwingComponentFactory.class, "close", "closing ", window.getClass(), " \"", title, "\"");
                window.setVisible(false);
                window.dispose();
            }
        }
        
        Log.note(getClass(), "closeAll", count, " windows closed");
    }
    
    @Override
    public boolean isOpenSupported() {
        if (Desktop.isDesktopSupported()) {
            return Desktop.getDesktop().isSupported(Desktop.Action.OPEN);
        }
        return false;
    }
    
    @Override
    public boolean openDirectory(Path path) {
        Objects.requireNonNull(path, "path");
        if (isOpenSupported()) {
            if (!FileSystem.instance().isDirectory(path)) {
                path = path.getParent();
                if (path == null || !FileSystem.instance().isDirectory(path)) {
                    return false;
                }
            }
            try {
                Desktop.getDesktop().open(path.toFile());
                return true;
            } catch (IOException | RuntimeException x) {
                Log.caught(getClass(), "while opening", x, false);
            }
        }
        return false;
    }
}