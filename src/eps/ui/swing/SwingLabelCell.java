/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.app.*;

import static eps.ui.swing.SwingMisc.*;

import javax.swing.*;
import java.awt.*;

@Deprecated
final class SwingLabelCell extends AbstractSwingTextCell implements LabelCell {
    private final JLabel label;
    
    SwingLabelCell(String labelText) {
        this.label = new JLabel() {
            @Override
            public Dimension getMaximumSize() {
                Dimension max = super.getMaximumSize();
                max.width = Short.MAX_VALUE;
                return max;
            }
        };
        
        label.setForeground(Color.BLACK);
        label.setAlignmentX(Component.LEFT_ALIGNMENT);
        label.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        
        this.setText(labelText);
        
        this.followTextStyle(Setting.BASIC_STYLE);
        this.afterTextComponentInitialization();
    }
    
    @Override
    public void close() {
        super.close();
    }
    
    @Override
    protected JLabel getTextComponent() {
        return label;
    }
    
    @Override
    protected JComponent getComponent() {
        requireEdt();
        return getTextComponent();
    }
    
    @Override
    public LabelCell followTextStyle(Setting<Style> setting) {
        super.followTextStyle(setting);
        return this;
    }
}