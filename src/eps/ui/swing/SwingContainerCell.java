/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.app.*;
import static eps.ui.swing.SwingMisc.*;
import static eps.app.App.*;

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.util.List;

final class SwingContainerCell extends SwingCell implements ContainerCell {
    private final List<Cell> cells;
    private final CellPanel  panel;
    
    SwingContainerCell() {
        this.cells = new ArrayList<>(4);
        this.panel = new CellPanel();
    }
    
    @Override
    public void close() {
        requireEdt();
        cells.forEach(Cell::close);
        super.close();
    }
    
    @Override
    public ContainerCell configure(Item item, eps.ui.Packet packet) {
        synchronized (panel.getTreeLock()) {
            return ContainerCell.super.configure(item, packet);
        }
    }
    
    private int getCellGap() {
        return this.getMainWindow().getCellPanel().getCellGap();
    }
    
    @Override
    protected JPanel getComponent() {
        return panel;
    }
    
    @Override
    public int getCellCount() {
        requireEdt();
        return cells.size();
    }
    
    @Override
    public List<Cell> getCells() {
        requireEdt();
        return Collections.unmodifiableList(cells);
    }
    
    @Override
    public int indexOfCell(Cell cell) {
        requireEdt();
        synchronized (panel.getTreeLock()) {
            return cells.indexOf(cell);
        }
    }
    
    private void modified() {
        panel.revalidate();
        panel.repaint();
        app().itemsChanged();
    }
    
    private Component createVerticalStrut() {
        return Box.createVerticalStrut(this.getCellGap());
    }
    
    @Override
    public ContainerCell removeAllCells() {
        requireEdt();
        synchronized (panel.getTreeLock()) {
            cells.clear();
            panel.removeAll();
            this.modified();
        }
        return this;
    }
    
    @Override
    public ContainerCell addCell(Cell cell) {
        requireEdt();
        Log.entering(SwingContainerCell.class, "addCell");
        
        JComponent comp = ((SwingCell) cell).getComponent();
        
        synchronized (panel.getTreeLock()) {
            if (!cells.isEmpty()) {
                panel.add(this.createVerticalStrut());
            }
            
            panel.add(comp);
            cells.add(cell);
            
            this.modified();
        }
        
        return this;
    }
    
    @Override
    public ContainerCell insertCell(int index, Cell cell) {
        requireEdt();
        JComponent comp = ((SwingCell) cell).getComponent();
        
        synchronized (panel.getTreeLock()) {
            cells.add(index, cell);
            
            index *= 2;
            panel.add(comp, index);
            
            if (index > 0) {
                panel.add(this.createVerticalStrut(), index);
            }
            
            this.modified();
        }
        
        return this;
    }
    
    @Override
    public ContainerCell removeCell(Cell cell) {
        requireEdt();
        
        synchronized (panel.getTreeLock()) {
            int index = cells.indexOf(cell);
            if (index >= 0) {
                int size = cells.size();
                
                cells.remove(index);
                panel.remove(2 * index);
                
                int strut = (index == 0) ? ((size > 1) ? 0 : -1)
                          : (size > 1 ? (index - 1) : -1);
                if (strut >= 0) {
                    panel.remove(strut);
                }
                
                this.modified();
            }
        }
        
        return this;
    }
    
    private static final class CellPanel extends JPanel {
        private CellPanel() {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setOpaque(false);
            setAlignmentX(SwingConstants.LEFT);
        }
        private Dimension restrict(Dimension dim) {
            synchronized (getTreeLock()) {
                if (getComponentCount() == 0)
                    dim.height = 0;
            }
            return dim;
        }
        @Override
        public Dimension getMinimumSize() {
            return restrict(super.getMinimumSize());
        }
        @Override
        public Dimension getPreferredSize() {
            return restrict(super.getPreferredSize());
        }
        @Override
        public Dimension getMaximumSize() {
            return restrict(super.getMaximumSize());
        }
    }
}