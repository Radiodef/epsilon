/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.undo.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;

final class TextEditUndoer extends UndoManager implements PropertyChangeListener {
    private final JTextComponent comp;
    
    private final UndoAction undo;
    private final RedoAction redo;
    
    TextEditUndoer(JTextComponent comp) {
        this.comp = Objects.requireNonNull(comp, "comp");
        this.undo = new UndoAction();
        this.redo = new RedoAction();
        
        setLimit(500); // TODO: add setting for this?
        
        comp.getDocument().addUndoableEditListener(this);
        comp.addPropertyChangeListener(SwingMisc.Store.DOCUMENT, this);
        
        putAction(SwingMisc.createUndoStroke(), undo);
        putAction(SwingMisc.createRedoStroke(), redo);
    }
    
    private void putAction(KeyStroke stroke, AbstractAction action) {
        String name = (String) action.getValue(Action.NAME);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(stroke, name);
        comp.getActionMap().put(name, action);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        Object o = e.getOldValue();
        Object n = e.getNewValue();
        
        discardAllEdits();
        
        if (o instanceof Document) {
            ((Document) o).removeUndoableEditListener(this);
        }
        if (n instanceof Document) {
            ((Document) n).addUndoableEditListener(this);
        }
    }
    
    @Override
    public void undoableEditHappened(UndoableEditEvent e) {
        UndoableEdit edit = e.getEdit();
        
        // filter out attribute changes
        if (edit instanceof DocumentEvent) {
            DocumentEvent.EventType type = ((DocumentEvent) edit).getType();
            if (type == DocumentEvent.EventType.CHANGE) {
                edit = new AttributeEdit(edit);
            }
        }
        
        addEdit(new CaretUpdatingEdit(edit, comp));
    }
    
    // We disable the syntax highlighter so that when we do undo() we don't
    // cause it to run. Letting it run would cause it to set character
    // attributes which would add new edits. The new attribute edits would
    // overwrite the edits which were just undone. This would effectively
    // disable redo functionality.
    private boolean setParsing(boolean isParsingEnabled) {
        if (comp instanceof MultilineEditor) {
            boolean wasParsingEnabled = ((MultilineEditor) comp).isParsingEnabled();
            ((MultilineEditor) comp).setParsingEnabled(isParsingEnabled);
            return wasParsingEnabled;
        }
        return false;
    }
    
    @Override
    public synchronized void undo() {
        UndoableEdit edit = editToBeUndone();
        if (edit != null) {
            int index = edits.indexOf(edit);
            if (index >= 0) {
                if (index >= 1) {
                    UndoableEdit prev = edits.get(index - 1);
                    if (prev instanceof CaretUpdatingEdit) {
                        super.undo();
                        ((CaretUpdatingEdit) prev).updateCaret();
                        return;
                    }
                }
            } else {
                Log.note(getClass(), "undo", "indexOf failed for ", edit);
            }
        }
        super.undo();
    }
    
    @Override
    public synchronized void redo() {
        redo(false);
    }
    
    private synchronized void redo(boolean wasParsingEnabled) {
        UndoableEdit edit = editToBeRedone();
        if (edit != null) {
            int index = edits.indexOf(edit);
            if (index >= 0) {
                // Note:
                //  The fix for triggering MultilineEditor parsing
                //  (see undoableEditHappened) worked, but on redo,
                //  we want to also redo the syntax highlighting
                //  edits (if any).
                int size = edits.size();
                UndoableEdit last = null;
                while (++index < size) {
                    UndoableEdit next = edits.get(index);
                    if (!isAttributeEdit(next)) {
                        break;
                    }
                    last = next;
                }
                if (last != null) {
                    // If this was the last edit to redo, re-enable
                    // parsing, then just redo the regular edit. This
                    // causes the text to be parsed again from scratch.
                    if (index == size && wasParsingEnabled) {
                        setParsing(true);
                        redoTo(edit);
                    } else {
                        redoTo(last);
                    }
                    return;
                }
            } else {
                Log.note(getClass(), "redo", "indexOf failed for ", edit);
            }
        }
        super.redo();
    }
    
    private boolean isAttributeEdit(UndoableEdit edit) {
        if (edit instanceof AttributeEdit) {
            return true;
        }
        if (edit instanceof DelegatingEdit) {
            return isAttributeEdit(((DelegatingEdit) edit).edit);
        }
        return false;
    }
    
    private String createActionName(String name) {
        return "edit-" + name + "-" + System.identityHashCode(this);
    }
    
    private final class UndoAction extends AbstractAction {
        UndoAction() {
            super(createActionName("undo"));
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean canUndo = canUndo();
            Log.note(getClass(), "actionPerformed", "canUndo = ", canUndo);
            if (canUndo) {
                boolean wasParsing = setParsing(false);
                try {
                    undo();
                } finally {
                    setParsing(wasParsing);
                }
            }
        }
    }
    private final class RedoAction extends AbstractAction {
        RedoAction() {
            super(createActionName("redo"));
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean canRedo = canRedo();
            Log.note(getClass(), "actionPerformed", "canRedo = ", canRedo);
            if (canRedo) {
                boolean wasParsing = setParsing(false);
                try {
                    redo(wasParsing);
                } finally {
                    setParsing(wasParsing);
                }
            }
        }
    }
    
    private static class DelegatingEdit implements UndoableEdit {
        protected final UndoableEdit edit;
        
        private DelegatingEdit(UndoableEdit edit) {
            this.edit = Objects.requireNonNull(edit, "edit");
        }
        
        @Override
        public boolean isSignificant() {
            return edit.isSignificant();
        }
        
        @Override
        public String getPresentationName() {
            return edit.getPresentationName();
        }
        
        @Override
        public String getUndoPresentationName() {
            return edit.getUndoPresentationName();
        }
        
        @Override
        public String getRedoPresentationName() {
            return edit.getRedoPresentationName();
        }
        
        @Override
        public boolean replaceEdit(UndoableEdit e) {
            return edit.replaceEdit(e);
        }
        
        @Override
        public boolean addEdit(UndoableEdit e) {
            return edit.addEdit(e);
        }
        
        @Override
        public void die() {
            edit.die();
        }
        
        @Override
        public boolean canUndo() {
            return edit.canUndo();
        }
        
        @Override
        public boolean canRedo() {
            return edit.canRedo();
        }
        
        @Override
        public void undo() {
            edit.undo();
        }
        
        @Override
        public void redo() {
            edit.redo();
        }
    }
    
    // Redoing the attribute edits caused the caret
    // to fail to be updated for some reason, so this
    // is a workaround for that.
    private static class CaretUpdatingEdit extends DelegatingEdit {
        private final JTextComponent comp;
        
        private final int dot;
        private final int mark;
        
        CaretUpdatingEdit(UndoableEdit edit, JTextComponent comp) {
            super(edit);
            this.comp = comp;
            Caret   c = comp.getCaret();
            this.dot  = c.getDot();
            this.mark = c.getMark();
        }
        
        @Override
        public void redo() {
            super.redo();
            updateCaret();
        }
        
        protected void updateCaret() {
            Caret c = comp.getCaret();
            c.setDot(mark);
            c.moveDot(dot);
        }
    }
    
    private static final class AttributeEdit extends DelegatingEdit {
        private AttributeEdit(UndoableEdit edit) {
            super(edit);
        }
        
        @Override
        public boolean isSignificant() {
            return false;
        }
    }
}