/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import java.beans.*;
import javax.swing.text.*;
import javax.swing.event.*;

class DocumentPropertyChangeListener implements PropertyChangeListener {
    private final DocumentListener listener;
    private final DocumentFilter   filter;
    
    DocumentPropertyChangeListener(DocumentListener listener) {
        this(listener, null);
    }
    
    DocumentPropertyChangeListener(DocumentFilter filter) {
        this(null, filter);
    }
    
    DocumentPropertyChangeListener(DocumentListener listener, DocumentFilter filter) {
        this.listener = listener;
        this.filter   = filter;
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        Object oldValue = e.getOldValue();
        Object newValue = e.getNewValue();
        
        if (listener != null) {
            if (oldValue instanceof Document) {
                ((Document) oldValue).removeDocumentListener(listener);
            }
            if (newValue instanceof Document) {
                ((Document) newValue).addDocumentListener(listener);
            }
        }
        
        if (filter != null) {
            if (newValue instanceof AbstractDocument) {
                ((AbstractDocument) newValue).setDocumentFilter(filter);
            }
        }
    }
    
    public static <C extends JTextComponent> C add(C comp, SimpleDocumentListener listener) {
        return add(comp, (DocumentListener) listener);
    }
    
    public static <C extends JTextComponent> C add(C comp, DocumentListener listener) {
        return add(comp, listener, null);
    }
    
    public static <C extends JTextComponent> C add(C comp, DocumentFilter filter) {
        return add(comp, null, filter);
    }
    
    public static <C extends JTextComponent> C add(C comp, SimpleDocumentListener listener, DocumentFilter filter) {
        return add(comp, (DocumentListener) listener, filter);
    }
    
    public static <C extends JTextComponent> C add(C comp, DocumentListener listener, DocumentFilter filter) {
        comp.addPropertyChangeListener(SwingMisc.Store.DOCUMENT, new DocumentPropertyChangeListener(listener, filter));
        if (listener != null) {
            comp.getDocument().addDocumentListener(listener);
        }
        if (filter != null) {
            Document doc = comp.getDocument();
            if (doc instanceof AbstractDocument) {
                ((AbstractDocument) doc).setDocumentFilter(filter);
            }
        }
        return comp;
    }
}