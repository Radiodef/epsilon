/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import java.util.*;
import javax.swing.text.*;

final class LineWrapEditorKit extends StyledEditorKit {
    private final ViewFactory viewFactory;
    
    LineWrapEditorKit() {
        this.viewFactory = new WrapViewFactory(super.getViewFactory());
    }
    
    @Override
    public ViewFactory getViewFactory() {
        return viewFactory;
    }
    
    private static final class WrapViewFactory implements ViewFactory {
        private final ViewFactory delegate;
        
        private WrapViewFactory(ViewFactory delegate) {
            this.delegate = Objects.requireNonNull(delegate, "delegate");
        }
        
        @Override
        public View create(Element e) {
            if (Objects.equals(e.getName(), AbstractDocument.ContentElementName)) {
                return new WrapLabelView(e);
            }
            return delegate.create(e);
        }
        
        private static final class WrapLabelView extends LabelView {
            private WrapLabelView(Element e) { super(e); }
            
            @Override
            public float getMinimumSpan(int axis) {
                return (axis == View.X_AXIS) ? 0 : super.getMinimumSpan(axis);
            }
        }
    }
}