/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.ui.Style;
import eps.app.*;
import eps.util.*;
import static eps.app.App.*;
import static eps.ui.swing.SwingMisc.*;

import java.util.function.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.Window;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.*;
import javax.swing.text.*;

final class SwingDebugWindow implements DebugWindow, DialogChild {
    private final JDialog diag;
    
    private boolean isVisible = false;
    private boolean hadFocus = false;
    
    private final WindowVisibilityFollower visFollower;
    
    private final Settings.Listeners listeners = new Settings.Listeners();
    
    private final JTextPane log;
    private final JList<Predicate<Log.Line>> filters;
    private final LogFilterModel filterModel;
    
    private final JPanel sidePane;
    private final FindField findField;
    
    private final Timer lineTimer;
    private final LogLineConsumer lineConsumer;
    private final LogLineNotifier lineNotifier;
    
    private int loFreqLogInterval = Integer.MAX_VALUE;
    private int hiFreqLogInterval = Integer.MAX_VALUE;
    
    private final EnumMap<Theme.Key, AttributeSet> attributes = new EnumMap<>(Theme.Key.class);
    private final EnumMap<Theme.Key, AttributeSet> highlights = new EnumMap<>(Theme.Key.class);
    
    private final FindResult findResult = new FindResult();
    
    SwingDebugWindow() {
        requireEdt();
        
        diag = new JDialog();
        diag.setTitle("Epsilon Debug");
        diag.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        diag.setModal(false);
        
        if (SystemInfo.isMacOsx()) {
            SwingMisc.configureFrameForMac(diag);
        }
        
        diag.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                setVisible(false);
            }
        });
        
        SwingMisc.onFirstOpen(diag, () -> diag.setMinimumSize(diag.getMinimumSize()));
        
        visFollower = new WindowVisibilityFollower();
        getMainFrame().addWindowStateListener(visFollower);
        
        log = new JTextPane() {
            @Override
            public boolean getScrollableTracksViewportWidth() {
                return true;
            }
        };
        
        log.setEditable(false);
        log.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
        log.setCaret(new NoUpdateCaret());
        
        log.setAlignmentY(SwingConstants.BOTTOM);
        
        int gap = app().getSetting(Setting.MAJOR_MARGIN);
        log.setBorder(BorderFactory.createEmptyBorder(gap, gap, gap, gap));
        
        log.setEditorKit(new LineWrapEditorKit());
        
        JScrollPane logPane = new AutoScrollPane(log, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                                      JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        
        filters = new JList<>();
        filters.setFocusable(false);
        filters.setLayoutOrientation(JList.VERTICAL);
        filters.setBorder(BorderFactory.createEmptyBorder(gap, gap, gap, gap));
        filters.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list,
                                                          Object   value,
                                                          int      index,
                                                          boolean  isSelected,
                                                          boolean  cellHasFocus) {
                Component comp = super.getListCellRendererComponent(list, value, index, isSelected, false);
                if (comp instanceof JLabel) {
                    ((JLabel) comp).setText(String.valueOf(value));
                }
                return comp;
            }
        });
        
        filterModel = new LogFilterModel();
        filters.setModel(filterModel);
        filters.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        filters.addListSelectionListener(filterModel);
        
        JScrollPane filterPane = new JScrollPane(filters, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                                          JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        JPanel findPane = new JPanel(new BorderLayout());
        findPane.setOpaque(false);
        
        JPanel findButtons = new JPanel();
        findButtons.setOpaque(false);
        findButtons.setAlignmentX(SwingConstants.CENTER);
        int min = app().getSetting(Setting.MINOR_MARGIN);
        findButtons.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createEtchedBorder(),
            BorderFactory.createEmptyBorder(min, min, min, min)));
        findButtons.setLayout(new BoxLayout(findButtons, BoxLayout.X_AXIS));
        
        CompactButton prev = new CompactButton("Previous");
        CompactButton next = new CompactButton("Next");
        
        prev.addActionListener(e -> findPrevious());
        next.addActionListener(e -> findNext());
        
        findButtons.add(Box.createHorizontalGlue());
        findButtons.add(prev);
        findButtons.add(Box.createHorizontalStrut(min));
        findButtons.add(next);
        findButtons.add(Box.createHorizontalGlue());
        
        findField = new FindField();
        findField.addActionListener(e -> findNext());
        SwingMisc.onFirstOpen(diag, () -> findField.requestFocus());
        
        findPane.add(findButtons, BorderLayout.NORTH);
        findPane.add(findField, BorderLayout.CENTER);
        
        sidePane = new JPanel(new BorderLayout());
        sidePane.add(filterPane, BorderLayout.CENTER);
        sidePane.add(findPane,   BorderLayout.SOUTH);
        
        JSplitPane logSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                                             logPane,
                                             sidePane);
        logSplit.setResizeWeight(1);
        logSplit.setContinuousLayout(true);
        SwingMisc.onFirstOpen(diag, () -> logSplit.setDividerLocation(0.8));
        
        diag.setContentPane(logSplit);
        
        listeners.addAndSet(Setting.MAIN_WINDOW_ALWAYS_ON_TOP, new AlwaysOnTopListener(this));
        
        listeners.add(Setting.BACKGROUND_COLOR, new UpdateStyleListener<>(this))
                 .add(Setting.CARET_COLOR,      new UpdateStyleListener<>(this))
                 .add(Setting.BASIC_STYLE,      new UpdateStyleListener<>(this));
        
        LogLineIntervalListener logIntervalListener = new LogLineIntervalListener(this);
        listeners.addAndSet(Setting.LO_FREQ_LOG_INTERVAL, logIntervalListener)
                 .addAndSet(Setting.HI_FREQ_LOG_INTERVAL, logIntervalListener);
        
        listeners.add(Setting.BASIC_STYLE,            new AttributesListener<>(this))
                 .add(Setting.UNEXPECTED_ERROR_STYLE, new AttributesListener<>(this))
                 .add(Setting.BACKGROUND_COLOR,       new AttributesListener<>(this));
        
        diag.pack();
        setBounds(null);
        
        lineConsumer = new LogLineConsumer();
        lineTimer    = new Timer(loFreqLogInterval, lineConsumer);
        lineTimer.setRepeats(true);
        lineTimer.setCoalesce(false);
        lineTimer.start();
        
        lineNotifier = new LogLineNotifier();
        Log.addLogConsumer(lineNotifier);
        
        updateStyle();
    }
    
    private JFrame getMainFrame() {
        return ((SwingMainWindow) app().getMainWindow()).getFrame();
    }
    
    @Override
    public JDialog getDialog() {
        return diag;
    }
    
    @Override
    public void close() {
        requireEdt();
        isVisible = false;
        
        Log.removeLogConsumer(lineNotifier);
        lineTimer.stop();
        
        diag.setVisible(false);
        diag.dispose();
        
        listeners.close();
        
        getMainFrame().removeWindowListener(visFollower);
    }
    
    @Override
    public boolean isVisible() {
        requireEdt();
        return isVisible;
    }
    
    @Override
    public void setVisible(boolean isVisible) {
        requireEdt();
        boolean wasVisible = this.isVisible;
        
        if (wasVisible == diag.isVisible()) {
            diag.setVisible(isVisible);
            if (isVisible) {
                this.toHighFreqLog();
            }
        }
        
        if (isVisible && !wasVisible && diag.isVisible()) {
            diag.toFront();
        }
        
        this.isVisible = isVisible;
    }
    
    @Override
    public BoundingBox getBounds() {
        requireEdt();
        return SwingMisc.getBounds(diag);
    }
    
    @Override
    public void setBounds(BoundingBox bounds) {
        requireEdt();
        if (bounds != null) {
            SwingMisc.setBounds(diag, bounds);
        } else {
            bounds     = app().getMainWindow().getBounds();
            
            int width  = (int) Math.round(bounds.width());
            int height = (int) Math.round(bounds.height());
            
            diag.setBounds(0, 0, width, height);
        }
        diag.revalidate();
        diag.repaint();
    }
    
    private void toHighFreqLog() {
        requireEdt();
        int delay    = lineTimer.getDelay();
        int interval = hiFreqLogInterval;
        if (delay != interval) {
            lineTimer.setDelay(interval);
            lineTimer.setInitialDelay(0);
            lineTimer.restart();
        }
    }
    
    private static class AlwaysOnTopListener extends SwingSettingWeakListener<Boolean, SwingDebugWindow> {
        private AlwaysOnTopListener(SwingDebugWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnEdt(SwingDebugWindow  window,
                                           Settings          settings,
                                           Setting<Boolean>  setting,
                                           Boolean           oldValue,
                                           Boolean           newValue) {
            window.diag.setAlwaysOnTop(newValue);
        }
    }
    
    private final class WindowVisibilityFollower extends WindowAdapter {
        private boolean isIconified(int state) {
            return (state & Frame.ICONIFIED) == Frame.ICONIFIED;
        }
        
        @Override
        public void windowStateChanged(WindowEvent e) {
            int oldState = e.getOldState();
            int newState = e.getNewState();
            
            if (!isIconified(oldState) && isIconified(newState)) {
                if (isVisible) {
                    hadFocus = diag.hasFocus();
                    diag.setVisible(false);
                }
            }
            
            if (isIconified(oldState) && !isIconified(newState)) {
                if (isVisible) {
                    diag.setVisible(true);
                    
                    // TODO: test other operating systems to see when
                    //       this is really necessary
                    Window window = hadFocus ? diag : getMainFrame();
                    window.requestFocusInWindow();
                    window.toFront();
                }
            }
        }
    }
    
    private void updateStyle() {
        ArgbColor bg    = app().getSetting(Setting.BACKGROUND_COLOR);
        ArgbColor caret = app().getSetting(Setting.CARET_COLOR);
        Style     abs   = Theme.getAbsoluteStyle(Setting.BASIC_STYLE);
        
        if (caret == null) {
            caret = bg.getOpposite();
        }
        
        Color awtBackground = SwingMisc.toAwtColor(bg);
        Color awtForeground = SwingMisc.toAwtColor(abs.getForeground());
        Color awtCaret      = SwingMisc.toAwtColor(caret);
        Font  awtFont       = SwingMisc.toAwtFontAbs(abs);
        
        log.setFont(awtFont);
        log.setBackground(awtBackground);
        log.setForeground(awtForeground);
        log.setCaretColor(awtCaret);
        
        log.revalidate();
        log.repaint();
        
        filters.setFont(awtFont);
        filters.setBackground(awtBackground);
        filters.setForeground(awtForeground);
        filters.setSelectionBackground(awtForeground);
        filters.setSelectionForeground(awtBackground);
        
        filters.revalidate();
        filters.repaint();
        
        findField.setFont(awtFont);
        findField.setForeground(awtForeground);
//        findField.setBackground(awtBackground);
        findField.setCaretColor(awtCaret);
        findField.setSelectionColor(awtForeground);
        findField.setSelectedTextColor(awtBackground);
        
        findField.revalidate();
        findField.repaint();
        
        sidePane.setBackground(awtBackground);
    }
    
    private final class FindField extends JTextField {
        FindField() {
            super(8);
//            updateUI();
            
            int gap = Settings.getOrDefault(Setting.MINOR_MARGIN);
            setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
                BorderFactory.createEmptyBorder(gap, gap, gap, gap)));
            setOpaque(false);
            
            DocumentPropertyChangeListener.add(this, new DocumentListener() {
                @Override
                public void changedUpdate(DocumentEvent e) {
                }
                @Override
                public void insertUpdate(DocumentEvent e) {
                    highlightFind();
                    findResult.clear();
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    highlightFind();
                    findResult.clear();
                }
            });
        }
        
//        @Override
//        public void updateUI() {
//            setUI(new javax.swing.plaf.basic.BasicTextFieldUI());
//        }
    }
    
    private void highlightFind() {
        JTextField findField = this.findField;
        if (findField != null) {
            highlightFind(findField.getText());
        }
    }
    
    private void highlightFind(String text) {
        List<Line> lines = lineConsumer.allLines;
        highlightFind(text, 0, lines.size());
    }
    
    private void highlightFind(String text, int start, int end) {
        StyledDocument doc = log.getStyledDocument();
        
        boolean find = !text.isEmpty();
        int     len  = text.length();
        
        List<Line> lines = lineConsumer.allLines;
        
        for (int i = start; i < end; ++i) {
            Line line = lines.get(i);
            int  offs = line.offs;
            
            if (line.marked) {
                line.marked = false;
                
                if (offs >= 0) {
                    doc.setCharacterAttributes(offs, line.line.getLine().length(), getAttributes(line.style), true);
                }
            }
            
            if (find) {
                String log  = line.line.getLine();
                int    from = 0;
                for (int index; (index = log.indexOf(text, from)) >= 0;) {
                    line.marked = true;
                    from        = index + len;
                    doc.setCharacterAttributes(offs + index, len, getHighlight(Theme.Key.NUMBER), true);
                }
            }
        }
    }
    
    private static final class FindResult {
        int  indexInList;
        int  indexInLine;
        Line line;
        
        FindResult clear() {
            line        = null;
            indexInList = -1;
            indexInLine = -1;
            return this;
        }
    }
    
    private void scrollLogTo(int offs) {
        try {
            Rectangle rect = log.modelToView(offs);
            log.scrollRectToVisible(rect);
        } catch (BadLocationException x) {
            Log.caught(SwingDebugWindow.class, "scrollLog", x, false);
        }
    }
    
    private void findNext() {
        FindResult     fr    = findResult;
        String         text  = findField.getText();
        int            len   = text.length();
        StyledDocument doc   = log.getStyledDocument();
        List<Line>     lines = lineConsumer.allLines;
        int            count = lines.size();
        int            start = 0;
        
        if (fr.line != null) {
            int offs   = fr.line.offs;
            int inLine = fr.indexInLine;
            doc.setCharacterAttributes(offs + inLine, len, getHighlight(Theme.Key.NUMBER), true);
            
            inLine = fr.line.line.getLine().indexOf(text, inLine + len);
            if (inLine >= 0) {
                fr.indexInLine = inLine;
                doc.setCharacterAttributes(offs + inLine, len, getHighlight(Theme.Key.FUNCTION), true);
                scrollLogTo(offs);
                return;
            }
            
            start = fr.indexInList + 1;
        }
        
        for (int i = start; i < (start + count); ++i) {
            int  inList = i % count;
            Line line   = lines.get(inList);
            
            if (!line.marked)
                continue;
            int inLine = line.line.getLine().indexOf(text);
            if (inLine < 0)
                continue;
            
            fr.line        = line;
            fr.indexInList = inList;
            fr.indexInLine = inLine;
            int       offs = line.offs;
            
            doc.setCharacterAttributes(offs + inLine, len, getHighlight(Theme.Key.FUNCTION), true);
            scrollLogTo(offs + inLine);
            return;
        }
    }
    
    private void findPrevious() {
        FindResult     fr    = findResult;
        String         text  = findField.getText();
        int            len   = text.length();
        StyledDocument doc   = log.getStyledDocument();
        List<Line>     lines = lineConsumer.allLines;
        int            count = lines.size();
        int            start = count - 1;
        
        if (fr.line != null) {
            int offs   = fr.line.offs;
            int inLine = fr.indexInLine;
            doc.setCharacterAttributes(offs + inLine, len, getHighlight(Theme.Key.NUMBER), true);
            
            inLine = fr.line.line.getLine().lastIndexOf(text, inLine - len);
            if (inLine >= 0) {
                fr.indexInLine = inLine;
                doc.setCharacterAttributes(offs + inLine, len, getHighlight(Theme.Key.FUNCTION), true);
                scrollLogTo(offs + inLine);
                return;
            }
            
            start = fr.indexInList - 1;
        }
        
        for (int i = start; i > (start - count); --i) {
            int  inList = (i + count) % count;
            Line line   = lines.get(inList);
            
            if (!line.marked)
                continue;
            int inLine = line.line.getLine().lastIndexOf(text);
            if (inLine < 0)
                continue;
            
            fr.line        = line;
            fr.indexInList = inList;
            fr.indexInLine = inLine;
            int       offs = line.offs;
            
            doc.setCharacterAttributes(offs + inLine, len, getHighlight(Theme.Key.FUNCTION), true);
            scrollLogTo(offs + inLine);
            return;
        }
    }
    
    private static final class AttributesListener<T> extends SwingSettingWeakListener<T, SwingDebugWindow> {
        AttributesListener(SwingDebugWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnEdt(SwingDebugWindow window,
                                           Settings         settings,
                                           Setting<T>       setting,
                                           T                oldValue,
                                           T                newValue) {
            window.attributes.clear();
            window.highlights.clear();
            window.lineConsumer.reset();
        }
    }
    
    private AttributeSet getAttributes(Theme.Key key) {
        return attributes.computeIfAbsent(key, CREATING_ATTRIBUTES);
    }
    
    private AttributeSet getHighlight(Theme.Key key) {
        return highlights.computeIfAbsent(key, CREATING_HIGHLIGHT);
    }
    
    private static final Function<Theme.Key, AttributeSet> CREATING_ATTRIBUTES =
        key -> SwingMisc.toAttributeSet(Theme.getAbsoluteStyle(key.getSetting()));
    
    private static final Function<Theme.Key, AttributeSet> CREATING_HIGHLIGHT =
        key -> createHighlight(key.getSetting());
    
    private static AttributeSet createHighlight(Setting<Style> set) {
        Style     style      = Theme.getAbsoluteStyle(set);
        ArgbColor foreground = style.getForeground();
        ArgbColor background = style.getBackground();
        if (background == null)
            background = app().getSetting(Setting.BACKGROUND_COLOR);
        style = style.setForeground(background).setBackground(foreground);
        return SwingMisc.toAttributeSet(style);
    }
    
    private final class LogFilterModel extends AbstractListModel<Predicate<Log.Line>> implements ListSelectionListener {
        private final List<Predicate<Log.Line>> model    = NonNullList.of(new ArrayList<>());
        private final List<Predicate<Log.Line>> selected = NonNullList.of(new ArrayList<>());
        
        private final Set<String> keys = new HashSet<>();
        
        LogFilterModel() {
            for (Predicate<Log.Line> filter : LogFilters.values())
                addFilter(filter);
        }
        
        List<Predicate<Log.Line>> getSelected() {
            return selected;
        }
        
        void addFilter(Predicate<Log.Line> filter) {
            int size = model.size();
            model.add(filter);
            fireIntervalAdded(this, size, size);
        }
        
        boolean addFilter(String key) {
            if (keys.add(key)) {
                addFilter(new LogFilter(key));
                return true;
            }
            return false;
        }
        
        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                selected.clear();
                selected.addAll(filters.getSelectedValuesList());
                lineConsumer.reset();
            }
        }
        
        @Override
        public Predicate<Log.Line> getElementAt(int index) {
            return model.get(index);
        }
        
        @Override
        public int getSize() {
            return model.size();
        }
    }
    
    private enum LogFilters implements Predicate<Log.Line> {
        STACK_TRACE {
            @Override
            public boolean test(Log.Line line) {
                return line.isStackTraceElement();
            }
            @Override
            public String toString() {
                return "Stack Trace";
            }
        },
        ALL {
            @Override
            public boolean test(Log.Line line) {
                return true;
            }
            @Override
            public String toString() {
                return "eps (all)";
            }
        }
    }
    
    private static final class LogFilter implements Predicate<Log.Line> {
        private final String key;
        
        private LogFilter(String key) {
            this.key = Objects.requireNonNull(key, "key");
        }
        
        @Override
        public boolean test(Log.Line line) {
            return line.getName().contains(key);
        }
        
        @Override
        public String toString() {
            return key;
        }
        
        @Override
        public int hashCode() {
            return key.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return (obj instanceof LogFilter) && Objects.equals(this.key, ((LogFilter) obj).key);
        }
    }
    
    private final class LogLineNotifier implements Consumer<java.util.logging.LogRecord>, Runnable {
        @Override
        public void accept(java.util.logging.LogRecord rec) {
            if (SwingUtilities.isEventDispatchThread()) {
                run();
            } else {
                SwingUtilities.invokeLater(this);
            }
        }
        @Override
        public void run() {
            toHighFreqLog();
        }
    }
    
    private static final class Line {
        final Log.Line line;
        Theme.Key      style;
        int            offs;
        boolean        marked;
        
        Line(Log.Line line) {
            this.line   = line;
            this.style  = null;
            this.offs   = -1;
            this.marked = false;
        }
    }
    
    private final class LogLineConsumer implements ActionListener, Consumer<ListDeque<Log.Line>>, Predicate<Log.Line> {
        private final    AttributeSet     noAttributes = new SimpleAttributeSet();
        private final    Log.LineReceiver receiver     = Log.newLineReceiver(256);
        
        private final List<Line> allLines = new ArrayList<>(512);
        
        private void reset() {
            requireEdt();
            log.setText("");
            display(0, allLines.size());
            
            Line line = findResult.line;
            if (line != null) {
                if (line.marked) {
                    int offs = line.offs;
                    if (offs >= 0) {
                        int index = findResult.indexInLine;
                        int len   = findField.getText().length();
                        log.getStyledDocument().setCharacterAttributes(offs + index, len, getHighlight(Theme.Key.FUNCTION), true);
                    }
                } else {
                    findResult.clear();
                }
            }
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!receiver.receive(this)) {
                int delay      = lineTimer.getDelay();
                int loInterval = loFreqLogInterval;
                
                if (delay != loInterval) {
                    lineTimer.setDelay(loInterval);
                    lineTimer.setInitialDelay(loInterval);
                    lineTimer.restart();
                }
            }
        }
        
        @Override
        public boolean test(Log.Line line) {
            List<Predicate<Log.Line>> filters = filterModel.getSelected();
            int count = filters.size();
            if (count > 0) {
                for (int i = 0; i < count; ++i)
                    if (filters.get(i).test(line))
                        return true;
                return false;
            }
            return true;
        }
        
        @Override
        public void accept(ListDeque<Log.Line> lines) {
            LogFilterModel filters = filterModel;
            List<Line>     all     = allLines;
            int            start   = all.size();
            int            size    = lines.size();
            
            for (int i = 0; i < size; ++i) {
                Log.Line line = lines.get(i);
                
                all.add(new Line(line));
                filters.addFilter(line.getName());
            }
            
            display(start, all.size());
        }
        
        private void display(int start, int end) {
            StyledDocument document = log.getStyledDocument();
            List<Line>     allLines = this.allLines;
            AttributeSet   noAtts   = this.noAttributes;
            
            for (int i = start; i < end; ++i) {
                Line     line = allLines.get(i);
                Log.Line log  = line.line;
                
                if (!test(log)) {
                    line.offs = -1;
                    line.marked = false;
                    continue;
                }
                
                try {
                    int len = document.getLength();
                    if (len == 0) {
                        line.offs = 0;
                    } else {
                        document.insertString(len, "\n", noAtts);
                        line.offs = len = document.getLength();
                    }
                    
                    Theme.Key style = line.style;
                    if (style == null) {
                        style = line.style = log.isStackTraceElement() ? Theme.Key.INTERNAL_ERROR : Theme.Key.BASIC;
                    }
                    
                    document.insertString(len, log.getLine(), getAttributes(style));
                } catch (BadLocationException x) {
                    Log.caught(LogLineConsumer.class, "accept", x, false);
                }
            }
            
            highlightFind(findField.getText(), start, end);
        }
    }
    
    private static final class LogLineIntervalListener extends SwingSettingWeakListener<Integer, SwingDebugWindow> {
        private LogLineIntervalListener(SwingDebugWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnEdt(SwingDebugWindow window,
                                           Settings         settings,
                                           Setting<Integer> setting,
                                           Integer          oldValue,
                                           Integer          newValue) {
            if (setting == Setting.LO_FREQ_LOG_INTERVAL) {
                window.loFreqLogInterval = newValue;
            } else if (setting == Setting.HI_FREQ_LOG_INTERVAL) {
                window.hiFreqLogInterval = newValue;
            }
        }
    }
    
    private static final class UpdateStyleListener<T> extends SwingSettingWeakListener<T, SwingDebugWindow> {
        private UpdateStyleListener(SwingDebugWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnEdt(SwingDebugWindow window,
                                           Settings         settings,
                                           Setting<T>       setting,
                                           T                oldValue,
                                           T                newValue) {
            window.updateStyle();
        }
    }
}