/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import java.util.*;

class SafeChangeTextField extends JTextField {
    SafeChangeTextField() {
    }
    
    SafeChangeTextField(int cols) {
        super(cols);
    }
    
    protected boolean isText(String text) {
        return Objects.equals(getText(), text);
//        Document doc = getDocument();
//        try {
//            return Objects.equals(doc.getText(0, doc.getLength()), text);
//        } catch (BadLocationException x) {
//            Log.caught(SafeChangeTextField.class, "isText", x, false);
//            return Objects.equals(getText(), text);
//        }
    }
    
    @Override
    public void setText(String text) {
        if (isText(text))
            return;
        
        Document doc = getDocument();
        DocumentListener[] listeners = null;
        
        if (doc instanceof AbstractDocument) {
            listeners = ((AbstractDocument) doc).getDocumentListeners();
            int len = listeners.length;
            for (int i = 0; i < len; ++i) {
                DocumentListener dl = listeners[i];
                if (dl.getClass().getName().startsWith("javax.swing.")) {
                    listeners[i] = null;
                } else {
                    doc.removeDocumentListener(dl);
                }
            }
        }
        
        try {
            super.setText(text);
        } finally {
            if (listeners != null) {
                for (DocumentListener dl : listeners) {
                    if (dl != null) {
                        doc.addDocumentListener(dl);
                    }
                }
            }
        }
    }
}