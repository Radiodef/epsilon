/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.util.function.*;

final class ColorSelector extends JComponent {
    private final Swatch    swatch;
    private final JButton   button;
    private final JCheckBox unspec;
    
    private ChangeListener[]  changeListeners = new ChangeListener[0];
    private final ChangeEvent changeEvent     = new ChangeEvent(this);
    private ActionListener[]  actionListeners = new ActionListener[0];
    private final ActionEvent actionEvent     = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null);
    
    private final boolean allowUnspecified;
    private final Supplier<ArgbColor> unspecifiedColor;
    
    private ArgbColor color = ArgbColor.BLACK;
    
    ColorSelector() {
        this(null);
    }
    
    ColorSelector(String label) {
        this(label, null, null);
    }
    
    ColorSelector(String label, String unspecLabel, Supplier<ArgbColor> unspecifiedColor) {
        setLayout(new BorderLayout());
        setAlignmentY(CENTER_ALIGNMENT);
        
        if (label != null) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), label));
        } else {
            setBorder(BorderFactory.createEtchedBorder());
        }
        
        button = new JButton("Choose");
        swatch = new Swatch();
        
        allowUnspecified = (unspecifiedColor != null);
        
        if (allowUnspecified) {
            unspec = new JCheckBox(unspecLabel == null ? "Default" : unspecLabel);
            
            unspec.setOpaque(false);
            unspec.setAlignmentY(CENTER_ALIGNMENT);
            
            this.unspecifiedColor = Objects.requireNonNull(unspecifiedColor, "unspecifiedColor");
        } else {
            unspec = null;
            this.unspecifiedColor = null;
        }
        
        JPanel swatchPane = new JPanel(new FlowLayout());
        swatchPane.setOpaque(false);
        swatchPane.add(swatch);
        
        add(swatchPane, BorderLayout.WEST);
        add(button,     BorderLayout.CENTER);
        
        if (unspec != null) {
            JPanel unspecPane = new JPanel();
            unspecPane.setLayout(new BoxLayout(unspecPane, BoxLayout.X_AXIS));
            unspecPane.setOpaque(false);
            unspecPane.add(unspec);
            
            add(unspecPane, BorderLayout.EAST);
        }
        
        setColor(color);
        
        button.addActionListener(e -> {
            String title  = (label == null) ? "Choose Color" : ("Choose " + label);
            Color  choice = JColorChooser.showDialog(this, title, getForeground());
            if (choice != null) {
                setColor(SwingMisc.fromAwtColor(choice));
                actionPerformed();
                updateForeground();
            }
        });
        
        if (unspec != null) {
            unspec.addActionListener(e -> {
                forceSetUnspecified(unspec.isSelected(), true);
                actionPerformed();
                updateForeground();
            });
        }
    }
    
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (allowUnspecified) {
            button.setEnabled(enabled && !unspec.isSelected());
            unspec.setEnabled(enabled);
        } else {
            button.setEnabled(enabled);
        }
    }
    
    private void stateChanged() {
        for (ChangeListener listener : changeListeners) {
            listener.stateChanged(changeEvent);
        }
    }
    
    public void addChangeListener(ChangeListener l) {
        changeListeners = Misc.append(changeListeners, Objects.requireNonNull(l, "l"));
    }
    
    public void removeChangeListener(ChangeListener l) {
        changeListeners = Misc.remove(changeListeners, Objects.requireNonNull(l, "l"));
    }
    
    public ChangeListener[] getChangeListeners() {
        return changeListeners.clone();
    }
    
    private void actionPerformed() {
        for (ActionListener listener : actionListeners) {
            listener.actionPerformed(actionEvent);
        }
    }
    
    public void addActionListener(ActionListener l) {
        actionListeners = Misc.append(actionListeners, Objects.requireNonNull(l, "l"));
    }
    
    public void removeActionListener(ActionListener l) {
        actionListeners = Misc.remove(actionListeners, Objects.requireNonNull(l, "l"));
    }
    
    public ActionListener[] getActionListeners() {
        return actionListeners.clone();
    }
    
    public void setUnspecified(boolean isUnspecified) {
        setUnspecified(isUnspecified, true);
    }
    
    private void setUnspecified(boolean isUnspecified, boolean notify) {
        setUnspecified(isUnspecified, notify, false);
    }
    
    private void forceSetUnspecified(boolean isUnspecified, boolean notify) {
        setUnspecified(isUnspecified, notify, true);
    }
    
    private void setUnspecified(boolean isUnspecified, boolean notify, boolean force) {
        if (!allowUnspecified) {
            if (isUnspecified) {
                throw new IllegalStateException();
            }
            return;
        }
        if (force || isUnspecified != unspec.isSelected()) {
            unspec.setSelected(isUnspecified);
            button.setEnabled(!isUnspecified);
            if (notify) {
                stateChanged();
            }
        }
        updateForeground();
    }
    
    public boolean isUnspecified() {
        return allowUnspecified && unspec.isSelected();
    }
    
    public void setColor(ArgbColor color) {
        if (color == null) {
            setUnspecified(true, true);
            return;
        } // else
        setUnspecified(false, false);
        
        if (!Objects.equals(this.color, color)) {
            this.color = color;

            stateChanged();
        }
        updateForeground();
    }
    
    public ArgbColor getColor() {
        if (isUnspecified()) {
            return null;
        } else {
            return color;
        }
    }
    
    private void updateForeground() {
        ArgbColor foreground = isUnspecified() ? unspecifiedColor.get() : this.color;
        setForeground(SwingMisc.toAwtColor(foreground));
    }
    
    @Override
    public Color getForeground() {
        return swatch.getForeground();
    }
    
    @Override
    public void setForeground(Color color) {
        swatch.setForeground(color);
    }
    
    @Override
    public void setFont(Font font) {
        button.setFont(font);
    }
    
    @Override
    public Font getFont() {
        return button.getFont();
    }
    
    @Override
    public Dimension getMaximumSize() {
        Dimension max = super.getMaximumSize();
        max.height = getPreferredSize().height;
        return max;
    }
    
    public JComponent getSwatch() {
        return swatch;
    }
    
    public JComponent getButton() {
        return button;
    }
    
    public JComponent getCheckBox() {
        return unspec;
    }
}