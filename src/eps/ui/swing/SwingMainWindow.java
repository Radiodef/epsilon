/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.util.*;
import static eps.ui.swing.SwingMisc.*;
import static eps.app.App.*;
import static eps.util.Literals.*;

import java.util.*;
import java.beans.*;
import java.lang.reflect.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.Timer;

// TODO:
//  * find a nice border design for the scroll panes
//  * entryField JPopupMenu (copy, cut, paste, select all, |, prev, next, etc.)
final class SwingMainWindow implements MainWindow {
    private final JFrame frame;
    
    private final ToolBar toolBar;
    private final CellPanel cellPanel;
    private final JScrollPane scrollPane;
    private final JSplitPane splitPane;
    private final MultilineEditor multiEntry;
    
    private final Settings.Listeners settingsListeners;
    private final KeyboardFocusManager focusManager;
    private final KeyEventDispatcher keyDispatcher;
    
    private final EventCallbacks callbacks = new EventCallbacks();
    
    private boolean isMultilineMode = false;
    
    SwingMainWindow() {
        Log.constructing(SwingMainWindow.class);
        requireEdt();
        
        Settings settings = app().getSettings();
        this.settingsListeners = new Settings.Listeners(settings);
        
        toolBar = new ToolBar();
        
        cellPanel = new CellPanel();
        
        scrollPane =
            new AutoScrollPane(cellPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                          JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        multiEntry = new MultilineEditor();
        
//        scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        multiScroll.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        
        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
        splitPane.setResizeWeight(1);
        // There's a dot in the MacOSX LAF which makes a small divider look bad.
//        splitPane.setDividerSize(3);
        
        splitPane.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new SplitPaneDividerListener(this));
        
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                app().exit();
            }
            @Override
            public void windowActivated(WindowEvent e) {
                if (isAlwaysOnTop())
                    frame.toFront();
            }
        });
        
        SwingMisc.onFirstOpen(frame, () -> {
            Insets    ins  = frame.getInsets();
            Dimension size = frame.getContentPane().getMinimumSize();
            size.width  += ins.left + ins.right;
            size.height += ins.top  + ins.bottom;
            frame.setMinimumSize(size);
            
            callbacks.windowFirstShown(SwingMainWindow.this);
        });
        
        frame.setContentPane(new JPanel());
        
        setMultilineMode(settings.get(Setting.MULTILINE_MODE), true);
        
        WindowBoundsListener boundsListener = new WindowBoundsListener();
        frame.addComponentListener(boundsListener);
        frame.addWindowStateListener(boundsListener);
        
        if (SystemInfo.isMacOsx()) {
            SwingMisc.configureFrameForMac(frame);
        }
        
        keyDispatcher = new TextEntryKeyHandler();
        focusManager  = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        focusManager.addKeyEventDispatcher(keyDispatcher);
        
        settingsListeners.add(Setting.MAIN_WINDOW_ALWAYS_ON_TOP, new AlwaysOnTopSettingListener(this))
                         .add(Setting.MULTILINE_MODE,            new MultiLineModeSettingListener(this))
                         .addAndSet(Setting.BACKGROUND_COLOR,    new BackgroundColorSettingListener(this));
    }
    
    JFrame getFrame() {
        requireEdt();
        return frame;
    }
    
    CellPanel getCellPanel() {
        requireEdt();
        return cellPanel;
    }
    
    enum WindowState {
        WINDOWED,
        MAXIMIZED;
    }
    
    private WindowState windowState = WindowState.WINDOWED;
    
    void setWindowState(WindowState windowState) {
        Objects.requireNonNull(windowState, "windowState");
        
        WindowState prevState = this.windowState;
        this.windowState = windowState;
        
        if (windowState != prevState) {
            switch (windowState) {
                case MAXIMIZED: callbacks.windowToMaximized(this);
                                break;
                case WINDOWED:  callbacks.windowToWindowed(this);
                                break;
            }
            
            this.windowState = windowState;
            Log.note(SwingMainWindow.class, "setWindowState", "windowState set to ", windowState);
        }
    }
    
    @Override
    public void close() {
        Log.entering(SwingMainWindow.class, "close");
        
        if (SwingUtilities.isEventDispatchThread()) {
            toolBar.close();
            cellPanel.close();
            multiEntry.close();
            settingsListeners.close();
            focusManager.removeKeyEventDispatcher(keyDispatcher);
            frame.setVisible(false);
            frame.dispose();
        } else {
            try {
                SwingUtilities.invokeAndWait(this::close);
            } catch (InterruptedException x) {
                Log.caught(SwingMainWindow.class, "Unexpected InterruptedException in close().", x, false);
            } catch (InvocationTargetException x) {
                Misc.rethrowCause(x);
            }
        }
    }
    
    @Override
    public BoundingBox getBounds() {
        requireEdt();
        Rectangle rect = frame.getBounds();
        return new BoundingBox(rect.y,
                               rect.x + rect.width,
                               rect.y + rect.height,
                               rect.x);
    }
    
    @Override
    public void setBounds(BoundingBox bounds) {
        requireEdt();
        if (bounds != null) {
            frame.setBounds((int) bounds.west,
                            (int) bounds.north,
                            (int) bounds.width(),
                            (int) bounds.height());
        } else {
            Dimension screenSize  = SwingMisc.getAvailableScreenSize(frame);
            Log.note(SwingMainWindow.class, "setBounds", "screenSize = ", screenSize);
            double    aspectRatio = (double) screenSize.width / (double) screenSize.height;
            double    newArea     = screenSize.width * screenSize.height * app().getSetting(Setting.DEFAULT_WINDOW_AREA_RATIO);
            screenSize.width      = (int) Math.sqrt(newArea * aspectRatio);
            screenSize.height     = (int) (screenSize.width / aspectRatio);
            frame.setSize(screenSize);
            
            frame.setLocationRelativeTo(null);
        }
        frame.revalidate();
        frame.repaint();
    }
    
    @Override
    public boolean isVisible() {
        requireEdt();
        return frame.isVisible();
    }
    
    @Override
    public void setVisible(boolean shouldBeVisible) {
        requireEdt();
        frame.setVisible(shouldBeVisible);
    }
    
    @Override
    public boolean isAlwaysOnTop() {
        requireEdt();
        return frame.isAlwaysOnTop();
    }
    
    @Override
    public void setAlwaysOnTop(boolean shouldBeAlwaysOnTop) {
        requireEdt();
        frame.setAlwaysOnTop(shouldBeAlwaysOnTop);
        toolBar.alwaysOnTop.setSelected(shouldBeAlwaysOnTop);
//        for (java.awt.Window w : SwingMisc.getChildren(frame)) {
//            w.setAlwaysOnTop(shouldBeAlwaysOnTop);
//        }
//        frame.toFront();
    }
    
    @Override
    public boolean isMaximized() {
        requireEdt();
        return windowState == WindowState.MAXIMIZED;
    }
    
    @Override
    public boolean isWindowed() {
        requireEdt();
        return windowState == WindowState.WINDOWED;
    }
    
    @Override
    public void setMaximized(boolean isMaximized) {
        WindowState prevState = windowState;
        setWindowState(isMaximized ? WindowState.MAXIMIZED : WindowState.WINDOWED);
        
        if (SystemInfo.isMacOsx()) {
            // Mac needs special handling.
            boolean wasMaximized = (prevState == WindowState.MAXIMIZED);
            
            if (wasMaximized != isMaximized) {
                SwingMisc.toggleFullScreenForMac(frame);
            }
        } else {
            if (isMaximized) {
                frame.setExtendedState(frame.getExtendedState() | Frame.MAXIMIZED_BOTH);
            } else {
                frame.setExtendedState(frame.getExtendedState() & ~Frame.MAXIMIZED_BOTH);
            }
        }
    }
    
    @Override
    public String getTitleText() {
        requireEdt();
        return frame.getTitle();
    }
    
    @Override
    public void setTitleText(String titleText) {
        requireEdt();
        frame.setTitle(titleText);
    }
    
    @Override
    public void addEventCallback(EventCallback callback) {
        requireEdt();
        callbacks.add(callback);
    }
    
    @Override
    public void removeEventCallback(EventCallback callback) {
        requireEdt();
        callbacks.remove(callback);
    }
    
    @Override
    public String getCommandText() {
        requireEdt();
        return multiEntry.getText();
    }
    
    @Override
    public void setCommandText(String commandText) {
        requireEdt();
        multiEntry.setText(commandText);
    }
    
    @Override
    public int indexOfCell(Cell cell) {
        requireEdt();
        return cellPanel.indexOfCell(cell);
    }
    
    @Override
    public void addCell(Cell cell) {
        Log.entering(SwingMainWindow.class, "addCell");
        requireEdt();
        cellPanel.addCell(cell);
    }
    
    @Override
    public void insertCell(int index, Cell cell) {
        Log.entering(SwingMainWindow.class, "insertCell");
        requireEdt();
        cellPanel.insertCell(index, cell);
    }
    
    @Override
    public void removeCell(Cell cell) {
        Log.entering(SwingMainWindow.class, "removeCell");
        requireEdt();
        cellPanel.removeCell(cell);
    }
    
    boolean isMultilineMode() {
        return this.isMultilineMode;
    }
    
    private void setMultilineMode(boolean isMultilineMode, boolean isInit) {
        if ((isMultilineMode == this.isMultilineMode) && !isInit)
            return;
        boolean entryHadFocus = multiEntry.hasFocus();
        this.isMultilineMode  = isMultilineMode;
        toolBar.multiline.setSelected(isMultilineMode);
        
        JPanel content = (JPanel) frame.getContentPane();
        content.removeAll();
        
        if (isMultilineMode) {
            splitPane.setTopComponent(scrollPane);
            splitPane.setBottomComponent(multiEntry.getScrollPane());
            content.setLayout(new BorderLayout());
            content.add(toolBar,   BorderLayout.NORTH);
            content.add(splitPane, BorderLayout.CENTER);
            
            SwingUtilities.invokeLater(new Runnable() {
                private final Double height = app().getSetting(Setting.MULTILINE_ENTRY_HEIGHT);
                @Override
                public void run() {
                    int maximum = splitPane.getMaximumDividerLocation();
                    int minimum = splitPane.getMinimumDividerLocation();
                    if (maximum < 0 || minimum < 0 || !splitPane.isDisplayable()) {
                        SwingUtilities.invokeLater(this);
                    } else {
                        int location;
                        if ((height == null) || (location = maximum - (int) Math.round(height)) < minimum) {
                            splitPane.setDividerLocation(2.0 / 3.0);
                        } else {
                            splitPane.setDividerLocation(location);
                        }
                    }
                }
            });
        } else {
            splitPane.setTopComponent(null);
            splitPane.setBottomComponent(null);
            
            content.setLayout(new BorderLayout());
            content.add(toolBar, BorderLayout.NORTH);
            content.add(scrollPane, BorderLayout.CENTER);
            content.add(multiEntry.getScrollPane(), BorderLayout.SOUTH);
            
//            content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
//            content.add(toolBar);
//            content.add(scrollPane);
//            content.add(multiEntry.getScrollPane());
        }
        multiEntry.setMultilineMode(isMultilineMode);
        
        if (frame.isDisplayable()) {
            content.revalidate();
            content.repaint();
        } else {
            frame.pack(); // only to make the frame displayable; the bounds get set later
        }
        
        if (entryHadFocus) {
            SwingUtilities.invokeLater(new Runnable() {
                private int attempts = 0;
                @Override
                public void run() {
                    if (!multiEntry.hasFocus() && attempts < 4) {
                        ++attempts;
                        multiEntry.requestFocus();
                        SwingUtilities.invokeLater(this);
                    }
                }
            });
        }
    }
    
    // TODO: Perhaps some abstraction is in order here.
    private class TextEntryKeyHandler implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent evt) {
            int keyCode = evt.getKeyCode();
            
            boolean isMultiLineMode = SwingMainWindow.this.isMultilineMode();
            
            switch (keyCode) {
                case KeyEvent.VK_UP:
                case KeyEvent.VK_DOWN:
                case KeyEvent.VK_ENTER:
                    boolean entryHasFocus = multiEntry.hasFocus();
                    if (entryHasFocus) {
                        int     modifiers    = evt.getModifiersEx();
                        boolean hasModifiers =
                            isMultiLineMode ? (modifiers == KeyEvent.CTRL_DOWN_MASK)
                                            : ((modifiers | KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK);
                        
                        if (hasModifiers) {
                            int triggerID =
                                (keyCode == KeyEvent.VK_ENTER) ? KeyEvent.KEY_PRESSED : KeyEvent.KEY_RELEASED;
                            
                            if (evt.getID() == triggerID) {
                                switch (keyCode) {
                                    case KeyEvent.VK_UP:
                                        callbacks.historyPrevious(SwingMainWindow.this);
                                        break;
                                    case KeyEvent.VK_DOWN:
                                        callbacks.historyNext(SwingMainWindow.this);
                                        break;
                                    case KeyEvent.VK_ENTER:
                                        String cmd = SwingMainWindow.this.getCommandText();
                                        callbacks.commandSent(SwingMainWindow.this, cmd);
                                        break;
                                }
                            }
                            return true;
                        }
                    }
                    break;
            }
            
            return false;
        }
    }
    
    private class WindowBoundsListener extends ComponentAdapter implements ActionListener, WindowStateListener {
        private final int   timeout = 1500;
        private final Timer timer   = new Timer(timeout, this);
        
        private WindowBoundsListener() {
            timer.setRepeats(false);
            timer.setCoalesce(false);
        }
        
        private void changing() {
            if (frame.isVisible()) {
                if (windowState == WindowState.WINDOWED) {
                    if (timer.isRunning()) {
                        timer.restart();
                    } else {
                        timer.start();
                    }
                } else {
                    timer.stop();
                }
            }
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            callbacks.windowBoundsChanged(SwingMainWindow.this, getBounds());
        }
        
        @Override
        public void componentMoved(ComponentEvent e) {
            changing();
        }
        
        @Override
        public void componentResized(ComponentEvent e) {
            changing();
        }
        
        private boolean isMaximized(int state) {
            return (state & (Frame.MAXIMIZED_BOTH | Frame.MAXIMIZED_HORIZ | Frame.MAXIMIZED_VERT)) != 0;
        }
        private boolean maximized(WindowEvent e) {
            return !isMaximized(e.getOldState()) && isMaximized(e.getNewState());
        }
        private boolean windowed(WindowEvent e) {
            return isMaximized(e.getOldState()) && !isMaximized(e.getNewState());
        }
        private boolean isIconified(int state) {
            return (state & Frame.ICONIFIED) != 0;
        }
        private boolean iconified(WindowEvent e) {
            return !isIconified(e.getOldState()) && isIconified(e.getNewState());
        }
        private boolean deiconified(WindowEvent e) {
            return isIconified(e.getOldState()) && !isIconified(e.getNewState());
        }
        
        @Override
        public void windowStateChanged(WindowEvent e) {
            if (!SystemInfo.isMacOsx()) {
                if (maximized(e)) {
                    setWindowState(WindowState.MAXIMIZED);
                } else if (windowed(e)) {
                    setWindowState(WindowState.WINDOWED);
                }
            }
        }
    }
    
    private static final class SplitPaneDividerListener implements PropertyChangeListener, ActionListener {
        private final java.lang.ref.WeakReference<SwingMainWindow> window;
        
        private       Double height;
        private final Timer  timer;
        
        private SplitPaneDividerListener(SwingMainWindow window) {
            this.window = new java.lang.ref.WeakReference<>(Objects.requireNonNull(window, "window"));
            this.timer  = new Timer(400, this);
            timer.setRepeats(false);
            timer.setCoalesce(false);
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            SwingMainWindow window = this.window.get();
            if (window != null) {
                app().getSettings().set(Setting.MULTILINE_ENTRY_HEIGHT, height);
            }
        }
        
        @Override
        public void propertyChange(PropertyChangeEvent e) {
            JSplitPane split  = (JSplitPane) e.getSource();
            Integer    val    = (Integer) e.getNewValue();
            this.height       = (val == null || val < 0) ? null : (double) (split.getMaximumDividerLocation() - val);
            
            SwingMainWindow window = this.window.get();
            if (window == null) {
                split.removePropertyChangeListener(this);
                Log.note(SplitPaneDividerListener.class, "propertyChange", "null window");
            } else {
                if (timer.isRunning()) {
                    timer.restart();
                } else {
                    timer.start();
                }
            }
        }
    }
    
    private static class AlwaysOnTopSettingListener extends SwingSettingWeakListener<Boolean, SwingMainWindow> {
        private AlwaysOnTopSettingListener(SwingMainWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnEdt(SwingMainWindow  window,
                                           Settings         settings,
                                           Setting<Boolean> setting,
                                           Boolean          wasAlwaysOnTop,
                                           Boolean          isAlwaysOnTop) {
            
            window.setAlwaysOnTop(isAlwaysOnTop);
        }
    }
    
    private static class MultiLineModeSettingListener extends SwingSettingWeakListener<Boolean, SwingMainWindow> {
        private MultiLineModeSettingListener(SwingMainWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnEdt(SwingMainWindow  window,
                                           Settings         settings,
                                           Setting<Boolean> setting,
                                           Boolean          wasMultilineMode,
                                           Boolean          isMultilLineMode) {
            window.setMultilineMode(isMultilLineMode, false);
        }
    }
    
    private static class BackgroundColorSettingListener extends SwingSettingWeakListener<ArgbColor, SwingMainWindow> {
        private BackgroundColorSettingListener(SwingMainWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnEdt(SwingMainWindow    window,
                                           Settings           settings,
                                           Setting<ArgbColor> setting,
                                           ArgbColor          oldBackground,
                                           ArgbColor          newBackground) {
            window.frame.getContentPane().setBackground(toAwtColor(newBackground));
        }
    }
    
    private static final class ToolBar extends JPanel implements AutoCloseable {
        private final int gap;
        
        private final CompactButton settings;
        private final CompactButton symbols;
        private final CompactButton multiline;
        private final CompactButton alwaysOnTop;
        
        private ToolBar() {
            gap = Settings.getOrDefault(Setting.MAJOR_MARGIN);
            
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            setBorder(BorderFactory.createEmptyBorder(gap, gap, gap, gap));
            
            setOpaque(false);
            setAlignmentX(Component.LEFT_ALIGNMENT);
            
            settings    = new CompactButton("Settings");
            symbols     = new CompactButton("Symbols");
            alwaysOnTop = new CompactButton("Pin", true);
            multiline   = new CompactButton("Multiline", true);
            
            add(settings);
            addStrut();
            add(symbols);
            addStrut();
            add(multiline);
            addStrut();
            add(alwaysOnTop);
            
            settings.setToolTipText("Opens the settings window if it's not already open.");
            symbols.setToolTipText("Opens the symbol editor window if it's not already open.");
            multiline.setToolTipText("Toggles whether or not the command entry text field allows multiline input.");
            alwaysOnTop.setToolTipText("Toggles whether or not the main window is always on top of all other windows.");
            
            settings.addActionListener(e -> {
                app().getSettingsWindow().show();
            });
            
            symbols.addActionListener(e -> {
                app().getSymbolsWindow().show();
            });
            
            multiline.addActionListener(e -> {
                app().getSettings().set(Setting.MULTILINE_MODE, multiline.isSelected());
            });
            
            alwaysOnTop.addActionListener(e -> {
                app().getSettings().set(Setting.MAIN_WINDOW_ALWAYS_ON_TOP, alwaysOnTop.isSelected());
            });
        }
        
        @Override
        public void close() {
            ListOf(settings, symbols, multiline, alwaysOnTop).forEach(CompactButton::close);
        }
        
        private void addStrut() {
            addStrut(1);
        }
        
        private void addStrut(int multiple) {
            add(Box.createHorizontalStrut(multiple * gap));
        }
    }
}