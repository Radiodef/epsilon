/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.util.*;

import java.util.*;
import javax.swing.*;
import java.awt.*;

final class CellPanel extends JPanel implements AutoCloseable {
    private final Settings.Listeners listeners;
    
    private final int cellGap;
    
    private int cellCount = 0;
    
    CellPanel() {
        Settings settings = App.app().getSettings();
        this.listeners = new Settings.Listeners(settings);
        
        setBackground(SwingMisc.toAwtColor(settings.get(Setting.BACKGROUND_COLOR)));
        setOpaque(true);
        
        cellGap = settings.get(Setting.MINOR_MARGIN);
        
        int maj = settings.get(Setting.MAJOR_MARGIN);
        setBorder(BorderFactory.createEmptyBorder(maj, maj, maj, maj));
        
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        add(Box.createVerticalGlue());
        
        listeners.add(Setting.BACKGROUND_COLOR, new BackgroundColorSettingListener(this));
    }
    
    @Override
    public void close() {
        listeners.close();
    }
    
    int getCellGap() {
        return cellGap;
    }
    
    int getCellCount() {
        return cellCount;
    }
    
    int indexOfCell(Cell cell) {
        synchronized (getTreeLock()) {
            JComponent searchComp = ((SwingCell) cell).getComponent();
            
            int index = 0;
            for (JComponent cellComp : cellComponents()) {
                if (cellComp == searchComp)
                    return index;
                ++index;
            }
            
            return -1;
        }
    }
    
    void addCell(Cell cell) {
        synchronized (getTreeLock()) {
            JComponent comp = ((SwingCell) cell).getComponent();
            
            if (cellCount > 0) {
                add(Box.createVerticalStrut(cellGap));
            }
            
            add(comp);
            
            ++cellCount;
            revalidate();
            repaint();
        }
    }
    
    void insertCell(int index, Cell cell) {
        synchronized (getTreeLock()) {
            if (index < 0 || cellCount < index) {
                throw Errors.newIndexOutOfBounds("cell index", index);
            }
            if (index == cellCount) {
                addCell(cell);
                return;
            }
            index = 1 + (2 * index);
            
            JComponent comp = ((SwingCell) cell).getComponent();
            
            if (getComponentCount() > 1) {
                add(Box.createVerticalStrut(cellGap), index);
            }
            
            add(comp, index);
            ++cellCount;
            
            revalidate();
            repaint();
        }
    }
    
    void removeCell(Cell cell) {
        synchronized (getTreeLock()) {
            JComponent comp = ((SwingCell) cell).getComponent();
            
            Iterator<JComponent> it = new CellComponentIterator();
            
            while (it.hasNext()) {
                if (it.next() == comp) {
                    it.remove();
                    break;
                }
            }
        }
    }
    
    private boolean assertTreeLockHeld() {
        assert Thread.holdsLock(getTreeLock()) : "tree lock not held";
        return true;
    }
    
    private boolean assertIsWellFormed() {
        synchronized (getTreeLock()) {
            int count = getComponentCount();
            if (count > 0) {
                assert getComponent(0) instanceof Box.Filler;
                if (count > 1) {
                    assert (count & 1) == 0 : count;
                    
                    for (int i = 1; i < count; ++i) {
                        Component c = getComponent(i);
                        boolean isBoxExpected = ((i & 1) == 0);
                        assert isBoxExpected == (c instanceof Box.Filler);
                    }
                }
            }
        }
        return true;
    }
    
    private Iterable<JComponent> cellComponents() {
        return CellComponentIterator::new;
    }
    
    private class CellComponentIterator implements Iterator<JComponent> {
        {
            assert assertTreeLockHeld();
            assert assertIsWellFormed();
        }
        private int prev  = -1;
        private int index = 1;
        
        @Override
        public boolean hasNext() {
            assert assertTreeLockHeld();
            
            return index < getComponentCount();
        }
        
        @Override
        public JComponent next() {
            assert assertTreeLockHeld();
            
            if (hasNext()) {
                int index = this.index;
                JComponent comp = (JComponent) getComponent(index);
                this.prev  = index;
                this.index = index + 2;
                return comp;
            }
            
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            assert assertTreeLockHeld();
            if (prev < 0) {
                throw new IllegalStateException();
            }
            
            CellPanel.this.remove(prev);
            
            if (prev > 1) { // note: cell 0 is at index 1
                CellPanel.this.remove(prev - 1);
            } else if (getComponentCount() > 1) {
                CellPanel.this.remove(prev);
            }
            
            --cellCount;
            index -= 2;
            prev  = -1;
            
            revalidate();
            repaint();
            
            assert assertIsWellFormed();
        }
    }
    
    private Dimension restrict(Dimension dim) {
        Component parent = getParent();
        
        if (parent instanceof JViewport) {
            dim.width = ((JViewport) parent).getWidth();
        }
        
        return dim;
    }
    
    @Override
    public Dimension getMaximumSize() {
        return this.restrict(super.getMaximumSize());
    }
    
    @Override
    public Dimension getMinimumSize() {
        return this.restrict(super.getMinimumSize());
    }
    
    @Override
    public Dimension getPreferredSize() {
        return this.restrict(super.getPreferredSize());
    }
    
    private static class BackgroundColorSettingListener extends SwingSettingWeakListener.OfBackgroundColor<CellPanel> {
        private BackgroundColorSettingListener(CellPanel panel) {
            super(panel);
        }
        
        @Override
        protected CellPanel getComponent(CellPanel panel) {
            return panel;
        }
    }
}