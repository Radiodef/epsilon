/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.ui.Style;
import eps.app.*;
import eps.util.*;
import eps.io.*;
import static eps.app.App.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.Window;
import java.awt.event.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.function.*;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.InvalidPathException;

public final class SwingMisc {
    private SwingMisc() {}
    
    static void requireEdt() {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException(Thread.currentThread().toString());
        }
    }
    
    static void beep() {
        Toolkit.getDefaultToolkit().beep();
    }
    
    @SuppressWarnings("unused")
    static JPanel createTitledPanel(String title) {
        return createTitledPanel(title, null, 0);
    }
    
    @SuppressWarnings("unused")
    static JPanel createTitledPanel(String title, Component comp) {
        return createTitledPanel(title, comp, 0);
    }
    
    static JPanel createTitledPanel(String title, Component comp, int margin) {
        JPanel panel = new JPanel(new BorderLayout()) {
            @Override
            public Dimension getMaximumSize() {
                Dimension max = super.getMaximumSize();
                max.height = getPreferredSize().height;
                return max;
            }
        };
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
        if (margin > 0) {
            panel.setBorder(BorderFactory.createCompoundBorder(
                panel.getBorder(),
                BorderFactory.createEmptyBorder(margin, margin, margin, margin)));
        }
        if (comp != null) {
            panel.add(comp, BorderLayout.CENTER);
        }
        return panel;
    }
    
    @SuppressWarnings("unused")
    static JPanel wrap(Component comp) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setOpaque(false);
        panel.add(comp, BorderLayout.CENTER);
        return panel;
    }
    
    @SuppressWarnings("unused")
    static Set<Window> getChildren(Window parent) {
        Set<Window> set = new LinkedHashSet<>();
        
        for (Window w : Window.getWindows()) {
            if (isParentOf(parent, w)) {
                set.add(w);
            }
        }
        
        return set;
    }
    
    static boolean isParentOf(Window parent, Window child) {
        Container cont = child.getParent();
        if (cont == null)
            return false;
        Window window;
        if (cont instanceof Window) {
            window = (Window) cont;
        } else {
            window = SwingUtilities.getWindowAncestor(cont);
        }
        if (window == parent)
            return true;
        return isParentOf(parent, window);
    }
    
    static KeyStroke createUndoStroke() {
        return createCtrlStroke(KeyEvent.VK_Z, 0);
    }
    static KeyStroke createRedoStroke() {
        if (SystemInfo.isMacOsx()) {
            return createCtrlStroke(KeyEvent.VK_Z, KeyEvent.SHIFT_DOWN_MASK);
        } else {
            return createCtrlStroke(KeyEvent.VK_Y, 0);
        }
    }
    
    static KeyStroke createCtrlStroke(int code, int mods) {
        int mod;
        if (SystemInfo.isMacOsx()) {
            mod = KeyEvent.META_DOWN_MASK;
        } else {
            mod = KeyEvent.CTRL_DOWN_MASK;
        }
        return KeyStroke.getKeyStroke(code, mod | mods);
    }
    
    static JDialog createSimpleModalDialog(Window parent, String title, String message) {
        JDialog diag   = new JDialog(parent, title, Dialog.ModalityType.APPLICATION_MODAL);
        diag.setUndecorated(true);
        diag.setAlwaysOnTop(parent.isAlwaysOnTop());
        JLabel label   = new JLabel(message);
        JPanel content = new JPanel(new BorderLayout());
        content.setBorder(BorderFactory.createEmptyBorder(50, 50, 50, 50));
        content.add(label, BorderLayout.CENTER);
        diag.setContentPane(content);
        diag.pack();
        diag.setLocationRelativeTo(parent);
        return diag;
    }
    
    static void await(Window parent, String title, String message, BooleanSupplier test) {
        JDialog diag  = createSimpleModalDialog(parent, title, message);
        Timer   timer = new Timer(100, new ActionListener() {
            private boolean isDone = false;
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isDone) {
                    if (!test.getAsBoolean()) {
                        isDone = true;
                    }
                }
                if (isDone && diag.isVisible()) {
                    ((Timer) e.getSource()).stop();
                    diag.setVisible(false);
                }
            }
        });
        timer.setRepeats(true);
        timer.start();
        diag.setVisible(true);
    }
    
    @SuppressWarnings("unused")
    static Path chooseFile(Component parent,
                           String    title,
                           int       type,
                           Path      path) {
        return chooseFile(parent, title, type, path, null, null);
    }
    
    static Path chooseFile(Component       parent,
                           String          title,
                           int             type,
                           Path            path,
                           Predicate<Path> filter,
                           String          description) {
        return chooseFile(parent, title, type, path, filter, description, false);
    }
    
    private static Path chooseFile(Component       parent,
                                   String          title,
                                   int             type,
                                   Path            path,
                                   Predicate<Path> filter,
                                   String          description,
                                   @SuppressWarnings("SameParameterValue")
                                   boolean useJFileChooser) {
        if (useJFileChooser) {
            JFileChooser diag = new JFileChooser();
            
            diag.setDialogType(type);
            diag.setDialogTitle(title);
            diag.setMultiSelectionEnabled(false);
            diag.setFileSelectionMode(JFileChooser.FILES_ONLY);
            diag.setAcceptAllFileFilterUsed(filter == null);
            
            if (path != null) {
                try {
                    if (FileSystem.instance().isDirectory(path)) {
                        diag.setCurrentDirectory(path.toFile());
                    } else {
                        diag.setSelectedFile(path.toFile());
                    }
                } catch (UnsupportedOperationException x) {
                    Log.caught(SwingMisc.class, "chooseFile", x, false);
                }
            }
            
            if (filter != null) {
                diag.setFileFilter(new javax.swing.filechooser.FileFilter() {
                    @Override
                    public boolean accept(File file) {
                        try {
                            return filter.test(file.toPath());
                        } catch (InvalidPathException x) {
                            Log.caught(getClass(), "accept", x, false);
                            return false;
                        }
                    }
                    @Override
                    public String getDescription() {
                        return description == null ? "File Filter" : description;
                    }
                });
            }
            
            int choice;
            switch (type) {
                case JFileChooser.OPEN_DIALOG:
                    choice = diag.showOpenDialog(parent);
                    break;
                case JFileChooser.SAVE_DIALOG:
                    choice = diag.showSaveDialog(parent);
                    break;
                default:
                    throw new IllegalArgumentException(Integer.toHexString(type));
            }
            
            if (choice != JFileChooser.APPROVE_OPTION) {
                return null;
            }
            
            File file = diag.getSelectedFile();
            if (file == null) {
                return null;
            }
            
            try {
                return file.toPath();
            } catch (InvalidPathException x) {
                Log.caught(SwingMisc.class, "chooseFile", x, false);
            }
        } else {
            switch (type) {
                case JFileChooser.OPEN_DIALOG:
                    // noinspection ConstantConditions
                    type = FileDialog.LOAD;
                    break;
                case JFileChooser.SAVE_DIALOG:
                    // noinspection ConstantConditions
                    type = FileDialog.SAVE;
                    break;
                default:
                    throw new IllegalArgumentException(Integer.toHexString(type));
            }
            
            if (!(parent instanceof Window)) {
                parent = SwingUtilities.getWindowAncestor(parent);
            }
            
            FileDialog diag;
            if (parent instanceof Dialog) {
                diag = new FileDialog((Dialog) parent, title, type);
            } else if (parent instanceof Frame) {
                diag = new FileDialog((Frame) parent, title, type);
            } else {
                throw new IllegalArgumentException(String.valueOf(parent));
            }
            
            diag.setMultipleMode(false);
            
            if (filter != null) {
                diag.setFilenameFilter(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        try {
                            Path path = dir.toPath();
                            path      = path.resolve(name);
                            return filter.test(path);
                        } catch (InvalidPathException x) {
                            Log.caught(getClass(), "accept", x, false);
                            return false;
                        }
                    }
                });
            }
            
            if (path != null) {
                if (FileSystem.instance().isDirectory(path)) {
                    diag.setDirectory(path.toAbsolutePath().toString());
                } else {
                    diag.setFile(path.toAbsolutePath().toString());
                }
            }
            
            diag.setVisible(true);
            
            String dir  = diag.getDirectory();
            String file = diag.getFile();
            
            if (dir == null || file == null) {
                return null;
            }
            
            try {
                Path result = Paths.get(dir);
                result      = result.resolve(file);
                return result;
            } catch (InvalidPathException x) {
                Log.caught(SwingMisc.class, "chooseFile", x, false);
            }
        }
        String message = "<html>"
                       + "There was an error while resolving the selected file path."
                       + "<br/><br/>"
                       + "The application log may have additional details."
                       + "</html>";
        JOptionPane.showMessageDialog(parent, message, "File Path Error", JOptionPane.ERROR_MESSAGE);
        return null;
    }
    
    static void selectAll(JList<?> list) {
        list.setSelectionInterval(0, list.getModel().getSize() - 1);
    }
    
    static int selectAll(JList<?> list, Iterable<?> elems) {
        return selectAll(list, elems, Objects::equals);
    }
    
    static <T, U> int selectAll(JList<T> list, Iterable<U> elems, BiPredicate<? super T, ? super U> p) {
        ListModel<T> model = list.getModel();
        int          size  = model.getSize();
        int          count = 0;
        
        for (U elem : elems) {
            boolean found = false;
            for (int i = 0; i < size; ++i) {
                if (p.test(model.getElementAt(i), elem)) {
                    list.addSelectionInterval(i, i);
                    found = true;
                    break;
                }
            }
            if (!found) {
                ++count;
            }
        }
        
        return count;
    }
    
    static <T> Iterable<T> each(DefaultListModel<? extends T> model) {
        return () -> Misc.iteratorOf(model.elements());
    }
    
    static JTextField setAsIfDisabled(JTextField field) {
        field.setEditable(false);
        
        UIDefaults defs = UIManager.getDefaults();
        Color      fg   = defs.getColor("TextField.inactiveForeground");
        Color      bg   = defs.getColor("TextField.inactiveBackground");
        if (fg != null && bg != null) {
            field.setForeground(fg);
            field.setBackground(bg);
        }
        
        field.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
        
        field.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                field.getCaret().setVisible(true);
            }
            @Override
            public void focusLost(FocusEvent e) {
                field.getCaret().setVisible(false);
            }
        });
        
        return field;
    }
    
    static <C extends JComponent> C space(C comp, int px) {
        LayoutManager lm = comp.getLayout();
        if (lm instanceof BoxLayout) {
            synchronized (comp.getTreeLock()) {
                int axis  = ((BoxLayout) lm).getAxis();
                int count = comp.getComponentCount();
                for (int i = count - 1; i > 0; --i) {
                    Component strut;
                    switch (axis) {
                        case BoxLayout.X_AXIS:
                            strut = Box.createHorizontalStrut(px);
                            break;
                        case BoxLayout.Y_AXIS:
                            strut = Box.createVerticalStrut(px);
                            break;
                        default:
                            return comp;
                    }
                    comp.add(strut, i);
                }
            }
        }
        return comp;
    }
    
    static SimpleAttributeSet toAttributeSet(Style style) {
        SimpleAttributeSet atts = new SimpleAttributeSet();
        
        if (style.isBackgroundSet()) {
            StyleConstants.setBackground(atts, toAwtColor(style.getBackground()));
        }
        if (style.isForegroundSet()) {
            StyleConstants.setForeground(atts, toAwtColor(style.getForeground()));
        }
        if (style.isFamilySet()) {
            StyleConstants.setFontFamily(atts, style.getFamily());
        }
        if (style.isSizeSet()) {
            float absSize = Settings.getAbsoluteSize(style);
            StyleConstants.setFontSize(atts, Math.round(absSize));
        }
        if (style.isBoldSet()) {
            StyleConstants.setBold(atts, style.isBold());
        }
        if (style.isItalicSet()) {
            StyleConstants.setItalic(atts, style.isItalic());
        }
        
        return atts;
    }
    
    static Style.Builder fromAwtStyle(Color foreground, Font font) {
        return Style.builder()
                    .setForeground(fromAwtColor(foreground))
                    .setFamily(font.getName())
                    .setSize(new Size(font.getSize()))
                    .setBold(font.isBold())
                    .setItalic(font.isItalic());
    }
    
//    @Deprecated
//    private static Font getBasicFont() {
//        Style   basic    = Settings.getBasicStyle();
//        Style   def      = app().getComponentFactory().getDefaultStyle();
//        String  family   = (basic.isFamilySet() ? basic : def).getFamily();
//        float   size     = (basic.isSizeSet()   ? basic : def).getSize().getAbsoluteSize();
//        boolean isBold   = replnull((basic.isBoldSet()   ? basic : def).isBold(),   false);
//        boolean isItalic = replnull((basic.isItalicSet() ? basic : def).isItalic(), false);
//        int     awtStyle = (isBold ? Font.BOLD : 0) | (isItalic ? Font.ITALIC : 0);
//        return new Font(family, awtStyle, Math.round(size));
//    }
    
    /*
    @Deprecated
    static Font toAwtFont(Style style) {
        style = Style.replnull(style);
        
        String  family    = Settings.getAbsolute(style, Style::getFamily);
        float   floatSize = Settings.getAbsoluteSize(style);
        boolean isBold    = replnull(Settings.getAbsolute(style, Style::isBold),   false);
        boolean isItalic  = replnull(Settings.getAbsolute(style, Style::isItalic), false);
        
        int awtStyle = Font.PLAIN;
        if (isBold  ) awtStyle |= Font.BOLD;
        if (isItalic) awtStyle |= Font.ITALIC;
        
        Font font = new Font(family, awtStyle, Math.round(floatSize));
//        Log.note(SwingMisc.class, "toAwtFont", "style = ", style, ", font = ", font);
        return font;
    }
    */
    
    static Font toAwtFontAbs(Style style) {
        String  family   = style.getFamily();
        float   size     = style.getSize().getAbsoluteSize();
        boolean isBold   = style.isBoldSet() && style.isBold();
        boolean isItalic = style.isItalicSet() && style.isItalic();
        int     awtStyle = (isBold ? Font.BOLD : 0) | (isItalic ? Font.ITALIC : 0);
        
        return new Font(family, awtStyle, Math.round(size));
    }
    
    static Color toAwtColor(Style style, ArgbColor def) {
        ArgbColor color = style.getForeground();
        return toAwtColor(color == null ? def : color);
    }
    
    static Color toAwtColor(ArgbColor color) {
        if (color == null) {
            return null;
        }
        return new Color(color.getAsInt(), true);
    }
    
    static ArgbColor fromAwtColor(Color color) {
        if (color == null) {
            return null;
        }
        return new ArgbColor(color.getRGB());
    }
    
    static BoundingBox getBounds(Component comp) {
        Rectangle rect = comp.getBounds();
        return new BoundingBox(rect.y,
                               rect.x + rect.width,
                               rect.y + rect.height,
                               rect.x);
    }
    
    static void setBounds(Component comp, BoundingBox box) {
        comp.setBounds((int) Math.round(box.west),
                       (int) Math.round(box.north),
                       (int) Math.round(box.east  - box.west),
                       (int) Math.round(box.south - box.north));
    }
    
    static void setTitle(Component comp, String title) {
        setTitle((JComponent) comp, title);
    }
    
    static void setTitle(JComponent comp, String title) {
        setTitle(comp.getBorder(), title);
    }
    
    static void setTitle(Border b, String title) {
        if (b instanceof TitledBorder) {
            ((TitledBorder) b).setTitle(title);
            
        } else if (b instanceof CompoundBorder) {
            CompoundBorder c = (CompoundBorder) b;
            
            setTitle(c.getInsideBorder(), title);
            setTitle(c.getOutsideBorder(), title);
        }
    }
    
    static JFrame getMainFrame() {
        return ((SwingMainWindow) app().getMainWindow()).getFrame();
    }
    
    static Dimension getAvailableScreenSize(JFrame frame) {
        // Gets the dimensions of the available space on the monitor.
        GraphicsConfiguration config = frame.getGraphicsConfiguration();
        Dimension         screenSize = config.getBounds().getSize();
        Insets                insets = Toolkit.getDefaultToolkit().getScreenInsets(config);
        screenSize.width  -= insets.left + insets.right;
        screenSize.height -= insets.top  + insets.bottom;
        return screenSize;
    }
    
    @SuppressWarnings("unused")
    static void printUIManagerDefaults() {
        UIDefaults m = UIManager.getLookAndFeelDefaults();
        m.forEach((k, v) -> System.out.println(k + " = " + v));
    }
    
    @SuppressWarnings("unused")
    static void printUIManagerFonts() {
        UIManager.getDefaults().forEach((k, v) -> { if (v instanceof Font) System.out.println(k + " = " + v); });
        UIManager.getLookAndFeelDefaults().forEach((k, v) -> { if (v instanceof Font) System.out.println(k + " = " + v); });
    }
    
    // Static variables go here because we don't want them to cause
    // Swing/AWT classes to be initialized before the preconfiguration
    // runs.
    public static final class Store {
        private Store() {
        }
        
//        public static final AttributeSet NO_ATTRIBUTES = new SimpleAttributeSet();
        
//        private static final java.util.Map<Class<?>, Font> DEFAULT_FONTS = new java.util.concurrent.ConcurrentHashMap<>();
        
        public static final String DEFAULT_FONT_NAME = Font.MONOSPACED;
        
        public static final Style DEFAULT_STYLE =
            Style.builder().setForeground(ArgbColor.BLACK)
                           .setSize(new Size(16f))
//                           .setSize(new Size(getDefaultFont(new JLabel()).getSize()))
                           .setFamily(DEFAULT_FONT_NAME)
                           .build();
        
        public static final String DOCUMENT = "document";
    }
    
    @SuppressWarnings("unused")
    static void invokeOnEdt(Runnable r) {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            SwingUtilities.invokeLater(r);
        }
    }
    
    @SuppressWarnings("unused")
    static void invokeLater(int howMuchLater, Runnable r) {
        Timer timer = new Timer(howMuchLater, e -> r.run());
        timer.setRepeats(false);
        timer.setCoalesce(false);
        timer.start();
    }
    
    static void invokeNow(Runnable r) {
        if (SwingUtilities.isEventDispatchThread()) {
            r.run();
        } else {
            try {
                SwingUtilities.invokeAndWait(r);
            } catch (InvocationTargetException | InterruptedException x) {
                Log.caught(SwingMisc.class, "invokeNow", x, false);
            }
        }
    }
    
    static void onFirstOpen(Window window, Runnable task) {
        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                task.run();
                window.removeWindowListener(this);
            }
        });
    }
    
    static <E extends EventListener> void unlisten
           (Component comp,
            Class<E>  type,
            Runnable  r) {
        Objects.requireNonNull(comp, "comp");
        Objects.requireNonNull(type, "type");
        Objects.requireNonNull(r,    "r");
        if (comp instanceof AbstractButton) {
            unlisten((AbstractButton) comp, type, r);
        } else if (comp instanceof JList<?>) {
            unlisten((JList<?>) comp, type, r);
        } else if (comp instanceof JComboBox<?>) {
            unlisten((JComboBox<?>) comp, type, r);
        } else if (comp instanceof JTable) {
            unlisten((JTable) comp, type, r);
        } else {
            unlistenReflective(comp, type, r);
        }
    }
    
    private static <E extends EventListener> void unlisten
           (AbstractButton btn,
            Class<E>       type,
            Runnable       r) {
        if (type == ActionListener.class) {
            unlisten(btn,
                     AbstractButton::getActionListeners,
                     AbstractButton::addActionListener,
                     AbstractButton::removeActionListener,
                     r);
        } else if (type == ItemListener.class) {
            unlisten(btn,
                     AbstractButton::getItemListeners,
                     AbstractButton::addItemListener,
                     AbstractButton::removeItemListener,
                     r);
        } else if (type == ChangeListener.class) {
            unlisten(btn,
                     AbstractButton::getChangeListeners,
                     AbstractButton::addChangeListener,
                     AbstractButton::removeChangeListener,
                     r);
        } else {
            unlistenReflective(btn, type, r);
        }
    }
    
    private static <E extends EventListener> void unlisten
           (JList<?> list,
            Class<E> type,
            Runnable r) {
        if (type == ListSelectionListener.class) {
            unlisten(list,
                     JList::getListSelectionListeners,
                     JList::addListSelectionListener,
                     JList::removeListSelectionListener,
                     r);
        } else {
            unlistenReflective(list, type, r);
        }
    }
    
    private static <E extends EventListener> void unlisten
           (JTable   table,
            Class<E> type,
            Runnable r) {
        if (type == TableModelListener.class && table.getModel() instanceof AbstractTableModel) {
            unlisten(table,
                     (t) -> ((AbstractTableModel) t.getModel()).getListeners(TableModelListener.class),
                     (t, tml) -> t.getModel().addTableModelListener(tml),
                     (t, tml) -> t.getModel().removeTableModelListener(tml),
                     r);
        } else {
            unlistenReflective(table, type, r);
        }
    }
    
    private static <E extends EventListener> void unlisten
           (JComboBox<?> box,
            Class<E>     type,
            Runnable     r) {
        if (type == ActionListener.class) {
            unlisten(box,
                     JComboBox::getActionListeners,
                     JComboBox::addActionListener,
                     JComboBox::removeActionListener,
                     r);
        } else if (type == ItemListener.class) {
            unlisten(box,
                     JComboBox::getItemListeners,
                     JComboBox::addItemListener,
                     JComboBox::removeItemListener,
                     r);
        } else {
            unlistenReflective(box, type, r);
        }
    }
    
    private static final class UnlistenInfo<C extends Component, E extends EventListener> {
        final Function<? super C, ? extends E[]> getter;
        final BiConsumer<? super C, ? super E>   adder;
        final BiConsumer<? super C, ? super E>   remover;
        
        final Object[] args;
        
        @SuppressWarnings("unused")
        UnlistenInfo(Function<? super C, ? extends E[]> getter,
                     BiConsumer<? super C, ? super E>   adder,
                     BiConsumer<? super C, ? super E>   remover) {
            this.getter  = getter;
            this.adder   = adder;
            this.remover = remover;
            this.args    = null;
        }
        
        UnlistenInfo(C comp, Class<E> type) throws NoSuchMethodException {
            this.args = new Object[1];
            
            Class<?>   clazz   = comp.getClass();
            Class<E[]> arr     = Misc.getArrayClass(type);
            String     name    = type.getSimpleName();
            Method     getter  = Misc.getDeclaredMethod(clazz, "get" + name + "s", (Class<?>[]) null);
            Class<?>[] params  = {type};
            Method     adder   = Misc.getDeclaredMethod(clazz, "add" + name, params);
            Method     remover = Misc.getDeclaredMethod(clazz, "remove" + name, params);
            
            this.getter = (C c) -> {
                try {
                    return arr.cast(getter.invoke(c, (Object[]) null));
                } catch (ReflectiveOperationException x) {
                    throw new UncheckedReflectiveOperationException(x);
                }
            };
            this.adder = (C c, E el) -> {
                assert SwingUtilities.isEventDispatchThread() : Thread.currentThread();
                try {
                    Object[] args = this.args;
                    args[0] = el;
                    adder.invoke(c, args);
                } catch (ReflectiveOperationException x) {
                    throw new UncheckedReflectiveOperationException(x);
                }
            };
            this.remover = (C c, E el) -> {
                assert SwingUtilities.isEventDispatchThread() : Thread.currentThread();
                try {
                    Object[] args = this.args;
                    args[0] = el;
                    remover.invoke(c, args);
                } catch (ReflectiveOperationException x) {
                    throw new UncheckedReflectiveOperationException(x);
                }
            };
        }
        
        static final Function<Class<?>, Map<Class<?>, UnlistenInfo<?, ?>>> CREATING_MAP =
            clazz -> new java.util.concurrent.ConcurrentHashMap<>();
        static final Map<Class<?>, Map<Class<?>, UnlistenInfo<?, ?>>> CACHE =
            new java.util.concurrent.ConcurrentHashMap<>();
        @SuppressWarnings("unchecked")
        static <C extends Component, E extends EventListener> UnlistenInfo<C, E> get(C comp, Class<E> type) {
            Class<?> clazz = comp.getClass();
            Map<Class<?>, UnlistenInfo<?, ?>> map = CACHE.computeIfAbsent(clazz, CREATING_MAP);
            
            UnlistenInfo<?, ?> info = map.get(type);
            
            if (info == null) {
                try {
                    info = new UnlistenInfo<>(comp, type);
                    map.put(type, info);
                } catch (ReflectiveOperationException x) {
                    Log.caught(UnlistenInfo.class, "get", x, false);
                }
            }
            
            return (UnlistenInfo<C, E>) info;
        }
    }
    
    private static <C extends Component, E extends EventListener> void unlistenReflective
           (C        comp,
            Class<E> type,
            Runnable r) {
        UnlistenInfo<C, E> info = UnlistenInfo.get(comp, type);
        if (info != null) {
            unlisten(comp, info.getter, info.adder, info.remover, r);
        } else {
            unlistenError(comp, type);
        }
    }
    
    private static <C extends Component, E extends EventListener> void unlisten
           (C comp,
            Function<? super C, ? extends E[]> getter,
            BiConsumer<? super C, ? super E>   adder,
            BiConsumer<? super C, ? super E>   remover,
            Runnable r) {
        try {
            E[] listeners = getter.apply(comp);
            for (E el : listeners)
                remover.accept(comp, el);
            Throwable caught = null;
            try {
                r.run();
            } catch (Throwable x) {
                caught = x;
                throw x;
            } finally {
                try {
                    for (E el : listeners)
                        adder.accept(comp, el);
                } catch (Throwable x) {
                    if (caught == null)
                        throw x;
                    caught.addSuppressed(x);
                }
            }
        } catch (UncheckedReflectiveOperationException x) {
            Log.caught(SwingMisc.class, "unlisten", x, false);
        }
    }
    
    private static void unlistenError(Component comp, Class<?> type) {
        Log.note(SwingMisc.class, "unlistenError", "type = ", type, ", comp = ", comp);
    }
    
    @SuppressWarnings("unused")
    static void setModified(JFrame frame, boolean isModified) {
        setModified(frame.getRootPane(), isModified);
    }
    
    static void setModified(JDialog dialog, boolean isModified) {
        setModified(dialog.getRootPane(), isModified);
    }
    
    static void setModified(JRootPane root, boolean isModified) {
        if (SystemInfo.isMacOsx()) {
            root.putClientProperty("Window.documentModified", isModified);
        }
    }
    
    static void toggleFullScreenForMac(JFrame frame) {
        Log.entering(SwingMisc.class, "toggleFullScreenForMac");
        if (frame.isDisplayable()) {
            MacTools.INSTANCE.requestToggleFullScreen(frame);
        } else {
            Log.note(SwingMisc.class, "toggleFullScreenForMac", "listening for visibility");
            // This seems to get messed up if the frame isn't displayable yet.
            onFirstOpen(frame, () -> toggleFullScreenForMac(frame));
        }
    }
    
    public static void preconfigure() {
        if (SystemInfo.isMacOsx()) {
            preconfigureForMac();
        }
        
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (ClassNotFoundException
               | InstantiationException
               | IllegalAccessException
               | UnsupportedLookAndFeelException x) {
            Log.caught(SwingMisc.class, "While setting system LAF.", x, false);
        }
        
        UIManager.put("ListUI", "javax.swing.plaf.basic.BasicListUI");
        
        UIManager.put("Tree.lineTypeDashed", true);
        UIManager.put("Tree.line", Color.LIGHT_GRAY);
        UIManager.put("Tree.hash", Color.LIGHT_GRAY);
        
//        UIManager.put("TreeUI", "javax.swing.plaf.basic.BasicTreeUI");
//        UIManager.put("SplitPaneUI", "javax.swing.plaf.metal.MetalSplitPaneUI");
//        UIManager.put("ScrollPaneUI", "javax.swing.plaf.basic.BasicScrollPaneUI");
        
        ToolTipManager.sharedInstance().setDismissDelay(30_000);
    }
    
    private static void preconfigureForMac() {
        // General Mac OS Resources:
        // https://developer.apple.com/library/content/documentation/Java/Conceptual/Java14Development/07-NativePlatformIntegration/NativePlatformIntegration.html
        // https://coderanch.com/how-to/javadoc/appledoc/api/overview-summary.html
        // https://alvinalexander.com/apple/mac/java-mac-native-look/
        // http://nadeausoftware.com/articles/2009/01/mac_java_tip_how_control_window_decorations (http://archive.is/ra7cM)
        
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        
        // Note: App is not yet instantiated, so there's no Settings object yet.
        String name = Setting.APPLICATION_NAME.getDefaultValue();
        // Note: this doesn't seem to work on newer JDKs.
        // See https://stackoverflow.com/questions/8918826/java-os-x-lion-set-application-name-doesnt-work
        System.setProperty("com.apple.mrj.application.apple.menu.about.name", name);
        // This seems to be a valid newer way:
        System.setProperty("apple.awt.application.name", name);
        
        MacTools.INSTANCE.addReOpenTask(SwingMisc::fixOsxAlwaysOnTopReOpenedBug);
    }
    
    // Workaround for disappearing frame bug (on El Capitan, and others?)
    private static void fixOsxAlwaysOnTopReOpenedBug() {
        Log.entering(SwingMisc.class, "fixOsxAlwaysOnTopReOpenedBug");
        
        // Needs more attention. The bug seems to be present on High Sierra.
        // (But sometimes not, if the program is run from a .jar?)
        boolean doToggle =
            SystemInfo.isMacOsxElCapitan()
            || SystemInfo.isMacOsx()
            ;
        
        if (doToggle) {
            MainWindow window = app().getMainWindow();
            if (window.isAlwaysOnTop()) {
                // Don't know why this is necessary, or even works
                // for that matter. (Frame gets hidden immediately
                // on El Capitan after being restored for some reason,
                // but only if it's set to always on top.)
                JFrame frame = ((SwingMainWindow) window).getFrame();
                
//                Log.note(frame.isVisible());
//                Log.note(frame.isDisplayable());
//                Log.note(frame.isShowing());
                
                frame.setVisible(false);
                frame.setVisible(true);
                // Would be nice if toFront() worked, but it doesn't.
            }
        }
    }
    
    @SuppressWarnings("JavaReflectionInvocation")
    static void configureFrameForMac(Window frame) {
        Log.entering(SwingMisc.class, "configureFrameForMac");
        
        // This behavior has been fixed on newer JDKs, and FullScreenUtilities will be removed.
        boolean doConfiguration =
            SystemInfo.isMacOsxElCapitan()
            || !SystemInfo.isJava9OrGreater()
            ;
        
        Log.note(SwingMisc.class, "configureFrameForMac", doConfiguration ? "configuring" : "skipping");
        
        if (!doConfiguration)
            return;
        
        try {
            // FullScreenUtilities
            // see e.g. https://stackoverflow.com/questions/30089804/true-full-screen-jframe-swing-application-in-mac-osx
            //      and https://stackoverflow.com/a/30308671/2891664
            
            Class<?> fullScreenUtilities    = Class.forName("com.apple.eawt.FullScreenUtilities");
            Method   setWindowCanFullScreen = fullScreenUtilities.getMethod("setWindowCanFullScreen", Window.class, boolean.class);
            
            InvocationHandler fullScreenListenerHandler = new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) {
                    Log.note(getClass(), "invoke", method.getName(), " invoked");
                    SwingMainWindow window = (SwingMainWindow) app().getMainWindow();
                    
                    switch (method.getName()) {
                        case "windowEnteringFullScreen":
                            if (frame == window.getFrame()) {
                                window.setWindowState(SwingMainWindow.WindowState.MAXIMIZED);
                            }
                            break;
                        case "windowEnteredFullScreen":
                        case "windowExitingFullScreen":
                            break;
                        case "windowExitedFullScreen":
                            if (SystemInfo.isMacOsxElCapitan() && window.isAlwaysOnTop()) {
                                // Workaround for disappearing frame bug on El Capitan.
                                frame.toFront();
                            }
                            if (frame == window.getFrame()) {
                                window.setWindowState(SwingMainWindow.WindowState.WINDOWED);
                            }
                            break;
                    }
                    
                    return null;
                }
            };
            
            Class<?> fullScreenListenerClass = Class.forName("com.apple.eawt.FullScreenListener");
            Object   fullScreenListenerProxy = Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
                                                                      new Class<?>[] { fullScreenListenerClass },
                                                                      fullScreenListenerHandler);
            Method addFullScreenListenerTo   = fullScreenUtilities.getMethod("addFullScreenListenerTo", Window.class, fullScreenListenerClass);
            
            setWindowCanFullScreen.invoke(null, frame, true);
            addFullScreenListenerTo.invoke(null, frame, fullScreenListenerProxy);
            
        } catch (ReflectiveOperationException x) {
            Log.caught(SwingMisc.class, "during preconfiguration reflection on interface thread", x, true);
        }
    }
}