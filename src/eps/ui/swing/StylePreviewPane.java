/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.app.*;
import eps.ui.*;
import eps.ui.Style;

import java.awt.*;
import javax.swing.*;
import javax.swing.text.*;

final class StylePreviewPane extends JTextPane {
    private static final String DEFAULT_EXAMPLE_TEXT = "The quick brown fox jumped over the lazy dog.";
    private static final AttributeSet NO_ATTRIBUTES = new SimpleAttributeSet();
    
    private Theme theme;
    private Theme.Key key;
    
    private final DocumentFilter documentFilter;
    
    private boolean isTextChanging = false;
    
    StylePreviewPane() {
        setEditorKit(new StyledEditorKit());
        setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
//        getCaret().setVisible(true);
        
        int major = Settings.getOrDefault(Setting.MAJOR_MARGIN);
        setBorder(BorderFactory.createEmptyBorder(major, major, major, major));
        
        documentFilter = new DocumentFilter() {
            @Override
            public void insertString(FilterBypass fb, int o, String s, AttributeSet a)
                    throws BadLocationException {
                if (isTextChanging) {
                    fb.insertString(o, s, a);
                }
            }
            @Override
            public void remove(FilterBypass fb, int o, int l)
                    throws BadLocationException {
                if (isTextChanging) {
                    fb.remove(o, l);
                }
            }
            @Override
            public void replace(FilterBypass fb, int o, int l, String s, AttributeSet a)
                    throws BadLocationException {
                if (isTextChanging) {
                    fb.replace(o, l, s, a);
                }
            }
        };
        
        putClientProperty(HONOR_DISPLAY_PROPERTIES, true);
        
        addPropertyChangeListener(SwingMisc.Store.DOCUMENT, e -> {
            Document o = (Document) e.getOldValue();
            Document n = (Document) e.getNewValue();
            if (o != null && n != null) {
                try {
                    n.remove(0, n.getLength());
                    n.insertString(0, o.getText(0, o.getLength()), NO_ATTRIBUTES);
                } catch (BadLocationException x) {
                    Log.caught(StylePreviewPane.class, "propertyChange", x, false);
                }
            }
            if (n instanceof AbstractDocument) {
                ((AbstractDocument) n).setDocumentFilter(documentFilter);
                setEditable(true);
            } else {
                setEditable(false);
            }
            update();
        });
        
        setStyledDocument(new DefaultStyledDocument());
        setText(DEFAULT_EXAMPLE_TEXT);
    }
    
    @Override
    public void setText(String text) {
        isTextChanging = true;
        try {
            super.setText(text);
        } finally {
            isTextChanging = false;
        }
    }
    
    public void setTheme(Theme theme) {
        this.theme = theme;
        update();
    }
    
    public void setStyle(Theme.Key key) {
        this.key = key;
        update();
    }
    
    private void update() {
        Color  foreground = Color.BLACK;
        Color  background = Color.WHITE;
        Color       caret = Color.BLACK;
        AttributeSet atts = NO_ATTRIBUTES;
        
        if (theme != null && key != null) {
            background = SwingMisc.toAwtColor(theme.getBackgroundColor());
            caret      = SwingMisc.toAwtColor(theme.getAbsoluteCaretColor());
            Style abs  = theme.getAbsoluteStyle(key);
            foreground = SwingMisc.toAwtColor(abs.getForeground());
            atts       = SwingMisc.toAttributeSet(abs);
        }
        
        StyledDocument doc = getStyledDocument();
        doc.setCharacterAttributes(0, doc.getLength(), atts, true);
        
        setBackground(background);
        setCaretColor(caret);
        
//        setSelectionColor(foreground);
//        setSelectedTextColor(background); // TODO: figure out why the hell this doesn't work
    }
    
    @Override
    public Dimension getPreferredSize() {
        Dimension pref = super.getPreferredSize();
        pref.height *= 2;
        return pref;
    }
}