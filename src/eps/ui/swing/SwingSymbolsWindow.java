/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.swing;

import eps.ui.*;
import eps.eval.*;
import eps.app.*;
import eps.util.*;
import static eps.app.App.*;
import static eps.util.Literals.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.util.List;

final class SwingSymbolsWindow implements SymbolsWindow, DialogChild, ListSelectionListener {
    private final JDialog diag;
    
    private final JList<Symbol> symList;
    private final SymbolListModel model;
    
    private final SymbolEditor editor;
    
    private final JButton cancel;
    private final JButton apply;
    private final JButton ok;
    
    private final JButton add;
    private final JButton del;
    private final JButton copy;
    private final JButton alias;
    
    private final JButton rest;
    private final JButton imp;
    private final JButton exp;
    
    private final List<Change> changes = new ArrayList<>();
    
    SwingSymbolsWindow() {
        diag = new JDialog(SwingMisc.getMainFrame());
        diag.setModal(false);
        diag.setTitle("Epsilon Symbols");
        diag.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        
        diag.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cancel();
            }
        });
        
        SwingMisc.onFirstOpen(diag, () -> diag.setMinimumSize(diag.getMinimumSize()));
        
        symList = new JList<>();
        model = new SymbolListModel(this);
        
        symList.setModel(model);
        symList.setCellRenderer(new SymbolCellRenderer());
        symList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JScrollPane symScroll =
            new JScrollPane(symList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                     JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        editor = new SymbolEditor(this);
        
        cancel = new JButton("Cancel");
        apply  = new JButton("Apply");
        ok     = new JButton("OK");
        
        add   = new JButton("Add New");
        alias = new JButton("Add Alias");
        copy  = new JButton("Copy");
        del   = new JButton("Remove");
        
        rest  = new JButton("Restore Defaults");
        imp   = new JButton("Import");
        exp   = new JButton("Export");
        
        ListOf(alias, del, copy).forEach(b -> b.setEnabled(false));
        
        symList.addListSelectionListener(this);
        
        cancel.addActionListener(e -> cancel());
        apply.addActionListener(e -> apply());
        ok.addActionListener(e -> ok());
        
        add.addActionListener(e -> addSymbol());
        alias.addActionListener(e -> addAlias());
        del.addActionListener(e -> removeSymbol());
        copy.addActionListener(e -> copySymbol());
        
        rest.addActionListener(e -> restoreDefaults());
        imp.addActionListener(e -> importSymbols());
        exp.addActionListener(e -> exportSymbols());
        
        int gap = app().getSetting(Setting.MINOR_MARGIN);
        
        JPanel top = new JPanel(new BorderLayout());
        top.setOpaque(false);
        top.setBorder(BorderFactory.createEmptyBorder(gap, gap, gap, gap));
        
        JToolBar tools = new JToolBar(JToolBar.HORIZONTAL);
        tools.setFloatable(false);
        
        tools.add(add);
        tools.add(alias);
        tools.add(copy);
        tools.add(del);
        tools.addSeparator();
        tools.add(rest);
        tools.add(imp);
        tools.add(exp);
        
        top.add(tools, BorderLayout.CENTER);
        
        JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        split.setContinuousLayout(true);
        split.setResizeWeight(0);
        split.setLeftComponent(symScroll);
        split.setRightComponent(editor);
        
        JPanel splitPanel = new JPanel(new BorderLayout());
        splitPanel.setBorder(BorderFactory.createEmptyBorder(0, gap, gap, gap));
        splitPanel.setOpaque(false);
        splitPanel.add(split, BorderLayout.CENTER);
        
        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        ListOf(cancel, apply, ok).forEach(buttons::add);
        
        JPanel content = new JPanel(new BorderLayout());
        content.add(top, BorderLayout.NORTH);
        content.add(splitPanel, BorderLayout.CENTER);
        content.add(buttons, BorderLayout.SOUTH);
        
        diag.setContentPane(content);
        diag.getRootPane().setDefaultButton(ok);
        
        setBounds(null);
        
        setToolTips();
    }
    
    private void setToolTips() {
        add.setToolTipText("Creates and adds a new symbol.");
        alias.setToolTipText("Creates and adds a new alias for the selected symbol.");
        del.setToolTipText("Removes the selected symbol.");
        copy.setToolTipText("Creates and adds a copy of the selected symbol.");
        rest.setToolTipText("Opens a wizard which guides the restoration of default symbols.");
        // TODO
//        exp.setToolTipText("");
//        imp.setToolTipText("");
    }
    
    @Override
    public JDialog getDialog() {
        return diag;
    }
    
    @Override
    public void close() {
        diag.setVisible(false);
        diag.dispose();
        
        editor.close();
    }
    
    @Override
    public boolean isVisible() {
        return diag.isVisible();
    }
    
    @Override
    public void setVisible(boolean isVisible) {
        boolean shown = isVisible && !diag.isVisible();
        if (shown) {
            refresh();
        }
        diag.setVisible(isVisible);
        if (shown) {
            symList.requestFocus();
        }
    }
    
    @Override
    public BoundingBox getBounds() {
        return SwingMisc.getBounds(diag);
    }
    
    @Override
    public void setBounds(BoundingBox box) {
        if (box == null) {
            diag.pack();
            diag.setLocationRelativeTo(SwingMisc.getMainFrame());
        } else {
            SwingMisc.setBounds(diag, box);
            diag.revalidate();
            diag.repaint();
        }
    }
    
    @Override
    public void refresh() {
        refresh(true);
    }
    
    private void refresh(boolean merge) {
        refreshSymbols(merge);
    }
    
    private void refreshSymbols(boolean merge) {
        try (Symbols syms = app().getSymbols().block()) {
            if (diag.isVisible()) {
                if (!syms.modifiedSince(model.appModRef)) {
                    return;
                }
            }
            refreshSymbols(syms, syms.getModificationRef(), merge);
        }
    }
    
    private void refreshSymbols(Symbols syms, Symbols.ModRef appModRef) {
        refreshSymbols(syms, appModRef, true);
    }
    
    private void refreshSymbols(Symbols syms, Symbols.ModRef appModRef, boolean merge) {
        Symbol sel = symList.getSelectedValue();
        symList.removeListSelectionListener(this);
        try {
            reset(syms, appModRef, merge);
        } finally {
            symList.addListSelectionListener(this);
        }
        
        if (sel != null && !model.contains(sel)) {
            Symbol found = null;
            for (Symbol exist : getSymbols()) {
                if (isEquivalent(exist, sel)) {
                    found = exist;
                    break;
                }
            }
            sel = found;
        }
        
        if (sel != null) {
            symList.setSelectedValue(sel, true);
        } else {
            symList.clearSelection();
        }
        editor.setSymbol(sel);
    }
    
    private void reset(Symbols syms, Symbols.ModRef appModRef, boolean merge) {
        model.reset(syms, appModRef);
        if (merge) {
            List<Change> changesCopy = new ArrayList<>(changes);
            changes.clear();
            for (Change c : changesCopy) {
                symbolChanged(c.before, c.after);
            }
        } else {
            changes.clear();
        }
        modified();
    }
    
    private boolean isEquivalent(Symbol lhs, Symbol rhs) {
        if (lhs == rhs)
            return true;
        if (lhs == null || rhs == null)
            return lhs == rhs;
        if (lhs.getSymbolKind() != rhs.getSymbolKind())
            return false;
        if (lhs.isSpanOp()) {
            SpanOp spanL = (SpanOp) lhs;
            SpanOp spanR = (SpanOp) rhs;
            return spanL.getLeftName().equals(spanR.getLeftName())
                && spanL.getRightName().equals(spanR.getRightName());
        } else {
            return lhs.getName().equals(rhs.getName());
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            selectionChanged();
        }
    }
    
    private Symbol selectionChanged() {
        Symbol sel = symList.getSelectedValue();
        editor.setSymbol(sel);
        sel = editor.getSymbol();
        
        del.setEnabled(sel != null && !sel.isIntrinsic());
        copy.setEnabled(sel != null);
        alias.setEnabled(sel != null);
        return sel;
    }
    
    private void addSymbol() {
        Symbol.Kind[] kinds = Symbol.Kind.values();
        
        Object choice =
            JOptionPane.showInputDialog(diag,
                                        "What type of symbol would you like to add?",
                                        "Choose Symbol Type",
                                        JOptionPane.QUESTION_MESSAGE,
                                        null,
                                        Arrays.stream(kinds).map(Symbol.Kind::toTitleCase).toArray(),
                                        Symbol.Kind.VARIABLE.toTitleCase());
        if (choice == null)
            return;
        for (Symbol.Kind kind : kinds) {
            if (kind.toTitleCase().equals(choice)) {
                addSymbol(kind);
                break;
            }
        }
    }
    
    private void addSymbol(Symbol.Kind kind) {
        if (kind == null) {
            assert false;
            return;
        }
        
        Symbol.Builder<?, ?> builder =
            kind.createBuilder()
                .setName(getUniqueName(kind));
        
        if (kind.isUnaryOp()) {
            ((UnaryOp.Builder) builder).makeFunction();
        }
        
        Symbol sym = builder.buildError();
        symbolChangedThenSelect(null, sym);
    }
    
    private void addAlias() {
        Symbol sel = symList.getSelectedValue();
        if (sel == null) {
            assert false : sel; // alias should be disabled
            SwingMisc.beep();
            return;
        }
        
        Symbol del = sel.getDelegate();
        Symbol alias;
        
        do {
            if (sel.isSpanOp()) {
                SpanOp span = (SpanOp) sel;
                if (!span.isSymmetric()) {
                    String left  = span.getLeftName();
                    String right = span.getRightName();
                    alias = ((SpanOp) del).createAlias(getUniqueName(Symbol.Kind.SPAN_OP, left),
                                                       getUniqueName(Symbol.Kind.SPAN_OP, right));
                    break;
                }
            }
            alias = del.createAlias(getUniqueName(sel));
        } while (false);
        
        symbolChangedThenSelect(null, alias);
        
//        symList.setSelectedValue(alias, true);
//        symList.requestFocus();
    }
    
    private void removeSymbol() {
        Symbol sel = symList.getSelectedValue();
        if (sel == null || sel.isIntrinsic()) {
            assert false : sel; // del should be disabled
            SwingMisc.beep();
            return;
        }
        
        symbolChangedThenSelect(sel, null);
        
//        symList.clearSelection(); // symbolChanged(...) does this without listeners
//        editor.setSymbol(null);   // update the editor
//        selectionChanged();
//        symList.requestFocus();
    }
    
    private void copySymbol() {
        Symbol sel = symList.getSelectedValue();
        if (sel == null) {
            assert false : sel; // copy should be disabled
            SwingMisc.beep();
            return;
        }
        
        String leftName  = null;
        String rightName = null;
        
        if (sel.isSpanOp()) {
            leftName  = ((SpanOp) sel).getLeftName();
            rightName = ((SpanOp) sel).getRightName();
        } else {
            leftName  = sel.getName();
        }
        
        Symbol.Kind kind = sel.getSymbolKind();
        
        leftName  = getValidName(kind, leftName);
        rightName = getValidName(kind, rightName);
        
        if (sel.isIntrinsic() || sel.isAlias()) {
            sel = sel.getDelegate().createAlias(sel);
        }
        
        Symbol.Builder<?, ?> b = sel.createBuilder();
        
        if (sel.isSpanOp()) {
            ((SpanOp.Builder) b).setLeftName(leftName)
                                .setRightName(rightName);
        } else {
            b.setName(leftName);
        }
        
        Log.note(getClass(), "copySymbol", "builder = ", b);
        sel = sel.configure(b);
        
        symbolChangedThenSelect(null, sel);
    }
    
    private String getValidName(Symbol.Kind kind, String name) {
        if (name != null) {
            name = Symbol.deintrinsify(name);
            if (name.isEmpty()) {
                name = getDefaultName(kind);
            }
            name = getUniqueName(kind, name);
        }
        return name;
    }
    
    String getDefaultName(Symbol.Kind kind) {
        if (kind.isVariable())
            return "var";
        if (kind.isOperator())
            return "op";
        return "sym";
    }
    
    String getUniqueName(Symbol sym) {
        return getUniqueName(sym.getSymbolKind(), sym.getName());
    }
    
    String getUniqueName(Symbol.Kind kind) {
        return getUniqueName(kind, getDefaultName(kind));
    }
    
    String getUniqueName(Symbol.Kind kind, String base) {
        for (int suffix = 0;; ++suffix) {
            String name = (suffix == 0) ? base : (base + suffix);
            if (!isDuplicateName(kind, name))
                return name;
        }
    }
    
    boolean isDuplicateName(Symbol.Kind kind, String name) {
        return isDuplicateName(null, kind, name);
    }
    
    boolean isDuplicateName(Symbol sym, String name) {
        return isDuplicateName(sym, sym.getSymbolKind(), name);
    }
    
    private boolean isDuplicateName(Symbol sym, Symbol.Kind kind, String name) {
        return Symbol.isDuplicate(getSymbols(), sym, kind, name);
    }
    
    private void restoreDefaults() {
        restoreDefaultsImpl();
    }
    
    private void importSymbols() {
        importSymbolsImpl();
    }
    
    private void exportSymbols() {
        exportSymbolsImpl();
    }
    
    Iterable<Symbol> getSymbols() {
        return SwingMisc.each(model);
    }
    
    void symbolChangedThenSelect(Symbol before, Symbol after) {
        symbolChanged(before, after);
        
        if (after == null) {
            symList.clearSelection();
        } else {
            symList.setSelectedValue(after, true);
        }
        
        selectionChanged();
        symList.requestFocus();
    }
    
    void symbolChanged(Symbol before, Symbol after) {
        assert before != null || after != null;
        
        Change change = null;
        
        if (before != null) {
            change = changes.stream()
                            .filter(c -> c.all.contains(before))
//                            .filter(c -> c.before == before || c.after == before)
                            .findFirst()
                            .orElse(null);
        }
        
        Symbol prev = before;
        
        Log.note(getClass(), "symbolChanged", "change = ", change);
        
        if (change != null) {
            if (true /*change.before == before*/)
                prev = change.after;
            change.setAfter(after);
        } else {
            change = new Change(before, after);
            changes.add(change);
        }
        
        modified();
        Log.low(getClass(), "symbolChanged", "before = ", before, ", after = ", after, ", changes = ", changes);
        
        symList.removeListSelectionListener(this);
        try {
            if (prev != null) {
                Symbol sel = symList.getSelectedValue();
                
                if (after != null) {
                    do {
                        if (prev.getName().equals(after.getName())) {
                            int index = model.indexOf(prev);
                            if (index >= 0) {
                                model.set(index, after);
                                break;
                            }
//                            assert false : f("before = %s, after = %s, prev = %s", before, after, prev);
                        }
                        model.removeElement(prev);
                        model.addSymbol(after);
                    } while (false);
                } else {
                    model.removeElement(prev);
                }
                
                if (prev == sel) {
                    if (after != null) {
                        symList.setSelectedValue(after, true);
                    } else {
                        symList.clearSelection();
                    }
                }
            } else if (after != null) {
                model.addSymbol(after);
            }
        } finally {
            symList.addListSelectionListener(this);
        }
    }
    
    private void cancel() {
        editor.cancelling();
        changes.clear();
        modified();
        refresh();
        setVisible(false);
    }
    
    private boolean validate(Symbol sym) {
        if (sym != null) {
            if (sym.isIntrinsic()) {
                assert false : sym;
                
                String message = "The name " + sym.getName() + " has the style of "
                               + "an intrinsic symbol, which is reserved. The symbol "
                               + "must be renamed before the change to this symbol can "
                               + "be made.";
                
                message = "<html><body style='width:500px'>" + message + "</body></html>";
                JOptionPane.showMessageDialog(diag, message, "Reserved Name", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        return true;
    }
    
    private void apply() {
        editor.applying();
        
        Symbol sel = symList.getSelectedValue();
        
        waitForSymbols();
        
        Symbols        syms;
        Symbols.ModRef mod;
        Symbols        copy = null;
        try (Symbols appSyms = app().getSymbols().block()) {
            syms = appSyms;
            mod  = syms.getModificationRef();
            if (!changes.isEmpty()) {
                copy = syms.copy();
            }
        }
        
        if (!changes.isEmpty()) {
            if (copy == null) {
                assert false;
                copy = syms.copy();
            }
            
            Iterator<Change> it = changes.iterator();
            while (it.hasNext()) {
                Change c = it.next();
                
                if (validate(c.after)) {
                    if (c.before != null) {
                        copy.remove(c.before);
                        
                        if (sel == c.before) {
                            sel = c.after;
                        }
                    }
                    if (c.after != null) {
                        copy.put(c.after);
                    }
                    
                    it.remove();
                }
            }
            
            syms.clear();
            syms.putAll(copy);
            app().writeSymbols();
            
            syms = copy;
        }
        
        List<Change> err = new ArrayList<>(changes);
        changes.clear();
        
        refreshSymbols(syms, mod);
        
        for (Change c : err) {
            symbolChanged(c.before, c.after);
        }
        
        modified();
        
        if (sel == null) {
            symList.clearSelection();
        } else {
            symList.setSelectedValue(sel, true);
        }
    }
    
    private void waitForSymbols() {
        Symbols syms = app().getSymbols();
        if (syms.hasActionsWaiting()) {
            String title   = "Symbols In Use";
            String message = "Waiting while the application symbols are in use...";
            SwingMisc.await(diag, title, message, syms::hasActionsWaiting);
        }
    }
    
    private void ok() {
        apply();
        setVisible(false);
    }
    
    private void modified() {
        modified(!changes.isEmpty());
    }
    
    private void modified(boolean modified) {
        SwingMisc.setModified(diag, modified);
    }
    
    private static final class SymbolListModel extends DefaultListModel<Symbol> {
        private final SwingSymbolsWindow window;
        
        private Symbols.ModRef appModRef;
        
        SymbolListModel(SwingSymbolsWindow window) {
            this.window = Objects.requireNonNull(window, "window");
            
            try (Symbols syms = app().getSymbols().block()) {
                reset(syms, syms.getModificationRef());
            }
        }
        
        void reset(Symbols syms, Symbols.ModRef appModRef) {
            Objects.requireNonNull(syms, "syms");
            this.appModRef = Objects.requireNonNull(appModRef, "appModRef");
            
            removeAllElements();
            
            syms.stream()
                .map(sym -> sym.isSpanOp() ? ((SpanOp) sym).getParent() : sym)
                .distinct()
                .sorted(SymbolComparator.INSTANCE)
                .forEach(this::addElement);
        }
        
        void addSymbol(Symbol sym) {
            addSymbol(sym, false);
        }
        
        Symbol addSymbol(Symbol sym, boolean overwrite) {
            int size = getSize();
            
            Symbol prev = null;
            
            if (overwrite) {
                int index = getOverwriteIndex(sym);
                if (index >= 0) {
                    prev = getElementAt(index);
                    remove(index);
                }
            }
            
            for (int i = 0; i <= size; ++i) {
                if (i < size) {
                    Symbol e = getElementAt(i);
                    int comp = SymbolComparator.INSTANCE.compare(e, sym);
                    if (comp < 0) {
                        continue;
                    }
                }
                add(i, sym);
                break;
            }
            
            return prev;
        }
        
        int getOverwriteIndex(Symbol sym) {
            int size = getSize();
            for (int i = 0; i < size; ++i) {
                if (Symbols.isOverwriteEqual(sym, getElementAt(i))) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    static final class SymbolCellRenderer extends DefaultListCellRenderer {
        private final Map<Symbol, DefaultListCellRenderer> renderers = new WeakHashMap<>();
        
        @Override
        public Component getListCellRendererComponent(JList<?> list,
                                                      Object   value,
                                                      int      index,
                                                      boolean  isSelected,
                                                      boolean  cellHasFocus) {
            if (value instanceof Symbol) {
                Symbol sym = (Symbol) value;
                
                DefaultListCellRenderer r = renderers.get(sym);
                
                String text;
                if (r == null) {
                    renderers.put(sym, r = new DefaultListCellRenderer());
                    text = getHtml(sym);
                } else {
                    text = r.getText();
                }
                
                return r.getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
            }
            
            return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
        
        private String getHtml(Symbol sym) {
            return "<html>"
                 + "<b>" + Misc.escapeHtml(sym.getName()) + "</b>"
                 + " "
                 + "<i>(" + sym.getSymbolKind().toLowerCase() + ")</i>"
                 + "</html>";
        }
        
        private String getText(Symbol sym) {
            return sym.getName() + " (" + sym.getSymbolKind().toLowerCase() + ")";
        }
    }
    
    // TODO: maybe this should go in interface Symbol.
    private enum SymbolComparator implements Comparator<Symbol> {
        INSTANCE;
        @Override
        public int compare(Symbol lhs, Symbol rhs) {
            if (lhs.isIntrinsic() != rhs.isIntrinsic()) {
                assert !lhs.getName().equals(rhs.getName()) : lhs + ", " + rhs;
                return lhs.isIntrinsic() ? +1 : -1;
            } else {
                return compare0(lhs, rhs);
            }
        }
        private int compare0(Symbol lhs, Symbol rhs) {
            int nameComp = lhs.getName().compareTo(rhs.getName());
            if (nameComp != 0) {
                return nameComp;
            }
            int kindComp = lhs.getSymbolKind().compareTo(rhs.getSymbolKind());
            if (kindComp != 0) {
                return kindComp;
            }
            if (lhs.isSpanOp()) {
                SpanOp spanL = (SpanOp) lhs;
                SpanOp spanR = (SpanOp) rhs;
                nameComp = spanL.getLeftName().compareTo(spanR.getLeftName());
                if (nameComp != 0) {
                    return nameComp;
                }
                nameComp = spanL.getRightName().compareTo(spanR.getRightName());
                if (nameComp != 0) {
                    return nameComp;
                }
            }
            return 0;
        }
    }
    
    static final Comparator<Symbol> SYMBOL_COMPARATOR = SymbolComparator.INSTANCE;
    
    private static final class Change {
        Symbol before;
        Symbol after;
        
        final Set<Symbol> all = Collections.newSetFromMap(new WeakHashMap<>());
        
        Change() {
        }
        
        Change(Symbol before, Symbol after) {
            setBefore(before);
            setAfter(after);
        }
        
        Change setBefore(Symbol before) {
            this.before = before;
            if (before != null)
                all.add(before);
            return this;
        }
        
        Change setAfter(Symbol after) {
            this.after = after;
            if (after != null)
                all.add(after);
            return this;
        }
        
        static Change before(Symbol before) {
            return new Change(before, null);
        }
        
        static Change after(Symbol after) {
            return new Change(null, after);
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(before) ^ Objects.hashCode(after);
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Change) {
                Change that = (Change) obj;
                return Objects.equals(this.before, that.before)
                    && Objects.equals(this.after,  that.after);
            }
            return false;
        }
        
        private static final ToString<Change> TO_STRING =
            ToString.builder(Change.class)
                    .add("before", c -> c.before)
                    .add("after",  c -> c.after)
                    .build();
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
    }
    
    /*
    WIZARD STUFF BELOW
    */
    
    private boolean restoreDefaultsImpl() {
        Wizard wiz = new Wizards.RestoreDefaultsWizard(this);
        
        Map<String, Object> result = wiz.show();
        if (result == null || !wiz.isDone()) {
            return false;
        }
        
        String mode = (String) result.get(Wizards.ChooseRestoreModeStep.TITLE);
        Log.note(getClass(), "restoreDefaultsImpl", "mode = ", mode);
        
        switch (mode) {
            case Wizards.ChooseRestoreModeStep.RESTORE_EVERYTHING: {
                Symbols syms = app().getSymbols();
                syms.clear();
                syms.putAll(Wizards.DEFAULT_SYMBOLS);

                waitForSymbols();
                app().writeSymbols();
                refresh(false);
                break;
            }
            case Wizards.ChooseRestoreModeStep.RESTORE_SELECT: {
                List<?> symbols = (List<?>) result.get(Wizards.ChooseRestoreSymbolsStep.TITLE);
                Log.note(getClass(), "restoreDefaultsImpl", "symbols = ", symbols);
                
                addAllSymbols(symbols);
                break;
            }
        }
        
        symList.requestFocus();
        return true;
    }
    
    private boolean importSymbolsImpl() {
        Wizard wiz = new Wizards.ImportSymbolsWizard(this);
        
        Map<String, Object> result = wiz.show();
        if (result == null || !wiz.isDone()) {
            return false;
        }
        
        List<?> symbols = (List<?>) result.get(Wizards.ChooseImportSymbolsStep.TITLE);
        addAllSymbols(symbols);
        
        symList.requestFocus();
        return true;
    }
    
    private void addAllSymbols(List<?> symbols) {
        if (symbols == null || symbols.isEmpty()) {
            return;
        }
        
        Symbol beforeSel = symList.getSelectedValue();
        Symbol afterSel  = beforeSel;
        
        symList.removeListSelectionListener(this);
        try {
            for (Object obj : symbols) {
                Symbol before = null;
                Symbol after  = (Symbol) obj;
                
                int index = model.getOverwriteIndex(after);
                if (index >= 0) {
                    before = model.getElementAt(index);
                    if (beforeSel == before) {
                        afterSel = after;
                    }
                }
                
                if (before != after) {
                    symbolChanged(before, after);
                }
            }
        } finally {
            symList.addListSelectionListener(this);
        }
        
        if (beforeSel != afterSel) {
            if (afterSel != null) {
                symList.setSelectedValue(afterSel, true);
                editor.setSymbol(afterSel);
            } else {
                symList.clearSelection();
                editor.setSymbol(null);
            }
        }
    }
    
    private boolean exportSymbolsImpl() {
        Wizard wiz = new Wizards.ExportSymbolsWizard(this);
        
        Map<String, Object> results = wiz.show();
        if (results == null || !wiz.isDone()) {
            return false;
        }
        
        // Nothing to do
        
        return true;
    }
}