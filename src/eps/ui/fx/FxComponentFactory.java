/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.ui.*;

public final class FxComponentFactory extends AbstractComponentFactory {
    public FxComponentFactory() {
        super(FxConstants.DEFAULT_STYLE);
    }
    
    @Override
    public AsyncExecutor newAsyncExecutor() {
        return new FxAsyncExecutor();
    }
    
    @Override
    public void displayError(String title, String text) {
        throw new UnsupportedComponentException();
    }
    
    @Override
    public MainWindow newMainWindow() {
        return new FxMainWindow();
    }
    
    @Override
    public DebugWindow newDebugWindow() {
        throw new UnsupportedComponentException();
    }
    
    @Override
    public SettingsWindow newSettingsWindow() {
        throw new UnsupportedComponentException();
    }
    
    @Override
    public SymbolsWindow newSymbolsWindow() {
        throw new UnsupportedComponentException();
    }
    
    @Override
    public EmptyCell newEmpty() {
        throw new UnsupportedComponentException();
    }
    
    @Override
    public ContainerCell newContainer() {
        throw new UnsupportedComponentException();
    }
    
    @Override
    @Deprecated
    public LabelCell newLabel(String labelText) {
        throw new UnsupportedComponentException();
    }
    
    @Override
    public TextAreaCell newTextArea(String areaText) {
        throw new UnsupportedComponentException();
    }
    
    @Override
    public ComputationCell newComputation() {
        throw new UnsupportedComponentException();
    }
}