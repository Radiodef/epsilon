/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.app.*;
import static eps.util.Literals.*;

import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

class MainWindowScene extends Scene {
    private final BorderPane root;
    
    private final FlowPane tools;
    
    private final Button settings;
    private final Button symbols;
    private final Button multiline;
    private final Button alwaysOnTop;
    
    private final TextField field;
    
    MainWindowScene() {
        super(new BorderPane());
        root = (BorderPane) getRoot();
        
        field = new TextField();
        field.setStyle(FxConstants.NO_FOCUS_TEXT_FIELD_CSS_STYLE);
        root.setBottom(field);
        
        field.setOnAction(e -> commandEntered());
        
        int maj = Settings.getOrDefault(Setting.MAJOR_MARGIN);
        
        tools = new FlowPane(maj, 0);
        tools.setPadding(new Insets(maj));
        
        settings    = new Button("Settings");
        symbols     = new Button("Symbols");
        alwaysOnTop = new Button("Pin");
        multiline   = new Button("Multiline");
        
        forEach(settings,
                symbols,
                alwaysOnTop,
                multiline,
                btn -> {
//            btn.setStyle("-fx-background-color: transparent;"
//                       + "-fx-padding: 0;"
//                       + "-fx-background-radius: 0;");
            tools.getChildren().add(btn);
        });
        
        root.setTop(tools);
    }
    
    String getCommandSource() {
        return field.getText();
    }
    
    void setCommandSource(String source) {
        field.setText(source == null ? "" : source);
    }
    
    protected void commandEntered() {
    }
}