/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.app.*;
import static eps.app.App.*;

import javafx.application.*;
import javafx.stage.*;
import java.util.concurrent.atomic.*;

public final class FxApplication extends Application {
    private static final AtomicReference<FxApplication> INSTANCE = new AtomicReference<>();
    
    public FxApplication() {
        if (!INSTANCE.compareAndSet(null, this)) {
            throw new IllegalStateException("application already created!");
        }
    }
    
    static FxApplication instance() {
        FxApplication instance = INSTANCE.get();
        if (instance == null) {
            throw new IllegalStateException("application not yet created!");
        }
        return instance;
    }
    
    private Stage primaryStage;
    
    Stage getPrimaryStage() {
        FxTools.requireFxThread();
        if (primaryStage == null) {
            throw new IllegalStateException("no primary Stage set!");
        }
        return primaryStage;
    }
    
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        
        setUserAgentStylesheet(STYLESHEET_MODENA);
        Platform.setImplicitExit(false);
        
        primaryStage.setOnCloseRequest(e -> {
            Log.entering(getClass(), "handle");
            app().exit(Platform::exit);
        });
        
        Runnable afterLaunch = AFTER_LAUNCH.get();
        if (afterLaunch != null) {
            afterLaunch.run();
        }
    }
    
    private static final AtomicReference<Runnable> AFTER_LAUNCH = new AtomicReference<>();
    
    public static void launch(Runnable afterLaunch) {
        if (afterLaunch == null) {
            afterLaunch = () -> {};
        }
        if (!AFTER_LAUNCH.compareAndSet(null, afterLaunch)) {
            throw new IllegalStateException("application already launched!");
        }
        
        Application.launch(FxApplication.class, EpsilonMain.getArguments().toArray(new String[0]));
    }
}