/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.app.*;
import eps.ui.BoundingBox;
import static eps.app.App.*;

import javafx.application.*;
import javafx.stage.*;
import javafx.geometry.*;
import java.util.*;

final class FxTools {
    private FxTools() {
    }
    
    static void requireFxThread() {
        if (!Platform.isFxApplicationThread()) {
            throw new IllegalStateException(Thread.currentThread().toString());
        }
    }
    
    static void runLater(Runnable task) {
        Platform.runLater(task);
    }
    
    static void runNow(Runnable task) {
        FxAsyncExecutor executor = (FxAsyncExecutor) app().getAsyncExecutor();
        executor.executeNow(task);
    }
    
    static BoundingBox newBounds(double x, double y, double width, double height) {
        if (Double.isNaN(x))
            x = 0;
        if (Double.isNaN(y))
            y = 0;
        if (Double.isNaN(width))
            width  = 0;
        if (Double.isNaN(height))
            height = 0;
        return new BoundingBox(y, x + width, y + height, x);
    }
    
    static BoundingBox getBounds(Stage stage) {
        return newBounds(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
    }
    
    static void setBounds(Stage stage, BoundingBox bounds) {
        stage.setX(bounds.west);
        stage.setY(bounds.north);
        stage.setWidth(bounds.width());
        stage.setHeight(bounds.height());
    }
    
    static void setBounds(Stage stage, Rectangle2D rect) {
        stage.setX(rect.getMinX());
        stage.setY(rect.getMinY());
        stage.setWidth(rect.getWidth());
        stage.setHeight(rect.getHeight());
    }
    
    static boolean isBounds(Stage stage, Rectangle2D bounds) {
        if (bounds.getMinX() != stage.getX())
            return false;
        if (bounds.getMinY() != stage.getY())
            return false;
        if (bounds.getWidth() != stage.getWidth())
            return false;
        if (bounds.getHeight() != stage.getHeight())
            return false;
        return true;
    }
    
    static Screen getScreen(Stage stage) {
        List<Screen> list = Screen.getScreensForRectangle(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight());
        Screen    primary = Screen.getPrimary();
        if (list.isEmpty() || list.contains(primary)) {
            return primary;
        } else {
            return list.get(0);
        }
    }
    
    static boolean isMaximized(Stage stage) {
        if (SystemInfo.isMacOsx()) {
            return sizeIsMaximized(stage);
        }
        return stage.isMaximized();
    }
    
    static boolean sizeIsMaximized(Stage stage) {
        return isBounds(stage, getScreen(stage).getVisualBounds());
    }
    
    static boolean isFullScreen(Stage stage) {
        if (SystemInfo.isMacOsxElCapitan()) {
            return isBounds(stage, getScreen(stage).getBounds());
        }
        return stage.isFullScreen();
    }
}