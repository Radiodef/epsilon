/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.ui.*;

final class FxConstants {
    private FxConstants() {
    }
    
    static final String DEFAULT_FONT_FAMILY = "Monospaced";
    
    static final Style DEFAULT_STYLE =
        Style.builder().setForeground(ArgbColor.BLACK)
                       .setSize(new Size(16f))
                       .setFamily(DEFAULT_FONT_FAMILY)
                       .build();
    
    static final String NO_FOCUS_TEXT_FIELD_CSS_STYLE =
          "-fx-background-insets: 0, 1, 2;"
        + "-fx-background-radius: 5, 4, 3;"
        + "-fx-background-color: "
            + "linear-gradient(to bottom, derive(-fx-text-box-border, -10%), -fx-text-box-border),"
            + "linear-gradient(from 0px 0px to 0px 5px, derive(-fx-control-inner-background, -9%), -fx-control-inner-background)"
        + ";";
}