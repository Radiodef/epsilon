/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.app.*;
import eps.ui.BoundingBox;
import static eps.app.App.*;
import static eps.util.Literals.*;

import javafx.stage.*;
import javafx.beans.value.*;
import javafx.animation.*;
import javafx.util.*;
import javafx.geometry.*;
import java.util.*;

// Note: FX's built-in behavior of what happens after fullscreen
//       is cancelled on Mac OS is totally broken. Maybe this only
//       applies to El Capitan, so we'll have to come back to this
//       when we can test it on Mavericks.
class StageBoundsListener implements ChangeListener<Object> {
    private static final double TIMEOUT_SECONDS = 0.125;
    
    private final Stage stage;
    
    private final PauseTransition timer;
    
    private final boolean maximizeBySize;
    
    private boolean sizeIsMaximized;
    
    private boolean wasMaximizedBeforeFullScreen;
    
    private int suppressCount = 0;
    
    private BoundingBox lastBounds;
    
    StageBoundsListener(Stage stage) {
        this.stage = Objects.requireNonNull(stage, "stage");
        lastBounds = FxTools.getBounds(stage);
        
        timer = new PauseTransition(Duration.seconds(TIMEOUT_SECONDS));
        timer.setOnFinished(e -> boundsChanged());
        
        forEach(stage.xProperty(),
                stage.yProperty(),
                stage.widthProperty(),
                stage.heightProperty(),
                prop -> {
            prop.addListener(this);
        });
        
        maximizeBySize = SystemInfo.isMacOsx();
        
        stage.fullScreenProperty().addListener((a, b, c) -> {
            boolean isFullScreen = isFullScreen();
            fullScreenChanged(isFullScreen);
            if (isFullScreen) {
                wasMaximizedBeforeFullScreen = isMaximized();
            } else {
                if (maximizeBySize) {
                    // TODO
//                    Log.note("sizeIsMaximized = " + sizeIsMaximized);
//                    FxTools.runLater(() -> setMaximizedBySize(wasMaximizedBeforeFullScreen));
                } else {
                    if (!isMaximized()) {
                        setBoundsSilently(lastBounds);
                    }
                }
            }
        });
        
        if (maximizeBySize) {
            sizeIsMaximized = isMaximized();
        } else {
            stage.maximizedProperty().addListener((a, b, c) -> {
                boolean isMaximized = isMaximized();
                maximizedChanged(isMaximized);
                if (!isMaximized) {
                    setBoundsSilently(lastBounds);
                }
            });
        }
        
        if (SystemInfo.isMacOsxElCapitan()) {
            stage.fullScreenProperty().addListener((a, b, c) -> {
                if (stage.isShowing() && stage.isAlwaysOnTop() && !isFullScreen()) {
                    stage.hide();
                    stage.show();
                }
            });
            stage.iconifiedProperty().addListener((a, b, c) -> {
                if (!stage.isIconified() && stage.isAlwaysOnTop()) {
                    stage.hide();
                    stage.show();
                }
            });
        }
    }
    
    Stage getStage() {
        return stage;
    }
    
    BoundingBox getBounds() {
        return lastBounds;
    }
    
    void setBounds(BoundingBox bounds) {
        if (!isWindowed()) {
            if (bounds == null) {
                Rectangle2D def = getDefaultBounds();
                bounds = FxTools.newBounds(def.getMinX(), def.getMinY(), def.getWidth(), def.getHeight());
            }
            lastBounds = bounds;
            return;
        }
        if (bounds != null) {
            lastBounds = bounds;
            FxTools.setBounds(stage, bounds);
            
        } else {
            Rectangle2D def = getDefaultBounds();
            
            stage.setWidth(def.getWidth());
            stage.setHeight(def.getHeight());
            stage.centerOnScreen();
            
            lastBounds = FxTools.getBounds(stage);
        }
    }
    
    private Rectangle2D getDefaultBounds() {
        Rectangle2D visual = Screen.getPrimary().getVisualBounds();
        double      width  = visual.getWidth();
        double      height = visual.getHeight();
        
        double aspectRatio = width / height;
        double newArea     = width * height * app().getSetting(Setting.DEFAULT_WINDOW_AREA_RATIO);
        
        width  = Math.sqrt(newArea * aspectRatio);
        height = (width / aspectRatio);
        
        double x = visual.getMinX() + (visual.getWidth()  / 2.0) - (width  / 2.0);
        double y = visual.getMinY() + (visual.getHeight() / 2.0) - (height / 2.0);
        
        return new Rectangle2D(x, y, width, height);
    }
    
    void setBoundsSilently(BoundingBox bounds) {
        doSilently(() -> setBounds(bounds));
    }
    
    private void doSilently(Runnable task) {
        ++suppressCount;
        try {
            task.run();
        } finally {
            --suppressCount;
            if (suppressCount < 0) {
                assert false : suppressCount;
                suppressCount = 0;
            }
        }
    }
    
    @Override
    public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
        if (suppressCount == 0) {
            timer.playFromStart();
        }
    }
    
    private void boundsChanged() {
        if (stage.isShowing()) {
            BoundingBox bounds = FxTools.getBounds(stage);
            Log.note(getClass(), "boundsChanged", "bounds = ", bounds);
            
            if (isWindowed()) {
                // possible OS X El Capitan bug? sometimes after leaving
                // "maximized" state, the bounds are all messed up with the
                // width at 0.
                // steps to reproduce:
                //  1. maximize the main window by double clicking the title bar.
                //  2. close and reopen the program.
                //  3. double click the title bar to leave the "maximized" state.
                //  4. window disappears because the width is 0.
                if (bounds.width() != 0 && bounds.height() != 0) {
                    lastBounds = bounds;
                } else {
                    setBoundsSilently(lastBounds);
                }
                
                boundsChanged(lastBounds);
            }
            
            if (maximizeBySize && !isFullScreen()) {
                if (bounds.width() == 0 || bounds.height() == 0) {
                    setMaximizedBySize(sizeIsMaximized);
                }
                
                boolean isMaximized = FxTools.sizeIsMaximized(stage); // isMaximized();
                
                if (sizeIsMaximized != isMaximized) {
                    sizeIsMaximized = isMaximized;
                    Log.note(getClass(), "boundsChanged", "sizeIsMaximized = ", isMaximized);
                    
                    maximizedChanged(isMaximized);
                }
            }
        }
    }
    
    protected void boundsChanged(BoundingBox bounds) {
    }
    
    boolean isWindowed() {
        return !isMaximized() && !isFullScreen();
    }
    
    boolean isMaximized() {
        if (maximizeBySize) {
            return sizeIsMaximized;
//            return FxTools.sizeIsMaximized(stage);
        } else {
            return FxTools.isMaximized(stage);
        }
    }
    
    void setMaximized(boolean isMaximized) {
        if (isMaximized != isMaximized()) {
            if (maximizeBySize) {
                setMaximizedBySize(isMaximized);
            } else {
                stage.setMaximized(isMaximized);
            }
            maximizedChanged(isMaximized);
        }
    }
    
    private void setMaximizedBySize(boolean isMaximized) {
        doSilently(() -> {
            if (isMaximized) {
                FxTools.setBounds(stage, FxTools.getScreen(stage).getVisualBounds());
            } else if (lastBounds != null) {
                FxTools.setBounds(stage, lastBounds);
            } else {
                FxTools.setBounds(stage, getDefaultBounds());
            }
            sizeIsMaximized = isMaximized;
        });
    }
    
    boolean isFullScreen() {
        return FxTools.isFullScreen(stage);
    }
    
    void setFullScreen(boolean isFullScreen) {
        stage.setFullScreen(isFullScreen);
    }
    
    protected void maximizedChanged(boolean isMaximized) {
    }
    
    protected void fullScreenChanged(boolean isFullScreen) {
    }
}