/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.app.*;
import eps.ui.*;
import static eps.app.App.*;
import static eps.ui.fx.FxTools.*;

import javafx.application.*;
import javafx.stage.*;

final class FxMainWindow implements MainWindow {
    private final Stage stage;
    
    private final MainWindowScene scene;
    
    private final StageBoundsListener stageBoundsListener;
    
    private final EventCallbacks eventCallbacks = new EventCallbacks();
    
    private final Settings.Listeners settingsListeners;
    
    FxMainWindow() {
        Log.constructing(FxMainWindow.class);
        requireFxThread();
        
        stage = FxApplication.instance().getPrimaryStage();
        
        stageBoundsListener = new StageBoundsListener(stage) {
            @Override
            protected void boundsChanged(BoundingBox box) {
                eventCallbacks.windowBoundsChanged(FxMainWindow.this, box);
            }
            @Override
            protected void maximizedChanged(boolean isMaximized) {
                eventCallbacks.windowMaximizedChanged(FxMainWindow.this, isMaximized);
            }
            @Override
            protected void fullScreenChanged(boolean isFullScreen) {
                eventCallbacks.windowFullScreenChanged(FxMainWindow.this, isFullScreen);
            }
        };
        
        stage.setOnShown(e -> {
            eventCallbacks.windowFirstShown(FxMainWindow.this);
            stage.setOnShown(null);
        });
        
        scene = new MainWindowScene() {
            @Override
            protected void commandEntered() {
                eventCallbacks.commandSent(FxMainWindow.this, getCommandSource());
            }
        };
        
        stage.setScene(scene);
        
        Settings sets = app().getSettings();
        settingsListeners = new Settings.Listeners(sets);
        
        settingsListeners.add(Setting.MAIN_WINDOW_ALWAYS_ON_TOP, new AlwaysOnTopListener(this));
    }
    
    @Override
    public boolean isVisible() {
        requireFxThread();
        return stage.isShowing();
    }
    
    @Override
    public void setVisible(boolean isVisible) {
        requireFxThread();
        if (isVisible) {
            stage.show();
        } else {
            stage.hide();
        }
    }
    
    @Override
    public BoundingBox getBounds() {
        requireFxThread();
        return stageBoundsListener.getBounds();
    }
    
    @Override
    public void setBounds(BoundingBox bounds) {
        requireFxThread();
        stageBoundsListener.setBoundsSilently(bounds);
    }
    
    @Override
    public void close() {
        Log.entering(getClass(), "close");
        if (Platform.isFxApplicationThread()) {
            settingsListeners.close();
            stage.close();
        } else {
            FxTools.runNow(this::close);
        }
    }
    
    @Override
    public boolean isAlwaysOnTop() {
        requireFxThread();
        return stage.isAlwaysOnTop();
    }
    
    @Override
    public void setAlwaysOnTop(boolean shouldBeAlwaysOnTop) {
        requireFxThread();
        stage.setAlwaysOnTop(shouldBeAlwaysOnTop);
    }
    
    @Override
    public boolean isMaximized() {
        requireFxThread();
        return stageBoundsListener.isMaximized();
    }
    
    @Override
    public void setMaximized(boolean isMaximized) {
        requireFxThread();
        stageBoundsListener.setMaximized(isMaximized);
    }
    
    @Override
    public boolean isFullScreen() {
        requireFxThread();
        return stageBoundsListener.isFullScreen();
    }
    
    @Override
    public void setFullScreen(boolean isFullScreen) {
        requireFxThread();
        stageBoundsListener.setFullScreen(isFullScreen);
    }
    
    @Override
    public boolean isWindowed() {
        return !isMaximized();
    }
    
    @Override
    public String getTitleText() {
        requireFxThread();
        return stage.getTitle();
    }
    
    @Override
    public void setTitleText(String titleText) {
        requireFxThread();
        stage.setTitle(titleText);
    }
    
    @Override
    public void addEventCallback(EventCallback callback) {
        requireFxThread();
        eventCallbacks.add(callback);
    }
    
    @Override
    public void removeEventCallback(EventCallback callback) {
        requireFxThread();
        eventCallbacks.remove(callback);
    }
    
    @Override
    public String getCommandText() {
        requireFxThread();
        return scene.getCommandSource();
    }
    
    @Override
    public void setCommandText(String commandText) {
        requireFxThread();
        scene.setCommandSource(commandText);
    }
    
    @Override
    public int indexOfCell(Cell cell) {
        requireFxThread();
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void addCell(Cell cell) {
        requireFxThread();
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void insertCell(int cellIndex, Cell cell) {
        requireFxThread();
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void removeCell(Cell cell) {
        requireFxThread();
        throw new UnsupportedOperationException();
    }
    
    private static final class AlwaysOnTopListener
            extends FxSettingsWeakListener.SimpleListener<Boolean, FxMainWindow> {
        private AlwaysOnTopListener(FxMainWindow window) {
            super(window);
        }
        @Override
        protected void settingChangedOnFx(FxMainWindow window, Boolean alwaysOnTop) {
            window.setAlwaysOnTop(alwaysOnTop);
        }
    }
}