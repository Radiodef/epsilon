/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui.fx;

import eps.app.*;

import javafx.application.*;
import java.util.function.*;
import java.util.*;

abstract class FxSettingsWeakListener<T, R> extends Settings.WeakListener<T, R> {
    FxSettingsWeakListener(R referent) {
        super(referent);
    }
    
    @Override
    protected final void settingChanged(R          referent,
                                        Settings   settings,
                                        Setting<T> setting,
                                        T          oldValue,
                                        T          newValue) {
        if (Platform.isFxApplicationThread()) {
            settingChangedOnFx(referent, settings, setting, oldValue, newValue);
        } else {
            Platform.runLater(() -> settingChangedOnFx(referent, settings, setting, oldValue, newValue));
        }
    }
    
    protected abstract void settingChangedOnFx(R          referent,
                                               Settings   settings,
                                               Setting<T> setting,
                                               T          oldValue,
                                               T          newValue);
    
    static abstract class SimpleListener<T, R> extends FxSettingsWeakListener<T, R> {
        SimpleListener(R referent) {
            super(referent);
        }
        
        @Override
        protected void settingChangedOnFx(R          ref,
                                          Settings   sets,
                                          Setting<T> set,
                                          T          old,
                                          T          val) {
            settingChangedOnFx(ref, val);
        }
        
        protected abstract void settingChangedOnFx(R ref, T val);
    }
    
    static class FromBiConsumer<T, R> extends SimpleListener<T, R> {
        protected final BiConsumer<R, T> handler;
        
        FromBiConsumer(R referent, BiConsumer<R, T> handler) {
            super(referent);
            this.handler = Objects.requireNonNull(handler, "handler");
        }
        
        @Override
        protected void settingChangedOnFx(R ref, T val) {
            handler.accept(ref, val);
        }
    }
    
    static <T, R> FxSettingsWeakListener<T, R> create(R referent, BiConsumer<R, T> handler) {
        return new FromBiConsumer<>(referent, handler);
    }
}