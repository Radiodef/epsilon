/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.app.*;
import eps.json.*;
import eps.util.*;

import java.util.*;
import java.lang.reflect.*;

interface StyleCommon<S> {
    ArgbColor getForeground();
    S setForeground(ArgbColor foreground);
    default boolean isForegroundSet() { return getForeground() != null; }
    
    ArgbColor getBackground();
    S setBackground(ArgbColor background);
    default boolean isBackgroundSet() { return getBackground() != null; }
    
    String getFamily();
    S setFamily(String family);
    default boolean isFamilySet() { return getFamily() != null; }
    
    Size getSize();
    S setSize(Size size);
    default boolean isSizeSet() { return getSize() != null; }
    
    default S setSize(String size) { return setSize(Size.parse(size)); }
    
    default S setSize(int relativeSign, float absoluteValue) {
        return setSize(new Size(relativeSign, absoluteValue));
    }
    
    default S setSize(float absoluteValue) {
        return setSize(new Size(absoluteValue));
    }
    
    Boolean isBold();
    S setBold(Boolean isBold);
    default boolean isBoldSet() { return isBold() != null; }
    
    Boolean isItalic();
    S setItalic(Boolean isItalic);
    default boolean isItalicSet() { return isItalic() != null; }
    
    Boolean isStrike();
    S setStrike(Boolean isStrike);
    default boolean isStrikeSet() { return isStrike() != null; }
    
    Boolean isUnderline();
    S setUnderline(Boolean isUnderline);
    default boolean isUnderlineSet() { return isUnderline() != null; }
    
//    Boolean isOverline();
//    S setOverline(Boolean isOverline);
//    default boolean isOverlineSet() { return isOverline() != null; }
    
    Object getByName(String propertyName);
    S setByName(String propertyName, Object val);
    S parseByName(String propertyName, String val);
    
    StringBuilder toJsonString(StringBuilder b);
    default String toJsonString() {
        return this.toJsonString(new StringBuilder()).toString();
    }
    StringBuilder toCssString(StringBuilder b);
    default String toCssString() {
        return this.toCssString(new StringBuilder()).toString();
    }
}

/**
 * Represents the style of text. Styles are immutable.
 * <p>
 * All properties of a style may be unspecified, marked with a value of {@code null}.
 * 
 * @author  David
 * @see     Style.Builder
 */
@JsonSerializable
public final class Style implements StyleCommon<Style> {
    @JsonProperty
    private final ArgbColor foreground;
    @JsonProperty
    private final ArgbColor background;
    @JsonProperty
    private final String family;
    @JsonProperty
    private final Size size;
    @JsonProperty
    private final long flags;
    
    /**
     * Constructs a new empty style.
     */
    public Style() {
        this((ArgbColor) null, (ArgbColor) null, (String) null, (Float) null, 0L);
    }
    
    public static final Style EMPTY = new Style();
    
    public static Style replnull(Style s) { return s == null ? EMPTY : s; }
    
//    public static final float MIN_SIZE = 0f;
    
    @JsonConstructor
    private Style(ArgbColor foreground, ArgbColor background, String family, Object size, long flags) {
        this.foreground = foreground;
        this.background = background;
        this.family     = family;
        this.size       = (size == null) ? null : (size instanceof Size) ? (Size) size : new Size(((Number) size).floatValue());
        this.flags      = flags;
        assert this.size == null || this.size.getAbsoluteValue() >= 0f : this.size;
//        if (this.size != null && this.size.getAbsoluteValue() < MIN_SIZE) {
//            throw new IllegalArgumentException("size = " + size);
//        }
    }
    
    /**
     * Copies the specified style. If the style is {@code null}, the the
     * new style is initialized as if by {@code new Style()}.
     * 
     * @param toCopy the style to copy.
     */
    public Style(Style toCopy) {
        if (toCopy == null) {
            this.foreground = null;
            this.background = null;
            this.family     = null;
            this.size       = null;
            this.flags      = 0L;
        } else {
            this.foreground = toCopy.foreground;
            this.background = toCopy.background;
            this.family     = toCopy.family;
            this.size       = toCopy.size;
            this.flags      = toCopy.flags;
        }
    }
    
    public Style(ArgbColor foreground) {
        this(foreground, null, null, null, 0L);
    }
    
    public Style(ArgbColor foreground, Boolean isBold, Boolean isItalic) {
        this(foreground, null, null, null, set(set(0, Style.BOLD_BIT, isBold), Style.ITALIC_BIT, isItalic));
    }
    
    /**
     * Parses a JSON object.
     * 
     * @param   text the JSON to parse.
     * @throws  IllegalArgumentException if the JSON cannot be parsed or is
     *          otherwise invalid for a Style.
     */
    public Style(String text) {
        this(Style.parse(text));
    }
    
    /**
     * Parses a JSON object.
     * 
     * @param   text the JSON to parse.
     * @return  a new style from the specified JSON or {@code null} if the
     *          text was {@code "null"}.
     * @throws  IllegalArgumentException if the JSON cannot be parsed or is
     *          otherwise invalid for a Style.
     */
    public static Style parse(String text) {
        try {
            return (Style) Json.objectify(text);
        } catch (ClassCastException x) {
            throw new IllegalArgumentException(text, x);
        } catch (IllegalArgumentException x) {
            Log.caught(Style.class, "<init>", x, true);
        }
        JsonValue json = Json.parse(text);
        if (Json.isNull(json)) {
            return null;
        } else {
            try {
                Map<String, JsonValue> map = DESTRUCTURER.destructure(json);
                
                Builder builder = Style.builder();
                
                for (Map.Entry<String, JsonValue> e : map.entrySet()) {
                    builder.setByName(e.getKey(), e.getValue());
                }
                
                return builder.build();
                
            } catch (IllegalArgumentException | ClassCastException x) {
                throw new IllegalArgumentException(text, x);
            }
        }
    }
    
    @Override
    public ArgbColor getForeground() {
        return this.foreground;
    }
    
    @Override
    public Style setForeground(ArgbColor foreground) {
        return new Style(foreground, this.background, this.family, this.size, this.flags);
    }
    
    @Override
    public ArgbColor getBackground() {
        return this.background;
    }
    
    @Override
    public Style setBackground(ArgbColor background) {
        return new Style(this.foreground, background, this.family, this.size, this.flags);
    }
    
    @Override
    public String getFamily() {
        return this.family;
    }
    
    @Override
    public Style setFamily(String family) {
        return new Style(this.foreground, this.background, family, this.size, this.flags);
    }
    
    @Override
    public Size getSize() {
        return this.size;
    }
    
    @Override
    public Style setSize(Size size) {
        return new Style(this.foreground, this.background, this.family, size, this.flags);
    }
    
    @Override
    public Boolean isBold() {
        return this.get(Style.BOLD_BIT);
    }
    
    @Override
    public Style setBold(Boolean isBold) {
        return new Style(this.foreground, this.background, this.family, this.size, this.set(Style.BOLD_BIT, isBold));
    }
    
    @Override
    public Boolean isItalic() {
        return this.get(Style.ITALIC_BIT);
    }
    
    @Override
    public Style setItalic(Boolean isItalics) {
        return new Style(this.foreground, this.background, this.family, this.size, this.set(Style.ITALIC_BIT, isItalics));
    }
    
    @Override
    public Boolean isStrike() {
        return this.get(Style.STRIKE_BIT);
    }
    
    @Override
    public Style setStrike(Boolean isStrike) {
        return new Style(this.foreground, this.background, this.family, this.size, this.set(Style.STRIKE_BIT, isStrike));
    }
    
    @Override
    public Boolean isUnderline() {
        return this.get(Style.UNDER_BIT);
    }
    
    @Override
    public Style setUnderline(Boolean isUnderline) {
        return new Style(this.foreground, this.background, this.family, this.size, this.set(Style.UNDER_BIT, isUnderline));
    }
    
//    @Override
//    public Boolean isOverline() {
//        return this.get(Style.OVER_BIT);
//    }
//    
//    @Override
//    public Style setOverline(Boolean isOverline) {
//        return new Style(this.foreground, this.background, this.family, this.size, this.set(Style.OVER_BIT, isOverline));
//    }
    
    public Style derive(Style that) {
        Builder builder = new Builder(this);
        
        Size size1 = this.getSize();
        Size size2 = that.getSize();
        
        Size size;
        if (size1 == null) {
            size = size2;
        } else {
            size = (size2 == null) ? size1 : size1.derive(size2);
        }
        
        return builder.setIfPresent(that).setSize(size).build();
    }
    
    @Override
    public Object getByName(String propertyName) {
        return Style.getByName(this, propertyName);
    }
    
    /**
     * Sets a property by its name, case insensitively. The property name
     * is the name to the right of "set", "get" or "is" in the setter and
     * getter for that property. For example, "foreground" or "italics".
     * <p>
     * If the specified property denotes a {@code float} or {@code boolean}
     * property and the specified value is {@code null}, then the value is
     * converted to the appropriate non-value for that property, either {@code -1f}
     * or {@code false}, respectively.
     * 
     * @param   propertyName    the name of the property to set.
     * @param   val             the new value for the specified property.
     * @return  a copy of this style with the specified property set to the
     *          specified value.
     * @throws  NullPointerException if the property name is {@code null}.
     * @throws  IllegalArgumentException if the specified property name
     *          does not denote an existing property.
     * @throws  ClassCastException if the specified value is of the wrong
     *          type for the specified property.
     * 
     * @see #isProperty(String)
     */
    @Override
    public Style setByName(String propertyName, Object val) {
        return Style.setByName(this, propertyName, val);
    }
    
    /**
     * Sets a property by its name, case insensitively. The property name
     * is the name to the right of "set", "get" or "is" in the setter and
     * getter for that property. For example, "foreground" or "italics".
     * <p>
     * The specified {@code String} is parsed to produce a value.
     * <p>
     * If the specified property denotes a {@code float} or {@code boolean}
     * property and the specified value is {@code "null"}, then the value is
     * converted to the appropriate non-value for that property, either {@code -1f}
     * or {@code false}, respectively.
     * 
     * @param   propertyName    the name of the property to set.
     * @param   val             the new value to parse for the specified property.
     * @return  a copy of this style with the specified property set to the
     *          specified value.
     * @throws  NullPointerException if the property name is {@code null}.
     * @throws  IllegalArgumentException if the specified property name
     *          does not denote an existing property.
     * 
     * @see #isProperty(String)
     */
    @Override
    public Style parseByName(String propertyName, String val) {
        return Style.parseByName(this, propertyName, val);
    }
    
    @Override
    public int hashCode() {
        int hash = Objects.hashCode(foreground);
        hash = 31 * hash + Objects.hashCode(background);
        hash = 31 * hash + Objects.hashCode(family);
        hash = 31 * hash + Objects.hashCode(size);
        hash = 31 * hash + Long.hashCode(flags);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Style) {
            Style that = (Style) obj;
            if (!Objects.equals(this.foreground, that.foreground))
                return false;
            if (!Objects.equals(this.background, that.background))
                return false;
            if (!Objects.equals(this.family, that.family))
                return false;
            if (!Objects.equals(this.size, that.size))
                return false;
            if (this.flags != that.flags)
                return false;
            return true;
        }
        return false;
    }
    
    @Override
    public StringBuilder toJsonString(StringBuilder b) {
        return Style.toJsonString0(b, this);
    }
    
    @Override
    public StringBuilder toCssString(StringBuilder b) {
        return Style.toCssString0(b, this);
    }
    
    @Override
    public String toString() {
        return "Style" + this.toJsonString();
    }
    
    private static final String FOREGROUND  = "Foreground";
    private static final String BACKGROUND  = "Background";
    private static final String FAMILY      = "Family";
    private static final String SIZE        = "Size";
    private static final String BOLD        = "Bold";
    private static final String ITALIC      = "Italic";
    private static final String STRIKE      = "Strike";
    private static final String UNDERLINE   = "Underline";
//    private static final String OVERLINE    = "Overline";
    
    private static final List<String> PROPERTIES =
        Arrays.asList(FOREGROUND, BACKGROUND, FAMILY, SIZE, BOLD, ITALIC, STRIKE, UNDERLINE/*, OVERLINE*/);
    private static final List<String> PROPERTIES_LOWER =
        PROPERTIES.stream().map(p -> p.toLowerCase(Locale.ROOT)).collect(java.util.stream.Collectors.toList());
    
    private long set(long bit, Boolean enable) {
        return set(this.flags, bit, enable);
    }
    
    private static long set(long flags, long bit, Boolean enable) {
        if (enable == null) {
            return flags & ~(bit | (bit << 1));
        }
        if (enable) {
            return (flags & ~(bit << 1)) | bit;
        } else {
            return (flags & ~bit) | (bit << 1);
        }
    }
    
    private Boolean get(long bit) {
        long flags = this.flags;
        if ((flags & bit) != 0) {
            return Boolean.TRUE;
        }
        if ((flags & (bit << 1)) != 0) {
            return Boolean.FALSE;
        }
        return null;
    }
    
    private static final long BOLD_BIT   = 1L << (2 * 0);
    private static final long ITALIC_BIT = 1L << (2 * 1);
    private static final long STRIKE_BIT = 1L << (2 * 2);
    private static final long UNDER_BIT  = 1L << (2 * 3);
//    private static final long OVER_BIT   = 1L << (2 * 4);
    
    private static StringBuilder toJsonString0(StringBuilder b, StyleCommon<?> instance) {
        Objects.requireNonNull(b, "b");
        b.append("{ ");
        
        int c = 0;
        
        for (String prop : PROPERTIES_LOWER) {
            Accessor a = ACCESSORS.get(prop);
            assert a != null : prop;
            String s = a.toString(instance);
            if (s != null) {
                if (c++ != 0) {
                    b.append(", ");
                }
                b.append('"').append(prop).append("\": ");
                if (a.type == String.class || a.type == Size.class) {
                    b.append('"').append(s).append('"');
                } else {
                    b.append(s);
                }
            }
        }
        
        b.append(" }");
        return b;
    }
    
    private static boolean isTrue(Boolean b) {
        return b != null && b;
    }
    private static StringBuilder semi(int c, StringBuilder b) {
        if (c != 0) {
            b.append("; ");
        }
        return b;
    }
    private static StringBuilder bar(int c, int d, StringBuilder b) {
        if (d == 0) {
            semi(c, b).append("text-decoration:");
        } else {
            b.append("|");
        }
        return b;
    }
    
    private static StringBuilder toCssString0(StringBuilder b, Style s) {
        Objects.requireNonNull(b, "b");
        int c = 0;
        
        if (s.isForegroundSet()) {
            semi(c++, b).append("color:#").append(s.getForeground().toHexRgbString());
        }
        if (s.isBackgroundSet()) {
            semi(c++, b).append("background-color:#").append(s.getBackground().toHexRgbString());
        }
        if (s.isFamilySet()) {
            semi(c++, b).append("font-family:'").append(s.getFamily()).append("'");
        }
        if (s.isSizeSet()) {
            semi(c++, b).append("font-size:").append(Math.round(Settings.getAbsoluteSize(s))).append("pt");
        }
        if (isTrue(s.isBold())) {
            semi(c++, b).append("font-weight:bold");
        }
        if (isTrue(s.isItalic())) {
            semi(c++, b).append("font-style:italics");
        }
        
        int d = 0;
        
        if (isTrue(s.isStrike())) {
            bar(c++, d++, b).append("line-through");
        }
        if (isTrue(s.isUnderline())) {
            bar(c++, d++, b).append("underline");
        }
//        if (isTrue(s.isOverline())) {
//            bar(c++, d++, b).append("overline");
//        }
        
        return b;
    }
    
    /**
     * Returns a new builder whose properties are a copy of this style.
     * 
     * @return a new builder whose properties are a copy of this style.
     */
    public Style.Builder toBuilder() {
        return new Style.Builder(this);
    }
    
    /**
     * Returns a new, empty builder.
     * 
     * @return a new, empty builder.
     */
    public static Style.Builder builder() {
        return new Style.Builder();
    }
    
    @JsonSerializable
    public static final class Builder implements Cloneable, StyleCommon<Builder> {
        @JsonProperty
        private ArgbColor foreground = null;
        @JsonProperty
        private ArgbColor background = null;
        @JsonProperty
        private String family = null;
        @JsonProperty
        private Size size = null;
        @JsonProperty
        private Boolean isBold = null;
        @JsonProperty
        private Boolean isItalic = null;
        @JsonProperty
        private Boolean isStrike = null;
        @JsonProperty
        private Boolean isUnderline = null;
//        @JsonProperty
//        private Boolean isOverline = null;
        
        /**
         * Constructs a new, empty builder.
         */
        public Builder() {
        }
        
        @JsonConstructor
        private Builder(ArgbColor foreground,
                        ArgbColor background,
                        String    family,
                        Size      size,
                        Boolean   isBold,
                        Boolean   isItalic,
                        Boolean   isStrike,
                        Boolean   isUnderline/*,
                        Boolean   isOverline*/) {
            this.foreground  = foreground;
            this.background  = background;
            this.family      = family;
            this.size        = size;
            this.isBold      = isBold;
            this.isItalic    = isItalic;
            this.isStrike    = isStrike;
            this.isUnderline = isUnderline;
//            this.isOverline  = isOverline;
        }
        
        /**
         * Copies all of the properties from the specified style.
         * 
         * @param   style the style to copy.
         * @throws  NullPointerException if {@code style} is {@code null}.
         */
        public Builder(Style style) {
            this.set(style);
        }
        
        /**
         * Copies all of the properties from the specified style.
         * 
         * @param   style the style to copy.
         * @return  {@code this}
         * @throws  NullPointerException if {@code style} is {@code null}.
         */
        public Builder set(Style style) {
            if (style == null)
                return this.clear();
            return this.setForeground(style.getForeground())
                       .setBackground(style.getBackground())
                       .setFamily    (style.getFamily()    )
                       .setSize      (style.getSize()      )
                       .setBold      (style.isBold()       )
                       .setItalic    (style.isItalic()     )
                       .setStrike    (style.isStrike()     )
                       .setUnderline (style.isUnderline()  )
                       /*.setOverline  (style.isOverline()   )*/;
        }
        
        public Builder clear() {
            foreground  = null;
            background  = null;
            family      = null;
            size        = null;
            isBold      = null;
            isItalic    = null;
            isStrike    = null;
            isUnderline = null;
//            isOverline  = null;
            return this;
        }
        
        /**
         * Copies all of the properties from the specified style which are
         * present.
         * 
         * @param   style the style to copy.
         * @return  {@code this}
         */
        public Builder setIfPresent(Style style) {
            if (style != null) {
                if (style.isForegroundSet()) this.setForeground(style.getForeground());
                if (style.isBackgroundSet()) this.setBackground(style.getBackground());
                if (style.isFamilySet()    ) this.setFamily    (style.getFamily()    );
                if (style.isSizeSet()      ) this.setSize      (style.getSize()      );
                if (style.isBoldSet()      ) this.setBold      (style.isBold()       );
                if (style.isItalicSet()    ) this.setItalic    (style.isItalic()     );
                if (style.isStrikeSet()    ) this.setStrike    (style.isStrike()     );
                if (style.isUnderlineSet() ) this.setUnderline (style.isUnderline()  );
//                if (style.isOverlineSet()  ) this.setOverline  (style.isOverline()   );
            }
            return this;
        }
        
        @Override
        public Object getByName(String propertyName) {
            return Style.getByName(this, propertyName);
        }
        
        /**
         * Sets a property by its name, case insensitively. The property name
         * is the name to the right of "set", "get" or "is" in the setter and
         * getter for that property. For example, "foreground" or "italics".
         * <p>
         * If the specified property denotes a {@code float} or {@code boolean}
         * property and the specified value is {@code null}, then the value is
         * converted to the appropriate non-value for that property, either {@code -1f}
         * or {@code false}, respectively.
         * 
         * @param   propertyName    the name of the property to set.
         * @param   val             the new value for the specified property.
         * @return  {@code this}
         * @throws  NullPointerException if the property name is {@code null}.
         * @throws  IllegalArgumentException if the specified property name
         *          does not denote an existing property.
         * @throws  ClassCastException if the specified value is of the wrong
         *          type for the specified property.
         * 
         * @see Style#isProperty(String)
         */
        @Override
        public Builder setByName(String propertyName, Object val) {
            return Style.setByName(this, propertyName, val);
        }
        
        /**
         * Sets a property by its name, case insensitively. The property name
         * is the name to the right of "set", "get" or "is" in the setter and
         * getter for that property. For example, "foreground" or "italics".
         * <p>
         * The specified {@code String} is parsed to produce a value.
         * <p>
         * If the specified property denotes a {@code float} or {@code boolean}
         * property and the specified value is {@code "null"}, then the value is
         * converted to the appropriate non-value for that property, either {@code -1f}
         * or {@code false}, respectively.
         * 
         * @param   propertyName    the name of the property to set.
         * @param   val             the new value to parse for the specified property.
         * @return  {@code this}
         * @throws  NullPointerException if the property name is {@code null}.
         * @throws  IllegalArgumentException if the specified property name
         *          does not denote an existing property.
         * 
         * @see #isProperty(String)
         */
        @Override
        public Builder parseByName(String propertyName, String val) {
            return Style.parseByName(this, propertyName, val);
        }
        
        @Override
        public Builder setForeground(ArgbColor foreground) {
            this.foreground = foreground;
            return this;
        }
        
        @Override
        public ArgbColor getForeground() {
            return this.foreground;
        }
        
        @Override
        public Builder setBackground(ArgbColor background) {
            this.background = background;
            return this;
        }
        
        @Override
        public ArgbColor getBackground() {
            return this.background;
        }
        
        @Override
        public Builder setFamily(String family) {
            this.family = family;
            return this;
        }
        
        @Override
        public String getFamily() {
            return this.family;
        }
        
        @Override
        public Builder setSize(Size size) {
            this.size = size;
            return this;
        }
        
        @Override
        public Size getSize() {
            return this.size;
        }
        
        @Override
        public Builder setBold(Boolean isBold) {
            this.isBold = isBold;
            return this;
        }
        
        @Override
        public Boolean isBold() {
            return this.isBold;
        }
        
        @Override
        public Builder setItalic(Boolean isItalic) {
            this.isItalic = isItalic;
            return this;
        }
        
        @Override
        public Boolean isItalic() {
            return this.isItalic;
        }
        
        @Override
        public Builder setStrike(Boolean isStrike) {
            this.isStrike = isStrike;
            return this;
        }
        
        @Override
        public Boolean isStrike() {
            return this.isStrike;
        }
        
        @Override
        public Builder setUnderline(Boolean isUnderline) {
            this.isUnderline = isUnderline;
            return this;
        }
        
        @Override
        public Boolean isUnderline() {
            return this.isUnderline;
        }
        
//        @Override
//        public Builder setOverline(Boolean isOverline) {
//            this.isOverline = isOverline;
//            return this;
//        }
//        
//        @Override
//        public Boolean isOverline() {
//            return this.isOverline;
//        }
        
        private long set(long flags, long bit, Boolean val) {
            if (val != null) {
                return flags | (val ? bit : (bit << 1));
            }
            return flags;
        }
        
        private long getFlags() {
            long flags = 0L;
            flags = set(flags, Style.BOLD_BIT,   this.isBold());
            flags = set(flags, Style.ITALIC_BIT, this.isItalic());
            flags = set(flags, Style.STRIKE_BIT, this.isStrike());
            flags = set(flags, Style.UNDER_BIT,  this.isUnderline());
//            flags = set(flags, Style.OVER_BIT,   this.isOverline());
            return flags;
        }
        
        @Override
        public StringBuilder toJsonString(StringBuilder b) {
            return Style.toJsonString0(b, this);
        }
        
        @Override
        public StringBuilder toCssString(StringBuilder b) {
            return Style.toCssString0(b, this.build());
        }
        
        @Override
        public String toString() {
            return "Style.Builder" + this.toJsonString();
        }
        
        /**
         * Returns a copy of this builder.
         * 
         * @return a copy of this builder.
         */
        @Override
        public Builder clone() {
            try {
                return (Builder) super.clone();
            } catch (CloneNotSupportedException x) {
                throw new AssertionError("clone", x);
            }
        }
        
        /**
         * Returns a new style whose properties are the same as this builder.
         * 
         * @return a new style whose properties are the same as this builder.
         */
        public Style build() {
            return new Style(this.foreground, this.background, this.family, this.size, this.getFlags());
        }
    }
    
    private static final class StyleDestructurer extends JsonDestructurer {
        @Override
        protected String mapKey(String key) {
            Accessor a = ACCESSORS.get(key);
            return (a != null) ? a.name : null;
        }
        @Override
        protected boolean isOptional(String key) {
            return true;
        }
        @Override
        protected Iterable<String> keys() {
            return PROPERTIES_LOWER;
        }
    }
    
    private static final JsonDestructurer DESTRUCTURER = new StyleDestructurer();
    
    private static final class Accessor {
        private final String   name;
        private final Class<?> type;
        private final Method   styleSet;
        private final Method   styleGet;
        private final Method   builderSet;
        private final Method   builderGet;
        
        private final ObjectParser<?> parser;
        
        private Accessor(String name, Class<?> type) {
            this.name       = Objects.requireNonNull(name, "name");
            this.type       = Objects.requireNonNull(type, "type");
            this.styleSet   = setter(type, name, Style.class);
            this.styleGet   = getter(type, name, Style.class);
            this.builderSet = setter(type, name, Builder.class);
            this.builderGet = getter(type, name, Builder.class);
            this.parser     =
                type == Boolean.class ? ObjectParser.ofBoolean().nullable()
                                      : ObjectParser.ofSimple(Misc.box(type)).nullable();
        }
        
        private static Method setter(Class<?> type, String name, Class<?> clazz) {
            try {
                Method m = clazz.getDeclaredMethod("set" + name, type);
                m.setAccessible(true);
                return m;
            } catch (ReflectiveOperationException x) {
                throw new AssertionError(name, x);
            }
        }
        
        private static Method getter(Class<?> type, String name, Class<?> clazz) {
            return getter(type == Boolean.class ? "is" : "get", name, clazz);
        }
        
        private static Method getter(String prefix, String name, Class<?> clazz) {
            try {
                Method m = clazz.getMethod(prefix + name);
                m.setAccessible(true);
                return m;
            } catch (ReflectiveOperationException x) {
                throw new AssertionError(name, x);
            }
        }
        
        Object get(Object instance) {
            try {
                return (instance instanceof Style ? styleGet : builderGet).invoke(instance);
            } catch (ReflectiveOperationException x) {
                throw new AssertionError(name, x);
            }
        }
        
        Object set(Object instance, Object val) {
            if (val instanceof JsonValue)
                return this.set(instance, (JsonValue) val);
            try {
                return ((instance instanceof Style) ? styleSet : builderSet).invoke(instance, val);
            } catch (ReflectiveOperationException x) {
                throw new AssertionError(name, x);
            }
        }
        
        private Object toValue(JsonValue val) {
            if (Json.isNull(val))
                return null;
            if (type == ArgbColor.class)
                return ArgbColor.parse(JsonString.getChars(val));
            if (type == String.class)
                return JsonString.getChars(val);
//            if (type == Float.class)
//                return (float) ((JsonNumber) val).doubleValue();
            if (type == Size.class)
                return Size.parse(JsonString.getChars(val));
            if (type == Boolean.class)
                return Json.toBoolean(val);
            throw new AssertionError(type + ", " + val);
        }
        
        Object set(Object instance, JsonValue val) {
            try {
                return this.set(instance, this.toValue(val));
            } catch (ClassCastException x) {
                throw new IllegalArgumentException(Json.toString(val), x);
            }
        }
        
        Object parse(Object instance, String text) {
            Object val = parser.parse(text);
            return this.set(instance, val);
        }
        
        String toString(Object instance) {
            Object val = this.get(instance);
            if (val != null) {
                if (type == ArgbColor.class)
                    return ((ArgbColor) val).toJsonString();
                if (type == String.class)
                    return (String) val;
//                if (type == Float.class)
//                    return ((Float) val).toString();
                if (type == Size.class)
                    return ((Size) val).toString();
                if (type == Boolean.class)
                    return ((Boolean) val).toString();
            }
            return null;
        }
        
        @Override
        public String toString() {
            return name + "Accessor";
        }
    }
    
    private static final Map<String, Accessor> ACCESSORS = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    private static void put(String name, Class<?> type) {
        ACCESSORS.put(name, new Accessor(name, type));
    }
    
    private static void put(String name, String synonymn) {
        ACCESSORS.put(synonymn, ACCESSORS.get(name));
    }
    
    private static void putc(String name) {
        put(name, ArgbColor.class);
        put(name, name + "color");
        put(name, name + "-color");
        put(name, name + "_color");
        put(name, name.charAt(0) + "g");
    }
    
    private static void puts(String name) {
        put(name, String.class);
        switch (name) {
            case FAMILY:
                put(name, "font"  + name);
                put(name, "font-" + name);
                put(name, "font_" + name);
                break;
        }
    }
    
//    @Deprecated
//    private static void putf(String name) {
//        put(name, Float.class);
//        switch (name) {
//            case SIZE:
//                put(name, "font"  + name);
//                put(name, "font-" + name);
//                put(name, "font_" + name);
//                break;
//        }
//    }
    
    private static void putz(String name) {
        put(name, Size.class);
        switch (name) {
            case SIZE:
                put(name, "font"  + name);
                put(name, "font-" + name);
                put(name, "font_" + name);
                break;
        }
    }
    
    private static void putbSynonymns(String name) {
        put(name, "is" + name);
        put(name, "is-" + name);
        put(name, "is_" + name);
    }
    
    private static void putb(String name) {
        put(name, Boolean.class);
        putbSynonymns(name);
        switch (name) {
            case ITALIC:
                put(name, "italics");
                putbSynonymns("italics");
                put(name, "i");
                break;
            case BOLD:
                put(name, "b");
                break;
            case UNDERLINE:
//            case OVERLINE:
                put(name, name + "d");
                put(name, "u");
                putbSynonymns(name + "d");
                break;
        }
    }
    
    static {
        putc(FOREGROUND);
        putc(BACKGROUND);
        puts(FAMILY);
        putz(SIZE);
        putb(BOLD);
        putb(ITALIC);
        putb(STRIKE);
        putb(UNDERLINE);
//        putb(OVERLINE);
    }
    
    @SuppressWarnings("unchecked")
    private static <T> T parseByName(T instance, String propertyName, String text) {
        Objects.requireNonNull(propertyName, "propertyName");
        Objects.requireNonNull(text,         "text");
        Accessor a = ACCESSORS.get(propertyName);
        if (a != null) {
            return (T) a.parse(instance, text);
        } else {
            throw new IllegalArgumentException(propertyName);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static <T> T setByName(T instance, String propertyName, Object val) {
        Objects.requireNonNull(propertyName, "propertyName");
        Accessor a = ACCESSORS.get(propertyName);
        if (a != null) {
            return (T) a.set(instance, val);
        } else {
            throw new IllegalArgumentException(propertyName);
        }
    }
    
    private static Object getByName(Object instance, String propertyName) {
        Objects.requireNonNull(propertyName, "propertyName");
        Accessor a = ACCESSORS.get(propertyName);
        if (a != null) {
            return a.get(instance);
        } else {
            throw new IllegalArgumentException(propertyName);
        }
    }
    
    /**
     * Returns {@code true} if the specified name denotes a valid property and
     * {@code false} otherwise.
     * <p>
     * Returns {@code false} of the name is {@code null}.
     * 
     * @param   propertyName    the property name to test.
     * @return  whether the specified name is a property.
     */
    public static boolean isProperty(String propertyName) {
        return propertyName != null && ACCESSORS.containsKey(propertyName);
    }
}