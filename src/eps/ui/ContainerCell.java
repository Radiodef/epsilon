/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.app.*;
import eps.json.*;
import eps.util.*;

import java.util.*;

public interface ContainerCell extends Cell {
    int getCellCount();
    
    default boolean isEmpty() {
        return this.getCellCount() == 0;
    }
    
    List<Cell> getCells();
    
    @Override
    default void collectLines(java.util.function.Consumer<? super String> action) {
        getCells().forEach(cell -> cell.collectLines(action));
    }
    
    int indexOfCell(Cell cell);
    
    default boolean containsCell(Cell cell) {
        return this.indexOfCell(cell) >= 0;
    }
    
    ContainerCell removeAllCells();
    ContainerCell addCell(Cell cell);
    ContainerCell insertCell(int index, Cell cell);
    ContainerCell removeCell(Cell cell);
    
    @Override
    default eps.ui.Packet getPacket() {
        return new Packet(this);
    }
    
    @Override
    default ContainerCell configure(Item item, eps.ui.Packet packet0) {
        Packet packet = (Packet) packet0;
        Cell.super.configure(item, packet);
        
        this.removeAllCells();
        for (eps.ui.Packet p : packet.packets) {
            this.addCell(p.newCell(item));
        }
        
        return this;
    }
    
    @JsonSerializable
    class Packet extends eps.ui.Packet {
        @JsonProperty
        private final List<eps.ui.Packet> packets;
        
        protected Packet(ContainerCell cell) {
            super(cell);
            List<Cell>          cells   = cell.getCells();
            int                 count   = cells.size();
            List<eps.ui.Packet> packets = new ArrayList<>(count);
            for (int i = 0; i < count; ++i) {
                packets.add(cells.get(i).getPacket());
            }
            this.packets = packets;
        }
        
        @JsonConstructor
        protected Packet(BoundingBox margins, List<eps.ui.Packet> packets) {
            super(margins);
            this.packets = Misc.requireNonNull(packets, "packets");
        }
        
        @Override
        public Cell newCell(Item item) {
            return App.app().getComponentFactory().newContainer().configure(item, this);
        }
    }
}