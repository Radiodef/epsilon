/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.util.*;
import eps.json.*;
import eps.math.*;
import static eps.util.Misc.*;
import static eps.util.Literals.*;

import java.util.*;

@JsonSerializable
public final class BoundingBox {
    @JsonProperty
    public final double north;
    @JsonProperty
    public final double east;
    @JsonProperty
    public final double south;
    @JsonProperty
    public final double west;
    
    private static final String NORTH = "north";
    private static final String EAST  = "east";
    private static final String SOUTH = "south";
    private static final String WEST  = "west";
    
    @JsonConstructor
    public BoundingBox(double north,
                       double east,
                       double south,
                       double west) {
        this.north = Errors.requireReal(NORTH, north);
        this.east  = Errors.requireReal(EAST,  east);
        this.south = Errors.requireReal(SOUTH, south);
        this.west  = Errors.requireReal(WEST,  west);
    }
    
    private BoundingBox(BoundingBox that) {
        this(that.north, that.east, that.south, that.west);
    }
    
    private static final JsonDestructurer DESTRUCTURER = new JsonDestructurer() {
        private final Map<String, String> map =
            TreeMapOf(String.CASE_INSENSITIVE_ORDER,
                      NORTH, NORTH, "n", NORTH,
                      EAST,  EAST,  "e", EAST,
                      SOUTH, SOUTH, "s", SOUTH,
                      WEST,  WEST,  "w", WEST);
        private final Iterable<String> keys = ListOf(NORTH, EAST, SOUTH, WEST);
        @Override
        protected int minimumSize() {
            return 4;
        }
        @Override
        protected int maximumSize() {
            return 4;
        }
        @Override
        protected String mapKey(String key) {
            return map.get(key);
        }
        @Override
        protected Iterable<String> keys() {
            return keys;
        }
    };
    
    public BoundingBox(String text) {
        this(BoundingBox.parse(text));
    }
    
    public static BoundingBox parse(String text) {
        Objects.requireNonNull(text, "text");
        try {
            try {
                return (BoundingBox) Json.objectify(text);
            } catch (IllegalArgumentException x) {
                eps.app.Log.caught(BoundingBox.class, "<init>", x, true);
            }
            
            Map<String, JsonValue> map = DESTRUCTURER.destructure(text);
            
            double north = ((JsonNumber) map.get(NORTH)).doubleValue();
            double east  = ((JsonNumber) map.get(EAST)).doubleValue();
            double south = ((JsonNumber) map.get(SOUTH)).doubleValue();
            double west  = ((JsonNumber) map.get(WEST)).doubleValue();
            
            return new BoundingBox(north, east, south, west);
            
        } catch (ClassCastException x) {
            throw new IllegalArgumentException(text, x);
        }
    }
    
    public double width() {
        return Math.abs(east - west);
    }
    
    public double height() {
        return Math.abs(south - north);
    }
    
    @Override
    public String toString() {
        return f("Box[north = %f, east = %f, south = %f, west = %f]", north, east, south, west);
    }
    
    public String toJsonString() {
        return f("{ \"%s\": %f, \"%s\": %f, \"%s\": %f, \"%s\": %f }",
                 NORTH, north, EAST, east, SOUTH, south, WEST, west);
    }
    
    @Override
    public int hashCode() {
        return Math2.bitHash(north) ^ Math2.bitHash(east) ^ Math2.bitHash(south) ^ Math2.bitHash(west);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BoundingBox) {
            BoundingBox that = (BoundingBox) obj;
            return Math2.bitEquals(this.north, that.north)
                && Math2.bitEquals(this.east,  that.east)
                && Math2.bitEquals(this.south, that.south)
                && Math2.bitEquals(this.west,  that.west);
        }
        return false;
    }
}