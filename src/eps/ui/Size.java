/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.json.*;
import eps.util.*;
import eps.app.*;
import eps.math.*;
import eps.eval.*;
import eps.math.Number;

import java.lang.Float;
import java.util.*;
import java.util.function.*;

@JsonSerializable
public final class Size {
    @JsonProperty
    private final int relativeSign;
    @JsonProperty
    private final float absoluteValue;
    
//    public static final float MIN_SIZE = 1f;
    
    @JsonConstructor
    public Size(int relativeSign, float absoluteValue) {
        if (absoluteValue < 0) {
            throw new IllegalArgumentException("absoluteValue = " + absoluteValue);
        }
        this.relativeSign  = Math2.signum(relativeSign); // relativeSign < 0 ? -1 : relativeSign > 0 ? +1 : 0;
        this.absoluteValue = Math.max(0f, absoluteValue);
        validate();
    }
    
    private Size(float relativeValue, Void $private) {
        this((int) Math.signum(relativeValue), Math.abs(relativeValue));
    }
    
    public Size(float absoluteValue) {
        this(0, absoluteValue);
    }
    
    public Size(String text) {
        Objects.requireNonNull(text, "text");
        text = text.trim();
        
        if (text.startsWith("-")) {
            this.relativeSign = -1;
            text = text.substring(1).trim();
        } else if (text.startsWith("+")) {
            this.relativeSign = +1;
            text = text.substring(1).trim();
        } else {
            this.relativeSign = 0;
        }
        
        MathEnv env = MathEnv.instance(MathEnv64.class);
        Number absoluteValue = NumberParser.DECIMAL.parse(text, env);
        
        this.absoluteValue = (float) ((Float64) absoluteValue.toFloatInexact(env)).doubleValue();
        
        validate();
    }
    
    private void validate() {
        switch (relativeSign) {
            case -1:
            case  0:
            case +1: {
                break;
            }
            default: {
                throw new IllegalArgumentException("relativeSign = " + relativeSign);
            }
        }
        if (Float.isNaN(absoluteValue) || Float.isInfinite(absoluteValue)) {
            throw new IllegalArgumentException("absoluteValue = " + absoluteValue);
        }
    }
    
    public static Size parse(String text) {
        Objects.requireNonNull(text, "text");
        text = text.trim();
        if (text.equals("null")) {
            return null;
        } else {
            return new Size(text);
        }
    }
    
    public boolean isRelative() {
        return relativeSign != 0;
    }
    
    public boolean isAbsolute() {
        return relativeSign == 0;
    }
    
    public int getRelativeSign() {
        return relativeSign;
    }
    
    public float getAbsoluteValue() {
        return absoluteValue;
    }
    
    public float getRelativeValue() {
        return relativeSign * absoluteValue;
    }
    
    public float getAbsoluteSize(DoubleSupplier absSupplier) {
        if (isAbsolute()) {
            return absoluteValue;
        } else {
            return Math.max(0f, (float) absSupplier.getAsDouble() + (absoluteValue * relativeSign));
//            return Math.max(MIN_SIZE, (float) absSupplier.getAsDouble() + (absoluteValue * relativeSign));
        }
    }
    
    public float getAbsoluteSize() {
        if (isAbsolute()) {
            return getAbsoluteValue();
        }
        throw new UnsupportedOperationException(toString());
    }
    
    public Size derive(Size that) {
        if (that.isAbsolute()) {
            return that;
        }
        if (this.isAbsolute()) {
            return new Size(Math.max(0f, this.absoluteValue + that.getRelativeValue()));
        }
        return new Size(this.getRelativeValue() + that.getRelativeValue(), (Void) null);
    }
    
    @Override
    public int hashCode() {
        return (73 * relativeSign) + Float.hashCode(absoluteValue);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Size) {
            Size that = (Size) obj;
            // Note: neither of these can be NaN (see constructor/validate).
            return (this.relativeSign == that.relativeSign) && (this.absoluteValue == that.absoluteValue);
        }
        return false;
    }
    
    private static final ToString<Size> TO_STRING =
        ToString.builder(Size.class)
                .add("relativeSign",  Size::getRelativeSign)
                .add("absoluteValue", Size::getAbsoluteValue)
                .build();
    
    public String toDebugString() {
        return TO_STRING.apply(this);
    }
    
    @Override
    public String toString() {
        String absoluteValue = Decimal.valueOf(this.absoluteValue).setApproximate(false).toString();
        int    dot           = absoluteValue.indexOf(Settings.getOrDefault(Setting.DECIMAL_MARK).intValue());
        int    length        = absoluteValue.length();
        int    decimals      = (dot < 0) ? 0 : (length - dot - 1);
        if (decimals > 3) {
            absoluteValue = absoluteValue.substring(0, dot + 1 + 3);
            while (absoluteValue.endsWith("0")) {
                absoluteValue = absoluteValue.substring(0, absoluteValue.length() - 1);
            }
            if (absoluteValue.length() == dot + 1) {
                absoluteValue = absoluteValue.substring(0, absoluteValue.length() - 1);
            }
            assert !absoluteValue.isEmpty();
        }
        
        if (relativeSign < 0) {
            return "-" + absoluteValue;
        }
        if (relativeSign > 0) {
            return "+" + absoluteValue;
        }
        
        return absoluteValue;
    }
}