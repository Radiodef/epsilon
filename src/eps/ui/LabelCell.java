/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.app.*;
import eps.json.*;
import eps.block.*;

@Deprecated
public interface LabelCell extends GeneralTextCell {
    @Override
    LabelCell followTextStyle(Setting<Style> settingToFollow);
    
    @Override
    default eps.ui.Packet getPacket() {
        return new Packet(this);
    }
    
    @JsonSerializable
    class Packet extends GeneralTextCell.Packet {
        @JsonConstructor
        protected Packet(BoundingBox margins, Setting<Style> toFollow, String text, Block block) {
            super(margins, toFollow, text, block);
        }
        protected Packet(LabelCell cell) {
            super(cell);
        }
        @Override
        public Cell newCell(Item item) {
            return App.app().getComponentFactory().newLabel().configure(item, this);
        }
    }
}