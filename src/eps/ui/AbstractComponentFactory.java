/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.app.*;

import java.util.concurrent.atomic.*;
import java.util.*;

public abstract class AbstractComponentFactory implements ComponentFactory {
    private final AtomicReference<App> app = new AtomicReference<>();
    
    private final Style defaultStyle;
    
    protected AbstractComponentFactory(Style defaultStyle) {
        this.defaultStyle = Objects.requireNonNull(defaultStyle, "defaultStyle");
        do {
            if (!defaultStyle.isFamilySet())
                break;
            if (!defaultStyle.isForegroundSet())
                break;
            if (!defaultStyle.isSizeSet())
                break;
            if (defaultStyle.getSize().isRelative())
                break;
            return;
        } while (false);
        throw new IllegalArgumentException(defaultStyle.toString());
    }
    
    @Override
    public Style getDefaultStyle() {
        return defaultStyle;
    }
    
    @Override
    public void setApp(App app) {
        if (!this.app.compareAndSet(null, app)) {
            throw new IllegalStateException("app already set");
        }
    }
    
    @Override
    public App getApp() {
        App app = this.app.get();
        if (app == null) {
            throw new IllegalStateException("app not set");
        }
        return app;
    }
}