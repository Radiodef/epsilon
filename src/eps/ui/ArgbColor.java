/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.app.*;
import eps.util.*;
import eps.json.*;
import static eps.util.Literals.*;

import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.lang.reflect.*;

public final class ArgbColor implements IntSupplier {
    private final int argb;
    
    public ArgbColor(int argb) {
        this.argb = argb;
    }
    
    public ArgbColor(int a, int r, int g, int b) {
        this.argb = pack(a, r, g, b);
    }
    
    public ArgbColor(int r, int g, int b) {
        this(0xFF, r, g, b);
    }
    
    public static ArgbColor createOpaque(int rgb) {
        return new ArgbColor(0xFF000000 | rgb);
    }
    
    public ArgbColor(String text) {
        ArgbColor color = ArgbColor.parse(text);
        Objects.requireNonNull(color, "parsed color");
        this.argb = color.argb;
    }
    
    private static final Pattern HEX_RGB_PATTERN = Pattern.compile("(0[xX])?([0-9A-Fa-f]{2})?[0-9A-Fa-f]{6}");
    
    // parses any of:
    //  * a case-insensitive name of a constant
    //  * a hex integer (either literal form with 0x or non)
    //  * a JSON of the form which would be produced
    //    after serializing an instance of ArgbColor
    //  * a JSON of the form
    //     { "alpha": 0, "red": 0, "green": 0, "blue": 0 } or
    //     { "a":     0, "r":   0, "g":     0, "b":    0 }
    //    case-insensitively, with alpha optional
    public static ArgbColor parse(String text) {
        Objects.requireNonNull(text, "text");
        text = text.trim();
        
        // name
        ArgbColor fromName = COLORS_BY_NAME.get(text);
        
        if (fromName != null) {
            return fromName;
        }
        
        // hex
        if (HEX_RGB_PATTERN.matcher(text).matches()) {
            if (text.startsWith("0x") || text.startsWith("0X")) {
                text = text.substring(2);
            }
            if (text.length() == 6) {
                text = "FF" + text;
            }
            
            return new ArgbColor(Integer.parseUnsignedInt(text, 16));
        }
        
        // json
        try {
            ArgbColor color = (ArgbColor) Json.objectify(text);
            return color;
        } catch (ClassCastException x) {
            // means the JSON deserialized, but to something other than ArgbColor
            throw new IllegalArgumentException(text, x);
        } catch (IllegalArgumentException x) {
            Log.caught(ArgbColor.class, "<init>", x, true);
        }
        
        // json
        // noinspection TryWithIdenticalCatches
        try {
            Map<String, JsonValue> map = DESTRUCTURER.destructure(text);
            
            JsonValue alpha = map.get("a");
            
            int argb = pack((alpha != null ? ((JsonNumber) alpha).intValue() : 0xFF),
                            ((JsonNumber) map.get("r")).intValue(),
                            ((JsonNumber) map.get("g")).intValue(),
                            ((JsonNumber) map.get("b")).intValue());
            
            return new ArgbColor(argb);
            
        } catch (NullPointerException x) {
            // throwing NPE directly could be fine too...
            throw new IllegalArgumentException(text, x);
        } catch (ClassCastException x) {
            // means the JSON deserialized, but to something unsuitable
            throw new IllegalArgumentException(text, x);
        } catch (IllegalArgumentException x) {
            throw new IllegalArgumentException(text, x);
        }
    }
    
    @Override
    public int getAsInt() {
        return argb;
    }
    
    public int getAlpha() {
        return (argb >>> 24);
    }
    
    public int getRed() {
        return (argb >>> 16) & 0xFF;
    }
    
    public int getGreen() {
        return (argb >>> 8) & 0xFF;
    }
    
    public int getBlue() {
        return argb & 0xFF;
    }
    
    public ArgbColor getOpposite() {
        return new ArgbColor(getAlpha(), 255 - getRed(), 255 - getGreen(), 255 - getBlue());
    }
    
    public ArgbColor getAverage(ArgbColor that) {
        int a = (this.getAlpha() + that.getAlpha()) >> 1;
        int r = (this.getRed()   + that.getRed()  ) >> 1;
        int g = (this.getGreen() + that.getGreen()) >> 1;
        int b = (this.getBlue()  + that.getBlue() ) >> 1;
        return new ArgbColor(a, r, g, b);
    }
    
    public String toHexRgbString() {
        return this.toHexString(false);
    }
    
    public String toHexArgbString() {
        return this.toHexString(true);
    }
    
    public String toHexString(boolean alpha) {
        char[] chars = new char[alpha ? 8 : 6];
        
        int argb = this.argb;
        
        int i = 0;
        if (alpha) {
            chars[i++] = Misc.toHexChar(argb >>> 28);
            chars[i++] = Misc.toHexChar(argb >>> 24);
        }
        chars[i++] = Misc.toHexChar(argb >>> 20);
        chars[i++] = Misc.toHexChar(argb >>> 16);
        chars[i++] = Misc.toHexChar(argb >>> 12);
        chars[i++] = Misc.toHexChar(argb >>>  8);
        chars[i++] = Misc.toHexChar(argb >>>  4);
        chars[i  ] = Misc.toHexChar(argb >>>  0);
        
        return new String(chars);
    }
    
    @Override
    public int hashCode() {
        return argb;
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof ArgbColor) && ((ArgbColor) obj).argb == this.argb;
    }
    
    @Override
    public String toString() {
        return String.format("%s[A=%s, R=%s, G=%s, B=%s]",
                             getClass().getSimpleName(),
                             Misc.toHexLiteral(getAlpha()),
                             Misc.toHexLiteral(getRed()),
                             Misc.toHexLiteral(getGreen()),
                             Misc.toHexLiteral(getBlue()));
    }
    
    public String toJsonString() {
        return String.format("{ \"alpha\": %d, \"red\": %d, \"green\": %d, \"blue\": %d }",
                             getAlpha(),
                             getRed(),
                             getGreen(),
                             getBlue());
    }
    
    public static int pack(int a, int r, int g, int b) {
        return (a << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | (b & 0xFF);
    }
    
    private static final JsonDestructurer DESTRUCTURER = new JsonDestructurer() {
        private final TreeMap<String, String> map =
            TreeMapOf(String.CASE_INSENSITIVE_ORDER,
                      "alpha", "a", "a", "a",
                      "red",   "r", "r", "r",
                      "green", "g", "g", "g",
                      "blue",  "b", "b", "b");
        private final List<String> keys = ListOf("a", "r", "g", "b");
        @Override
        protected int minimumSize() {
            return 3;
        }
        @Override
        protected int maximumSize() {
            return 4;
        }
        @Override
        protected String mapKey(String key) {
            return map.get(key);
        }
        @Override
        protected boolean isOptional(String key) {
            return "a".equals(map.get(key));
        }
        @Override
        protected Iterable<String> keys() {
            return keys;
        }
    };
    
    public static final ArgbColor WHITE          = createOpaque(0xFFFFFF);
    public static final ArgbColor LIGHTEST_GRAY  = createOpaque(0xE0E0E0);
    public static final ArgbColor LIGHT_GRAY     = createOpaque(0xC0C0C0);
    public static final ArgbColor MED_LIGHT_GRAY = createOpaque(0xA0A0A0);
    public static final ArgbColor GRAY           = createOpaque(0x808080);
    public static final ArgbColor MED_DARK_GRAY  = createOpaque(0x606060);
    public static final ArgbColor DARK_GRAY      = createOpaque(0x404040);
    public static final ArgbColor DARKEST_GRAY   = createOpaque(0x202020);
    public static final ArgbColor BLACK          = createOpaque(0x00000);
    public static final ArgbColor MAROON         = createOpaque(0x800000);
    public static final ArgbColor RED            = createOpaque(0xFF0000);
    public static final ArgbColor MELON          = createOpaque(0xFDBCB4);
    public static final ArgbColor ORANGE         = createOpaque(0xFF8000);
    public static final ArgbColor TAN            = createOpaque(0xFFD699);
    public static final ArgbColor CREAM          = createOpaque(0xFFFFCC);
    public static final ArgbColor YELLOW         = createOpaque(0xFFFF00);
    public static final ArgbColor LIME           = createOpaque(0x00FF00);
    public static final ArgbColor CELADON        = createOpaque(0xACE1AF);
    public static final ArgbColor GREEN          = createOpaque(0x008000);
    public static final ArgbColor SKY_BLUE       = createOpaque(0x87CEFA);
    public static final ArgbColor DUSTY_BLUE     = createOpaque(0xE7F5FD); // was 0xE6FFFF
    public static final ArgbColor PALE_BLUE      = createOpaque(0xCCFFFF); // was 0xC4E8ED
    public static final ArgbColor AQUA           = createOpaque(0x00FFFF);
    public static final ArgbColor TEAL           = createOpaque(0x008080);
    public static final ArgbColor BLUE           = createOpaque(0x0000FF);
    public static final ArgbColor NAVY           = createOpaque(0x000080);
    public static final ArgbColor DARK_NAVY      = createOpaque(0x000048);
    public static final ArgbColor PURPLE         = createOpaque(0x800080);
    public static final ArgbColor ORCHID         = createOpaque(0xDA70D6);
    public static final ArgbColor PALE_ORCHID    = createOpaque(0xEAAEE8);
    public static final ArgbColor FUSCHIA        = createOpaque(0xFF00FF);
    public static final ArgbColor MAGENTA        = createOpaque(0xF653A6);
    
    private static final Map<String, ArgbColor> COLORS_BY_NAME;
    static {
        Map<String, ArgbColor> colorsByName = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        
        for (Field field : ArgbColor.class.getFields()) {
            if (field.getType() != ArgbColor.class)
                continue;
            if (field.getModifiers() != Misc.PUBLIC_STATIC_FINAL)
                continue;
            try {
                ArgbColor color = (ArgbColor) field.get(null);
                String    name  = field.getName();
                colorsByName.put(name,                  color);
                colorsByName.put(name.replace("_", ""), color);
            } catch (ReflectiveOperationException x) {
                eps.app.Log.caught(ArgbColor.class, "<clinit>", x, false);
            }
        }
        
        COLORS_BY_NAME = colorsByName;
    }
    
    static {
        ToStringConverter<ArgbColor> converter =
            ToStringConverter.create(
                ArgbColor.class,
                color -> Misc.toHexLiteral(color.argb),
                ArgbColor::new);
        ToStringConverter.put(converter);
    }
}