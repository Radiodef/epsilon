/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import java.util.*;
import java.util.concurrent.*;

public interface MainWindow extends Window {
    boolean isAlwaysOnTop();
    void setAlwaysOnTop(boolean shouldBeAlwaysOnTop);
    
    String getTitleText();
    void setTitleText(String titleText);
    
    void addEventCallback(EventCallback callback);
    void removeEventCallback(EventCallback callback);
    
    String getCommandText();
    void setCommandText(String commandText);
    
    int indexOfCell(Cell cell);
    void addCell(Cell cell);
    void insertCell(int cellIndex, Cell cell);
    void removeCell(Cell cell);
    
    boolean isMaximized();
    boolean isWindowed();
    void setMaximized(boolean isMaximized);
    
    default boolean isFullScreen() {
        return false;
    }
    
    default void setFullScreen(boolean isFullScreen) {
    }
    
    interface EventCallback {
        default void historyPrevious(MainWindow source) {
        }
        default void historyNext(MainWindow source) {
        }
        default void commandSent(MainWindow source, String commandText) {
        }
        default void windowFirstShown(MainWindow source) {
        }
        default void windowBoundsChanged(MainWindow source, BoundingBox bounds) {
        }
        default void windowToMaximized(MainWindow source) {
            windowMaximizedChanged(source, true);
            windowFullScreenChanged(source, false);
        }
        default void windowToWindowed(MainWindow source) {
            windowMaximizedChanged(source, false);
            windowFullScreenChanged(source, false);
        }
        default void windowMaximizedChanged(MainWindow source, boolean isMaximized) {
        }
        default void windowFullScreenChanged(MainWindow source, boolean isFullScreen) {
        }
    }
    
    final class EventCallbacks implements EventCallback {
        private final List<EventCallback> list = new CopyOnWriteArrayList<>();
        
        public EventCallbacks() {
        }
        
        public void add(EventCallback c) {
            list.add(Objects.requireNonNull(c, "c"));
        }
        public void remove(EventCallback c) {
            list.remove(Objects.requireNonNull(c, "c"));
        }
        public List<EventCallback> listView() {
            return Collections.unmodifiableList(list);
        }
        
        @Override
        public void historyPrevious(MainWindow source) {
            for (EventCallback c : list)
                c.historyPrevious(source);
        }
        @Override
        public void historyNext(MainWindow source) {
            for (EventCallback c : list)
                c.historyNext(source);
        }
        @Override
        public void commandSent(MainWindow source, String commandText) {
            for (EventCallback c : list)
                c.commandSent(source, commandText);
        }
        @Override
        public void windowFirstShown(MainWindow source) {
            for (EventCallback c : list)
                c.windowFirstShown(source);
        }
        @Override
        public void windowBoundsChanged(MainWindow source, BoundingBox bounds) {
            for (EventCallback c : list)
                c.windowBoundsChanged(source, bounds);
        }
        @Override
        public void windowToMaximized(MainWindow source) {
            for (EventCallback c : list)
                c.windowToMaximized(source);
        }
        @Override
        public void windowToWindowed(MainWindow source) {
            for (EventCallback c : list)
                c.windowToWindowed(source);
        }
        @Override
        public void windowMaximizedChanged(MainWindow source, boolean isMaximized) {
            for (EventCallback c : list)
                c.windowMaximizedChanged(source, isMaximized);
        }
        @Override
        public void windowFullScreenChanged(MainWindow source, boolean isFullScreen) {
            for (EventCallback c : list)
                c.windowFullScreenChanged(source, isFullScreen);
        }
    }
}