/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.app.*;

import java.util.*;
import java.util.function.*;

public interface Cell extends AutoCloseable {
    @Override
    void close();
    
    Packet getPacket();
    
    default Cell configure(Item item, Packet packet) {
        this.setMargins(packet.getMargins());
        return this;
    }
    
    BoundingBox getMargins();
    void setMargins(BoundingBox margins);
    
    default void addDeleteListener(Runnable listener) {
        this.addDeleteListener(cell -> listener.run());
    }
    
    void addDeleteListener(Consumer<? super Cell> listener);
    void removeDeleteListener(Consumer<? super Cell> listener);
    
    void collectLines(Consumer<? super String> lineConsumer);
    
    default List<String> getLines() {
        List<String> list = new ArrayList<>();
        this.collectLines(list::add);
        return list;
    }
}