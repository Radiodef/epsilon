/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.app.*;
import eps.block.*;
import eps.json.*;

import java.util.function.*;

public interface GeneralTextCell extends Cell {
    String getText();
    void setText(String text);
    
    default Block getBlock() {
        return null;
    }
    
    default void setText(Block block) {
        this.setText(block.toString());
    }
    
    default void setText(CharSequence text) {
        this.setText(text == null ? "" : text.toString());
    }
    
    @Override
    default void collectLines(Consumer<? super String> action) {
        String text;
        Block block = this.getBlock();
        if (block != null) {
            text = block.toString();
        } else {
            text = this.getText();
        }
        if (text != null) {
            String[] lines = text.split("\\r\\n|\\n");
            for (String ln : lines)
                action.accept(ln);
        }
    }
    
    default void formatText(String format, Object... args) {
        this.setText(String.format(format, args));
    }
    
    ArgbColor getTextColor();
    void setTextColor(ArgbColor color);
    
    // use followTextStyle instead
//    void setTextStyle(Style style);
    
    void setTextStyle(Setting<Style> setting);
    
    Setting<Style> getFollowedStyle();
    GeneralTextCell followTextStyle(Setting<Style> settingToFollow);
    
    @Override
    default GeneralTextCell configure(Item item, eps.ui.Packet packet0) {
        Packet packet = (Packet) packet0;
        Cell.super.configure(item, packet);
        
        this.followTextStyle(packet.toFollow);
        if (packet.block != null) {
            this.setText(packet.block);
        } else {
            this.setText(packet.text);
        }
        
        return this;
    }
    
    @JsonSerializable
    abstract class Packet extends eps.ui.Packet {
        @JsonProperty
        private final Setting<Style> toFollow;
        @JsonProperty
        private final String text;
        @JsonProperty
        private final Block block;
        
        @JsonConstructor
        protected Packet(BoundingBox margins, Setting<Style> toFollow, String text, Block block) {
            super(margins);
            this.toFollow = toFollow;
            this.text     = text;
            this.block    = block;
        }
        
        protected Packet(GeneralTextCell cell) {
            super(cell);
            this.toFollow = cell.getFollowedStyle();
            
            Block block = cell.getBlock();
            if (block == null) {
                this.block = null;
                this.text  = cell.getText();
            } else {
                this.block = block;
                this.text  = null;
            }
        }
        
        protected Setting<Style> getFollowedStyle() {
            return toFollow;
        }
        
        protected String getText() {
            return text;
        }
        
        protected Block getBlock() {
            return block;
        }
    }
}