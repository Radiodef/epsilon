/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.util.*;
import eps.app.*;
import eps.json.*;
import static eps.app.App.*;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;
import java.util.function.*;
import java.util.concurrent.*;
import static java.util.Objects.*;

// TODO: style for operators?
//       separate style for numbers/variables?
@JsonSerializable
public final class Theme {
    @JsonProperty
    private final String name;
    @JsonDefault("name")
    private static String defaultName() { return "Untitled Theme"; }
    @JsonProperty
    private final ArgbColor backgroundColor;
    @JsonProperty
    private final ArgbColor caretColor;
    @JsonDefault("caretColor")
    private static ArgbColor defaultCaretColor() { return null; }
    @JsonProperty
    private final Style basicStyle;
    @JsonProperty
    private final Style noteStyle;
    @JsonProperty
    private final Style commandStyle;
    @JsonProperty
    private final Style numberStyle;
    @JsonProperty
    private final Style literalStyle;
    @JsonProperty
    private final Style functionStyle;
    @JsonProperty
    private final Style bracketStyle;
    @JsonProperty
    private final Style buttonStyle;
    @JsonDefault("buttonStyle")
    private static Style defaultButtonStyle() { return Style.EMPTY; }
    @JsonProperty
    private final Style buttonSelectedStyle;
    @JsonDefault("buttonSelectedStyle")
    private static Style defaultButtonSelectedStyle() { return Style.EMPTY; }
    @JsonProperty
    private final Style unexpectedErrorStyle;
    
    private void validate() {
        if (basicStyle.isSizeSet() && basicStyle.getSize().isRelative()) {
            throw new IllegalArgumentException(basicStyle.getSize().toString());
        }
    }
    
    @JsonConstructor
    public Theme(String    name,
                 ArgbColor backgroundColor,
                 ArgbColor caretColor,
                 Style     basicStyle,
                 Style     noteStyle,
                 Style     commandStyle,
                 Style     numberStyle,
                 Style     literalStyle,
                 Style     functionStyle,
                 Style     bracketStyle,
                 Style     buttonStyle,
                 Style     buttonSelectedStyle,
                 Style     unexpectedErrorStyle) {
        this.name                 = requireNonNull(name,                 "name");
        this.backgroundColor      = requireNonNull(backgroundColor,      "backgroundColor");
        this.caretColor           = caretColor;
        this.basicStyle           = requireNonNull(basicStyle,           "basicStyle");
        this.noteStyle            = requireNonNull(noteStyle,            "noteStyle");
        this.commandStyle         = requireNonNull(commandStyle,         "commandStyle");
        this.numberStyle          = requireNonNull(numberStyle,          "numberStyle");
        this.literalStyle         = requireNonNull(literalStyle,         "literalStyle");
        this.functionStyle        = requireNonNull(functionStyle,        "functionStyle");
        this.bracketStyle         = requireNonNull(bracketStyle,         "bracketStyle");
        this.buttonStyle          = requireNonNull(buttonStyle,          "buttonStyle");
        this.buttonSelectedStyle  = requireNonNull(buttonSelectedStyle,  "buttonSelectedStyle");
        this.unexpectedErrorStyle = requireNonNull(unexpectedErrorStyle, "unexpectedErrorStyle");
        
        validate();
    }
    
    public Theme(Settings sets) {
        requireNonNull(sets, "sets");
        this.name                 = requireSet(sets, Setting.THEME_NAME);
        this.backgroundColor      = requireSet(sets, Setting.BACKGROUND_COLOR);
        this.caretColor           = sets.get(Setting.CARET_COLOR);
        this.basicStyle           = requireSet(sets, Setting.BASIC_STYLE);
        this.noteStyle            = requireSet(sets, Setting.NOTE_STYLE);
        this.commandStyle         = requireSet(sets, Setting.COMMAND_STYLE);
        this.numberStyle          = requireSet(sets, Setting.NUMBER_STYLE);
        this.literalStyle         = requireSet(sets, Setting.LITERAL_STYLE);
        this.functionStyle        = requireSet(sets, Setting.FUNCTION_STYLE);
        this.bracketStyle         = requireSet(sets, Setting.BRACKET_STYLE);
        this.buttonStyle          = requireSet(sets, Setting.BUTTON_STYLE);
        this.buttonSelectedStyle  = requireSet(sets, Setting.BUTTON_SELECTED_STYLE);
        this.unexpectedErrorStyle = requireSet(sets, Setting.UNEXPECTED_ERROR_STYLE);
        
        validate();
    }
    
    private static <T> T requireSet(Settings settings, Setting<T> key) {
        T val = settings.get(key);
        if (val == null && !settings.contains(key)) {
            throw new IllegalArgumentException("no setting for " + key);
        }
        return val;
    }
    
    public String    getName()                 { return this.name;                 }
    public ArgbColor getBackgroundColor()      { return this.backgroundColor;      }
    public ArgbColor getCaretColor()           { return this.caretColor;           }
    public Style     getBasicStyle()           { return this.basicStyle;           }
    public Style     getNoteStyle()            { return this.noteStyle;            }
    public Style     getCommandStyle()         { return this.commandStyle;         }
    public Style     getNumberStyle()          { return this.numberStyle;          }
    public Style     getLiteralStyle()         { return this.literalStyle;         }
    public Style     getFunctionStyle()        { return this.functionStyle;        }
    public Style     getBracketStyle()         { return this.bracketStyle;         }
    public Style     getButtonStyle()          { return this.buttonStyle;          }
    public Style     getButtonSelectedStyle()  { return this.buttonSelectedStyle;  }
    public Style     getUnexpectedErrorStyle() { return this.unexpectedErrorStyle; }
    
    public ArgbColor getAbsoluteCaretColor() {
        ArgbColor caretColor = this.caretColor;
        if (caretColor == null) {
            return getComputedCaretColor();
        } else {
            return caretColor;
        }
    }
    
    public ArgbColor getComputedCaretColor() {
        return getBackgroundColor().getOpposite();
    }
    
    public Style getStyle(Setting<Style> set) {
        return Key.valueOf(set).apply(this);
    }
    
    public Style getStyle(Key key) {
        return Objects.requireNonNull(key, "key").apply(this);
    }
    
    private static volatile Supplier<Style> DEFAULT_STYLE_SUPPLIER =
        () -> app().getComponentFactory().getDefaultStyle();
    
    static void setDefaultStyleSupplier(Supplier<Style> supplier) {
        Objects.requireNonNull(supplier, "supplier");
        DEFAULT_STYLE_SUPPLIER = supplier;
    }
    
    static Style getDefaultStyle() {
        return DEFAULT_STYLE_SUPPLIER.get();
    }
    
    public Style getAbsoluteStyle(Key key) {
        Style style;
        
        Key parent = key.getParent();
        if (parent == null) {
            style = getDefaultStyle();
        } else {
            style = getAbsoluteStyle(parent);
        }
        
        return style.derive(getStyle(key));
    }
    
    /*
    public static Style getAbsoluteStyle(Setting<Style> set) {
        Style style;
        
        Key parent = Key.valueOf(set).getParent();
        if (parent == null) {
            style = getDefaultStyle();
        } else {
            style = getAbsoluteStyle(parent.getSetting());
//            style = sets.getSetting(parent.getSetting());
//            style = app().getSetting(parent.getSetting());
        }
        
        return style.derive(MiniSettings.instance().getSetting(set));
//        return style.derive(app.getSetting(set));
    }
    */
    
    public static Style getAbsoluteStyle(Setting<Style> set) {
        return getAbsoluteStyle(MiniSettings.instance(), set);
    }
    
    private static Style getAbsoluteStyle(MiniSettings sets, Setting<Style> set) {
        Style style;
        
        Key parent = Key.valueOf(set).getParent();
        if (parent == null) {
            style = getDefaultStyle();
        } else {
            style = getAbsoluteStyle(sets, parent.getSetting());
        }
        
        return style.derive(sets.getSetting(set));
    }
    
    public Theme derive(Key key, Style style) {
        return new FreeBuilder(this).setStyle(key, style).build();
    }
    
    public Theme rename(String name) {
        return Theme.builder()
                    .setName(name)
                    .setBackgroundColor(backgroundColor)
                    .setCaretColor(caretColor)
                    .setBasicStyle(basicStyle)
                    .setNoteStyle(noteStyle)
                    .setCommandStyle(commandStyle)
                    .setNumberStyle(numberStyle)
                    .setLiteralStyle(literalStyle)
                    .setFunctionStyle(functionStyle)
                    .setBracketStyle(bracketStyle)
                    .setButtonStyle(buttonStyle)
                    .setButtonSelectedStyle(buttonSelectedStyle)
                    .setUnexpectedErrorStyle(unexpectedErrorStyle)
                    .build();
    }
    
    public void setAll(Settings settings) {
        setAll(settings, false);
    }
    
    public void setWithoutSize(Settings settings) {
        setAll(settings, true);
    }
    
    public void setAll(Settings settings, boolean withoutSize) {
        settings.doSynchronized(s -> {
            s.set(Setting.THEME_NAME,       this.name);
            s.set(Setting.BACKGROUND_COLOR, this.backgroundColor);
            s.set(Setting.CARET_COLOR,      this.getAbsoluteCaretColor());
            
            for (Key key : Key.VALUES) {
                if (withoutSize) {
                    setws(s, key.getSetting(), key.apply(this));
                } else {
                    s.set(key.getSetting(), key.apply(this));
                }
            }
        });
    }
    
    private static Settings setws(Settings s, Setting<Style> key, Style style) {
        Style prev = s.get(key);
        
        if (prev != null) {
            if (prev.isSizeSet()) {
                style = style.setSize(prev.getSize());
            }
        }
        
        s.set(key, style);
        return s;
    }
    
    public boolean isPreset() {
        return isPreset(this);
    }
    
    @Override
    public int hashCode() {
        int hash = name.hashCode();
        hash = 31 * hash + backgroundColor.hashCode();
        hash = 31 * hash + Objects.hashCode(caretColor);
        for (Key key : Key.VALUES) {
            hash = 31 * hash + key.apply(this).hashCode();
        }
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof Theme) {
            Theme that = (Theme) obj;
            if (!this.name.equals(that.name))
                return false;
            if (!this.backgroundColor.equals(that.backgroundColor))
                return false;
            if (!Objects.equals(this.caretColor, that.caretColor))
                return false;
            for (Key key : Key.VALUES)
                if (!key.apply(this).equals(key.apply(that)))
                    return false;
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean multiline) {
        return TO_STRING.apply(this, multiline);
    }
    
    public enum Key implements Function<Theme, Style> {
        BASIC          ( Theme::getBasicStyle,           "BASIC_STYLE"            ),
        NOTE           ( Theme::getNoteStyle,            "NOTE_STYLE"             ),
        COMMAND        ( Theme::getCommandStyle,         "COMMAND_STYLE"          ),
        NUMBER         ( Theme::getNumberStyle,          "NUMBER_STYLE"           ),
        LITERAL        ( Theme::getLiteralStyle,         "LITERAL_STYLE"          ),
        FUNCTION       ( Theme::getFunctionStyle,        "FUNCTION_STYLE"         ),
        BRACKET        ( Theme::getBracketStyle,         "BRACKET_STYLE"          ),
        BUTTON         ( Theme::getButtonStyle,          "BUTTON_STYLE"           ),
        BUTTON_SELECTED( Theme::getButtonSelectedStyle,  "BUTTON_SELECTED_STYLE"  ),
        INTERNAL_ERROR ( Theme::getUnexpectedErrorStyle, "UNEXPECTED_ERROR_STYLE" );
        
        private final Function<Theme, Style> func;
        private final String setName;
        
        // Necessary because of class initialization order.
        private volatile Setting<Style> set;
        
        private final String lower;
        
        Key(Function<Theme, Style> func, String setName) {
            this.func    = Objects.requireNonNull(func,     "func");
            this.setName = Objects.requireNonNull(setName,  "setName");
            
            this.lower =
                Arrays.stream(name().split("_"))
                      .map(s -> s.charAt(0) + s.substring(1).toLowerCase(Locale.ROOT))
                      .collect(Collectors.joining());
        }
        
        @Override
        public Style apply(Theme theme) {
            return func.apply(theme);
        }
        
        public Setting<Style> getSetting() {
            Setting<Style> set = this.set;
            
            if (set == null) {
                this.set = set = Setting.valueOf(setName).as(Style.class);
            }
            
            return set;
        }
        
        public Key getParent() {
            switch (this) {
                case BASIC:
                    return null;
                case BUTTON_SELECTED:
                    return BUTTON;
                default:
                    return BASIC;
            }
        }
        
        public String getLowerCaseName() {
            return lower;
        }
        
        public String getDescription() {
            return getSetting().getDescription();
        }
        
        public static final List<Key> VALUES =
            Collections.unmodifiableList((List<Key>) Stream.of(values()).collect(Collectors.toCollection(ArrayList::new)));
        
        // created lazily also because of initialization order
        private static volatile Map<Setting<Style>, Key> KEYS_BY_SET;
        
        public static Key valueOf(Setting<Style> set) {
            Map<Setting<Style>, Key> keysBySet = KEYS_BY_SET;
            
            if (keysBySet == null) {
                keysBySet = new HashMap<>(2 * VALUES.size());
                
                for (Key key : VALUES) {
                    keysBySet.put(key.getSetting(), key);
                }
                
                KEYS_BY_SET = keysBySet;
            }
            
            Key key = keysBySet.get(set);
            if (key != null) {
                return key;
            }
            
            Objects.requireNonNull(set, "set");
            throw new IllegalArgumentException(set.toString());
        }
    }
    
    public static final Theme BRIGHT =
        Theme.builder()
             .setName                ( "Bright" )
             .setBackgroundColor     ( ArgbColor.WHITE )
             .setCaretColor          ( null )
             .setBasicStyle          ( new Style(ArgbColor.BLACK,          null, null) )
             .setNoteStyle           ( new Style(ArgbColor.DARK_GRAY,      null, true).setSize(-1, 4f) )
             .setCommandStyle        ( new Style((ArgbColor) null,         null, true) )
             .setNumberStyle         ( new Style(ArgbColor.ORANGE,         true, true) )
             .setLiteralStyle        ( new Style(ArgbColor.MAROON,         null, true) )
             .setFunctionStyle       ( new Style(ArgbColor.MAGENTA,        true, null) )
             .setBracketStyle        ( new Style(ArgbColor.MED_LIGHT_GRAY, null, null) )
             .setButtonStyle         ( new Style((ArgbColor) null,         null, null).setSize(10f) )
             .setButtonSelectedStyle ( new Style(ArgbColor.LIGHT_GRAY,     null, null) )
             .setUnexpectedErrorStyle( new Style(ArgbColor.RED,            true, null) )
             .build();
    public static final Theme AURORA =
        Theme.builder()
             .setName                ( "Aurora" )
             .setBackgroundColor     ( ArgbColor.BLACK )
             .setCaretColor          ( ArgbColor.LIGHTEST_GRAY )
             .setBasicStyle          ( new Style(ArgbColor.WHITE,         null, null) )
             .setNoteStyle           ( new Style(ArgbColor.LIGHT_GRAY,    null, true).setSize(-1, 4f) )
             .setCommandStyle        ( new Style((ArgbColor) null,        null, true) )
             .setNumberStyle         ( new Style(ArgbColor.CELADON,       true, true) )
             .setLiteralStyle        ( new Style(ArgbColor.PALE_ORCHID,   null, true) )
             .setFunctionStyle       ( new Style(ArgbColor.PALE_BLUE,     true, null) )
             .setBracketStyle        ( new Style(ArgbColor.MED_DARK_GRAY, null, null) )
             .setButtonStyle         ( new Style((ArgbColor) null,        null, null).setSize(10f) )
             .setButtonSelectedStyle ( new Style(ArgbColor.CELADON,       null, null) )
             .setUnexpectedErrorStyle( new Style(ArgbColor.MAGENTA,       true, null) )
             .build();
    public static final Theme HELL =
        Theme.builder()
             .setName                ( "Hell" )
             .setBackgroundColor     ( ArgbColor.createOpaque(0xC0) )
             .setCaretColor          ( ArgbColor.LIGHTEST_GRAY )
             .setBasicStyle          ( new Style(ArgbColor.WHITE,  null, null) )
             .setNoteStyle           ( new Style((ArgbColor) null, null, true).setSize(-1, 4f) )
             .setCommandStyle        ( new Style((ArgbColor) null, null, true) )
             .setNumberStyle         ( new Style((ArgbColor) null, true, true) )
             .setLiteralStyle        ( new Style((ArgbColor) null, null, true) )
             .setFunctionStyle       ( new Style((ArgbColor) null, true, null) )
             .setBracketStyle        ( new Style((ArgbColor) null, null, null) )
             .setButtonStyle         ( new Style((ArgbColor) null, null, null).setSize(10f) )
             .setButtonSelectedStyle ( new Style(ArgbColor.LIGHT_GRAY)         )
             .setUnexpectedErrorStyle( new Style((ArgbColor) null, true, null) )
             .build();
    public static final Theme ICEBERG =
        Theme.builder()
             .setName                ( "Iceberg" )
             .setBackgroundColor     ( ArgbColor.DARK_NAVY )
             .setCaretColor          ( ArgbColor.WHITE )
             .setBasicStyle          ( new Style(ArgbColor.WHITE,         null, null) )
             .setNoteStyle           ( new Style(ArgbColor.LIGHTEST_GRAY, null, true).setSize(-1, 4f) )
             .setCommandStyle        ( new Style((ArgbColor) null,        null, true) )
             .setNumberStyle         ( new Style(ArgbColor.DUSTY_BLUE,    true, true) )
             .setLiteralStyle        ( new Style(ArgbColor.DUSTY_BLUE,    null, true) )
             .setFunctionStyle       ( new Style((ArgbColor) null,        true, null) )
             .setBracketStyle        ( new Style(ArgbColor.SKY_BLUE,      null, null) )
             .setButtonStyle         ( new Style((ArgbColor) null,        null, null).setSize(10f) )
             .setButtonSelectedStyle ( new Style(ArgbColor.SKY_BLUE,      null, null) )
             .setUnexpectedErrorStyle( new Style(ArgbColor.PALE_ORCHID,   true, null) )
             .build();
    public static final Theme GALAXY =
        Theme.builder()
             .setName                ( "Galaxy" )
             .setBackgroundColor     ( ArgbColor.BLACK )
             .setCaretColor          ( ArgbColor.LIGHTEST_GRAY )
             .setBasicStyle          ( new Style(ArgbColor.WHITE,           null, null) )
             .setNoteStyle           ( new Style(ArgbColor.LIGHT_GRAY,      null, true).setSize(-1, 4f) )
             .setCommandStyle        ( new Style((ArgbColor) null,          null, true) )
             .setNumberStyle         ( new Style(new ArgbColor(0xFFC7E6F9), true, true) )
             .setLiteralStyle        ( new Style(new ArgbColor(0xFFCCCCFF), null, true) )
             .setFunctionStyle       ( new Style((ArgbColor) null,          true, null) )
             .setBracketStyle        ( new Style(ArgbColor.TAN,             null, null) )
             .setButtonStyle         ( new Style((ArgbColor) null,          null, null).setSize(10f) )
             .setButtonSelectedStyle ( new Style(ArgbColor.TAN,             null, null) )
             .setUnexpectedErrorStyle( new Style(new ArgbColor(0xFFFF7F7F), true, null) )
             .build();
    // ADD NEW THEMES HERE.
    
    public static final Theme DEFAULT = Theme.GALAXY;
    
    public static Set<String> names() {
        return new TreeSet<>(THEMES_BY_NAME.keySet());
    }
    
    private static final Set<Theme> PRESETS;
    
    private static final Map<String, Theme> THEMES_BY_NAME = new ConcurrentHashMap<>();
    
    static {
        Set<Theme> presets = new LinkedHashSet<>();
        
        for (Field field : Theme.class.getFields()) {
            if (field.getModifiers() != Misc.PUBLIC_STATIC_FINAL)
                continue;
            if (field.getType() != Theme.class)
                continue;
            
            try {
                Theme theme = (Theme) field.get(null);
                
                THEMES_BY_NAME.put(theme.getName(), theme);
                presets.add(theme);
                
            } catch (ReflectiveOperationException x) {
                Log.caught(Theme.class, "<clinit>", x, false);
            }
        }
        
        PRESETS = Collections.unmodifiableSet(presets);
    }
    
    public static Set<Theme> presets() {
        return PRESETS;
    }
    
    public static boolean isPreset(Theme theme) {
        Objects.requireNonNull(theme, "theme");
        return isPreset(theme.getName());
    }
    
    public static boolean isPreset(String name) {
        Objects.requireNonNull(name, "name");
        for (Theme preset : PRESETS) {
            if (preset.getName().equals(name))
                return true;
        }
        return false;
    }
    
    public static boolean put(Theme theme) {
        Objects.requireNonNull(theme, "theme");
        if (theme.isPreset()) {
            return false;
        } else {
            THEMES_BY_NAME.put(theme.getName(), theme);
            return true;
        }
    }
    
    public static boolean remove(Theme theme) {
        Objects.requireNonNull(theme, "theme");
        return remove(theme.getName());
    }
    
    public static boolean remove(String name) {
        Objects.requireNonNull(name, "name");
        if (isPreset(name)) {
            return false;
        } else {
            return THEMES_BY_NAME.remove(name) != null;
        }
    }
    
    public static void setAll(Iterable<? extends Theme> themes) {
        THEMES_BY_NAME.clear();
        for (Theme pre : PRESETS) {
            THEMES_BY_NAME.put(pre.getName(), pre);
        }
        for (Theme theme : themes) {
            if (!theme.isPreset()) {
                THEMES_BY_NAME.put(theme.getName(), theme);
            }
        }
    }
    
    public static Theme valueOf(String name) {
        Objects.requireNonNull(name, "name");
        
        Theme theme = THEMES_BY_NAME.get(name);
        
        if (theme != null) {
            return theme;
        }
        
        throw new IllegalArgumentException(name);
    }
    
    public static Theme valueOfIgnoreCase(String name) {
        Objects.requireNonNull(name, "name");
        
        Theme theme = THEMES_BY_NAME.get(name);
        
        if (theme != null) {
            return theme;
        }
        
        for (Theme val : THEMES_BY_NAME.values()) {
            if (val.getName().equalsIgnoreCase(name))
                return val;
        }
        
        throw new IllegalArgumentException(name);
    }
    
    public static List<Theme> values() {
        List<Theme> values = new ArrayList<>(THEMES_BY_NAME.values());
//        values.sort(Comparator.comparing(Theme::getName));
        return values;
    }
    
    public static NameStep builder() {
        return new Theme.BuilderImpl();
    }
    
    public interface NameStep {
        BackgroundColorStep setName(String name);
    }
    public interface BackgroundColorStep {
        CaretColorStep setBackgroundColor(ArgbColor backgroundColor);
    }
    public interface CaretColorStep {
        BasicStyleStep setCaretColor(ArgbColor caretColor);
    }
    public interface BasicStyleStep {
        NoteStyleStep setBasicStyle(Style noteStyle);
    }
    public interface NoteStyleStep {
        CommandStyleStep setNoteStyle(Style numberStyle);
    }
    public interface CommandStyleStep {
        NumberStyleStep setCommandStyle(Style commandStyle);
    }
    public interface NumberStyleStep {
        LiteralStyleStep setNumberStyle(Style functionStyle);
    }
    public interface LiteralStyleStep {
        FunctionStyleStep setLiteralStyle(Style literalStyle);
    }
    public interface FunctionStyleStep {
        BracketStyleStep setFunctionStyle(Style functionStyle);
    }
    public interface BracketStyleStep {
        ButtonStyleStep setBracketStyle(Style bracketStyle);
    }
    public interface ButtonStyleStep {
        ButtonSelectedStyleStep setButtonStyle(Style buttonStyle);
    }
    public interface ButtonSelectedStyleStep {
        UnexpectedErrorStyleStep setButtonSelectedStyle(Style buttonSelectedStyle);
    }
    public interface UnexpectedErrorStyleStep {
        BuildStep setUnexpectedErrorStyle(Style unexpectedErrorStyle);
    }
    public interface BuildStep {
        Theme build();
    }
    
    private static final class BuilderImpl
    implements  NameStep,
                BackgroundColorStep,
                CaretColorStep,
                BasicStyleStep,
                NoteStyleStep,
                CommandStyleStep,
                NumberStyleStep,
                FunctionStyleStep,
                LiteralStyleStep,
                BracketStyleStep,
                ButtonStyleStep,
                ButtonSelectedStyleStep,
                UnexpectedErrorStyleStep,
                BuildStep {
        private String    name;
        private ArgbColor backgroundColor;
        private ArgbColor caretColor;
        private Style     basicStyle;
        private Style     noteStyle;
        private Style     commandStyle;
        private Style     numberStyle;
        private Style     literalStyle;
        private Style     functionStyle;
        private Style     bracketStyle;
        private Style     buttonStyle;
        private Style     buttonSelectedStyle;
        private Style     unexpectedErrorStyle;
        
        private BuilderImpl() {
        }
        
        @Override
        public BackgroundColorStep setName(String name) {
            this.name = requireNonNull(name, "name");
            return this;
        }
        
        @Override
        public CaretColorStep setBackgroundColor(ArgbColor backgroundColor) {
            this.backgroundColor = requireNonNull(backgroundColor, "backgroundColor");
            return this;
        }
        
        @Override
        public BasicStyleStep setCaretColor(ArgbColor caretColor) {
            this.caretColor = caretColor;
            return this;
        }
        
        @Override
        public NoteStyleStep setBasicStyle(Style basicStyle) {
            this.basicStyle = requireNonNull(basicStyle, "basicStyle");
            return this;
        }
        
        @Override
        public CommandStyleStep setNoteStyle(Style noteStyle) {
            this.noteStyle = requireNonNull(noteStyle, "noteStyle");
            return this;
        }
        
        @Override
        public NumberStyleStep setCommandStyle(Style commandStyle) {
            this.commandStyle = requireNonNull(commandStyle, "commandStyle");
            return this;
        }
        
        @Override
        public LiteralStyleStep setNumberStyle(Style numberStyle) {
            this.numberStyle = requireNonNull(numberStyle, "numberStyle");
            return this;
        }
        
        @Override
        public FunctionStyleStep setLiteralStyle(Style literalStyle) {
            this.literalStyle = requireNonNull(literalStyle, "literalStyle");
            return this;
        }
        
        @Override
        public BracketStyleStep setFunctionStyle(Style functionStyle) {
            this.functionStyle = requireNonNull(functionStyle, "functionStyle");
            return this;
        }
        
        @Override
        public ButtonStyleStep setBracketStyle(Style bracketStyle) {
            this.bracketStyle = requireNonNull(bracketStyle, "bracketStyle");
            return this;
        }
        
        @Override
        public ButtonSelectedStyleStep setButtonStyle(Style buttonStyle) {
            this.buttonStyle = requireNonNull(buttonStyle, "buttonStyle");
            return this;
        }
        
        @Override
        public UnexpectedErrorStyleStep setButtonSelectedStyle(Style buttonSelectedStyle) {
            this.buttonSelectedStyle = requireNonNull(buttonSelectedStyle, "buttonSelectedStyle");
            return this;
        }
        
        @Override
        public BuildStep setUnexpectedErrorStyle(Style unexpectedErrorStyle) {
            this.unexpectedErrorStyle = requireNonNull(unexpectedErrorStyle, "unexpectedErrorStyle");
            return this;
        }
        
        @Override
        public Theme build() {
            return new Theme(this.name,
                             this.backgroundColor,
                             this.caretColor,
                             this.basicStyle,
                             this.noteStyle,
                             this.commandStyle,
                             this.numberStyle,
                             this.literalStyle,
                             this.functionStyle,
                             this.bracketStyle,
                             this.buttonStyle,
                             this.buttonSelectedStyle,
                             this.unexpectedErrorStyle);
        }
    }
    
    public static final class FreeBuilder {
        private String    name;
        private ArgbColor backgroundColor;
        private ArgbColor caretColor;
        
        private final Map<Key, Style> styles = new HashMap<>();
        
        public FreeBuilder(Theme theme) {
            setAll(theme);
        }
        
        public FreeBuilder setAll(Theme theme) {
            this.name            = theme.getName();
            this.backgroundColor = theme.getBackgroundColor();
            this.caretColor      = theme.getCaretColor();
            for (Key key : Key.VALUES) {
                styles.put(key, key.apply(theme));
            }
            return this;
        }
        
        public FreeBuilder setName(String name) {
            this.name = Objects.requireNonNull(name, "name");
            return this;
        }
        
        public FreeBuilder setBackgroundColor(ArgbColor backgroundColor) {
            this.backgroundColor = Objects.requireNonNull(backgroundColor, "backgroundColor");
            return this;
        }
        
        public FreeBuilder setCaretColor(ArgbColor caretColor) {
            this.caretColor = caretColor;
            return this;
        }
        
//        public FreeBuilder setStyle(Setting<Style> key, Style val) {
//            Objects.requireNonNull(key, "key");
//            Objects.requireNonNull(val, "val");
//            styles.put(key, val);
//            return this;
//        }
        
        public FreeBuilder setStyle(Key key, Style val) {
            Objects.requireNonNull(key, "key");
            Objects.requireNonNull(val, "val");
            styles.put(key, val);
            return this;
        }
        
        public Theme build() {
            return new Theme(this.name,
                             this.backgroundColor,
                             this.caretColor,
                             styles.get(Key.BASIC),
                             styles.get(Key.NOTE),
                             styles.get(Key.COMMAND),
                             styles.get(Key.NUMBER),
                             styles.get(Key.LITERAL),
                             styles.get(Key.FUNCTION),
                             styles.get(Key.BRACKET),
                             styles.get(Key.BUTTON),
                             styles.get(Key.BUTTON_SELECTED),
                             styles.get(Key.INTERNAL_ERROR));
        }
    }
    
    private static final class Accessor {
        private final    String                 name;
        private final    Function<Theme, Style> getter;
        private volatile Setting<Style>         setting;
        
        Accessor(String                 name,
                 Function<Theme, Style> getter) {
            this.name    = name;
            this.getter  = getter;
        }
        
        String getName() {
            return this.name;
        }
        
        Style getStyle(Theme theme) {
            return this.getter.apply(theme);
        }
        
        Setting<Style> getSetting() {
            Setting<Style> setting = this.setting;
            if (setting == null) {
                this.setting = setting = Setting.valueOf(this.name).as(Style.class);
            }
            return setting;
        }
    }
    
    private static final List<Accessor> ACCESSORS = new ArrayList<>();
    
    static {
        for (Field f : Theme.class.getDeclaredFields()) {
            if (f.getModifiers() != (Modifier.PRIVATE | Modifier.FINAL))
                continue;
            if (f.getType() != Style.class)
                continue;
            String                 name    = f.getName();
            String                 getName = "get" + name.toUpperCase(Locale.ROOT).substring(0, 1) + name.substring(1);
            Function<Theme, Style> getter  = Misc.createInstanceFunction(Theme.class, getName, Style.class);
            
            ACCESSORS.add(new Accessor(name, getter));
        }
    }
    
    private static final ToString<Theme> TO_STRING;
    static {
        ToString.Builder<Theme> builder = ToString.builder(Theme.class);
        
        builder.add("name", Theme::getName);
        builder.add("backgroundColor", Theme::getBackgroundColor);
        builder.add("caretColor", Theme::getCaretColor);
        for (Key key : Key.VALUES) {
            builder.add(key.getLowerCaseName(), key::apply);
        }
        
        TO_STRING = builder.build();
    }
}