/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.util.*;

import java.util.function.*;

public interface AsyncExecutor {
    boolean isInterfaceThread();
    
    default void execute(Runnable task) {
        if (this.isInterfaceThread()) {
            task.run();
        } else {
            this.executeLater(task);
        }
    }
    
    void executeLater(Runnable task);
    
    default void executeNow(Runnable task) {
        this.executeNow(null, task);
    }
    
    default void executeNow(Object monitor, Runnable task) {
        this.executeNow(monitor, () -> { task.run(); return (Void) null; });
    }
    
    default <T> T executeNow(Supplier<? extends T> task) {
        return this.executeNow(null, task);
    }
    
    default <T> T executeNow(Object monitor0, Supplier<? extends T> task) {
        if (this.isInterfaceThread()) {
            return task.get();
        }
        final class Result<T> {
            boolean          done   = false;
            T                result = null;
            RuntimeException caught = null;
        }
        
        Result<T> result  = new Result<>();
        Object    monitor = monitor0 == null ? result : monitor0;
        
        synchronized (monitor) {
            this.executeLater(() -> {
                synchronized (monitor) {
                    try {
                        result.result = task.get();
                    } catch (RuntimeException x) {
                        result.caught = x;
                    } catch (Throwable x) {
                        result.caught = new AsyncException();
                        throw x;
                    } finally {
                        result.done = true;
                        monitor.notifyAll();
                    }
                }
            });
            
            do {
                Misc.wait(monitor);
            } while (!result.done);
            
            if (result.caught != null) {
                throw result.caught;
            }
            
            return result.result;
        }
    }
    
    final class AsyncException extends RuntimeException {
    }
}