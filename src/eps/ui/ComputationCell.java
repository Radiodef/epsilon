/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.json.*;
import eps.eval.*;
import eps.app.*;
import eps.block.*;

import java.util.function.*;
import java.io.Serializable;

public interface ComputationCell extends GeneralTextCell {
    Computation getComputation();
    void setComputation(Computation comp, boolean updateUi);
    
    boolean isComplete();
    void setComplete(boolean isComplete);
    
    void addStopListener(Consumer<? super ComputationCell> listener);
    
    default void addStopListener(Runnable listener) {
        this.addStopListener((Consumer<? super ComputationCell> & Serializable) cell -> listener.run());
    }
    
    void removeStopListener(Consumer<? super ComputationCell> listener);
    
    void addReevalListener(Consumer<? super ComputationCell> listener);
    
    default void addReevalListener(Runnable listener) {
        this.addReevalListener((Consumer<? super ComputationCell> & Serializable) cell -> listener.run());
    }
    
    void removeReevalListener(Consumer<? super ComputationCell> listener);
    
    @Override
    ComputationCell followTextStyle(Setting<Style> setting);
    
    @Override
    default eps.ui.Packet getPacket() {
        return new Packet(this);
    }
    
    @Override
    default ComputationCell configure(Item item, eps.ui.Packet packet0) {
        Packet packet = (Packet) packet0;
        GeneralTextCell.super.configure(item, packet);
        
        if (packet.tree != null) {
            Computation comp = Computation.create(item, packet.tree, packet.hasResult, packet.answerFormat, packet.postComp);
            this.setComputation(comp, false);
        }
        this.setComplete(true);
        
        return this;
    }
    
    @JsonSerializable
    class Packet extends GeneralTextCell.Packet {
        @JsonProperty
        private final Tree tree;
        @JsonProperty
        private final boolean hasResult;
        @JsonDefault("hasResult")
        private static boolean hasResultDefault() { return false; }
        @JsonProperty
        private final AnswerFormatter answerFormat;
        @JsonDefault("answerFormat")
        private static AnswerFormatter answerFormatDefault() { return AnswerFormatter.DECIMAL; }
        @JsonProperty
        private final PostComputation postComp;
        @JsonDefault("postComp")
        private static PostComputation postCompDefault() { return PostComputation.IDENTITY; }
        
        @JsonConstructor
        protected Packet(BoundingBox     margins,
                         Setting<Style>  toFollow,
                         String          text,
                         Block           block,
                         Tree            tree,
                         boolean         hasResult,
                         AnswerFormatter answerFormat,
                         PostComputation postComp) {
            super(margins, toFollow, text, block);
            this.tree         = tree;
            this.hasResult    = hasResult;
            this.answerFormat = answerFormat;
            this.postComp     = postComp;
        }
        protected Packet(ComputationCell cell) {
            super(cell.getMargins(), cell.getFollowedStyle(), getText(cell), cell.getBlock());
            Computation comp = cell.getComputation();
            if (comp == null) {
                this.tree         = null;
                this.hasResult    = false;
                this.answerFormat = null;
                this.postComp     = null;
            } else {
                this.tree         = comp.getTree();
                this.hasResult    = comp.hasResult();
                this.answerFormat = comp.getAnswerFormatter();
                this.postComp     = comp.getPostComputation();
            }
        }
        
        private static String getText(ComputationCell cell) {
            return cell.getComputation() == null && cell.getBlock() == null ? cell.getText() : null;
        }
        
        protected Tree getTree() {
            return tree;
        }
        
        @Override
        public Cell newCell(Item item) {
            ComputationCell cell = App.app().getComponentFactory().newComputation();
            cell.configure(item, this);
            return cell;
        }
    }
}