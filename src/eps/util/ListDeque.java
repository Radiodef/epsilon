/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.math.Math2.*;

import java.util.*;

public class ListDeque<E> extends AbstractList<E> implements Deque<E> {
    private Object[] elements;
    
    private int startIndex;
    private int endIndex;
    
    private int size;
    
    public ListDeque() {
        this(10);
    }
    
    public ListDeque(int initialCapacity) {
        if (initialCapacity < 0) {
            throw Errors.newIllegalArgument("initialCapacity", initialCapacity);
        }
        
        elements   = new Object[initialCapacity];
        startIndex = endIndex = initialCapacity / 2;
    }
    
    public ListDeque(Collection<? extends E> elems) {
        this(elems.size());
        addAll(elems);
    }
    
    // wrong
//    private int toListIndex(int internalIndex) {
//        assert inRange(internalIndex, 0, elements.length) : internalIndex;
//        int startIndex = this.startIndex;
//        
//        if (internalIndex < startIndex) {
//            return elements.length + internalIndex;
//        } else {
//            return startIndex + internalIndex;
//        }
//    }
    
    private int toListIndex(int internalIndex) {
        assert inRange(internalIndex, 0, elements.length) : internalIndex;
        int startIndex = this.startIndex;
        
        if (internalIndex < startIndex) {
            internalIndex += elements.length;
        }
        return internalIndex - startIndex;
    }
    
    private int toInternalIndex(int listIndex) {
        assert listIndex >= 0 : listIndex;
        return (startIndex + listIndex) % elements.length;
    }
    
    private static int nextIndex(int index, Object[] elements) {
        int length = elements.length;
        
        while (index < 0) {
            index += length;
        }
        
        return (index + 1) % length;
    }
    
    private static int prevIndex(int index, Object[] elements) {
        int length = elements.length;
        
        --index;
        
        while (index < 0) {
            index += length;
        }
        
        return index % length;
    }
    
    private boolean isInBounds(int index) {
        long startIndex = this.startIndex;
        long endIndex   = this.endIndex;
        
        if (endIndex < startIndex) {
            endIndex = startIndex + (long) size;
        }
        
        return (0 <= index) && ((startIndex + (long) index) < endIndex);
    }
    
    private void requireBounds(int index) {
        if (!isInBounds(index)) {
            throw Errors.newIndexOutOfBounds("index", index);
        }
    }
    
    public boolean ensureCapacity(int additionalSize) {
        if (additionalSize < 0) {
            return false;
        }
        
        Object[] elements = this.elements;
        
        long oldCap  = elements.length;
        int  oldSize = size;
        long newSize = (long) oldSize + (long) additionalSize;
        
        if (newSize < oldCap) {
            return false;
        }
        
        long newCap = ((newSize + 1) * 3) >> 1;
        
        if ((newCap > Integer.MAX_VALUE) && ((newCap = newSize) > Integer.MAX_VALUE)) {
            throw new OutOfMemoryError(String.valueOf(newSize));
        }
        
        this.elements = elements = Arrays.copyOf(elements, (int) newCap);
        
        int startIndex = this.startIndex;
        int endIndex   = this.endIndex;
        
        if (endIndex < startIndex) {
            long virtualEnd = (long) startIndex + (long) oldSize;
            
            int dstStart = (int) oldCap;
            int dstEnd   = (int) Math.min(virtualEnd, newCap);
            int moveLen  = dstEnd - dstStart;
            
            System.arraycopy(elements, 0, elements, dstStart, moveLen);
            
            int clearStart = 0;
            int clearEnd   = endIndex;
            
            if (virtualEnd > newCap) {
                int shiftLen = endIndex - moveLen;
                System.arraycopy(elements, moveLen, elements, 0, shiftLen);
                
                clearStart = shiftLen;
                endIndex   = shiftLen;
            } else {
                endIndex   = dstEnd;
            }
            
            Arrays.fill(elements, clearStart, clearEnd, null);
            this.endIndex = endIndex;
        }
        
        return true;
    }
    
    @SuppressWarnings("unchecked")
    private E getElement(int index) {
        return (E) elements[toInternalIndex(index)];
    }
    
    @SuppressWarnings("unchecked")
    private E setElement(int index, E element) {
        Object[] elements = this.elements;
        
        index = toInternalIndex(index);
        
        E old = (E) elements[index];
        elements[index] = element;
        
        return old;
    }
    
    @Override
    public int size() {
        return size;
    }
    
    public int capacity() {
        return elements.length;
    }
    
    @Override
    public void add(int index, E element) {
        if (true) {
            // throws
            super.add(index, element);
            return;
        }
        
        // TODO
    }
    
    @Override
    public boolean add(E element) {
        return offerLast(element);
    }
    
    @Override
    public void addLast(E element) {
        add(element);
    }
    
    @Override
    public boolean offer(E element) {
        return offerLast(element);
    }
    
    @Override
    public boolean offerLast(E element) {
        ensureCapacity(1);
        
        Object[] elements  = this.elements;
        int      endIndex  = this.endIndex;
        long     length    = elements.length;
        int      lastIndex = (int) ( ((long) endIndex + length) % length );
        
        elements[lastIndex] = element;
        
        this.endIndex = nextIndex(endIndex, elements);
        ++this.size;
        return true;
    }
    
    @Override
    public void addFirst(E element) {
        offerFirst(element);
    }
    
    @Override
    public boolean offerFirst(E element) {
        int size = this.size;
        if (size == 0) {
            return offerLast(element);
        }
        
        ensureCapacity(1);
        
        Object[] elements   = this.elements;
        int      startIndex = this.startIndex;
        int      prevIndex  = prevIndex(startIndex, elements);
        
        elements[prevIndex] = element;
        
        this.startIndex = prevIndex;
        this.size       = size + 1;
        return true;
    }
    
    @Override
    public void push(E element) {
        offerFirst(element);
    }
    
    @Override
    public E peek() {
        return peekFirst();
    }
    
    @SuppressWarnings("unchecked")
    private E head() {
        Object[] elements = this.elements;
        long     length   = elements.length;
        int      index    = (int) ( ((long) startIndex + length) % length );
        return (E) elements[index];
    }
    
    @SuppressWarnings("unchecked")
    private E tail() {
        Object[] elements = this.elements;
        long     length   = elements.length;
        int      index    = (int) ( ((long) endIndex + length - 1L) % length );
        return (E) elements[index];
    }
    
    @Override
    public E peekFirst() {
        return isEmpty() ? null : head();
    }
    
    @Override
    public E element() {
        return getFirst();
    }
    
    @Override
    public E getFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return head();
    }
    
    @Override
    public E peekLast() {
        return isEmpty() ? null : tail();
    }
    
    @Override
    public E getLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return tail();
    }
    
    @Override
    public E get(int index) {
        requireBounds(index);
        return getElement(index);
    }
    
    @Override
    public E set(int index, E element) {
        requireBounds(index);
        return setElement(index, element);
    }
    
    @Override
    public E remove(int index) {
        requireBounds(index);
        
        Object[] elements = this.elements;
        
        int internal = toInternalIndex(index);
        @SuppressWarnings("unchecked")
        E element = (E) elements[internal];
        
        int size = this.size;
        int dst  = internal;
        int src;
        for (;;) {
            if (++index == size) {
                elements[dst] = null;
                break;
            }
            
            src = toInternalIndex(index);
            elements[dst] = elements[src];
            
            dst = src;
        }
        
        this.endIndex = prevIndex(this.endIndex, elements);
        this.size     = size - 1;
        return element;
    }
    
    @Override
    @SuppressWarnings("collection-remove")
    public boolean removeFirstOccurrence(Object element) {
        return remove(element);
    }
    
    @Override
    public boolean removeLastOccurrence(Object element) {
        int lastIndex = lastIndexOf(element);
        if (lastIndex < 0) {
            return false;
        } else {
            remove(lastIndex);
            return true;
        }
    }
    
    @Override
    public E pop() {
        return removeFirst();
    }
    
    @Override
    public E poll() {
        return pollFirst();
    }
    
    @Override
    public E pollFirst() {
        if (isEmpty()) {
            return null;
        } else {
            return removeFirst();
        }
    }
    
    @Override
    public E remove() {
        return removeFirst();
    }
    
    @Override
    public E removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        
        Object[] elements   = this.elements;
        int      startIndex = this.startIndex;
        long     length     = elements.length;
        int      firstIndex = (int) ( ((long) startIndex + length) % length );
        
        @SuppressWarnings("unchecked")
        E element = (E) elements[firstIndex];
        
        this.startIndex = nextIndex(startIndex, elements);
        --this.size;
        return element;
    }
    
    @Override
    public E pollLast() {
        if (isEmpty()) {
            return null;
        } else {
            return removeLast();
        }
    }
    
    @Override
    public E removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        
        Object[] elements = this.elements;
        int      endIndex = prevIndex(this.endIndex, elements);
        
        @SuppressWarnings("unchecked")
        E element = (E) elements[endIndex];
        
        this.endIndex = endIndex;
        --this.size;
        return element;
    }
    
    @Override
    public Iterator<E> descendingIterator() {
        return new DescendingIterator();
    }
    
    private class DescendingIterator implements Iterator<E> {
        private int prev   = -1;
        private int cursor = endIndex;
        
        @Override
        public boolean hasNext() {
            return cursor != startIndex;
        }
        
        @Override
        public E next() {
            if (hasNext()) {
                Object[] elements = ListDeque.this.elements;
                
                int cursor  = this.cursor;
                int index   = ListDeque.prevIndex(cursor, elements);
                
                @SuppressWarnings("unchecked")
                E   element = (E) elements[index];
                
                this.prev   = index;
                this.cursor = index;
                
                return element;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            if (prev == -1) {
                throw new IllegalStateException();
            }
            
            ListDeque.this.remove(toListIndex(prev));
            prev = -1;
            // we don't need to do anything else, because
            // remove(int) only decrements the end index
        }
    }
}