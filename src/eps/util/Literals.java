/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.util.Misc.*;

import java.util.*;
import java.util.function.*;
import java.lang.reflect.*;

import static java.lang.System.out;

public final class Literals {
    private static final int MAX_OVERLOAD = 12;
    
    private final Class<?> resultType;
    private final Class<?> actualType;
    
    private final int maxOverload = MAX_OVERLOAD;
    
    private Literals(Class<?> resultType,
                     Class<?> actualType) {
        this.resultType = resultType;
        this.actualType = actualType;
    }
    
    private static final class Param {
        final String type;
        final String name;
        Param(String type, String name) { this.type = type;
                                          this.name = name; }
        String name(int n) { return name + n; }
    }
    
    private List<Param> params() {
        if (Collection.class.isAssignableFrom(actualType)) {
            return Arrays.asList(new Param("E", "elem"));
        }
        if (Map.class.isAssignableFrom(actualType)) {
            return Arrays.asList(new Param("K", "key"), new Param("V", "val"));
        }
        throw new AssertionError(actualType);
    }
    
    private void generate() {
        for (int i = 0; i <= maxOverload; ++i) {
            generateOne(i, null);
        }
        
        if (resultType.getSimpleName().contains("Tree")) {
            Param comp = new Param("Comparator<? super " + params().get(0).type + ">", "comp");
            
            for (int i = 0; i <= maxOverload; ++i) {
                generateOne(i, comp);
            }
        }
    }
    
    private String typeVars() {
        TypeVariable<?>[] vars = resultType.getTypeParameters();
        if (vars.length > 0) {
            return j(Arrays.asList(vars), ", ", "<", ">", TypeVariable::getName);
        } else {
            return "";
        }
    }
    
    private Method accumulator() {
        try {
            if (Collection.class.isAssignableFrom(actualType)) {
                return Collection.class.getMethod("add", Object.class);
            } else if (Map.class.isAssignableFrom(actualType)) {
                return Map.class.getMethod("put", Object.class, Object.class);
            } else {
                throw new AssertionError(actualType);
            }
        } catch (ReflectiveOperationException x) {
            throw new AssertionError("", x);
        }
    }
    
    private String constructorArg(int count) {
        if (actualType == ArrayList.class) {
            return Integer.toString(count);
        }
        if (actualType.getSimpleName().contains("Hash")) {
            return Integer.toString((count == 0) ? 0 : eps.math.Math2.next2((count + 1) * 3 / 2));
        }
        return "";
    }
    
    private void generateOne(int paramCount, Param specialParam) {
        String      typeVars     = typeVars();
        boolean     hasTypeVars  = !typeVars.isEmpty();
        String      diamond      = hasTypeVars ? "<>" : "";
        String      simpleResult = resultType.getSimpleName();
        String      simpleActual = actualType.getSimpleName();
        Method      accumulator  = accumulator();
        List<Param> params       = params();
        
        out.print("    public static ");
        if (hasTypeVars)
            out.printf("%s ", typeVars);
        out.print(simpleResult);
        if (hasTypeVars)
            out.printf("%s", typeVars);
        out.printf(" %sOf(", simpleResult);
        
        if (specialParam != null) {
            out.printf("%s %s", specialParam.type, specialParam.name);
            if (paramCount != 0)
                out.print(", ");
        }
        
        for (int i = 0; i < paramCount; ++i) {
            if (i != 0) out.print(", ");
            int i0 = i;
            out.print(j(params, ", ", p -> p.type + " " + p.name(i0)));
        }
        
        out.println(") {");
        
        if (specialParam != null) {
            out.printf("        %s%s result = new %s%s(%s);%n", simpleResult, typeVars, simpleActual, diamond, specialParam.name);
        } else {
            out.printf("        %s%s result = new %s%s(%s);%n", simpleResult, typeVars, simpleActual, diamond, constructorArg(paramCount));
        }
        
        for (int i = 0; i < paramCount; ++i) {
            int j = i;
            out.printf("        result.%s(%s);%n", accumulator.getName(), j(params, ", ", p -> p.name(j)));
        }
        
        out.println("        return result;");
        out.println("    }");
        out.println("    ");
    }
    
    public static void generateAllOverloads() {
        List<Literals> l = new ArrayList<>();
        
        l.add(new Literals(List.class, ArrayList.class));
        l.add(new Literals(Set.class,  HashSet.class));
        l.add(new Literals(Map.class,  HashMap.class));
        l.add(new Literals(TreeSet.class, TreeSet.class));
        l.add(new Literals(TreeMap.class, TreeMap.class));
        l.add(new Literals(LinkedHashSet.class, LinkedHashSet.class));
        l.add(new Literals(LinkedHashMap.class, LinkedHashMap.class));
        
        l.forEach(Literals::generate);
    }
    
    public static void generateForEachOverloads() {
        for (int count = 0; count <= MAX_OVERLOAD; ++count) {
            if (count != 0) {
                out.println("    ");
            }
            generateForEachOverload(count);
        }
    }
    
    private static void generateForEachOverload(int count) {
        out.print("    public static <T> void forEach(");
        
        for (int i = 0; i < count; ++i) {
            if (i != 0) out.print(", ");
            out.print("T elem");
            out.print(i);
        }
        
        if (count > 0) out.print(", ");
        
        out.println("Consumer<? super T> action) {");
        
        for (int i = 0; i < count; ++i) {
            out.print("        action.accept(elem");
            out.print(i);
            out.println(");");
        }
        
        out.println("    }");
    }
    
    public static <E> List<E> ListOf() {
        List<E> result = new ArrayList<>(0);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0) {
        List<E> result = new ArrayList<>(1);
        result.add(elem0);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1) {
        List<E> result = new ArrayList<>(2);
        result.add(elem0);
        result.add(elem1);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2) {
        List<E> result = new ArrayList<>(3);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3) {
        List<E> result = new ArrayList<>(4);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4) {
        List<E> result = new ArrayList<>(5);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5) {
        List<E> result = new ArrayList<>(6);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6) {
        List<E> result = new ArrayList<>(7);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7) {
        List<E> result = new ArrayList<>(8);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8) {
        List<E> result = new ArrayList<>(9);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9) {
        List<E> result = new ArrayList<>(10);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10) {
        List<E> result = new ArrayList<>(11);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        return result;
    }
    
    public static <E> List<E> ListOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10, E elem11) {
        List<E> result = new ArrayList<>(12);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        result.add(elem11);
        return result;
    }
    
    public static <E> Set<E> SetOf() {
        Set<E> result = new HashSet<>(0);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0) {
        Set<E> result = new HashSet<>(4);
        result.add(elem0);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1) {
        Set<E> result = new HashSet<>(4);
        result.add(elem0);
        result.add(elem1);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2) {
        Set<E> result = new HashSet<>(8);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3) {
        Set<E> result = new HashSet<>(8);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4) {
        Set<E> result = new HashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5) {
        Set<E> result = new HashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6) {
        Set<E> result = new HashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7) {
        Set<E> result = new HashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8) {
        Set<E> result = new HashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9) {
        Set<E> result = new HashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10) {
        Set<E> result = new HashSet<>(32);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        return result;
    }
    
    public static <E> Set<E> SetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10, E elem11) {
        Set<E> result = new HashSet<>(32);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        result.add(elem11);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf() {
        Map<K, V> result = new HashMap<>(0);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0) {
        Map<K, V> result = new HashMap<>(4);
        result.put(key0, val0);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1) {
        Map<K, V> result = new HashMap<>(4);
        result.put(key0, val0);
        result.put(key1, val1);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2) {
        Map<K, V> result = new HashMap<>(8);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3) {
        Map<K, V> result = new HashMap<>(8);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4) {
        Map<K, V> result = new HashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5) {
        Map<K, V> result = new HashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6) {
        Map<K, V> result = new HashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7) {
        Map<K, V> result = new HashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8) {
        Map<K, V> result = new HashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9) {
        Map<K, V> result = new HashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10) {
        Map<K, V> result = new HashMap<>(32);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        return result;
    }
    
    public static <K, V> Map<K, V> MapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10, K key11, V val11) {
        Map<K, V> result = new HashMap<>(32);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        result.put(key11, val11);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf() {
        TreeSet<E> result = new TreeSet<>();
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10, E elem11) {
        TreeSet<E> result = new TreeSet<>();
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        result.add(elem11);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp) {
        TreeSet<E> result = new TreeSet<>(comp);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4, E elem5) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        return result;
    }
    
    public static <E> TreeSet<E> TreeSetOf(Comparator<? super E> comp, E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10, E elem11) {
        TreeSet<E> result = new TreeSet<>(comp);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        result.add(elem11);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf() {
        TreeMap<K, V> result = new TreeMap<>();
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10, K key11, V val11) {
        TreeMap<K, V> result = new TreeMap<>();
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        result.put(key11, val11);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        return result;
    }
    
    public static <K, V> TreeMap<K, V> TreeMapOf(Comparator<? super K> comp, K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10, K key11, V val11) {
        TreeMap<K, V> result = new TreeMap<>(comp);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        result.put(key11, val11);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf() {
        LinkedHashSet<E> result = new LinkedHashSet<>(0);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0) {
        LinkedHashSet<E> result = new LinkedHashSet<>(4);
        result.add(elem0);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1) {
        LinkedHashSet<E> result = new LinkedHashSet<>(4);
        result.add(elem0);
        result.add(elem1);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2) {
        LinkedHashSet<E> result = new LinkedHashSet<>(8);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3) {
        LinkedHashSet<E> result = new LinkedHashSet<>(8);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4) {
        LinkedHashSet<E> result = new LinkedHashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5) {
        LinkedHashSet<E> result = new LinkedHashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6) {
        LinkedHashSet<E> result = new LinkedHashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7) {
        LinkedHashSet<E> result = new LinkedHashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8) {
        LinkedHashSet<E> result = new LinkedHashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9) {
        LinkedHashSet<E> result = new LinkedHashSet<>(16);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10) {
        LinkedHashSet<E> result = new LinkedHashSet<>(32);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        return result;
    }
    
    public static <E> LinkedHashSet<E> LinkedHashSetOf(E elem0, E elem1, E elem2, E elem3, E elem4, E elem5, E elem6, E elem7, E elem8, E elem9, E elem10, E elem11) {
        LinkedHashSet<E> result = new LinkedHashSet<>(32);
        result.add(elem0);
        result.add(elem1);
        result.add(elem2);
        result.add(elem3);
        result.add(elem4);
        result.add(elem5);
        result.add(elem6);
        result.add(elem7);
        result.add(elem8);
        result.add(elem9);
        result.add(elem10);
        result.add(elem11);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf() {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(0);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(4);
        result.put(key0, val0);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(4);
        result.put(key0, val0);
        result.put(key1, val1);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(8);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(8);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(16);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(32);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        return result;
    }
    
    public static <K, V> LinkedHashMap<K, V> LinkedHashMapOf(K key0, V val0, K key1, V val1, K key2, V val2, K key3, V val3, K key4, V val4, K key5, V val5, K key6, V val6, K key7, V val7, K key8, V val8, K key9, V val9, K key10, V val10, K key11, V val11) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>(32);
        result.put(key0, val0);
        result.put(key1, val1);
        result.put(key2, val2);
        result.put(key3, val3);
        result.put(key4, val4);
        result.put(key5, val5);
        result.put(key6, val6);
        result.put(key7, val7);
        result.put(key8, val8);
        result.put(key9, val9);
        result.put(key10, val10);
        result.put(key11, val11);
        return result;
    }
    
    public static <T> void forEach(Consumer<? super T> action) {
    }
    
    public static <T> void forEach(T elem0, Consumer<? super T> action) {
        action.accept(elem0);
    }
    
    public static <T> void forEach(T elem0, T elem1, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, T elem5, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
        action.accept(elem5);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, T elem5, T elem6, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
        action.accept(elem5);
        action.accept(elem6);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, T elem5, T elem6, T elem7, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
        action.accept(elem5);
        action.accept(elem6);
        action.accept(elem7);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, T elem5, T elem6, T elem7, T elem8, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
        action.accept(elem5);
        action.accept(elem6);
        action.accept(elem7);
        action.accept(elem8);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, T elem5, T elem6, T elem7, T elem8, T elem9, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
        action.accept(elem5);
        action.accept(elem6);
        action.accept(elem7);
        action.accept(elem8);
        action.accept(elem9);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, T elem5, T elem6, T elem7, T elem8, T elem9, T elem10, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
        action.accept(elem5);
        action.accept(elem6);
        action.accept(elem7);
        action.accept(elem8);
        action.accept(elem9);
        action.accept(elem10);
    }
    
    public static <T> void forEach(T elem0, T elem1, T elem2, T elem3, T elem4, T elem5, T elem6, T elem7, T elem8, T elem9, T elem10, T elem11, Consumer<? super T> action) {
        action.accept(elem0);
        action.accept(elem1);
        action.accept(elem2);
        action.accept(elem3);
        action.accept(elem4);
        action.accept(elem5);
        action.accept(elem6);
        action.accept(elem7);
        action.accept(elem8);
        action.accept(elem9);
        action.accept(elem10);
        action.accept(elem11);
    }
}