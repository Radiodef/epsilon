/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.math.Math2.*;

import java.util.*;
import java.lang.reflect.*;

public final class AnyArrayIterator implements Iterator<Object> {
    private final Object array;
    private final int    end;
    
    private int i;
    
    public AnyArrayIterator(Object array) {
        this(array, 0, Array.getLength(array), (Void) null);
    }
    
    public AnyArrayIterator(Object array, int start, int end) {
        this(array, start, end, (Void) null);
        
        int length = Array.getLength(array);
        
        if (!inRange(end, 0, Array.getLength(array))) {
            throw Errors.newIndexOutOfBounds("end", end, 0, length);
        }
        if (!inRange(start, 0, end)) {
            throw Errors.newIndexOutOfBounds("start", start, 0, end);
        }
    }
    
    private AnyArrayIterator(Object array, int start, int end, Void $private) {
        this.array = Objects.requireNonNull(array, "array");
        this.i     = start;
        this.end   = end;
        
        if (!array.getClass().isArray()) {
            // Array.getLength also throws IllegalArgumentException
            throw new IllegalArgumentException(array.getClass().getName());
        }
    }
    
    @Override
    public boolean hasNext() {
        return i < end;
    }
    
    @Override
    public Object next() {
        int i = this.i;
        
        if (i < end) {
            Object next = Array.get(array, i);
            this.i = i + 1;
            return next;
        }
        
        throw new NoSuchElementException(Integer.toString(i));
    }
}