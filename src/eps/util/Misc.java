/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.app.*;
import static eps.util.Literals.*;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.lang.invoke.*;
import java.lang.invoke.MethodHandles.*;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

import static java.util.stream.Collectors.*;

public final class Misc {
    private Misc() {
    }
    
    @SuppressWarnings("unchecked")
    public static final Class<Class<?>> WILD_CLASS =
        (Class<Class<?>>) (Class<? super Class<?>>) Class.class;
    
    @SuppressWarnings("unchecked")
    public static final Class<List<?>> WILD_LIST =
        (Class<List<?>>) (Class<? super List<?>>) List.class;
    
    @SuppressWarnings("unchecked")
    public static final Class<Set<?>> WILD_SET =
        (Class<Set<?>>) (Class<? super Set<?>>) Set.class;
    
    @SuppressWarnings("unchecked")
    public static final Class<Map<?, ?>> WILD_MAP =
        (Class<Map<?, ?>>) (Class<? super Map<?, ?>>) Map.class;
    
    @SuppressWarnings("unchecked")
    public static final Class<List<Character>> LIST_OF_CHAR =
        (Class<List<Character>>) (Class<? super List<Character>>) List.class;
    
    @SuppressWarnings("unchecked")
    public static final Class<List<CodePoint>> LIST_OF_CODE_POINT =
        (Class<List<CodePoint>>) (Class<? super List<CodePoint>>) List.class;
    
    @SuppressWarnings("unchecked")
    public static final Class<Set<CodePoint>> SET_OF_CODE_POINT =
        (Class<Set<CodePoint>>) (Class<? super Set<CodePoint>>) Set.class;
    
    public static final Object[]   EMPTY_OBJECT_ARRAY = new Object[0];
    public static final Class<?>[] EMPTY_CLASS_ARRAY  = new Class<?>[0];
    public static final String[]   EMPTY_STRING_ARRAY = new String[0];
    public static final int[]      EMPTY_INT_ARRAY    = new int[0];
    
    public static final int PUBLIC_STATIC_FINAL = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;
    
    public static final Consumer<Object> REQUIRE_NON_NULL = (Consumer<Object> & Serializable) Objects::requireNonNull;
    
    @SuppressWarnings("unchecked")
    public static <T> Consumer<T> requiringNonNull() {
        return (Consumer<T>) REQUIRE_NON_NULL;
    }
    
    public static <T> T replnull(T maybeNull, T replacement) {
        return maybeNull == null ? replacement : maybeNull;
    }
    
    public static <T> T weakGet(List<? extends T> list, int index) {
        if (0 <= index && index < list.size()) {
            return list.get(index);
        }
        return null;
    }
    
    public static <T, U> U weakGet(List<? extends T> list, int index, Function<? super T, ? extends U> fn) {
        if (0 <= index && index < list.size()) {
            return fn.apply(list.get(index));
        }
        return null;
    }
    
    public static <T, U> U coals(T obj, Function<? super T, ? extends U> fn) {
        return obj == null ? null : fn.apply(obj);
    }
    
    public static int nullCount(Object... args) {
        int count = 0;
        if (args != null) {
            int length = args.length;
            for (int i = 0; i < length; ++i)
                if (args[i] == null)
                    ++count;
        }
        return count;
    }
    
    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }
    
    public static String f(String format, Object... args) {
        return String.format(format, args);
    }
    
    public static String j(Iterable<?> it) {
        return j(it, null, null, null, null);
    }
    
    public static <T> String j(Iterable<? extends T> it, Function<? super T, ?> fn) {
        return j(it, null, null, null, fn);
    }
    
    public static <T> String j(Iterable<? extends T> it, String delim, Function<? super T, ?> fn) {
        return j(it, delim, null, null, fn);
    }
    
    public static <T> String j(Iterable<? extends T> it,
                               String delim,
                               String prefix,
                               String suffix,
                               Function<? super T, ?> fn) {
        if (it     == null) return "null";
        if (delim  == null) delim  = "";
        if (prefix == null) prefix = "";
        if (suffix == null) suffix = "";
        if (fn     == null) fn     = Function.identity();
        
        StringJoiner j = new StringJoiner(delim, prefix, suffix);
        
        for (T obj : it) {
            j.add(String.valueOf(fn.apply(obj)));
        }
        
        return j.toString();
    }
    
    public static <T> String j(T[]    arr,
                               String delim,
                               String prefix,
                               String suffix,
                               Function<? super T, ?> fn) {
        if (arr    == null) return "null";
        if (delim  == null) delim  = "";
        if (prefix == null) prefix = "";
        if (suffix == null) suffix = "";
        if (fn     == null) fn     = Function.identity();
        
        StringJoiner j = new StringJoiner(delim, prefix, suffix);
        
        for (T obj : arr) {
            j.add(String.valueOf(fn.apply(obj)));
        }
        
        return j.toString();
    }
    
    public static void p() {
        System.out.println();
    }
    
    public static void p(Object obj) {
        System.out.println(obj);
    }
    
    public static String toHexLiteral(int n) {
        char[] chars = new char[10];
        chars[0] = '0';
        chars[1] = 'x';
        
        for (int i = 0; i < 8; ++i) {
            int   shift  = 4 * (7 - i);
            int   nibble = (n >>> shift) & 0xF;
            chars[2 + i] = (char) ((nibble < 10) ? ('0' + nibble) : ('A' + (nibble - 10)));
        }
        
        return new String(chars);
    }
    
    public static char toHexChar(int nibble) {
        nibble &= 0xF;
        if (nibble < 10) {
            return (char) ('0' + nibble);
        } else {
            return (char) ('A' + nibble - 10);
        }
    }
    
    public static <T> T rethrow(Throwable x) {
        if (x instanceof Error)
            throw (Error) x;
        if (x instanceof RuntimeException)
            throw (RuntimeException) x;
        throw new RuntimeException(x);
    }
    
    public static <T> T rethrowCause(InvocationTargetException x) {
        Throwable cause = x.getCause();
        if (cause instanceof Error)
            throw (Error) cause;
        if (cause instanceof RuntimeException)
            throw (RuntimeException) cause;
        throw new RuntimeException(cause);
    }
    
    public static StackTraceElement getCaller() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        return trace[2];
    }
    
    public static StackTraceElement getCaller(int additionalFrames) {
        if (additionalFrames < 0) {
            throw new IllegalArgumentException(String.valueOf(additionalFrames));
        }
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        int frame = 2 + additionalFrames;
        try {
            return trace[frame];
        } catch (IndexOutOfBoundsException x) {
            throw new IllegalArgumentException(x);
        }
    }
    
    public static <T> List<T> collectConstants(Class<T> declaringClassAndType) {
        return collectConstants(declaringClassAndType, declaringClassAndType);
    }
    
    public static <T> List<T> collectConstants(Class<?> declaringClass, Class<T> type) {
        Objects.requireNonNull(declaringClass, "declaringClass");
        Objects.requireNonNull(type, "type");
        
        List<T> list = new ArrayList<>();
        
        try {
            for (Field field : declaringClass.getFields()) {
                if (field.isAnnotationPresent(IfDeveloper.class)) {
                    if (!EpsilonMain.isDeveloper()) {
                        continue;
                    }
                }
                if (type.isAssignableFrom(field.getType())) {
                    int mods = field.getModifiers();
                    if (Modifier.isPublic(mods) && Modifier.isStatic(mods) && Modifier.isFinal(mods)) {
                        Object val = field.get(null);
                        list.add(type.cast(val));
                    }
                }
            }
        } catch (ReflectiveOperationException x) {
            throw new IllegalArgumentException(x);
        }
        
        return list;
    }
    
    private static final Map<Class<?>, Class<?>> PRIMITIVE_BY_BOXED =
        MapOf(Boolean.class,   boolean.class,
              Character.class, char.class,
              Byte.class,      byte.class,
              Short.class,     short.class,
              Integer.class,   int.class,
              Long.class,      long.class,
              Float.class,     float.class,
              Double.class,    double.class,
              Void.class,      void.class);
    private static final Map<Class<?>, Class<?>> BOXED_BY_PRIMITIVE =
        PRIMITIVE_BY_BOXED.entrySet().stream()
                          .collect(toMap(Map.Entry::getValue, Map.Entry::getKey));
    
    @SuppressWarnings("unchecked")
    public static <T> Class<T> box(Class<T> cls) {
        Objects.requireNonNull(cls, "cls");
        return (Class<T>) BOXED_BY_PRIMITIVE.getOrDefault(cls, cls);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> Class<T> unbox(Class<T> cls) {
        Objects.requireNonNull(cls, "cls");
        return (Class<T>) PRIMITIVE_BY_BOXED.getOrDefault(cls, cls);
    }
    
    @SuppressWarnings("unchecked")
    private static <E extends Enum<E>> E getEnumConstantImpl(Class<?> clazz, String name) {
        return Enum.valueOf((Class<E>) clazz, name);
    }
    
    public static Enum<?> getEnumConstant(Class<?> clazz, String name) {
        return getEnumConstantImpl(clazz, name);
    }
    
//    @Deprecated
//    public static Object newInstance(Object obj) {
//        Class<?> clazz = obj.getClass();
//        
//        while (clazz != null) {
//            int mods = clazz.getModifiers();
//            if (Modifier.isAbstract(mods))
//                continue;
//            if (!Modifier.isPublic(mods))
//                continue;
//            if (clazz.getEnclosingClass() != null && !Modifier.isStatic(mods))
//                continue;
//            
//            try {
//                Constructor<?> ctor = clazz.getConstructor();
//                return ctor.newInstance();
//            } catch (ReflectiveOperationException x) {
//            }
//            
//            clazz = clazz.getSuperclass();
//        }
//        
//        // should always return at least a new Object()
//        throw new AssertionError(obj.getClass());
//    }
    
    private static final MethodType RETURNS_FUNCTION = MethodType.methodType(Function.class);
    private static final MethodType FUNCTION_APPLY   = MethodType.methodType(Object.class, Object.class);
    
    private static Function<?, ?> createFunction(Lookup       lookup,
                                                 MethodHandle implHandle,
                                                 MethodType   functionType) {
        try {
            CallSite site = LambdaMetafactory.metafactory(lookup,
                                                          "apply",
                                                          RETURNS_FUNCTION,
                                                          FUNCTION_APPLY,
                                                          implHandle,
                                                          functionType);
            try {
                return (Function<?, ?>) site.getTarget().invokeExact();
            } catch (RuntimeException | Error x) {
                throw x;
            } catch (Throwable x) {
                throw new IllegalArgumentException(x);
            }
        } catch (LambdaConversionException x) {
            throw new IllegalArgumentException(x);
        }
    }
    
    public static <T, U> Function<T, U> createConstructorFunction(Class<U> constructedClass,
                                                                  Class<T> typeOfT) {
        Lookup       lookup       = MethodHandles.lookup();
        MethodType   implType     = MethodType.methodType(void.class, typeOfT);
        MethodType   functionType = MethodType.methodType(constructedClass, typeOfT);
        MethodHandle implHandle;
        try {
            implHandle = lookup.findConstructor(constructedClass, implType);
        } catch (ReflectiveOperationException x) {
            throw new IllegalArgumentException(x);
        }
        
        @SuppressWarnings("unchecked")
        Function<T, U> result = (Function<T, U>) createFunction(lookup, implHandle, functionType);
        return result;
    }
    
    public static <T, U> Function<T, U> createInstanceFunction(Class<T> receiverClass,
                                                               String   methodName,
                                                               Class<U> typeOfU) {
        Lookup       lookup       = MethodHandles.lookup();
        MethodType   implType     = MethodType.methodType(typeOfU);
        MethodType   functionType = MethodType.methodType(typeOfU, receiverClass);
        MethodHandle implHandle;
        try {
            implHandle = lookup.findVirtual(receiverClass,
                                            methodName,
                                            implType);
        } catch (ReflectiveOperationException x) {
            throw new IllegalArgumentException(x);
        }
        
        @SuppressWarnings("unchecked")
        Function<T, U> result = (Function<T, U>) createFunction(lookup, implHandle, functionType);
        return result;
    }
    
    public static <T, U> Function<T, U> createStaticFunction(Class<?> enclosingClass,
                                                             String   methodName,
                                                             Class<T> typeOfT,
                                                             Class<U> typeOfU) {
        Lookup       lookup   = MethodHandles.lookup();
        MethodType   implType = MethodType.methodType(typeOfU, typeOfT);
        MethodHandle implHandle;
        try {
            implHandle = lookup.findStatic(enclosingClass,
                                           methodName,
                                           implType);
        } catch (ReflectiveOperationException x) {
            throw new IllegalArgumentException(x);
        }
        
        @SuppressWarnings("unchecked")
        Function<T, U> result = (Function<T, U>) createFunction(lookup, implHandle, implType);
        return result;
    }
    
    public static boolean rangeEquals(String s, int i, String r) {
        return s.regionMatches(i, r, 0, r.length());
    }
    
    public static boolean rangeBeforeEquals(String s, int i, String r) {
        Objects.requireNonNull(s, "s");
        Objects.requireNonNull(r, "r");
        int rLen = r.length();
        if ((i - rLen) < 0 || i > s.length()) {
            return false;
        }
        for (int j = rLen; j > 0; --i, --j) {
            if (s.codePointBefore(i) != r.codePointBefore(j))
                return false;
        }
        return true;
    }
    
    public static Class<?> getClass(String name) {
        Objects.requireNonNull(name, "name");
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException x) {
            throw new IllegalArgumentException(name, x);
        }
    }
    
    public static Double parseNullableDouble(String s) {
        if (s == null || (s = s.trim()).equals("null")) {
            return null;
        } else {
            return Double.valueOf(s);
        }
    }
    
    public static <T> T[] requireNonNull(T[] array, String name) {
        if (array == null) {
            throw new NullPointerException(name);
        }
        int len = array.length;
        for (int i = 0; i < len; ++i) {
            if (array[i] == null) {
                throw new NullPointerException("element " + i + " in " + name);
            }
        }
        return array;
    }
    
    public static <T, I extends Iterable<T>> I requireNonNull(I iterable, String name) {
        if (iterable == null) {
            throw new NullPointerException(name);
        }
        Iterator<T> iter = iterable.iterator();
        for (int i = 0; iter.hasNext(); ++i) {
            if (iter.next() == null) {
                throw new NullPointerException("element " + i + " in " + name);
            }
        }
        return iterable;
    }
    
    public static int size(Collection<?> coll) {
        return coll == null ? 0 : coll.size();
    }
    
    public static <T> Set<T> newIdentityHashSet() {
        return Collections.newSetFromMap(new IdentityHashMap<>());
    }
    
    public static <T> Set<T> newWeakHashSet() {
        return Collections.newSetFromMap(new WeakHashMap<>());
    }
    
    public static void join(Thread t) {
        try {
            t.join();
        } catch (InterruptedException x) {
            Log.caught(Misc.class, "join(Thread)", x, false);
        }
    }
    
    public static void sleep(long ms) {
        if (ms > 0) {
            try {
                Thread.sleep(ms);
            } catch (InterruptedException x) {
                Log.caught(Misc.class, "sleep(long)", x, false);
            }
        }
    }
    
    public static void sleepSilently(long millis) {
        long start  = System.nanoTime();
        long remain = millis;
        while (remain > 0) {
            try {
                Thread.sleep(remain);
            } catch (InterruptedException ignored) {
            }
            long elapsed = (System.nanoTime() - start) / 1_000_000L;
            remain       = millis - elapsed;
        }
    }
    
    public static void wait(Object monitor) {
        synchronized (monitor) {
            try {
                monitor.wait();
            } catch (InterruptedException x) {
                Log.caught(Misc.class, "wait(Object)", x, false);
            }
        }
    }
    
    public static void wait(Object monitor, long ms) {
        if (ms > 0) {
            synchronized (monitor) {
                try {
                    monitor.wait(ms);
                } catch (InterruptedException x) {
                    Log.caught(Misc.class, "wait(Object, long)", x, false);
                }
            }
        }
    }
    
    public static <T extends Throwable> T initCause(T x, Throwable cause) {
        x.initCause(cause);
        return x;
    }
    
    private static final Map<Class<?>, Constructor<?>> THROWABLE_CTORS =
        new java.util.concurrent.ConcurrentHashMap<>();
    @SuppressWarnings("unchecked")
    private static <T> Constructor<? extends T> getConstructor(T throwable) {
        Class<? extends T> cls = (Class<? extends T>) throwable.getClass();
        return (Constructor<? extends T>)
                THROWABLE_CTORS.computeIfAbsent(cls, key -> {
            try {
                return cls.getConstructor(String.class, Throwable.class);
            } catch (ReflectiveOperationException x) {
            }
            try {
                return cls.getConstructor(String.class);
            } catch (ReflectiveOperationException x) {
            }
            try {
                return cls.getConstructor();
            } catch (ReflectiveOperationException x) {
            }
            return null;
        });
    }
    
    public static <T extends Throwable> T setMessage(T throwable, Object message) {
        Constructor<? extends T> ctor = getConstructor(throwable);
        
        if (ctor != null) {
            try {
                T copy;
                switch (ctor.getParameterCount()) {
                    case 0:
                        copy = ctor.newInstance();
                        return initCause(copy, throwable);
                    case 1:
                        copy = ctor.newInstance(String.valueOf(message));
                        return initCause(copy, throwable);
                    case 2:
                        copy = ctor.newInstance(String.valueOf(message), throwable);
                        return copy;
                }
            } catch (ReflectiveOperationException x) {
                Log.caught(Misc.class, "setMessage", x, false);
            }
        }
        
        return throwable;
    }
    
    public static <T> T[] append(T[] arr, T elem) {
        int len   = arr.length;
        T[] copy  = Arrays.copyOf(arr, len + 1);
        copy[len] = elem;
        return copy;
    }
    
    public static <T> T[] insert(T[] arr, int index, T elem) {
        int len = arr.length;
        if (index == len)
            return append(arr, elem);
        T[] copy = Arrays.copyOf(arr, len + 1);
        System.arraycopy(arr, index, copy, index + 1, len - index);
        copy[index] = elem;
        return copy;
    }
    
    public static <T> T[] remove(T[] arr, int index) {
        int len  = arr.length;
        T[] copy = Arrays.copyOf(arr, len - 1);
        System.arraycopy(arr, index + 1, copy, index, len - index - 1);
        return copy;
    }
    
    public static <T> T[] remove(T[] arr, T elem) {
        int len = arr.length;
        for (int i = 0; i < len; ++i)
            if (Objects.equals(arr[i], elem))
                return remove(arr, i);
        return arr;
    }
    
    public static PrintStream toStringStream() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        return new PrintStream(out) {
            @Override
            public String toString() {
                return out.toString();
            }
        };
    }
    
    public static String getSimpleName(Class<?> cls) {
        // maybe this should return "null" instead
        Objects.requireNonNull(cls, "cls");
        
        int      dims = 0;
        Class<?> comp = cls;
        
        while (comp.isArray()) {
            comp = comp.getComponentType();
            ++dims;
        }
        
        String name = comp.getSimpleName();
        
        if (name != null && !name.isEmpty()) {
            if (dims == 0) {
                return name;
            }
            return cls.getSimpleName();
        }
        
        // TODO: cache?
        
        name = comp.getName();
        
        int dot = name.lastIndexOf('.');
        if (dot >= 0) {
            name = name.substring(dot + 1);
        }
        
        if (dims > 0) {
            int    len   = name.length();
            char[] chars = new char[len + 2*dims];
            name.getChars(0, len, chars, 0);
            do {
                chars[len++] = '[';
                chars[len++] = ']';
                --dims;
            } while (dims > 0);
            name = new String(chars);
        }
        
        return name;
    }
    
    public static String toSimpleString(Enum<?> e) {
        return getSimpleName(e.getDeclaringClass()) + "." + e.name();
    }
    
    public static String createSimpleMessage(Throwable x) {
        return getSimpleName(x.getClass()) + ": " + x.getMessage();
    }
    
    public static <T> Iterator<T> iteratorOf(Enumeration<? extends T> enumeration) {
        Objects.requireNonNull(enumeration, "enumeration");
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return enumeration.hasMoreElements();
            }
            @Override
            public T next() {
                if (hasNext()) {
                    return enumeration.nextElement();
                }
                throw new NoSuchElementException();
            }
        };
    }
    
    public static <T> Iterator<T> cat(Iterator<? extends T> iterA, Iterator<? extends T> iterB) {
        Objects.requireNonNull(iterA, "iterA");
        Objects.requireNonNull(iterB, "iterB");
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return iterA.hasNext() || iterB.hasNext();
            }
            @Override
            public T next() {
                if (iterA.hasNext()) {
                    return iterA.next();
                }
                if (iterB.hasNext()) {
                    return iterB.next();
                }
                throw new NoSuchElementException();
            }
        };
    }
    
    @SuppressWarnings("unchecked")
    public static <T> Class<T[]> getArrayClass(Class<T> componentType) {
        // the result would not be a Class<T[]>
        if (componentType.isPrimitive()) {
            throw new IllegalArgumentException(componentType.toString());
        }
        Object arr = Array.newInstance(componentType, 0);
        return (Class<T[]>) arr.getClass();
    }
    
    public static Class<?> getArrayClass(Class<?> componentType, int dims) {
        Objects.requireNonNull(componentType, "componentType");
        
        if (dims < 0) {
            // we could also do
            // for (; dims < 0; --dims) {
            //     if (!componentType.isArray()) throw new IllegalArgumentException(...);
            //     componentType = componentType.getComponentType();
            // }
            throw new IllegalArgumentException("dims = " + dims);
        }
        
        if (dims == 0) {
            return componentType;
        }
        if (dims > 256) {
            dims = 256;
        }
        
        Object arr = Array.newInstance(componentType, new int[dims]);
        return arr.getClass();
        
//        Class<?> result = componentType;
//        
//        while (dims > 0) {
//            Object arr = Array.newInstance(result, 0);
//            result = arr.getClass();
//            --dims;
//        }
//        
//        return result;
    }
    
    private static final Class<?>[] EMPTY_PARAMS = new Class<?>[0];
    
    public static Method getDeclaredMethod(Class<?> clazz, String name, Class<?>... params)
            throws NoSuchMethodException {
        if (params == null) {
            params = EMPTY_PARAMS;
        }
        requireNonNull(params, "params");
        NoSuchMethodException thrown = null;
        
        do {
            try {
                Method m = clazz.getDeclaredMethod(name, params);
                m.setAccessible(true);
                return m;
            } catch (NoSuchMethodException x) {
                if (thrown != null) {
                    x.addSuppressed(thrown);
                }
                thrown = x;
            }
        } while ((clazz = clazz.getSuperclass()) != null);
        
        if (thrown == null) {
            thrown = new NoSuchMethodException(clazz.getName() + "." + name + j(Arrays.asList(params), ", ", "(", ")", Class::getName));
        }
        throw thrown;
    }
    
    private static final Pattern DESPACE_PAT = Pattern.compile("\\p{javaWhitespace}+");
    
    @Deprecated
    @SuppressWarnings("fallthrough")
    private static String despace_old(String str) {
        int len = str.length();
        switch (len) {
            case 0:
                return str;
            case 1:
                if (Character.isWhitespace(str.charAt(0))) {
                    return "";
                } else {
                    return str;
                }
            case 2:
                if (str.codePointCount(0, 2) == 1) {
                    if (Character.isWhitespace(str.codePointAt(0))) {
                        return "";
                    } else {
                        return str;
                    }
                } // else fall through
            default:
                return DESPACE_PAT.matcher(str).replaceAll("");
        }
    }
    
    public static String despace(String str) {
        int len = str.length();
        
        if (len == 0) {
            return str;
        }
        if (len == 1) {
            if (Character.isWhitespace(str.charAt(0))) {
                return "";
            }
            return str;
        }
        
        StringBuilder b = null;
        
        int prev = 0;
        
        for (int i = 0; i < len;) {
            int code  = str.codePointAt(i);
            int count = Character.charCount(code);
            
            if (Character.isWhitespace(code)) {
                if (b == null) {
                    b = new StringBuilder(len);
                }
                b.append(str, prev, i);
                i = prev = i + count;
            } else {
                i += count;
            }
        }
        
        return (b == null) ? str : b.append(str, prev, len).toString();
    }
    
    public static String escapeHtml(String text) {
        return eps.block.Block.escapeHtml(text);
    }
    
    public static <T> Predicate<T> not(Predicate<T> p) {
        return p.negate();
    }
    
    @SafeVarargs
    public static <T> boolean allMatch(Predicate<? super T> test, T... values) {
        for (T val : values) {
            if (!test.test(val))
                return false;
        }
        return true;
    }
    
    public static String unquote(String s) {
        if (s != null) {
            int len = s.length();
            if (len >= 2) {
                if (s.startsWith("\"") && s.endsWith("\"")) {
                    s = s.substring(1, len - 1);
                }
            }
        }
        return s;
    }
}