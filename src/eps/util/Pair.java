/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.json.*;

import java.util.*;
import java.util.function.*;
import java.io.Serializable;

@JsonSerializable
public final class Pair<L, R> implements Serializable, Cloneable {
    private static final long serialVersionUID = 0L;
    
    @JsonProperty
    public L left;
    @JsonProperty
    public R right;
    
    @JsonConstructor
    public Pair(L left, R right) {
        this.left  = left;
        this.right = right;
    }
    
    public Pair<L, R> set(L left, R right) {
        this.left  = left;
        this.right = right;
        return this;
    }
    
    public static <L, R> Pair<L, R> of(L left, R right) {
        return new Pair<>(left, right);
    }
    
    public static <L, R> Pair<L, R> empty() {
        return new Pair<>(null, null);
    }
    
    private static final Function<Pair<?, ?>, ?> LEFT  = (Function<Pair<?, ?>, ?> & Serializable) p -> p.left;
    private static final Function<Pair<?, ?>, ?> RIGHT = (Function<Pair<?, ?>, ?> & Serializable) p -> p.right;
    
    @SuppressWarnings("unchecked")
    public static <L, R> Function<Pair<L, R>, L> left() {
        return (Function<Pair<L, R>, L>) (Function<? super Pair<L, R>, ? super L>) LEFT;
    }
    
    @SuppressWarnings("unchecked")
    public static <L, R> Function<Pair<L, R>, R> right() {
        return (Function<Pair<L, R>, R>) (Function<? super Pair<L, R>, ? super R>) RIGHT;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(left) ^ Objects.hashCode(right);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pair<?, ?>) {
            Pair<?, ?> that = (Pair<?, ?>) obj;
            return Objects.equals(this.left,  that.left)
                && Objects.equals(this.right, that.right);
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "(" + left + ", " + right + ")";
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Pair<L, R> clone() {
        try {
            return (Pair<L, R>) super.clone();
        } catch (CloneNotSupportedException x) {
            throw new AssertionError("clone", x);
        }
    }
}