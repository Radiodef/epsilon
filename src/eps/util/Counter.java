/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.math.*;
import java.util.*;
import java.util.function.*;

public final class Counter implements Comparable<Counter>, Supplier<BigInteger> {
    private long       longValue = 0;
    private BigInteger bigValue  = null;
    
    public Counter() {
    }
    
    public Counter(long initialValue) {
        if (initialValue < 0) {
            throw Errors.newIllegalArgument(initialValue);
        }
        this.longValue = initialValue;
    }
    
    public Counter(BigInteger initialValue) {
        Objects.requireNonNull(initialValue, "initialValue");
        
        if (initialValue.signum() < 0) {
            throw Errors.newIllegalArgument(initialValue);
        }
        
        if (initialValue.compareTo(LONG_MAX_VALUE) <= 0) {
            this.longValue = initialValue.longValueExact();
        } else {
            this.longValue = Long.MAX_VALUE;
            this.bigValue  = initialValue;
        }
    }
    
    public boolean isZero() {
        return longValue == 0;
    }
    
    public boolean isBig() {
        return bigValue != null;
    }
    
    @Override
    public BigInteger get() {
        if (bigValue != null) {
            return bigValue;
        }
        return BigInteger.valueOf(longValue);
    }
    
    public Counter increment() {
        long longValue = this.longValue;
        
        if (longValue != Long.MAX_VALUE) {
            this.longValue = longValue + 1;
            
        } else {
            BigInteger bigValue = this.bigValue;
            if (bigValue == null) {
                bigValue = BigInteger.valueOf(longValue);
            }
            
            this.bigValue = bigValue.add(BigInteger.ONE);
        }
        return this;
    }
    
    private static final BigInteger LONG_MAX_VALUE = BigInteger.valueOf(Long.MAX_VALUE);
    
    public Counter decrement() {
        BigInteger bigValue = this.bigValue;
        
        if (bigValue != null) {
            bigValue = bigValue.subtract(BigInteger.ONE);
            
            if (bigValue.equals(LONG_MAX_VALUE)) {
                this.bigValue = null;
                assert longValue == Long.MAX_VALUE : longValue;
            } else {
                this.bigValue = bigValue;
            }
        } else {
            long longValue = this.longValue;
            if (longValue == 0) {
                throw Errors.newIllegalState(longValue);
            }
            this.longValue = longValue - 1;
        }
        
        return this;
    }
    
    @Override
    public int compareTo(Counter that) {
        BigInteger thisBig = this.bigValue;
        BigInteger thatBig = that.bigValue;
        
        if (thisBig != null) {
            return (thatBig == null) ? +1 : thisBig.compareTo(thatBig);
        }
        if (thatBig != null) {
            return -1;
        }
        
        return Long.compare(this.longValue, that.longValue);
    }
    
    @Override
    public int hashCode() {
        return Long.hashCode(longValue) ^ Objects.hashCode(bigValue);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Counter) {
            Counter that = (Counter) obj;
            return (this.longValue == that.longValue) && Objects.equals(this.bigValue, that.bigValue);
        }
        return false;
    }
    
    @Override
    public String toString() {
        BigInteger bigValue = this.bigValue;
        if (bigValue != null) {
            return bigValue.toString();
        } else {
            return Long.toString(longValue);
        }
    }
}