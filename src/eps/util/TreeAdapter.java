/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;
import java.util.function.*;
import java.io.Serializable;

public abstract class TreeAdapter<T, U> implements Serializable {
    private static final long serialVersionUID = 0L;
    
    private Node<T, U> root;
    
    public TreeAdapter() {
    }
    
    public TreeAdapter(T obj) {
        setTree(obj);
    }
    
    public TreeAdapter<T, U> setTree(T obj) {
        if (obj == null) {
            root = null;
        } else {
            root = createNode(obj);
        }
        return this;
    }
    
    public TreeAdapter<T, U> rebuild() {
        root = createNode(root.getObject());
        return this;
    }
    
    public Node<T, U> getRoot() {
        return root;
    }
    
    protected Node<T, U> createNode(T obj) {
        return new Node<>(this, obj);
    }
    
    protected U getValue(T obj) {
        return null;
    }
    
    protected boolean allowsValue(T obj) {
        return true;
    }
    
    protected boolean allowsChildren(T obj) {
        return true;
    }
    
    protected int getChildCount(T obj) {
        return 0;
    }
    
    protected T getChildAt(T obj, int index) {
        throw new IndexOutOfBoundsException(Integer.toString(index));
    }
    
    public void forEach(Consumer<? super U> action) {
        Node<T, U> root = this.root;
        if (root != null) {
            root.forEach(action);
        }
    }
    
    public int valueCount() {
        MutableInt count = new MutableInt();
        forEach(x -> count.increment());
        return count.value;
    }
    
    public List<U> toList() {
        List<U> list = new ArrayList<>();
        forEach(list::add);
        return list;
    }
    
    protected String getEmptyString() {
        return "{}";
    }
    
    @Override
    public String toString() {
        Node<T, U> root = this.root;
        return root != null ? root.toString() : getEmptyString();
    }
    
    public String toString(String indent) {
        return toString(0, indent);
    }
    
    public String toString(int indents, String indent) {
        StringBuilder b = new StringBuilder();
        Node<T, U> root = this.root;
        if (root != null) {
            root.toString(b, indents, indent);
        } else {
            for (; indents > 0; --indents)
                b.append(indent);
            b.append(getEmptyString());
        }
        return b.toString();
    }
    
    @Override
    public int hashCode() {
        return hashCode(root);
    }
    
    protected int hashCode(Node<T, U> node) {
        int hash = 0;
        if (node != null) {
            if (node.allowsValue()) {
                hash = Objects.hashCode(node.getValue());
            }
            if (node.allowsChildren()) {
                int count = node.childCount();
                for (int i = 0; i < count; ++i) {
                    hash = 31 * hash + hashCode(node.getChildAt(i));
                }
            }
        }
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj instanceof TreeAdapter<?, ?>) {
            TreeAdapter<?, ?> that = (TreeAdapter<?, ?>) obj;
            return equals(this.getRoot(), that.getRoot());
        }
        return false;
    }
    
    protected boolean equals(Node<T, U> lhs, Node<?, ?> rhs) {
        if (lhs == null || rhs == null) {
            return lhs == rhs;
        }
        if (lhs.allowsValue()) {
            if (!rhs.allowsValue())
                return false;
            if (!Objects.equals(lhs.getValue(), rhs.getValue()))
                return false;
        }
        if (lhs.allowsChildren()) {
            if (!rhs.allowsChildren())
                return false;
            int count = lhs.childCount();
            if (count != rhs.childCount())
                return false;
            for (int i = 0; i < count; ++i)
                if (!equals(lhs.getChildAt(i), rhs.getChildAt(i)))
                    return false;
        }
        return true;
    }
    
    public static class Node<T, U> implements Serializable {
        private static final long serialVersionUID = 0L;
        
        private final TreeAdapter<T, U> tree;
        
        private Node<T, U> parent;
        
        private T obj;
        
        private List<Node<T, U>> children;
        
        protected Node(TreeAdapter<T, U> tree, T obj) {
            this.tree = Objects.requireNonNull(tree, "tree");
            setObject(obj);
        }
        
        public T getObject() {
            return obj;
        }
        
        public void setObject(T obj) {
            Objects.requireNonNull(obj, "obj");
            this.obj = obj;
            
            List<Node<T, U>> children = this.children;
            
            if (children != null) {
                int count = children.size();
                for (int i = 0; i < count; ++i) {
                    children.get(i).setParent(null);
                }
                children.clear();
            }
            
            TreeAdapter<T, U> tree = this.tree;
            
            if (tree.allowsChildren(obj)) {
                int count = tree.getChildCount(obj);
                
                if (children == null) {
                    children = this.children = new ArrayList<>(count);
                }
                
                for (int i = 0; i < count; ++i) {
                    T          child = tree.getChildAt(obj, i);
                    Node<T, U> node  = tree.createNode(child);
                    node.setParent(this);
                    children.add(node);
                }
            }
        }
        
        public Node<T, U> getRoot() {
            Node<T, U> node = this;
            for (Node<T, U> parent; (parent = node.getParent()) != null;)
                node = parent;
            return node;
        }
        
        public boolean isRoot() {
            return getParent() == null;
        }
        
        public Node<T, U> getParent() {
            return parent;
        }
        
        protected void setParent(Node<T, U> parent) {
            this.parent = parent;
        }
        
        public boolean allowsValue() {
            return tree.allowsValue(getObject());
        }
        
        public U getValue() {
            if (allowsValue()) {
                return tree.getValue(getObject());
            }
            return null;
        }
        
        public boolean allowsChildren() {
            return tree.allowsChildren(getObject());
        }
        
        public boolean isEmpty() {
            return childCount() == 0;
        }
        
        public int childCount() {
            List<Node<T, U>> children = this.children;
            return (children == null) ? 0 : children.size();
        }
        
        public List<Node<T, U>> getChildren() {
            List<Node<T, U>> children = this.children;
            if (children == null || children.isEmpty()) {
                return Collections.emptyList();
            } else {
                return Collections.unmodifiableList(children);
            }
        }
        
        public Node<T, U> getChildAt(int i) {
            List<Node<T, U>> children = this.children;
            if (children == null) {
                children = Collections.emptyList();
            }
            return children.get(i);
        }
        
        public void forEach(Consumer<? super U> action) {
            if (allowsChildren()) {
                List<Node<T, U>> children = this.children;
                if (children != null) {
                    int count = children.size();
                    for (int i = 0; i < count; ++i) {
                        children.get(i).forEach(action);
                    }
                }
            }
            if (allowsValue()) {
                action.accept(getValue());
            }
        }
        
        @Override
        public String toString() {
            StringBuilder b = new StringBuilder();
            toString(b);
            return b.toString();
        }
        
        protected void toString(StringBuilder b) {
            b.append('{');
            if (allowsValue()) {
                b.append("value = ").append(getValue());
                if (allowsChildren()) {
                    b.append(", ");
                }
            }
            if (allowsChildren()) {
                b.append("children = [");
                List<Node<T, U>> children = this.children;
                if (children != null) {
                    int count = children.size();
                    for (int i = 0; i < count; ++i) {
                        if (i > 0) b.append(", ");
                        children.get(i).toString(b);
                    }
                }
                b.append("]");
            }
            b.append('}');
        }
        
        public String toString(String indent) {
            return toString(0, indent);
        }
        
        public String toString(int indents, String indent) {
            StringBuilder b = new StringBuilder();
            toString(new StringBuilder(), indents, indent);
            return b.toString();
        }
        
        protected void indent(StringBuilder b, int count, String indent) {
            while (count > 0) {
                b.append(indent);
                --count;
            }
        }
        
        protected void toString(StringBuilder b, int indents, String indent) {
            indent(b, indents, indent);
            b.append('{');
            
            boolean ln = false;
            
            if (allowsValue()) {
                indent(b.append('\n'), indents + 1, indent);
                ln = true;
                
                if (allowsChildren()) {
                //  b.append("children = ")
                    b.append("value    = ");
                } else {
                    b.append("value = ");
                }
                b.append(getValue()).append(';');
            }
            
            if (allowsChildren()) {
                indent(b.append('\n'), indents + 1, indent);
                ln = true;
                
                b.append("children = [");
                
                List<Node<T, U>> children = this.children;
                int size = 0;
                if (children != null) {
                    size = children.size();
                    if (size > 0) {
                        for (int i = 0; i < size; ++i) {
                            b.append('\n');
                            children.get(i).toString(b, indents + 2, indent);
                            b.append(',');
                        }
                    }
                }
                
                if (size > 0) {
                    indent(b.append('\n'), indents + 1, indent);
                }
                b.append("];");
            }
            
            if (ln) {
                indent(b.append('\n'), indents, indent);
            }
            b.append('}');
        }
    }
}