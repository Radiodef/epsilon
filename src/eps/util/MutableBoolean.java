/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.function.*;

public final class MutableBoolean implements BooleanSupplier, Comparable<MutableBoolean>, Cloneable {
    public boolean value;
    
    public MutableBoolean() {
    }
    
    public MutableBoolean(boolean value) {
        this.value = value;
    }
    
    public MutableBoolean set(boolean value) {
        this.value = value;
        return this;
    }
    
    public MutableBoolean or(boolean value) {
        this.value |= value;
        return this;
    }
    
    public MutableBoolean and(boolean value) {
        this.value &= value;
        return this;
    }
    
    public boolean getAndSet(boolean value) {
        boolean before = this.value;
        this.value     = value;
        return before;
    }
    
    public boolean setAndGet(boolean value) {
        return this.value = value;
    }
    
    public boolean is(boolean value) {
        return this.value == value;
    }
    
    @Override
    public boolean getAsBoolean() {
        return value;
    }
    
    @Override
    public int compareTo(MutableBoolean that) {
        return Boolean.compare(this.value, that.value);
    }
    
    @Override
    public int hashCode() {
        return Boolean.hashCode(value);
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof MutableBoolean) && ((MutableBoolean) obj).value == this.value;
    }
    
    @Override
    public String toString() {
        return Boolean.toString(value);
    }
    
    @Override
    public MutableBoolean clone() {
        try {
            return (MutableBoolean) super.clone();
        } catch (CloneNotSupportedException x) {
            throw new AssertionError("clone", x);
        }
    }
}