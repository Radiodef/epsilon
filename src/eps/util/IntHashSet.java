/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.json.*;

import java.util.*;

public class IntHashSet extends AbstractSet<Integer> implements IntSet {
    private final IntMap<Boolean> map;
    
    public IntHashSet() {
        this.map = new IntHashMap<>();
    }
    
    public IntHashSet(int initialCapacity) {
        this.map = new IntHashMap<>(initialCapacity);
    }
    
    public IntHashSet(Collection<? extends Integer> toCopy) {
        Objects.requireNonNull(toCopy, "toCopy");
        
        IntMap<Boolean> map = new IntHashMap<>(toCopy.size());
        
        for (Integer i : toCopy) {
            map.put(i, Boolean.TRUE);
        }
        
        this.map = map;
    }
    
    public IntHashSet(String json) {
        Throwable cause = null;
        try {
            List<?> list = (List<?>) Json.objectify(json);
            
            IntMap<Boolean> map = new IntHashMap<>(list.size());
            
            for (Object e : list) {
                map.put(((Number) e).intValue(), Boolean.TRUE);
            }
            
            this.map = map;
            return;
        } catch (IllegalArgumentException | ClassCastException | NullPointerException x) {
            cause = x;
        }
        throw new IllegalArgumentException(json.replaceAll("\\s+", " "), cause);
    }
    
    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }
    
    @Override
    public int size() {
        return map.size();
    }
    
    @Override
    public void clear() {
        map.clear();
    }
    
    @Override
    public boolean add(Integer val) {
        return this.addAsInt(val);
    }
    
    @Override
    public boolean addAsInt(int val) {
        return map.putAsInt(val, Boolean.TRUE) == null;
    }
    
    @Override
    public boolean remove(Object obj) {
        return this.removeAsInt((Integer) obj);
    }
    
    @Override
    public boolean removeAsInt(int val) {
        return map.removeAsInt(val) != null;
    }
    
    @Override
    public boolean containsAsInt(int val) {
        return map.containsKeyAsInt(val);
    }
    
    @Override
    public PrimitiveIterator.OfInt iterator() {
        return map.keySet().iterator();
    }
    
//    @Override
//    public PrimitiveIterator.OfInt iterator() {
//        return new PrimitiveIterator.OfInt() {
//            private final Iterator<Map.Entry<Integer, Boolean>> it = map.entrySet().iterator();
//            
//            @Override
//            public boolean hasNext() {
//                return it.hasNext();
//            }
//            
//            @Override
//            public int nextInt() {
//                return ((IntMap.Entry<Boolean>) it.next()).getKeyAsInt();
//            }
//            
//            @Override
//            public void remove() {
//                it.remove();
//            }
//        };
//    }
}