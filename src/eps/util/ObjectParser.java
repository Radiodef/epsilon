/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.json.*;
import static eps.util.Literals.*;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;

@JsonSerializable
public abstract class ObjectParser<T> {
    @JsonProperty
    private final Class<T> type;
    
    @JsonConstructor
    protected ObjectParser(Class<T> type) {
        this.type = Objects.requireNonNull(type, "type");
        if (type.isPrimitive()) {
            throw Errors.newIllegalArgument(type);
        }
    }
    
    public final Class<T> getType() {
        return this.type;
    }
    
    public final ObjectParser<T> requiring(Predicate<? super T> test) {
        return new AfterPredicateParser<>(this, test);
    }
    
    public final ObjectParser<T> nullable() {
        return new NullableParser<>(this);
    }
    
    public final <C extends Collection<T>> ObjectParser<C> asCollection(Supplier<? extends C> supplier) {
        return new JsonCollectionParser<>(this, supplier);
    }
    
    public abstract T parse(String text);
    
    @Override
    public String toString() {
        String name = type.getCanonicalName();
        if (name == null) {
            name = type.getName();
        }
        return "ObjectParser<" + name + ">";
    }
    
    @SuppressWarnings("unchecked")
    public static <T> ObjectParser<?> put(ObjectParser<T> parser) {
        Objects.requireNonNull(parser, "parser");
        return (ObjectParser<T>) PARSERS.put(parser.getType(), parser);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> ObjectParser<T> get(Class<T> type) {
        return (ObjectParser<T>) PARSERS.get(type);
    }
    
    public static <T> ObjectParser<T> ofSimple(Class<T> type) {
        return ObjectParser.ofSimple(type, (String) null);
    }
    
    public static <T> ObjectParser<T> ofSimple(Class<T> type, String staticMethod) {
        Objects.requireNonNull(type, "type");
        if (staticMethod == null) {
            @SuppressWarnings("unchecked")
            ObjectParser<T> p = (ObjectParser<T>) SIMPLE_PARSERS.get(type);
            if (p == null) {
                p = new SimpleParser<>(type, null);
                SIMPLE_PARSERS.put(type, p);
            }
            return p;
        }
        return new SimpleParser<>(type, staticMethod);
    }
    
    public static ObjectParser<Boolean> ofBoolean() {
        return BooleanParser.INSTANCE;
    }
    
    public static ObjectParser<String> ofString() {
        return StringParser.INSTANCE;
    }
    
    @SuppressWarnings("unchecked")
    public static <T> ObjectParser<T> alwaysNull() {
        return (ObjectParser<T>) AlwaysNullParser.INSTANCE;
    }
    
    public static ObjectParser<Class<?>> ofClass() {
        return ClassParser.INSTANCE;
    }
    
    public static <T> ObjectParser<Class<? extends T>> ofSubtypesOf(Class<T> type) {
        return new SubtypeParser<>(type);
    }
    
    private static final Map<Class<?>, ObjectParser<?>> SIMPLE_PARSERS = new ConcurrentHashMap<>();
    
    private static final Map<Class<?>, ObjectParser<?>> PARSERS = new ConcurrentHashMap<>();
    static {
        put(BooleanParser.INSTANCE);
        put(ClassParser.INSTANCE);
        
        for (Class<?> type : ListOf(Byte.class,
                                    Short.class,
                                    Integer.class,
                                    Long.class,
                                    Float.class,
                                    Double.class)) {
            put(new SimpleParser<>(type, (String) null));
        }
    }
    
    @JsonSerializable
    private static final class SimpleParser<T> extends ObjectParser<T> {
        private final Function<String, ? extends T> fn;
        
        @JsonProperty
        private final String staticMethod;
        
        @JsonConstructor
        private SimpleParser(Class<T> type, String staticMethod) {
            super(type);
            if (staticMethod == null) {
                this.fn = Misc.createConstructorFunction(type, String.class);
            } else {
                this.fn = Misc.createStaticFunction(type, staticMethod, String.class, type);
            }
            this.staticMethod = staticMethod;
        }
        
        @Override
        public T parse(String text) {
            try {
                return fn.apply(text);
            } catch (IllegalArgumentException x) {
                throw new BadStringException(text, x);
            }
        }
    }
    
    private static final class BooleanParser extends ObjectParser<Boolean> {
        private static final BooleanParser INSTANCE = new BooleanParser();
        
        static {
            ToStringConverter.put(
                ToStringConverter.create(
                    BooleanParser.class,
                    ignored -> "",
                    ignored -> INSTANCE
                )
            );
        }
        
        private BooleanParser() {
            super(Boolean.class);
        }
        
        private static final Set<String> TRUE_STRINGS =
            TreeSetOf(String.CASE_INSENSITIVE_ORDER, "true", "t", "yes", "y", "on", "enable", "enabled", "1");
        private static final Set<String> FALSE_STRINGS =
            TreeSetOf(String.CASE_INSENSITIVE_ORDER, "false", "f", "no", "n", "off", "disable", "disabled", "0", "null", "undefined", "");
        static {
            assert Collections.disjoint(TRUE_STRINGS, FALSE_STRINGS)
                 : TRUE_STRINGS + ", " + FALSE_STRINGS;
        }
        
        @Override
        public Boolean parse(String text) {
            text = text.trim();
            
            if (TRUE_STRINGS.contains(text))
                return Boolean.TRUE;
            if (FALSE_STRINGS.contains(text))
                return Boolean.FALSE;
            
            throw new BadStringException(text);
        }
    }
    
    private static final class ClassParser extends ObjectParser<Class<?>> {
        private static final ClassParser INSTANCE = new ClassParser();
        
        static {
            ToStringConverter.put(
                ToStringConverter.create(
                    ClassParser.class,
                    ignored -> "",
                    ignored -> INSTANCE
                )
            );
        }
        
        private ClassParser() {
            super(Misc.WILD_CLASS);
        }
        
        @Override
        public Class<?> parse(String name) {
            try {
                return Misc.getClass(name);
            } catch (IllegalArgumentException x) {
                throw new BadStringException(name, x);
            }
        }
    }
    
    private static final class StringParser extends ObjectParser<String> {
        private static final StringParser INSTANCE = new StringParser();
        
        static {
            ToStringConverter.put(
                ToStringConverter.create(
                    StringParser.class,
                    ignored -> "",
                    ignored -> INSTANCE
                )
            );
        }
        
        private StringParser() {
            super(String.class);
        }
        
        @Override
        public String parse(String string) {
            return Objects.requireNonNull(string, "string");
        }
    }
    
    private static final class AlwaysNullParser extends ObjectParser<Object> {
        private static final AlwaysNullParser INSTANCE = new AlwaysNullParser();
        
        static {
            ToStringConverter.put(
                ToStringConverter.create(
                    AlwaysNullParser.class,
                    ignored -> "",
                    ignored -> INSTANCE
                )
            );
        }
        
        private AlwaysNullParser() {
            super(Object.class);
        }
        
        @Override
        public String parse(String text) {
            Objects.requireNonNull(text, "text");
            return null;
        }
    }
    
    @JsonSerializable
    private static final class SubtypeParser<T> extends ObjectParser<Class<? extends T>> {
        @JsonProperty
        private final Class<T> min;
        
        @JsonConstructor
        private SubtypeParser(Class<?> type, Class<T> min) {
            this(min);
        }
        
        @SuppressWarnings("unchecked")
        private SubtypeParser(Class<T> min) {
            super((Class<Class<? extends T>>) (Class<? super Class<? extends T>>) Class.class);
            this.min = Objects.requireNonNull(min, "min");
        }
        
        @Override
        public Class<? extends T> parse(String name) {
            try {
                Class<?> clazz = ClassParser.INSTANCE.parse(name);
                return clazz.asSubclass(min);
            } catch (ClassCastException x) {
                throw new BadStringException(name, x);
            }
        }
    }
    
    @JsonSerializable
    private static final class JsonCollectionParser<T, C extends Collection<T>> extends ObjectParser<C> {
        @JsonProperty
        private final ObjectParser<T> elemParser;
        @JsonProperty
        private final Supplier<? extends C> supplier;
        
        @SuppressWarnings("unchecked")
        private JsonCollectionParser(ObjectParser<T> elemParser, Supplier<? extends C> supplier) {
            this((Class<C>) supplier.get().getClass(), elemParser, supplier);
        }
        
        @JsonConstructor
        private JsonCollectionParser(Class<C> collType, ObjectParser<T> elemParser, Supplier<? extends C> supplier) {
            super(collType);
            this.elemParser = Objects.requireNonNull(elemParser, "elemParser");
            this.supplier   = Objects.requireNonNull(supplier, "suppler");
        }
        
        @Override
        public C parse(String text) {
            try {
                Iterable<?> elems = (Iterable<?>) Json.objectify(text);
                if (elems == null) {
                    return null;
                }
                
                C c = supplier.get();
                
                for (Object elem : elems) {
                    c.add(elemParser.parse(String.valueOf(elem)));
                }
                
                return c;
            } catch (RuntimeException x) {
                throw new BadStringException(text, x);
            }
        }
    }
    
    @JsonSerializable
    private static final class AfterPredicateParser<T> extends ObjectParser<T> {
        @JsonProperty
        private final ObjectParser<T> parser;
        @JsonProperty
        private final Predicate<? super T> test;
        
        @JsonConstructor
        private AfterPredicateParser(Class<T> type, ObjectParser<T> parser, Predicate<? super T> test) {
            this(parser, test);
        }
        
        private AfterPredicateParser(ObjectParser<T> parser, Predicate<? super T> test) {
            super(parser.getType());
            this.parser = parser;
            this.test   = Objects.requireNonNull(test,   "test");
        }
        
        @Override
        public T parse(String text) {
            T obj = parser.parse(text);
            if (test.test(obj)) {
                return obj;
            } else {
                throw new BadStringException(text);
            }
        }
    }
    
    @JsonSerializable
    private static final class NullableParser<T> extends ObjectParser<T> {
        @JsonProperty
        private final ObjectParser<T> parser;
        
        @JsonConstructor
        private NullableParser(Class<T> type, ObjectParser<T> parser) {
            this(parser);
        }
        
        private NullableParser(ObjectParser<T> parser) {
            super(parser.getType());
            this.parser = parser;
        }
        
        @Override
        public T parse(String text) {
            if (text == null || text.trim().equals("null")) {
                return null;
            }
            return parser.parse(text);
        }
    }
    
    public static final class BadStringException extends IllegalArgumentException {
        private BadStringException(String text) {
            super(text);
        }
        private BadStringException(String text, Throwable cause) {
            super(text, cause);
        }
        public BadStringException setMessage(String message) {
            return new BadStringException(message, this);
        }
        public BadStringException setMessage(String fmt, Object... args) {
            return this.setMessage(String.format(fmt, args));
        }
    }
}