/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

public final class PreinitialConsumer<T> implements Consumer<T> {
    private static final class InitialQueue<T> implements Consumer<T> {
        private volatile List<T> list = new CopyOnWriteArrayList<>();
        @Override
        public void accept(T obj) {
            assert list != null;
            list.add(obj);
        }
    }
    
    private final InitialQueue<T> queue;
    private final AtomicReference<Consumer<? super T>> consumer;
    
    public PreinitialConsumer() {
        queue    = new InitialQueue<>();
        consumer = new AtomicReference<>(queue);
    }
    
    public void set(Consumer<? super T> action) {
        Objects.requireNonNull(action, "action");
        if (consumer.compareAndSet(queue, action)) {
            Throwable caught = null;
            try {
                for (T obj : queue.list) {
                    try {
                        action.accept(obj);
                    } catch (Throwable x) {
                        if (caught != null) {
                            x.addSuppressed(caught);
                        }
                        caught = x;
                        if (x instanceof Error && !(x instanceof AssertionError)) {
                            break;
                        }
                    }
                }
            } finally {
                queue.list = null;
                if (caught != null) {
                    Misc.rethrow(caught);
                }
            }
        } else {
            throw new IllegalStateException("consumer already set");
        }
    }
    
    @Override
    public void accept(T obj) {
        consumer.get().accept(obj);
    }
}