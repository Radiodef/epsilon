/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.util.Misc.*;

import java.util.*;
import java.util.function.*;

public final class ToString<T> implements Function<T, String> {
    private final Class<T> type;
    private final String typeName;
    private final List<Pair<String, Function<? super T, ?>>> elements;
    private final int maxWidth;
    
    private ToString(Class<T> type, List<Pair<String, Function<? super T, ?>>> elements) {
        this.type     = type;
        this.typeName = Misc.getSimpleName(type);
        this.elements = elements;
        this.maxWidth = elements.stream().map(Pair.left()).mapToInt(String::length).max().orElse(0);
    }
    
    public Class<T> getType() {
        return type;
    }
    
    public String getTypeName() {
        return typeName;
    }
    
    public String toString(T obj) {
        return apply(obj);
    }
    
    @Override
    public String apply(T obj) {
        return apply(obj, false);
    }
    
    public String apply(T obj, boolean multiline) {
        return apply(obj, multiline, 0);
    }
    
    public String apply(T obj, int spaces) {
        return apply(obj, true, spaces);
    }
    
    public String apply(T obj, boolean multiline, int spaces) {
        if (obj == null)
            return "null";
        StringBuilder b = new StringBuilder(32);
        
        if (spaces > 0) {
            for (int s = 0; s < spaces; ++s) {
                b.append(' ');
            }
        }
        
        b.append(typeName).append(multiline ? " {" : "{");
        
        int i = 0;
        for (Pair<String, Function<? super T, ?>> e : elements) {
            if (i++ != 0) {
                b.append(multiline ? "," : ", ");
            }
            if (multiline) {
                b.append("\n");
                
                if (spaces > 0) {
                    for (int s = 0; s < spaces; ++s) {
                        b.append(' ');
                    }
                }
                
                b.append("  ");
            }
            b.append(e.left);
            if (multiline) {
                for (int dif = maxWidth - e.left.length(); dif > 0; --dif) {
                    b.append(' ');
                }
            }
            b.append(" = ");
            b.append(e.right.apply(obj));
        }
        
        if (multiline && i != 0) {
            b.append("\n");
            
            if (spaces > 0) {
                for (int s = 0; s < spaces; ++s) {
                    b.append(' ');
                }
            }
        }
        
        return b.append("}").toString();
    }
    
    @SuppressWarnings("unchecked")
    public static final Class<ToString<?>> WILD_CLASS =
        (Class<ToString<?>>) (Class<? super ToString<?>>) ToString.class;
    
    private static final ToString<ToString<?>> TO_STRING =
        ToString.builder(ToString.WILD_CLASS)
                .add("type",     ToString::getType)
                .add("typeName", ToString::getTypeName)
                .add("maxWidth", ts -> ts.maxWidth)
                .add("elements", ts -> j(ts.elements, ", ", "[", "]", Pair.left()))
                .build();
    
    @Override
    public String toString() {
        return TO_STRING.apply(this);
    }
    
    public <U extends T> Builder<U> derive(Class<U> type) {
        Builder<U> b = builder(type);
        for (Pair<String, Function<? super T, ?>> e : elements) {
            b.add(e.left, e.right);
        }
        return b;
    }
    
    public static <T> Builder<T> builder(Class<T> type) {
        return new Builder<>(type);
    }
    
    public static final class Builder<T> {
        private final Class<T> type;
        private final List<Pair<String, Function<? super T, ?>>> elements = new ArrayList<>();
        
        private Builder(Class<T> type) {
            this.type = Objects.requireNonNull(type, "type");
            if (type.isPrimitive()) {
                throw Errors.newIllegalArgument(type);
            }
        }
        
        public Builder<T> add(String name, Function<? super T, ?> fn) {
            Objects.requireNonNull(name, "name");
            Objects.requireNonNull(fn,   "fn");
            elements.add(Pair.of(name, fn));
            return this;
        }
        
        public ToString<T> build() {
            return new ToString<>(type, new ArrayList<>(elements));
        }
    }
}