/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.json.*;

import java.util.*;
import java.util.function.*;
import java.io.Serializable;

public final class CodePoint implements CharSequence, Comparable<CodePoint>, IntSupplier, Serializable {
    private static final long serialVersionUID = 1L;
    
    private final int code;
    
    public CodePoint(int code) {
        this.code = code;
    }
    
    public CodePoint(String code) {
        Objects.requireNonNull(code, "code");
        
        int length = code.length();
        if (length >= 0) {
            int code0 = code.codePointAt(0);
            if (Character.charCount(code0) == length) {
                this.code = code0;
                return;
            }
        }
        
        throw new IllegalArgumentException(code);
    }
    
    public static CodePoint valueOf(int code) {
        return new CodePoint(code);
    }
    
    public int intValue() {
        return code;
    }
    
    @Override
    public int getAsInt() {
        return code;
    }
    
    @Override
    public int compareTo(CodePoint that) {
        return Integer.compare(this.code, that.code);
    }
    
    @Override
    public int length() {
        return Character.charCount(code);
    }
    
    @Override
    public char charAt(int index) {
        int code  = this.code;
        int count = Character.charCount(code);
        switch (count) {
            case 1:
                if (index == 0) {
                    return (char) code;
                }
                break;
            case 2:
                if (index == 0) {
                    return Character.highSurrogate(code);
                }
                if (index == 1) {
                    return Character.lowSurrogate(code);
                }
                break;
            default:
                throw new AssertionError(this);
        }
        throw Errors.newIndexOutOfBounds("index", index, 0, count);
    }
    
    @Override
    public CharSequence subSequence(int start, int end) {
        return toString().subSequence(start, end);
    }
    
    public static boolean equals(CodePoint lhs, int code) {
        return lhs != null && lhs.code == code;
    }
    
    public boolean codeEquals(int code) {
        return this.code == code;
    }
    
    public boolean contentEquals(CharSequence seq) {
        int code = this.code;
        int len  = Character.charCount(code);
        if (len == seq.length()) {
            return code == Character.codePointAt(seq, 0);
        }
        return false;
    }
    
    @Override
    public String toString() {
        return new String(Character.toChars(code));
    }
    
    @Override
    public int hashCode() {
        return code;
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof CodePoint) && ((CodePoint) obj).code == this.code;
    }
    
    static {
        ToStringConverter<CodePoint> converter =
            ToStringConverter.create(
                CodePoint.class,
                CodePoint::toString,
                CodePoint::new);
        ToStringConverter.put(converter);
    }
}