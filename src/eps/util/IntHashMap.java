/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

//import eps.math.*;

import java.util.*;

public class IntHashMap<V> extends AbstractMap<Integer, V> implements IntMap<V> {
    public static final int    MAX_TABLE_LENGTH         = 1 << 30;
    public static final int    DEFAULT_INITIAL_CAPACITY = 16;
    public static final double DEFAULT_LOAD_FACTOR      = 0.75;
    public static final double MIN_LOAD_FACTOR          = 0.0625;
    
    private final double loadFactor;
    
    private EntryImpl<V>[] table;
    
    private int size;
    
    public IntHashMap() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
    }
    
    public IntHashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }
    
    public IntHashMap(int initialCapacity, double loadFactor) {
        if (Double.isNaN(loadFactor)) {
            throw new IllegalArgumentException(Double.toString(loadFactor));
        }
        if (loadFactor < MIN_LOAD_FACTOR) {
            loadFactor = MIN_LOAD_FACTOR;
        }
        
        this.loadFactor = loadFactor;
        
        if (initialCapacity < 2) {
            initialCapacity = 2;
        } else if (initialCapacity > MAX_TABLE_LENGTH) {
            throw new OutOfMemoryError(Integer.toString(initialCapacity));
        } else {
            initialCapacity = eps.math.Math2.next2(initialCapacity);
        }
        
        this.table = newUncheckedArray(initialCapacity);
    }
    
    private boolean ensureCapacity(int additionalSize) {
        if (additionalSize > 0) {
            EntryImpl<V>[] oldTable  = this.table;
            int            oldLength = oldTable.length;
            int            oldSize   = size;
            long           newSize   = (long) oldSize + (long) additionalSize;
            if (newSize > (oldLength * loadFactor)) {
                int newLength = oldLength << 1;
                
                if (newLength < newSize) {
                    if (newSize > MAX_TABLE_LENGTH) {
                        throw new OutOfMemoryError(Long.toString(newSize));
                    }
                    newLength = eps.math.Math2.next2((int) newSize);
                }
                
                EntryImpl<V>[] newTable = newUncheckedArray(newLength);
                
                // rehash
                int count = 0;
                for (int i = 0; (i < oldLength) && (count < oldSize); ++i) {
                    EntryImpl<V> e = oldTable[i];
                    while (e != null) {
                        EntryImpl<V> next = e.next;
                        e.next = null;
                        
                        int index = indexOf(e.key, newLength);
                        EntryImpl<V> bucket = newTable[index];
                        if (bucket == null) {
                            newTable[index] = e;
                        } else {
                            bucket.tail().next = e;
                        }
                        
                        ++count;
                        e = next;
                    }
                }
                
                table = newTable;
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean isEmpty() {
        return size == 0;
    }
    
    @Override
    public int size() {
        return size;
    }
    
    public int capacity() {
        return table.length;
    }
    
    @Override
    public void clear() {
        Arrays.fill(table, null);
        size = 0;
    }
    
    @Override
    public V get(Object key) {
        return getAsInt((Integer) key);
    }
    
    @Override
    public V getAsInt(int key) {
        EntryImpl<V>[] table = this.table;
        
        EntryImpl<V> e = table[indexOf(key, table.length)];
        if (e != null) {
            do {
                if (e.key == key)
                    return e.val;
            } while ((e = e.next) != null);
        }
        
        return null;
    }
    
    @Override
    public V put(Integer key, V val) {
        return putAsInt(key, val);
    }
    
    private V putAsInt(int key, V val, int additionalSize) {
        EntryImpl<V>[] table = this.table;
        
        int index = indexOf(key, table.length);
        EntryImpl<V> e = table[index];
        
        if (e == null) {
            if (ensureCapacity(additionalSize)) {
                return putAsInt(key, val, 0);
            } else {
                table[index] = new EntryImpl<>(key, val);
                ++size;
            }
        } else {
            for (;;) {
                if (e.key == key) {
                    return e.setValue(val);
                }
                EntryImpl<V> next = e.next;
                if (next == null) {
                    if (ensureCapacity(additionalSize)) {
                        return putAsInt(key, val, 0);
                    } else {
                        e.next = new EntryImpl<>(key, val);
                        ++size;
                        break;
                    }
                }
                e = next;
            }
        }
        
        return null;
    }
    
    @Override
    public V putAsInt(int key, V val) {
        return putAsInt(key, val, 1);
    }
    
    @Override
    public V putIfAbsentAsInt(int key, V val) {
        EntryImpl<V>[] table = this.table;
        
        int index = indexOf(key, table.length);
        
        for (EntryImpl<V> e = table[index]; e != null; e = e.next) {
            if (e.key == key)
                return e.val;
        }
        
        return putAsInt(key, val);
    }
    
    @Override
    public V putIfAbsent(Integer key, V val) {
        return putIfAbsentAsInt(key, val);
    }
    
    @Override
    public V remove(Object obj) {
        return removeAsInt((Integer) obj);
    }
    
    @Override
    public V removeAsInt(int key) {
        EntryImpl<V>[] table = this.table;
        
        int index = indexOf(key, table.length);
        
        EntryImpl<V> e = table[index];
        if (e != null) {
            EntryImpl<V> prev = null;
            do {
                if (e.key == key) {
                    if (prev == null) {
                        table[index] = e.next;
                    } else {
                        prev.next = e.next;
                    }
                    
                    --size;
                    return e.val;
                }
                
                prev = e;
                e = e.next;
            } while (e != null);
        }
        
        return null;
    }
    
    @Override
    public boolean containsKeyAsInt(int key) {
        EntryImpl<V>[] table = this.table;
        
        int index = indexOf(key, table.length);
        for (EntryImpl<V> e = table[index]; e != null; e = e.next) {
            if (e.key == key) {
                return true;
            }
        }
        
        return false;
    }
    
    @Override
    public boolean containsKey(Object key) {
        return containsKeyAsInt((Integer) key);
    }
    
    public double calculateLoad() {
        int collisions = countCollisions();
        return (double) collisions / (double) size;
    }
    
    public int countCollisions() {
        int count = 0;
        
        EntryImpl<V>[] table  = this.table;
        int            length = table.length;
        
        for (int i = 0; i < length; ++i) {
            EntryImpl<V> e = table[i];
            
            if (e != null) {
                for (;;) {
                    EntryImpl<V> next = e.next;
                    if (next == null)
                        break;
                    e = next;
                    ++count;
                }
            }
        }
        
        return count;
    }
    
    private static int indexOf(int key, int tableLength) {
        return key & (tableLength - 1);
    }
    
    @SuppressWarnings("unchecked")
    private static <V> EntryImpl<V>[] newUncheckedArray(int length) {
        try {
            return (EntryImpl<V>[]) new EntryImpl<?>[length];
        } catch (OutOfMemoryError err) {
            // maybe a bit risky
            try {
                throw Misc.initCause(new OutOfMemoryError(Integer.toString(length)), err);
            } catch (OutOfMemoryError err2) {
                throw err;
            }
        }
    }
    
    static final class EntryImpl<V> implements IntMap.Entry<V> {
        private final int key;
        private V val;
        
        EntryImpl<V> next;
        
        EntryImpl(int key, V val) {
            this.key = key;
            this.val = val;
        }
        
        EntryImpl<V> tail() {
            EntryImpl<V> e = this;
            for (;;) {
                EntryImpl<V> next = e.next;
                if (next == null)
                    return e;
                e = next;
            }
        }
        
        @Override
        public int getKeyAsInt() {
            return key;
        }
        
        @Override
        public V getValue() {
            return val;
        }
        
        @Override
        public V setValue(V val) {
            V old = this.val;
            this.val = val;
            return old;
        }
        
        @Override
        public int hashCode() {
            return key ^ Objects.hashCode(val);
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Map.Entry<?, ?>) {
                if (obj instanceof IntMap.Entry<?>) {
                    if (((IntMap.Entry<?>) obj).getKeyAsInt() != this.key) {
                        return false;
                    }
                } else {
                    Object key = ((Map.Entry<?, ?>) obj).getKey();
                    if (!(key instanceof Integer) || ((Integer) key).intValue() != this.key) {
                        return false;
                    }
                }
                return Objects.equals(val, ((Map.Entry<?, ?>) obj).getValue());
            }
            return false;
        }
        
        @Override
        public String toString() {
            return key + "=" + val;
        }
    }
    
    private transient Store<V> store;
    
    private static class Store<V> {
        IntHashMap<V>.EntrySet entrySet;
        IntHashMap<V>.KeySet   keySet;
    }
    
    private Store<V> store() {
        Store<V> store = this.store;
        if (store == null) {
            store = this.store = new Store<>();
        }
        return store;
    }
    
    @Override
    public Set<Map.Entry<Integer, V>> entrySet() {
        Store<V> store    = store();
        EntrySet entrySet = store.entrySet;
        if (entrySet == null) {
            entrySet = store.entrySet = new EntrySet();
        }
        return entrySet;
    }
    
    @Override
    public IntSet keySet() {
        Store<V> store  = store();
        KeySet   keySet = store.keySet;
        if (keySet == null) {
            keySet = store.keySet = new KeySet();
        }
        return keySet;
    }
    
    private final class EntrySet extends AbstractSet<Map.Entry<Integer, V>> {
        @Override
        public int size() {
            return size;
        }
        @Override
        public boolean contains(Object obj) {
            if (obj instanceof Map.Entry<?, ?>) {
                Map.Entry<?, ?> e = (Map.Entry<?, ?>) obj;
                
                int key;
                
                if (e instanceof IntMap.Entry<?>) {
                    key = ((IntMap.Entry<?>) e).getKeyAsInt();
                } else {
                    Object objKey = e.getKey();
                    if (!(objKey instanceof Integer)) {
                        return false;
                    }
                    key = (Integer) objKey;
                }
                
                V val = IntHashMap.this.getAsInt(key);
                
                if (val == null) {
                    return e.getValue() == null && IntHashMap.this.containsKeyAsInt(key);
                } else {
                    return Objects.equals(e.getValue(), val);
                }
            }
            return false;
        }
        @Override
        @SuppressWarnings("collection-remove")
        public boolean remove(Object obj) {
            if (contains(obj)) {
                IntHashMap.this.remove(((Map.Entry<?, ?>) obj).getKey());
                return true;
            }
            return false;
        }
        @Override
        @SuppressWarnings("unchecked")
        public Iterator<Map.Entry<Integer, V>> iterator() {
            return
                (Iterator<Map.Entry<Integer, V>>)
                    (Iterator<? extends Map.Entry<Integer, V>>)
                        new EntryIterator<V>(IntHashMap.this);
        }
    }
    
    private static abstract class AbstractIterator<V, E> implements Iterator<E> {
        private final IntHashMap<V> map;
        
        private int i = -1;
        private EntryImpl<V> prev;
        private EntryImpl<V> next;
        
        private AbstractIterator(IntHashMap<V> map) {
            this.map = map;
            advance();
        }
        
        private void advance() {
            EntryImpl<V> prev = this.next;
            
            if (prev != null) {
                EntryImpl<V> next = prev.next;
                if (next != null) {
                    this.next = next;
                    return;
                }
            }
            
            EntryImpl<V>[] table  = map.table;
            int            length = table.length;
            
            for (int i = this.i + 1; i < length; ++i) {
                EntryImpl<V> e = table[i];
                if (e != null) {
                    this.next = e;
                    this.i    = i;
                    return;
                }
            }
            
            this.next = null;
            this.i    = length - 1;
        }
        
        @Override
        public boolean hasNext() {
            return next != null;
        }
        
        @Override
        public E next() {
            EntryImpl<V> next = this.next;
            if (next != null) {
                this.prev = next;
                advance();
                return getNext(next);
            }
            
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            EntryImpl<V> prev = this.prev;
            
            if (prev == null) {
                throw new IllegalStateException();
            }
            
            this.prev = null;
            map.removeAsInt(prev.key);
        }
        
        abstract E getNext(EntryImpl<V> e);
    }
    
    private static class EntryIterator<V> extends AbstractIterator<V, EntryImpl<V>> {
        private EntryIterator(IntHashMap<V> map) {
            super(map);
        }
        
        @Override
        EntryImpl<V> getNext(EntryImpl<V> e) {
            return e;
        }
    }
    
    private final class KeySet extends AbstractSet<Integer> implements IntSet {
        @Override
        public int size() {
            return IntHashMap.this.size();
        }
        @Override
        public boolean addAsInt(int key) {
            throw new UnsupportedOperationException("addAsInt(int)");
        }
        @Override
        public boolean contains(Object obj) {
            return (obj instanceof Integer) && containsAsInt((Integer) obj);
        }
        @Override
        public boolean containsAsInt(int key) {
            return IntHashMap.this.containsKeyAsInt(key);
        }
        @Override
        public boolean remove(Object obj) {
            return (obj instanceof Integer) && removeAsInt((Integer) obj);
        }
        @Override
        public boolean removeAsInt(int key) {
            if (IntHashMap.this.containsKeyAsInt(key)) {
                IntHashMap.this.removeAsInt(key);
                return true;
            }
            return false;
        }
        @Override
        public PrimitiveIterator.OfInt iterator() {
            return new IntKeyIterator(IntHashMap.this);
        }
    }
    
    private static class KeyIterator<V> extends AbstractIterator<V, Integer> {
        private KeyIterator(IntHashMap<V> map) {
            super(map);
        }
        
        @Override
        Integer getNext(EntryImpl<V> e) {
            return e.getKey();
        }
    }
    
    private static class IntKeyIterator implements PrimitiveIterator.OfInt {
        private final EntryIterator<?> iter;
        
        private IntKeyIterator(IntHashMap<?> map) {
            iter = new EntryIterator<>(map);
        }
        
        @Override
        public boolean hasNext() {
            return iter.hasNext();
        }
        
        @Override
        public int nextInt() {
            return iter.next().key;
        }
        
        @Override
        public void remove() {
            iter.remove();
        }
    }
}