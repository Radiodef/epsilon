/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;

public interface IntSet extends Set<Integer> {
    boolean addAsInt(int val);
    
    @Override
    default boolean add(Integer val) {
        return this.addAsInt(val);
    }
    
    boolean removeAsInt(int val);
    
    @Override
    default boolean remove(Object obj) {
        return this.removeAsInt((Integer) obj);
    }
    
    boolean containsAsInt(int val);
    
    @Override
    default boolean contains(Object obj) {
        return this.containsAsInt((Integer) obj);
    }
    
    @Override
    PrimitiveIterator.OfInt iterator();
    
    static IntSet unmodifiableView(IntSet set) {
        if (set instanceof UnmodifiableIntSet)
            return set;
        return new UnmodifiableIntSet(set);
    }
    
    static IntSet empty() {
        return EmptyIntSet.INSTANCE;
    }
}

@eps.json.JsonSerializable
final class UnmodifiableIntSet implements IntSet {
    @eps.json.JsonProperty
    private final IntSet set;
    
    @eps.json.JsonConstructor
    UnmodifiableIntSet(IntSet set) {
        this.set = Objects.requireNonNull(set, "set");
    }
    
    @Override public boolean isEmpty() { return set.isEmpty(); }
    @Override public int     size()    { return set.size();    }
    
    @Override public boolean containsAsInt(int val)       { return set.containsAsInt(val); }
    @Override public boolean containsAll(Collection<?> c) { return set.containsAll(c);     }
    
    @Override public void    clear()              { throw new UnsupportedOperationException(); }
    @Override public boolean addAsInt(int val)    { throw new UnsupportedOperationException(); }
    @Override public boolean removeAsInt(int val) { throw new UnsupportedOperationException(); }
    
    @Override public boolean addAll(Collection<? extends Integer> c) { throw new UnsupportedOperationException(); }
    @Override public boolean retainAll(Collection<?> c)              { throw new UnsupportedOperationException(); }
    @Override public boolean removeAll(Collection<?> c)              { throw new UnsupportedOperationException(); }
    
    @Override public     Object[] toArray()      { return set.toArray();  }
    @Override public <T> T[]      toArray(T[] a) { return set.toArray(a); }
    
    @Override
    public PrimitiveIterator.OfInt iterator() {
        return new PrimitiveIterator.OfInt() {
            private final PrimitiveIterator.OfInt it = set.iterator();
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public int nextInt() {
                return it.nextInt();
            }
        };
    }
    
    @Override public int     hashCode()         { return set.hashCode();  }
    @Override public boolean equals(Object obj) { return set.equals(obj); }
    @Override public String  toString()         { return set.toString();  }
}

final class EmptyIntSet implements IntSet {
    static final EmptyIntSet INSTANCE = new EmptyIntSet();
    
    static {
        eps.json.ToStringConverter<EmptyIntSet> converter =
            eps.json.ToStringConverter.create(EmptyIntSet.class,
                                              instance -> "",
                                              text     -> INSTANCE);
        eps.json.ToStringConverter.put(converter);
    }
    
    private EmptyIntSet() {}
    
    @Override public boolean isEmpty() { return true; }
    @Override public int     size()    { return 0;    }
    
    @Override public boolean containsAsInt(int val)       { return false; }
    @Override public boolean containsAll(Collection<?> c) { return c.isEmpty(); }
    
    @Override public void clear() {}
    
    @Override public boolean addAsInt(int val)    { throw new UnsupportedOperationException(); }
    @Override public boolean removeAsInt(int val) { throw new UnsupportedOperationException(); }
    
    @Override public boolean addAll(Collection<? extends Integer> c) { throw new UnsupportedOperationException(); }
    @Override public boolean retainAll(Collection<?> c)              { throw new UnsupportedOperationException(); }
    @Override public boolean removeAll(Collection<?> c)              { throw new UnsupportedOperationException(); }
    
    @Override public Object[] toArray() { return new Object[0]; }
    
    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length > 0) {
            a[0] = null;
        }
        return a;
    }
    
    private static final PrimitiveIterator.OfInt EMPTY_ITER =
        new PrimitiveIterator.OfInt() {
            @Override
            public boolean hasNext() {
                return false;
            }
            @Override
            public int nextInt() {
                throw new NoSuchElementException();
            }
        };
    
    @Override
    public PrimitiveIterator.OfInt iterator() {
        return EMPTY_ITER;
    }
    
    @Override public int     hashCode()         { return Collections.emptySet().hashCode(); }
    @Override public boolean equals(Object obj) { return (obj instanceof Set<?>) && ((Set<?>) obj).isEmpty(); }
    @Override public String  toString()         { return "[]"; }
}