/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;

public interface IntMap<V> extends Map<Integer, V> {
    V getAsInt(int key);
    
    @Override
    default V get(Object key) {
        return getAsInt((Integer) key);
    }
    
    V putAsInt(int key, V val);
    
    @Override
    default V put(Integer key, V val) {
        return putAsInt(key, val);
    }
    
    default V putIfAbsentAsInt(int key, V val) {
        if (containsKeyAsInt(key)) {
            return getAsInt(key);
        } else {
            return putAsInt(key, val);
        }
    }
    
    @Override
    default V putIfAbsent(Integer key, V val) {
        return putIfAbsentAsInt(key, val);
    }
    
    V removeAsInt(int key);
    
    @Override
    default V remove(Object key) {
        return removeAsInt((Integer) key);
    }
    
    boolean containsKeyAsInt(int key);
    
    @Override
    default boolean containsKey(Object key) {
        return containsKeyAsInt((Integer) key);
    }
    
    @Override
    abstract IntSet keySet();
    
    interface Entry<V> extends Map.Entry<Integer, V> {
        int getKeyAsInt();
        
        @Override
        default Integer getKey() {
            return this.getKeyAsInt();
        }
    }
    
    static <V> IntMap<V> unmodifiableView(IntMap<? extends V> map) {
        return new UnmodifiableIntMap<>(Objects.requireNonNull(map, "map"));
    }
}

final class UnmodifiableIntMap<V> implements IntMap<V> {
    private final IntMap<? extends V> map;
    
    UnmodifiableIntMap(IntMap<? extends V> map) {
        this.map = Objects.requireNonNull(map, "map");
    }
    
    @Override public int     size()                  { return map.size();    }
    @Override public boolean isEmpty()               { return map.isEmpty(); }
    @Override public V       getAsInt(int k)         { return map.getAsInt(k);         }
    @Override public boolean containsKeyAsInt(int k) { return map.containsKeyAsInt(k); }
    @Override public boolean containsValue(Object v) { return map.containsValue(v);    }
    
    @Override public Set<Map.Entry<Integer, V>> entrySet() { return new UnmodifiableIntMapEntrySet<>(map); }
    @Override public Collection<V>              values()   { return new UnmodifiableIntMapValues<>(map);   }
    @Override public IntSet                     keySet()   { return new UnmodifiableIntMapKeySet(map);     }
    
    @Override public void clear()              { throw new UnsupportedOperationException(); }
    @Override public V    putAsInt(int k, V v) { throw new UnsupportedOperationException(); }
    @Override public V    removeAsInt(int k)   { throw new UnsupportedOperationException(); }
    @Override public void putAll(Map<? extends Integer, ? extends V> m) { throw new UnsupportedOperationException(); }
    
    @Override public int     hashCode()       { return map.hashCode(); }
    @Override public boolean equals(Object o) { return map.equals(o);  }
    @Override public String  toString()       { return map.toString(); }
}

final class UnmodifiableIntMapEntrySet<V> extends AbstractSet<Map.Entry<Integer, V>> {
    private final Set<? extends Map.Entry<Integer, ? extends V>> entries;
    
    UnmodifiableIntMapEntrySet(IntMap<? extends V> map) {
        this.entries = Objects.requireNonNull(map, "map").entrySet();
    }
    
    @Override
    public int size() {
        return entries.size();
    }
    
    @Override
    public boolean contains(Object obj) {
        return entries.contains(obj);
    }
    
    static final class UnmodifiableEntry<V> implements IntMap.Entry<V> {
        private final IntMap.Entry<? extends V> entry;
        
        UnmodifiableEntry(IntMap.Entry<? extends V> entry) {
            this.entry = Objects.requireNonNull(entry, "entry");
        }
        
        @Override
        public int getKeyAsInt() {
            return entry.getKeyAsInt();
        }
        
        @Override
        public V getValue() {
            return entry.getValue();
        }
        
        @Override
        public V setValue(V val) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int hashCode() {
            return entry.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return entry.equals(obj);
        }
        
        @Override
        public String toString() {
            return entry.toString();
        }
    }
    
    static final class UnmodifiableEntryIterator<V> implements Iterator<Map.Entry<Integer, V>> {
        private final Iterator<? extends Map.Entry<Integer, ? extends V>> iter;
        
        UnmodifiableEntryIterator(Set<? extends Map.Entry<Integer, ? extends V>> entries) {
            iter = Objects.requireNonNull(entries, "entries").iterator();
        }
        
        @Override
        public boolean hasNext() {
            return iter.hasNext();
        }
        
        @Override
        public Map.Entry<Integer, V> next() {
            return new UnmodifiableEntry<>((IntMap.Entry<? extends V>) iter.next());
        }
    }
    
    @Override
    public Iterator<Map.Entry<Integer, V>> iterator() {
        return new UnmodifiableEntryIterator<>(entries);
    }
}

final class UnmodifiableIntMapValues<V> extends AbstractCollection<V> {
    private final Collection<? extends V> values;
    
    UnmodifiableIntMapValues(IntMap<? extends V> map) {
        this.values = Objects.requireNonNull(map, "map").values();
    }
    
    @Override
    public int size() {
        return values.size();
    }
    
    @Override
    public boolean contains(Object obj) {
        return values.contains(obj);
    }
    
    static final class UnmodifiableValueIterator<V> implements Iterator<V> {
        private final Iterator<? extends V> iter;
        
        UnmodifiableValueIterator(Collection<? extends V> values) {
            iter = Objects.requireNonNull(values, "values").iterator();
        }
        
        @Override
        public boolean hasNext() {
            return iter.hasNext();
        }
        
        @Override
        public V next() {
            return iter.next();
        }
    }
    
    @Override
    public Iterator<V> iterator() {
        return new UnmodifiableValueIterator<>(values);
    }
}

final class UnmodifiableIntMapKeySet extends AbstractSet<Integer> implements IntSet {
    private final IntSet keys;
    
    UnmodifiableIntMapKeySet(IntMap<?> map) {
        this.keys = Objects.requireNonNull(map, "map").keySet();
    }
    
    @Override
    public int size() {
        return keys.size();
    }
    
    @Override
    public boolean containsAsInt(int key) {
        return keys.containsAsInt(key);
    }
    
    @Override
    public boolean contains(Object obj) {
        return keys.contains(obj);
    }
    
    @Override public boolean removeAsInt(int key)     { throw new UnsupportedOperationException(); }
    @Override public boolean remove     (Object key)  { throw new UnsupportedOperationException(); }
    @Override public boolean addAsInt   (int key)     { throw new UnsupportedOperationException(); }
    @Override public boolean add        (Integer key) { throw new UnsupportedOperationException(); }
    
    static final class UnmodifiableKeyIterator implements PrimitiveIterator.OfInt {
        private final PrimitiveIterator.OfInt iter;
        
        UnmodifiableKeyIterator(IntSet keys) {
            iter = Objects.requireNonNull(keys, "keys").iterator();
        }
        
        @Override
        public boolean hasNext() {
            return iter.hasNext();
        }
        
        @Override
        public int nextInt() {
            return iter.nextInt();
        }
    }
    
    @Override
    public PrimitiveIterator.OfInt iterator() {
        return new UnmodifiableKeyIterator(keys);
    }
}