/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;
import java.util.function.*;

public final class Lazy<T> implements Supplier<T> {
    private final Supplier<? extends T> supplier;
    
    private volatile T obj;
    
    private volatile boolean isInitialized = false;
    
    public Lazy(Supplier<? extends T> supplier) {
        this.supplier = Objects.requireNonNull(supplier, "supplier");
    }
    
    public static <T> Lazy<T> from(Supplier<? extends T> supplier) {
        return new Lazy<>(supplier);
    }
    
    @Override
    public T get() {
        if (!isInitialized) {
            synchronized (this) {
                if (!isInitialized) {
                    obj = supplier.get();
                    isInitialized = true;
                }
            }
        }
        return obj;
    }
    
    public Optional<T> getIfInitialized() {
        synchronized (this) {
            return isInitialized ? Optional.of(obj) : Optional.empty();
        }
    }
    
    public void ifInitialized(Consumer<? super T> action) {
        synchronized (this) {
            if (isInitialized) {
                action.accept(obj);
            }
        }
    }
}