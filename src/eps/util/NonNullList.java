/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;
import static java.util.Objects.*;

public final class NonNullList<E> implements List<E> {
    private final List<E> list;
    
    public NonNullList() {
        this.list = new ArrayList<>();
    }
    
    public NonNullList(List<E> list) {
        requireNonNull(list, "list");
        
        if (!list.isEmpty()) {
            int i = 0;
            for (E e : list) {
                if (e == null) {
                    throw new NullPointerException("element at index " + i);
                }
                ++i;
            }
        }
        
        this.list = list;
    }
    
    public NonNullList(Collection<? extends E> coll) {
        this(new ArrayList<>(coll));
    }
    
    public static <E> NonNullList<E> empty() {
        return new NonNullList<>();
    }
    
    public static <E> NonNullList<E> of(List<E> list) {
        return new NonNullList<>(list);
    }
    
    private NonNullList(List<E> list, Void $private) {
        this.list = list;
    }
    
    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }
    
    @Override
    public int size() {
        return list.size();
    }
    
    @Override
    public void clear() {
        list.clear();
    }
    
    @Override
    public boolean add(E elem) {
        return list.add(requireNonNull(elem, "elem"));
    }
    
    @Override
    public void add(int index, E elem) {
        list.add(index, requireNonNull(elem, "elem"));
    }
    
    @Override
    public boolean addAll(Collection<? extends E> coll) {
        Misc.requireNonNull(coll, "coll");
        return list.addAll(coll);
    }
    
    @Override
    public boolean addAll(int index, Collection<? extends E> coll) {
        Misc.requireNonNull(coll, "coll");
        return list.addAll(index, coll);
    }
    
    @Override
    public boolean remove(Object obj) {
        return obj != null && list.remove(obj);
    }
    
    @Override
    public E remove(int index) {
        return list.remove(index);
    }
    
    @Override
    public E get(int index) {
        return list.get(index);
    }
    
    @Override
    public E set(int index, E elem) {
        return list.set(index, requireNonNull(elem, "elem"));
    }
    
    @Override
    public boolean contains(Object obj) {
        return obj != null && list.contains(obj);
    }
    
    @Override
    public int indexOf(Object obj) {
        return obj == null ? -1 : list.indexOf(obj);
    }
    
    @Override
    public int lastIndexOf(Object obj) {
        return obj == null ? -1 : list.lastIndexOf(obj);
    }
    
    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }
    
    @Override
    public boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }
    
    @Override
    public boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }
    
    @Override
    public List<E> subList(int start, int end) {
        return new NonNullList<>(list.subList(start, end), (Void) null);
    }
    
    @Override
    public Object[] toArray() {
        return list.toArray();
    }
    
    @Override
    public <T> T[] toArray(T[] array) {
        return list.toArray(array);
    }
    
    @Override
    public int hashCode() {
        return list.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        return list.equals(obj);
    }
    
    @Override
    public String toString() {
        return list.toString();
    }
    
    @Override
    public Iterator<E> iterator() {
        return this.listIterator();
    }
    
    @Override
    public java.util.ListIterator<E> listIterator() {
        return this.listIterator(0);
    }
    
    @Override
    public java.util.ListIterator<E> listIterator(int index) {
        return new ListIterator<>(list.listIterator(index));
    }
    
    private static final class ListIterator<E> implements java.util.ListIterator<E> {
        private final java.util.ListIterator<E> it;
        
        private ListIterator(java.util.ListIterator<E> it) {
            this.it = it;
        }
        
        @Override
        public boolean hasNext() {
            return it.hasNext();
        }
        
        @Override
        public int nextIndex() {
            return it.nextIndex();
        }
        
        @Override
        public E next() {
            return it.next();
        }
        
        @Override
        public boolean hasPrevious() {
            return it.hasPrevious();
        }
        
        @Override
        public int previousIndex() {
            return it.previousIndex();
        }
        
        @Override
        public E previous() {
            return it.previous();
        }
        
        @Override
        public void remove() {
            it.remove();
        }
        
        @Override
        public void set(E elem) {
            it.set(requireNonNull(elem, "elem"));
        }
        
        @Override
        public void add(E elem) {
            it.add(requireNonNull(elem, "elem"));
        }
    }
}