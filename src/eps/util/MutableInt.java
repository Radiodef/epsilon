/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.function.*;

public final class MutableInt implements IntSupplier, IntConsumer, IntPredicate, Comparable<MutableInt>, Cloneable {
    public int value;
    
    public MutableInt() {
    }
    
    public MutableInt(int value) {
        this.value = value;
    }
    
    public MutableInt set(int value) {
        this.value = value;
        return this;
    }
    
    public int getAndSet(int value) {
        int before = this.value;
        this.value = value;
        return before;
    }
    
    public int setAndGet(int value) {
        return this.value = value;
    }
    
    public MutableInt increment() {
        ++value;
        return this;
    }
    
    public int incrementAndGet() {
        return ++value;
    }
    
    public int getAndIncrement() {
        return value++;
    }
    
    @Override
    public int getAsInt() {
        return value;
    }
    
    @Override
    public void accept(int value) {
        this.value = value;
    }
    
    public boolean is(int value) {
        return this.value == value;
    }
    
    @Override
    public boolean test(int value) {
        return this.value == value;
    }
    
    @Override
    public int compareTo(MutableInt that) {
        return Integer.compare(this.value, that.value);
    }
    
    @Override
    public int hashCode() {
        return value;
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof MutableInt) && ((MutableInt) obj).value == this.value;
    }
    
    @Override
    public String toString() {
        return Integer.toString(value);
    }
    
    @Override
    public MutableInt clone() {
        try {
            return (MutableInt) super.clone();
        } catch (CloneNotSupportedException x) {
            throw new AssertionError("clone", x);
        }
    }
}