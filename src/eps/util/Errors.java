/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;

public final class Errors {
    private Errors() {}
    
    public static double requireReal(String name, double d) {
        if (eps.math.Math2.isReal(d)) {
            return d;
        } else {
            throw newIllegalArgument(name, d);
        }
    }
    
    public static IllegalArgumentException newIllegalArgument(int val) {
        return new IllegalArgumentException(String.valueOf(val));
    }
    
    public static IllegalArgumentException newIllegalArgument(Object val) {
        return new IllegalArgumentException(String.valueOf(val));
    }
    
    public static IllegalArgumentException newIllegalArgument(String label, int val) {
        return new IllegalArgumentException(label + " was " + val);
    }
    
    public static IllegalArgumentException newIllegalArgument(String label, Object val) {
        return new IllegalArgumentException(label + " was " + val);
    }
    
    public static IllegalArgumentException newIllegalArgumentF(String format, Object... args) {
        return new IllegalArgumentException(String.format(format, args));
    }
    
    public static IllegalStateException newIllegalState(Object val) {
        return new IllegalStateException(String.valueOf(val));
    }
    
    public static IndexOutOfBoundsException newIndexOutOfBounds(int val) {
        return new IndexOutOfBoundsException(String.valueOf(val));
    }
    
    public static IndexOutOfBoundsException newIndexOutOfBounds(String label, int val) {
        return new IndexOutOfBoundsException(label + " was " + val);
    }
    
    public static IndexOutOfBoundsException newIndexOutOfBounds(String label, int val, int min, int max) {
        return new IndexOutOfBoundsException(String.format("%s was %d, with bounds %d (inclusive) to %d (exclusive)", label, val, min, max));
    }
    
    public static IndexOutOfBoundsException newIndexOutOfBoundsClosed(String label, int val, int min, int max) {
        return new IndexOutOfBoundsException(String.format("%s was %d, with bounds %d (inclusive) to %d (inclusive)", label, val, min, max));
    }
    
    public static ArithmeticException newArithmetic(Object val) {
        return new ArithmeticException(String.valueOf(val));
    }
    
    public static NoSuchElementException newNoSuchElement(Object val) {
        return new NoSuchElementException(String.valueOf(val));
    }
}