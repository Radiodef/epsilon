/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.app.*;

import java.util.*;
import java.util.function.*;

import java.lang.ref.*;

public interface Gate extends AutoCloseable {
    boolean isBlocked();
    Gate block();
    Gate unblock();
    Gate flush();
    Gate forceUnsafeFlush();
    boolean hasActionsWaiting();
//    Object monitor();
    default boolean isActive() {
        return isBlocked() || hasActionsWaiting();
    }
    
    /**
     * Note: this actually <em>opens</em> the gate. Unfortunate clash of names.
     * <p>
     * {@code close()} shouldn't be called directly. It's just present for
     * try-with-resources.
     */
    @Override
    default void close() {
        unblock();
    }
    
    public static final class Consumer implements Gate, java.util.function.Consumer<Runnable> {
        private final Deque<Runnable> actions = new ArrayDeque<>();
        
        private final Object monitor = new Object();
        
        private final Map<Thread, Counter> locks = new WeakHashMap<>();
        
        public Consumer() {
            QueueMonitor.put(this);
        }
        
//        @Override
        public Object monitor() {
            return monitor;
        }
        
        public void await() throws InterruptedException {
            synchronized (monitor) {
                while (isBlocked()) {
                    monitor.wait();
                }
                flush();
            }
        }
        
        public boolean await(long millis) throws InterruptedException {
            synchronized (monitor) {
                if (isBlocked()) {
                    monitor.wait(millis);
                }
                flush();
                return !isBlocked() && !hasActionsWaiting();
            }
        }
        
        @Override
        public boolean hasActionsWaiting() {
            synchronized (monitor) {
                return !actions.isEmpty();
            }
        }
        
        @Override
        public void accept(Runnable action) {
            synchronized (monitor) {
                if (locks.isEmpty()) {
                    action.run();
                } else {
                    actions.addLast(action);
                }
            }
        }
        
        @Override
        public boolean isBlocked() {
            synchronized (monitor) {
                return !locks.isEmpty();
            }
        }
        
        private static final Function<Thread, Counter> NEW_COUNTER = t -> new Counter();
        
        @Override
        public Gate block() {
            synchronized (monitor) {
                Thread  t = Thread.currentThread();
                Counter c = locks.computeIfAbsent(t, NEW_COUNTER);
                if (c.isZero()) {
                    QueueMonitor.put(t);
                }
                c.increment();
            }
            return this;
        }
        
        @Override
        public Gate unblock() {
            synchronized (monitor) {
                Map<Thread, Counter> locks = this.locks;
                
                Thread  t = Thread.currentThread();
                Counter c = locks.get(t);
                if (c == null) {
                    throw Errors.newIllegalState(t);
                }
                
                assert !c.isZero() : c;
                
                if (c.decrement().isZero()) {
                    locks.remove(t);

                    if (locks.isEmpty()) {
                        clearQueue();
                    }
                }
            }
            return this;
        }
        
        @Override
        public Gate flush() {
            synchronized (monitor) {
                if (locks.isEmpty()) {
                    clearQueue();
                }
            }
            return this;
        }
        
        @Override
        public Gate forceUnsafeFlush() {
            synchronized (monitor) {
                clearQueue();
            }
            return this;
        }
        
        private void clearQueue() {
            synchronized (monitor) {
                Deque<Runnable> actions = this.actions;
                
                Throwable caught = null;
                
                for (Runnable r; (r = actions.pollFirst()) != null;) {
                    try {
                        r.run();
                    } catch (Throwable x) {
                        if (caught != null) {
                            x.addSuppressed(caught);
                        }
                        caught = x;
                        if (x instanceof Error && !(x instanceof AssertionError)) {
                            throw x;
                        }
                    }
                }
                
                monitor.notifyAll();
                
                if (caught != null) {
                    Misc.rethrow(caught);
                }
            }
        }
        
        private enum QueueMonitor implements Runnable {
            INSTANCE;
            
            private final Set<Gate.Consumer> gates   = Misc.newWeakHashSet();
            private final Set<Thread>        threads = Misc.newWeakHashSet();
            private final Set<Reference<?>>  refs    = new HashSet<>();
            
            private final ReferenceQueue<Thread> queue = new ReferenceQueue<>();
            
            private final Thread thread;
            
            private QueueMonitor() {
                Thread thread = App.newThread(this);
                thread.setName(getClass().getName());
                thread.setDaemon(true);
                this.thread = thread;
                
                thread.start();
            }
            
            private static void put(Gate.Consumer self) {
                QueueMonitor qm = INSTANCE;
                synchronized (qm) {
                    qm.gates.add(self);
                }
            }
            
            private static void put(Thread thread) {
                QueueMonitor qm = INSTANCE;
                synchronized (qm) {
                    if (qm.threads.add(thread)) {
                        Log.note(QueueMonitor.class, "put(Thread)", "thread = ", thread.getName());
                        
                        qm.refs.add(new WeakReference<>(thread, qm.queue));
                    }
                }
            }
            
            private static final class GateCollectedException extends Exception {
            }
            
            @Override
            public void run() {
                List<Gate.Consumer> local = new ArrayList<>();
                for (;;) {
                    Reference<? extends Thread> ref;
                    
                    int count = 0;
                    
                    try {
                        Log.note(getClass(), "run", "waiting on the queue");
                        ref = queue.remove();
                        ++count;
                        synchronized (this) {
                            refs.remove(ref);
                        }
                    } catch (InterruptedException x) {
                        Log.caught(getClass(), "run", x, false);
                    }
                    
                    for (;;) {
                        ref = queue.poll();
                        if (ref == null) {
                            break;
                        }
                        ++count;
                        synchronized (this) {
                            refs.remove(ref);
                        }
                    }
                    
                    if (count > 0) {
                        Log.caught(getClass(), "blocking thread garbage collected",
                                   new GateCollectedException(), false);
                    }
                    
                    // do not nest synchronized blocks
                    synchronized (this) {
                        local.addAll(gates);
                    }
                    
                    Log.note(getClass(), "run", "notifying all monitors");
                    
                    for (Gate.Consumer gate : local) {
                        Object monitor = gate.monitor();
                        
                        synchronized (monitor) {
                            if (!gate.isBlocked()) {
                                monitor.notifyAll();
                            }
                        }
                    }
                    
                    local.clear();
                }
            }
        }
    }
}