/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;

public class ArraySet<E> extends AbstractSet<E> {
    private final List<E> list;
    
    public ArraySet() {
        this(10);
    }
    
    public ArraySet(int initialCapacity) {
        this.list = new ArrayList<>(initialCapacity);
    }
    
    public ArraySet(Collection<? extends E> toCopy) {
        if (toCopy instanceof Set<?>) {
            this.list = new ArrayList<>(toCopy);
        } else {
            this.list = new ArrayList<>(new LinkedHashSet<>(toCopy));
        }
    }
    
    @Override
    public int size() {
        return list.size();
    }
    
    public E get(int index) {
        return list.get(index);
    }
    
    public boolean set(int index, E elem) {
        int indexOf = list.indexOf(elem);
        if (indexOf == index || indexOf < 0) {
            list.set(index, elem);
            return true;
        }
        return false;
    }
    
    public E remove(int index) {
        return list.remove(index);
    }
    
    @Override
    public boolean add(E elem) {
        return !list.contains(elem) && list.add(elem);
    }
    
    @Override
    public boolean remove(Object obj) {
        return list.remove(obj);
    }
    
    @Override
    public boolean contains(Object obj) {
        return list.contains(obj);
    }
    
    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }
}