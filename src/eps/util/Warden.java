/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import java.util.*;

public final class Warden implements AutoCloseable {
    public static final class Permit implements AutoCloseable {
        private final Warden  owner;
        private final boolean isValid;
        private final String  description;
        
        private Permit(Warden owner, boolean isValid, String description) {
            this.owner       = Objects.requireNonNull(owner, "owner");
            this.isValid     = isValid;
            this.description = description;
        }
        
        public boolean isValid() {
            return isValid;
        }
        
        public boolean ifValid(Runnable task) {
            if (isValid()) {
                task.run();
                return true;
            }
            return false;
        }
        
        public String getDescription() {
            return description;
        }
        
        @Override
        public void close() {
            owner.remove(this);
        }
        
        private static final ToString<Permit> TO_STRING =
            ToString.builder(Permit.class)
                    .add("isValid",     Permit::isValid)
                    .add("description", Permit::getDescription)
                    .build();
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
    }
    
    private final String name;
    
    private final Object monitor = new Object();
    
    private final Set<Permit> permits = new LinkedHashSet<>();
    
    private boolean isOpen = true;
    
    public Warden(String name) {
        this.name = String.valueOf(name);
    }
    
    private boolean remove(Permit p) {
        synchronized (monitor) {
            if (permits.remove(p)) {
                monitor.notifyAll();
                return true;
            }
            return false;
        }
    }
    
    public Permit acquire() {
        return acquire(null);
    }
    
    public Permit acquire(String description) {
        synchronized (monitor) {
            Permit p = new Permit(this, isOpen, description);
            permits.add(p);
            return p;
        }
    }
    
    public boolean hasPermits() {
        return getPermitCount() > 0;
    }
    
    public int getPermitCount() {
        synchronized (monitor) {
            return permits.size();
        }
    }
    
    public boolean hasValidPermits() {
        synchronized (monitor) {
            return permits.stream().anyMatch(Permit::isValid);
        }
    }
    
    public int getValidPermitCount() {
        synchronized (monitor) {
            return (int) permits.stream().filter(Permit::isValid).count();
        }
    }
    
    public void await() {
        synchronized (monitor) {
            while (hasValidPermits()) {
                Misc.wait(monitor);
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (monitor) {
            isOpen = false;
        }
    }
    
    public void closeAndWait() {
        close();
        await();
    }
    
    private static final ToString<Warden> TO_STRING =
        ToString.builder(Warden.class)
                .add("name",    w -> w.name)
                .add("monitor", w -> w.monitor)
                .add("isOpen",  w -> w.isOpen)
                .add("permits", w -> w.permits)
                .build();
    @Override
    public String toString() {
        synchronized (monitor) {
            return TO_STRING.apply(this);
        }
    }
}