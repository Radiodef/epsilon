/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.ui.*;
import eps.json.*;
import eps.app.*;
import eps.eval.*;
import static eps.app.App.*;

@JsonSerializable
public class StyleBlock extends AbstractDecorationBlock {
    @JsonProperty
    private Setting<Style> setting;
    @JsonProperty
    private Style style;
    
    public StyleBlock() {
        this.style = Style.EMPTY;
    }
    
    public StyleBlock(Block block) {
        super(block);
        this.style = Style.EMPTY;
    }
    
    public StyleBlock(String text) {
        this(new TextBlock(text));
    }
    
    @JsonConstructor
    private StyleBlock(Block block, Setting<Style> setting, Style style) {
        super(block);
        this.setting = setting;
        if (setting == null) {
            this.style = Style.replnull(style);
        }
    }
    
    @Override
    public StyleBlock copy() {
        StyleBlock copy = new StyleBlock(this.getBlock().copy(), this.setting, this.style);
        return this.copyTo(copy);
    }
    
    public Style getStyle() {
        if (setting != null) {
            return Style.replnull(app().getSetting(setting));
        } else {
            return Style.replnull(style);
        }
    }
    
    private StyleBlock set(Style style) {
        if (style == null)
            style = Style.EMPTY;
        this.style   = style;
        this.setting = null;
        return this;
    }
    
    public StyleBlock set(Setting<Style> setting) {
        if (setting == null) {
            return this.set(Style.EMPTY);
        }
        this.setting = setting;
        this.style   = null;
        return this;
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, Context c) {
        return this.getBlock().toString(b, c);
    }
    
    @Override
    public StringBuilder toHtml(StringBuilder b, Context c) {
        b.append("<span style=\"");
        
        Style style = this.style;
        
        if (setting != null) {
            style = (c == null) ? app().getSetting(setting) : c.getSetting(setting);
        }
        
        Style.replnull(style).toCssString(b);
        
        b.append("\">");
        this.getBlock().toHtml(b, c);
        b.append("</span>");
        return b;
    }
}