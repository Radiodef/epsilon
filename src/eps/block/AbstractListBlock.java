/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.util.*;
import eps.json.*;

import java.util.*;
import java.util.function.*;

@JsonSerializable
public abstract class AbstractListBlock extends AbstractBlock implements ListBlock {
    @JsonProperty
    private final List<Block> blocks;
    
    public AbstractListBlock() {
        this.blocks = NonNullList.empty();
    }
    
    public AbstractListBlock(int initialCap) {
        this.blocks = NonNullList.of(new ArrayList<>(initialCap));
    }
    
    @JsonConstructor
    protected AbstractListBlock(List<Block> blocks) {
        if (blocks == null) {
            this.blocks = new ArrayList<>(0);
        } else {
            this.blocks = new ArrayList<>(blocks);
            blocks.removeIf(Objects::isNull);
        }
    }
    
    public AbstractListBlock(Iterable<? extends Block> blocks) {
        if (blocks instanceof Collection<?>) {
            this.blocks = NonNullList.of(new ArrayList<>(((Collection<?>) blocks).size()));
        } else {
            this.blocks = NonNullList.empty();
        }
        
        for (Block block : blocks) {
            this.blocks.add(EmptyBlock.replnull(block));
        }
    }
    
    protected <B extends AbstractListBlock> B copyListTo(B dst) {
        for (Block b : this.blocks) {
            ((AbstractListBlock) dst).blocks.add(b.copy());
        }
        return dst;
    }
    
    protected <B extends Block> Block replaceAllInList(Class<B> type, Function<? super B, ? extends Block> fn) {
        List<Block> blocks = this.blocks;
        int         size   = blocks.size();
        
        for (int i = 0; i < size; ++i) {
            blocks.set(i, blocks.get(i).replaceAll(type, fn));
        }
        
        return this;
    }
    
    protected Block findInBlocks(Predicate<? super Block> test) {
        List<Block> blocks = this.blocks;
        int         size   = blocks.size();
        
        for (int i = 0; i < size; ++i) {
            Block b = blocks.get(i).findFirst(test);
            if (b != null) {
                return b;
            }
        }
        
        return null;
    }
    
    @Override
    public int count() {
        return blocks.size();
    }
    
    @Override
    public List<Block> getBlocks() {
        return blocks;
    }
    
    @Override
    public AbstractListBlock setBlocks(Iterable<? extends Block> blocks) {
        this.blocks.clear();
        for (Block block : blocks) {
            this.addBlock(block);
        }
        return this;
    }
    
    @Override
    public AbstractListBlock addBlock(Block block) {
        this.blocks.add(EmptyBlock.replnull(block));
        return this;
    }
    
    @Override
    public AbstractListBlock removeBlock(Block block) {
        this.blocks.remove(EmptyBlock.replnull(block));
        return this;
    }
}