/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.eval.*;
import eps.json.*;

import java.util.function.*;

@JsonSerializable
public abstract class AbstractBlock implements Block {
//    private Node node;
    private Token token;
    
    @JsonConstructor
    public AbstractBlock() {
    }
    
    protected <B extends AbstractBlock> B copyTo(B dst) {
//        ((AbstractBlock) dst).node  = this.node;
        ((AbstractBlock) dst).token = this.token;
        return dst;
    }
    
//    @Override
//    public Block setNode(Node node) {
//        this.node = node;
//        return this;
//    }
    
//    @Override
//    public Node getNode() {
//        return this.node;
//    }
    
    @Override
    public Block setToken(Token token) {
        this.token = token;
        return this;
    }
    
    @Override
    public Token getToken() {
        return this.token;
    }
    
    @Override
    public String toString() {
        return this.toString(new StringBuilder()).toString();
    }
    
    protected static <T extends Block, U extends Block> U tryReplace(Block b, Class<T> type, Function<? super T, ? extends U> fn) {
        if (type.isInstance(b)) {
            return fn.apply(type.cast(b));
        }
        return null;
    }
    
    protected <B extends Block> Block replaceAllNoChildren(Class<B> type, Function<? super B, ? extends Block> fn) {
        Block repl = tryReplace(this, type, fn);
        if (repl != null) {
            return repl;
        } else {
            return this;
        }
    }
    
    @Override
    public Block findFirst(Predicate<? super Block> test) {
        return test.test(this) ? this : null;
    }
    
    @Override
    public void forEach(Consumer<? super Block> action) {
        action.accept(this);
    }
}