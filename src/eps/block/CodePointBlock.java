/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.json.*;
import eps.eval.*;

import java.util.function.*;

@JsonSerializable
public class CodePointBlock extends AbstractBlock {
    @JsonProperty
    private int code;
    
    @JsonConstructor
    public CodePointBlock(int code) {
        this.code = code;
    }
    
    public CodePointBlock(eps.util.CodePoint code) {
        this.code = code.intValue();
    }
    
    @Override
    public CodePointBlock copy() {
        CodePointBlock copy = new CodePointBlock(this.code);
        return this.copyTo(copy);
    }
    
    public int getCode() {
        return code;
    }
    
    public CodePointBlock setCode(int code) {
        this.code = code;
        return this;
    }
    
    @Override
    public <B extends Block> Block replaceAll(Class<B> type, Function<? super B, ? extends Block> fn) {
        return this.replaceAllNoChildren(type, fn);
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, Context c) {
        return b.appendCodePoint(code);
    }
    
    @Override
    public StringBuilder toHtml(StringBuilder b, Context c) {
        int    code = this.code;
        String esc  = Block.escapeHtml(code);
        if (esc != null) {
            return b.append(esc);
        }
        if (code == '\r' || code == '\n') {
            return b.append("<br />");
        }
        return b.appendCodePoint(code);
    }
    
    @Override
    public String toString() {
        return new String(Character.toChars(code));
    }
}