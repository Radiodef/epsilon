/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.util.*;
import eps.json.*;
import eps.eval.*;

import java.util.function.*;

@JsonSerializable
public class TextBlock extends AbstractBlock implements Appendable, CharSequence {
    @JsonProperty(interimConverter = "eps.block.TextBlock$StringBuilderInterimConverter")
    private final StringBuilder text;
    
    private static final class StringBuilderInterimConverter extends InterimConverter<StringBuilder, String> {
        private StringBuilderInterimConverter() {
            super(StringBuilder.class, String.class);
        }
        @Override
        public String toInterim(Object obj) {
            return obj.toString();
        }
        @Override
        public StringBuilder fromInterim(Object obj) {
            return obj == null ? new StringBuilder(0) : new StringBuilder(obj.toString());
        }
    }
    
    @JsonConstructor
    private TextBlock(StringBuilder text) {
        this.text = java.util.Objects.requireNonNull(text, "text");
    }
    
    public TextBlock() {
        this.text = new StringBuilder(0);
    }
    
    public TextBlock(CharSequence text) {
        this.text = new StringBuilder(text);
    }
    
    public TextBlock(Object text) {
        this.text = new StringBuilder(String.valueOf(text));
    }
    
    private TextBlock(StringBuilder text, int start, int end) {
        if (start < 0 || text.length() < start) {
            throw Errors.newIndexOutOfBounds(start);
        }
        if (start > end) {
            throw Errors.newIndexOutOfBounds(end);
        }
        this.text = new StringBuilder(end - start).append(text, start, end);
    }
    
    @Override
    public TextBlock copy() {
        TextBlock copy = new TextBlock(this.text);
        return this.copyTo(copy);
    }
    
    @Override
    public <B extends Block> Block replaceAll(Class<B> type, Function<? super B, ? extends Block> fn) {
        return this.replaceAllNoChildren(type, fn);
    }
    
    public StringBuilder getText() {
        return text;
    }
    
    public TextBlock setText(Object text) {
        this.text.append(text);
        return this;
    }
    
    @Override
    public TextBlock append(char c) {
        text.append(c);
        return this;
    }
    
    @Override
    public TextBlock append(CharSequence text) {
        this.text.append(text);
        return this;
    }
    
    @Override
    public TextBlock append(CharSequence text, int start, int end) {
        this.text.append(text, start, end);
        return this;
    }
    
    public TextBlock append(int count, CharSequence text) {
        StringBuilder b = this.text;
        
        b.ensureCapacity(b.length() + (count * text.length()));
        
        for (; count > 0; --count) {
            b.append(text);
        }
        
        return this;
    }
    
    public TextBlock append(Object val) {
        this.text.append(val);
        return this;
    }
    
    public TextBlock appendCodePoint(int code) {
        this.text.appendCodePoint(code);
        return this;
    }
    
    @Override
    public int length() {
        return text.length();
    }
    
    @Override
    public char charAt(int index) {
        return text.charAt(index);
    }
    
    @Override
    public TextBlock subSequence(int start, int end) {
        return new TextBlock(this.text, start, end);
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, Context c) {
        return b.append(this.text);
    }
    
    @Override
    public StringBuilder toHtml(StringBuilder b, Context c) {
        StringBuilder text = this.text;
        int           len  = text.length();
        int           prev = 0;
//        b.append("<pre>");
        
        for (int i = 0; i < len;) {
            int    code  = text.codePointAt(i);
            String esc   = Block.escapeHtml(code);
            int    count = Character.charCount(code);
            if (esc != null) {
                b.append(text, prev, i).append(esc);
                prev = (i += count);
            } else if (code == '\r' || code == '\n') {
                if (code == '\r') {
                    if ((i + 1) < len && text.codePointAt(i + 1) == '\n') {
                        ++i;
                    }
                }
                b.append(text, prev, i).append("<br />");
                prev = (i += count);
            } else {
                i += count;
            }
        }
        
        if (prev < len) {
            b.append(text, prev, len);
        }
        
//        b.append("</pre>");
        return b;
    }
    
    @Override
    public String toString() {
        return text.toString();
    }
}