/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.json.*;

import java.util.function.*;

@JsonSerializable
public abstract class AbstractDecorationBlock extends AbstractBlock implements DecorationBlock {
    @JsonProperty
    private Block block;
    
    public AbstractDecorationBlock() {
        this.block = new EmptyBlock();
    }
    
    @JsonConstructor
    public AbstractDecorationBlock(Block block) {
        this.block = EmptyBlock.replnull(block);
    }
    
    @Override
    protected <B extends AbstractBlock> B copyTo(B dst) {
        ((AbstractDecorationBlock) dst).block = this.block.copy();
        return super.copyTo(dst);
    }
    
    @Override
    public <B extends Block> Block replaceAll(Class<B> type, Function<? super B, ? extends Block> fn) {
        Block repl;
        
        repl = tryReplace(this, type, fn);
        if (repl != null) {
            return repl;
        }
        
        this.block = this.block.replaceAll(type, fn);
        return this;
    }
    
    @Override
    public Block findFirst(Predicate<? super Block> test) {
        if (test.test(this))
            return this;
        if (test.test(this.block))
            return this.block;
        return null;
    }
    
    @Override
    public Block getBlock() {
        return this.block;
    }
    
    @Override
    public AbstractDecorationBlock setBlock(Block block) {
        this.block = EmptyBlock.replnull(block);
        return this;
    }
}