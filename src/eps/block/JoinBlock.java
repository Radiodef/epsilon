/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.json.*;
import eps.eval.*;
import eps.util.*;

import java.util.*;
import java.util.function.*;

@JsonSerializable
public class JoinBlock extends AbstractListBlock {
    @JsonProperty
    private Block delim;
    
    @JsonConstructor
    protected JoinBlock(List<Block> blocks, Block delim) {
        super(blocks);
        this.delim = delim;
    }
    
    public JoinBlock() {
    }
    
    public JoinBlock(int initialCap) {
        super(initialCap);
    }
    
    public JoinBlock(Iterable<? extends Block> blocks) {
        super(blocks);
    }
    
    public JoinBlock(Block delim) {
        this.delim = delim;
    }
    
    @Override
    public JoinBlock copy() {
        JoinBlock copy = new JoinBlock(this.count());
        copy.delim = Block.copy(this.delim);
        return this.copyListTo(this.copyTo(copy));
    }
    
    @Override
    public <B extends Block> Block replaceAll(Class<B> type, Function<? super B, ? extends Block> fn) {
        Block repl = tryReplace(this, type, fn);
        if (repl != null) {
            return repl;
        }
        
        Block delim = this.delim;
        if (delim != null) {
            this.delim = delim.replaceAll(type, fn);
        }
        
        return this.replaceAllInList(type, fn);
    }
    
    @Override
    public Block findFirst(Predicate<? super Block> test) {
        if (test.test(this)) {
            return this;
        }
        if (delim != null && test.test(delim)) {
            return delim;
        }
        return this.findInBlocks(test);
    }
    
    @Override
    public void forEach(Consumer<? super Block> action) {
        action.accept(this);
        if (delim != null) {
            action.accept(delim);
        }
        this.getBlocks().forEach(b -> b.forEach(action));
    }
    
    public Block getDelimiter() {
        return delim;
    }
    
    public JoinBlock setDelimiter(Block delim) {
        this.delim = delim;
        return this;
    }
    
    public JoinBlock setDelimiter(String delim) {
        return this.setDelimiter(new TextBlock(delim));
    }
    
    public JoinBlock setDelimiter(int code) {
        return this.setDelimiter(new CodePointBlock(code));
    }
    
    @Override
    public JoinBlock setBlocks(Iterable<? extends Block> blocks) {
        super.setBlocks(blocks);
        return this;
    }
    
    @Override
    public JoinBlock addBlock(Block block) {
        super.addBlock(block);
        return this;
    }
    
    @Override
    public JoinBlock removeBlock(Block block) {
        super.removeBlock(block);
        return this;
    }
    
    protected StringBuilder toString(StringBuilder b, Context c, TriConsumer<Block, StringBuilder, Context> action) {
        List<Block> blocks = this.getBlocks();
        int         count  = blocks.size();
        Block       delim  = this.delim;
        for (int i = 0; i < count; ++i) {
            if (i != 0 && delim != null) {
                action.accept(delim, b, c);
            }
            action.accept(blocks.get(i), b, c);
        }
        return b;
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, Context c) {
        return this.toString(b, c, Block::toString);
    }
    
    @Override
    public StringBuilder toHtml(StringBuilder b, Context c) {
//        if (true) return toTable(b, c);
        List<Block> blocks = this.getBlocks();
        int         count  = blocks.size();
        Block       delim  = this.delim;
        
//        b.append("<div style='display:inline'>");
        
        for (int i = 0; i < count; ++i) {
            if (i != 0 && delim != null) {
                delim.toHtml(b, c);
            }
            blocks.get(i).toHtml(b, c);
        }
        
//        b.append("</div>");
        
        return b;
    }
    
    private StringBuilder toTable(StringBuilder b, Context c) {
        List<Block> blocks = this.getBlocks();
        int         count  = blocks.size();
        Block       delim  = this.delim;
        
//        b.append("<table cellpadding='0' cellspacing='0' style='white-space:pre'><tr>");
        b.append("<table cellpadding='0' cellspacing='0'><tr>");
        
        for (int i = 0; i < count; ++i) {
            if (i != 0 && delim != null) {
                b.append("<td style='white-space:pre'>");
                delim.toHtml(b, c);
                b.append("</td>");
            }
            b.append("<td style='white-space:pre'>");
            blocks.get(i).toHtml(b, c);
            b.append("</td>");
        }
        
        b.append("</tr></table>");
        
        return b;
    }
}