/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.app.*;
import eps.eval.*;
import eps.util.*;
import static eps.util.Misc.*;

import java.util.function.*;
import static java.lang.Character.*;

public interface Block extends Copyable<Block> {
//    Block setNode(Node node);
//    Node getNode();
    Block setToken(Token token);
    Token getToken();
    
    default Block takeTokenFrom(Block block) {
        Token token = block.getToken();
        block.setToken((Token) null);
        return this.setToken(token);
    }
    
    default Block setToken(Node node) {
        return this.setToken(node.getToken());
    }
    
    default StringBuilder toString(StringBuilder b) {
        return this.toString(b, null);
    }
    
    StringBuilder toString(StringBuilder b, Context c);
    
    default String toHtml() {
        return this.toHtml(new StringBuilder(), null).toString();
    }
    
    default StringBuilder toHtml(StringBuilder b) {
        return this.toHtml(b, null);
    }
    
    StringBuilder toHtml(StringBuilder b, Context c);
    
    default Block replaceAll(UnaryOperator<Block> op) {
        return this.replaceAll(Block.class, op);
    }
    
    <B extends Block> Block replaceAll(Class<B> type, Function<? super B, ? extends Block> fn);
    
    void forEach(Consumer<? super Block> action);
    Block findFirst(Predicate<? super Block> test);
    
    default <B extends Block> B findFirst(Class<B> type) {
        Block b = this.findFirst(type::isInstance);
        return type.cast(b);
    }
    
    static Block copy(Block block) {
        return block == null ? null : block.copy();
    }
    
    static CodePointBlock of(int code) {
        return new CodePointBlock(code);
    }
    
    static TextBlock of(CharSequence text) {
        return new TextBlock(text);
    }
    
    static TextBlock format(String fmt, Object... args) {
        return new TextBlock(String.format(fmt, args));
    }
    
    static Block empty() {
        return new EmptyBlock();
    }
    
    static Block ln() {
        return new NewLineBlock();
    }
    
    static Block space() {
        return new CodePointBlock(' ');
    }
    
    static Block indent(Context context) {
        String indent = Misc.unquote(context.getSetting(Setting.EDITOR_INDENT));
        if (indent != null) {
            return new TextBlock(indent);
        } else {
            return new EmptyBlock();
        }
    }
    
    static JoinBlock join(Block lhs, Block rhs) {
        return new JoinBlock(2).addBlock(lhs)
                               .addBlock(rhs);
    }
    
    static JoinBlock join(Block block0, Block block1, Block block2) {
        return new JoinBlock(3).addBlock(block0)
                               .addBlock(block1)
                               .addBlock(block2);
    }
    
    static JoinBlock join(Block... blocks) {
        if (blocks != null) {
            JoinBlock join = new JoinBlock(blocks.length);
            for (Block b : blocks)
                join.addBlock(b);
            return join;
        }
        return new JoinBlock(0);
    }
    
    static JoinBlock join(String delim, Block... blocks) {
        return Block.join(blocks).setDelimiter(delim);
    }
    
    static JoinBlock join(int delim, Block... blocks) {
        return Block.join(blocks).setDelimiter(delim);
    }
    
    static String escapeHtml(int code) {
        switch (code) {
//            case ' ' : return "&nbsp;";
            case '&' : return "&amp;";
            case '<' : return "&lt;";
            case '>' : return "&gt;";
            case '"' : return "&quot;";
            case '\'': return "&#39;";
            default  : return null;
        }
    }
    
    static String escapeHtml(String text) {
        StringBuilder b = escapeHtml(null, text);
        return (b == null) ? text : b.toString();
    }
    
    static StringBuilder escapeHtml(StringBuilder b, String text) {
        int len  = text.length();
        int prev = 0;
        for (int i = 0; i < len;) {
            int    code = text.codePointAt(i);
            String esc  = escapeHtml(code);
            int    next = i + Character.charCount(code);
            if (esc != null) {
                if (b == null) {
                    b = new StringBuilder(len);
                }
                b.append(text, prev, i).append(esc);
                prev = next;
            }
            i = next;
        }
        
        return (b == null) ? null : b.append(text, prev, len);
    }
    
    static Block reindent(Block root) {
        String indent = Settings.getEditorIndent();
        
        return root.replaceAll(block -> {
            Token token = block.getToken();
            if (token != null) {
                Substring source = token.getSource();
                String    full   = source.getFullSource();
                
                int indents = 0;
                boolean  ln = false;
                
                int   end  = 0;
                Token prev = token.getPrev();
                if (prev != null) {
                    end = prev.getSource().getSourceEnd();
                }
                
                for (int i = source.getSourceStart(); i > end;) {
                    if (rangeBeforeEquals(full, i, indent)) {
                        ++indents;
                        i -= indent.length();
                    } else {
                        int code = full.codePointBefore(i);
                        if (code == '\n') {
                            ln = true;
                            break;
                        }
                        if (!isWhitespace(code)) {
                            break;
                        }
                        i -= charCount(code);
                    }
                }
                
                if (ln || indents != 0) {
                    JoinBlock result = new JoinBlock(3);
                    
                    if (ln) {
                        result.addBlock(new NewLineBlock());
                    }
                    if (indents != 0) {
                        result.addBlock(new TextBlock().append(indents, indent));
                    }
                    
                    return result.addBlock(block);
                }
            }
            return null;
        });
    }
}