/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.json.*;
import eps.eval.*;

@JsonSerializable
public abstract class AbstractSuperscriptSubscriptBlock extends AbstractDecorationBlock {
    public AbstractSuperscriptSubscriptBlock() {
    }
    
    @JsonConstructor
    public AbstractSuperscriptSubscriptBlock(Block block) {
        super(block);
    }
    
    protected abstract String openingHtmlTag();
    protected abstract String closingHtmlTag();
    
    @Override
    public AbstractSuperscriptSubscriptBlock setBlock(Block block) {
        super.setBlock(block);
        return this;
    }
    
    @Override
    public StringBuilder toHtml(StringBuilder b, Context c) {
        b.append(this.openingHtmlTag());
        this.getBlock().toHtml(b, c);
        b.append(this.closingHtmlTag());
        return b;
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, Context c) {
        return this.getBlock().toString(b, c);
    }
    
    @Override
    public String toString() {
        return this.getBlock().toString();
    }
}