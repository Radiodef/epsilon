/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.block;

import eps.util.*;
import eps.eval.*;

import java.util.*;
import java.util.function.*;

// Experimental. It seems that Java doesn't understand the
// display:inline-block property so that makes this very
// unlikely to work inline, I guess.
public class TowerBlock extends AbstractListBlock {
    public TowerBlock() {
    }
    
    public TowerBlock(int initialCap) {
        super(initialCap);
    }
    
    public TowerBlock(Iterable<? extends Block> blocks) {
        super(blocks);
    }
    
    @Override
    public TowerBlock copy() {
        TowerBlock copy = new TowerBlock(this.count());
        return this.copyListTo(this.copyTo(copy));
    }
    
    @Override
    public <B extends Block> Block replaceAll(Class<B> type, Function<? super B, ? extends Block> fn) {
        Block repl = tryReplace(this, type, fn);
        if (repl != null) {
            return repl;
        }
        
        return this.replaceAllInList(type, fn);
    }
    
    @Override
    public Block findFirst(Predicate<? super Block> test) {
        if (test.test(this)) {
            return this;
        }
        return this.findInBlocks(test);
    }
    
    @Override
    public void forEach(Consumer<? super Block> action) {
        action.accept(this);
        this.getBlocks().forEach(b -> b.forEach(action));
    }
    
    @Override
    public TowerBlock setBlocks(Iterable<? extends Block> blocks) {
        super.setBlocks(blocks);
        return this;
    }
    
    @Override
    public TowerBlock addBlock(Block block) {
        super.addBlock(block);
        return this;
    }
    
    @Override
    public TowerBlock removeBlock(Block block) {
        super.removeBlock(block);
        return this;
    }
    
    protected StringBuilder toString(StringBuilder b, Context c, TriConsumer<Block, StringBuilder, Context> action) {
        List<Block> blocks = this.getBlocks();
        int         count  = blocks.size();
        for (int i = 0; i < count; ++i) {
            action.accept(blocks.get(i), b, c);
        }
        return b;
    }
    
    @Override
    public StringBuilder toString(StringBuilder b, Context c) {
        return this.toString(b, c, Block::toString);
    }
    
    @Override
    public StringBuilder toHtml(StringBuilder b, Context c) {
        // this is the correct style
        b.append("<div style='display:inline-block; vertical-align:middle; text-align:center'>");
        
        // ...
//        b.append("<div style='display:inline-block'>");
//        b.append("<span style='vertical-align:middle; float:left'>");
//        b.append("<span style=''>");
        
        for (Block block : this.getBlocks()) {
            // this is the correct style
//            b.append("<div style='display:block'>");
            
            // ...
//            b.append("<span style='display:block'>");
            b.append("<div>");
            
            block.toHtml(b, c);
            
            b.append("</div>");
        }
        
        b.append("</div>");
        return b;
    }
}