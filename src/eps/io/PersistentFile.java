/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.app.*;
import eps.util.*;

import java.util.*;
import java.util.function.*;

import java.nio.file.Path;
import java.nio.file.InvalidPathException;
import java.nio.file.FileAlreadyExistsException;
import java.io.IOException;

public class PersistentFile {
    private final PersistentDirectory parent;
    
    private final String name;
    private final String extension;
    
    private volatile boolean pathCreated;
    private volatile Path    cachedPath;
    
    private volatile boolean didError = false;
    
    PersistentFile(PersistentDirectory parent, String name, String extension) {
        this.parent    = parent;
        this.name      = Objects.requireNonNull(name, "name");
        this.extension = extension;
        
        if (name.isEmpty()) {
            throw new IllegalArgumentException("empty name");
        }
    }
    
    public final PersistentDirectory getParent() {
        return parent;
    }
    
    public final String getName() {
        return name;
    }
    
    public final String getExtension() {
        return extension;
    }
    
    public final String getFileName() {
        String actual = getActualFileName();
        if (actual != null) {
            return actual;
        } else {
            return createFileName(name);
        }
    }
    
    public final String getActualFileName() {
        Path path = getPath();
        if (path == null) {
            path = cachedPath;
        }
        if (path != null) {
            return path.getFileName().toString();
        } else {
            return null;
        }
    }
    
    public final boolean errorIsSet() {
        return didError || Log.caughtError();
    }
    
    protected final void setError() {
        didError = true;
    }
    
    public boolean isDirectory() {
        return extension == null;
    }
    
    /**
     * @return the path to the actual file, or {@code null} if there was a problem.
     */
    public final Path getPath() {
        if (errorIsSet()) {
            return null;
        }
        synchronized (this) {
            if (pathCreated) {
                return cachedPath;
            }
            if (initPathAndContents(null)) {
                return cachedPath;
            }
            return null;
        }
    }
    
    protected String createFileName(String name) {
        if (extension != null) {
            name += "." + extension;
        }
        return name;
    }
    
    /**
     * Creates a new file with default contents at the specified location.
     * <p>
     * Passes the contents of the new file to the specified consumer, if
     * applicable and the consumer is not {@code null}.
     * <p>
     * Subclasses should override {@code createNewFileImpl} to actually create
     * the file.
     * 
     * @param  path
     * @param  contentsOut
     * @return {@code true} if the file was created and {@code false} otherwise.
     * @throws FileAlreadyExistsException
     * @throws IOException 
     */
    protected final boolean createNewFile(Path path, Consumer<Object> contentsOut)
            throws FileAlreadyExistsException, IOException {
        synchronized (this) {
            if (errorIsSet()) {
                Log.note(getClass(), "createNewFile", "Attempted to create ", this);
                return false;
            } else {
                Log.note(getClass(), "createNewFile", "Creating ", this);
                return createNewFileImpl(path, contentsOut);
            }
        }
    }
    
    /**
     * Actually creates the new file with default contents at the specified
     * location.
     * <p>
     * Subclasses should override this to implement file creation.
     * 
     * @param  path
     * @param  contentsOut
     * @return {@code true} if the file was created and {@code false} otherwise.
     * @throws FileAlreadyExistsException
     * @throws IOException 
     */
    protected boolean createNewFileImpl(Path path, Consumer<Object> contentsOut)
            throws FileAlreadyExistsException, IOException {
        throw new IOException("not implemented for " + this);
    }
    
    public byte[] readAllBytes() {
        Path path = getPath();
        
        if (path != null) {
            try {
                return FileSystem.instance().readAllBytes(path);
            } catch (IOException x) {
                Log.caught(getClass(), "while reading " + this, x, false);
            }
        }
        
        return null;
    }
    
    /**
     * Returns whether or not the file at the specified path has contents which
     * are valid for this persistent file. If the file must be read to check its
     * contents, then if the contents are valid, they are passed to the consumer,
     * if the consumer is not {@code null}.
     * <p>
     * Subclasses should override this method to customize the test.
     * 
     * @param  path
     * @param  contentsOut
     * @return {@code true} if the specified file is valid and {@code false} otherwise.
     * @throws IOException 
     */
    protected boolean hasValidContents(Path path, Consumer<Object> contentsOut) throws IOException {
        return true;
    }
    
    protected final boolean isInitialized() {
        return pathCreated;
    }
    
    /**
     * Initializes the {@code cachedPath} by invoking {@code getPathAndContents}.
     * <p>
     * Note: in general, this is the method to actually invoke for initialization.
     * 
     * @param  contentsOut
     * @return {@code true} if the operation was a success and {@code false} otherwise.
     */
    protected final boolean initPathAndContents(Consumer<Object> contentsOut) {
        synchronized (this) {
            Mutable<Path> path = new Mutable<>(null);
            boolean    success = getPathAndContents(path::set, contentsOut);
            cachedPath  = path.value;
            pathCreated = true;
            return success;
        }
    }
    
    /**
     * Determines the location of the actual file, reading or creating it if
     * necessary. If the file is read or created, then the contents are passed
     * to the contents consumer, if it's not {@code null}.
     * <p>
     * If the operation is a success, then the path of the actual file is
     * passed to the path consumer, if it's not {@code null}.
     * <p>
     * Note: should be called under {@code synchronized(this)}!
     * 
     * @param  pathOut
     * @param  contentsOut
     * @return {@code true} if the operation was a success and {@code false} otherwise.
     */
    protected final boolean getPathAndContents(Consumer<Path> pathOut, Consumer<Object> contentsOut) {
        assert Thread.holdsLock(this) : this;
        if (errorIsSet()) {
            return false;
        }
        
        Path parentPath;
        if (parent != null) {
            parentPath = parent.getPath();
        } else {
            parentPath = IoMisc.getWorkingDirectory();
        }
        if (parentPath == null) {
            return false;
        }
        
        Path completePath = null;
        
        for (String candidate : Suffixer.each(name)) {
            String nameAndExt = createFileName(candidate);
            
            Path path;
            try {
                path = parentPath.resolve(nameAndExt);
            } catch (InvalidPathException x) {
                Log.caught(getClass(), toString(), x, false);
                break;
            }
            
            if (FileSystem.instance().exists(path)) {
                try {
                    if (hasValidContents(path, contentsOut)) {
                        completePath = path;
                        break;
                    } else {
                        // continue back around with the next suffix
                    }
                } catch (IOException x) {
                    Log.caught(getClass(), toString(), x, false);
                    break;
                }
            } else {
                try {
                    createNewFile(path, contentsOut);
                    completePath = path;
                    break;
                } catch (FileAlreadyExistsException x) {
                    Log.caught(getClass(), toString(), x, true);
                    // continue back around with the next suffix
                } catch (IOException x) {
                    Log.caught(getClass(), toString(), x, false);
                    break;
                }
            }
        }
        
        if (completePath != null) {
            if (pathOut != null) {
                pathOut.accept(completePath);
            }
            return true;
        } else {
            setError();
            return false;
        }
    }
    
    protected static final ToString<PersistentFile> PERSISTENT_FILE_TO_STRING =
        ToString.builder(PersistentFile.class)
                .add("name",        PersistentFile::getName)
                .add("extension",   PersistentFile::getExtension)
                .add("parent",      PersistentFile::getParent)
                .add("pathCreated", pf -> pf.pathCreated)
                .add("cachedPath",  pf -> pf.cachedPath)
                .build();
    
    @Override
    public String toString() {
        return PERSISTENT_FILE_TO_STRING.apply(this);
    }
}