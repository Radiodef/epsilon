/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import static eps.io.IoMisc.abs;

import java.util.stream.*;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.LinkOption;
import java.nio.file.OpenOption;
import java.nio.file.CopyOption;
import java.nio.file.NotDirectoryException;

public enum NoFileSystem implements FileSystem {
    INSTANCE;
    
    @Override
    public String toString() {
        return Misc.toSimpleString(this);
    }
    
    @Override
    public boolean exists(Path path) {
        return false;
    }
    
    @Override
    public boolean isDirectory(Path path, LinkOption... opts) {
        return false;
    }
    
    @Override
    public boolean isRegularFile(Path path, LinkOption... opts) {
        return false;
    }
    
    @Override
    public byte[] readAllBytes(Path path) throws IOException {
        throw new IOException("reading " + abs(path));
    }
    
    @Override
    public void write(Path path, byte[] bytes, OpenOption... opts) throws IOException {
        throw new IOException("writing " + abs(path));
    }
    
    @Override
    public void createDirectory(Path path) throws IOException {
        throw new IOException("creating " + abs(path));
    }
    
    @Override
    public Stream<Path> list(Path path) throws IOException {
        throw new NotDirectoryException(abs(path).toString());
    }
    
    @Override
    public boolean deleteIfExists(Path path) throws IOException {
        return false;
    }
    
    @Override
    public Path copy(Path from, Path to, CopyOption... opts) throws IOException {
        throw new IOException("source does not exist " + abs(from));
    }
    
    @Override
    public OutputStream newOutputStream(Path path, OpenOption... opts) throws IOException {
        throw new IOException("cannot write to " + abs(path));
    }
}