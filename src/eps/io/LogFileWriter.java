/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import eps.ui.*;
import eps.app.*;
import eps.app.Log.*;
import static eps.io.IoMisc.*;

import java.time.*;
import java.time.format.*;
import java.util.*;
import java.util.function.*;
import java.util.concurrent.atomic.*;
import java.util.regex.*;
import java.util.stream.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.InvalidPathException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.StandardOpenOption;

public class LogFileWriter implements AutoCloseable {
    private final FileSystem fileSystem;
    
    private final Path path;
    
    private final BufferedWriter writer;
    
    private final long interval;
    
    private final Thread thread;
    
    private final LineConsumer consumer;
    
    private final ConsumerConsumer receiver;
    
    private final Object monitor = new Object();
    
    private volatile boolean isOpen = true;
    
    public LogFileWriter() {
        this(null);
    }
    
    public LogFileWriter(ConsumerConsumer receiver) {
        this(null, -1, receiver);
    }
    
    public static final long DEFAULT_INTERVAL = 5_000;
    
    public LogFileWriter(FileSystem fileSystem, long interval, ConsumerConsumer receiver) {
        this.fileSystem = (fileSystem == null) ? (fileSystem = FileSystem.instance()) : fileSystem;
        
        Pair<Path, BufferedWriter> pair = createFile(fileSystem);
        
        this.path   = pair.left;
        this.writer = pair.right;
        
        assert (path == null) == (writer == null) : this;
        assert (path == null) || isLogFile(path) : this;
        
        this.interval = (interval < 0 ? DEFAULT_INTERVAL : interval);
        this.receiver = (receiver == null) ? (receiver = Log.newLineReceiver(256)) : receiver;
        
        if (writer == null) {
            this.consumer = null;
            this.thread   = null;
        } else {
            this.consumer = new LineConsumer(writer);
            this.thread   = App.newThread(this::run);
            
            thread.setName(createThreadName(this));
            thread.setDaemon(true);
        }
        
        Log.note(getClass(), "<init>", "log file writer is ", this);
    }
    
    private Pair<Path, BufferedWriter> createFile(FileSystem fileSystem) {
        Path logDir = PersistentFiles.LOG_DIRECTORY.getPath();
        if (logDir != null) {
            String fileName = createFileNameForNow();
            
            for (String possible : Suffixer.each(fileName)) {
                Path path;
                try {
                    path = logDir.resolve(possible + DOT_EXTENSION);
                } catch (InvalidPathException x) {
                    Log.caught(getClass(), toString(), x, false);
                    break;
                }
                
                if (!fileSystem.exists(path)) {
                    try {
                        BufferedWriter writer =
                            fileSystem.newBufferedWriter(path, StandardOpenOption.CREATE_NEW,
                                                               StandardOpenOption.APPEND);
                        
                        return Pair.of(path, writer);
                        
                    } catch (FileAlreadyExistsException x) {
                        Log.caught(getClass(), toString(), x, false);
                        // try the next one
                    } catch (IOException x) {
                        Log.caught(getClass(), toString(), x, false);
                        break;
                    }
                }
            }
        }
        
        return Pair.of(null, null);
    }
    
    public Path getPath() {
        return path;
    }
    
    private static final class LineConsumer implements Consumer<ListDeque<Line>> {
        private final BufferedWriter writer;
        
        private final String sep = System.lineSeparator();
        
        private final AtomicReference<IOException> caught = new AtomicReference<>();
        
        private LineConsumer(BufferedWriter writer) {
            this.writer = Objects.requireNonNull(writer, "writer");
        }
        
        @Override
        public void accept(ListDeque<Line> lines) {
            BufferedWriter writer = this.writer;
            String         sep    = this.sep;
            IOException    caught = null;
            int            size   = lines.size();
            for (int i = 0; i < size; ++i) {
                String line = lines.get(i).getLine();
                try {
                    writer.write(line, 0, line.length());
                    writer.write(sep,  0, sep.length());
                } catch (IOException x) {
                    caught = x;
                    break;
                }
            }
            try {
                writer.flush();
            } catch (IOException x) {
                if (caught != null) {
                    x.addSuppressed(caught);
                }
                caught = x;
            }
            if (caught != null) {
                while (!this.caught.compareAndSet(null, caught)) {
                    IOException prev = this.caught.getAndSet(null);
                    if (prev != null) {
                        caught.addSuppressed(prev);
                    }
                }
            }
        }
        
        private boolean handleIOException() {
            IOException caught = this.caught.getAndSet(null);
            if (caught != null) {
                Log.caught(getClass(), toString(), caught, false);
                return true;
            }
            return false;
        }
    }
    
    private void run() {
        final long             interval = this.interval;
        final Object           monitor  = this.monitor;
        final ConsumerConsumer receiver = this.receiver;
        final LineConsumer     consumer = this.consumer;
        while (isOpen) {
            receiver.accept(consumer);
            if (consumer.handleIOException())
                break;
            Misc.wait(monitor, interval);
        }
    }
    
    public LogFileWriter start() {
        if (thread != null) {
            thread.start();
        }
        return this;
    }
    
    public boolean isOpen() {
        return isOpen;
    }
    
    public boolean isAlive() {
        return thread != null && thread.isAlive();
    }
    
    @Override
    public void close() {
        boolean wasOpen;
        // synchronized to make sure the get and set here can never interleave
        synchronized (monitor) {
            wasOpen = isOpen;
            if (wasOpen) {
                isOpen = false;
                monitor.notifyAll();
            }
        }
        if (wasOpen) {
            if (thread != null) {
                Misc.join(thread);
            }
            if (receiver != null) {
                if (consumer != null) {
                    receiver.accept(consumer);
                    consumer.handleIOException();
                }
                if (receiver instanceof LineReceiver) {
                    ((LineReceiver) receiver).close();
                }
            }
            if (writer != null) {
                try {
                    writer.write("LogFileWriter.close() invoked");
                    writer.write(System.lineSeparator());
                    
                    if (Thread.currentThread() == SHUTDOWN_HOOK) {
                        writer.write("LogFileWriter shutdown hook was run");
                        writer.write(System.lineSeparator());
                    }
                } catch (IOException x) {
                    Log.caught(getClass(), toString(), x, false);
                }
                try {
                    writer.flush();
                } catch (IOException x) {
                    Log.caught(getClass(), toString(), x, false);
                }
                try {
                    writer.close();
                } catch (IOException x) {
                    Log.caught(getClass(), toString(), x, false);
                }
            }
        }
    }
    
    private static final ToString<LogFileWriter> TO_STRING =
        ToString.builder(LogFileWriter.class)
                .add("fileSystem", lfw -> lfw.fileSystem)
                .add("path",       lfw -> abs(lfw.path))
                .add("interval",   lfw -> lfw.interval)
                .add("writer",     lfw -> lfw.writer)
                .add("receiver",   lfw -> lfw.receiver)
                .add("thread",     lfw -> lfw.thread)
                .add("monitor",    lfw -> lfw.monitor)
                .add("isOpen",     lfw -> lfw.isOpen)
                .build();
    @Override
    public String toString() {
        return TO_STRING.apply(this);
    }
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ *//* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    // static
    
    private static final AtomicReference<LogFileWriter> INSTANCE = new AtomicReference<>();
    
    private static volatile Thread SHUTDOWN_HOOK;
    
    public static LogFileWriter createInstance() {
        LogFileWriter lfw = new LogFileWriter();
        
        if (INSTANCE.compareAndSet(null, lfw)) {
            Thread hook = App.newThread(lfw::close);
            hook.setName("LogFileWriterShutdownHook");
            Runtime.getRuntime().addShutdownHook(hook);
            SHUTDOWN_HOOK = hook;
            
            cleanLogDirectory(PersistentFiles.LOG_DIRECTORY.getPath(), lfw.getPath());
            
            return lfw.start();
            
        } else {
            lfw.close();
            throw new IllegalStateException(INSTANCE.get().toString());
        }
    }
    
    private static String createThreadName(LogFileWriter lfw) {
        return "LogFileWriter@" + Integer.toHexString(System.identityHashCode(lfw)) + ".thread";
    }
    
    public static final String FILE_NAME     = "epsilon_log";
    public static final String EXTENSION     = "txt";
    public static final String DOT_EXTENSION = "." + EXTENSION;
    
    static String createFileNameForNow() {
        return FILE_NAME + "_" + getDate();
    }
    
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd-kk-mm-ss";
    
    private static String getDate() {
        LocalDateTime     now  = LocalDateTime.now();
        DateTimeFormatter fmt  = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
        String            date = now.format(fmt);
        return date;
    }
    
    private static final Pattern LOG_FILE_PATTERN =
        Pattern.compile(Pattern.quote(FILE_NAME + "_")
                      + DATE_TIME_PATTERN.replaceAll("\\w", Matcher.quoteReplacement("\\d"))
                      + "([1-9]\\d*)?" // duplicate name suffix
                      + Pattern.quote(DOT_EXTENSION));
    
    public static String getLogFilePattern() {
        return LOG_FILE_PATTERN.pattern();
    }
    
    public static boolean isLogFile(String fileName) {
        return LOG_FILE_PATTERN.matcher(fileName).matches();
    }
    
    public static boolean isLogFile(Path path) {
        Objects.requireNonNull(path, "path");
        Path logDir = PersistentFiles.LOG_DIRECTORY.getPath();
        if (logDir != null) {
            Path parent = path.toAbsolutePath().getParent();
            if (parent != null) {
                if (parent.equals(logDir.toAbsolutePath())) {
                    if (FileSystem.instance().isRegularFile(path)) {
                        return isLogFile(path.getFileName().toString());
                    }
                }
            }
        }
        return false;
    }
    
    public static List<Path> getLogFiles() {
        List<Path> list = new ArrayList<>();
        
        Path dir = PersistentFiles.LOG_DIRECTORY.getPath();
        if (dir != null) {
            try (Stream<Path> files = FileSystem.instance().list(dir)) {
                
                files.filter(LogFileWriter::isLogFile)
                     .forEach(list::add);
                
            } catch (IOException | UncheckedIOException x) {
                Log.caught(LogFileWriter.class, abs(dir).toString(), x, false);
            }
        }
        
        return list;
    }
    
    private static void cleanLogDirectory(Path logDir, Path currentLog) {
        if (logDir == null || currentLog == null) {
            return;
        }
        App.whenAvailable(app -> {
            List<Path> logFiles = getLogFiles();
            int        logCount = logFiles.size();
            if (logCount > app.getSetting(Setting.MAX_LOG_COUNT_BEFORE_NOTICE)) {
                app.addItemOnInterfaceThread(item -> {
                    ComponentFactory factory = app.getComponentFactory();
                    TextAreaCell cell = factory.newTextArea();
                    
                    String text =
                        String.format("Note: The log directory '%s' has %d files. "
                                    + "The log files (with names matching %s_DATE-TIME.%s) may be safely deleted, "
                                    + "except for '%s' which is currently in use.",
                                      abs(logDir), logCount, FILE_NAME, EXTENSION, currentLog.getFileName());
                    if (factory.isOpenSupported()) {
                        text += "\n • Enter 'open' as the next command to open the log directory.";
                    }
                    text += "\n • Enter 'delete' as the next command to delete all past log files.";
                    
                    app.addCommandFilter(cmd -> {
                        if ("open".equalsIgnoreCase(cmd) && factory.isOpenSupported()) {
                            factory.openDirectory(logDir);
                            return App.CommandFilter.CONSUME | App.CommandFilter.DONE;
                        }
                        if ("delete".equalsIgnoreCase(cmd)) {
                            for (Path log : logFiles) {
                                if (!absEquals(log, currentLog)) {
                                    try {
                                        FileSystem.instance().deleteIfExists(log);
                                    } catch (IOException x) {
                                        Log.caught(LogFileWriter.class, abs(log).toString(), x, false);
                                    }
                                }
                            }
                            return App.CommandFilter.CONSUME | App.CommandFilter.DONE;
                        }
                        return App.CommandFilter.DONE;
                    });
                    
                    cell.setText(text);
                    cell.followTextStyle(Setting.NOTE_STYLE);
                    
                    item.addCell(cell);
                });
            }
        });
    }
}