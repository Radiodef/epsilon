/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;

import java.util.*;

public class Suffixer implements Iterator<String> {
    private final String name;
    private final long start;
    private final long end;
    
    private final boolean noZero;
    
    private long current;
    
    private boolean didEnd;
    
    public Suffixer(String name) {
        this(name, true);
    }
    
    public Suffixer(String name, boolean noZero) {
        this(name, 0L, Long.MAX_VALUE, noZero);
    }
    
    public Suffixer(String name, long end) {
        this(name, 0L, end, true);
    }
    
    public Suffixer(String name, long start, long end, boolean noZero) {
        this.name    = (name == null ? "" : name);
        this.start   = start;
        this.end     = end;
        this.current = start;
        this.noZero  = noZero;
        this.didEnd  = false;
    }
    
    public static Iterable<String> each(String name) {
        return () -> new Suffixer(name);
    }
    
    public static Iterable<String> each(String name, long end) {
        return () -> new Suffixer(name, end);
    }
    
    public boolean isNoZero() {
        return noZero;
    }
    
    public String getName() {
        return name;
    }
    
    public long getStart() {
        return start;
    }
    
    public long getEnd() {
        return end;
    }
    
    public long getCurrent() {
        return current;
    }
    
    @Override
    public boolean hasNext() {
        return !(current == end && didEnd);
    }
    
    public long nextSuffix() {
        if (hasNext()) {
            long next = current;
            if (next == end) {
                assert !didEnd : this;
                didEnd = true;
            } else {
                current = next + 1;
            }
            return next;
        }
        throw new NoSuchElementException(toString());
    }
    
    @Override
    public String next() {
        long next = nextSuffix();
        if (noZero && next == 0) {
            return name;
        } else {
            return name + next;
        }
    }
    
    private static final ToString<Suffixer> TO_STRING =
        ToString.builder(Suffixer.class)
                .add("name",    Suffixer::getName)
                .add("noZero",  Suffixer::isNoZero)
                .add("start",   Suffixer::getStart)
                .add("end",     Suffixer::getEnd)
                .add("current", Suffixer::getCurrent)
                .add("hasNext", Suffixer::hasNext)
                .build();
    @Override
    public String toString() {
        return TO_STRING.apply(this);
    }
}