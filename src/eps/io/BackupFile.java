/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.app.*;
import eps.util.*;

import java.util.*;

import java.nio.file.Path;
import java.nio.file.InvalidPathException;
import java.io.IOException;

public final class BackupFile implements AutoCloseable {
    private final FileSystem fileSystem;
    
    private final Path source;
    private final Path backup;
    
    private final boolean backupSuccess;
    
    public BackupFile(Path path) {
        this(path, null);
    }
    
    public BackupFile(Path path, FileSystem fileSystem) {
        this.source     = Objects.requireNonNull(path, "path");
        this.fileSystem = (fileSystem == null) ? (fileSystem = FileSystem.instance()) : fileSystem;
        
        if (!fileSystem.exists(path)) {
            this.backup        = null;
            this.backupSuccess = false;
            return;
//            throw new IllegalArgumentException("file does not exist: " + path);
        }
        
        Path backup = getBackupFile(path);
        this.backup = backup;
        
        boolean backupSuccess = false;
        
        if (backup != null) {
            try {
                fileSystem.copy(path, backup);
                backupSuccess = true;
            } catch (IOException x) {
                Log.caught(getClass(), toString(), x, false);
            }
        }
        
        this.backupSuccess = backupSuccess;
    }
    
    private Path getBackupFile(Path source) {
        Pair<String, String> split = IoMisc.splitFileName(source);
        
        Path parent = IoMisc.getParent(source);
        
        for (String name : Suffixer.each(split.left + "_backup")) {
            if (split.right != null) {
                name += "." + split.right;
            }
            
            try {
                Path possible = parent.resolve(name);
                
                if (!fileSystem.exists(possible)) {
                    return possible;
                }
                
            } catch (InvalidPathException x) {
                Log.caught(getClass(), toString(), x, false);
                return null;
            }
        }
        
        assert false : this;
        return null;
    }
    
    public FileSystem getFileSystem() {
        return fileSystem;
    }
    
    public Path getSource() {
        return source;
    }
    
    public Path getBackup() {
        return backup;
    }
    
    public boolean backupSuccess() {
        return backupSuccess;
    }
    
    @Override
    public void close() {
        if (backup != null) {
            try {
                boolean deleted = fileSystem.deleteIfExists(backup);
                Log.note(getClass(), "close", "backup at '", backup, "' ",
                         deleted ? "deleted" : "not deleted/did not exist");
            } catch (IOException x) {
                Log.caught(getClass(), toString(), x, false);
            }
        }
    }
    
    private static final ToString<BackupFile> TO_STRING =
        ToString.builder(BackupFile.class)
                .add("fileSystem", BackupFile::getFileSystem)
                .add("source",     BackupFile::getSource)
                .add("backup",     BackupFile::getBackup)
                .build();
    
    @Override
    public String toString() {
        return TO_STRING.apply(this);
    }
}