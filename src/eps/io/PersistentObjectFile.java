/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import eps.app.*;
import eps.json.*;

import java.util.*;
import java.util.function.*;

import java.nio.file.Path;
import java.io.IOException;

public class PersistentObjectFile extends PersistentFile {
    private final Class<?> type;
    
    PersistentObjectFile(String name, Class<?> type) {
        this((PersistentDirectory) null, name, type);
    }
    
    PersistentObjectFile(PersistentDirectory parent, String name, Class<?> type) {
        super(parent, name, EXTENSION);
        this.type = Objects.requireNonNull(type, "type");
    }
    
    public final Class<?> getObjectType() {
        return type;
    }
    
    @Override
    public final boolean isDirectory() {
        return false;
    }
    
    protected byte[] toBytes(Object contents) {
        return Json.toBytes(contents, true);
    }
    
    protected Object fromBytes(byte[] bytes) {
        return Json.fromBytes(bytes);
    }
    
    @Override
    protected final boolean createNewFileImpl(Path path, Consumer<Object> action) throws IOException {
        try {
            Object def   = getDefaultValue();
            byte[] bytes = toBytes(def);
            
            FileSystem.instance().writeNew(path, bytes);
            if (action != null) {
                action.accept(def);
            }
            return true;
        } catch (RuntimeException x) {
            throw new IOException(x);
        }
    }
    
    /**
     * Reads the specified file and checks its contents.
     * <p>
     * Subclasses should override {@code isValidObject} to customize the test.
     * 
     * @param  path
     * @param  action
     * @return {@code true} if the specified file has valid contents and
     *         {@code false} otherwise.
     * @throws IOException 
     */
    @Override
    protected final boolean hasValidContents(Path path, Consumer<Object> action) throws IOException {
        if (FileSystem.instance().isRegularFile(path)) {
            Object obj = readAsJson(path);
            if (obj != READ_FAILURE && isValidObject(obj)) {
                if (action != null) {
                    action.accept(obj);
                }
                return true;
            }
        }
        return false;
    }
    
    /**
     * Subclasses should override this method to customize the contents test.
     * 
     * @return {@code true} if this object file may contain {@code null} as a
     *         value and {@code false} otherwise.
     */
    protected boolean isNullValid() {
        return true;
    }
    
    /**
     * Tests whether or not the specified object is valid for this file. By
     * default, returns {@code isNullValid()} if the object is null, otherwise
     * returns {@code getObjectType().isInstance(obj)}.
     * <p>
     * Subclasses should override this method to customize the test.
     * 
     * @param  obj the object to test.
     * @return {@code true} if the specified object is a valid value for this
     *         object file and {@code false} otherwise.
     */
    protected boolean isValidObject(Object obj) {
        if (obj == null) {
            return isNullValid();
        } else {
            return type.isInstance(obj);
        }
    }
    
    public final <T> T requireValidContents(T obj) {
        if (!isValidObject(obj)) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
        return obj;
    }
    
    protected enum ReadFailure { INSTANCE }
    protected static final Object READ_FAILURE = ReadFailure.INSTANCE;
    
    /**
     * Attempts to read the specified file as a Json object.
     * 
     * @param  path
     * @return the contents of the file, or {@code ReadFailure.INSTANCE} if
     *         there was a problem.
     * @throws IOException 
     */
    protected final Object readAsJson(Path path) throws IOException {
        if (!errorIsSet()) {
            try {
                byte[] bytes = FileSystem.instance().readAllBytes(path);
                Object obj   = fromBytes(bytes);
                return obj;
            } catch (RuntimeException x) {
                Log.caught(getClass(), "reading '" + path.toAbsolutePath() + "'", x, true);
            }
        }
        return READ_FAILURE;
    }
    
    /**
     * Reads the actual file if it already existed, or creates a new file if it
     * did not. Either way, if the operation is a success, then the contents of
     * the file are passed to the contents consumer.
     * 
     * @param  contentsOut
     * @return {@code true} if the operation was a success and {@code false} otherwise.
     * @throws NullPointerException if {@code contentsOut} is {@code null}.
     */
    public final boolean readOrCreate(Consumer<Object> contentsOut) {
        Objects.requireNonNull(contentsOut, "contentsOut");
        synchronized (this) {
            if (isInitialized()) {
                Path path = getPath();
                if (path != null) {
                    try {
                        Object obj = readAsJson(path);
                        if (obj != READ_FAILURE) {
                            contentsOut.accept(obj);
                            return true;
                        }
                    } catch (IOException x) {
                        Log.caught(getClass(), toString(), x, false);
                    }
                }
            } else {
                if (initPathAndContents(contentsOut)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Reads the file.
     * 
     * @return the contents of the file (by reading it), or the default value
     *         if there was a problem.
     */
    public final Object getContents() {
        Mutable<Object> contents = new Mutable<>();
        if (readOrCreate(contents::set)) {
            return contents.value;
        } else {
            return getDefaultValue();
        }
    }
    
    protected Object getDefaultValue() {
        return null;
    }
    
    public final boolean putContents(Object contents) {
        return putContents(contents, null);
    }
    
    /**
     * Writes the object to the file.
     * <p>
     * If the backup consumer is not {@code null}, then the backup file is
     * passed to it, if a backup was attempted.
     * 
     * @param  contents
     * @param  backupOut
     * @return {@code true} if the operation was a success and {@code false} otherwise.
     * @throws IllegalArgumentException if the specified contents is invalid
     *                                  according to {@code isValidObject}.
     */
    public final boolean putContents(Object contents, Consumer<BackupFile> backupOut) {
        if (!isValidObject(contents)) {
            throw new IllegalArgumentException("contents invalid: " + contents);
        }
        return write(contents, backupOut);
    }
    
    protected final boolean write(Object contents, Consumer<BackupFile> backupOut) {
        if (errorIsSet()) {
            return false;
        }
        try (Warden.Permit permit = PersistentFiles.acquireWritePermit(getName())) {
            if (permit.isValid()) {
                synchronized (this) {
                    Path path = getPath();
                    if (path != null) {
                        return toBytesThenWrite(path, contents, backupOut);
                    }
                }
            }
            return false;
        }
    }
    
    private boolean toBytesThenWrite(Path path, Object contents, Consumer<BackupFile> backupOut) {
        try {
            byte[] bytes = toBytes(contents);
            return backupThenWrite(path, bytes, backupOut);
        } catch (RuntimeException x) {
            setError();
            Log.caught(getClass(), toString(), x, false);
            return false;
        }
    }
    
    private boolean backupThenWrite(Path path, byte[] bytes, Consumer<BackupFile> backupOut) {
        BackupFile backup = new BackupFile(path);
        Throwable  caught = null;
        try {
            if (writeToFile(path, bytes)) {
                backup.close();
                return true;
            } else {
                return false;
            }
        } catch (Throwable x) {
            caught = x;
            throw x;
        } finally {
            if (backupOut != null) {
                try {
                    backupOut.accept(backup);
                } catch (Throwable x) {
                    if (caught != null) {
                        x.addSuppressed(caught);
                    }
                    throw x;
                }
            }
        }
    }
    
    private boolean writeToFile(Path path, byte[] bytes) {
        if (errorIsSet()) {
            return false;
        }
        try {
            FileSystem.instance().writeExisting(path, bytes);
            return true;
        } catch (IOException x) {
            setError();
            Log.caught(getClass(), toString(), x, false);
            return false;
        }
    }
    
    protected static final ToString<PersistentObjectFile> PERSISTENT_OBJECT_FILE_TO_STRING =
        PERSISTENT_FILE_TO_STRING.derive(PersistentObjectFile.class)
                                 .add("objectType", PersistentObjectFile::getObjectType)
                                 .build();
    
    @Override
    public String toString() {
        return PERSISTENT_OBJECT_FILE_TO_STRING.apply(this);
    }
    
    // static:
    
    public static final String EXTENSION = "epsilonobj";
    
    public static final String DOT_EXTENSION = "." + EXTENSION;
    
    public static boolean isObjectFile(String name) {
        if (name.endsWith(DOT_EXTENSION)) {
            return (name.length() - DOT_EXTENSION.length()) > 0;
        }
        return false;
    }
    
    public static boolean isObjectFile(Path path) {
        if (FileSystem.instance().isRegularFile(path)) {
            return isObjectFile(path.getFileName().toString());
        }
        return false;
    }
    
    public static String toObjectFile(String name) {
        if (!isObjectFile(name)) {
            name += DOT_EXTENSION;
        }
        return name;
    }
    
    public static String fromObjectFile(String name) {
        if (isObjectFile(name)) {
            name = name.substring(0, name.length() - DOT_EXTENSION.length());
        }
        return name;
    }
}