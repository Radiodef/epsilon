/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.app.*;
import eps.util.*;

import java.util.*;
import java.util.stream.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.LinkOption;
import java.nio.file.OpenOption;
import java.nio.file.CopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.InvalidPathException;
import java.io.IOException;
import java.io.UncheckedIOException;

public final class IoMisc {
    private IoMisc() {
    }
    
    private static volatile Path WORKING_DIRECTORY;
    
    public static Path getWorkingDirectory() {
        Path dir = WORKING_DIRECTORY;
        if (dir == null) {
            try {
                dir = Paths.get("", Misc.EMPTY_STRING_ARRAY);
                WORKING_DIRECTORY = dir;
            } catch (InvalidPathException x) {
                Log.caught(IoMisc.class, "getWorkingDirectory", x, false);
            }
        }
        return dir;
    }
    
    public static final LinkOption[] NO_LINK_OPTIONS = new LinkOption[0];
    
    public static final OpenOption[] NO_OPEN_OPTIONS = new OpenOption[0];
    
    public static final FileAttribute<?>[] NO_FILE_ATTRIBUTES = new FileAttribute<?>[0];
    
    public static final CopyOption[] NO_COPY_OPTIONS = new CopyOption[0];
    
    public static long countFiles(Path directory) {
        Objects.requireNonNull(directory, "directory");
        
        try (Stream<Path> list = FileSystem.instance().list(directory)) {
            return list.count();
            
        } catch (IOException | UncheckedIOException x) {
            Log.caught(IoMisc.class, "while counting files in directory '" + directory + "'", x, false);
            return -1L;
        }
    }
    
    public static Pair<String, String> splitFileName(Path path) {
        Objects.requireNonNull(path, "path");
        return splitFileName(path.getFileName().toString());
    }
    
    public static Pair<String, String> splitFileName(String name) {
        Objects.requireNonNull(name, "name");
        
        int dot = name.lastIndexOf('.');
        if (dot <= 0) {
            return Pair.of(name, null);
        }
        
        String extension = name.substring(dot + 1, name.length());
        name = name.substring(0, dot);
        
        return Pair.of(name, extension);
    }
    
    public static Path getParent(Path path) {
        Path parent = path.getParent();
        if (parent == null) {
            return path.toAbsolutePath().getParent();
        } else {
            return parent;
        }
    }
    
    public static Path abs(Path path) {
        if (path == null) {
            return null;
        } else {
            return path.toAbsolutePath();
        }
    }
    
    public static boolean absEquals(Path lhs, Path rhs) {
        return Objects.equals(abs(lhs), abs(rhs));
    }
}