/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import eps.app.*;
import eps.eval.*;
import eps.json.*;
import eps.ui.*;
import eps.job.*;
import static eps.util.Misc.*;
import static eps.io.IoMisc.abs;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;

public final class PersistentFiles {
    private PersistentFiles() {
    }
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ *//* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    // constants
    
    public static final PersistentDirectory APP_FILES_DIRECTORY = new PersistentDirectory("EpsilonApplicationFiles");
    
    public static final PersistentDirectory LOG_DIRECTORY =
        new PersistentDirectory(APP_FILES_DIRECTORY, "epsilon_logs");
    
    public static final PersistentDirectory THEME_DIRECTORY =
        new PersistentDirectory(APP_FILES_DIRECTORY, "epsilon_themes");
    
    public static final PersistentDirectory MISC_DIRECTORY =
        new PersistentDirectory(APP_FILES_DIRECTORY, "epsilon_misc");
    
    public static final PersistentObjectFile SETTINGS_FILE =
            new PersistentObjectFile(APP_FILES_DIRECTORY, "epsilon_settings", Settings.Concurrent.class) {
        @Override
        protected boolean isNullValid() {
            return false;
        }
        @Override
        protected Settings.Concurrent getDefaultValue() {
            return Settings.Concurrent.ofDefaults();
        }
        @Override
        protected byte[] toBytes(Object contents) {
            Mutable<byte[]> bytes = new Mutable<>();
            ((Settings.Concurrent) contents).doSynchronized(sets -> {
                try (Gate gate = sets.block()) {
                    bytes.value = Json.toBytes(sets, true);
                }
            });
            return bytes.value;
        }
    };
    
    public static final PersistentObjectFile SYMBOLS_FILE =
            new PersistentObjectFile(APP_FILES_DIRECTORY, "epsilon_symbols", Symbols.Concurrent.class) {
        @Override
        protected boolean isNullValid() {
            return false;
        }
        @Override
        protected Symbols.Concurrent getDefaultValue() {
            return Symbols.Concurrent.ofDefaults();
        }
        @Override
        protected byte[] toBytes(Object contents) {
            try (Symbols syms = ((Symbols.Concurrent) contents).block()) {
                return Json.toBytes(syms, true);
            }
        }
    };
    
    public static final PersistentObjectFile ITEMS_FILE =
            new PersistentObjectFile(APP_FILES_DIRECTORY, "epsilon_items", List.class) {
        @Override
        protected boolean isNullValid() {
            return false;
        }
        @Override
        protected boolean isValidObject(Object obj) {
            if (obj instanceof List<?>) {
                for (Object e : (List<?>) obj) {
                    if (!(e instanceof Item.Info)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        @Override
        protected List<Item.Info> getDefaultValue() {
            return new ArrayList<>(0);
        }
        @Override
        protected byte[] toBytes(Object contents) {
            // TODO?
            return super.toBytes(contents);
        }
    };
    
    public static final List<PersistentFile> VALUES = Misc.collectConstants(PersistentFiles.class, PersistentFile.class);
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ *//* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    // not constants below
    
    private static final Warden WRITE_WARDEN = new Warden(PersistentFiles.class.getName() + ".WRITE_WARDEN");
    
    static Warden.Permit acquireWritePermit(String description) {
        return WRITE_WARDEN.acquire(description);
    }
    
    public static void exitingAbruptly() {
        WRITE_WARDEN.closeAndWait();
    }
    
    enum AppWorkerSupplier implements Consumer<Consumer<Worker>> {
        WORKER1(App::getWorker),
        WORKER2(App::getWorker2);
        
        private final Function<App, Worker> func;
        
        AppWorkerSupplier(Function<App, Worker> func) {
            this.func = Objects.requireNonNull(func);
        }
        
        @Override
        public void accept(Consumer<Worker> action) {
            Objects.requireNonNull(action, "action");
            App.whenAvailable(app -> action.accept(func.apply(app)));
        }
    }
    
    private static volatile Consumer<Consumer<Worker>> WORKER_SUPPLIER = AppWorkerSupplier.WORKER2;
    
    static void setWorkerSupplier(Consumer<Consumer<Worker>> supplier) {
        Objects.requireNonNull(supplier, "supplier");
        WORKER_SUPPLIER = supplier;
    }
    
    private static void usingWorker(Consumer<Worker> action) {
        WORKER_SUPPLIER.accept(action);
    }
    
    public static Settings.Concurrent readSettings() {
        Settings.Concurrent sets = (Settings.Concurrent) SETTINGS_FILE.getContents();
        
        int newCount = 0;
        
        for (Setting<?> set : Setting.values()) {
            if (!sets.contains(set)) {
                sets.putDefault(set);
                
                if (!set.isTransient())
                    ++newCount;
            }
        }
        
        if (newCount > 0) {
            writeSettings(sets);
        }
        
        return sets;
    }
    
    public static Symbols.Concurrent readSymbols() {
        Symbols.Concurrent syms = Symbols.Concurrent.empty();
        readSymbols(syms);
        return syms;
    }
    
    public static void readSymbols(Symbols.Concurrent appSyms) {
        Objects.requireNonNull(appSyms, "appSyms");
        
        Symbols ints = Symbols.Concurrent.ofIntrinsics();
        Symbols prev = Symbols.setLocalSymbols(ints);
        try {
            Symbols.Concurrent syms = (Symbols.Concurrent) SYMBOLS_FILE.getContents();
            
            // add any intrinsic symbols which were added to the program
            // since the last time the symbols file was written
            List<Symbol> add =
                ints.stream()
                    .filter(i -> !syms.contains(i))
                    .collect(Collectors.toList());
            for (Symbol i : add)
                for (Symbol alias : i.createPreferredAliases())
                    for (Symbol sym : alias.getSymbols())
                        syms.put(sym);
            if (!add.isEmpty()) {
                writeSymbols(syms);
            }
            
            appSyms.clear();
            appSyms.putAll(syms);
        } finally {
            Symbols.setLocalSymbols(prev);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static List<Item.Info> readItems() {
        return (List<Item.Info>) ITEMS_FILE.getContents();
    }
    
    public static void writeSettings(Settings.Concurrent sets) {
        write(SETTINGS_FILE, sets);
    }
    
    public static void writeSymbols(Symbols.Concurrent syms) {
        write(SYMBOLS_FILE, syms);
    }
    
    public static void writeItems(List<Item.Info> items) {
        write(ITEMS_FILE, items);
    }
    
    private static void write(PersistentObjectFile file, Object contents) {
        Objects.requireNonNull(file, "file");
        file.requireValidContents(contents);
        
        if (file.errorIsSet()) {
            return;
        }
        
        class WriteJob extends AbstractJob {
            private final PersistentObjectFile file;
            private final Object contents;
            
            WriteJob(PersistentObjectFile file, Object contents) {
                this.file     = file;
                this.contents = contents;
            }
            
            @Override
            protected void executeImpl() {
                writeImpl(file, contents);
            }
            
            @Override
            protected void doneImpl() {
            }
        }
        
        usingWorker(worker -> {
            Job job = new WriteJob(file, contents);
            worker.enqueue(job, true);
        });
    }
    
    private static boolean writeImpl(PersistentObjectFile file, Object contents) {
        if (file.errorIsSet()) {
            return false;
        }
        
        Mutable<BackupFile> backup = new Mutable<>();
        
        if (file.putContents(contents, backup::set)) {
            return true;
        }
        
        if (backup.value != null && backup.value.backupSuccess()) {
            App.whenAvailable(app -> app.addItemOnInterfaceThread(item -> {
                String msg = f("An error occurred while writing to the file %s. "
                              + "A backup of its previous value has been created at %s "
                              + "in case the error caused the file to be corrupted. "
                              + "No further attempts to write to this file will be "
                              + "performed.",
                                file.getFileName(), backup.value.getBackup().toAbsolutePath());
                
                TextAreaCell err = app.getComponentFactory().newTextArea(msg);
                item.addCell(err.followTextStyle(Setting.UNEXPECTED_ERROR_STYLE));
            }));
        }
        return false;
    }
    
    public static void readAndPutThemes() {
        readThemes().forEach(Theme::put);
    }
    
    static List<Theme> readThemes() {
        synchronized (THEME_DIRECTORY) {
            Path path = THEME_DIRECTORY.getPath();
            return readThemes(path);
        }
    }
    
    private static List<Theme> readThemes(Path path) {
        List<Theme> themes = new ArrayList<>();
        
        if (path != null) {
            try (Stream<Path> files = FileSystem.instance().list(path)) {
                files.map(PersistentFiles::readTheme)
                     .filter(Objects::nonNull)
                     .forEachOrdered(themes::add);
            } catch (IOException | UncheckedIOException x) {
                Log.caught(PersistentFiles.class, "reading " + abs(path), x, false);
            }
        }
        
        return themes;
    }
    
    private static Theme readTheme(Path file) {
        Mutable<Theme> result = new Mutable<>();
        if (readTheme(file, result)) {
            return result.value;
        } else {
            return null;
        }
    }
    
    private static boolean isTheme(Path file) {
        return readTheme(file, null);
    }
    
    private static boolean readTheme(Path file, Mutable<Theme> result) {
        if (Log.caughtError()) {
            return false;
        }
        if (PersistentObjectFile.isObjectFile(file)) {
            try {
                byte[] bytes = FileSystem.instance().readAllBytes(file);
                Theme  theme = (Theme) Json.fromBytes(bytes);
                if (theme == null) {
                    Log.note(PersistentFiles.class, "readTheme", "found null theme in ", abs(file));
                }
                if (result != null) {
                    result.set(theme);
                }
                return true;
            } catch (IOException | RuntimeException x) {
                Log.caught(PersistentFiles.class, "reading " + abs(file), x, false);
            }
        }
        return false;
    }
    
    public static void writeThemes() {
        writeThemes(Theme.values());
    }
    
    static int writeThemes(List<Theme> themes) {
        synchronized (THEME_DIRECTORY) {
            Path path = THEME_DIRECTORY.getPath();
            if (path != null) {
                return writeThemes(path, themes);
            }
            return 0;
        }
    }
    
    private static int writeThemes(Path parent, List<Theme> themes) {
        Set<Path> used = new LinkedHashSet<>();
        
        for (Theme theme : themes) {
            if (!theme.isPreset()) {
                Path path = writeTheme(parent, theme);
                if (path != null) {
                    used.add(path.getFileName());
                }
            }
        }
        
        if (!Log.caughtError()) {
            cleanThemeDirectory(parent, used);
        }
        
        return used.size();
    }
    
    private static void cleanThemeDirectory(Path parent, Set<Path> used) {
        try (Stream<Path> files = FileSystem.instance().list(parent)) {
            files.filter(file -> !used.contains(file.getFileName()))
                 .filter(PersistentFiles::isTheme)
                 .forEachOrdered(file -> {
                Log.note(PersistentFiles.class, "writeThemes", "found unused theme file ", abs(file));
                try {
                    FileSystem.instance().deleteIfExists(file);
                } catch (IOException x) {
                    throw new UncheckedIOException(x);
                }
            });
        } catch (IOException | RuntimeException x) {
            Log.caught(PersistentFiles.class, "while filtering " + abs(parent), x, false);
        }
    }
    
    private static Path writeTheme(Path parent, Theme theme) {
        if (!Log.caughtError()) {
            try {
                Path   path  = parent.resolve(PersistentObjectFile.toObjectFile(theme.getName()));
                byte[] bytes = Json.toBytes(theme, true);
                
                // use any custom options?
                FileSystem.instance().write(path, bytes);
                return path;
                
            } catch (IOException | RuntimeException x) {
                Log.caught(PersistentFiles.class, f("parent: %s, theme: %s", abs(parent), theme.getName()), x, false);
            }
        }
        return null;
    }
}