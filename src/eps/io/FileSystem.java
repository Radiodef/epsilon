/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import java.util.stream.*;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.nio.file.Path;
import java.nio.file.LinkOption;
import java.nio.file.OpenOption;
import java.nio.file.CopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.charset.CharsetEncoder;

public interface FileSystem {
    boolean exists(Path path);
    
    default boolean notExists(Path path) {
        return !exists(path);
    }
    
    default boolean isDirectory(Path path) {
        return isDirectory(path, IoMisc.NO_LINK_OPTIONS);
    }
    
    boolean isDirectory(Path path, LinkOption... opts);
    
    default boolean isRegularFile(Path path) {
        return isRegularFile(path, IoMisc.NO_LINK_OPTIONS);
    }
    
    boolean isRegularFile(Path path, LinkOption... opts);
    
    default String readString(Path path) throws IOException {
        return readString(path, getDefaultCharset());
    }
    
    default String readString(Path path, Charset charset) throws IOException {
        byte[] bytes = readAllBytes(path);
        return new String(bytes, charset);
    }
    
    byte[] readAllBytes(Path path) throws IOException;
    
    default void write(Path path, byte[] bytes) throws IOException {
        write(path, bytes, IoMisc.NO_OPEN_OPTIONS);
    }
    
    default void writeNew(Path path, byte[] bytes) throws IOException {
        write(path, bytes, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
    }
    
    default void writeExisting(Path path, byte[] bytes) throws IOException {
        write(path, bytes, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
    }
    
    void write(Path path, byte[] bytes, OpenOption... opts) throws IOException;
    
    void createDirectory(Path path) throws IOException;
    
    Stream<Path> list(Path path) throws IOException;
    
    default Path copy(Path from, Path to) throws IOException {
        return copy(from, to, IoMisc.NO_COPY_OPTIONS);
    }
    
    Path copy(Path from, Path to, CopyOption... opts) throws IOException;
    
    boolean deleteIfExists(Path path) throws IOException;
    
    default Charset getDefaultCharset() {
        return StandardCharsets.UTF_8;
    }
    
    default BufferedWriter newBufferedWriter(Path path, OpenOption... opts) throws IOException {
        return newBufferedWriter(path, getDefaultCharset(), opts);
    }
    
    default BufferedWriter newBufferedWriter(Path path, Charset charset, OpenOption... opts) throws IOException {
        CharsetEncoder encoder = charset.newEncoder();
        OutputStream out = newOutputStream(path, opts);
        try {
            return new BufferedWriter(new OutputStreamWriter(out, encoder));
        } catch (Throwable x) {
            try {
                out.close();
            } catch (Throwable y) {
                y.addSuppressed(x);
                throw y;
            }
            throw x;
        }
    }
    
    OutputStream newOutputStream(Path path, OpenOption... opts) throws IOException;
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ *//* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    // static
    
    static FileSystem instance() {
        return FileSystemPrivate.getFileSystem();
    }
    
    static void setInstance(FileSystem instance) {
        FileSystemPrivate.setFileSystem(instance);
    }
}

final class FileSystemPrivate {
    private FileSystemPrivate() {
    }
    
    private static volatile FileSystem fileSystem = NoFileSystem.INSTANCE;
    
    static FileSystem getFileSystem() {
        return FileSystemPrivate.fileSystem;
    }
    
    static void setFileSystem(FileSystem fileSystem) {
        if (fileSystem == null) {
            fileSystem = NoFileSystem.INSTANCE;
        }
        FileSystemPrivate.fileSystem = fileSystem;
    }
}