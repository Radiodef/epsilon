/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.util.*;
import eps.json.*;
import eps.block.*;
import eps.ui.*;
import static eps.app.App.*;
import static eps.util.Literals.*;

import java.util.*;

@JsonSerializable
public abstract class EchoFormatter {
    public abstract StringBuilder format(StringBuilder b, Context context, Node node);
    
    public abstract Block toBlock(StringBuilder temp, Context context, Node node);
    
    protected StringBuilder toEchoString(Node node, StringBuilder b, Context context) {
        return node.toEchoString(b, context);
    }
    
    protected CodePoint getLeftParenthesis(Context context) {
        return context.getSetting(Setting.LEFT_PARENTHESIS);
    }
    
    protected CodePoint getRightParenthesis(Context context) {
        return context.getSetting(Setting.RIGHT_PARENTHESIS);
    }
    
    protected Block parenthesize(Context context, Block block) {
        CodePoint lp = getLeftParenthesis(context);
        CodePoint rp = getRightParenthesis(context);
        if (lp != null && rp != null) {
            Block lb = Block.of(lp);
            Block rb = Block.of(rp);
            
            lb = new StyleBlock(lb).set(Setting.BRACKET_STYLE);
            rb = new StyleBlock(rb).set(Setting.BRACKET_STYLE);
            
            block = new JoinBlock(3).addBlock(lb).addBlock(block).addBlock(rb);
        }
        return block;
    }
    
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        return this.getClass() == obj.getClass();
    }
    
    @Override
    public String toString() {
        return toString(getProperties());
    }
    
    protected String getSimpleName() {
        Class<?> cls  = getClass();
        String   name = cls.getSimpleName();
        do {
            if (name != null) {
                if ((cls = cls.getEnclosingClass()) != null) {
                    String encl = cls.getSimpleName();
                    if (encl != null) {
                        name = encl + "." + name;
                        break;
                    }
                } else {
                    break;
                }
            }
            name = cls.getName();
        } while (false);
        return name;
    }
    
    protected Map<String, Object> getProperties() {
        return new LinkedHashMap<>(0);
    }
    
    protected String toString(Map<String, Object> props) {
        if (props == null || props.isEmpty()) {
            return getSimpleName();
        }
        StringBuilder b = new StringBuilder();
        b.append(getSimpleName()).append("(");
        int i = 0;
        for (Map.Entry<String, Object> p : props.entrySet()) {
            if (i++ != 0) b.append(", ");
            b.append(p.getKey()).append(" = ").append(p.getValue());
        }
        return b.append(")").toString();
    }
    
    @JsonSerializable
    public static class OfVariableName extends EchoFormatter {
        protected String getName(Variable var) {
            return var.getName();
        }
        
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node) {
            Variable var = (Variable) ((Node.OfOperand) node).get();
            return b.append(this.getName(var));
        }
        
        protected Setting<Style> getStyle(Context context, Variable var) {
            return Setting.NUMBER_STYLE;
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node) {
            Variable       var   = (Variable) ((Node.OfOperand) node).get();
            Setting<Style> style = this.getStyle(context, var);
            temp.setLength(0);
            return new StyleBlock(Block.of(this.format(temp, context, node))).set(style).setToken(node);
        }
    }
    
    public static final EchoFormatter OF_VARIABLE_NAME = new EchoFormatter.OfVariableName();
    
    @JsonSerializable
    public static class OfVariableLiteral extends EchoFormatter.OfVariableName {
        @JsonProperty
        private final String literal;
        
        @JsonConstructor
        public OfVariableLiteral(String literal) {
            this.literal = Objects.requireNonNull(literal, "literal");
        }
        
        @Override
        protected Map<String, Object> getProperties() {
            return LinkedHashMapOf("literal",  literal);
        }
        
        @Override
        public int hashCode() {
            return 31 * super.hashCode() + Objects.hashCode(literal);
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj) && Objects.equals(this.literal, ((OfVariableLiteral) obj).literal);
        }
        
        @Override
        protected String getName(Variable var) {
            return this.literal;
        }
    }
    
    public static EchoFormatter ofVariable(String literal) {
        return new EchoFormatter.OfVariableLiteral(literal);
    }
    
    @JsonSerializable
    public static class OfLiteral extends EchoFormatter {
        @JsonProperty
        private final String literal;
        
        @JsonConstructor
        public OfLiteral(String literal) {
            this.literal = Objects.requireNonNull(literal, "literal");
        }
        
        @Override
        protected Map<String, Object> getProperties() {
            return LinkedHashMapOf("literal", literal);
        }
        
        @Override
        public int hashCode() {
            return 31 * super.hashCode() + Objects.hashCode(literal);
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj) && Objects.equals(this.literal, ((OfLiteral) obj).literal);
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node) {
            return b.append(this.literal);
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node) {
            return new TextBlock(this.literal).setToken(node);
        }
    }
    
    public static EchoFormatter of(String literal) {
        return new EchoFormatter.OfLiteral(literal);
    }
    
    @JsonSerializable
    public static class OfBinaryOp extends EchoFormatter {
        @JsonProperty
        private final boolean isLeftSpaced;
        @JsonProperty
        private final boolean isRightSpaced;
        
        @JsonConstructor
        public OfBinaryOp(boolean isLeftSpaced, boolean isRightSpaced) {
            this.isLeftSpaced  = isLeftSpaced;
            this.isRightSpaced = isRightSpaced;
        }
        
        @Override
        protected Map<String, Object> getProperties() {
            return LinkedHashMapOf("isLeftSpaced",  isLeftSpaced,
                                   "isRightSpaced", isRightSpaced);
        }
        
        @Override
        public int hashCode() {
            int hash = super.hashCode();
            hash = 31 * hash + Boolean.hashCode(isLeftSpaced);
            hash = 31 * hash + Boolean.hashCode(isRightSpaced);
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (super.equals(obj)) {
                OfBinaryOp that = (OfBinaryOp) obj;
                return this.isLeftSpaced  == that.isLeftSpaced
                    && this.isRightSpaced == that.isRightSpaced;
            }
            return false;
        }
        
        public boolean isLeftSpaced()  { return this.isLeftSpaced;  }
        public boolean isRightSpaced() { return this.isRightSpaced; }
        
        protected String getName(BinaryOp op) {
            return op.getName();
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node0) {
            Node.OfBinaryOp node = (Node.OfBinaryOp) node0;
            
            BinaryOp op  = node.getOperator();
            Node     lhs = node.getLeftHandSide();
            Node     rhs = node.getRightHandSide();
            
            this.toEchoString(lhs, b, context);
            
            if (this.isLeftSpaced())
                b.append(' ');
            b.append(this.getName(op));
            if (this.isRightSpaced())
                b.append(' ');
            
            this.toEchoString(rhs, b, context);
            
            return b;
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfBinaryOp node = (Node.OfBinaryOp) node0;
            
            JoinBlock join = new JoinBlock(3);
            
            Block lhs = node.getLeftHandSide().toBlock(temp, context);
            Block rhs = node.getRightHandSide().toBlock(temp, context);
            Block op  = new TextBlock(this.getName(node.getOperator())).setToken(node);
            
            join.addBlock(lhs);
            if (this.isLeftSpaced())
                join.addBlock(Block.of(' '));
            join.addBlock(op);
            if (this.isRightSpaced())
                join.addBlock(Block.of(' '));
            join.addBlock(rhs);
            
            return join;
        }
    }
    
    public static final EchoFormatter OF_BINARY_OP = new EchoFormatter.OfBinaryOp(false, false);
    
    public static final EchoFormatter OF_SPACED_BINARY_OP = new EchoFormatter.OfBinaryOp(true, true);
    
    public static final EchoFormatter OF_LEFT_SPACED_BINARY_OP = new EchoFormatter.OfBinaryOp(true, false);
    
    public static final EchoFormatter OF_RIGHT_SPACED_BINARY_OP = new EchoFormatter.OfBinaryOp(false, true);
    
    // TODO: this doesn't work while nesting superscripts like 2^(n^m)
    @JsonSerializable
    public static class OfExponentialBinaryOp extends EchoFormatter.OfBinaryOp {
        public OfExponentialBinaryOp() {
            super(false, false);
        }
        
        @JsonConstructor
        public OfExponentialBinaryOp(boolean a, boolean b) {
            super(a, b);
        }
        
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfBinaryOp node = (Node.OfBinaryOp) node0;
            
            if (context.getSetting(Setting.DO_NOT_NEST_HTML_SUPERSCRIPTS)) {
                boolean exponentHasExponent = node.getRightHandSide().find(n -> {
                    if (n.getNodeKind().isBinaryOp()) {
                        BinaryOp op = ((Node.OfBinaryOp) n).getOperator();
                        return op.getEchoFormatter() instanceof EchoFormatter.OfExponentialBinaryOp;
                    }
                    return false;
                });
                if (exponentHasExponent) {
                    return super.toBlock(temp, context, node0);
                }
            }
            
            Block lhs = node.getLeftHandSide().toBlock(temp, context);
            Block rhs = node.getRightHandSide().toBlock(temp, context);
            
            return Block.join(lhs, new EmptyBlock(node.getToken()), new SuperscriptBlock(rhs));
        }
    }
    
    public static final EchoFormatter OF_EXP_BINARY_OP = new EchoFormatter.OfExponentialBinaryOp();
    
    @JsonSerializable
    public static class OfNthRoot extends EchoFormatter {
        @JsonConstructor
        public OfNthRoot() {
        }
        
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node) {
            switch (node.getNodeKind()) {
                case UNARY_OP:
                    return OF_FUNCTION_LIKE.format(b, context, node);
                case BINARY_OP:
                    return OF_SPACED_BINARY_OP.format(b, context, node);
                default:
                    throw new IllegalArgumentException(node.toString());
            }
        }
        
        protected Block toDefaultBlock(StringBuilder temp, Context context, Node node) {
            switch (node.getNodeKind()) {
                case UNARY_OP:
                    return OF_FUNCTION_LIKE.toBlock(temp, context, node);
                case BINARY_OP:
                    return OF_SPACED_BINARY_OP.toBlock(temp, context, node);
                default:
                    throw new IllegalArgumentException(node.toString());
            }
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node) {
            Node index;
            Node radic;
            
            switch (node.getNodeKind()) {
                case BINARY_OP:
                    Node.OfBinaryOp binary = (Node.OfBinaryOp) node;
                    index = binary.getLeftHandSide();
                    radic = binary.getRightHandSide();
                    break;
                case UNARY_OP:
                    Node operand = ((Node.OfUnaryOp) node).getOperand().getExpressionNode();
                    if (operand.isNaryOp()) {
                        Node.OfNaryOp nary = (Node.OfNaryOp) operand;
                        if (nary.getOperator().getDelegate() == NaryOp.TUPLE) {
                            List<Node> operands = nary.getOperands();
                            if (operands.size() == 2) {
                                index = operands.get(0);
                                radic = operands.get(1);
                                break;
                            }
                        }
                    }
                    return toDefaultBlock(temp, context, node);
                default:
                    return toDefaultBlock(temp, context, node);
            }
            
            Block indexBk = index.toBlock(temp, context);
            
            if (!index.isOperand() && !index.isSpanOp()) {
                Block parenBk = parenthesize(context, indexBk);
                if (parenBk == indexBk) {
                    return toDefaultBlock(temp, context, node);
                }
                indexBk = parenBk;
            }
            
            indexBk = new SuperscriptBlock(indexBk);
            
            Block radicBk = radic.toBlock(temp, context);
            
            if (!radic.isOperand() && !radic.isSpanOp()) {
                Block parenBk = parenthesize(context, radicBk);
                if (parenBk == radicBk) {
                    return toDefaultBlock(temp, context, node);
                }
                radicBk = parenBk;
            }
            
            Block radixBk = Block.of('√');
            Block block   = Block.join(indexBk, radixBk, radicBk);
            
            (node.isBinaryOp() ? radixBk : block).setToken(node);
            
            return block;
        }
    }
    
    public static final EchoFormatter OF_NTH_ROOT = new EchoFormatter.OfNthRoot();
    
    @JsonSerializable
    public static class OfSubscriptBinaryOp extends EchoFormatter.OfBinaryOp {
        @JsonConstructor
        public OfSubscriptBinaryOp(boolean a, boolean b) {
            super(a, b);
        }
        
        public OfSubscriptBinaryOp() {
            super(false, false);
        }
        
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfBinaryOp node = (Node.OfBinaryOp) node0;
            
            // These could perhaps be nested laterally as (0, 1, 2).
            if (context.getSetting(Setting.DO_NOT_NEST_HTML_SUPERSCRIPTS)) {
                boolean subscriptHasSubscript = node.getRightHandSide().find(n -> {
                    if (n.getNodeKind().isBinaryOp()) {
                        BinaryOp op = ((Node.OfBinaryOp) n).getOperator();
                        return op.getEchoFormatter() instanceof EchoFormatter.OfSubscriptBinaryOp;
                    }
                    return false;
                });
                if (subscriptHasSubscript) {
                    return super.toBlock(temp, context, node0);
                }
            }
            
            Block lhs = node.getLeftHandSide().toBlock(temp, context);
            Block rhs = node.getRightHandSide().toBlock(temp, context);
            
            return Block.join(lhs, new EmptyBlock(node.getToken()), new SubscriptBlock(rhs));
        }
    }
    
    public static final EchoFormatter OF_SUB_BINARY_OP = new EchoFormatter.OfSubscriptBinaryOp();
    
    @JsonSerializable
    public static class OfBinaryLiteral extends EchoFormatter.OfBinaryOp {
        @JsonProperty
        private final String literal;
        
        @JsonConstructor
        public OfBinaryLiteral(boolean isLeftSpaced, boolean isRightSpaced, String literal) {
            super(isLeftSpaced, isRightSpaced);
            this.literal = Objects.requireNonNull(literal, "literal");
        }
        
        @Override
        protected Map<String, Object> getProperties() {
            Map<String, Object> props = super.getProperties();
            props.put("literal", literal);
            return props;
        }
        
        @Override
        public int hashCode() {
            return 31 * super.hashCode() + Objects.hashCode(literal);
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj) && Objects.equals(this.literal, ((OfBinaryLiteral) obj).literal);
        }
        
        @Override
        protected String getName(BinaryOp op) {
            return this.literal;
        }
    }
    
    public static EchoFormatter ofBinary(String literal, boolean isSpaced) {
        return new EchoFormatter.OfBinaryLiteral(isSpaced, isSpaced, literal);
    }
    
    @JsonSerializable
    public static class OfEmptyMultiply extends EchoFormatter {
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        protected StringBuilder formatNonEmpty(StringBuilder b, Context context, Node node) {
            return BinaryOp.MULTIPLY.getEchoFormatter().format(b, context, node);
        }
        protected Block toNonEmptyBlock(StringBuilder temp, Context context, Node node) {
            return BinaryOp.MULTIPLY.getEchoFormatter().toBlock(temp, context, node);
        }
        protected StringBuilder formatEmpty(StringBuilder b, Context context, Node node) {
            ((Node.OfBinaryOp) node).getLeftHandSide().toEchoString(b, context);
            ((Node.OfBinaryOp) node).getRightHandSide().toEchoString(b, context);
            return b;
        }
        protected Block toEmptyBlock(StringBuilder temp, Context context, Node node) {
            Block lhs = ((Node.OfBinaryOp) node).getLeftHandSide().toBlock(temp, context);
            Block rhs = ((Node.OfBinaryOp) node).getRightHandSide().toBlock(temp, context);
            return Block.join(lhs, Block.empty().setToken(node), rhs);
        }
        
        protected boolean parenthesize(StringBuilder b, Context c, Node node) {
            CodePoint lp = getLeftParenthesis(c);
            CodePoint rp = getRightParenthesis(c);
            if (lp != null && rp != null) {
                b.append(lp);
                node.toEchoString(b, c);
                b.append(rp);
                return true;
            }
            return false;
        }
        protected Block parenthesizeBk(StringBuilder temp, Context c, Node node) {
            Block block = node.toBlock(temp, c);
            Block paren = parenthesize(c, block);
            if (block == paren) {
                return null;
            }
            return paren.takeTokenFrom(block);
        }
        
        protected boolean areEmptyOperands(Node lhs, Node rhs) {
            if (lhs.isOperand() && rhs.isOperand()) {
                Operand opL = ((Node.OfOperand) lhs).get();
                Operand opR = ((Node.OfOperand) rhs).get();
                return (opL.isNumber() && opR.isVariable()) || (opL.isVariable() && opR.isNumber());
            }
            return false;
        }
        protected boolean eitherIsSpan(Node lhs, Node rhs) {
            if (lhs.isSpanOp() && rhs.isSpanOp())
                return true;
            if (lhs.isSpanOp() && rhs.isOperand())
                return true;
            if (lhs.isOperand() && rhs.isSpanOp())
                return true;
            return false;
        }
        
        protected boolean isPossible(Node node) {
            return node.isOperand() || node.isSpanOp();
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node0) {
            Node.OfBinaryOp node = (Node.OfBinaryOp) node0;
            
            Node lhs = node.getLeftHandSide();
            Node rhs = node.getRightHandSide();
            int  len = b.length();
            
            if (areEmptyOperands(lhs, rhs) || eitherIsSpan(lhs, rhs)) {
                return formatEmpty(b, context, node);
            }
            if (lhs.isOperand()) {
                lhs.toEchoString(b, context);
                if (parenthesize(b, context, rhs)) {
                    return b;
                }
            }
            
            b.setLength(len);
            return formatNonEmpty(b, context, node);
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfBinaryOp node = (Node.OfBinaryOp) node0;
            
            Node lhs = node.getLeftHandSide();
            Node rhs = node.getRightHandSide();
            
            if (areEmptyOperands(lhs, rhs) || eitherIsSpan(lhs, rhs)) {
                return toEmptyBlock(temp, context, node);
            }
            if (lhs.isOperand()) {
                Block blockR = parenthesizeBk(temp, context, rhs);
                if (blockR != null) {
                    Block blockL = lhs.toBlock(temp, context);
                    return Block.join(blockL, Block.empty().setToken(node), blockR);
                }
            }
            
            return toNonEmptyBlock(temp, context, node);
        }
    }
    
    public static final EchoFormatter OF_EMPTY_MULTIPLY = new OfEmptyMultiply();
    
    @JsonSerializable
    public static class OfSpanOp extends EchoFormatter {
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node0) {
            Node.OfSpanOp node = (Node.OfSpanOp) node0;
            
            SpanOp op = node.getOperator();
            b.append(op.getLeft().getName());
            
            this.toEchoString(node.getOperand(), b, context);
            
            b.append(op.getRight().getName());
            
            return b;
        }
        
        protected Setting<Style> getStyle(Context context, SpanOp span) {
            return span.isLiteral()     ? Setting.LITERAL_STYLE
                 : span.isParenthesis() ? Setting.BRACKET_STYLE
                 : null;
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfSpanOp node = (Node.OfSpanOp) node0;
            SpanOp        span = node.getOperator();
            JoinBlock     join = new JoinBlock(3);
            
            Block left  = new TextBlock(span.getLeft().getName()).setToken(node.getLeftMostToken());
            Block right = new TextBlock(span.getRight().getName()).setToken(node.getRightMostToken());
            Block op    = node.getOperand().toBlock(temp, context);
            
            Setting<Style> style = this.getStyle(context, span);
            if (style != null) {
                left  = new StyleBlock(left).set(style).takeTokenFrom(left);
                right = new StyleBlock(right).set(style).takeTokenFrom(right);
            }
            
            return join.addBlock(left).addBlock(op).addBlock(right);
        }
    }
    
    public static final EchoFormatter OF_SPAN_OP = new EchoFormatter.OfSpanOp();
    
    @JsonSerializable
    public static class OfBracketSpanOp extends OfSpanOp {
        @JsonConstructor
        public OfBracketSpanOp() {
        }
        
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        @Override
        protected Setting<Style> getStyle(Context context, SpanOp span) {
            return Setting.BRACKET_STYLE;
        }
    }
    
    public static final EchoFormatter OF_BRACKET_SPAN_OP = new EchoFormatter.OfBracketSpanOp();
    
    @JsonSerializable
    public static class OfUnaryOp extends EchoFormatter {
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        protected String getName(UnaryOp unary) {
            return unary.getName();
        }
        
        protected boolean shouldSpace(UnaryOp unary, Node operand) {
            if (operand.getNodeKind().isSpanOp()) {
                return false;
            }
            
            IntSet bounds = app().getSetting(Setting.BOUNDARY_CHARS_AS_INT);
            String name   = this.getName(unary);
            int    len    = name.length();
            
            if (len > 0) {
                if (unary.isPrefix()) {
                    return !bounds.containsAsInt(name.codePointBefore(len));
                }
                if (unary.isSuffix()) {
                    return !bounds.containsAsInt(name.codePointAt(0));
                }
            }
            return false;
        }
        
        protected void appendName(StringBuilder b, UnaryOp unary, Node operand) {
            String name = this.getName(unary);
            
            boolean shouldSpace = this.shouldSpace(unary, operand);
            
            if (unary.isSuffix() && shouldSpace) {
                b.append(' ');
            }
            
            b.append(name);
            
            if (unary.isPrefix() && shouldSpace) {
                b.append(' ');
            }
        }
        
        @Override
        @SuppressWarnings("fallthrough")
        public StringBuilder format(StringBuilder b, Context context, Node node0) {
            Node.OfUnaryOp node    = (Node.OfUnaryOp) node0;
            UnaryOp        unary   = node.getOperator();
            Node           operand = node.getOperand();
            
            if (unary.isPrefix()) {
                this.appendName(b, unary, operand);
            }
            
            switch (operand.getNodeKind()) {
                // case UNARY_OP: ?
                case BINARY_OP:
                    CodePoint lp = this.getLeftParenthesis(context);
                    CodePoint rp = this.getRightParenthesis(context);
                    
                    if (lp != null && rp != null) {
                        b.appendCodePoint(lp.intValue());
                        this.toEchoString(operand, b, context);
                        b.appendCodePoint(rp.intValue());
                        break;
                    } // else fall through
                    
                default:
                    this.toEchoString(operand, b, context);
                    break;
            }
            
            if (unary.isSuffix()) {
                this.appendName(b, unary, operand);
            }
            
            return b;
        }
        
        @Override
        @SuppressWarnings("fallthrough")
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfUnaryOp node    = (Node.OfUnaryOp) node0;
            UnaryOp        unary   = node.getOperator();
            Node           operand = node.getOperand();
            
            Block     op   = new TextBlock(this.getName(unary)).setToken(node);
            JoinBlock join = new JoinBlock(2);
            
            if (this.shouldSpace(unary, operand)) {
                join.setDelimiter(new CodePointBlock(' '));
            }
            
            if (unary.isPrefix()) {
                join.addBlock(op);
            }
            
            Block operandBk = operand.toBlock(temp, context);
            
            switch (operand.getNodeKind()) {
                case BINARY_OP:
                    join.addBlock(this.parenthesize(context, operandBk).takeTokenFrom(operandBk));
                    break;
                default:
                    join.addBlock(operandBk);
                    break;
            }
            
            if (unary.isSuffix()) {
                join.addBlock(op);
            }
            
            return join;
        }
    }
    
    public static final EchoFormatter OF_UNARY_OP = new EchoFormatter.OfUnaryOp();
    
    @JsonSerializable
    public static class OfFunctionLike extends EchoFormatter {
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
        
        protected String getName(UnaryOp unary) {
            return unary.getName();
        }
        
        @Override
        @SuppressWarnings("fallthrough")
        public StringBuilder format(StringBuilder b, Context context, Node node0) {
            Node.OfUnaryOp node  = (Node.OfUnaryOp) node0;
            UnaryOp        unary = node.getOperator();
            
            if (unary.isPrefix()) {
                b.append(this.getName(unary));
            }
            
            Node op = node.getOperand();
            switch (op.getNodeKind()) {
                case SPAN_OP:
                    SpanOp span = ((Node.OfSpanOp) op).getOperator();
                    
                    if (unary.isPrefix() && span.isLiteral())
                        b.append(' ');
                    this.toEchoString(op, b, context);
                    if (unary.isSuffix() && span.isLiteral())
                        b.append(' ');
                    
                    break;
                    
                case UNARY_OP:
                case BINARY_OP:
                    CodePoint lp = getLeftParenthesis(context);
                    CodePoint rp = getRightParenthesis(context);
                    
                    if (lp != null && rp != null) {
                        b.appendCodePoint(lp.intValue());
                        this.toEchoString(op, b, context);
                        b.appendCodePoint(rp.intValue());
                        break;
                    } // else fall through
                    
                default:
                    if (unary.isPrefix())
                        b.append(' ');
                    this.toEchoString(op, b, context);
                    if (unary.isSuffix())
                        b.append(' ');
                    break;
            }
            
            if (unary.isSuffix()) {
                b.append(this.getName(unary));
            }
            
            return b;
        }
        
        protected Setting<Style> getStyle(Context context, UnaryOp unary) {
            return Setting.FUNCTION_STYLE;
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfUnaryOp node    = (Node.OfUnaryOp) node0;
            UnaryOp        unary   = node.getOperator();
            Node           operand = node.getOperand();
            JoinBlock      join    = new JoinBlock(2);
            Setting<Style> style   = this.getStyle(context, unary);
            Block          unaryBk = new StyleBlock(new TextBlock(this.getName(unary))).set(style).setToken(node);
            
            if (unary.isPrefix()) {
                join.addBlock(unaryBk);
            }
            
            boolean shouldSpace = true;
            
            Block operandBk = operand.toBlock(temp, context);
            
            switch (operand.getNodeKind()) {
                case SPAN_OP:
                    SpanOp span = ((Node.OfSpanOp) operand).getOperator();
                    
                    if (!span.isLiteral()) {
                        shouldSpace = false;
                    }
                    
                    join.addBlock(operandBk);
                    break;
                    
                case UNARY_OP:
                case BINARY_OP:
                    Block block = parenthesize(context, operandBk).takeTokenFrom(operandBk);
                    shouldSpace = (block == operandBk);
                    join.addBlock(block);
                    break;
                    
                default:
                    join.addBlock(operandBk);
                    break;
            }
            
            if (shouldSpace) {
                join.setDelimiter(new CodePointBlock(' '));
            }
            
            if (unary.isSuffix()) {
                join.addBlock(unaryBk);
            }
            
            return join;
        }
    }
    
    public static final EchoFormatter OF_FUNCTION_LIKE = new EchoFormatter.OfFunctionLike();
    
    @JsonSerializable
    public static class OfFunctionLikeLiteral extends EchoFormatter.OfFunctionLike {
        @JsonProperty
        private final String literal;
        
        @JsonConstructor
        public OfFunctionLikeLiteral(String literal) {
            this.literal = Objects.requireNonNull(literal, "literal");
        }
        
        @Override
        protected Map<String, Object> getProperties() {
            return LinkedHashMapOf("literal", literal);
        }
        
        @Override
        public int hashCode() {
            return 31 * super.hashCode() + Objects.hashCode(literal);
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj) && Objects.equals(this.literal, ((OfFunctionLikeLiteral) obj).literal);
        }
        
        @Override
        protected String getName(UnaryOp unary) {
            return this.literal;
        }
    }
    
    public static EchoFormatter ofFunction(String literal) {
        return new EchoFormatter.OfFunctionLikeLiteral(literal);
    }
    
    @JsonSerializable
    public static class OfUnaryLiteral extends EchoFormatter.OfUnaryOp {
        @JsonProperty
        private final String literal;
        
        @JsonConstructor
        public OfUnaryLiteral(String literal) {
            this.literal = Objects.requireNonNull(literal, "literal");
        }
        
        @Override
        public int hashCode() {
            return 31 * super.hashCode() + Objects.hashCode(literal);
        }
        
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj) && Objects.equals(this.literal, ((OfUnaryLiteral) obj).literal);
        }
        
        @Override
        protected String getName(UnaryOp unary) {
            return this.literal;
        }
    }
    
    public static EchoFormatter ofUnary(String literal) {
        return new EchoFormatter.OfUnaryLiteral(literal);
    }
    
    @JsonSerializable
    public static class OfUnarySpan extends EchoFormatter {
        @JsonProperty
        private final String left;
        @JsonProperty
        private final String right;
        
        @JsonConstructor
        public OfUnarySpan(String left, String right) {
            this.left  = Objects.requireNonNull(left,  "left");
            this.right = Objects.requireNonNull(right, "right");
        }
        
        @Override
        public int hashCode() {
            int hash = super.hashCode();
            hash = 31 * hash + Objects.hashCode(left);
            hash = 31 * hash + Objects.hashCode(right);
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (super.equals(obj)) {
                OfUnarySpan that = (OfUnarySpan) obj;
                return Objects.equals(this.left,  that.left)
                    && Objects.equals(this.right, that.right);
            }
            return false;
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node) {
            b.append(left);
            
            this.toEchoString(((Node.OfUnaryOp) node).getOperand().getExpressionNode(), b, context);
            
            b.append(right);
            return b;
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node) {
            return new JoinBlock(3).addBlock(new TextBlock(left).setToken(node))
                                   .addBlock(((Node.OfUnaryOp) node).getOperand().getExpressionNode().toBlock(temp, context))
                                   .addBlock(new TextBlock(right));
        }
    }
    
    public static EchoFormatter ofUnarySpan(String left, String right) {
        return new EchoFormatter.OfUnarySpan(left, right);
    }
    
    @JsonSerializable
    public static class OfNaryOp extends EchoFormatter {
        @JsonProperty
        private final boolean spaceBefore;
        @JsonProperty
        private final boolean spaceAfter;
        
        @JsonConstructor
        public OfNaryOp(boolean spaceBefore, boolean spaceAfter) {
            this.spaceBefore = spaceBefore;
            this.spaceAfter  = spaceAfter;
        }
        
        @Override
        protected Map<String, Object> getProperties() {
            return LinkedHashMapOf("spaceBefore", spaceBefore, "spaceAfter", spaceAfter);
        }
        
        @Override
        public int hashCode() {
            int hash = super.hashCode();
            hash = 31 * hash + Boolean.hashCode(spaceBefore);
            hash = 31 * hash + Boolean.hashCode(spaceAfter);
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (super.equals(obj)) {
                OfNaryOp that = (OfNaryOp) obj;
                return this.spaceBefore == that.spaceBefore
                    && this.spaceAfter  == that.spaceAfter;
            }
            return false;
        }
        
        protected String getName(NaryOp nary) {
            return nary.getName();
        }
        
        @Override
        public StringBuilder format(StringBuilder b, Context context, Node node0) {
            Node.OfNaryOp node  = (Node.OfNaryOp) node0;
            NaryOp        nary  = node.getOperator();
            String        name  = this.getName(nary);
            List<Node>    ops   = node.getOperands();
            int           count = ops.size();
            
            boolean spaceBefore = this.spaceBefore;
            boolean spaceAfter  = this.spaceAfter;
            
            for (int i = 0; i < count; ++i) {
                if (i != 0) {
                    if (spaceBefore)
                        b.append(' ');
                    b.append(name);
                    if (spaceAfter)
                        b.append(' ');
                }
                this.toEchoString(ops.get(i), b, context);
            }
            
            return b;
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context, Node node0) {
            Node.OfNaryOp node  = (Node.OfNaryOp) node0;
            NaryOp        nary  = node.getOperator();
            String        name  = this.getName(nary);
            List<Node>    ops   = node.getOperands();
            int           count = ops.size();
            
            boolean spaceBefore = this.spaceBefore;
            boolean spaceAfter  = this.spaceAfter;
            
            JoinBlock join  = new JoinBlock(count);
            Block     delim = new TextBlock(name);
            
            if (spaceBefore || spaceAfter) {
                CodePointBlock space = new CodePointBlock(' ');
                if (spaceBefore && spaceAfter) {
                    delim = Block.join(space, delim, space);
                } else if (spaceBefore) {
                    delim = Block.join(space, delim);
                } else if (spaceAfter) {
                    delim = Block.join(delim, space);
                }
            }
            
            Iterator<Token> tokens = node.getTokens().iterator();
            
            for (int i = 0; i < count; ++i) {
                if (i != 0) {
                    join.addBlock(delim.copy().setToken(tokens.next()));
                }
                tokens.next();
                join.addBlock(ops.get(i).toBlock(temp, context));
            }
            
            return join;
        }
    }
    
    public static final EchoFormatter OF_NARY_OP = new EchoFormatter.OfNaryOp(false, false);
    
    public static final EchoFormatter OF_NARY_OP_SPACED = new EchoFormatter.OfNaryOp(true, true);
    
    public static final EchoFormatter OF_NARY_OP_SPACE_BEFORE = new EchoFormatter.OfNaryOp(true, false);
    
    public static final EchoFormatter OF_NARY_OP_SPACE_AFTER = new EchoFormatter.OfNaryOp(false, true);
}