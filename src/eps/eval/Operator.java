/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.json.*;
import eps.util.*;
import eps.app.*;

import java.util.*;
import java.lang.reflect.*;

@JsonSerializable
public abstract class Operator extends AbstractSymbol implements Comparable<Operator> {
    @JsonProperty
    private final int precedence;
    @JsonProperty
    private final Associativity associativity;
    
    @JsonDefault("precedence")
    private static final int DEFAULT_PRECEDENCE = 0;
    @JsonDefault("associativity")
    private static final Associativity DEFAULT_ASSOCIATIVITY = Associativity.LEFT_TO_RIGHT;
    
    public Operator(Builder<?, ?> builder) {
        super(builder);
        this.precedence    = builder.getPrecedence();
        this.associativity = Objects.requireNonNull(builder.getAssociativity(), "associativity");
    }
    
    @JsonConstructor
    protected Operator(String        name,
                       EchoFormatter echoFormatter,
                       Set<String>   preferredAliases,
                       int           precedence,
                       Associativity associativity) {
        super(name, echoFormatter, preferredAliases);
        this.precedence    = precedence;
        this.associativity = Objects.requireNonNull(associativity, "associativity");
    }
    
    protected Operator(String name, Operator delegate) {
        this(name, null, delegate);
    }
    
    protected Operator(String name, EchoFormatter echoFormatter, Operator delegate) {
        super(name, echoFormatter, delegate);
        this.precedence    = delegate.getPrecedence();
        this.associativity = delegate.getAssociativity();
    }
    
    @Override
    public boolean isEqualTo(Symbol sym) {
        if (super.isEqualTo(sym)) {
            Operator that = (Operator) sym;
            if (this.precedence != that.precedence)
                return false;
            if (this.associativity != that.associativity)
                return false;
            return true;
        }
        return false;
    }
    
    @Override
    public int compareTo(Operator that) {
        return java.lang.Integer.compare(this.getPrecedence(), that.getPrecedence());
    }
    
    @Override
    public boolean isOperator() {
        return true;
    }
    
    public boolean isUnary() {
        return getSymbolKind().isUnaryOp();
    }
    
    public boolean isBinary() {
        return getSymbolKind().isBinaryOp();
    }
    
    public boolean isNary() {
        return getSymbolKind().isNaryOp();
    }
    
    public boolean isSpan() {
        return getSymbolKind().isSpanOp();
    }
    
    @Override
    public Operator createAlias(Symbol sym) {
        return (Operator) super.createAlias(sym);
    }
    
    @Override
    public Operator createAlias(Symbol sym, EchoFormatter fmt) {
        return (Operator) super.createAlias(sym, fmt);
    }
    
    @Override
    public Operator createAlias(String name) {
        return (Operator) super.createAlias(name);
    }
    
    @Override
    public abstract Operator createAlias(String name, EchoFormatter fmt);
    
    public static final int HIGHEST_PRECEDENCE          = Integer.MAX_VALUE;
    public static final int VERY_HIGH_PRECEDENCE        = 256;
//    public static final int FACTORIAL_PRECEDENCE        = 192;
    public static final int EXPONENT_PRECEDENCE         = 128;
    public static final int FUNCTION_PRECEDENCE         = 112;
    public static final int FACTORIAL_PRECEDENCE        = 96;
    public static final int UNARY_NEGATION_PRECEDENCE   = 80;
    public static final int MULTIPLY_DIVIDE_PRECEDENCE  = 64;
    public static final int ADD_SUBTRACT_PRECEDENCE     = 56;
    public static final int RELATIONAL_PRECEDENCE       = 48;
    public static final int BOOLEAN_OP_PRECEDENCE       = 32;
//    public static final int ASSIGN_PRECEDENCE           = 12;
//    public static final int TERMINATOR_PRECEDENCE       = 9;
//    public static final int COMMA_PRECEDENCE            = 8;
    public static final int NARY_DEFAULT_PRECEDENCE     = 20;
    public static final int ASSIGN_PRECEDENCE           = 16;
    public static final int TERMINATOR_PRECEDENCE       = 12;
    public static final int COMMA_PRECEDENCE            = 8;
    public static final int VERY_LOW_PRECEDENCE         = 1;
    public static final int LOWEST_PRECEDENCE           = Integer.MIN_VALUE;
    
    public static final Map<String, Integer> PRECEDENCE_BY_NAME;
    public static final Map<Integer, String> PRECEDENCE_BY_VALUE;
    static {
        List<Pair<String, Integer>> entries = new ArrayList<>();
        
        for (Field field : Operator.class.getFields()) {
            if (field.getType() != int.class)
                continue;
            if (field.getModifiers() != Misc.PUBLIC_STATIC_FINAL)
                continue;
            String name = field.getName();
            if (!name.endsWith("_PRECEDENCE"))
                continue;
            name = name.substring(0, name.length() - "_PRECEDENCE".length());
            name = name.toLowerCase(Locale.ROOT).replace("_", " ");
            try {
                entries.add(Pair.of(name, field.getInt(null)));
            } catch (ReflectiveOperationException x) {
                Log.caught(Operator.class, "<clinit>", x, false);
            }
        }
        
        Map<String, Integer> byName  = new LinkedHashMap<>(entries.size());
        Map<Integer, String> byValue = new LinkedHashMap<>(entries.size());
        
        entries.sort(Comparator.comparing(Pair.right(), Comparator.reverseOrder()));
        
        for (Pair<String, Integer> e : entries) {
            byName.put(e.left, e.right);
            byValue.put(e.right, e.left);
        }
        
        PRECEDENCE_BY_NAME  = Collections.unmodifiableMap(byName);
        PRECEDENCE_BY_VALUE = Collections.unmodifiableMap(byValue);
    }
    
    public int getPrecedence() {
        return precedence;
    }
    
    public static String precedenceToString(int p) {
        String name = PRECEDENCE_BY_VALUE.get(p);
        if (name == null) {
            return Integer.toString(p);
        }
        return name;
    }
    
    public enum Associativity {
        LEFT_TO_RIGHT,
        RIGHT_TO_LEFT;
        
        public boolean isLeftToRight() {
            return this == LEFT_TO_RIGHT;
        }
        public boolean isRightToLeft() {
            return this == RIGHT_TO_LEFT;
        }
    }
    
    public Associativity getAssociativity() {
        return associativity;
    }
    
    public static abstract class Builder<O extends Operator, B extends Builder<O, B>> extends Symbol.Builder<O, B> {
        private Associativity associativity = Associativity.LEFT_TO_RIGHT;
        private int           precedence    = VERY_LOW_PRECEDENCE;
        
        @Override
        public B setAll(O operator) {
            this.setAssociativity(operator.getAssociativity())
                .setPrecedence(operator.getPrecedence());
            
            return super.setAll(operator);
        }
        
        public B setAssociativity(Associativity associativity) {
            this.associativity = Objects.requireNonNull(associativity, "associativity");
            return self();
        }
        
        public Associativity getAssociativity() {
            return associativity;
        }
        
        public B setPrecedence(int precedence) {
            this.precedence = precedence;
            return self();
        }
        
        public int getPrecedence() {
            return precedence;
        }
        
        @SuppressWarnings("unchecked")
        protected static final Class<Builder<?, ?>> OPERATOR_BUILDER_TYPE =
            (Class<Builder<?, ?>>) (Class<? super Builder<?, ?>>) Builder.class;
        
        protected static final ToString<Builder<?, ?>> OPERATOR_BUILDER_TO_STRING =
            Symbol.Builder.SYMBOL_BUILDER_TO_STRING.derive(Builder.OPERATOR_BUILDER_TYPE)
                  .add("associativity", Builder::getAssociativity)
                  .add("precedence",    b -> precedenceToString(b.precedence))
                  .build();
        
        @Override
        public String toString(boolean multiline) {
            return OPERATOR_BUILDER_TO_STRING.apply(this, multiline);
        }
    }
}