/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.math.*;
import eps.util.*;
import eps.json.*;

import java.util.*;
import java.io.Serializable;

@JsonSerializable
public abstract class NaryOp extends Operator {
    // Quick note: n-ary operators have no particular associativity.
    // They're only ever grouped by span operators (parentheses) or
    // interactions with other n-ary operators.
    
    public NaryOp(Builder builder) {
        super(builder);
    }
    
    private NaryOp(String name, EchoFormatter fmt, NaryOp delegate) {
        super(name, fmt, delegate);
    }
    
    @JsonConstructor
    protected NaryOp(String        name,
                     EchoFormatter echoFormatter,
                     Set<String>   preferredAliases,
                     int           precedence,
                     Associativity associativity) {
        super(name, echoFormatter, preferredAliases, precedence, associativity);
    }
    
    @Override
    protected EchoFormatter getDefaultEchoFormatter() {
        return EchoFormatter.OF_NARY_OP;
    }
    
    @Override
    public Kind getSymbolKind() {
        return Kind.NARY_OP;
    }
    
    @Override
    public NaryOp createAlias(String aliasName, EchoFormatter fmt) {
        return new Alias(aliasName, fmt, (NaryOp) this.getDelegate());
    }
    
    public Operand apply(Context context, Node.OfNaryOp node) {
        List<Node> operands = node.getOperands();
        int        count    = operands.size();
        
        if (count == 2) {
            if (SpecialSyms.isEllipse(context, operands.get(1))) {
                // invocation with the ellipse form x, ...
                Operand lhs = operands.get(0).evaluate(context);
                // TODO:
                //  maybe there should just be an overload
                //  apply(Context, Operand) and the function
                //  should not always receive a tuple. This
                //  would be consistent with the behavior of
                //  TreeFunction which merely assigns the
                //  parameter variable to the argument directly
                //  rather than wrapping it in a tuple.
                switch (lhs.getOperandKind()) {
                    case TUPLE:
                        break;
                    case EMPTY:
                        lhs = Tuple.empty();
                        break;
                    default:
                        lhs = Tuple.of(lhs);
                        break;
                }
                return apply(context, (Tuple) lhs);
            }
        }
        
        Tuple.Builder builder = Tuple.builder(count);
        
        for (int i = 0; i < count; ++i) {
            builder.append(operands.get(i).evaluate(context));
        }
        
        return apply(context, builder.build());
    }
    
    protected abstract Operand apply(Context context, Tuple operands);
    
    public static class Alias extends NaryOp {
        private final NaryOp delegate;
        
        public Alias(String name, NaryOp delegate) {
            this(name, null, delegate);
        }
        
        public Alias(String name, EchoFormatter fmt, NaryOp delegate) {
            super(name, fmt, Objects.requireNonNull(delegate, "delegate"));
            this.delegate = delegate;
        }
        
        static {
            JsonObjectAdapter.put(NaryOp.Alias.class, Symbol::aliasToMap, map -> (NaryOp.Alias) Symbol.mapToAlias(map));
        }
        
        @Override
        public Alias configure(Symbol.Builder<?, ?> b) {
            return new Alias(b.getName(), b.getEchoFormatter(), delegate);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                return Symbol.isEqual(this.delegate, ((Alias) sym).delegate);
            }
            return false;
        }
        
        @Override
        public Symbol getDelegate() {
            return delegate.getDelegate();
        }
        
        @Override
        public Operand apply(Context context, Node.OfNaryOp node) {
            return delegate.apply(context, node);
        }
        
        @Override
        protected Operand apply(Context context, Tuple operands) {
            return delegate.apply(context, operands);
        }
    }
    
    @FunctionalInterface
    public interface TupleFunction {
        Operand apply(NaryOp self, Context context, Tuple operands);
    }
    @FunctionalInterface
    public interface TupleFunctionMath extends TupleFunction {
        @Override
        default Operand apply(NaryOp self, Context context, Tuple operands) {
            return apply(context.getMathEnvironment(), operands);
        }
        Operand apply(MathEnv env, Tuple operands);
    }
    
    @JsonSerializable
    public static class FromFunction extends NaryOp {
        @JsonProperty
        private final TupleFunction fn;
        
        public FromFunction(Builder builder) {
            super(builder);
            this.fn = Objects.requireNonNull(builder.fn, "fn");
        }
        
        private FromFunction(Builder b, TupleFunction fn) {
            super(b);
            this.fn = fn;
        }
        
        @JsonConstructor
        protected FromFunction(String        name,
                               EchoFormatter echoFormatter,
                               Set<String>   preferredAliases,
                               int           precedence,
                               Associativity associativity,
                               TupleFunction fn) {
            super(name, echoFormatter, preferredAliases, precedence, associativity);
            this.fn = Objects.requireNonNull(fn, "fn");
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                FromFunction that = (FromFunction) sym;
                return Objects.equals(this.fn, that.fn);
            }
            return false;
        }
        
        @Override
        public FromFunction configure(Symbol.Builder<?, ?> b) {
            return new FromFunction((Builder) b, fn);
        }
        
        @Override
        public Operand apply(Context context, Tuple operands) {
            return fn.apply(this, context, operands);
        }
    }
    
    @JsonSerializable
    public static class NaryError extends NaryOp {
        public NaryError(Builder builder) {
            super(builder);
        }
        
        @JsonConstructor
        protected NaryError(String        name,
                            EchoFormatter echoFormatter,
                            Set<String>   preferredAliases,
                            int           precedence,
                            Associativity associativity) {
            super(name, echoFormatter, preferredAliases, precedence, associativity);
        }
        
        @Override
        public NaryError configure(Symbol.Builder<?, ?> b) {
            return new NaryError((Builder) b);
        }
        
        @Override
        public boolean isError() {
            return true;
        }
        
        @Override
        public Operand apply(Context c, Node.OfNaryOp n) {
            throw EvaluationException.unresolved(this);
        }
        
        @Override
        protected Operand apply(Context c, Tuple o) {
            throw EvaluationException.unresolved(this);
        }
    }
    
    public static NaryError createNaryError(String name) {
        return new Builder().setName(name)
                            .setPrecedence(HIGHEST_PRECEDENCE) // TODO?
                            .buildError();
    }
    
    @JsonSerializable
    public static class FromTree extends NaryOp {
        @JsonProperty(interimConverter = TreeFunction.INTERIM_CONVERTER,
                      deferInterimToConstructor = true)
        private final TreeFunction treeFn;
        
        private static Builder createBuilderForSuperConstructor(Node.OfOperator decl) {
            return createBuilderForSuperConstructor(decl.getOperator().getName());
        }
        
        private static Builder createBuilderForSuperConstructor(String name) {
            return new Builder().setName(name)
                                .setEchoFormatter(EchoFormatter.OF_NARY_OP_SPACED)
                                .setPrecedence(NARY_DEFAULT_PRECEDENCE)
                                ;
        }
        
        public FromTree(Context context, Node.OfBinaryOp decl, Node body) {
            super(createBuilderForSuperConstructor(decl));
            this.treeFn = new TreeFunction(context, this, decl, body);
        }
        
        public FromTree(Context context, Node.OfNaryOp decl, Node body) {
            super(createBuilderForSuperConstructor(decl));
            this.treeFn = new TreeFunction(context, this, decl, body);
        }
        
        public FromTree(Builder builder, TreeAdapter<?, String> params, String body) {
            super(builder);
            this.treeFn = new TreeFunction(this, params, body);
        }
        
        @JsonConstructor
        protected FromTree(String        name,
                           EchoFormatter echoFormatter,
                           Set<String>   preferredAliases,
                           int           precedence,
                           Associativity associativity,
                           Object        treeFn) {
            super(name, echoFormatter, preferredAliases, precedence, associativity);
            this.treeFn = TreeFunction.fromInterimObject(this, treeFn);
        }
        
        private FromTree(Builder builder, TreeFunction treeFn) {
            super(builder);
            this.treeFn = treeFn.copy(this);
        }
        
        @Override
        public FromTree configure(Symbol.Builder<?, ?> b) {
            return new FromTree((Builder) b, treeFn);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                return TreeFunction.isEqual(this.treeFn, ((FromTree) sym).treeFn);
            }
            return false;
        }
        
        TreeFunction getTreeFunction() {
            return treeFn;
        }
        
        @Override
        public Operand apply(Context context, Node.OfNaryOp node) {
            return treeFn.evaluate(context, node);
        }
        
        @Override
        protected Operand apply(Context context, Tuple tuple) {
            throw new AssertionError(this);
        }
    }
    
    public static class Builder extends Operator.Builder<NaryOp, Builder> {
        private static final TupleFunction NO_FUNCTION =
            (TupleFunction & Serializable) (a, b, c) -> { throw new IllegalStateException(); };
        
        private TupleFunction fn = NO_FUNCTION;
        
        public Builder() {
            super.setAssociativity(Associativity.LEFT_TO_RIGHT);
            super.setPrecedence(Operator.NARY_DEFAULT_PRECEDENCE);
            super.setEchoFormatter(EchoFormatter.OF_NARY_OP);
        }
        
        @Override
        public Builder setAll(NaryOp nary) {
            if (nary instanceof NaryOp.FromFunction) {
                this.setFunction(((NaryOp.FromFunction) nary).fn);
            }
            return super.setAll(nary);
        }
        
        @Override
        public Builder setAssociativity(Associativity a) {
            // NaryOps don't have associativity
            return this;
        }
        
        public Builder setFunction(TupleFunction fn) {
            this.fn = Objects.requireNonNull(fn, "fn");
            return self();
        }
        
        public Builder setFunction(TupleFunctionMath fn) {
            return setFunction((TupleFunction) fn);
        }
        
        @Override
        public NaryOp build() {
            return new FromFunction(this);
        }
        
        @Override
        public NaryError buildError() {
            return new NaryError(this);
        }
        
        protected static final ToString<Builder> NARY_OP_BUILDER_TO_STRING =
            OPERATOR_BUILDER_TO_STRING.derive(Builder.class)
                                      .add("fn", b -> b.fn)
                                      .build();
        @Override
        public String toString(boolean m) {
            return NARY_OP_BUILDER_TO_STRING.apply(this, m);
        }
    }
    
    public static final NaryOp TUPLE =
        new Builder().setName("tuple")
                     .makeIntrinsic()
                     .setEchoFormatter(EchoFormatter.OF_NARY_OP_SPACE_AFTER)
                     .addPreferredAlias(",")
                     .setPrecedence(COMMA_PRECEDENCE)
                     .setFunction((env, operands) -> operands)
                     .build();
    
    public static final NaryOp TERMINATOR =
        new Builder().setName("terminator")
                     .makeIntrinsic()
                     .setEchoFormatter(EchoFormatter.OF_NARY_OP_SPACE_AFTER)
                     .addPreferredAlias(";")
                     .setPrecedence(TERMINATOR_PRECEDENCE)
                     .setFunction((self, context, operands) -> {
                         int count = operands.count();
                         if (count == 0) {
                             throw UnsupportedException.operandType(operands, self.getName());
                         }
                         int last = (count - 1);
                         for (int i = 0; i < last; ++i) {
                             operands.get(i).evaluate(context);
                         }
                         return operands.get(last).evaluate(context);
                     })
                     .build();
    
    public static final List<NaryOp> DEFAULTS =
        Collections.unmodifiableList(Misc.collectConstants(NaryOp.class));
}