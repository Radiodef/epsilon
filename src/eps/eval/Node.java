/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.block.*;
import static eps.util.Misc.*;
import static eps.util.Literals.*;

import java.util.*;
import java.util.function.*;
import java.io.Serializable;

public abstract class Node implements Copyable<Node> {
    private volatile Object parent;
    private final    Token  token;
    
    private transient volatile List<Token> tokens;
    private transient volatile List<Node>  nodes;
    
    private Node(Token token) {
        this.token = Objects.requireNonNull(token, "token");
    }
    
    public abstract Node replaceAll(UnaryOperator<Node> op);
    public abstract void collectSymbols(Consumer<? super Symbol> action);
    
    public void forEach(Consumer<? super Node> action) {
        action.accept(this);
    }
    
    public boolean find(Predicate<? super Node> p) {
        return p.test(this);
    }
    
    public static final BiFunction<Node, Context, Operand> EVALUATE =
        (BiFunction<Node, Context, Operand> & Serializable) Node::evaluate;
    
    public enum Kind {
        @Deprecated
        OTHER,
        OPERAND,
        UNARY_OP,
        BINARY_OP,
        NARY_OP,
        SPAN_OP,
        @Deprecated
        PARENTHESIZED_EXPRESSION;
        
        private final String lower;
        
        private Kind() {
            lower = name().toLowerCase().replace('_', ' ');
        }
        
        public String toLowerCase() {
            return lower;
        }
        
        @Override
        public String toString() {
            return this.toLowerCase();
        }
        
        @Deprecated
        public boolean isOther()    { return this == OTHER;     }
        public boolean isOperand()  { return this == OPERAND;   }
        public boolean isUnaryOp()  { return this == UNARY_OP;  }
        public boolean isBinaryOp() { return this == BINARY_OP; }
        public boolean isNaryOp()   { return this == NARY_OP;   }
        public boolean isSpanOp()   { return this == SPAN_OP;   }
        @Deprecated
        public boolean isParanthesizedExpression() { return this == PARENTHESIZED_EXPRESSION; }
        
        public boolean isOperator() {
            switch (this) {
                case UNARY_OP:
                case BINARY_OP:
                case NARY_OP:
                case SPAN_OP:
                    return true;
                default:
                    return false;
            }
        }
    }
    
    public abstract Kind getNodeKind();
    
    public boolean isOperator() { return getNodeKind().isOperator(); }
    public boolean isOther()    { return getNodeKind().isOther();    }
    public boolean isOperand()  { return getNodeKind().isOperand();  }
    public boolean isUnaryOp()  { return getNodeKind().isUnaryOp();  }
    public boolean isBinaryOp() { return getNodeKind().isBinaryOp(); }
    public boolean isNaryOp()   { return getNodeKind().isNaryOp();   }
    public boolean isSpanOp()   { return getNodeKind().isSpanOp();   }
    
    Node setTree(Tree tree) {
        if (parent != null) {
            throw Errors.newIllegalState(parent);
        }
        parent = tree;
        return this;
    }
    
    public boolean isIndependant() {
        return parent == null;
    }
    
    public boolean isRoot() {
        Object parent = this.parent;
        return (parent == null) || (parent instanceof Tree);
    }
    
    public Node getParent() {
        Object parent = this.parent;
        return (parent instanceof Node) ? (Node) parent : null;
    }
    
    public Tree getTree() {
        Node node = this;
        for (;;) {
            Object parent = node.parent;
            if (parent instanceof Node) {
                node = (Node) parent;
            } else if (parent instanceof Tree) {
                return (Tree) parent;
            } else {
                throw Errors.newIllegalState(f("%s ... parent = %s (%s)", this, parent, coals(parent, Object::getClass)));
            }
        }
    }
    
    public boolean isAncestorOf(Node node) {
        for (;;) {
            Node parent = node.getParent();
            if (parent == this)
                return true;
            if (parent == null)
                return false;
            node = parent;
        }
    }
    
    // turned out to not use these for their intented purpose
    /*
    public boolean anyParentMatches(Predicate<? super Node> p) {
        return findParent(p) != null;
    }
    
    public Node findParent(Predicate<? super Node> p) {
        Node node = this;
        for (;;) {
            Node parent = node.getParent();
            if (parent == null)
                return null;
            if (p.test(parent))
                return parent;
            node = parent;
        }
    }
    */
    
    public Node getExpressionNode() {
        return this;
    }
    
    public Token getToken() {
        return token;
    }
    
    public List<Token> getTokens() {
        List<Token> tokens = this.tokens;
        if (tokens == null) {
            this.tokens = tokens = this.getTokensImpl();
        }
        return tokens;
    }
    
    public abstract void collectTokens(Consumer<? super Token> action);
    
    protected List<Token> getTokensImpl() {
        List<Token> tokens = new ArrayList<>();
        this.collectTokens(tokens::add);
        return Collections.unmodifiableList(tokens);
    }
    
    public Substring getSource() {
        return getToken().getSource();
    }
    
    public Substring getExpressionSource() {
        return getSource();
    }
    
    public abstract Token getLeftMostToken();
    public abstract Token getRightMostToken();
    
    protected abstract List<Node> getNodesImpl();
    
    public List<Node> getNodes() {
        List<Node> nodes = this.nodes;
        if (nodes == null) {
            this.nodes = nodes = this.getNodesImpl();
        }
        return nodes;
    }
    
    public Node getLeftMostNode() {
        return this.getNodes().get(0);
    }
    
    public Node getRightMostNode() {
        List<Node> nodes = this.getNodes();
        return nodes.get(nodes.size() - 1);
    }
    
    public Node getNodeBefore(Node child) {
        List<Node> nodes = this.getNodes();
        
        int index = nodes.indexOf(child);
        if (index >= 0) {
            if (index == 0) {
                if (!this.isRoot()) {
                    return this.getParent().getNodeBefore(this);
                }
            } else {
                return nodes.get(index - 1);
            }
        }
        
        throw Errors.newIllegalArgument(child);
    }
    
    public Node getNodeAfter(Node child) {
        List<Node> nodes = this.getNodes();
        
        int index = nodes.lastIndexOf(child);
        if (index >= 0) {
            if (index == (nodes.size() - 1)) {
                if (!this.isRoot()) {
                    return this.getParent().getNodeAfter(this);
                }
            } else {
                return nodes.get(index + 1);
            }
        }
        
        throw Errors.newIllegalArgument(child);
    }
    
    public abstract Operand evaluate(Context context);
    
    public abstract StringBuilder toEchoString(StringBuilder b, Context context);
    
    public Block toBlock(Context context) {
        return toBlock(new StringBuilder(), context);
    }
    
    public abstract Block toBlock(StringBuilder b, Context context);
    
    @Override
    public String toString() {
        return getExpressionSource().toString();
    }
    
    public static abstract class OfOperator extends Node {
        OfOperator(Token token) {
            super(token);
        }
        
        @Override
        public void forEach(Consumer<? super Node> action) {
            action.accept(this);
            for (Node op : getOperands()) {
                op.forEach(action);
            }
        }
        
        @Override
        public boolean find(Predicate<? super Node> p) {
            if (p.test(this)) {
                return true;
            }
            for (Node op : getOperands()) {
                if (op.find(p)) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public void collectSymbols(Consumer<? super Symbol> action) {
            action.accept(getOperator());
            for (Node op : getOperands()) {
                op.collectSymbols(action);
            }
        }
        
        public abstract Operator getOperator();
        
        protected Operator getOperator(Context context) {
            Operator      op    = getOperator();
            Symbols       scope = context.getScope();
            Symbols.Group group = scope.get(op.getName());
            if (group != null) {
                Symbol.Kind kind  = op.getSymbolKind();
                Operator    other = (Operator) group.get(kind);
                if (other != null) {
                    if (other != op) {
                        if (!kind.isUnaryOp() || (((UnaryOp) op).getAffix() == ((UnaryOp) other).getAffix())) {
                            return other;
                        }
                    }
                }
            }
            return op;
        }
        
        private transient volatile List<Node> operands;
        
        public List<Node> getOperands() {
            List<Node> operands = this.operands;
            if (operands == null) {
                this.operands = operands = this.getOperandsImpl();
            }
            return operands;
        }
        
        protected abstract List<Node> getOperandsImpl();
        
        @Override
        public Block toBlock(StringBuilder temp, Context context) {
            return getOperator().toBlock(temp, context, this);
        }
    }
    
    public static final class OfOperand extends Node implements Supplier<Operand> {
        private final Operand operand;
        
        OfOperand(Token token, Operand operand) {
            super(token);
            this.operand = Objects.requireNonNull(operand, "operand");
        }
        
        @Override
        public OfOperand copy() {
            return new OfOperand(getToken(), operand);
        }
        
        @Override
        public Node replaceAll(UnaryOperator<Node> op) {
            Node repl = op.apply(this);
            return (repl != null) ? repl : copy();
        }
        
        @Override
        public void collectSymbols(Consumer<? super Symbol> action) {
            if (operand instanceof Symbol) {
                action.accept((Symbol) operand);
            }
        }
        
        @Override
        public Kind getNodeKind() {
            return Kind.OPERAND;
        }
        
        @Override
        public Operand get() {
            return operand;
        }
        
        @Override
        public Operand evaluate(Context context) {
            return get().evaluate(context);
        }
        
        @Override
        public void collectTokens(Consumer<? super Token> action) {
            action.accept(getToken());
        }
        
        @Override
        protected List<Token> getTokensImpl() {
            return Collections.singletonList(getToken());
        }
        
        @Override
        protected List<Node> getNodesImpl() {
            return Collections.singletonList(this);
        }
        
        @Override
        public Token getLeftMostToken() {
            return getToken();
        }
        
        @Override
        public Token getRightMostToken() {
            return getToken();
        }
        
        @Override
        public StringBuilder toEchoString(StringBuilder b, Context context) {
            Operand op = get();
            switch (op.getOperandKind()) {
                case NUMBER:
                    return b.append(getExpressionSource());
                case VARIABLE:
                    return ((Variable) op).toEchoString(b, context, this);
                default:
                    break;
            }
            return op.toResultString(b, context, null, null);
        }
        
        @Override
        public Block toBlock(StringBuilder temp, Context context) {
            return get().toSourceBlock(temp, context, this);
        }
        
        @Override
        public String toString() {
            return getSource().toString();
        }
    }
    
    public static final class OfUnaryOp extends Node.OfOperator {
        private final UnaryOp operator;
        private final Node    operand;
        
        OfUnaryOp(Token token, UnaryOp operator, Node operand) {
            super(token);
            this.operator = Objects.requireNonNull(operator, "operator");
            this.operand  = Objects.requireNonNull(operand,  "operand");
            
            operand.parent = this;
        }
        
        @Override
        public OfUnaryOp copy() {
            return new OfUnaryOp(getToken(), operator, operand.copy());
        }
        
        @Override
        public Node replaceAll(UnaryOperator<Node> op) {
            Node repl = op.apply(this);
            if (repl != null)
                return repl;
            return new OfUnaryOp(getToken(), operator, operand.replaceAll(op));
        }
        
        @Override
        public Kind getNodeKind() {
            return Kind.UNARY_OP;
        }
        
        @Override
        public UnaryOp getOperator() {
            return operator;
        }
        
        public Node getOperand() {
            return operand;
        }
        
        @Override
        public Operand evaluate(Context context) {
            return ((UnaryOp) getOperator(context)).apply(context, operand);
        }
        
        @Override
        protected List<Node> getOperandsImpl() {
            return Collections.singletonList(operand);
        }
        
        @Override
        public void collectTokens(Consumer<? super Token> action) {
            if (operator.isPrefix()) {
                action.accept(getToken());
            }
            
            operand.collectTokens(action);
            
            if (operator.isSuffix()) {
                action.accept(getToken());
            }
        }
        
        @Override
        protected List<Node> getNodesImpl() {
            List<Node> list = new ArrayList<>(2);
            if (operator.isPrefix()) {
                list.add(this);
                list.add(operand);
            } else {
                list.add(operand);
                list.add(this);
            }
            return Collections.unmodifiableList(list);
        }
        
        @Override
        public Token getLeftMostToken() {
            if (operator.isPrefix()) {
                return getToken();
            } else {
                return operand.getLeftMostToken();
            }
        }
        
        @Override
        public Token getRightMostToken() {
            if (operator.isPrefix()) {
                return operand.getRightMostToken();
            } else {
                return getToken();
            }
        }
        
        @Override
        public StringBuilder toEchoString(StringBuilder b, Context context) {
            return operator.toEchoString(b, context, this);
        }
        
        @Override
        public Substring getExpressionSource() {
            return getSource().interpolate(operand.getExpressionSource());
        }
    }
    
    public static final class OfSpanOp extends Node.OfOperator {
        private final SpanOp operator;
        private final Node   operand;
        
        OfSpanOp(Token token, SpanOp operator, Node operand) {
            super(token);
            
            this.operator = Objects.requireNonNull(operator, "operator");
            this.operand  = Objects.requireNonNull(operand,  "operand"); // TODO?
            
            operand.parent = this;
        }
        
        @Override
        public OfSpanOp copy() {
            return new OfSpanOp(getToken(), operator, operand.copy());
        }
        
        @Override
        public Node replaceAll(UnaryOperator<Node> fn) {
            Node repl = fn.apply(this);
            if (repl != null)
                return repl;
            return new OfSpanOp(getToken(), operator, operand.replaceAll(fn));
        }
        
        @Override
        public Kind getNodeKind() {
            return Kind.SPAN_OP;
        }
        
        @Override
        public SpanOp getOperator() {
            return operator;
        }
        
        public Node getOperand() {
            return operand;
        }
        
        @Override
        public Token getLeftMostToken() {
            return operand.getLeftMostToken().getPrev();
//            return getToken();
        }
        
        @Override
        public Token getRightMostToken() {
            return getToken();
//            return operand.getRightMostToken().getNext();
//            return getToken().getNext();
        }
        
        @Override
        public Node getExpressionNode() {
            if (operator.isParenthesis()) {
                return operand.getExpressionNode();
            }
            return this;
        }
        
        @Override
        public void collectTokens(Consumer<? super Token> action) {
            action.accept(getLeftMostToken());
            operand.collectTokens(action);
            action.accept(getRightMostToken());
        }
        
        @Override
        protected List<Node> getOperandsImpl() {
            return Collections.singletonList(operand);
        }
        
        @Override
        public List<Node> getNodesImpl() {
            List<Node> nodes = new ArrayList<>(3);
            nodes.add(this);
            nodes.add(operand);
            nodes.add(this);
            return Collections.unmodifiableList(nodes);
        }
        
        @Override
        public Operand evaluate(Context context) {
            return ((SpanOp) getOperator(context)).apply(context, operand);
        }
        
        @Override
        public StringBuilder toEchoString(StringBuilder b, Context context) {
            return operator.toEchoString(b, context, this);
        }
        
        @Override
        public Substring getExpressionSource() {
            return getLeftMostToken().getSource().interpolate(getRightMostToken().getSource());
        }
    }
    
    public static final class OfBinaryOp extends Node.OfOperator {
        private final BinaryOp operator;
        private final Node     lhs;
        private final Node     rhs;
        
        OfBinaryOp(Token token, BinaryOp operator, Node lhs, Node rhs) {
            super(token);
            this.operator = Objects.requireNonNull(operator, "operator");
            this.lhs      = Objects.requireNonNull(lhs,      "lhs");
            this.rhs      = Objects.requireNonNull(rhs,      "rhs");
            
            lhs.parent = this;
            rhs.parent = this;
        }
        
        @Override
        public OfBinaryOp copy() {
            return new OfBinaryOp(getToken(), operator, lhs.copy(), rhs.copy());
        }
        
        @Override
        public Node replaceAll(UnaryOperator<Node> op) {
            Node repl = op.apply(this);
            if (repl != null)
                return repl;
            return new OfBinaryOp(getToken(), operator, lhs.replaceAll(op), rhs.replaceAll(op));
        }
        
        @Override
        public Kind getNodeKind() {
            return Kind.BINARY_OP;
        }
        
        @Override
        public BinaryOp getOperator() {
            return operator;
        }
        
        public Node getLeftHandSide() {
            return lhs;
        }
        
        public Node getRightHandSide() {
            return rhs;
        }
        
        @Override
        public Token getLeftMostToken() {
            return lhs.getLeftMostToken();
        }
        
        @Override
        public Token getRightMostToken() {
            return rhs.getRightMostToken();
        }
        
        @Override
        protected List<Node> getNodesImpl() {
            List<Node> nodes = new ArrayList<>(3);
            nodes.add(lhs);
            nodes.add(this);
            nodes.add(rhs);
            return Collections.unmodifiableList(nodes);
        }
        
        @Override
        protected List<Node> getOperandsImpl() {
            return Collections.unmodifiableList(ListOf(lhs, rhs));
        }
        
        @Override
        public void collectTokens(Consumer<? super Token> action) {
            lhs.collectTokens(action);
            action.accept(getToken());
            rhs.collectTokens(action);
        }
        
        @Override
        public Operand evaluate(Context context) {
            return ((BinaryOp) getOperator(context)).apply(context, lhs, rhs);
        }
        
        @Override
        public StringBuilder toEchoString(StringBuilder b, Context context) {
            return operator.toEchoString(b, context, this);
        }
        
        @Override
        public Substring getExpressionSource() {
            return getSource().interpolate(lhs.getExpressionSource()).interpolate(rhs.getExpressionSource());
        }
    }
    
    public static final class OfNaryOp extends Node.OfOperator {
        private final NaryOp     operator;
        private final List<Node> operands;
        
        OfNaryOp(Token token, NaryOp operator, List<Node> operands) {
            super(token);
            this.operator = Objects.requireNonNull(operator, "operator");
            
            Objects.requireNonNull(operands, "operands");
            List<Node> copy = new ArrayList<>(operands);
            
            int count = copy.size();
            for (int i = 0; i < count; ++i) {
                Node operand = Objects.requireNonNull(copy.get(i), "operand");
                operand.parent = this;
            }
            
            this.operands = Collections.unmodifiableList(copy);
        }
        
        @Override
        public OfNaryOp copy() {
            List<Node> operands = this.operands;
            int        count    = operands.size();
            List<Node> opsCopy  = new ArrayList<>(count);
            for (int i = 0; i < count; ++i) {
                opsCopy.add(operands.get(i).copy());
            }
            return new OfNaryOp(getToken(), operator, Collections.unmodifiableList(opsCopy));
        }
        
        @Override
        public Node replaceAll(UnaryOperator<Node> op) {
            Node repl = op.apply(this);
            if (repl != null)
                return repl;
            List<Node> operands = this.operands;
            int        count    = operands.size();
            List<Node> opsCopy  = new ArrayList<>(count);
            for (int i = 0; i < count; ++i) {
                opsCopy.add(operands.get(i).replaceAll(op));
            }
            return new OfNaryOp(getToken(), operator, Collections.unmodifiableList(opsCopy));
        }
        
        @Override
        public Kind getNodeKind() {
            return Kind.NARY_OP;
        }
        
        @Override
        public Token getLeftMostToken() {
            return getLeftMostNode().getLeftMostToken();
        }
        
        @Override
        public Token getRightMostToken() {
            return getRightMostNode().getRightMostToken();
        }
        
        @Override
        public NaryOp getOperator() {
            return operator;
        }
        
        @Override
        protected List<Node> getOperandsImpl() {
            return operands;
        }
        
        @Override
        protected List<Node> getNodesImpl() {
            List<Node> operands = this.operands;
            int        size     = operands.size();
            List<Node> nodes    = new ArrayList<>(size * 2 - 1);
            
            for (int i = 0; i < size; ++i) {
                if (i != 0) {
                    nodes.add(this);
                }
                nodes.add(operands.get(i));
            }
            
            return Collections.unmodifiableList(nodes);
        }
        
        @Override
        public void collectTokens(Consumer<? super Token> action) {
            Iterator<Node> operands = this.operands.iterator();
            
            if (operands.hasNext()) {
                Node last = operands.next();
                last.collectTokens(action);
                
                while (operands.hasNext()) {
                    action.accept(last.getRightMostToken().getNext());
                    
                    last = operands.next();
                    last.collectTokens(action);
                }
            }
            
//            if (operands.hasNext()) {
//                operands.next().collectTokens(action);
//                
//                List<Token> tokens = getToken().getList();
//                assert tokens.size() == (this.operands.size() - 1) : this + " ... " + tokens;
//                Iterator<Token> operators = tokens.iterator();
//                
//                while (operands.hasNext()) {
//                    action.accept(operators.next());
//                    operands.next().collectTokens(action);
//                }
//            }
        }
        
        @Override
        public Operand evaluate(Context context) {
            return ((NaryOp) getOperator(context)).apply(context, this);
        }
        
        @Override
        public StringBuilder toEchoString(StringBuilder b, Context context) {
            return operator.toEchoString(b, context, this);
        }
        
        @Override
        public Substring getExpressionSource() {
            Substring sub = getSource();
            for (Node op : operands) {
                sub = sub.interpolate(op.getExpressionSource());
            }
            return sub;
        }
    }
    
    static final class ListBuilder extends Node {
        private final List<Node> list = new ArrayList<>(0);
        
        ListBuilder(Token token) {
            super(token);
        }
        
        @Override
        public void forEach(Consumer<? super Node> action) {
            action.accept(this);
            for (Node node : list) {
                node.forEach(action);
            }
        }
        
        @Override
        public boolean find(Predicate<? super Node> p) {
            if (p.test(this)) {
                return true;
            }
            for (Node node : list) {
                if (node.find(p)) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public ListBuilder copy() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Node replaceAll(UnaryOperator<Node> op) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void collectSymbols(Consumer<? super Symbol> action) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Kind getNodeKind() {
            return Kind.OTHER;
        }
        
        ListBuilder add(Node node) {
            list.add(Objects.requireNonNull(node, "node"));
            return this;
        }
        
        List<Node> list() {
            return list;
        }
        
        @Override
        public void collectTokens(Consumer<? super Token> action) {
            throw Errors.newIllegalState(this);
        }
        
        @Override
        protected List<Node> getNodesImpl() {
            throw Errors.newIllegalState(this);
        }
        
        @Override
        public Token getLeftMostToken() {
            throw Errors.newIllegalState(this);
        }
        
        @Override
        public Token getRightMostToken() {
            throw Errors.newIllegalState(this);
        }
        
        @Override
        public Operand evaluate(Context context) {
            throw Errors.newIllegalState(this);
        }
        
        @Override
        public StringBuilder toEchoString(StringBuilder b, Context c) {
            throw Errors.newIllegalState(this);
        }
        
        @Override
        public Block toBlock(StringBuilder t, Context c) {
            throw Errors.newIllegalState(this);
        }
    }
}