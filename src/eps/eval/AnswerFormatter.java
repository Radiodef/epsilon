/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.app.*;
import eps.block.*;
import eps.job.*;
import eps.math.*;
import eps.math.Number;
import eps.math.Float;
import eps.math.Integer;

import java.math.*;
import java.util.*;

public enum AnswerFormatter {
    NONE("none") {
        @Override
        protected Block toResultBlock(Context context, Number num, MutableBoolean approx) {
            return new EmptyBlock();
        }
        @Override
        public StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx) {
            return b;
        }
    },
    DECIMAL("decimal") {
        @Override
        protected Block toResultBlock(Context context, Number num, MutableBoolean approx) {
            Block   resultBk = num.toResultBlock(context, this, approx);
            MathEnv env      = context.getMathEnvironment();
            
            switch (num.getNumberKind()) {
                case FRACTION:
                    Decimal    d = env.toDecimal((Fraction) num);
                    Block equals = Block.of(d.isApproximate() ? '≈' : '=');
                    Block dBk    = new StyleBlock(new TextBlock(d)).set(Setting.NUMBER_STYLE);
                    resultBk     = Block.join(resultBk, equals, dBk).setDelimiter(' ');
                    break;
                case FLOAT:
                    approx.or(((Float) num).isApproximate());
                    break;
            }
            
            return resultBk;
        }
        @Override
        public StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx) {
            return num.toResultString(b, context, approx);
        }
    },
    BINARY("binary") {
        @Override
        protected Block toResultBlock(Context context, Number num, MutableBoolean approx) {
            return AnswerFormatter.toResultBlock(context, num, approx, 2);
        }
        @Override
        public StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx) {
            return AnswerFormatter.toResultString(num, b, context, approx, 2);
        }
    },
    HEXADECIMAL("hexadecimal") {
        @Override
        protected Block toResultBlock(Context context, Number num, MutableBoolean approx) {
            return AnswerFormatter.toResultBlock(context, num, approx, 16);
        }
        @Override
        public StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx) {
            return AnswerFormatter.toResultString(num, b, context, approx, 16);
        }
    },
    SCIENTIFIC("scientific notation") {
        @Override
        protected Block toResultBlock(Context context, Number num, MutableBoolean approx) {
            Pair<Number, Number> result = convert(context, num, approx);
            
            Block left = toBlock(result.left, context, approx);
            Block right = result.right.toResultBlock(context, null, approx);
            
            return Block.join(left, new StyleBlock(Block.of("×10")).set(Setting.NUMBER_STYLE), new SuperscriptBlock(right));
        }
        @Override
        public StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx) {
            Pair<Number, Number> result = convert(context, num, approx);
            
            b.append(toString(result.left, context, approx));
            b.append("×10^");
            result.right.toResultString(b, context, approx);
            
            return b;
        }
        private Pair<Number, Number> convert(Context context, Number num, MutableBoolean approx) {
            MathEnv env = context.getMathEnvironment();
            
            int sign = num.signum();
            if (sign == 0) {
                return Pair.of(num, env.zero());
            }
            
            Number  scale = env.zero();
            Number  abs   = num.abs(env);
            Integer ten   = env.valueOf(10);
            Integer one   = env.one();
            
            while (env.relate(abs, ten) > 0) {
                scale = env.add(scale, one);
                abs   = env.divide(abs, ten);
            }
            while (env.relate(abs, one) < 0) {
                scale = env.subtract(scale, one);
                abs   = env.multiply(abs, ten);
            }
            
            if (sign < 0) {
                abs = abs.negate(env);
            }
            return Pair.of(abs, scale);
        }
        private String toString(Number m, Context c, MutableBoolean approx) {
            Decimal d = c.getMathEnvironment().toDecimal(m);
            String  s = d.toString();
            if (s.endsWith("...")) {
                s = s.substring(0, s.length() - 3);
            }
            approx.or(d.isApproximate());
            return s;
        }
        private Block toBlock(Number m, Context c, MutableBoolean approx) {
            return new StyleBlock(Block.of(toString(m, c, approx))).set(Setting.NUMBER_STYLE);
        }
    },
    @Deprecated
    PRIME_FACTORS("factor") {
        @Override
        protected Block toResultBlock(Context context, Number num, MutableBoolean approx) {
            return factor(num, context, approx).toResultBlock(context, DECIMAL, approx);
        }
        @Override
        public StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx) {
            return factor(num, context, approx).toResultString(b, context, DECIMAL, approx);
        }
        private Tuple factor(Number num, Context context, MutableBoolean approx) {
            MathEnv env     = context.getMathEnvironment();
            Integer intVal  = num.toInteger(env);
            if (intVal == null) {
                approx.set(true);
                intVal = num.toIntegerInexact(env);
            }
            return intVal.factor(env);
        }
    };
    
    private final String description;
    
    private AnswerFormatter(String description) {
        this.description = Objects.requireNonNull(description, "description");
    }
    
    public String description() {
        return description;
    }
    
    public Block toResultBlock(Computation comp, MutableBoolean approx) {
        Worker.executing();
        Context context = comp.getContext();
        Operand result  = comp.getResult();
        
        if (result != null) {
            if (!result.isUndefinition() && !result.isEmpty() && !result.isReference()) {
                Block resultBk;
                
                if (result.isNumber()) {
                    resultBk = toResultBlock(context, (Number) result, approx);
                } else {
                    resultBk = result.toResultBlock(context, this, approx);
                }
                
                return resultBk;
            }
        }
        
        return null;
    }
    
    protected abstract Block toResultBlock(Context context, Number num, MutableBoolean approx);
    
    public abstract StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx);
    
    private static Block toResultBlock(Context context, Number num, MutableBoolean approx, int radix) {
        String value = toString(context.getMathEnvironment(), num, approx, radix);
        
        return new StyleBlock(new TextBlock(value)).set(Setting.NUMBER_STYLE);
    }
    
    private static StringBuilder toResultString(Number num, StringBuilder b, Context context, MutableBoolean approx, int radix) {
        return b.append(toString(context.getMathEnvironment(), num, approx, radix));
    }
    
    private static String toString(MathEnv env, Number num, MutableBoolean approx, int radix) {
        Integer asInt = num.toInteger(env);
        
        if (asInt == null) {
            approx.set(true);
            asInt = num.toIntegerInexact(env);
        }
        
        BigInteger big = asInt.toBigIntegerExact(env);
        boolean    neg = big.signum() < 0;
        if (neg)   big = big.negate();
        
        String prefix = (neg ? "-" : "") + ((radix == 2) ? "0b" : (radix == 16) ? "0x" : "");
        String value  = big.toString(radix).toUpperCase(Locale.ROOT);
        
        return prefix + value;
    }
    
    public static AnswerFormatter replnull(AnswerFormatter fmt) {
        return fmt == null ? DECIMAL : fmt;
    }
}