/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import static eps.util.Misc.*;

import java.util.*;
import java.util.function.*;

final class TreeBuilder {
    private final Context     context;
    private final List<Token> tokens;
    
    private TreeBuilder(Context context, List<Token> tokens) {
        Log.constructing(TreeBuilder.class);
        
        this.context = Objects.requireNonNull(context, "context");
        this.tokens  = Objects.requireNonNull(tokens,  "tokens");
        
        Log.note(TreeBuilder.class, "init", "infix = ", tokens);
    }
    
    static Node buildTree(Context context) {
        List<Token> tokens = new Parser(context).parse();
        return new TreeBuilder(context, tokens).buildTree();
    }
    
    private Node buildTree() {
        List<Token> postfix = toPostfix();
        
        Log.note(TreeBuilder.class, "evaluate", "postfix = ", postfix);
        Log.note(TreeBuilder.class, "evaluate", () -> j(postfix, ", ", "[", "]", Token::toDebugString));
        
        Node root = createTree(postfix);
        assert tokensAreCorrect(root);
        return root;
    }
    
    private boolean tokensAreCorrect(Node root) {
        root.collectTokens(new Consumer<Token> () {
            private Token prev = null;
            @Override
            public void accept(Token curr) {
                if (prev != null) {
                    Token currPrev = curr.getPrev();
                    if (prev != currPrev) {
                        throw new AssertionError(f("prev = %s, curr = %s, currPrev = %s",
                                                   prev, curr, currPrev));
                    }
                    Token prevNext = prev.getNext();
                    if (prevNext != curr) {
                        throw new AssertionError(f("prev = %s, prevNext = %s, curr = %s",
                                                   prev, prevNext, curr));
                    }
                    int prevEnd   = prev.getSource().getSourceEnd();
                    int currStart = curr.getSource().getSourceStart();
                    if (prevEnd > currStart) {
                        throw new AssertionError(f("prev = %s, prevEnd = %s, curr = %s, currStart = %s",
                                                   prev, prevEnd, curr, currStart));
                    }
                }
                prev = curr;
            }
        });
        return true;
    }
    
    private List<Token> toPostfix() {
        List<Token>  infix     = tokens;
        int          size      = infix.size();
        List<Token>  postfix   = new ArrayList<>(size);
        Deque<Token> operators = new ArrayDeque<>();
        
        for (int i = 0; i < size; ++i) {
            Token token = infix.get(i);
            
            if (token.isUnresolved() || token.isAmbiguous()) {
                throw EvaluationException.unresolved(token);
            }
            
            if (token.isOperand()) {
                postfix.add(token);
                continue;
            } // else
            
            if (token.isOperator()) {
                Operator op = (Operator) token.getValue();
                
                // Special handling for n-ary operators. This is essentially
                // the same as a regular operator, but n-ary operators are
                // always considered as the lowest precedence. For example, the
                // expression "2+2, 3+3" must always be grouped as "(2+2), (3+3)"
                // and never "2+(2, 3)+3".
                if (op.isNary()) {
                    while (!operators.isEmpty()) {
                        Token topToken = operators.peek();
                        // Respecting parentheses.
                        if (!topToken.isOperator() || topToken.isLeftSpan()) {
                            break;
                        } // else
                        
                        Operator top = (Operator) topToken.getValue();
                        
                        if (top.isNary()) {
                            // If they're the same operator, then we mark the
                            // one we're about to put on to the output as having
                            // another.
                            if (top == op) {
                                topToken = new Token.OfSymbol(topToken, top, false);
                            } else {
                                if (top.compareTo(op) < 0)
                                    break;
                            }
                        }
                        
                        operators.pop();
                        postfix.add(topToken);
                    }
                    
                    operators.push(token);
                    continue;
                } // else
                
                if (op.isSpan()) {
                    if (token.isRightSpan()) {
                        for (;;) {
                            if (operators.isEmpty()) {
                                throw EvaluationException.missingLeftSpan((SpanOp) op);
                            }
                            // more
                            Token top = operators.pop();
                            if (top.isLeftSpan()) {
                                SpanOp span = (SpanOp) top.getValue();
                                
                                if (span.getParent() == ((SpanOp) op).getParent()) {
                                    operators.push(new Token.OfSymbol(token, span.getParent(), true));
//                                    operators.push(new Token.OfSymbol(top, token, span.getParent(), true));
//                                    postfix.add(new Token.OfSymbol(top, token, span.getParent(), true));
                                    break;
                                    
                                } else {
                                    // Span operators must be nested correctly.
                                    // For example, this catches a situation such
                                    // as "( { x ) }", where the span operators
                                    // overlap in an incorrect way.
                                    throw EvaluationException.missingRightSpan(span);
                                }
                            }
                            postfix.add(top);
                        }
                    } else {
                        operators.push(token);
                    }
                    continue;
                } // else
                
                while (!operators.isEmpty()) {
                    Token topToken = operators.peek();
                    // Respecting parentheses.
                    if (!topToken.isOperator() || topToken.isLeftSpan()) {
                        break;
                    } // else
                    
                    Operator top = (Operator) topToken.getValue();
                    
                    // We never want to flip unary and binary operators,
                    // for example "2^-3" should always be "2^(-3)", even if
                    // ^ has a higher precedence than -.
                    if (op.isUnary() && ((UnaryOp) op).isPrefix()) {
                        if (top.isBinary())
                            break;
                        // We never want to flip unary prefix operators.
                        // For example -sqrt 2 should never become sqrt -2,
                        // even if unary negation has higher precedence than
                        // sqrt.
                        if (top.isUnary() && ((UnaryOp) top).isPrefix())
                            break;
                    } // else
                    
                    if (top.isUnary()) {
                        UnaryOp unary = (UnaryOp) top;
                        // Likewise, "2!^3" should always be "(2!)^3", even if
                        // ^ has a higher precedence than !.
                        if (unary.isSuffix()) {
                            if (op.isBinary()) {
                                postfix.add(operators.pop());
                                continue;
                            }
                            // And we never want to flip unary suffix operators.
                            // For example, 2!% should never become 2%!, even if
                            // % has a higher precedence than !.
                            if (op.isUnary() && ((UnaryOp) op).isSuffix()) {
                                postfix.add(operators.pop());
                                continue;
                            }
                        }
                        // Experimental: always bind function-like unary prefix
                        // operators to span operators. For example, sin(2)^3 is
                        // always interpreted as (sin 2)^3 and never sin((2)^3).
                        if (unary.isPrefix() && unary.isFunctionLike()) {
                            if (!postfix.isEmpty()) {
                                Token last = postfix.get(postfix.size() - 1);
                                if (last.isSpanOp()) {
                                    postfix.add(operators.pop());
                                    continue;
                                }
                            }
                        }
                    } // else
                    
                    if (top.getAssociativity().isLeftToRight()) {
                        if (top.compareTo(op) < 0)
                            break;
                    } else {
                        if (top.compareTo(op) <= 0)
                            break;
                    } // else
                    
                    postfix.add(operators.pop());
                }
                
                operators.push(token);
                continue;
            } // else
            
            /*
            if (token.isLeftParenthesis()) {
                operators.push(token);
                continue;
            } // else
            
            if (token.isRightParenthesis()) {
                for (;;) {
                    if (operators.isEmpty()) {
                        throw new EvaluationException("missing left parenthesis");
                    }
                    Token top = operators.pop();
                    if (top.isLeftParenthesis()) {
                        //////////////////////////
                        // Experimental stuff here. (Seems fine...?)
                        int   last  = postfix.size() - 1;
                        Token value = postfix.get(last);
                        Token expr  = new Token.OfParenthesizedExpression(value, top, token);
                        postfix.set(last, expr);
                        //////////////////////////
                        break;
                    }
                    postfix.add(top);
                }
                continue;
            } // else
            */
            
            throw EvaluationException.unexpected(token);
        }
        
        for (Token op : operators) {
//            if (op.isParenthesis()) {
//                throw new EvaluationException("missing right parenthesis");
//            }
            if (op.isLeftSpan() && !op.isSymmetricSpan()) {
                throw EvaluationException.missingRightSpan((SpanOp) op.getValue());
            }
            postfix.add(op);
        }
        
        return postfix;
    }
    
    private Node createTree(List<Token> postfix) {
        Deque<Node> operands = new ArrayDeque<>();
        
        for (Token token : postfix) {
            Node node = createNode(token, operands);
            operands.push(node);
        }
        
        if (operands.size() != 1) {
            throw EvaluationException.fmt("internal error; too many operands in the stack: %s; postfix = %s", operands, postfix);
        }
        Node result = operands.pop();
        if (result instanceof Node.ListBuilder) {
            throw EvaluationException.fmt("internal error; found a builder in the stack: %s; postfix = %s", result, postfix);
        }
        return result;
    }
    
    private Node createNode(Token token, Deque<Node> operands) {
//        if (token.isParenthesizedExpression()) {
//            Token.OfParenthesizedExpression expr = (Token.OfParenthesizedExpression) token;
//            
//            Node value = createNode(expr.getValue(), operands);
//            
//            return new Node.OfParenthesis(token, value);
//        }
        
        if (token.isOperand()) {
            return new Node.OfOperand(token, (Operand) token.getValue());
        }
        
        if (token.isOperator()) {
            Operator op = (Operator) token.getValue();
            
            Node result;
            
            if (op.isUnary()) {
                result = new Node.OfUnaryOp(token, (UnaryOp) op, operands.pop());
                
            } else if (op.isBinary()) {
                Node rhs = operands.pop();
                Node lhs = operands.pop();
                
                result = new Node.OfBinaryOp(token, (BinaryOp) op, lhs, rhs);
                
            } else if (op.isNary()) {
                Node rhs = operands.pop();
                Node lhs = operands.pop();
                
                Node.ListBuilder builder;
                
                if (lhs instanceof Node.ListBuilder) {
                    builder = ((Node.ListBuilder) lhs).add(rhs);
//                    ((Token.Abstract) builder.getToken()).append(token);
                } else {
                    builder = new Node.ListBuilder(token).add(lhs).add(rhs);
                }
                
                if (token.isTerminal()) {
                    result = new Node.OfNaryOp(builder.getToken(), (NaryOp) op, builder.list());
                } else {
                    result = builder;
                }
            } else if (op.isSpan()) {
                result = new Node.OfSpanOp(token, (SpanOp) op, operands.pop());
                
            } else {
                throw new AssertionError(op.toString());
            }

            return result;
        }
        
        throw EvaluationException.unexpected(token);
    }
}