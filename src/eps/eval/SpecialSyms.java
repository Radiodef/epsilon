/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.job.*;
import eps.util.*;
import eps.math.*;
import eps.math.Number;
import eps.math.Integer;
import eps.eval.Operator.*;

import java.util.*;

// Any symbols that require special consideration because they e.g.
// work on syntax trees directly or otherwise don't fit in as a typical
// symbol.
public final class SpecialSyms {
    private SpecialSyms() {
    }
    
    // TODO: interpret a span assignment like {x} := undefined as an actual
    //       span assignment, rather than calling getExpressionNode() and
    //       interpreting it as a variable assignment.
    public static class AssignOp extends BinaryOp {
        public AssignOp(BinaryOp.Builder builder) {
            super(builder);
        }
        
        @Override
        public AssignOp configure(Symbol.Builder<?, ?> b) {
            return new AssignOp((BinaryOp.Builder) b);
        }
        
        @Override
        public Operand apply(Context context, Node lhs, Node rhs) {
            Worker.executing();
            
            lhs = lhs.getExpressionNode();
            rhs = rhs.getExpressionNode();
            
            switch (lhs.getNodeKind()) {
                case OPERAND:
                    return this.apply(context, ((Node.OfOperand) lhs).get(), rhs);
                case UNARY_OP:
                case BINARY_OP:
                case NARY_OP:
                case SPAN_OP:
                    return this.apply(context, (Node.OfOperator) lhs, rhs);
            }
            
            throw UnsupportedException.operandTypes(lhs, rhs, "assignment");
        }
        
        private boolean shouldUndefine(Context c, Node rhs, Operand result) {
            if (result == null) {
                rhs = rhs.getExpressionNode();
                // We don't want to evaluate the expression unless
                // it's an operand, because otherwise funny things
                // could happen.
                if (rhs.getNodeKind() != Node.Kind.OPERAND) {
                    return false;
                }
                try {
                    result = rhs.evaluate(c);
                } catch (EvaluationException ignored) {
                    // TODO (this means rhs was probably a function body)
                    return false;
                }
            }
            if (result.isUndefinition()) {
                if (rhs.getNodeKind() == Node.Kind.OPERAND) {
                    return ((Node.OfOperand) rhs).get() != SpecialSyms.UNDEFINED; // __undefined__ vs. undefined
                }
                return true;
            }
            return false;
        }
        
        private Operand apply(Context context, Operand lhs, Node rhs) {
            String name;
            Operand result = null;
            
            switch (lhs.getOperandKind()) {
//                case CONSTANT:
                case VARIABLE:
                    name   = Symbol.requireNonIntrinsic(((Symbol) lhs).getName());
                    result = rhs.evaluate(context);
                    
                    if (result.isReference()) {
                        Symbol sym = ((Reference) result).get();
                        if (sym.isVariable()) {
                            result = ((Variable) sym).evaluate(context);
                        } else {
                            break;
                        }
                    }
                    
                    if (shouldUndefine(context, rhs, result)) {
                        context.undefine((Symbol) lhs);
                        return Undefinition.INSTANCE;
                    } else {
                        Variable var = Variable.fromTree(name, context, rhs, result);
                        context.define(var);
                        return new Reference(var);
                    }
                default:
                    // We do this redundantly because we don't want to evaluate
                    // it before we check the variable name.
                    result = rhs.evaluate(context);
                    break;
            }
            
            return this.apply(context, lhs, result); // throws
        }
        
        private Operand apply(Context context, Node.OfOperator lhs, Node rhs) {
            Operator op = lhs.getOperator();
            Symbol.requireNonIntrinsic(op.getName());
            
            if (shouldUndefine(context, rhs, null)) {
                context.undefine(op);
                return Undefinition.INSTANCE;
            } else {
                createNewOperator: {
                    if (Trees.isAliasForward(context, lhs, rhs)) {
                        Operator fwd =
                            Trees.findFirst(rhs, Node.OfOperator.class).get()
                                 .getOperator();
                        //
                        // Only create an alias if the delegated-to operator is
                        // in the main symbol store. This avoids issues related
                        // to expressions like f(x) := f(x) which should result
                        // in infinite recursion rather than creating an alias
                        // to the FromScope operator on the right-hand side which
                        // will throw UnsupportedException.unresolved.
                        //
                        if (context.getSymbols().contains(fwd)) {
                            // Minor note: people can do something like
                            // (a) op (a) := a + a. It works fine; simply
                            // declares an alias for binary +, but it's
                            // weird looking.
                            // TreeFunction rejects such forms with duplicate
                            // names.
                            // We should probably do the same here as well.
                            op = fwd.createAlias(op);
                            break createNewOperator;
                        }
                    }  // else
                    switch (lhs.getNodeKind()) {
                        case UNARY_OP:
                            op = new UnaryOp.FromTree(context, (Node.OfUnaryOp) lhs, rhs);
                            break;
                        case SPAN_OP:
                            op = new SpanOp.FromTree(context, (Node.OfSpanOp) lhs, rhs);
                            break;
                        case NARY_OP:
                        case BINARY_OP:
                            op = this.createBinaryOrNary(context, lhs, rhs);
                            break;
                        default:
                            throw UnsupportedException.operandTypes(lhs, rhs, "assignment");
                    }
                }
                context.define(op);
                return new Reference(op);
            }
        }
        
        private Operator createBinaryOrNary(Context context, Node.OfOperator lhs, Node rhs) {
            List<Node> operands  = lhs.getOperands();
            int        nOperands = operands.size();
            if (nOperands != 2) {
                throw EvaluationException.fmt("%s is not a valid operator declaration; too many parameters",
                                              lhs.getExpressionSource());
            }
            
            Node last = operands.get(nOperands - 1).getExpressionNode();
            
            if (SpecialSyms.isEllipse(context, last)) {
                if (lhs.isBinaryOp()) {
                    return new NaryOp.FromTree(context, (Node.OfBinaryOp) lhs, rhs);
                } else {
                    return new NaryOp.FromTree(context, (Node.OfNaryOp) lhs, rhs);
                }
            }
            
            if (lhs.isBinaryOp()) {
                return new BinaryOp.FromTree(context, (Node.OfBinaryOp) lhs, rhs);
            } else {
                return new BinaryOp.FromTree(context, (Node.OfNaryOp) lhs, rhs);
            }
        }
    }
    
    public static final BinaryOp ASSIGN =
        new AssignOp(new BinaryOp.Builder()
                                 .setName("assign")
                                 .makeIntrinsic()
                                 .addPreferredAlias(":=")
                                 .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                                 .setPrecedence(Operator.ASSIGN_PRECEDENCE)
                                 .setAssociativity(Associativity.RIGHT_TO_LEFT));
    
    private static Operand evaluateConditionDo(Context context, Node lhs, Node rhs) {
        Operand condition = lhs.evaluate(context);
        
        if (condition.isTruthy(context)) {
            return rhs.evaluate(context);
        } else {
            return null;
        }
    }
    
    public static class ConditionDoOp extends BinaryOp {
        public ConditionDoOp(BinaryOp.Builder b) {
            super(b);
        }
        @Override
        public ConditionDoOp configure(Symbol.Builder<?, ?> b) {
            return new ConditionDoOp((BinaryOp.Builder) b);
        }
        @Override
        public Operand apply(Context context, Node lhs, Node rhs) {
            Worker.executing();
            
            Operand result = SpecialSyms.evaluateConditionDo(context, lhs, rhs);
            if (result != null) {
                return result;
            } else {
                return Undefinition.INSTANCE;
            }
        }
    }
    
    public static final BinaryOp CONDITION_DO =
        new ConditionDoOp(new BinaryOp.Builder()
                                      .setName("condition_do")
                                      .makeIntrinsic()
                                      .addPreferredAlias(":")
                                      .setEchoFormatter(EchoFormatter.OF_RIGHT_SPACED_BINARY_OP)
                                      .setPrecedence(Operator.ASSIGN_PRECEDENCE)
                                      .setAssociativity(Associativity.LEFT_TO_RIGHT));
    
    public static class ForConditionsOp extends UnaryOp {
        public ForConditionsOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public ForConditionsOp configure(Symbol.Builder<?, ?> b) {
            return new ForConditionsOp((UnaryOp.Builder) b);
        }
        
        @Override
        public Operand apply(Context context, Node operand) {
            Worker.executing();
            
            operand = operand.getExpressionNode();
            
            switch (operand.getNodeKind()) {
                case NARY_OP:
                    Node.OfNaryOp nary = (Node.OfNaryOp) operand;
                    
                    if (nary.getOperator().getDelegate() == NaryOp.TUPLE) {
                        for (Node node : nary.getOperands()) {
                            Operand result = this.evaluateConditionDo(context, node);
                            if (result != null) {
                                return result;
                            }
                        }
                        return Undefinition.INSTANCE;
                    }
                    
                    break;
                default:
                    break;
            }
            
            Operand result = evaluateConditionDo(context, operand);
            if (result != null) {
                return result;
            } else {
                return Undefinition.INSTANCE;
            }
        }
        
        private Operand evaluateConditionDo(Context context, Node node) {
            node = node.getExpressionNode();
            
            switch (node.getNodeKind()) {
                case BINARY_OP:
                    Node.OfBinaryOp binary = (Node.OfBinaryOp) node;
                    
                    if (binary.getOperator().getDelegate() == CONDITION_DO) {
                        Node    lhs    = binary.getLeftHandSide();
                        Node    rhs    = binary.getRightHandSide();
                        Operand result = SpecialSyms.evaluateConditionDo(context, lhs, rhs);
                        return result;
                    }
                    
                    break;
                default:
                    break;
            }
            
            // TODO: maybe throw?
            return node.evaluate(context);
        }
    }
    
    public static final UnaryOp FOR_CONDITIONS =
        new ForConditionsOp(new UnaryOp.Builder()
                                       .setName("for_conditions")
                                       .makeIntrinsic()
                                       .addPreferredAlias("for")
                                       .makeFunction()
                                       .anyTuple());
    
    public static abstract class AbstractSummationOp extends UnaryOp {
        public AbstractSummationOp(UnaryOp.Builder builder) {
            super(builder.setTupleCount(3));
        }
        
        protected String description() {
            return this.getName();
        }
        
        @Override
        public Operand apply(Context context, Node operand) {
            operand = operand.getExpressionNode();
            
            switch (operand.getNodeKind()) {
                case NARY_OP:
                    return this.apply(context, (Node.OfNaryOp) operand);
                default:
                    break;
            }
            
            throw UnsupportedException.operandType(operand, this.getName());
        }
        
        private Operand apply(Context context, Node.OfNaryOp node) {
            NaryOp nary = node.getOperator();
            
            if (nary.getDelegate() != NaryOp.TUPLE) {
                throw UnsupportedException.operandType(node, this.getName());
            }
            
            List<Node> args = node.getOperands();
            int        size = args.size();
            
            if (size != 3) {
                throw UnsupportedException.fmt("required 3 arguments to operator %s; found %d: %s",
                                               this.getName(), size, node.getExpressionSource());
            }
            
            Node    assign = args.get(0).getExpressionNode();
            Operand max    = args.get(1).getExpressionNode().evaluate(context);
            Node    expr   = args.get(2).getExpressionNode();
            
            if (!max.isNumber() || !((Number) max).isInteger(context.getMathEnvironment())) {
                throw UnsupportedException.fmt("%s max must be an integer; found %s",
                                               this.description(), max.toResultString(context));
            }
            
            Variable.Temp var;
            createVar: {
                if (assign.getNodeKind().isBinaryOp()) {
                    Node.OfBinaryOp binary = (Node.OfBinaryOp) assign;
                    
                    if (binary.getOperator().getDelegate() == SpecialSyms.ASSIGN) {
                        Node lhs = binary.getLeftHandSide().getExpressionNode();
                        Node rhs = binary.getRightHandSide().getExpressionNode();
                        
                        if (lhs.getNodeKind().isOperand()) {
                            Operand op = ((Node.OfOperand) lhs).get();
                            if (op.isVariable()) {
                                var = Variable.createTemp((Variable) op, rhs.evaluate(context));
                                break createVar;
                            }
                        }
                    }
                }
                throw UnsupportedException.fmt("0th argument to %s must be a variable assignment; found %s",
                                               this.description(), assign.getExpressionSource());
            }
            
            Symbols scope = Symbols.Concurrent.empty();
            Symbols prev  = context.getScope();
            scope.put(var);
            context.setScope(new Symbols.Overlay(scope, prev));
            try {
                return this.apply(context, var, (Integer) max, expr);
            } finally {
                context.setScope(prev);
            }
        }
        
        protected abstract Number initial(MathEnv env);
        protected abstract Number accumulate(MathEnv env, Number lhs, Number rhs);
        
        private Operand apply(Context context, Variable.Temp var, Integer max, Node expr) {
            Operand val = var.evaluate(context);
            MathEnv env = context.getMathEnvironment();
            
            if (!val.isNumber() || !((Number) val).isInteger(env)) {
                throw UnsupportedException.fmt("index of %s must start as an integer; found %s",
                                               this.description(), val.toResultString(context));
            }
            
            Integer index  = (Integer) val;
            Integer one    = env.one();
            Number  result = this.initial(env);
            
            if (index.relateTo(max, env) > 0) {
                throw UnsupportedException.fmt("summation maximum index must not be less than the starting "
                                             + "index; found %s := %s to %s", var.getName(), index, max);
            }
            
            while (index.relateTo(max, env) <= 0) {
                Worker.executing();
                
                Operand interim = expr.evaluate(context);
                if (!interim.isNumber()) {
                    throw UnsupportedException.operandType(interim, this.getName());
                }
                
                result = this.accumulate(env, result, (Number) interim);
                index  = index.add(one, env).toIntegerExact(env);
                var.setValue(index);
            }
            
            return result;
        }
    }
    
    public static class SummationOp extends AbstractSummationOp {
        public SummationOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public SummationOp configure(Symbol.Builder<?, ?> b) {
            return new SummationOp((UnaryOp.Builder) b);
        }
        @Override
        protected Number initial(MathEnv env) {
            return env.zero();
        }
        @Override
        protected Number accumulate(MathEnv env, Number lhs, Number rhs) {
            return env.add(lhs, rhs);
        }
    }
    
    public static final UnaryOp SUMMATION =
        new SummationOp(new UnaryOp.Builder()
                                   .setName("summation")
                                   .makeIntrinsic()
                                   .addPreferredAliases("sum", "sigma", "Σ")
                                   .makeFunction()
                                   .setEchoFormatter(EchoFormatter.ofFunction("Σ")));
    
    public static class ProductOp extends AbstractSummationOp {
        public ProductOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public ProductOp configure(Symbol.Builder<?, ?> b) {
            return new ProductOp((UnaryOp.Builder) b);
        }
        @Override
        protected Number initial(MathEnv env) {
            return env.one();
        }
        @Override
        protected Number accumulate(MathEnv env, Number lhs, Number rhs) {
            return env.multiply(lhs, rhs);
        }
    }
    
    public static final UnaryOp PRODUCT =
        new ProductOp(new UnaryOp.Builder()
                                 .setName("product")
                                 .makeIntrinsic()
                                 .addPreferredAliases("product", "∏") // note: ∏, not Π
                                 .makeFunction()
                                 .setEchoFormatter(EchoFormatter.ofFunction("∏")));
    
    public static class SubscriptOp extends BinaryOp {
        public SubscriptOp(BinaryOp.Builder b) {
            super(b);
        }
        @Override
        public SubscriptOp configure(Symbol.Builder<?, ?> b) {
            return new SubscriptOp((BinaryOp.Builder) b);
        }
        @Override
        public Operand apply(Context context, Operand lhs, Operand rhs) {
            int size = this.sizeOf(lhs);
            if (size >= 0) {
                MathEnv env = context.getMathEnvironment();
                
                if (!rhs.isNumber() || !((Number) rhs).isInteger(env)) {
                    throw UnsupportedException.fmt("the right-hand side of the subscript operator must be an integer; found %s", rhs);
                }
                
                Integer count = env.valueOf(this.sizeOf(lhs));
                Integer index = (Integer) rhs;
                
                if (index.relateTo(env.zero(), env) < 0 || count.relateTo(index, env) <= 0) {
                    throw UndefinedException.fmt("subscript index %s outside the range %s (inclusive) to %s (exclusive) for tuple %s",
                                                 index, 0, count, lhs.toResultString(context));
                }
                
                return this.getElement(context, lhs, index.toPrimitiveIntExact(env));
            }
            throw UnsupportedException.operandTypes(lhs, rhs, getName());
        }
        private int sizeOf(Operand op) {
            switch (op.getOperandKind()) {
                case       VARIABLE:
                case         NUMBER: return 1;
                case          TUPLE: return ((Tuple) op).count();
                case STRING_LITERAL: return ((StringLiteral) op).codeLength();
                default            : return -1;
            }
        }
        private Operand getElement(Context c, Operand op, int index) {
            switch (op.getOperandKind()) {
                case       VARIABLE: return op.evaluate(c);
                case         NUMBER: return op;
                case          TUPLE: return ((Tuple) op).get(index);
                case STRING_LITERAL: return new StringLiteral(((StringLiteral) op).codePointAt(index));
                default: throw new AssertionError(op);
            }
        }
    }
    
    public static final BinaryOp SUBSCRIPT =
            new SubscriptOp(new BinaryOp.Builder()
                                        .setName("subscript")
                                        .makeIntrinsic()
                                        .addPreferredAliases("sub", "#")
                                        .setPrecedence(Operator.VERY_HIGH_PRECEDENCE)
                                        .setEchoFormatter(EchoFormatter.OF_SUB_BINARY_OP));
    
    public static class MapElementsOp extends BinaryOp {
        public MapElementsOp(BinaryOp.Builder b) {
            super(b);
        }
        
        @Override
        public MapElementsOp configure(Symbol.Builder<?, ?> b) {
            return new MapElementsOp((BinaryOp.Builder) b);
        }
        
        @Override
        public Operand apply(Context context, Node nodeL, Node nodeR) {
            nodeR = nodeR.getExpressionNode();
            if (nodeR.isBinaryOp()) {
                Node.OfBinaryOp binary = (Node.OfBinaryOp) nodeR;
                if (binary.getOperator().getDelegate() == ASSIGN) {
                    Node var = binary.getLeftHandSide().getExpressionNode();
                    if (var.isOperand()) {
                        Operand op = ((Node.OfOperand) var).get();
                        if (op.isVariable()) {
                            return apply(context, nodeL.evaluate(context), (Variable) op, binary.getRightHandSide());
                        }
                    }
                }
            }
            throw UnsupportedException.fmt("right-hand side of %s must be a variable assignment; found %s",
                                           getName(), nodeR.getExpressionSource());
        }
        
        private Operand apply(Context context, Operand lhs, Variable var, Node expr) {
            if (lhs.isTuple() && ((Tuple) lhs).count() == 0) {
                return lhs;
            }
            
            Symbols       scope = Symbols.Concurrent.empty();
            Variable.Temp temp  = Variable.createTemp(var, lhs.isTuple() ? ((Tuple) lhs).get(0) : lhs);
            scope.put(temp);
            
            Symbols prev = context.setScope(new Symbols.Overlay(scope, context.getScope()));
            try {
                if (lhs.isTuple()) {
                    Tuple         tuple  = (Tuple) lhs;
                    int           count  = tuple.count();
                    Tuple.Builder result = Tuple.builder(count);
                    for (int i = 0;;) {
                        Worker.executing();
                        result.append(expr.evaluate(context));
                        if (( ++i ) < count) {
                            temp.setValue(tuple.get(i));
                        } else {
                            return result.build();
                        }
                    }
                } else {
                    return expr.evaluate(context);
                }
            } finally {
                context.setScope(prev);
            }
        }
    }
    
    public static final BinaryOp MAP_ELEMENTS =
        new MapElementsOp(new BinaryOp.Builder()
                                      .setName("map")
                                      .makeIntrinsic()
                                      .addPreferredAlias("->")
                                      .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                                      .setPrecedence(Operator.ASSIGN_PRECEDENCE)
                                      .setAssociativity(Associativity.RIGHT_TO_LEFT));
    
    public static class ExpressionOfOp extends UnaryOp {
        public ExpressionOfOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public ExpressionOfOp configure(Symbol.Builder<?, ?> b) {
            return new ExpressionOfOp((UnaryOp.Builder) b);
        }
        @Override
        public Operand apply(Context context, Node node) {
            switch (node.getNodeKind()) {
                case OPERAND:
                    Operand op = ((Node.OfOperand) node).get();
                    switch (op.getOperandKind()) {
                        case VARIABLE:
                            op = ((Variable) op).resolve(context);
                            if (op instanceof Variable.FromTree) {
                                String src = ((Variable.FromTree) op).getTreeFunction().getExpressionSource(context);
                                return new StringLiteral(src);
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            String src = node.getExpressionSource().toString();
            return new StringLiteral(src);
        }
    }
    
    public static final UnaryOp EXPRESSION_OF =
        new ExpressionOfOp(new UnaryOp.Builder()
                                      .setName("expression_of")
                                      .makeIntrinsic()
                                      .addPreferredAlias("exprof")
                                      .makeFunction()
                                      .anyTuple());
    
    public static class TypeOfOp extends UnaryOp {
        public TypeOfOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public TypeOfOp configure(Symbol.Builder<?, ?> b) {
            return new TypeOfOp((UnaryOp.Builder) b);
        }
        @Override
        public Operand apply(Context context, Node node) {
            Operand result = node.evaluate(context);
            String  typeof = result.getOperandKind().toTypeOfString();
            return new StringLiteral(typeof);
        }
    }
    
    public static final UnaryOp TYPE_OF =
        new TypeOfOp(new UnaryOp.Builder()
                                .setName("type_of")
                                .makeIntrinsic()
                                .addPreferredAlias("typeof")
                                .makeFunction()
                                .anyTuple());
    
    public static class SizeOfOp extends UnaryOp {
        public SizeOfOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public SizeOfOp configure(Symbol.Builder<?, ?> b) {
            return new SizeOfOp((UnaryOp.Builder) b);
        }
        @Override
        public Operand apply(Context context, Node node) {
            MathEnv env = context.getMathEnvironment();
            Operand op  = node.evaluate(context);
            switch (op.getOperandKind()) {
                case TUPLE:
                    return env.valueOf(((Tuple) op).count());
                case STRING_LITERAL:
                    return env.valueOf(((StringLiteral) op).codeLength());
                case VARIABLE:
                case NUMBER:
                    return env.one();
                default:
                    throw UnsupportedException.operandType(op, getName());
            }
        }
    }
    
    public static final UnaryOp SIZE_OF =
        new SizeOfOp(new UnaryOp.Builder()
                                .setName("size_of")
                                .makeIntrinsic()
                                .addPreferredAlias("sizeof")
                                .makeFunction()
                                .anyTuple());
    
    public static class IsTruthyOp extends UnaryOp {
        public IsTruthyOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public IsTruthyOp configure(Symbol.Builder<?, ?> b) {
            return new IsTruthyOp((UnaryOp.Builder) b);
        }
        @Override
        public Operand apply(Context context, Node node) {
            return eps.math.Boolean.valueOf(node.evaluate(context).isTruthy(context));
        }
    }
    
    public static final UnaryOp IS_TRUTHY =
        new IsTruthyOp(new UnaryOp.Builder()
                                  .setName("is_truthy")
                                  .makeIntrinsic()
                                  .addPreferredAlias("?")
                                  .setPrecedence(Operator.FACTORIAL_PRECEDENCE)
                                  .setAffix(UnaryOp.Affix.SUFFIX)
                                  .setTuplesAsForEach(true));
    
    public static class FailOp extends UnaryOp {
        public FailOp(UnaryOp.Builder b) {
            super(b);
        }
        @Override
        public FailOp configure(Symbol.Builder<?, ?> b) {
            return new FailOp((UnaryOp.Builder) b);
        }
        @Override
        public Operand apply(Context context, Node operand) {
            return this.apply(context, operand.evaluate(context));
        }
        @Override
        protected Operand apply(Context context, Operand operand) {
            throw new EvaluationException(operand.toResultString(context));
        }
    }
    
    public static final UnaryOp FAIL =
        new FailOp(new UnaryOp.Builder()
                              .setName("fail")
                              .makeIntrinsic()
                              .addPreferredAlias("fail")
                              .makeFunction()
                              .anyTuple());
    
    public enum VariableFunction implements Variable.VariableFunction {
        DEBUG_THROW {
            @Override
            public Operand apply(Variable self, Context c) {
                final class ThrowConstantException extends RuntimeException {
                    ThrowConstantException(Variable self) {
                        super("evaluation of " + self.getName() + " constant");
                    }
                }
                throw new ThrowConstantException(self);
            }
        },
        DEBUG_CRASH {
            private void crash(boolean crash) {
                if (crash) {
                    crash(crash);
                }
            }
            @Override
            public Operand apply(Variable self, Context c) {
                crash(true);
                throw new AssertionError();
            }
        },
        DEBUG_HALT {
            @Override
            public Operand apply(Variable self, Context c) {
                for (;;) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException x) {
                        throw new AssertionError("interrupted", x);
                    }
                    Worker.executing();
                }
            }
        },
        UNDEFINITION {
            @Override
            public Operand apply(Variable self, Context c) {
                return Undefinition.INSTANCE;
            }
        },
        ELLIPSE {
            @Override
            public Operand apply(Variable self, Context c) {
                throw UnsupportedException.fmt("evaluation of %s", self.getName());
            }
        },
        ANSWER {
            @Override
            public Operand apply(Variable self, Context c) {
                Symbols       syms  = c.getSymbols();
                Symbols.Group group = syms.get(self.getName());
                Variable      var   = (Variable) group.get(Symbol.Kind.VARIABLE);
                if (var != self) {
                    return var.evaluate(c);
                }
                throw new EvaluationException("no calculations performed; no answer set");
            }
        }
    }
    
    @IfDeveloper
    public static final Variable DEBUG_THROW =
        new Variable.Builder()
                    .setName("throw")
                    .makeIntrinsic()
                    .addPreferredAlias("throw")
                    .setFunction(VariableFunction.DEBUG_THROW)
                    .build();
    
    @IfDeveloper
    public static final Variable DEBUG_CRASH =
        new Variable.Builder()
                    .setName("crash")
                    .makeIntrinsic()
                    .addPreferredAlias("crash")
                    .setFunction(VariableFunction.DEBUG_CRASH)
                    .build();
    
    @IfDeveloper
    public static final Variable DEBUG_HALT =
        new Variable.Builder()
                    .setName("halt")
                    .makeIntrinsic()
                    .addPreferredAlias("halt")
                    .setFunction(VariableFunction.DEBUG_HALT)
                    .build();
    
    public static final Variable UNDEFINED =
        new Variable.Builder()
                    .setName("undefined")
                    .makeIntrinsic()
                    .addPreferredAlias("undefined")
                    .setFunction(VariableFunction.UNDEFINITION)
                    .build();
    
    public static boolean isEllipse(Context context, Node node) {
        node = node.getExpressionNode();
        if (node.isOperand()) {
            Operand op = ((Node.OfOperand) node).get();
            if (op.isVariable()) {
                boolean isEllipse =
                    // We could also evaluate to a special value like Undefinition does.
                    ((Variable) op).resolve(context).getDelegate() == SpecialSyms.ELLIPSE;
                return isEllipse;
            }
        }
        return false;
    }
    
    public static final Variable ELLIPSE =
        new Variable.Builder()
                    .setName("ellipse")
                    .makeIntrinsic()
                    .addPreferredAlias("...")
                    .setFunction(VariableFunction.ELLIPSE)
                    .build();
    
    public static void setAnswer(Context context, Tree tree, Operand result) {
        SpecialSyms.setAnswer(context, tree.getRoot(), result);
//        tree.useTree(context, (root, treeContext) -> { SpecialSyms.setAnswer(treeContext, root, result); });
    }
    
    public static void setAnswer(Context context, Node node, Operand result) {
        switch (result.getOperandKind()) {
            case VARIABLE:
            case NUMBER:
            case TUPLE:
                Variable ans = Variable.fromTree(ANSWER.getName(), context, node, result);
                context.getSymbols().put(ans);
                break;
            default:
                break;
        }
    }
    
    public static final Variable ANSWER =
        new Variable.Builder()
                    .setName("answer")
                    .makeIntrinsic()
                    .addPreferredAlias("ans")
                    .setFunction(VariableFunction.ANSWER)
                    .build();
    
    public static final List<Symbol> DEFAULTS =
        Collections.unmodifiableList(Misc.collectConstants(SpecialSyms.class, Symbol.class));
}