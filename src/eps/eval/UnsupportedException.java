/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.json.*;
import static eps.util.Misc.*;

import java.util.*;

public final class UnsupportedException extends EvaluationException {
    static {
        JsonObjectAdapter.put(UnsupportedException.class,
                              UnsupportedException::toMap,
                              UnsupportedException::new);
    }
    
    public UnsupportedException(String description) {
        super(replnull(description, "unsupported"));
    }
    
    public UnsupportedException(Map<?, ?> map) {
        super(map);
    }
    
    public static UnsupportedException fmt(String fmt, Object... args) {
        return new UnsupportedException(String.format(fmt, args));
    }
    
    @Deprecated
    public static UnsupportedException operandType(Operand op) {
        return fmt("unsupported %s operand", op.getOperandKind().toLowerCase());
    }
    
    public static UnsupportedException operandType(Operand op, String operation) {
        return fmt("unsupported %s operand (%s) for operation '%s'",
                   op.getOperandKind().toLowerCase(), op, operation);
    }
    
    public static UnsupportedException operandType(String op, String operation) {
        return fmt("unsupported %s operand with operation '%s'", op, operation);
    }
    
    public static UnsupportedException operandType(Node node, String op) {
        return fmt("unsupported operand %s with operation '%s'", node.getExpressionSource(), op);
    }
    
    @Deprecated
    public static UnsupportedException operandTypes(Operand lhs, Operand rhs) {
        return fmt("unsupported operands %s and %s", lhs.getOperandKind().toLowerCase(), rhs.getOperandKind().toLowerCase());
    }
    
    public static UnsupportedException operandTypes(Operand lhs, Operand rhs, String op) {
        return fmt("unsupported operands %s (%s) and %s (%s) for operation '%s'",
                   lhs.getOperandKind().toLowerCase(), lhs,
                   rhs.getOperandKind().toLowerCase(), rhs, op);
    }
    
    public static UnsupportedException operandTypes(Node lhs, Node rhs, String op) {
        return operandTypes(lhs.getExpressionSource(), rhs.getExpressionSource(), op);
    }
    
    public static UnsupportedException operandTypes(Object lhs, Object rhs, String op) {
        return fmt("unsupported operands %s and %s for operation '%s'", lhs, rhs, op);
    }
    
    @Deprecated
    public static UnsupportedException overflow() {
        return new UnsupportedException("too large to compute");
    }
    
    public static UnsupportedException overflow(Object tooLarge) {
        return new UnsupportedException("number too large: " + tooLarge);
    }
    
    public static void requireArgumentCount(Tuple t, int count) {
        if (t.count() == count)
            return;
        throw fmt("required %d arguments; found %d", count, t.count());
    }
}