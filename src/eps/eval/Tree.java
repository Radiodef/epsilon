/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.util.*;
import eps.math.*;
import eps.block.*;
import eps.json.*;
import static eps.app.App.*;

import java.util.*;
import java.util.function.*;
import java.lang.Boolean;

@JsonSerializable
public final class Tree {
    @JsonProperty
    private final String source;
    @JsonProperty(interimConverter = Settings.AppSettingsInterimConverter.NAME)
    private final Settings settings;
    @JsonProperty(interimConverter = Symbols.AppSymbolsInterimConverter.NAME)
    private final Symbols syms;
    
    @JsonProperty
    private final EvaluationException fail;
    @JsonDefault("fail")
    private static final EvaluationException FAIL_DEFAULT = null;
    
    private transient volatile Build build;
    
    private static final class Build {
        final Node           root;
        final Symbols.ModRef modRef;
        final MathEnv        mathEnv;
        
        Build(Node root, Symbols.ModRef modRef, MathEnv mathEnv) {
            this.root    = root;
            this.modRef  = modRef;
            this.mathEnv = mathEnv;
        }
        
        @Override
        public String toString() {
            return "Build(" + root + ")";
        }
    }
    
    private Build build() {
        return build(false);
    }
    
    private Build build(boolean buildIfNotBuilt) {
        Build build = this.build;
        if (build == null) {
            if (buildIfNotBuilt) {
                return rebuild(createContext());
            }
            throw Errors.newIllegalState(this);
        } else {
            return build;
        }
    }
    
    private Tree(String source, Settings settings, Symbols syms, Build build) {
        Log.constructing(Tree.class);
        
        this.source   = Objects.requireNonNull(source, "source");
        this.syms     = Objects.requireNonNull(syms,   "syms");
        this.settings = settings.copyOf(Setting.Category.EVAL);
        this.build    = build; // may be null
        this.fail     = null;
        if (build != null) {
            build.root.setTree(this); // Note: may throw IllegalStateException
        }
    }
    
    @JsonConstructor
    private Tree(String source, Settings settings, Symbols syms, EvaluationException fail) {
        this.source   = Objects.requireNonNull(source,   "source");
        this.settings = Objects.requireNonNull(settings, "settings");
        this.syms     = Objects.requireNonNull(syms,     "syms");
        this.fail     = fail;
    }
    
    public Context createContext() {
        String   source   = this.source;
        Settings settings = this.settings;
        MathEnv  mathEnv  = MathEnv.instance(settings.get(Setting.MATH_ENV_CLASS));
        Symbols  syms     = this.syms;
        return new Context(source, settings, mathEnv, syms);
    }
    
    private static Tree create(Context context, String source, Symbols symbols, boolean isStatic) {
        Tree tree = new Tree(source, context.getSettings(), isStatic ? symbols.copy() : symbols, (Build) null);
        tree.rebuildIfDated(context); // always
        return tree;
    }
    
    public static Tree create(Context context) {
        Objects.requireNonNull(context, "context");
        return Tree.create(context, context.getSource(), context.getSymbols(), false);
    }
    
    public static Tree fromSource(String source, boolean isStatic) {
        Objects.requireNonNull(source, "source");
        App app = app();
        
        try (Settings sets = app.getSettings().block();
             Symbols  syms = app.getSymbols().block()) {
            try {
                Context context = new Context(source, app);
                Node    root    = TreeBuilder.buildTree(context);
                
                Settings settings = context.getSettings();
                Symbols  symbols  = context.getSymbols();
                
                if (isStatic) {
                    symbols = Symbols.Concurrent.of(root);
                }
                
                Build build = new Build(root, symbols.getModificationRef(), context.getMathEnvironment());
                return new Tree(source, settings, symbols, build);
                
            } catch (EvaluationException x) {
                if (isStatic) {
                    return new Tree(source, sets, Symbols.empty(), x);
                }
                return new Tree(source, sets, syms, (Build) null);
            }
        }
    }
    
    public static Tree fromNode(Context context, Node node, boolean isStatic) {
        return Tree.fromNode(context, node, (Node) null, isStatic);
    }
    
    public static Tree fromNode(Context context, Node node, Node copy, boolean isStatic) {
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(node,    "node");
        
        if (copy == null || node == copy) {
            copy = node.copy();
        } else {
            assert copy.isIndependant() : copy;
        }
        
        String  source  = copy.getExpressionSource().toString();
        Symbols symbols = context.getSymbols();
        MathEnv mathEnv = context.getMathEnvironment();
        
        Tree  tree  = node.getTree();
        Build build = tree.build;
        
        if ((tree.syms != symbols) || (build.mathEnv != mathEnv)) {
            // Parse a brand new tree.
            // TODO: Does this really make sense?
            Log.note(Tree.class, "fromNode", "parsing a brand new tree for ", source);
            return Tree.create(context, source, symbols, isStatic);
        }
        
        if (isStatic) {
            symbols = Symbols.Concurrent.of(copy);
            build   = new Build(copy, symbols.getModificationRef(), mathEnv);
        } else {
            build   = new Build(copy, build.modRef, mathEnv);
        }
        
        return new Tree(source, context.getSettings(), symbols, build);
    }
    
    public String getSource() {
        return source;
    }
    
    public Node getRoot() {
        return getRoot(false);
    }
    
    public Node getRoot(boolean buildIfNotBuilt) {
        return build(buildIfNotBuilt).root;
    }
    
    public Node getRootIfAvailable() {
        try {
            return getRoot(true);
        } catch (EvaluationException x) {
            Log.caught(getClass(), "getRootIfAvailable", x, true);
            return null;
        }
    }
    
    public void forEach(Consumer<? super Node> action) {
        build().root.forEach(action);
    }
    
    // If the symbol store has changed, we need
    // to rebuild the tree. Nodes in this tree may
    // have different referants.
    private Pair<Build, Boolean> rebuildIfDated(Context evaluationContext) {
        Build build = this.build;
        
        try (Symbols syms = this.syms.block()) {
            MathEnv mathEnv = evaluationContext.getMathEnvironment();
            
            String why;
            do {
                if (build == null) {
                    why = "build == null";
                    break;
                }
                if (syms.modifiedSince(build.modRef)) {
                    why = "syms was modified";
                    break;
                }
                if (build.mathEnv != mathEnv) {
                    why = "build.mathEnv != mathEnv";
                    break;
                }
                return Pair.of(build, false);
            } while (false);
            
            Log.low(Tree.class, "rebuildIfDated", "rebuilding \"", source, "\" because ", why);
            
            Context context = new Context(source, settings, mathEnv, syms);
            return Pair.of(rebuild(context), true);
        }
    }
    
    private Build rebuild(Context context) {
        Log.entering(Tree.class, "rebuild");
        
        if (fail != null) {
            throw new EvaluationException(fail);
        }
        
        try (Symbols syms = this.syms.block()) {
            Node root = TreeBuilder.buildTree(context);
            root.setTree(this);
            
            Build build = new Build(root, syms.getModificationRef(), context.getMathEnvironment());
            this.build = build;
            return build;
        }
    }
    
//    public Operand evaluate(Context context) {
//        return evaluate(context, (Operand) null);
//    }
//    
//    public Operand evaluate(Context context, Operand cachedResult) {
//        Log.entering(Tree.class, "evaluate(Context, Operand)");
//        Objects.requireNonNull(context, "context");
//        Symbols syms = this.syms;
//        
//        if (context.getSymbols() != syms) {
//            Log.low(Tree.class, "evaluate", "using local symbols for ", this);
//            context = context.derive(syms);
//        }
//        
//        try (Symbols syms0 = syms.block()) {
//            Pair<Build, Boolean> build = rebuildIfDated(context);
//            
//            if (build.right || (cachedResult == null)) {
//                return build.left.root.evaluate(context);
//            } else {
//                return cachedResult;
//            }
//        }
//    }
    
    public Operand evaluate(Context context) {
        return this.useTree(false, context, Node.EVALUATE).get();
    }
    
    public Optional<Operand> evaluateIfRebuilt(Context context) {
        return this.useTree(true, context, Node.EVALUATE);
    }
    
    public <T> Optional<T> ifRebuilt(Context context, BiFunction<? super Node, ? super Context, ? extends T> fn) {
        return this.useTree(true, context, fn);
    }
    
    public void useTree(Context callingContext, BiConsumer<? super Node, ? super Context> action) {
        this.useTree(false, callingContext, action);
    }
    
    public void useTree(boolean onlyIfRebuilt, Context callingContext, BiConsumer<? super Node, ? super Context> action) {
        this.useTree(onlyIfRebuilt, callingContext, (root, treeContext) -> {
            action.accept(root, treeContext);
            return null;
        });
    }
    
    public <T> T useTree(Context callingContext, BiFunction<? super Node, ? super Context, ? extends T> fn) {
        return this.useTree(false, callingContext, fn).get();
    }
    
    public <T> Optional<T> useTree(boolean onlyIfRebuilt, Context context, BiFunction<? super Node, ? super Context, ? extends T> fn) {
        Log.entering(Tree.class, "Optional<T> useTree(boolean, Context, BiFunction)");
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(fn,      "fn");
        
        Symbols syms = this.syms;
        
        // Note:
        // we are really checking if this tree
        // is static here. this test does
        // accomplish that, but it's probably
        // wrong if there's ever another scenario
        // where this.syms is something other than
        // the App symbols. (can that ever happen?
        // perhaps with local functions?)
        if (context.getSymbols() != syms) {
            Log.low(Tree.class, "evaluate", "using local symbols for ", this);
            context = context.derive(syms);
        }
        
        try (Symbols syms0 = syms.block()) {
            Pair<Build, Boolean> build = this.rebuildIfDated(context);
            
            if (!onlyIfRebuilt || build.right) {
                return Optional.ofNullable(fn.apply(build.left.root, context));
            } else {
                return Optional.empty();
            }
        }
    }
    
    public String toEchoString(Context context) {
        return build().root.toEchoString(new StringBuilder(), context).toString();
    }
    
    public Block toBlock(Context context) {
        return build().root.toBlock(context);
    }
    
    @Override
    public String toString() {
        return "Tree(" + source + ")";
    }
}