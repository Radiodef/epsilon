/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.math.*;
import eps.math.Number;
//import eps.math.Integer;

public enum PostComputation {
    IDENTITY(null) {
        @Override
        public Operand apply(Operand result, Context context) {
            return result;
        }
    },
    PRIME_FACTORS("prime factors of") {
        @Override
        public Operand apply(Operand result, Context context) {
            if (result.isNumber()) {
                Number  num = (Number) result;
                MathEnv env = context.getMathEnvironment();
                
                return num.abs(env).toIntegerInexact(env).factor(env);
            }
            throw UnsupportedException.operandType(result, "prime factorization");
        }
    };
    
    private final String prefix;
    
    private PostComputation(String prefix) {
        this.prefix = prefix;
    }
    
    public String prefix() {
        return prefix;
    }
    
    public abstract Operand apply(Operand result, Context context);
    
    public static PostComputation replnull(PostComputation post) {
        return post == null ? PostComputation.IDENTITY : post;
    }
}