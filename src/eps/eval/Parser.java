/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.job.*;
import eps.util.*;
import eps.math.Number;
import eps.eval.UnaryOp.Affix;
import static eps.util.Literals.*;
import static eps.util.Misc.*;
import static eps.math.Math2.*;

import java.util.*;
import java.util.concurrent.*;
import static java.lang.Character.*;

// TODO: * eliminate more thrown exceptions when requiresResolution is false.
//       * fix issue with defining symbols like e.g. arc_tangent_2 ?
//         sort of conflicts with the empty multiply (is x2 a variable 'x2' or
//         a variable 'x' multiplied by '2'?)
//       * join non-boundary chars with _, for example #_1 should always be
//         one token #1, never two tokens # and 1.
public final class Parser {
    private final Context       context;
    private final boolean       requiresResolution;
    private final IntSet        boundaries;
    private final boolean       emptyMultiply;
    private       List<Token>   tokens;
    private       IntMap<Token> deresolvedTokens;
    
    Parser(Context context) {
        this(context, true);
    }
    
    Parser(Context context, boolean requiresResolution) {
        Log.constructing(Parser.class);
        this.context       = Objects.requireNonNull(context, "context");
        this.boundaries    = context.getSetting(Setting.BOUNDARY_CHARS_AS_INT);
        this.emptyMultiply = context.getSetting(Setting.EMPTY_MULTIPLY);
        
        this.requiresResolution = requiresResolution;
    }
    
    public static List<Token> tokenize(Context context) {
        return new Parser(context, false).parse();
    }
    
    List<Token> parse() {
        this.tokens           = null;
        this.deresolvedTokens = null;
        try {
            return this.parseImpl();
        } finally {
            this.tokens           = null;
            this.deresolvedTokens = null;
        }
    }
    
    private List<Token> parseImpl() {
        List<Substring> initialSplit = splitOnBoundaries();
        List<Token>     tokens       = toTokens(initialSplit);
        
        if (tokens.isEmpty()) {
            if (this.requiresResolution) {
                throw new EvaluationException("empty expression; nothing to parse");
            } else {
                return tokens;
            }
        }
        
        initialSplit.clear();
        @SuppressWarnings("unchecked")
        List<Token> temp = (List<Token>) (List<?>) initialSplit; // new ArrayList<>(tokens.size());
        initialSplit     = null;
        preprocessSpanOperators(tokens, temp);
        
        Log.low(Parser.class, "parseImpl", () -> j(tokens, ", ", "initial tokens = [", "]", Token::toDebugString));
        
        this.tokens           = tokens;
        this.deresolvedTokens = new IntHashMap<>(tokens.size());
        
        List<Token> unresolved = new ArrayList<>(0);
        
        EvaluationException err = null;
        try {
            boolean ambiguous = true;
            
            for (;;) {
                Worker.executing();
                
                int disambiguatedCount = 0;
                if (ambiguous) {
                    disambiguatedCount = resolveAmbiguousTokens(temp, false);
                    
                    if (temp.isEmpty()) {
                        ambiguous = false;
                    } else {
                        unresolved.addAll(temp);
                    }
                }
                if (disambiguatedCount > 0) {
                    buildStringLiterals(tokens, temp);
                }
                
                if (this.emptyMultiply) {
                    insertEmptyMultiplies(tokens, true);
                }
                
                int deresCode = deresolveErroneousTokens(temp, false);
                if ((deresCode == DERES_2) && this.requiresResolution) {
                    break;
                }
                
                // If we don't require resolution, then we'll attempt
                // to classify the deresolved tokens once more before
                // returning.
                int classifiedCount = classifyUnresolvedTokens(temp);
                unresolved.addAll(temp);
                
                if ((deresCode == DERES_2) || ((classifiedCount == 0) && (disambiguatedCount == 0))) {
                    break;
                }
                
                Log.low(Parser.class, "parseImpl", () -> j(tokens, ", ", "tokens = [", "]", Token::toDebugString));
            }
            
            if (unresolved.isEmpty()) {
                deresolveErroneousTokens(temp, true);
                classifyUnresolvedTokens(temp);
            }
            
            if (!unresolved.isEmpty() && this.requiresResolution) {
                unresolved.sort(Comparator.comparingInt(tokens::indexOf));
                String        message = j(new LinkedHashSet<>(unresolved), ", ", "unresolved token(s) ", "", Token::getSource);
                EvaluationException x = new EvaluationException(message);
                
                for (Token u : unresolved) {
                    // TODO: maybe the exception should be created at the point
                    //       where the token is deresolved so we have a useful
                    //       stack trace.
                    if (u instanceof Token.OfUnresolved && ((Token.OfUnresolved) u).hasReason()) {
                        x.addSuppressed(EvaluationException.unresolved(u));
                    }
                }
                
                if (err != null) {
                    x.addSuppressed(err);
                }
                
                err = x;
            }
        } catch (EvaluationException x) {
            err = x;
            
        } catch (Throwable x) {
            if (err != null) {
                x.addSuppressed(x);
            }
            throw x;
        }
        
        if (err != null) {
            throw err;
        }
        
        linkList(tokens); // finally
        return tokens;
    }
    
    private void linkList(List<Token> tokens) {
        int size = tokens.size();
        for (int i = 0; i < size; ++i) {
            Token.Abstract t = (Token.Abstract) tokens.get(i);
            if (i > 0) {
                t.setPrev(tokens.get(i - 1));
            }
            if (i < (size - 1)) {
                t.setNext(tokens.get(i + 1));
            }
        }
    }
    
    private List<Substring> splitOnBoundaries() {
        Context         context    = this.context;
        IntSet          boundaries = this.boundaries;
        String          source     = context.getSource();
        int             length     = source.length();
        List<Substring> tokens     = new ArrayList<>(12);
        
        int start = 0;
        
        CodePoint join = context.getSettings().get(Setting.BOUNDARY_JOINER);
        
        for (int i = 0; i < length;) {
            int code = source.codePointAt(i);
            int next = i + charCount(code);
            
            if (boundaries.containsAsInt(code)) {
                if (start < i) {
                    tokens.add(Substring.snip(source, start, i));
                }
                
                Substring sub = Substring.codeAt(source, i);
                
                // join adjacent boundary characters if they
                // are infixed with the joiner (_ by default).
                // for example, *_* is definitely interpreted
                // as **, and never two separate tokens * and *.
                while (next < length) {
                    code = source.codePointAt(next);
                    if (!CodePoint.equals(join, code))
                        break;
                    int adj = next + charCount(code);
                    if (adj >= length)
                        break;
                    code = source.codePointAt(adj);
                    if (!boundaries.containsAsInt(code))
                        break;
                    sub  = sub.cat(Substring.codeAt(source, adj));
                    next = adj + charCount(code);
                }
                
                tokens.add(sub);
                start = next;
                
            } else if (isWhitespace(code)) {
                if (start < i) {
                    tokens.add(Substring.snip(source, start, i));
                }
                
                start = next;
            }
            
            i = next;
        }
        
        if (start < length) {
            tokens.add(Substring.snip(source, start, length));
        }
        
        return tokens;
    }
    
    private boolean isBoundary(Substring s) {
        int code0 = s.toString().codePointAt(0);
        if (charCount(code0) == s.length()) {
            return this.boundaries.containsAsInt(code0);
        } else {
            return false;
        }
    }
    
    /*
    @Deprecated
    private List<Token> toTokens_old(List<Substring> initialSplit) {
        int    initialSize = initialSplit.size();
        List<Token> tokens = new ArrayList<>(initialSize);
        
        Symbols symbols = context.getSymbols();
        MathEnv mathEnv = context.getMathEnvironment();
        
        for (int i = 0; i < initialSize; ++i) {
            Substring s = initialSplit.get(i);
            
            Symbols.Group group;
            
            if (isBoundary(s)) {
                group = symbols.get(s.toString());
                
                Substring sum  = s;
                int       last = i;
                
                for (int j = last + 1; j < initialSize; ++j) {
                    Substring next = initialSplit.get(j);
                    if (!isBoundary(next)) {
                        break;
                    }
                    // Don't join boundary characters which were
                    // sperated by whitespace.
                    if (sum.getSourceEnd() != next.getSourceStart()) {
                        break;
                    }
                    
                    sum = sum.cat(next);
                    
                    Symbols.Group possible = symbols.get(sum.toString());
                    
                    if (possible != null) {
                        group = possible;
                        last  = j;
                        s     = sum;
                    }
                }
                
                if (group != null) {
                    i = last;
                }
                
            } else {
                group = symbols.get(s.toString());
            }
            
            if (group != null) {
                if (group.count() == 1) {
                    Log.low(Parser.class, "toTokens", "group = ", group);
                    tokens.add(new Token.OfSymbol(s, group.get()));
                } else {
                    tokens.add(new Token.OfAmbiguous(s, group));
                }
            } else {
                Number n = NumberParser.parseDefault(mathEnv, s.toString());
                if (n != null) {
                    tokens.add(new Token.OfNumber(s, n));
                } else {
                    tokens.add(new Token.OfUnresolved(s));
                }
            }
        }
        
        return tokens;
    }
    */
    
    private List<Token> toTokens(List<Substring> split) {
        int size = split.size();
        List<Token> tokens = new ArrayList<>(size);
        
        MutableInt end = new MutableInt();
        for (int i = 0; i < size; size = split.size()) {
            toTokens(split, tokens, i, end.set(size));
            i = end.value;
        }
        
        return tokens;
    }
    
    private void toTokens(List<Substring> split, List<Token> tokens, int start, MutableInt end) {
        int end0 = end.value;
        if (start >= end0) {
            return;
        }
        
        Context   context = this.context;
        Substring sub     = split.get(start);
        Symbols   syms    = context.getSymbols();
        
        end.value = start + 1;
        
        Symbols.Group group;
        
        if (isBoundary(sub)) {
            group = syms.get(sub.toString());
            
            Substring sum  = sub;
            int       last = start;
            
            for (int i = (last + 1); i < end0; ++i) {
                Substring next = split.get(i);
                if (!isBoundary(next)) {
                    break;
                }
                // Don't join boundary characters which were
                // sperated by whitespace.
                if (sum.getSourceEnd() != next.getSourceStart()) {
                    break;
                }
                
                sum = sum.cat(next);
                
                Symbols.Group possible = syms.get(sum.toString());
                
                if (possible != null) {
                    group = possible;
                    last  = i;
                    sub   = sum;
                }
            }
            
            if (group != null) {
                end.value = last + 1;
            }
            
        } else {
            group = syms.get(sub.toString());
        }

        if (group != null) {
            if (group.count() == 1) {
                tokens.add(new Token.OfSymbol(sub, group.get()));
            } else {
                tokens.add(new Token.OfAmbiguous(sub, group));
            }
        } else {
            int length = sub.length();
            for (int i = 0; i < length;) {
                Number num = NumberParser.parseAny(context, sub, i, end);
                
                if (num == null) {
                    i += charCount(sub.codePointAt(i));
                    continue;
                }
                
                int numStart = i;
                int numEnd   = end.value;
                
                Substring numSub = sub;
                
                if (numStart == 0 && numEnd == length) {
                    end.value = start + 1;
                    tokens.add(new Token.OfNumber(sub, num));
                } else {
                    if (numStart > 0) {
                        Substring before = sub.subSequence(0, numStart);
                        split.add(start, before);
                        
                        end.value = start + 1;
                        toTokens(split, tokens, start, end);
                        start = end.value;
                    }
                    
                    numSub = sub.subSequence(numStart, numEnd);
                    split.set(start, numSub);
                    tokens.add(new Token.OfNumber(numSub, num));
                    
                    end.value = start + 1;
                }
                
                if (numEnd < length) {
                    Substring after = sub.subSequence(numEnd, length);
                    split.add(start + 1, after);
                } else {
                    Substring cat = numSub;
                    
                    for (int next = start + 1; next < end0;) {
                        int endValue = end.value;
                        
                        Substring nextSub = split.get(next);
                        if (cat.getSourceEnd() != nextSub.getSourceStart()) {
                            break;
                        }
                        
                        cat = cat.interpolate(nextSub);
                        num = NumberParser.parseAny(context, cat, 0, end);
                        
                        numEnd    = end.value;
                        end.value = endValue;
                        
                        if (num == null) {
                            break;
                        }
                        
                        int last = tokens.size() - 1;
                        
                        if (numEnd < cat.length()) {
                            split.set(next, cat.subSequence(numEnd, cat.length()));
                            
                            cat = cat.subSequence(0, numEnd);
                            
                            split.set(start, cat);
                            tokens.set(last, new Token.OfNumber(cat, num));
                            break;
                        }
                        
                        split.remove(next);
                        --end0;
                        
                        split.set(start, cat);
                        tokens.set(last, new Token.OfNumber(cat, num));
                    }
                }
                
                return;
            }
            
            // all else
            tokens.add(new Token.OfUnresolved(sub));
        }
    }
    
    private void preprocessSpanOperators(List<Token> tokens, List<Token> temp) {
        findSpanEnd(tokens, -1, null, false);
        insertEmptyOperands(tokens);
        buildStringLiterals(tokens, temp);
    }
    
    private int findSpanEnd(List<Token> tokens, int start, Token t, boolean inLiteral) {
        int    size = tokens.size();
        SpanOp left = null;
        
        if (t != null) {
            left = (SpanOp) t.getValue();
            tokens.set(start, new Token.OfSymbol(t, left, false));
        }
        
        for (int i = (start + 1); i < size; ++i) {
            Token u = tokens.get(i);
            
            if (!u.isSpanOp()) {
                continue;
            }
            
            SpanOp span = (SpanOp) u.getValue();
            
            boolean isRightMatch =
                   ( left != null )
                && ( left.getParent() == span.getParent() )
                && ( span.isSymmetric() || span.isRight() );
            
            if (isRightMatch) {
                tokens.set(i, new Token.OfSymbol(u, span, true));
                return i;
                
            } else if (!inLiteral) {
                if (!span.isSymmetric() && span.isRight()) {
                    return -1;
//                    if (this.requiresResolution) {
//                        throw EvaluationException.missingLeftSpan(span);
//                    } else {
//                        continue;
//                    }
                }
                
                int end = findSpanEnd(tokens, i, u, span.isLiteral());
                if (end < 0) {
                    tokens.set(i, new Token.OfUnresolved(u.getSource()));
                    continue;
//                    if (this.requiresResolution) {
//                        throw EvaluationException.missingRightSpan(span);
//                    } else {
//                        break;
//                    }
                }
                // For literals, we'll insert an empty string operand.
                // We could do something like this more generally if
                // other span operators should allow no operand.
                // (We do this now.)
//                if (end == (i + 1) && !span.isLiteral() && this.requiresResolution) {
//                    throw EvaluationException.emptySpan(span);
//                }
                i = end;
            }
        }
        
        return -1;
    }
    
    private void insertEmptyOperands(List<Token> tokens) {
        int size = tokens.size();
        
        for (int i = 0; i < size; ++i) {
            Token left = tokens.get(i);
            if (!left.isLeftSpan())
                continue;
            
            SpanOp span = ((SpanOp) left.getValue()).getParent();
            
            if (i == (size - 1)) {
                if (this.requiresResolution) {
                    throw EvaluationException.missingRightSpan(span);
                } else {
                    break;
                }
            }
            
            Token right = tokens.get(i + 1);
            if (!right.isRightSpan())
                continue;
            
            if (span == ((SpanOp) right.getValue()).getParent()) {
                tokens.add(i + 1, new Token.OfEmpty(left));
                // (   .   )   .
                // ^   ^   ^   ^
                // |   |   |   L (i += 2, ++i)
                // |   |   L (++i, ++i)
                // |   L (++i)
                // L (i)
                i += 2;
                ++size;
            }
        }
    }
    
    private void buildStringLiterals(List<Token> tokens, List<Token> temp) {
        int size = tokens.size();
        temp.clear();
        
        for (int i = 0; i < size; ++i) {
            Token left = tokens.get(i);
            temp.add(left);
            
            if (!left.isLeftSpan())
                continue;
            
            SpanOp span = ((SpanOp) left.getValue()).getParent();
            
            if (!span.isLiteral())
                continue;
//            Log.low(Parser.class, "buildStringLiterals", "left = ", left.toDebugString());
            
            // check if this was done on a prior pass
            if ((i + 2) < size) {
                Token next = tokens.get(i + 1);
                if (next instanceof Token.OfStringLiteral) {
                    Token right = tokens.get(i + 2);
                    if (right.isRightSpan() && span.getParent() == ((SpanOp) right.getValue()).getParent()) {
                        temp.add(tokens.get(++i));
                        temp.add(tokens.get(++i));
                        continue;
                    }
                }
            }
            
            Token     right = null;
            Substring str   = left.getSource();
            int       start = str.getSourceEnd();
            
            for (int j = (i + 1); j < size; ++j) {
                right = tokens.get(j);
                i = j;
                
                if (right.isSpanOp()) { // isRightSpan() ... ?
                    if (((SpanOp) right.getValue()).getParent() == span) {
                        int end = right.getSource().getSourceStart();
                        str     = Substring.snip(str.getFullSource(), start, end);
                        break;
                    }
                } // else
                right = null;
            }
            
            if (right == null) {
                if (this.requiresResolution) {
                    throw EvaluationException.missingRightSpan(span);
                } else {
                    Token last = tokens.get(size - 1);
                    if (last != left) {
                        int end = last.getSource().getSourceEnd();
                        str     = Substring.snip(str.getFullSource(), start, end);
                        temp.add(new Token.OfStringLiteral(str));
                    }
                }
            } else {
//                Log.low(Parser.class, "buildStringLiterals", "right = ", right.toDebugString());
                temp.add(new Token.OfStringLiteral(str));
                temp.add(right);
            }
        }
        
        tokens.clear();
        tokens.addAll(temp);
    }
    
    private void insertEmptyMultiplies(List<Token> tokens, boolean strict) {
        for (int i = 0; i < (tokens.size() - 1); ++i) {
            Token lhs = tokens.get(i);
            if (!lhs.isOperand(strict) && !lhs.isSpanOp(strict) && !lhs.isUnaryOp(strict))
                continue;
            Token rhs = tokens.get(i + 1);
            if (!rhs.isOperand(strict) && !rhs.isSpanOp(strict) && !rhs.isUnaryOp(strict))
                continue;
            
            boolean insertEmptyMultiply =
                // Note: this changes the behavior of parsing ambiguous expressions like f(x) when strict is false.
                //       maybe some of these should always be strict.
                //                                                                              vvvvvvvvvvvvvvvvvvvvvvvvvvvv
                  lhs.isOperand(strict)     ? ( rhs.isOperand(strict) || rhs.isLeftSpan(strict) || rhs.isUnaryPrefix(strict) )
                : lhs.isRightSpan(strict)   ? ( rhs.isLeftSpan(strict) || rhs.isOperand(strict) )
                : lhs.isUnarySuffix(strict) ? ( rhs.isLeftSpan(strict) || rhs.isOperand(strict) )
                : false;
            
            if (insertEmptyMultiply) {
                int       end = lhs.getSource().getSourceEnd();
                Substring sub = Substring.snip(this.context.getSource(), end, end);
                tokens.add(i + 1, new Token.OfSymbol(sub, BinaryOp.EMPTY_MULTIPLY));
                ++i;
            }
        }
    }
    
    private int resolveAmbiguousTokens(List<Token> stillAmbiguous, boolean strict) {
        Log.entering(Parser.class, "resolveAmbiguousTokens");
        List<Token> tokens  = this.tokens;
        Symbols     symbols = this.context.getSymbols();
        
        int size  = tokens.size();
        int total = 0;
        int resolvedCount;
        do {
            resolvedCount = 0;
            stillAmbiguous.clear();
            
            for (int i = 0; i < size; ++i) {
                Token token = tokens.get(i);
                if (!token.isAmbiguous())
                    continue;
                
                Substring     sub   = token.getSource();
                Symbols.Group group = symbols.get(sub.toString());

                if (group != null) {
                    Token resolved = null;
                    
                    do {
                        int groupCount = group.count();
                        
                        if (groupCount <= 1) {
                            assert false : "toTokens should have done this";
                            resolved = new Token.OfSymbol(sub, group.get());
                            break;
                        }
                        
                        Operator b = (Operator) group.get(Symbol.Kind.BINARY_OP);
                        if (b == null) {
                            b = (Operator) group.get(Symbol.Kind.NARY_OP);
                        } else {
                            if (group.contains(Symbol.Kind.NARY_OP)) {
                                --groupCount;
                            }
                        }
                        if (b != null) {
                            if (--groupCount <= 0) {
                                resolved = new Token.OfSymbol(sub, b);
                                break;
                            }
                            if ((resolved = resolveAmbiguousBinaryOrNaryOp(i, b, strict)) != null) {
                                break;
                            }
                        }
                        
                        UnaryOp u = (UnaryOp) group.get(Symbol.Kind.UNARY_OP);
                        if (u != null) {
                            if (--groupCount <= 0) {
                                resolved = new Token.OfSymbol(sub, u);
                                break;
                            }
                            if ((resolved = resolveAmbiguousUnaryOp(i, u, strict)) != null) {
                                break;
                            }
                        }
                        
                        SpanOp s = (SpanOp) group.get(Symbol.Kind.SPAN_OP);
                        if (s != null) {
                            if (--groupCount <= 0) {
                                // TODO: is it a left or right span??
//                                resolved = new Token.OfSymbol(sub, s);
//                                break;
                            }
                            if ((resolved = resolveAmbiguousSpanOp(i, s, strict)) != null) {
                                break;
                            }
                        }
                        
                        Variable v = (Variable) group.get(Symbol.Kind.VARIABLE);
                        if (v != null) {
                            if (--groupCount <= 0) {
                                resolved = new Token.OfSymbol(sub, v);
                                break;
                            }
                            if ((resolved = resolveAmbiguousVariable(i, v, strict)) != null) {
                                break;
                            }
                        }
                        
                    } while (false);
                    
                    if (resolved != null) {
                        Log.low(Parser.class, "resolveAmbiguousTokens",
                                "resolved ambiguous token ", token, " to ", ((Symbol) resolved.getValue()).getSymbolKind());
                        tokens.set(i, resolved);
                        ++resolvedCount;
                        continue;
                    }
                }
                
                stillAmbiguous.add(token);
            }
            total += resolvedCount;
        } while (resolvedCount != 0);
        
        return total;
    }
    
    private Token resolveAmbiguousBinaryOrNaryOp(int index, Operator op, boolean strict) {
        List<Token> tokens = this.tokens;
        Token       amb    = tokens.get(index);
        
        Token lhs = null;
        Token rhs = null;
        
        for (int left = index - 1; left >= 0; --left) {
            Token t = tokens.get(left);
            
            if (t.isOperand(strict) || t.isRightSpan(strict)) {
                lhs = t;
                break;
            } // else
            
            if (!(t.isOperator(strict) && t.isUnarySuffix(strict))) {
                break;
            } // else
        }
        
        int size = tokens.size();
        for (int right = index + 1; right < size; ++right) {
            Token t = tokens.get(right);
            
            if (t.isOperand(strict) || t.isLeftSpan(strict)) {
                rhs = t;
                break;
            } // else
            
            if (!(t.isOperator(strict) && t.isUnaryPrefix(strict))) {
                break;
            } // else
        }
        
        return (lhs == null || rhs == null) ? null : new Token.OfSymbol(amb.getSource(), op);
    }
    
    private Token resolveAmbiguousUnaryOp(int index, UnaryOp op, boolean strict) {
        List<Token> tokens = this.tokens;
        
        Token ambiguous = tokens.get(index);
        Token operand   = null;
        
        Affix affix     = op.getAffix();
        int   increment = affix.isPrefix() ? +1 : -1;
        int   end       = affix.isPrefix() ? tokens.size() : -1;
        
        for (int i = index + increment; i != end; i += increment) {
            Token possible = tokens.get(i);
            
            if (possible.isOperand(strict) || (affix.isPrefix() ? possible.isLeftSpan(strict)  : possible.isRightSpan(strict))) {
                operand = possible;
                break;
            } // else
            
            if (!(possible.isUnaryOp(strict) && (possible.isUnaryPrefix(strict) == affix.isPrefix()))) {
                break;
            } // else
        }
        
        return (operand == null) ? null : new Token.OfSymbol(ambiguous.getSource(), op);
    }
    
    private Token resolveAmbiguousSpanOp(int index, SpanOp op, boolean strict) {
        List<Token> tokens = this.tokens;
        Token       amb    = tokens.get(index);
        
        boolean isLeft = amb.isLeftSpan(strict);
        if (!isLeft) {
            return null;
        }
        
        for (int inc = -1; inc <= +1; inc += 2) {
            if (!isValidAdjacentToSpan(true, index, inc, strict))
                return null;
        }
        
        int end = findSpanEnd(tokens, index, strict);
        if (end >= 0) {
            for (int inc = -1; inc <= +1; inc += 2) {
                if (!isValidAdjacentToSpan(false, end, inc, strict))
                    return null;
            }
            Token.OfSymbol res = new Token.OfSymbol(tokens.get(end).getSource(), op, true);
            tokens.set(end, res);
            return new Token.OfSymbol(amb.getSource(), op, false);
        }
        
//        int size = tokens.size();
//        forEachToken:
//        for (int i = index + 1; i < size; ++i) {
//            Token next = tokens.get(i);
//            if (!next.isAmbiguous())
//                continue;
//            if (!next.isSpanOp(strict))
//                continue;
//            if (!next.isRightSpan(strict))
//                continue;
//            if (op != ((Token.OfAmbiguous) next).getSymbol(Symbol.Kind.SPAN_OP))
//                continue;
//            for (int inc = -1; inc <= +1; inc += 2) {
//                if (!isValidAdjacentToSpan(false, i, inc, strict))
//                    continue forEachToken;
//            }
//            Token.OfSymbol res = new Token.OfSymbol(next.getSource(), op, true);
//            tokens.set(i, res);
//            return new Token.OfSymbol(amb.getSource(), op, false);
//        }
        
        return null;
    }
    
    private int findSpanEnd(List<Token> tokens, int start, boolean strict) {
        int size = tokens.size();
        
        Token  t  = tokens.get(start);
        SpanOp ts = null;
        if (t.isAmbiguous()) {
            ts = (SpanOp) ((Token.OfAmbiguous) t).getSymbol(Symbol.Kind.SPAN_OP);
        } else if (t.isSpanOp()) {
            ts = (SpanOp) t.getValue();
        }
        
        if (ts == null || !t.isLeftSpan(strict)) {
            return -1;
        }
        
        for (int i = start + 1; i < size; ++i) {
            Token  u  = tokens.get(i);
            SpanOp us = null;
            if (t.isAmbiguous()) {
                if (u.isAmbiguous()) {
                    us = (SpanOp) ((Token.OfAmbiguous) u).getSymbol(Symbol.Kind.SPAN_OP);
                }
            } else if (t.isSpanOp()) {
                if (u.isSpanOp()) {
                    us = (SpanOp) u.getValue();
                }
            }
            if (us == null)
                continue;
            if (ts.getParent() == us.getParent()) {
                if (ts.isSymmetric() || u.isRightSpan(strict))
                    return i;
                int end = findSpanEnd(tokens, i, strict);
                if (end < 0)
                    return -1;
                i = end;
            } else {
                if (u.isRightSpan(strict))
                    return -1;
                int end = findSpanEnd(tokens, i, strict);
                if (end < 0)
                    return -1;
                i = end;
            }
        }
        
        return -1;
    }
    
    private boolean isValidAdjacentToSpan(boolean isLeft, int index, int inc, boolean strict) {
        assert inc == -1 || inc == +1 : inc;
        Token adj = weakGet(this.tokens, index + inc);
        
        if (adj == null) {
            return isLeft ? inc < 0 : inc > 0;
        }
        if (adj.isOperand(strict)) {
            return isLeft ? inc > 0 : inc < 0;
        }
        
        if (adj.isOperator(strict)) {
            if (adj.isUnaryPrefix(strict)) {
                return isLeft;
            }
            if (adj.isUnarySuffix(strict)) {
                return !isLeft;
            }
            
            if (adj.isSpanOp(strict)) {
                return isLeft ? adj.isLeftSpan(strict) : adj.isRightSpan(strict);
            }
        }
        
        return true;
    }
    
    private Token resolveAmbiguousVariable(int index, Variable var, boolean strict) {
        List<Token> tokens = this.tokens;
        Token       amb    = tokens.get(index);
        
        // Note: variables can now be adjacent due to empty multiply.
        if (!this.emptyMultiply) {
            for (int i = index - 1; i <= (index + 1); i += 2) {
                Token possible = weakGet(tokens, i);
                
                if (possible != null) {
                    if (possible.isOperand(strict)) {
                        return null;
                    }
                }
            }
        }
        
        return new Token.OfSymbol(amb.getSource(), var);
    }
    
    private static final int RES_OK  = 0;
    private static final int DERES   = 1;
    private static final int DERES_2 = 2;
    
    private int deresolveErroneousTokens(List<Token> deresolved, boolean strict) {
        Log.entering(Parser.class, "deresolveErroneousTokens");
        List<Token> tokens = this.tokens;
        
        int allResult = RES_OK;
        
        int size = tokens.size();
        int deresolvedCount;
        do {
            deresolvedCount = 0;
            deresolved.clear();
            
            for (int i = 0; i < size; ++i) {
                Token t = tokens.get(i);
                
                int code;
                switch (code = deresolveErroneousToken(i, strict)) {
                    case RES_OK:
                        break;
                    case DERES:
                    case DERES_2:
                        if (allResult != DERES_2) {
                            allResult = code;
                        }
                        
                        ++deresolvedCount;
                        deresolved.add(tokens.get(i));
                        
                        Object type = t.getTokenKind().isSymbol() ? ((Symbol) t.getValue()).getSymbolKind()
                                    : t.getTokenKind().isNumber() ? ((Number) t.getValue()).getNumberKind() : "???";
                        Log.low(Parser.class, "deresolveErroneousTokens",
                                "deresolved ", t, " from ",
                                type);
                }
            }
            // If we've found a twice-deresolved token, we don't
            // recursively deresolve all the tokens near it. We
            // just return.
        } while ((allResult != DERES_2) && (deresolvedCount != 0));
        
        return allResult;
    }
    
    private int deresolveErroneousToken(int index, boolean strict) {
        Token t = this.tokens.get(index);
        
        int code;
        
        if (t.isOperator()) {
            if (t.isUnaryOp()) {
                if ((code = requireAdjacentOperand(index, t.isUnaryPrefix() ? +1 : -1, strict)) != RES_OK)
                    return code;
                if ((code = requireNoAdjacentOperand(index, t.isUnaryPrefix() ? -1 : +1, strict)) != RES_OK)
                    return code;
                
            } else if (t.isBinaryOp() || t.isNaryOp()) {
                if ((code = requireAdjacentOperand(index, -1, strict)) != RES_OK)
                    return code;
                if ((code = requireAdjacentOperand(index, +1, strict)) != RES_OK)
                    return code;
                
            } else if (t.isSpanOp()) {
                if ((code = requireNoAdjacentSpan(index, strict)) != RES_OK)
                    return code;
                
            } else {
                throw EvaluationException.unexpected(t);
            }
        } else if (t.isOperand()) {
            // Note: variables may now be adjacent due to empty multiply.
            if ((code = requireNoAdjacentOperand(index, -1, strict)) != RES_OK)
                return code;
            if ((code = requireNoAdjacentOperand(index, +1, strict)) != RES_OK)
                return code;
            
        } else if (t.isAmbiguous()) {
            if (strict) {
                throw new EvaluationException("ambiguous token " + t.getSource());
            }
        } else if (!t.isUnresolved()) {
            throw EvaluationException.unexpected(t);
        }
        
        return RES_OK;
    }
    
    private int requireAdjacentOperand(int index, int increment, boolean strict) {
        assert increment == -1 || increment == +1 : increment;
        List<Token> tokens = this.tokens;
        Token       t      = tokens.get(index);
        Operator    op     = (Operator) t.getValue();
        
        if (inRange(index + increment, 0, tokens.size())) {
            Token operand = tokens.get(index + increment);
            
            if (operand.isOperand(strict)) {
                return RES_OK;
            }
            
            if (operand.isOperator(strict)) {
                if (operand.isSpanOp(strict)) {
                    if (operand.isLeftSpan(strict) == (increment > 0)) {
                        return RES_OK;
                    }
                }
                
                if (operand.isUnaryOp(strict) && (operand.isUnaryPrefix(strict) == (increment > 0))) {
                    return RES_OK;
                }
            }
            
            if (!strict && operand.isUnresolved()) {
                return RES_OK;
            }
        }
        
        Log.low(Parser.class, "requireAdjacentOperand", "deresolving ", t);
        Token u = new Token.OfUnresolved(t, "missing operand for " + op.getKindDescription() + " " + op.getName());
        t       = this.deresolvedTokens.putAsInt(index, u);
        
        if (t != null) {
            return DERES_2;
//            throw EvaluationException.unresolved(t);
        }
        
        tokens.set(index, u);
        return DERES;
    }
    
    private static String badAdjacent(Token a, int indexA, Token b, int indexB) {
        if (indexA > indexB) {
            Token c = a;
            a = b;
            b = c;
        }
        return f("unable to resolve adjacent tokens %s and %s", a.getSource(), b.getSource());
    }
    
    private int requireNoAdjacentOperand(int index, int increment, boolean strict) {
        assert increment == -1 || increment == +1 : increment;
        List<Token> tokens = this.tokens;
        
        if (!inRange(index + increment, 0, tokens.size())) {
            return RES_OK;
        }
        
        Token adj = tokens.get(index + increment);
        
        if (!adj.isOperand(strict) || adj.isOperator(strict)) {
            
            if (adj.isOperator(strict)) {
                if (adj.isSpanOp(strict)) {
                    if (adj.isLeftSpan(strict) == (increment < 0)) {
                        return RES_OK;
                    }
                } else {
                    if (!adj.isUnaryOp(strict) || (adj.isUnaryPrefix(strict) == (increment < 0))) {
                        return RES_OK;
                    }
                }
            }
            
            if (!strict && adj.isUnresolved()) {
                return RES_OK;
            }
        }
        
        Token t = tokens.get(index);
        
        String message = badAdjacent(t, index, adj, index + increment);
        
        // do not reclassify numbers
        if (t.getTokenKind().isNumber()) {
            if (this.requiresResolution) {
                throw new EvaluationException(message);
            } else {
                // Maybe we should always do this.
                return RES_OK;
            }
        }
        
        Log.low(Parser.class, "requireNoAdjacentOperand", "deresolving ", t);
        Token u = new Token.OfUnresolved(t, message);
        t       = this.deresolvedTokens.putAsInt(index, u);
        
        if (t != null) {
            return DERES_2;
//            throw EvaluationException.unresolved(t);
        }
        
        tokens.set(index, u);
        return DERES;
    }
    
    private int requireNoAdjacentSpan(int index, boolean strict) {
        List<Token> tokens = this.tokens;
        
        Token span = tokens.get(index);
        assert span.isSpanOp() : index + " in " + tokens;
        
        for (int increment = -1; increment <= +1; increment += 2) {
            if (!inRange(index + increment, 0, tokens.size())) {
                continue;
            }
            
            Token adj = tokens.get(index + increment);
            if (!adj.isSpanOp(strict)) {
                continue;
            }
            
            // )) or ((
            if (adj.isLeftSpan(strict) == span.isLeftSpan(strict)) {
                continue;
            }
            
            // These are OK:
            // -increment => [ ] and +increment => [ ]
            //                 ^                   ^
            if (increment < 0 ? span.isRightSpan(strict) : span.isLeftSpan(strict)) {
                continue;
            }
            
            Token left  = span.isLeftSpan(strict)  ? span : adj;
            Token right = span.isRightSpan(strict) ? span : adj;
            
            Log.low(Parser.class, "requireNoAdjacentSpan", "deresolving ", span, " and ", adj);
            String msg = f("found adjacent spans %s%s", left.getSource(), right.getSource());
            
            if (!strict) {
                boolean spanIsParenthesis =
                    !span.isAmbiguous() && span.isSpanOp() && ((SpanOp) span.getValue()).isParenthesis();
                boolean adjIsParenthesis =
                    !adj.isAmbiguous() && adj.isSpanOp() && ((SpanOp) adj.getValue()).isParenthesis();
                
                if (spanIsParenthesis != adjIsParenthesis) {
                    if (spanIsParenthesis) {
                        return RES_OK;
                    }
                    
                    Token u = new Token.OfUnresolved(span, msg);
                    Token p = this.deresolvedTokens.putAsInt(index, u);
                    
                    return (p == null) ? DERES : DERES_2;
                }
            }
            
            // otherwise deres in pairs
            Token u1 = new Token.OfUnresolved(span, msg);
            Token p1 = this.deresolvedTokens.putAsInt(index, u1);
            Token u2 = new Token.OfUnresolved(adj, msg);
            Token p2 = this.deresolvedTokens.putAsInt(index + increment, u2);
            
            if (p1 != null || p2 != null) {
                return DERES_2;
            }
            
            tokens.set(index, u1);
            tokens.set(index + increment, u2);
            return DERES;
        }
        
        return RES_OK;
    }
    
    /*
    private void requireNonEmptyParenthesis(int index) {
        List<Token> tokens      = this.tokens;
        Token       parenthesis = tokens.get(index);
        
        int increment = parenthesis.isLeftParenthesis() ? +1 : -1;
        
        if (!inRange(index + increment, 0, tokens.size())) {
            // We'll catch this during tree creation.
            return;
        }
        
        Token adjacent = tokens.get(index + increment);
        if (parenthesis.isLeftParenthesis() ? adjacent.isRightParenthesis() : adjacent.isLeftParenthesis()) {
            throw new EvaluationException("empty parenthetical");
        }
    }
    */
    
    // TODO: Consider loose classification?
    private int classifyUnresolvedTokens(List<Token> stillUnclassified) {
        Log.entering(Parser.class, "classifyUnresolvedTokens");
        List<Token> tokens = this.tokens;
        
        int size  = tokens.size();
        int total = 0;
        int classifiedCount;
        do {
            classifiedCount = 0;
            stillUnclassified.clear();
            
            for (int i = 0; i < size; ++i) {
                Token t = tokens.get(i);
                if (!t.isUnresolved())
                    continue;
                Token.OfUnresolved u = (Token.OfUnresolved) t;
                //
                
                Token before   = weakGet(tokens, i - 1);
                Token after    = weakGet(tokens, i + 1);
                Token resolved = null;
                
                List<Classifier> classifiers  = Parser.CLASSIFIERS;
                int              nClassifiers = classifiers.size();
                for (int j = 0; j < nClassifiers; ++j) {
                    Classifier classifier = classifiers.get(j);
                    if ((resolved = classifier.classify(this, tokens, i, u, before, after)) != null)
                        break;
                }
                
                if (resolved != null) {
                    Log.low(Parser.class, "classifyUnresolvedTokens", "reclassified ", resolved, " to ", resolved.toDebugString());
                    tokens.set(i, resolved);
                    ++classifiedCount;
                } else {
                    stillUnclassified.add(u);
                }
            }
            total += classifiedCount;
        } while (classifiedCount != 0);
        
        return total;
    }
    
    private boolean allBoundaries(Token t) {
        IntSet bounds = this.boundaries;
        String src    = t.getSource().toString();
        int    len    = src.length();
        for (int i = 0, c; i < len; i += charCount(c)) {
            c = src.codePointAt(i);
            if (!bounds.containsAsInt(c))
                return false;
        }
        return true;
    }
    
    private boolean noBoundaries(Token t) {
        IntSet bounds = this.boundaries;
        String src    = t.getSource().toString();
        int    len    = src.length();
        for (int i = 0, c; i < len; i += charCount(c)) {
            c = src.codePointAt(i);
            if (bounds.containsAsInt(c))
                return false;
        }
        return true;
    }
    
    private interface Classifier {
        default Token classify(Parser             self,
                               List<Token>        tokens,
                               int                index,
                               Token.OfUnresolved u,
                               Token              before,
                               Token              after) {
            return this.classify(self, u, before, after);
        }
        default Token classify(Parser self, Token.OfUnresolved u, Token before, Token after) {
            return null;
        }
    }
    
    private static final class StartClassifier implements Classifier {
        @Override
        public Token classify(Parser self, Token.OfUnresolved u, Token before, Token after) {
            if (before == null && after != null) {
                boolean isUnaryPrefix =
                       ( after.isOperand() )
                    || ( after.isLeftSpan() )
                    || ( after.isUnresolved() && self.allBoundaries(u) && self.noBoundaries(after) );
                
                if (isUnaryPrefix) {
                    return new Token.OfSymbol(u, UnaryOp.createUnaryError(u, Affix.PREFIX));
                }
                
                boolean isVariable =
                       ( after.isOperator() && !after.isUnaryPrefix() )
                    || ( after.isUnresolved() && self.noBoundaries(u) && self.allBoundaries(after) );
                
                if (isVariable) {
                    // What's the significance of this check? I don't remember.
                    // It delineates between cases like x) and +)
                    isVariable = !after.isRightSpan() || self.noBoundaries(u);
                    if (isVariable) {
                        return new Token.OfSymbol(u, Variable.create(u));
                    }
                }
            }
            return null;
        }
    }
    
    private static final class EndClassifier implements Classifier {
        @Override
        public Token classify(Parser self, Token.OfUnresolved u, Token before, Token after) {
            if (before != null && after == null) {
                boolean isUnarySuffix =
                       ( before.isOperand() )
                    || ( before.isRightSpan() )
                    || ( before.isUnresolved() && self.noBoundaries(before) && self.allBoundaries(u) );
                
                if (isUnarySuffix) {
                    return new Token.OfSymbol(u, UnaryOp.createUnaryError(u, Affix.SUFFIX));
                }
                
                boolean isVariable =
                       ( before.isOperator() && !before.isUnarySuffix() )
                    || ( before.isUnresolved() && self.allBoundaries(before) && self.noBoundaries(u) );
                
                if (isVariable) {
                    isVariable = !before.isLeftSpan() || self.noBoundaries(u);
                    if (isVariable) {
                        return new Token.OfSymbol(u, Variable.create(u));
                    }
                }
            }
            return null;
        }
    }
    
    private static final class VariableClassifier implements Classifier {
        @Override
        public Token classify(Parser self, Token.OfUnresolved u, Token before, Token after) {
            if (before == null && after == null) {
                return new Token.OfSymbol(u, Variable.create(u));
            }
            if (before == null || after == null) {
                return null;
            }
            
            boolean beforeIsLeftOfVariable =
                   ( before.isLeftSpan() )
                || ( before.isOperator() && !before.isUnarySuffix() && !before.isRightSpan() )
                || ( before.isUnresolved() && self.allBoundaries(before) && self.noBoundaries(u) );
            
            if (!beforeIsLeftOfVariable)
                return null;
            
            boolean afterIsRightOfVariable =
                   ( after.isRightSpan() )
                || ( after.isOperator() && !after.isUnaryPrefix() && !after.isLeftSpan() )
                || ( after.isUnresolved() && self.allBoundaries(after) && self.noBoundaries(u) );
            
            if (!afterIsRightOfVariable)
                return null;
            
            return new Token.OfSymbol(u, Variable.create(u));
        }
    }
    
    private static final class UnaryPrefixClassifier implements Classifier {
        @Override
        public Token classify(Parser self, Token.OfUnresolved u, Token before, Token after) {
            if (before == null || after == null) {
                return null;
            }
            
            boolean beforeIsLeftOfPrefix =
                   ( before.isLeftSpan() )
                || ( before.isOperator() && !before.isUnarySuffix() && !before.isRightSpan() );
//                || ( before.isUnresolved() && self.hasNoBoundaries(before) && self.isAllBoundaries(u) );
            
            if (!beforeIsLeftOfPrefix)
                return null;
            
            boolean afterIsRightOfPrefix =
                   ( after.isOperand() )
                || ( after.isLeftSpan() )
                || ( after.isUnaryPrefix() )
                || ( after.isUnresolved() && self.noBoundaries(after) && self.allBoundaries(u) );
            
            if (!afterIsRightOfPrefix)
                return null;
            
            return new Token.OfSymbol(u, UnaryOp.createUnaryError(u, Affix.PREFIX));
        }
    }
    
    private static final class UnarySuffixClassifier implements Classifier {
        @Override
        public Token classify(Parser self, Token.OfUnresolved u, Token before, Token after) {
            if (before == null || after == null) {
                return null;
            }
            
            boolean beforeIsLeftOfSuffix =
                   ( before.isOperand() )
                || ( before.isRightSpan() )
                || ( before.isUnarySuffix() )
                || ( before.isUnresolved() && self.noBoundaries(before) && self.allBoundaries(u) );
            
            if (!beforeIsLeftOfSuffix)
                return null;
            
            boolean afterIsRightOfSuffix =
                   ( after.isRightSpan() )
                || ( after.isOperator() && !after.isUnaryPrefix() && !after.isLeftSpan() );
            
            if (!afterIsRightOfSuffix)
                return null;
            
            return new Token.OfSymbol(u, UnaryOp.createUnaryError(u, Affix.SUFFIX));
        }
    }
    
    private static final class BinaryOperatorClassifier implements Classifier {
        @Override
        public Token classify(Parser self, Token.OfUnresolved u, Token before, Token after) {
            if (before == null || after == null) {
                return null;
            }
            
            boolean beforeIsLeftOfBinaryOp =
                   ( before.isRightSpan() )
                || ( before.isOperand() )
                || ( before.isUnarySuffix() )
                || ( before.isUnresolved() && self.noBoundaries(before) && self.allBoundaries(u) );
            
            if (!beforeIsLeftOfBinaryOp)
                return null;
            
            boolean afterIsRightOfBinaryOp =
                   ( after.isLeftSpan() )
                || ( after.isOperand() )
                || ( after.isUnaryPrefix() )
                || ( after.isUnresolved() && self.noBoundaries(after) && self.allBoundaries(u) );
            
            if (!afterIsRightOfBinaryOp)
                return null;
            
            return new Token.OfSymbol(u, BinaryOp.createBinaryError(u));
        }
    }
    
    private static final class SpanClassifier implements Classifier {
        @Override
        public Token classify(Parser             self,
                              List<Token>        tokens,
                              int                index,
                              Token.OfUnresolved u,
                              Token              before,
                              Token              after) {
            if (after == null)
                return null;
            
            boolean beforeIsLeftOfLeftSpan =
                   ( before == null )
                || ( before.isLeftSpan() )
                || ( before.isOperator() && !before.isUnarySuffix() && !before.isRightSpan() );
            
            if (!beforeIsLeftOfLeftSpan)
                return null;
            
            boolean afterIsRightOfLeftSpan =
                   ( after.isOperand() )
                || ( after.isUnaryPrefix() )
                || ( after.isLeftSpan() )
                || ( after.isUnresolved() && self.noBoundaries(after) && self.allBoundaries(u) );
            // TODO: allow empty span? do it here
            
            if (!afterIsRightOfLeftSpan)
                return null;
            
            boolean leftIsBoundaries = self.allBoundaries(u);
            
            // Then look for a suitable right span.
            int size = tokens.size();
            
            for (int i = (index + 2); i < size; ++i) {
                Token next = tokens.get(i);
                
                if (!next.isUnresolved())
                    continue;
                
                before = tokens.get(i - 1);
                
                boolean beforeIsLeftOfRightSpan =
                       ( before.isOperand() )
                    || ( before.isUnarySuffix() )
                    || ( before.isRightSpan() )
                    || ( before.isUnresolved() && self.noBoundaries(before) && self.allBoundaries(next) );
                
                if (!beforeIsLeftOfRightSpan)
                    continue;
                
                after = ((i + 1) < size) ? tokens.get(i + 1) : ( null );
                
                boolean afterIsRightOfRightSpan =
                       ( after == null )
                    || ( after.isOperator() && !after.isUnaryPrefix() && !after.isLeftSpan() );
                
                if (!afterIsRightOfRightSpan)
                    continue;
                
                boolean boundariesMatch =
                    leftIsBoundaries == self.allBoundaries(u);
                
                if (!boundariesMatch)
                    continue;
                
                // else classify
                SpanOp op = SpanOp.createSpanError(u, (Token.OfUnresolved) next);
                
                tokens.set(i, new Token.OfSymbol(next, op.getRight(), true));
                return new Token.OfSymbol(u, op.getLeft(), false);
            }
            
            return null;
        }
    }
    
    private static final List<Classifier> CLASSIFIERS =
        ListOf( new VariableClassifier()       ,
                new SpanClassifier()           ,
                new UnaryPrefixClassifier()    ,
                new UnarySuffixClassifier()    ,
                new BinaryOperatorClassifier() ,
                new StartClassifier()          ,
                new EndClassifier()            );
    
    // Note:
    // This is in a nested class to avoid issues with the circular
    // reference of Parser static init referring to Settings which
    // refers to the DEFAULT_BOUNDARIES list.
    // The Settings class init will load the Stuff class without
    // loading the Parser class itself.
    // DEFAULT_BOUNDARIES could go somewhere else, but it really
    // makes sense to put it in the Parser class. It could also
    // go in the Settings class, I guess.
    public static final class Stuff {
        private Stuff() {}
        
        public static final Set<CodePoint> DEFAULT_BOUNDARIES;
        
        static {
            Set<CodePoint> b = new LinkedHashSet<>();
            for (char c : new char[] {'+',
                                      '-',
                                      '±',
                                      '=',
                                      '≠',
                                      '<',
                                      '>',
                                      '≤',
                                      '≥',
                                      '*',
                                      '⋅', // multiplication dot (proper)
                                      '·', // middle dot
                                      '∙', // bullet operator
                                      '×',
                                      '÷',
                                      '/',
                                      '\\',
                                      '!',
                                      '√',
                                      '%',
                                      '^',
                                      '&',
                                      '|',
                                      '?',
                                      ':',
                                      ';',
                                      '@',
                                      '#',
                                      '$',
                                      ',',
                                      '~',
                                      '`',
                                      '\'',
                                      '"',
                                      '(',
                                      ')',
                                      '[',
                                      ']',
                                      '{',
                                      '}'}) {
                b.add(CodePoint.valueOf(c));
            }
            DEFAULT_BOUNDARIES = Collections.unmodifiableSet(b);
        }
        
        private static final Map<Set<CodePoint>, String> STRINGS_BY_CODE_POINT_SETS =
            new ConcurrentHashMap<>();
        private static final Map<String, Set<CodePoint>> CODE_POINT_SETS_BY_STRINGS =
            new ConcurrentHashMap<>();
        private static final Map<Set<CodePoint>, IntSet> INT_SETS_BY_CODE_POINT_SETS =
            new ConcurrentHashMap<>();
        private static final Map<Set<CodePoint>, Set<CodePoint>> IMMUTABLE_CODE_POINT_SETS =
            new ConcurrentHashMap<>();
        private static final Map<IntSet, IntSet> IMMUTABLE_INT_SETS =
            new ConcurrentHashMap<>();
        static {
            String compact = computeStringFromCodePointSet(DEFAULT_BOUNDARIES);
            STRINGS_BY_CODE_POINT_SETS.put(DEFAULT_BOUNDARIES, compact);
            CODE_POINT_SETS_BY_STRINGS.put(compact, DEFAULT_BOUNDARIES);
            
            IntSet ints = computeIntSetFromCodePointSet(DEFAULT_BOUNDARIES);
            INT_SETS_BY_CODE_POINT_SETS.put(DEFAULT_BOUNDARIES, ints);
            
            IMMUTABLE_CODE_POINT_SETS.put(DEFAULT_BOUNDARIES, DEFAULT_BOUNDARIES);
            IMMUTABLE_INT_SETS.put(ints, ints);
        }
        
        public static final IntSet DEFAULT_BOUNDARIES_AS_INT =
            toImmutableIntSet(toIntSet(DEFAULT_BOUNDARIES));
        
        // don't let the caches ever get too big
        private static <M extends Map<?, ?>> M flush(M cache) {
            final int max = 24;
            if (cache.size() > max) {
                cache.clear();
            }
            return cache;
        }
        
        private static String computeStringFromCodePointSet(Set<CodePoint> set) {
            StringBuilder b = new StringBuilder(set.size());
            
            for (CodePoint code : set) {
                b.appendCodePoint(code.getAsInt());
            }
            
            return b.toString();
        }
        
        public static String toString(Set<CodePoint> codePoints) {
            Objects.requireNonNull(codePoints, "codePoints");
            String str = STRINGS_BY_CODE_POINT_SETS.get(codePoints);
            
            if (str == null) {
                // We have to make sure to put an immutable set in as the key.
                str = computeStringFromCodePointSet(codePoints);
                flush(STRINGS_BY_CODE_POINT_SETS).put(toImmutableCodePointSet(codePoints), str);
            }
            
            return str;
        }
        
        private static Set<CodePoint> computeCodePointSetFromString(String str) {
            int length = str.length();
            int count  = str.codePointCount(0, length);
            Set<CodePoint> set = new LinkedHashSet<>(2 * count);
            
            for (int i = 0; i < length;) {
                int code = str.codePointAt(i);
                set.add(CodePoint.valueOf(code));
                i += charCount(code);
            }
            
            return Collections.unmodifiableSet(set);
        }
        
        public static Set<CodePoint> toCodePointSet(String str) {
            Objects.requireNonNull(str, "str");
            return flush(CODE_POINT_SETS_BY_STRINGS).computeIfAbsent(str, Stuff::computeCodePointSetFromString);
        }
        
        private static IntSet computeIntSetFromCodePointSet(Set<CodePoint> codePoints) {
            IntSet ints = new IntHashSet(codePoints.size());
            
            for (CodePoint code : codePoints) {
                ints.addAsInt(code.intValue());
            }
            
            return IntSet.unmodifiableView(ints);
        }
        
        public static IntSet toIntSet(Set<CodePoint> codePoints) {
            Objects.requireNonNull(codePoints, "codePoints");
            IntSet ints = INT_SETS_BY_CODE_POINT_SETS.get(codePoints);
            
            if (ints == null) {
                // make sure to put an immutable key
                ints = computeIntSetFromCodePointSet(codePoints);
                flush(INT_SETS_BY_CODE_POINT_SETS).put(toImmutableCodePointSet(codePoints), ints);
            }
            
            return ints;
        }
        
        private static IntSet computeImmutableIntSet(IntSet ints) {
            return IntSet.unmodifiableView(new IntHashSet(ints));
        }
        
        public static IntSet toImmutableIntSet(IntSet ints) {
            Objects.requireNonNull(ints, "ints");
            IntSet imm = IMMUTABLE_INT_SETS.get(ints);
            
            if (imm == null) {
                // make sure to put an immutable key
                imm = computeImmutableIntSet(ints);
                flush(IMMUTABLE_INT_SETS).put(imm, imm);
            }
            
            return imm;
        }
        
        private static Set<CodePoint> computeImmutableCodePointSet(Set<CodePoint> codePoints) {
            Set<CodePoint> copy = Misc.requireNonNull(new LinkedHashSet<>(codePoints), "codePoints");
            return Collections.unmodifiableSet(copy);
        }
        
        public static Set<CodePoint> toImmutableCodePointSet(Set<CodePoint> codePoints) {
            Objects.requireNonNull(codePoints, "codePoints");
            Set<CodePoint> imm = IMMUTABLE_CODE_POINT_SETS.get(codePoints);
            
            if (imm == null) {
                // make sure to put an immutable key
                imm = computeImmutableCodePointSet(codePoints);
                flush(IMMUTABLE_CODE_POINT_SETS).put(imm, imm);
            }
            
            return imm;
        }
    }
}