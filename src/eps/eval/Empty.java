/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.block.*;
import eps.util.*;

public enum Empty implements Operand {
    INSTANCE;
    
    @Override
    public Operand.Kind getOperandKind() {
        return Operand.Kind.EMPTY;
    }
    
    @Override
    public boolean isEmpty() {
        return true;
    }
    
    @Override
    public boolean isTruthy(Context context) {
        return false;
    }
    
    @Override
    public String toString() {
        return "";
    }
    
    @Override
    public String toResultString(Context c, AnswerFormatter f, MutableBoolean a) {
        return this.toString();
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context c, AnswerFormatter f, MutableBoolean a) {
        return b;
    }
    
    @Override
    public Block toResultBlock(StringBuilder temp, Context context, AnswerFormatter f, MutableBoolean a) {
        return new EmptyBlock();
    }
    
    @Override
    public Block toSourceBlock(StringBuilder temp, Context context, Node node) {
        return new EmptyBlock();
    }
}