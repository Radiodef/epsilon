/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.util.*;
import eps.json.*;
import static eps.util.Misc.*;
import static eps.app.App.*;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.lang.ref.*;

public interface Symbols extends Copyable<Symbols>, Gate {
    Symbols clear();
    boolean isEmpty();
    
    boolean put(Symbol sym);
    boolean remove(Symbol sym);
    
    default boolean putIfAbsent(Symbol sym) {
        if (overwritesAny(sym)) {
            return false;
        }
        return put(sym);
    }
    
    Group get(String name);
    
    default <S extends Symbol> S get(String name, Class<S> type) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(type, "type");
        Group group = get(name);
        if (group != null) {
            return group.get(type);
        }
        return null;
    }
    
    default Symbol get(String name, Symbol.Kind kind) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(kind, "kind");
        Group group = get(name);
        if (group != null) {
            return group.get(kind);
        }
        return null;
    }
    
    default boolean contains(Symbol sym) {
        Objects.requireNonNull(sym, "sym");
        Group group = this.get(sym.getName());
        if (group != null) {
            return group.get(sym.getSymbolKind()) == sym;
        }
        return false;
    }
    
    default boolean contains(String name, Symbol.Kind kind) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(kind, "kind");
        Group group = get(name);
        if (group != null) {
            return group.contains(kind);
        }
        return false;
    }
    
    default boolean contains(String name, Class<? extends Symbol> type) {
        Objects.requireNonNull(name, "name");
        Objects.requireNonNull(type, "type");
        Group group = get(name);
        if (group != null) {
            return group.contains(type);
        }
        return false;
    }
    
    default boolean overwritesAny(Symbol sym) {
        Objects.requireNonNull(sym, "sym");
        if (sym.isSpanOp()) {
            SpanOp span = (SpanOp) sym;
            if (!span.isSymmetric()) {
                return contains(span.getLeftName(),  Symbol.Kind.SPAN_OP)
                    || contains(span.getRightName(), Symbol.Kind.SPAN_OP);
            }
        }
        return contains(sym.getName(), sym.getSymbolKind());
    }
    
    Map<String, Group> mapView();
    
    default Stream<Symbol> stream() {
        return this.mapView().values().stream().flatMap(Group::stream);
    }
    
    default void putAll(Symbols that) {
        try (Symbols that0 = that.block()) {
            if (!that.isEmpty()) {
                for (Group g : that.mapView().values()) {
                    for (Symbol sym : g.mapView().values()) {
                        put(sym);
                    }
                }
            }
        }
    }
    
    @Override
    Symbols block();
    @Override
    Symbols unblock();
    @Override
    Symbols flush();
    @Override
    Symbols forceUnsafeFlush();
    
    ModRef getModificationRef();
    boolean modifiedSince(ModRef modRef);
    boolean isReference(ModRef modRef);
    
    static boolean isOverwriteEqual(Symbol lhs, Symbol rhs) {
        return lhs.isSameKind(rhs) && lhs.hasName(rhs);
    }
    
    static Symbols getLocalSymbols() {
        return SymbolsPrivate.getLocal();
    }
    
    static Symbols setLocalSymbols(Symbols syms) {
        return SymbolsPrivate.setLocal(syms);
    }
    
    @JsonSerializable
    final class IntrinsicSymbolSupplier implements Supplier<Symbol> {
        private final Symbol symbol;
        @JsonProperty
        private final String name;
        @JsonProperty
        private final Symbol.Kind kind;
        
        public IntrinsicSymbolSupplier(Symbol symbol) {
            this.symbol = Objects.requireNonNull(symbol, "symbol");
            if (!symbol.isIntrinsic()) {
                throw new IllegalArgumentException(symbol.toString());
            }
            this.name = symbol.getName();
            this.kind = symbol.getSymbolKind();
        }
        
        @JsonConstructor
        public IntrinsicSymbolSupplier(String name, Symbol.Kind kind) {
            Symbol  symbol = null;
        //  Symbols syms   = app().getSymbols();
            Symbols syms   = getLocalSymbols();
            Group   group  = syms.get(name);
            if (group != null) {
                symbol = group.get(kind);
            }
            this.symbol = symbol;
            this.name   = name;
            this.kind   = kind;
        //  throw new IllegalArgumentException(name + ", group = " + group);
        }
        
        @Override
        public Symbol get() {
            return symbol;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(symbol);
        }
        
        @Override
        public boolean equals(Object obj) {
            return (obj instanceof IntrinsicSymbolSupplier)
                && Objects.equals(this.symbol, (((IntrinsicSymbolSupplier) obj).symbol));
        }
        
        private static final ToString<IntrinsicSymbolSupplier> TO_STRING =
            ToString.builder(IntrinsicSymbolSupplier.class)
                    .add("symbol", sup -> sup.symbol)
                    .add("name",   sup -> sup.name)
                    .add("kind",   sup -> sup.kind).build();
        
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
    }
    
    final class AppSymbolsInterimConverter extends InterimConverter<Symbols, Symbols> {
        public static final String NAME = "eps.eval.Symbols$AppSymbolsInterimConverter";
        public AppSymbolsInterimConverter() {
            super(Symbols.class, Symbols.class);
        }
        @Override
        public Symbols toInterim(Object obj) {
            return (obj == app().getSymbols()) ? null : (Symbols) obj;
        }
        @Override
        public Symbols fromInterim(Object obj) {
            return (obj == null) ? app().getSymbols() : (Symbols) obj;
        }
    }
    
    interface ModRef {
        boolean isSameModification(ModRef that);
    }
    
    interface Group {
        int count();
        
        default boolean isEmpty() {
            return count() == 0;
        }
        
        Symbol get();
        Symbol get(Symbol.Kind kind);
        
        @SuppressWarnings("unchecked")
        default <S extends Symbol> S get(Class<S> type) {
            Symbol.Kind kind = Symbol.Kind.valueOf(type);
            Symbol      sym  = get(kind);
            if (type.isInstance(sym)) {
                return (S) sym;
            }
            return null;
        }
        
        default boolean contains(Symbol sym) {
            return get(sym.getSymbolKind()) == sym;
        }
        
        default boolean contains(Symbol.Kind kind) {
            return get(kind) != null;
        }
        
        default boolean contains(Class<? extends Symbol> type) {
            return get(type) != null;
        }
        
        Group add(Symbol sym);
        
        default Group addAll(Group that) {
            Group copy = this;
            for (Symbol sym : that.mapView().values()) {
                copy = copy.add(sym);
            }
            return copy;
        }
        
        default Group remove(Symbol sym) {
            if (contains(sym)) {
                return remove(sym.getSymbolKind());
            } else {
                return this;
            }
        }
        
        Group remove(Symbol.Kind kind);
        
        Map<Symbol.Kind, Symbol> mapView();
        
        default Stream<Symbol> stream() {
            return this.mapView().values().stream();
        }
    }
    
    abstract class AbstractGroup implements Group {
        @Override
        public int hashCode() {
            return mapView().hashCode();
        }
        @Override
        public boolean equals(Object obj) {
            return (obj instanceof Group) && mapView().equals(((Group) obj).mapView());
        }
        @Override
        public String toString() {
            return mapView().toString();
        }
    }
    
    static Empty empty() {
        return Empty.INSTANCE;
    }
    
    enum Empty implements Symbols {
        INSTANCE;
        private enum Ref implements ModRef {
            INSTANCE;
            @Override
            public boolean isSameModification(ModRef that) {
                return this == that;
            }
        }
        @Override public boolean            isReference(ModRef r)   { return r == Ref.INSTANCE; }
        @Override public boolean            modifiedSince(ModRef r) { return false; }
        @Override public ModRef             getModificationRef()    { return Ref.INSTANCE; }
        @Override public Symbols            forceUnsafeFlush()      { return this; }
        @Override public Symbols            flush()                 { return this; }
        @Override public Symbols            block()                 { return this; }
        @Override public Symbols            unblock()               { return this; }
        @Override public Map<String, Group> mapView()               { return Collections.emptyMap(); }
        @Override public Group              get(String n)           { return null; }
        @Override public boolean            remove(Symbol s)        { return false; }
        @Override public boolean            put(Symbol s)           { return false; }
        @Override public boolean            isEmpty()               { return true; }
        @Override public Symbols            clear()                 { return this; }
        @Override public Symbols            copy()                  { return this; }
        @Override public boolean            hasActionsWaiting()     { return false; }
        @Override public boolean            isBlocked()             { return false; }
    }
    
    // TODO:
    //  turns out this should probably not use ConcurrentHashMap.
    //  it should probably just use synchronization so we can perform
    //  multiple actions on the map without overlap
    final class Concurrent implements Symbols {
        private final Map<String, Group> byName = new ConcurrentHashMap<>();
        
        private final Gate.Consumer gate = new Gate.Consumer();
        
        private final AtomicReference<SimpleModRef> mod = new AtomicReference<>(new SimpleModRef(this, 0));
        
        private Concurrent() {
        }
        
        static {
            JsonObjectAdapter.put(
                Symbols.Concurrent.class,
                Symbols.Concurrent::toJsonMap,
                props -> new Symbols.Concurrent((Set<?>) props.get("symbols"))
            );
        }
        
        private Map<?, ?> toJsonMap() {
            synchronized (gate.monitor()) {
                Set<Object> set = new LinkedHashSet<>();
                for (Group group : byName.values()) {
                    for (Symbol sym : group.mapView().values()) {
                        if (sym.isIntrinsic()) {
                            set.add(new IntrinsicSymbolSupplier(sym));
                        } else {
                            if (sym.isSpanOp()) {
                                sym = ((SpanOp) sym).getParent();
                            }
                            set.add(sym);
                        }
                    }
                }
                return Collections.singletonMap("symbols", set);
            }
        }
        
        private Concurrent(Set<?> symbols) {
            synchronized (gate.monitor()) {
                for (Object obj : symbols) {
                    Symbol sym;
                    if (obj instanceof IntrinsicSymbolSupplier) {
                        sym = ((IntrinsicSymbolSupplier) obj).get();
                        if (sym == null) {
                            Log.note(Concurrent.class, "<init>", "found unknown intrinsic ", obj);
                            continue;
                        }
                    } else if (obj instanceof Symbol) {
                        sym = (Symbol) obj;
                    } else {
                        Log.note(Concurrent.class, "<init>", "found ", coals(obj, Object::getClass), ": ", obj);
                        continue;
                    }
                    if (!sym.isError()) {
                        putSymbol(sym);
                    } else {
                        Log.note(Concurrent.class, "<init>", "found error ", coals(sym, Symbol::getClass), ": ", sym);
                    }
                }
            }
        }
        
        public static Concurrent empty() {
            return new Concurrent();
        }
        
        public static Concurrent ofDefaults() {
            Concurrent c = new Concurrent();
            synchronized (c.gate.monitor()) {
                Symbol.forEachDefault(c::putDefaults);
            }
            return c;
        }
        
        public static Concurrent ofIntrinsics() {
            Concurrent c = new Concurrent();
            synchronized (c.gate.monitor()) {
                Symbol.forEachDefault(sym -> {
                    if (sym.isIntrinsic())
                        c.putIntrinsic(sym);
                });
            }
            return c;
        }
        
        public static Concurrent of(Node tree) {
            Concurrent syms = empty();
            tree.collectSymbols(syms::put);
            return syms;
        }
        
        private Concurrent(Concurrent toCopy) {
            try (Gate gate = toCopy.block()) {
                byName.putAll(toCopy.byName);
            }
        }
        
        @Override
        public Concurrent clear() {
            gate.accept(() -> {
                byName.clear();
                incrementModRef();
            });
            return this;
        }
        
        @Override
        public Symbols copy() {
            return new Concurrent(this);
        }
        
        @Override
        public boolean isEmpty() {
            return byName.isEmpty();
        }
        
        private static final UnaryOperator<SimpleModRef> INCREMENT_MOD = mod -> {
            return new SimpleModRef(mod.self.get(), mod.value + 1);
        };
        
        private void incrementModRef() {
            mod.getAndUpdate(INCREMENT_MOD);
        }
        
        private void putIntrinsic(Symbol sym) {
            if (sym.isIntrinsic()) {
                putSymbol(sym);
            } else {
                Log.note(Concurrent.class, "putIntrinsic", "found ", sym.getName());
            }
        }
        
        private void putDefaults(Symbol sym) {
            putSymbol(sym);
            for (Symbol alias : sym.createPreferredAliases()) {
                putSymbol(alias);
            }
        }
        
        private void putSymbol(Symbol symbol) {
            if (symbol.isSpanOp()) {
                putSpanOpImpl(symbol);
            } else {
                putSymbolImpl(symbol);
            }
        }
        
        private void putSpanOpImpl(Symbol span) {
            for (Symbol child : span.getSymbols()) {
                SpanOp prev = (SpanOp) putAtomic(child);
                if (prev != null && !prev.isSymmetric()) {
                    if (!prev.isRight()) {
                        removeImpl(prev.getRight());
                    }
                    if (!prev.isLeft()) {
                        removeImpl(prev.getLeft());
                    }
                }
            }
        }
        
        private Symbol putAtomic(Symbol symbol) {
            String name = symbol.getName();
            Group before, after;
            for (;;) {
                before = byName.get(name);
                if (before == null) {
                    after = new SimpleImmutableGroup(symbol);
                    if (byName.putIfAbsent(name, after) == null)
                        break;
                } else {
                    after = before.add(symbol);
                    if (byName.replace(name, before, after))
                        break;
                }
            }
            return (before == null) ? null : before.get(symbol.getSymbolKind());
        }
        
        private void putSymbolImpl(Symbol symbol) {
            byName.compute(symbol.getName(), (key, group) -> {
                if (group == null) {
                    return new SimpleImmutableGroup(symbol);
                } else {
                    return group.add(symbol);
                }
            });
        }
        
        @Override
        public boolean put(Symbol symbol) {
            Objects.requireNonNull(symbol, "symbol");
            gate.accept(() -> {
                putSymbol(symbol);
                incrementModRef();
            });
            return true;
        }
        
        @Override
        public boolean remove(Symbol symbol) {
            Objects.requireNonNull(symbol, "symbol");
            synchronized (gate.monitor()) {
                boolean willRemove = false;
                
                for (Symbol sym : symbol.getSymbols()) {
                    willRemove |= byName.containsKey(sym.getName());
                    
                    gate.accept(() -> {
                        removeImpl(sym);
                        incrementModRef();
                    });
                }
                
                return willRemove;
            }
        }
        
        private void removeImpl(Symbol sym) {
            byName.compute(sym.getName(), (key, group) -> {
                if (group != null) {
                    group = group.remove(sym);
                    if (group.count() != 0) {
                        return group;
                    }
                }
                return null;
            });
        }
        
        @Override
        public Group get(String name) {
            return byName.get(Objects.requireNonNull(name, "name"));
        }
        
        @Override
        public boolean hasActionsWaiting() {
            return gate.hasActionsWaiting();
        }
        
        @Override
        public boolean isBlocked() {
            return gate.isBlocked();
        }
        
        @Override
        public Symbols block() {
            gate.block();
            return this;
        }
        
        @Override
        public Symbols unblock() {
            gate.unblock();
            return this;
        }
        
        @Override
        public Symbols flush() {
            gate.flush();
            return this;
        }
        
        @Override
        public Symbols forceUnsafeFlush() {
            gate.forceUnsafeFlush();
            return this;
        }
        
        @Override
        public ModRef getModificationRef() {
            return mod.get();
        }
        
        @Override
        public boolean isReference(ModRef mod) {
            if (mod instanceof SimpleModRef) {
                Symbols self = ((SimpleModRef) mod).self();
                return this == self;
            }
            return false;
        }
        
        @Override
        public boolean modifiedSince(ModRef mod) {
            return !mod.isSameModification(this.mod.get());
        }
        
        @Override
        public Map<String, Group> mapView() {
            return Collections.unmodifiableMap(byName);
        }
        
        @Override
        public Stream<Symbol> stream() {
            return byName.values().stream().sequential().flatMap(Group::stream);
        }
        
        @Override
        public String toString() {
            return byName.toString();
        }
    }
    
    @JsonSerializable
    final class Overlay implements Symbols {
        @JsonProperty(interimConverter = Symbols.AppSymbolsInterimConverter.NAME)
        private final Symbols first;
        @JsonProperty(interimConverter = Symbols.AppSymbolsInterimConverter.NAME)
        private final Symbols next;
        
        @JsonConstructor
        public Overlay(Symbols first, Symbols next) {
            this.first = Objects.requireNonNull(first, "first");
            this.next  = Objects.requireNonNull(next,  "next");
        }
        
        @Override
        public Symbols copy() {
            return new Overlay(first.copy(), next.copy());
        }
        
        @Override
        public Overlay clear() {
            first.clear();
            return this;
        }
        
        @Override
        public boolean isEmpty() {
            return first.isEmpty() && next.isEmpty();
        }
        
        @Override
        public Group get(String name) {
            Group g = new OverlayGroup(first.get(name), next.get(name));
            Log.low(Overlay.class, "get(String)", "result = ", g);
            return g;
        }
        
        @Override
        public boolean remove(Symbol sym) {
            return first.remove(sym) || next.remove(sym);
        }
        
        @Override
        public boolean put(Symbol sym) {
            return first.put(sym);
        }
        
        @Override
        public Symbols block() {
            first.block();
            next.block();
            return this;
        }
        
        @Override
        public Symbols unblock() {
            next.unblock();
            first.unblock();
            return this;
        }
        
        @Override
        public boolean isBlocked() {
            return first.isBlocked() || next.isBlocked();
        }
        
        @Override
        public boolean hasActionsWaiting() {
            return first.hasActionsWaiting() || next.hasActionsWaiting();
        }
        
        @Override
        public Symbols flush() {
            first.flush();
            next.flush();
            return this;
        }
        
        @Override
        public Symbols forceUnsafeFlush() {
            first.forceUnsafeFlush();
            next.forceUnsafeFlush();
            return this;
        }
        
        @Override
        public ModRef getModificationRef() {
            return new OverlayModRef(first.getModificationRef(), next.getModificationRef());
        }
        
        @Override
        public boolean isReference(ModRef ref) {
            if (ref instanceof OverlayModRef) {
                OverlayModRef o = (OverlayModRef) ref;
                return first.isReference(o.getRefA()) && next.isReference(o.getRefB());
            }
            return false;
        }
        
        @Override
        public boolean modifiedSince(ModRef ref) {
            if (ref instanceof OverlayModRef) {
                OverlayModRef o = (OverlayModRef) ref;
                return first.modifiedSince(o.getRefA()) || next.modifiedSince(o.getRefB());
            }
            return false;
        }
        
        @Override
        public Map<String, Group> mapView() {
            Map<String, Group> map = new LinkedHashMap<>();
            
            first.block();
            next.block();
            try {
                map.putAll(next.mapView());
                for (Map.Entry<String, Group> e : first.mapView().entrySet()) {
                    Group g = map.get(e.getKey());
                    if (g == null) {
                        map.put(e.getKey(), e.getValue());
                    } else {
                        map.put(e.getKey(), g.addAll(e.getValue()));
                    }
                }
            } finally {
                next.unblock();
                first.unblock();
            }
            
            return Collections.unmodifiableMap(map);
        }
    }
    
    final class SimpleImmutableGroup extends AbstractGroup {
        private final Map<Symbol.Kind, Symbol> symbols;
        
        public static final SimpleImmutableGroup EMPTY = new SimpleImmutableGroup();
        
        public SimpleImmutableGroup() {
            this(Collections.emptyMap(), (Void) null);
        }
        
        public SimpleImmutableGroup(Symbol sym) {
            this(Collections.singletonMap(sym.getSymbolKind(), sym), (Void) null);
        }
        
        public SimpleImmutableGroup(Map<Symbol.Kind, Symbol> symbols) {
            this(new EnumMap<>(symbols), (Void) null);
        }
        
        private SimpleImmutableGroup(Map<Symbol.Kind, Symbol> symbols, Void _private) {
            this.symbols = symbols;
        }
        
        @Override
        public Group add(Symbol sym) {
            Objects.requireNonNull(sym, "sym");
            
            if (symbols.isEmpty()) {
                return new SimpleImmutableGroup(sym);
            } else {
                Map<Symbol.Kind, Symbol> copy = new EnumMap<>(symbols);
                copy.put(sym.getSymbolKind(), sym);
                return new SimpleImmutableGroup(copy, (Void) null);
            }
        }
        
        @Override
        public Group remove(Symbol.Kind kind) {
            if (symbols.containsKey(kind)) {
                if (symbols.size() == 1) {
                    return SimpleImmutableGroup.EMPTY;
                } else {
                    Map<Symbol.Kind, Symbol> copy = new EnumMap<>(symbols);
                    copy.remove(kind);
                    return new SimpleImmutableGroup(copy, (Void) null);
                }
            }
            return this;
        }
        
        @Override
        public int count() {
            return symbols.size();
        }
        
        @Override
        public Symbol get() {
            if (symbols.size() == 1) {
                Log.low(Symbols.class, "get", "symbols.size() == 1: ", symbols);
                
                return symbols.values().iterator().next();
            } else {
                return null;
            }
        }
        
        @Override
        public Symbol get(Symbol.Kind kind) {
            Objects.requireNonNull(kind, "kind");
            
            return symbols.get(kind);
        }
        
        @Override
        public boolean contains(Symbol.Kind kind) {
            return get(kind) != null;
        }
        
        @Override
        public Map<Symbol.Kind, Symbol> mapView() {
            return Collections.unmodifiableMap(symbols);
        }
        
        @Override
        public Stream<Symbol> stream() {
            return symbols.values().stream();
        }
        
        @Override
        public int hashCode() {
            return symbols.hashCode();
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Group) {
                Map<Symbol.Kind, Symbol> symbols =
                    (obj instanceof SimpleImmutableGroup)
                        ? ((SimpleImmutableGroup) obj).symbols
                        : ((Group) obj).mapView();
                return this.symbols.equals(symbols);
            }
            return false;
        }
    }
    
    final class OverlayGroup extends AbstractGroup {
        private final Group first;
        private final Group next;
        
        public OverlayGroup(Group first, Group next) {
            this.first = replnull(first, SimpleImmutableGroup.EMPTY);
            this.next  = replnull(next,  SimpleImmutableGroup.EMPTY);
        }
        
        @Override
        public int count() {
            return mapView().size();
        }
        
        @Override
        public Group add(Symbol sym) {
            return new SimpleImmutableGroup(mapView(), (Void) null).add(sym);
        }
        
        @Override
        public Group remove(Symbol.Kind kind) {
            if (contains(kind)) {
                return new SimpleImmutableGroup(mapView(), (Void) null).remove(kind);
            }
            return this;
        }
        
        @Override
        public Symbol get() {
            Symbol sym0 = first.get();
            Symbol sym1 = next.get();
            return (sym0 == sym1) ? sym0 : null;
        }
        
        @Override
        public Symbol get(Symbol.Kind kind) {
            Symbol sym = first.get(kind);
            if (sym != null) {
                return sym;
            } else {
                return next.get(kind);
            }
        }
        
        private transient volatile Map<Symbol.Kind, Symbol> mapView;
        
        @Override
        public Map<Symbol.Kind, Symbol> mapView() {
            Map<Symbol.Kind, Symbol> mapView = this.mapView;
            if (mapView == null) {
                if (first.isEmpty() && next.isEmpty()) {
                    mapView = Collections.emptyMap();
                } else {
                    mapView = new EnumMap<>(Symbol.Kind.class);
                    mapView.putAll(next.mapView());
                    mapView.putAll(first.mapView());
                    mapView = Collections.unmodifiableMap(mapView);
                }
                this.mapView = mapView;
            }
            return mapView;
        }
    }
    
    final class SimpleModRef implements ModRef {
        private final WeakReference<Symbols> self;
        
        private final long value;
        
        SimpleModRef(Symbols self, long value) {
            this.self  = new WeakReference<>(Objects.requireNonNull(self, "self"));
            this.value = value;
        }
        
        Symbols self() {
            Symbols self = this.self.get();
            assert self != null : "this would be unexpected, but it's why there's a WeakReference";
            return self;
        }
        
        @Override
        public boolean isSameModification(ModRef ref) {
            if (ref instanceof SimpleModRef) {
                SimpleModRef that  = (SimpleModRef) ref;
                Symbols      selfA = this.self();
                Symbols      selfB = that.self();
                return (selfA != null) && (selfB != null) && (selfA == selfB) && (this.value == that.value);
            }
            Objects.requireNonNull(ref, "ref");
            return false;
        }
    }
    
    final class OverlayModRef implements ModRef {
        private final ModRef refA;
        private final ModRef refB;
        
        OverlayModRef(ModRef refA, ModRef refB) {
            this.refA = Objects.requireNonNull(refA, "refA");
            this.refB = Objects.requireNonNull(refB, "refB");
        }
        
        ModRef getRefA() { return refA; }
        ModRef getRefB() { return refB; }
        
        @Override
        public boolean isSameModification(ModRef ref) {
            if (ref instanceof OverlayModRef) {
                OverlayModRef that = (OverlayModRef) ref;
                return this.refA.isSameModification(that.refA) && this.refB.isSameModification(that.refB);
            }
            Objects.requireNonNull(ref, "ref");
            return false;
        }
    }
}

final class SymbolsPrivate {
    private SymbolsPrivate() {
    }
    
    private static final ThreadLocal<Symbols> LOCAL = new ThreadLocal<>();
    
    static Symbols setLocal(Symbols syms) {
        Symbols prev = LOCAL.get();
        if (syms == null) {
            LOCAL.remove();
        } else {
            LOCAL.set(syms);
        }
        return prev;
    }
    
    static Symbols getLocal() {
        Symbols syms = LOCAL.get();
        if (syms == null) {
            syms = app().getSymbols();
        }
        return syms;
    }
}