/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.util.*;
import eps.math.*;
import eps.math.Number;

import java.util.*;
import java.util.stream.*;
import java.math.*;
import static java.lang.Character.*;

// TODO:
//  * Other radixes...? (Is there a point to that...?)
public enum NumberParser {
    BINARY {
        @Override
        public Number parse(MiniSettings sets, MathEnv env, String source, int start, MutableInt end) {
            int length = source.length();
            if (start < length) {
                int code0 = source.codePointAt(start);
                if (code0 == '0') {
                    if (++start < length) {
                        int code1 = source.codePointAt(start);
                        if (code1 == 'b' || code1 == 'B') {
                            if (++start < length) {
                                return parse0(sets, env, source, start, end);
                            }
                        }
                    }
                }
            }
            return null;
        }
        private Number parse0(MiniSettings sets, MathEnv env, String source, int i, MutableInt end) {
            BigInteger result = BigInteger.ZERO;
            
            CodePoint sepCode = sets.getSetting(Setting.DIGIT_SEPARATOR);
            int       sepInt  = (sepCode == null) ? 0 : sepCode.getAsInt();
            
            int count = 0;
            for (int len = source.length(), code; i < len; i += charCount(code)) {
                code = source.codePointAt(i);
                
                if (code == sepInt && sepCode != null)
                    continue;
                if (code != '0' && code != '1')
                    break;
                ++count;
                
                result = result.shiftLeft(1);
                if (code == '1') {
                    result = result.setBit(0);
                }
            }
            if (count == 0) {
                return null;
            }
            
            end.value = i;
            return env.valueOf(result);
        }
    },
    HEXADECIMAL {
        @Override
        public Number parse(MiniSettings sets, MathEnv env, String source, int start, MutableInt end) {
            int length = source.length();
            if (start < length) {
                int code0 = source.codePointAt(start);
                if (code0 == '0') {
                    if (++start < length) {
                        int code1 = source.codePointAt(start);
                        if (code1 == 'x' || code1 == 'X') {
                            if (++start < length) {
                                return parse0(sets, env, source, start, end);
                            }
                        }
                    }
                }
            }
            return null;
        }
        
        private final BigInteger[] digits = IntStream.range(0, 16).mapToObj(BigInteger::valueOf).toArray(BigInteger[]::new);
        
        private Number parse0(MiniSettings sets, MathEnv env, String source, int i, MutableInt end) {
            BigInteger result = BigInteger.ZERO;
            
            CodePoint sepCode = sets.getSetting(Setting.DIGIT_SEPARATOR);
            int       sepInt  = (sepCode == null) ? 0 : sepCode.getAsInt();
            
            BigInteger[] digits = this.digits;
            
            int count = 0;
            for (int len = source.length(), code; i < len; i += charCount(code)) {
                code = source.codePointAt(i);
                
                if (code == sepInt && sepCode != null)
                    continue;
                
                BigInteger digit;
                
                if ('0' <= code && code <= '9') {
                    digit = digits[code - '0'];
                } else if ('a' <= code && code <= 'f') {
                    digit = digits[10 + code - 'a'];
                } else if ('A' <= code && code <= 'F') {
                    digit = digits[10 + code - 'A'];
                } else {
                    break;
                }
                
                ++count;
                
                result = result.shiftLeft(4).or(digit);
            }
            if (count == 0) {
                return null;
            }
            
            end.value = i;
            return env.valueOf(result);
        }
    },
    DECIMAL {
        @Override
        public Number parse(MiniSettings sets, MathEnv env, String source, int start, MutableInt end) {
            Number zero = parseZero(sets, env, source, start, end);
            
            if (zero != null) {
                return zero;
            }
            
            return parseDecimal(sets, env, source, start, end);
        }
        
        private Number parseZero(MiniSettings sets, MathEnv env, String source, int i, MutableInt end) {
            int length = source.length();
            int count  = 0;
            
            CodePoint sepCode = sets.getSetting(Setting.DIGIT_SEPARATOR);
            int       sepInt  = sepCode == null ? 0 : sepCode.getAsInt();
            CodePoint decCode = sets.getSetting(Setting.DECIMAL_MARK);
            int       decInt  = decCode == null ? 0 : decCode.getAsInt();
            
//            Log.low(getClass(), "parseZero", "decimal mark = ", decCode);
            
            boolean decimalFound = false;
            
            for (int code; i < length; i += charCount(code)) {
                code = source.codePointAt(i);
//                Log.low(getClass(), "parseZero", "i = ", i, ", code = ", CodePoint.valueOf(code));
                
                if (code == '0') {
                    ++count;
                    continue;
                }
                if (code == sepInt && sepCode != null) {
                    continue;
                }
                if (code == decInt && decCode != null) {
                    if (decimalFound) {
                        break;
                    }
                    decimalFound = true;
                    continue;
                }
                if ('1' <= code && code <= '9') {
                    // not zero
                    return null;
                }
                break;
            }
            
            if (count > 0) {
                end.value = i;
//                Log.low(getClass(), "parseZero", "result = 0");
                return env.zero();
            }
            
            return null;
        }
        
        private final BigInteger[] digits = IntStream.range(0, 10).mapToObj(BigInteger::valueOf).toArray(BigInteger[]::new);
        
        private Number parseDecimal(MiniSettings sets, MathEnv env, String source, int i, MutableInt end) {
            int length = source.length();
            int count  = 0;
            
            CodePoint sepCode = sets.getSetting(Setting.DIGIT_SEPARATOR);
            int       sepInt  = sepCode == null ? 0 : sepCode.getAsInt();
            CodePoint decCode = sets.getSetting(Setting.DECIMAL_MARK);
            int       decInt  = decCode == null ? 0 : decCode.getAsInt();
            
//            Log.low(getClass(), "parseDecimal", "decimal mark = ", decCode);
            
            boolean decimalFound = false;
            
            BigInteger mag   = BigInteger.ZERO;
            BigInteger scale = BigInteger.ONE;
            
            BigInteger[] digits = this.digits;
            
            for (int code; i < length; i += charCount(code)) {
                code = source.codePointAt(i);
//                Log.low(getClass(), "parseDecimal", "i = ", i, ", code = ", CodePoint.valueOf(code));
                
                if (code == sepInt && sepCode != null) {
                    continue;
                }
                
                if (code == decInt && decCode != null) {
                    if (decimalFound)
                        break;
                    decimalFound = true;
                    continue;
                }
                
                if (code < '0' || '9' < code) {
                    break;
                }
                
                BigInteger digit = digits[code - '0'];
                
                mag = mag.multiply(BigInteger.TEN).add(digit);
                ++count;
                
                if (decimalFound) {
                    scale = scale.multiply(BigInteger.TEN);
                }
            }
            
            if (count > 0) {
                end.value = i;
                
                Number result;
                if (scale.equals(BigInteger.ONE)) {
                    result = env.valueOf(mag);
                } else {
                    result = env.valueOf(env.valueOf(mag), env.valueOf(scale));
                }
                
//                Log.low(getClass(), "parseDecimal", "result = ", result);
                return result;
            }
            
            return null;
        }
    };
    
    public Number parse(Context context, String source, int start, MutableInt end) {
        return parse(context, context.getMathEnvironment(), source, start, end);
    }
    
    public Number parse(String source) {
        return parse(source, MathEnv.instance());
    }
    
    public Number parse(String source, MathEnv env) {
        MutableInt end = new MutableInt();
        Number     num = parse(MiniSettings.instance(), env, source, 0, end);
        if (num == null || end.value != source.length()) {
            throw new NumberFormatException(source);
        }
        return num;
    }
    
    public abstract Number parse(MiniSettings sets, MathEnv env, String source, int start, MutableInt end);
    
    private static final NumberParser[] VALUES = values();
    
    public static Number parseAny(Context context, CharSequence source, int start, MutableInt end) {
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(source,  "source");
        Objects.requireNonNull(end,     "end");
        
        if (start < 0 || source.length() < start) {
            throw Errors.newIndexOutOfBoundsClosed("start", start, 0, source.length());
        }
        
        String str = source.toString();
        
        for (NumberParser parser : VALUES) {
            Number num = parser.parse(context, str, start, end);
            if (num != null) {
                return num;
            }
        }
        
        return null;
    }
}