/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.json.*;
import eps.util.*;

import java.util.*;

@JsonSerializable
public abstract class AbstractSymbol implements Symbol {
    @JsonProperty
    private final String name;
    @JsonProperty
    private final EchoFormatter echoFormatter;
    @JsonProperty(interimConverter = "eps.eval.AbstractSymbol$PreferredAliasesInterimConverter")
    private final Set<String> preferredAliases;
    
    @JsonDefault("name")
    private static final String DEFAULT_NAME = "unnamed_sym";
    @JsonDefault("echoFormatter")
    private static final EchoFormatter DEFAULT_ECHO_FORMATTER = null;
    @JsonDefault("preferredAliases")
    private static final Set<String> DEFAULT_PREFERRED_ALIASES = Collections.emptySet();
    
    private static final class PreferredAliasesInterimConverter extends InterimConverter<Set<?>, Set<?>> {
        private PreferredAliasesInterimConverter() {
            super(Misc.WILD_SET, Misc.WILD_SET);
        }
        @Override
        public Set<?> toInterim(Object obj) {
            return new LinkedHashSet<>((Set<?>) obj);
        }
        @Override
        public Set<?> fromInterim(Object obj) {
            Set<?> set = Misc.requireNonNull((Set<?>) obj, "preferredAliases");
            return set.isEmpty() ? Collections.emptySet() : Collections.unmodifiableSet(set);
        }
    }
    
    @JsonConstructor
    protected AbstractSymbol(String        name,
                             EchoFormatter echoFormatter,
                             Set<String>   preferredAliases) {
        this.name             = Objects.requireNonNull(name, "name");
        if (name.isEmpty()) {
            throw new IllegalArgumentException("empty name");
        }
        if (echoFormatter == null) {
            echoFormatter = getDefaultEchoFormatter();
            if (echoFormatter == null) {
                echoFormatter = EchoFormatter.of(name);
            }
        }
        this.echoFormatter    = echoFormatter;
        this.preferredAliases = Objects.requireNonNull(preferredAliases, "preferredAliases");
    }
    
    protected AbstractSymbol(Builder<?, ?> builder) {
        this(builder.getName(), builder.getEchoFormatter(), builder.getPreferredAliases());
    }
    
    // Alias constructor.
    protected AbstractSymbol(String name, Symbol delegate) {
        this(name, null, delegate);
    }
    
    // Alias constructor.
    protected AbstractSymbol(String name, EchoFormatter echoFormatter, Symbol delegate) {
        this(name, echoFormatter == null ? delegate.getEchoFormatter() : echoFormatter, new String[] {name});
    }
    
    private AbstractSymbol(String         name,
                           EchoFormatter  echoFormatter,
                           String[]       preferredAliases) {
        this(name, echoFormatter, createPreferredAliasSet(name, preferredAliases));
    }
    
    private static Set<String> createPreferredAliasSet(String name, String[] preferredAliases) {
        if (preferredAliases == null || preferredAliases.length == 0) {
            return Collections.emptySet();
//            return Collections.singleton(name);
        }
        
        Set<String> set = new LinkedHashSet<>(preferredAliases.length);
        
        for (String alias : preferredAliases) {
            set.add(Objects.requireNonNull(alias, "preferred alias"));
        }
        
        return Collections.unmodifiableSet(set);
    }
    
    protected EchoFormatter getDefaultEchoFormatter() {
        return null;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public EchoFormatter getEchoFormatter() {
        return echoFormatter;
    }
    
    @Override
    public Set<String> getPreferredAliases() {
        return preferredAliases;
    }
    
    @Override
    public boolean isEqualTo(Symbol sym) {
        if (sym == null)
            return false;
        if (this == sym)
            return true;
        if (this.getClass() != sym.getClass())
            return false;
        AbstractSymbol that = (AbstractSymbol) sym;
        if (!Objects.equals(this.name, that.name))
            return false;
        if (!Objects.equals(this.echoFormatter, that.echoFormatter))
            return false;
        if (!Objects.equals(this.preferredAliases, that.preferredAliases))
            return false;
        return true;
    }
    
    @Override
    public String toString() {
        return getName();
    }
}