/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.math.*;
import eps.math.Number;
import eps.math.Boolean;
import eps.json.*;

import java.util.*;
import java.util.function.*;

@JsonSerializable
public abstract class BinaryOp extends Operator {
    @JsonProperty
    private final boolean tupleOnLeft;
    @JsonProperty
    private final boolean tupleOnRight;
    
    @JsonDefault("tupleOnLeft")
    private static final boolean TUPLE_ON_LEFT_DEFAULT = false;
    @JsonDefault("tupleOnRight")
    private static final boolean TUPLE_ON_RIGHT_DEFAULT = false;
    
    public BinaryOp(Builder builder) {
        super(builder);
        
        this.tupleOnLeft  = builder.tupleOnLeft;
        this.tupleOnRight = builder.tupleOnRight;
    }
    
    private BinaryOp(String name, EchoFormatter echoFormatter, BinaryOp delegate) {
        super(name, echoFormatter, delegate);
        
        this.tupleOnLeft  = delegate.tupleOnLeft;
        this.tupleOnRight = delegate.tupleOnRight;
    }
    
    @JsonConstructor
    protected BinaryOp(String        name,
                       EchoFormatter echoFormatter,
                       Set<String>   preferredAliases,
                       int           precedence,
                       Associativity associativity,
                       boolean       tupleOnLeft,
                       boolean       tupleOnRight) {
        super(name, echoFormatter, preferredAliases, precedence, associativity);
        this.tupleOnLeft  = tupleOnLeft;
        this.tupleOnRight = tupleOnRight;
    }
    
    @Override
    protected EchoFormatter getDefaultEchoFormatter() {
        return EchoFormatter.OF_SPACED_BINARY_OP;
    }
    
    @Override
    public boolean isEqualTo(Symbol sym) {
        if (super.isEqualTo(sym)) {
            BinaryOp that = (BinaryOp) sym;
            if (this.tupleOnLeft != that.tupleOnLeft)
                return false;
            if (this.tupleOnRight != that.tupleOnRight)
                return false;
            return true;
        }
        return false;
    }
    
    @Override
    public Kind getSymbolKind() {
        return Kind.BINARY_OP;
    }
    
    @Override
    public BinaryOp createAlias(String aliasName, EchoFormatter echoFormatter) {
        return new Alias(aliasName, echoFormatter, (BinaryOp) this.getDelegate());
    }
    
    public boolean tupleOnLeft() {
        return tupleOnLeft;
    }
    
    public boolean tupleOnRight() {
        return tupleOnRight;
    }
    
    public Operand apply(Context context, Node lhs, Node rhs) {
        if (SpecialSyms.isEllipse(context, rhs)) {
            Operand op = lhs.evaluate(context);
            if (op.isTuple()) {
                Tuple tuple = (Tuple) op;
                int   count = tuple.count();
                if (count >= 2) {
                    Operand result = apply(context, tuple.get(0), tuple.get(1));
                    for (int i = 2; i < count; ++i) {
                        result = apply(context, result, tuple.get(i));
                    }
                    return result;
                } // else TODO: return tuple[0]? return operator-specific empty result? ([] + ... = 0, [] * ... = 1, etc.)
                throw UnsupportedException.fmt("ellipse form of %s requires a tuple with at least 2 elements; found %s",
                                               getName(), lhs.getExpressionSource());
            }
            throw UnsupportedException.operandType(op, "ellipse form of " + getName());
        }
        
        return apply(context, lhs.evaluate(context), rhs.evaluate(context));
    }
    
    protected Operand apply(Context context, Operand lhs, Operand rhs) {
        lhs = lhs.evaluate(context);
        rhs = rhs.evaluate(context);
        
        if (lhs.isNumber() && rhs.isNumber()) {
            return apply(context, (Number) lhs, (Number) rhs);
        }
        if (lhs.isStringLiteral() && rhs.isStringLiteral()) {
            return apply(context, (StringLiteral) lhs, (StringLiteral) rhs);
        }
        
        if (lhs.isTuple() && tupleOnLeft() && rhs.isNumber()) {
            Operand rhs0 = rhs;
            return ((Tuple) lhs).map(e -> apply(context, e, rhs0));
        }
        if (rhs.isTuple() && tupleOnRight() && lhs.isNumber()) {
            Operand lhs0 = lhs;
            return ((Tuple) rhs).map(e -> apply(context, lhs0, e));
        }
        
        throw UnsupportedException.operandTypes(lhs, rhs, getName());
    }
    
    protected Operand apply(Context c, Number lhs, Number rhs) {
        throw UnsupportedException.operandTypes(lhs, rhs, getName());
    }
    
    protected Operand apply(Context c, StringLiteral lhs, StringLiteral rhs) {
        throw UnsupportedException.operandTypes(lhs, rhs, getName());
    }
    
    public static class Alias extends BinaryOp {
        private final BinaryOp delegate;
        
        public Alias(String name, BinaryOp delegate) {
            this(name, null, delegate);
        }
        
        public Alias(String name, EchoFormatter echoFormatter, BinaryOp delegate) {
            super(name, echoFormatter, Objects.requireNonNull(delegate, "delegate"));
            this.delegate = delegate;
        }
        
        static {
            JsonObjectAdapter.put(BinaryOp.Alias.class,
                                  Symbol::aliasToMap,
                                  map -> (BinaryOp.Alias) Symbol.mapToAlias(map));
        }
        
        @Override
        public Alias configure(Symbol.Builder<?, ?> b) {
            return new Alias(b.getName(), b.getEchoFormatter(), delegate);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            return super.isEqualTo(sym) && Symbol.isEqual(this.delegate, ((Alias) sym).delegate);
        }
        
        @Override
        public Symbol getDelegate() {
            return delegate.getDelegate();
        }
        
        @Override
        public Operand apply(Context c, Node lhs, Node rhs) {
            return delegate.apply(c, lhs, rhs);
        }
        
        @Override
        protected Operand apply(Context c, Operand lhs, Operand rhs) {
            return delegate.apply(c, lhs, rhs);
        }
        
        @Override
        protected Operand apply(Context c, StringLiteral lhs, StringLiteral rhs) {
            return delegate.apply(c, lhs, rhs);
        }
        
        @Override
        protected Operand apply(Context c, Number lhs, Number rhs) {
            return delegate.apply(c, lhs, rhs);
        }
    }
    
    @FunctionalInterface
    public interface OpOverload<O extends Operand> {
        Operand apply(BinaryOp self, Context c, O lhs, O rhs);
    }
    @FunctionalInterface
    public interface MathOpOverload<O extends Operand> extends OpOverload<O> {
        @Override
        default Operand apply(BinaryOp self, Context c, O lhs, O rhs) {
            return apply(c.getMathEnvironment(), lhs, rhs);
        }
        Operand apply(MathEnv env, O lhs, O rhs);
    }
    
    @JsonSerializable
    public static class FromFunction extends BinaryOp {
        @JsonProperty
        private final OpOverload<Number> numberFn;
        @JsonProperty
        private final OpOverload<StringLiteral> stringFn;
        
        public FromFunction(Builder builder) {
            super(builder);
            
            this.numberFn = Objects.requireNonNull(builder.numberFn, "numberFn");
            this.stringFn = Objects.requireNonNull(builder.stringFn, "stringFn");
        }
        
        private FromFunction(Builder builder, OpOverload<Number> numberFn, OpOverload<StringLiteral> stringFn) {
            super(builder);
            
            this.numberFn = numberFn;
            this.stringFn = stringFn;
        }
        
        @JsonConstructor
        protected FromFunction(String                    name,
                               EchoFormatter             echoFormatter,
                               Set<String>               preferredAliases,
                               int                       precedence,
                               Associativity             associativity,
                               boolean                   tupleOnLeft,
                               boolean                   tupleOnRight,
                               OpOverload<Number>        numberFn,
                               OpOverload<StringLiteral> stringFn) {
            super(name, echoFormatter, preferredAliases, precedence, associativity, tupleOnLeft, tupleOnRight);
            this.numberFn = Objects.requireNonNull(numberFn, "numberFn");
            this.stringFn = Objects.requireNonNull(stringFn, "stringFn");
        }
        
        @Override
        public FromFunction configure(Symbol.Builder<?, ?> b) {
            return new FromFunction((Builder) b, numberFn, stringFn);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                FromFunction that = (FromFunction) sym;
                return Objects.equals(this.numberFn, that.numberFn)
                    && Objects.equals(this.stringFn, that.stringFn);
            }
            return false;
        }
        
        @Override
        public Operand apply(Context c, Number lhs, Number rhs) {
            return numberFn.apply(this, c, lhs, rhs);
        }
        
        @Override
        public Operand apply(Context c, StringLiteral lhs, StringLiteral rhs) {
            return stringFn.apply(this, c, lhs, rhs);
        }
    }
    
    @JsonSerializable
    public static class BinaryError extends BinaryOp {
        public BinaryError(Builder builder) {
            super(builder);
        }
        
        @JsonConstructor
        protected BinaryError(String        name,
                              EchoFormatter echoFormatter,
                              Set<String>   preferredAliases,
                              int           precedence,
                              Associativity associativity,
                              boolean       tupleOnLeft,
                              boolean       tupleOnRight) {
            super(name, echoFormatter, preferredAliases, precedence, associativity, tupleOnLeft, tupleOnRight);
        }
        
        @Override
        public BinaryError configure(Symbol.Builder<?, ?> b) {
            return new BinaryError((Builder) b);
        }
        
        @Override
        public boolean isError() {
            return true;
        }
        
        @Override
        public Operand apply(Context c, Node l, Node r) {
            throw EvaluationException.unresolved(this);
        }
        
        @Override
        public Operand apply(Context c, Operand l, Operand r) {
            throw EvaluationException.unresolved(this);
        }
        
        @Override
        public Operand apply(Context c, Number l, Number r) {
            throw EvaluationException.unresolved(this);
        }
        
        @Override
        public Operand apply(Context c, StringLiteral l, StringLiteral r) {
            throw EvaluationException.unresolved(this);
        }
    }
    
    public static BinaryError createBinaryError(Token.OfUnresolved u) {
        return createBinaryError(u.getSource().toString());
    }
    
    public static BinaryError createBinaryError(String name) {
        return new Builder().setName(name)
                            .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                            .setPrecedence(HIGHEST_PRECEDENCE)
                            .setAssociativity(Associativity.LEFT_TO_RIGHT) // TODO?
                            .buildError();
    }
    
    @JsonSerializable
    public static class FromTree extends BinaryOp {
        @JsonProperty(interimConverter = TreeFunction.INTERIM_CONVERTER,
                      deferInterimToConstructor = true)
        private final TreeFunction treeFn;
        
        private static Builder createBuilderForSuperConstructor(Node.OfOperator decl) {
            Operator op = decl.getOperator();
            return new Builder().setName(op.getName())
                                .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                                .setAssociativity(op.getAssociativity())
                                .setPrecedence(MULTIPLY_DIVIDE_PRECEDENCE) // TODO
                                ;
        }
        
        public FromTree(Context context, Node.OfBinaryOp decl, Node body) {
            super(createBuilderForSuperConstructor(decl));
            
            this.treeFn = new TreeFunction(context, this, decl, body);
        }
        
        public FromTree(Context context, Node.OfNaryOp decl, Node body) {
            super(createBuilderForSuperConstructor(decl));
            assert decl.getOperands().size() == 2 : decl.getExpressionSource();
            
            this.treeFn = new TreeFunction(context, this, decl, body);
        }
        
        private FromTree(Builder b, TreeFunction treeFn) {
            super(b);
            
            this.treeFn = treeFn.copy(this);
        }
        
        public FromTree(Builder builder, TreeAdapter<?, String> params, String body) {
            super(builder);
            
            this.treeFn = new TreeFunction(this, params, body);
        }
        
        @Override
        public FromTree configure(Symbol.Builder<?, ?> b) {
            return new FromTree((Builder) b, treeFn);
        }
        
        @JsonConstructor
        protected FromTree(String        name,
                           EchoFormatter echoFormatter,
                           Set<String>   preferredAliases,
                           int           precedence,
                           Associativity associativity,
                           boolean       tupleOnLeft,
                           boolean       tupleOnRight,
                           Object        treeFn) {
            super(name, echoFormatter, preferredAliases, precedence, associativity, tupleOnLeft, tupleOnRight);
            
            this.treeFn = TreeFunction.fromInterimObject(this, treeFn);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                FromTree that = (FromTree) sym;
                return TreeFunction.isEqual(this.treeFn, that.treeFn);
            }
            return false;
        }
        
        TreeFunction getTreeFunction() {
            return treeFn;
        }
        
        @Override
        public Operand apply(Context context, Node lhs, Node rhs) {
            return treeFn.evaluate(context, lhs, rhs);
        }
        
        @Override
        protected Operand apply(Context context, Operand lhs, Operand rhs) {
            throw UnsupportedException.operandTypes(lhs, rhs, getName());
        }
        
        @Override
        protected Operand apply(Context c, Number lhs, Number rhs) {
            throw UnsupportedException.operandTypes(lhs, rhs, getName());
        }
        
        @Override
        protected Operand apply(Context c, StringLiteral lhs, StringLiteral rhs) {
            throw UnsupportedException.operandTypes(lhs, rhs, getName());
        }
    }
    
    public static class Builder extends Operator.Builder<BinaryOp, Builder> {
        private enum NoNumber implements OpOverload<Number> {
            INSTANCE;
            @Override
            public Operand apply(BinaryOp self, Context c, Number lhs, Number rhs) {
                throw UnsupportedException.operandTypes(lhs, rhs, self.getName());
            }
        }
        private enum NoString implements OpOverload<StringLiteral> {
            INSTANCE;
            @Override
            public Operand apply(BinaryOp self, Context c, StringLiteral lhs, StringLiteral rhs) {
                throw UnsupportedException.operandTypes(lhs, rhs, self.getName());
            }
        }
        
        private OpOverload<Number> numberFn = NoNumber.INSTANCE;
        private OpOverload<StringLiteral> stringFn = NoString.INSTANCE;
        
        private boolean tupleOnLeft;
        private boolean tupleOnRight;
        
        public Builder() {
            super.setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                 .setPrecedence(Operator.MULTIPLY_DIVIDE_PRECEDENCE);
        }
        
        @Override
        public Builder setAll(BinaryOp binary) {
            this.tupleOnLeft  = binary.tupleOnLeft();
            this.tupleOnRight = binary.tupleOnRight();
            
            if (binary instanceof BinaryOp.FromFunction) {
                this.setNumberFunction(((BinaryOp.FromFunction) binary).numberFn)
                    .setStringFunction(((BinaryOp.FromFunction) binary).stringFn);
            }
            
            return super.setAll(binary);
        }
        
        public Builder setNumberFunction(OpOverload<Number> numberFn) {
            this.numberFn = Objects.requireNonNull(numberFn, "numberFn");
            return this;
        }
        
        public Builder setNumberFunction(MathOpOverload<Number> numberFn) {
            return setNumberFunction((OpOverload<Number>) numberFn);
        }
        
        public Builder setStringFunction(OpOverload<StringLiteral> stringFn) {
            this.stringFn = Objects.requireNonNull(stringFn, "stringFn");
            return this;
        }
        
        public Builder allowTupleOnLeft() {
            tupleOnLeft = true;
            return this;
        }
        
        public Builder allowTupleOnRight() {
            tupleOnRight = true;
            return this;
        }
        
        @Override
        public BinaryOp build() {
            return new FromFunction(this);
        }
        
        @Override
        public BinaryError buildError() {
            return new BinaryError(this);
        }
        
        protected static final ToString<Builder> BINARY_OP_BUILDER_TO_STRING =
            OPERATOR_BUILDER_TO_STRING.derive(Builder.class)
                                      .add("numberFn",     b -> b.numberFn)
                                      .add("stringFn",     b -> b.stringFn)
                                      .add("tupleOnLeft",  b -> b.tupleOnLeft)
                                      .add("tupleOnRight", b -> b.tupleOnRight)
                                      .build();
        @Override
        public String toString(boolean multiline) {
            return BINARY_OP_BUILDER_TO_STRING.apply(this, multiline);
        }
    }
    
    public static class AddOp extends BinaryOp {
        public AddOp(Builder builder) {
            super(builder);
        }
        @Override
        protected Operand apply(Context context, Operand lhs, Operand rhs) {
            lhs = lhs.evaluate(context);
            rhs = rhs.evaluate(context);
            
            if (lhs.isStringLiteral() || rhs.isStringLiteral()) {
                return new StringLiteral(context, lhs, rhs);
            }
            if (lhs.isTuple() && rhs.isTuple()) {
                return ((Tuple) lhs).cat((Tuple) rhs);
            }
            if (lhs.isTuple()) {
                return ((Tuple) lhs).append(rhs);
            }
            if (rhs.isTuple()) {
                return ((Tuple) rhs).prepend(lhs);
            }
            
            return super.apply(context, lhs, rhs);
        }
        @Override
        protected Operand apply(Context c, Number lhs, Number rhs) {
            return c.getMathEnvironment().add(lhs, rhs);
        }
        @Override
        public AddOp configure(Symbol.Builder<?, ?> b) {
            return new AddOp((Builder) b);
        }
    }
    
    public static final BinaryOp ADD =
        new AddOp(new Builder().setName("add")
                               .makeIntrinsic()
                               .addPreferredAlias("+")
                               .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                               .setPrecedence(ADD_SUBTRACT_PRECEDENCE)
                               .setAssociativity(Associativity.LEFT_TO_RIGHT));
    
    public static final BinaryOp SUBTRACT =
        new Builder().setName("subtract")
                     .makeIntrinsic()
                     .addPreferredAlias("-")
                     .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                     .setPrecedence(ADD_SUBTRACT_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::subtract)
                     .build();
    
    public static final BinaryOp MULTIPLY =
        new Builder().setName("multiply")
                     .makeIntrinsic()
                     .addPreferredAliases("*", "×", "∙")
                     .setEchoFormatter(EchoFormatter.ofBinary("∙", true))
                     .setPrecedence(MULTIPLY_DIVIDE_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::multiply)
                     .allowTupleOnLeft()
                     .allowTupleOnRight()
                     .build();
    
    public static final BinaryOp EMPTY_MULTIPLY =
        new Builder().setName("empty_multiply")
                     .makeIntrinsic()
                     .setEchoFormatter(EchoFormatter.OF_EMPTY_MULTIPLY)
                     .setPrecedence(MULTIPLY_DIVIDE_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::multiply)
                     .allowTupleOnLeft()
                     .allowTupleOnRight()
                     .build();
    
    public static final BinaryOp DIVIDE =
        new Builder().setName("divide")
                     .makeIntrinsic()
                     .addPreferredAlias("/")
                     .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                     .setPrecedence(MULTIPLY_DIVIDE_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::divide)
                     .allowTupleOnLeft()
                     .build();
    
    public static final BinaryOp REMAINDER =
        new Builder().setName("remainder")
                     .makeIntrinsic()
                     .addPreferredAlias("mod")
                     .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                     .setPrecedence(MULTIPLY_DIVIDE_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::mod)
                     .allowTupleOnLeft()
                     .build();
    
    public static final BinaryOp POW =
        new Builder().setName("pow")
                     .makeIntrinsic()
                     .addPreferredAlias("^")
                     .setEchoFormatter(EchoFormatter.OF_EXP_BINARY_OP)
                     .setPrecedence(EXPONENT_PRECEDENCE)
                     .setAssociativity(Associativity.RIGHT_TO_LEFT)
                     .setNumberFunction(MathEnv::pow)
                     .build();
    
    public static final BinaryOp DECIMAL_EXPONENT =
        new Builder().setName("decimal_exponent")
                     .makeIntrinsic()
                     .addPreferredAlias("e")
                     .setEchoFormatter(EchoFormatter.OF_BINARY_OP)
                     .setPrecedence(EXPONENT_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction((env, base, exp) -> {
                         return env.multiply(base, env.pow(env.valueOf(10), exp));
                     })
                     .build();
    
    public static final BinaryOp NTH_ROOT =
        new Builder().setName("binary_nth_rt")
                     .makeIntrinsic()
                     .addPreferredAliases("rt")
                     .setEchoFormatter(EchoFormatter.OF_NTH_ROOT)
                     .setPrecedence(EXPONENT_PRECEDENCE)
                     .setAssociativity(Associativity.RIGHT_TO_LEFT)
                     .setNumberFunction(MathEnv::nrt)
                     .build();
    
    // TODO: Could these be n-ary ops?
    private static BinaryOp createRelationalOp(String       name,
                                               String       alias,
                                               String       echo,
                                               IntPredicate predicate) {
        return new Builder().setName(name)
                            .makeIntrinsic()
                            .addPreferredAlias(alias)
                            .setEchoFormatter(EchoFormatter.ofBinary(echo, true))
                            .setPrecedence(RELATIONAL_PRECEDENCE)
                            .setAssociativity(Associativity.LEFT_TO_RIGHT)
                            .setNumberFunction((env, lhs, rhs) -> Boolean.valueOf(predicate.test(env.relate(lhs, rhs))))
                            .setStringFunction((self, c, lhs, rhs) -> Boolean.valueOf(predicate.test(lhs.compareTo(rhs))))
                            .build();
    }
    
    public static final BinaryOp IS_EQUAL_TO = createRelationalOp("isequalto",  "=", "=", r -> r == 0);
    
    public static final BinaryOp IS_NOT_EQUAL_TO = createRelationalOp("isnotequalto",  "!=", "≠", r -> r != 0);
    
    public static final BinaryOp IS_LESS_THAN = createRelationalOp("islessthan", "<", "<", r -> r < 0);
    
    public static final BinaryOp IS_GREATER_THAN = createRelationalOp("isgreaterthan", ">", ">", r -> r > 0);
    
    public static final BinaryOp IS_LESS_THAN_OR_EQUAL_TO = createRelationalOp("islessthanorequalto", "<=", "≤", r -> r <= 0);
    
    public static final BinaryOp IS_GREATER_THAN_OR_EQUAL_TO = createRelationalOp("isgreaterthanorequalto", ">=", "≥", r -> r >= 0);
    
    public static final BinaryOp BOOLEAN_OR =
        new Builder().setName("or")
                     .makeIntrinsic()
                     .addPreferredAlias("or")
                     .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                     .setPrecedence(BOOLEAN_OP_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::or)
                     .build();
    
    public static final BinaryOp BOOLEAN_AND =
        new Builder().setName("and")
                     .makeIntrinsic()
                     .addPreferredAlias("and")
                     .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                     .setPrecedence(BOOLEAN_OP_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::and)
                     .build();
    
    public static final BinaryOp BOOLEAN_XOR =
        new Builder().setName("xor")
                     .makeIntrinsic()
                     .addPreferredAlias("xor")
                     .setEchoFormatter(EchoFormatter.OF_SPACED_BINARY_OP)
                     .setPrecedence(BOOLEAN_OP_PRECEDENCE)
                     .setAssociativity(Associativity.LEFT_TO_RIGHT)
                     .setNumberFunction(MathEnv::xor)
                     .build();
    
    public static final List<BinaryOp> DEFAULTS =
        Collections.unmodifiableList(Misc.collectConstants(BinaryOp.class));
}