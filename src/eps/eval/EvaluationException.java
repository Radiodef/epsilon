/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.json.*;
import static eps.util.Misc.*;
import static eps.util.Literals.*;

import java.util.*;
import java.util.function.*;

public class EvaluationException extends RuntimeException {
    static {
        JsonObjectAdapter.put(EvaluationException.class,
                              EvaluationException::toMap,
                              EvaluationException::new);
    }
    
    public EvaluationException() {
        this((String) null);
    }
    
    public EvaluationException(String message) {
        super(replnull(message, "error during evaluation"));
    }
    
    public EvaluationException(Throwable cause) {
        super(cause != null ? cause.getMessage() : null, cause);
    }
    
    public EvaluationException(Map<?, ?> map) {
        super((String) map.get("message"));
        
        StackTraceElement[] trace = (StackTraceElement[]) map.get("trace");
        if (trace != null) {
            setStackTrace(trace);
        }
    }
    
    public Map<?, ?> toMap() {
        String              message = getMessage();
        StackTraceElement[] trace   = getStackTrace();
        return LinkedHashMapOf("message", message,
                               "trace",   trace);
    }
    
    @Override
    public EvaluationException initCause(Throwable cause) {
        super.initCause(cause);
        return this;
    }
    
    public void collectAllMessages(Consumer<? super String> action) {
        Objects.requireNonNull(action, "action");
        action.accept(this.getMessage());
        
        for (Throwable s : this.getSuppressed()) {
            if (s instanceof EvaluationException)
                ((EvaluationException) s).collectAllMessages(action);
            else {
                action.accept(s.getMessage());
            }
        }
    }
    
    public List<String> getAllMessages() {
        List<String> list = new ArrayList<>(1);
        this.collectAllMessages(list::add);
        return list;
    }
    
    static EvaluationException fmt(String format, Object... args) {
        return new EvaluationException(String.format(format, args));
    }
    
    static EvaluationException unexpected(Token token) {
        return fmt("internal error; unexpected %s token: %s", token.getTokenKind().toLowerCase(), token.getSource());
    }
    
    static EvaluationException unresolved(Token token) {
        if (token.isUnresolved()) {
            String reason = ((Token.OfUnresolved) token).getReason();
            if (reason != null) {
                return new EvaluationException(reason);
            }
        }
        return unresolvedToken(token.getSource().toString());
    }
    
    static EvaluationException unresolvedToken(String token) {
        return new EvaluationException("unresolved token " + token);
    }
    
    static EvaluationException unresolved(Operator op) {
        return new EvaluationException("unresolved " + op.getKindDescription() + " " + op.getName());
    }
    
    static EvaluationException unresolved(Symbol sym) {
        return new EvaluationException("unresolved " + sym.getKindDescription() + " " + sym.getName());
    }
    
    static EvaluationException unresolvedOperator(String token) {
        return new EvaluationException("unresolved operator " + token);
    }
    
    static EvaluationException missingLeftSpan(SpanOp op) {
        return new EvaluationException("missing left span " + op.getLeft().getName());
    }
    
    static EvaluationException missingRightSpan(SpanOp op) {
        return new EvaluationException("missing right span " + op.getRight().getName());
    }
    
    static EvaluationException emptySpan(SpanOp op) {
        return new EvaluationException("empty span " + op.toBothString());
    }
}