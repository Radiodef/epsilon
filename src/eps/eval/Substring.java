/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.json.*;

import java.util.*;

@JsonSerializable
public final class Substring implements CharSequence {
    @JsonProperty
    private final String source;
    @JsonProperty
    private final String chars;
    @JsonProperty
    private final int srcStart;
    @JsonProperty
    private final int srcEnd;
    
    @JsonConstructor
    private Substring(String source, String chars, int srcStart, int srcEnd) {
        this.source   = Objects.requireNonNull(source, "source");
        this.chars    = Objects.requireNonNull(chars,  "chars");
        this.srcStart = srcStart;
        this.srcEnd   = srcEnd;
    }
    
    public static Substring snip(String chars, int start, int end) {
        return new Substring(chars, chars.substring(start, end), start, end);
    }
    
    public static Substring codeAt(String chars, int i) {
        int code = chars.codePointAt(i);
        return new Substring(chars, new String(Character.toChars(code)), i, i + Character.charCount(code));
    }
    
    public Substring cat(Substring that) {
        assert this.source.equals(that.source) : this + " cat " + that;
        return new Substring(this.source, this.chars + that.chars, this.srcStart, that.srcEnd);
    }
    
    public boolean inside(Substring that) {
        return that.srcStart <= this.srcStart && this.srcEnd <= that.srcEnd;
    }
    
    public Substring interpolate(Substring that) {
        if (this.inside(that)) return that;
        if (that.inside(this)) return this;
        
        int start = Math.min(this.srcStart, that.srcStart);
        int end   = Math.max(this.srcEnd,   that.srcEnd);
        
        return snip(source, start, end);
    }
    
    public String getFullSource() {
        return source;
    }
    
    public int getSourceStart() {
        return srcStart;
    }
    
    public int getSourceEnd() {
        return srcEnd;
    }
    
    public boolean subEquals(CharSequence seq) {
        return subEquals(seq, 0);
    }
    
    public boolean subEquals(CharSequence seq, int seqOffset) {
        String source = this.source;
        int    start  = this.srcStart;
        int    end    = this.srcEnd;
        
        if (source != seq) {
            if ((end + seqOffset) > seq.length()) {
                return false;
            }
            if ((start + seqOffset) < 0) {
                return false;
            }
        } else {
            if (seqOffset == 0) {
                return true;
            }
        }
        
        for (; start < end; ++start) {
            if (source.charAt(start) != seq.charAt(start + seqOffset)) {
                return false;
            }
        }
        
        return true;
    }
    
    public boolean contentEquals(CharSequence seq) {
        return chars.contentEquals(seq);
    }
    
    public boolean contentEquals(int code) {
        String chars = this.chars;
        int    len   = chars.length();
        if (len == 0)
            return false;
        int code0 = chars.codePointAt(0);
        if (code0 != code)
            return false;
        return len == Character.charCount(code);
    }
    
    @Override
    public int length() {
        return chars.length();
    }
    
    @Override
    public char charAt(int i) {
        return chars.charAt(i);
    }
    
    public int codePointAt(int i) {
        return chars.codePointAt(i);
    }
    
    @Override
    public Substring subSequence(int start, int end) {
        String chars = this.chars;
        
        if (start == 0 && end == chars.length()) {
            return this;
        }
        
        chars = chars.substring(start, end);
        
        int srcStart = this.srcStart;
        // This is probably bad if it gets called on a Substring that is
        // a concatenation of two Substrings which were not adjacent.
        assert (srcEnd - srcStart) == this.chars.length() : toDebugString();
        
        return new Substring(source, chars, srcStart + start, srcStart + end);
    }
    
    @Override
    public String toString() {
        return chars;
    }
    
    private static final ToString<Substring> TO_DEBUG_STRING =
        ToString.builder(Substring.class)
                .add("source",   Substring::getFullSource)
                .add("chars",    Substring::toString)
                .add("srcStart", Substring::getSourceStart)
                .add("srcEnd",   Substring::getSourceEnd)
                .build();
    
    public String toDebugString() {
        return TO_DEBUG_STRING.apply(this);
    }
    
    @Override
    public int hashCode() {
        int hash = chars.hashCode();
        hash = 23 * hash + srcStart;
        hash = 23 * hash + srcEnd;
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Substring) {
            Substring that = (Substring) obj;
            return this.chars.equals(that.chars)
                && this.srcStart == that.srcStart
                && this.srcEnd   == that.srcEnd;
        }
        return false;
    }
}