/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.ui.*;
import eps.app.*;
import eps.math.Number;

import java.util.*;
import java.util.function.*;
import java.io.Serializable;

public interface Token {
    enum Kind {
        UNRESOLVED,
        AMBIGUOUS,
//        @Deprecated
//        PARENTHESIS,
        NUMBER,
        STRING_LITERAL,
        EMPTY,
        SYMBOL;
//        @Deprecated
//        PARENTHESIZED_EXPRESSION;
        
        private final String lower = name().toLowerCase().replace('_', ' ');
        
        public String toLowerCase() {
            return lower;
        }
        
        public boolean isUnresolved()    { return this == UNRESOLVED;     }
        public boolean isAmbiguous()     { return this == AMBIGUOUS;      }
//        @Deprecated
//        public boolean isParenthesis()   { return this == PARENTHESIS;    }
        public boolean isNumber()        { return this == NUMBER;         }
        public boolean isStringLiteral() { return this == STRING_LITERAL; }
        public boolean isSymbol()        { return this == SYMBOL;         }
//        @Deprecated
//        public boolean isParenthesizedExpression() { return this == PARENTHESIZED_EXPRESSION; }
    }
    
    Substring getSource();
    
    Token getPrev();
    Token getNext();
    
    Object getValue();
    
    Kind getTokenKind();
    
    Function<Token, Kind> GET_KIND = (Function<Token, Kind> & Serializable) Token::getTokenKind;
    
    default Setting<Style> getStyleSetting() {
        return null;
    }
    
    default boolean isUnresolved()          { return false; }
    default boolean isAmbiguous()           { return false; }
    default boolean isSymbol()              { return false; }
    default boolean isLooseSymbol()         { return isSymbol(); }
    default boolean isOperator()            { return false; }
    default boolean isLooseOperator()       { return isOperator(); }
    default boolean isUnaryOp()             { return false; }
    default boolean isLooseUnaryOp()        { return isUnaryOp(); }
    default boolean isUnaryPrefix()         { return false; }
    default boolean isLooseUnaryPrefix()    { return isUnaryPrefix(); }
    default boolean isUnarySuffix()         { return false; }
    default boolean isLooseUnarySuffix()    { return isUnarySuffix(); }
    default boolean isBinaryOp()            { return false; }
    default boolean isLooseBinaryOp()       { return isBinaryOp(); }
    default boolean isNaryOp()              { return false; }
    default boolean isLooseNaryOp()         { return isNaryOp(); }
    default boolean isTerminal()            { return false; }
    default boolean isOperand()             { return false; }
    default boolean isLooseOperand()        { return isOperand(); }
//    @Deprecated
//    default boolean isParenthesis()         { return false; }
//    @Deprecated
//    default boolean isLeftParenthesis()     { return false; }
//    @Deprecated
//    default boolean isRightParenthesis()    { return false; }
    
    default boolean isSpanOp()             { return false; }
    default boolean isLooseSpanOp()        { return isSpanOp(); }
    default boolean isLeftSpan()           { return false; }
    default boolean isLooseLeftSpan()      { return isLeftSpan(); }
    default boolean isRightSpan()          { return false; }
    default boolean isLooseRightSpan()     { return isRightSpan(); }
    default boolean isSymmetricSpan()      { return false; }
    default boolean isLooseSymmetricSpan() { return isSymmetricSpan(); }
    
    default boolean isSymbol     (boolean strict) { return strict ? isSymbol()       : isLooseSymbol();      }
    default boolean isOperand    (boolean strict) { return strict ? isOperand()      : isLooseOperand();     }
    default boolean isOperator   (boolean strict) { return strict ? isOperator()     : isLooseOperator();    }
    default boolean isUnaryOp    (boolean strict) { return strict ? isUnaryOp()      : isLooseUnaryOp();     }
    default boolean isUnaryPrefix(boolean strict) { return strict ? isUnaryPrefix()  : isLooseUnaryPrefix(); }
    default boolean isUnarySuffix(boolean strict) { return strict ? isUnarySuffix()  : isLooseUnarySuffix(); }
    default boolean isBinaryOp   (boolean strict) { return strict ? isBinaryOp()     : isLooseBinaryOp();    }
    default boolean isNaryOp     (boolean strict) { return strict ? isNaryOp()       : isLooseNaryOp();      }
    
    default boolean isSpanOp       (boolean strict) { return strict ? isSpanOp()        : isLooseSpanOp();        }
    default boolean isLeftSpan     (boolean strict) { return strict ? isLeftSpan()      : isLooseLeftSpan();      }
    default boolean isRightSpan    (boolean strict) { return strict ? isRightSpan()     : isLooseRightSpan();     }
    default boolean isSymmetricSpan(boolean strict) { return strict ? isSymmetricSpan() : isLooseSymmetricSpan(); }
    
    default boolean isEmpty() {
        return false;
    }
    
//    @Deprecated
//    default boolean isParenthesizedExpression() { return false; }
    
    default String toDebugString() {
        return getTokenKind() + "(" + getSource() + ")";
    }
    
    abstract class Abstract implements Token {
        private final Substring source;
        
        private volatile Token prev;
        private volatile Token next;
        
        Abstract(Substring source) {
            this.source = Objects.requireNonNull(source, "source");
        }
        
        Abstract(Token toCopy) {
            this.source = toCopy.getSource();
            Token prev  = toCopy.getPrev();
            Token next  = toCopy.getNext();
            if (prev != null) {
                this.prev = prev;
                if (prev instanceof Abstract) {
                    ((Abstract) prev).setNext(this);
                }
            }
            if (next != null) {
                this.next = next;
                if (next instanceof Abstract) {
                    ((Abstract) next).setPrev(this);
                }
            }
        }
        
        Abstract setPrev(Token prev) {
            this.prev = prev;
            return this;
        }
        
        @Override
        public Token getPrev() {
            return prev;
        }
        
        Abstract setNext(Token next) {
            this.next = next;
            return this;
        }
        
        @Override
        public Token getNext() {
            return next;
        }
        
        @Override
        public Substring getSource() {
            return source;
        }
        
        @Override
        public String toString() {
            return source.toString();
        }
    }
    
    final class OfEmpty extends Abstract {
        OfEmpty(Substring before) {
            super(Substring.snip(before.getFullSource(), before.getSourceEnd(), before.getSourceEnd()));
        }
        
        OfEmpty(Token before) {
            this(before.getSource());
        }
        
        @Override
        public Kind getTokenKind() {
            return Kind.EMPTY;
        }
        
        @Override
        public Empty getValue() {
            return Empty.INSTANCE;
        }
        
        @Override
        public boolean isOperand() {
            return true;
        }
        
        @Override
        public boolean isEmpty() {
            return true;
        }
    }
    
    final class OfUnresolved extends Abstract {
        private final String reason;
        
        OfUnresolved(Substring source) {
            super(source);
            this.reason = null;
        }
        
        OfUnresolved(Token toCopy, String reason) {
            super(toCopy);
            this.reason = reason;
        }
        
        @Override
        public Substring getValue() {
            return this.getSource();
        }
        
        @Override
        public Kind getTokenKind() {
            return Kind.UNRESOLVED;
        }
        
        public boolean hasReason() {
            return reason != null;
        }
        
        public String getReason() {
            return reason;
        }
        
        @Override
        public boolean isUnresolved() {
            return true;
        }
        
        @Override
        public boolean isLooseSymbol() {
            return true;
        }
        
        @Override
        public boolean isLooseOperand() {
            return true;
        }
        
        @Override
        public boolean isLooseOperator() {
            return true;
        }
        
        // TODO: other loose tests?
    }
    
    final class OfAmbiguous extends Abstract {
        private final Symbols.Group group;
        
        OfAmbiguous(Substring source, Symbols.Group group) {
            super(source);
            this.group = Objects.requireNonNull(group, "group");
        }
        
        @Override
        public Object getValue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Kind getTokenKind() {
            return Kind.AMBIGUOUS;
        }
        
        @Override
        public boolean isAmbiguous() {
            return true;
        }
        
        public Symbols.Group getGroup() {
            return group;
        }
        
        public Symbol getSymbol(Symbol.Kind kind) {
            return group.get(kind);
        }
        
        @Override
        public boolean isLooseSymbol() {
            return isLooseOperator() || isLooseVariable();
        }
        
        public boolean isLooseVariable() {
            return group.contains(Symbol.Kind.VARIABLE);
        }
        
        @Override
        public boolean isLooseOperator() {
            Symbols.Group group = this.group;
            return group.contains(Symbol.Kind.UNARY_OP)
                || group.contains(Symbol.Kind.BINARY_OP)
                || group.contains(Symbol.Kind.SPAN_OP)
                || group.contains(Symbol.Kind.NARY_OP);
        }
        
        @Override
        public boolean isLooseUnaryOp() {
            return group.contains(Symbol.Kind.UNARY_OP);
        }
        
        @Override
        public boolean isLooseUnaryPrefix() {
            Symbol sym = group.get(Symbol.Kind.UNARY_OP);
            return (sym != null) && ((UnaryOp) sym).isPrefix();
        }
        
        @Override
        public boolean isLooseUnarySuffix() {
            Symbol sym = group.get(Symbol.Kind.UNARY_OP);
            return (sym != null) && ((UnaryOp) sym).isPrefix();
        }
        
        @Override
        public boolean isLooseBinaryOp() {
            return group.contains(Symbol.Kind.BINARY_OP);
        }
        
        @Override
        public boolean isLooseNaryOp() {
            return group.contains(Symbol.Kind.NARY_OP);
        }
        
        @Override
        public boolean isLooseOperand() {
            return group.contains(Symbol.Kind.VARIABLE); // || group.contains(Symbol.Kind.CONSTANT);
        }
        
        @Override
        public boolean isLooseSpanOp() {
            return group.contains(Symbol.Kind.SPAN_OP);
        }
        
        private SpanOp getSpan() {
            return (SpanOp) group.get(Symbol.Kind.SPAN_OP);
        }
        
        @Override
        public boolean isLooseLeftSpan() {
            SpanOp span = getSpan();
            return span != null && span.isLeft();
        }
        
        @Override
        public boolean isLooseRightSpan() {
            SpanOp span = getSpan();
            return span != null && span.isRight();
        }
        
        @Override
        public boolean isLooseSymmetricSpan() {
            SpanOp span = getSpan();
            return span != null && span.isSymmetric();
        }
    }
    
//    @Deprecated
//    final class OfParenthesis extends Abstract {
//        private final boolean isLeft;
//        
//        OfParenthesis(Substring source, boolean isLeft) {
//            super(source);
//            this.isLeft = isLeft;
//        }
//        
//        @Override
//        public Object getValue() {
//            throw new UnsupportedOperationException();
//        }
//        
//        @Override
//        public Kind getTokenKind() {
//            return Kind.PARENTHESIS;
//        }
//        
//        @Override
//        public boolean isParenthesis() {
//            return true;
//        }
//        
//        @Override
//        public boolean isLeftParenthesis() {
//            return isLeft;
//        }
//        
//        @Override
//        public boolean isRightParenthesis() {
//            return !isLeft;
//        }
//    }
    
    final class OfNumber extends Abstract {
        private final Number number;
        
        OfNumber(Substring source, Number number) {
            super(source);
            this.number = Objects.requireNonNull(number, "number");
        }
        
        @Override
        public Kind getTokenKind() {
            return Kind.NUMBER;
        }
        
        @Override
        public Number getValue() {
            return number;
        }
        
        @Override
        public boolean isOperand() {
            return true;
        }
        
        @Override
        public Setting<Style> getStyleSetting() {
            return Setting.NUMBER_STYLE;
        }
    }
    
    final class OfStringLiteral extends Abstract {
        private final StringLiteral str;
        
        OfStringLiteral(Substring source) {
            super(source);
            this.str = new StringLiteral(source.toString());
        }
        
        @Override
        public Kind getTokenKind() {
            return Kind.STRING_LITERAL;
        }
        
        @Override
        public StringLiteral getValue() {
            return str;
        }
        
        @Override
        public boolean isOperand() {
            return true;
        }
        
        @Override
        public Setting<Style> getStyleSetting() {
            return Setting.LITERAL_STYLE;
        }
    }
    
    final class OfSymbol extends Abstract {
        private final Symbol  symbol;
        private final boolean isTerminal;
        
        OfSymbol(Token toCopy, Symbol symbol) {
            this(toCopy, symbol, true);
        }
        
//        OfSymbol(Token toCopyL, Token toCopyR, Symbol symbol, boolean isTerminal) {
//            this(toCopyL, symbol, isTerminal);
//            append(toCopyR);
//        }
        
        OfSymbol(Substring source, Symbol symbol) {
            this(source, symbol, true);
        }
        
        OfSymbol(Substring source, Symbol symbol, boolean isTerminal) {
            super(source);
            this.symbol     = Objects.requireNonNull(symbol, "symbol");
            this.isTerminal = isTerminal;
        }
        
//        OfSymbol(Token toCopy, Symbol symbol, boolean isTerminal) {
//            this(toCopy.getSource(), symbol, isTerminal);
//        }
        
        OfSymbol(Token toCopy, Symbol symbol, boolean isTerminal) {
            super(toCopy);
            this.symbol     = Objects.requireNonNull(symbol, "symbol");
            this.isTerminal = isTerminal;
            if (!isTerminal && !(symbol.isNaryOp() || symbol.isSpanOp())) {
                throw new IllegalArgumentException(String.format("%s is not an NaryOp or SpanOp", symbol));
            }
        }
        
        @Override
        public String toDebugString() {
            String s = getTokenKind() + ":" + symbol.getSymbolKind();
            if (symbol.getSymbolKind().isUnaryOp()) {
                s += ":" + ((UnaryOp) symbol).getAffix().name();
            }
            if (symbol.getSymbolKind().isSpanOp()) {
                SpanOp span = (SpanOp) symbol;
                s += ":" + (span.isSymmetric() ? "SYMMETRIC" : span.isParent() ? "PARENT" : span.isLeft() ? "LEFT" : "RIGHT");
            }
            return s + "(" + getSource() + ")";
        }
        
        @Override
        public Kind getTokenKind() {
            return Kind.SYMBOL;
        }
        
        @Override
        public Symbol getValue() {
            return symbol;
        }
        
        @Override
        public Setting<Style> getStyleSetting() {
            return symbol.getStyleSetting();
        }
        
        @Override
        public boolean isSymbol() {
            return true;
        }
        
        @Override
        public boolean isOperand() {
            return symbol.getSymbolKind().isOperand();
        }
        
        @Override
        public boolean isOperator() {
            return symbol.getSymbolKind().isOperator();
        }
        
        @Override
        public boolean isUnaryOp() {
            return symbol.getSymbolKind().isUnaryOp();
        }
        
        @Override
        public boolean isUnaryPrefix() {
            return isUnaryOp() && ((UnaryOp) symbol).isPrefix();
        }
        
        @Override
        public boolean isUnarySuffix() {
            return isUnaryOp() && ((UnaryOp) symbol).isSuffix();
        }
        
        @Override
        public boolean isBinaryOp() {
            return symbol.getSymbolKind().isBinaryOp();
        }
        
        @Override
        public boolean isNaryOp() {
            return symbol.getSymbolKind().isNaryOp();
        }
        
        @Override
        public boolean isTerminal() {
            return isTerminal;
        }
        
        @Override
        public boolean isSpanOp() {
            return symbol.getSymbolKind().isSpanOp();
        }
        
        @Override
        public boolean isLeftSpan() {
            if (isSpanOp()) {
                SpanOp op = (SpanOp) symbol;
                return op.isSymmetric() ? !isTerminal : op.isLeft();
            }
            return false;
        }
        
        @Override
        public boolean isRightSpan() {
            if (isSpanOp()) {
                SpanOp op = (SpanOp) symbol;
                return op.isSymmetric() ? isTerminal : op.isRight();
            }
            return false;
        }
        
        @Override
        public boolean isSymmetricSpan() {
            return isSpanOp() && ((SpanOp) symbol).isSymmetric();
        }
    }
    
//    @Deprecated
//    final class OfParenthesizedExpression extends Abstract {
//        private final Token value;
//        private final Token left;
//        private final Token right;
//        
//        OfParenthesizedExpression(Token value, Token left, Token right) {
//            super(left.getSource().interpolate(right.getSource()).interpolate(value.getSource()));
//            this.value = Objects.requireNonNull(value, "value");
//            this.left  = Objects.requireNonNull(left,  "left");
//            this.right = Objects.requireNonNull(right, "right");
//            assert left.isLeftParenthesis()   : left;
//            assert right.isRightParenthesis() : right;
//        }
//        
//        @Override
//        public Token getValue() {
//            return value;
//        }
//        
//        public Token getLeft() {
//            return left;
//        }
//        
//        public Token getRight() {
//            return right;
//        }
//        
//        @Override
//        public Kind getTokenKind() {
//            return Kind.PARENTHESIZED_EXPRESSION;
//        }
//        
//        @Override
//        public boolean isParenthesizedExpression() {
//            return true;
//        }
//    }
}