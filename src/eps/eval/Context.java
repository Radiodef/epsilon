/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.math.*;
import eps.util.*;
import eps.job.*;

import java.util.*;
import java.util.function.*;

public class Context implements AutoCloseable, MiniSettings {
    private final    String         source;
    private final    MathEnv        mathEnv;
    private final    Settings       settings;
    private final    Symbols        symbols;
    private volatile Symbols        scope;
    private final    Context        parent;
    private final    List<Runnable> onSuccess;
    private volatile boolean        isClosed   = false;
    private final    Context.Depth  stackDepth;
    
    public Context(String source) {
        this(source, App.app());
    }
    
    public Context(String source, App app) {
        this(source, app.getSettings(), app.getMathEnvironment(), app.getSymbols());
    }
    
    public Context(String source, Settings settings, MathEnv mathEnv, Symbols symbols) {
        this(source, settings, mathEnv, symbols, Symbols.Concurrent.empty(), (Context) null);
    }
    
    private Context(String source, Settings settings, MathEnv mathEnv, Symbols symbols, Symbols scope, Context parent) {
        this.source     = Objects.requireNonNull(source,   "source");
        this.settings   = Objects.requireNonNull(settings, "settings");
        this.mathEnv    = Objects.requireNonNull(mathEnv,  "mathEnv");
        this.symbols    = Objects.requireNonNull(symbols,  "symbols");
        this.scope      = Objects.requireNonNull(scope,    "scope");
        this.parent     = parent;
        this.stackDepth = (parent == null) ? new Context.Depth(settings) : parent.stackDepth;
        this.onSuccess  = Collections.synchronizedList(new ArrayList<>(0));
    }
    
    public String getSource() {
        return source;
    }
    
    public Settings getSettings() {
        return settings;
    }
    
    @Override
    public <T> T getSetting(Setting<T> key) {
        return settings.get(key);
    }
    
    public MathEnv getMathEnvironment() {
        return mathEnv;
    }
    
    public Symbols getSymbols() {
        return symbols;
    }
    
    public Symbols getScope() {
        return scope;
    }
    
    public Symbols setScope(Symbols scope) {
        Symbols prev = this.scope;
        this.scope = Objects.requireNonNull(scope, "scope");
        return prev;
    }
    
    public Context derive(Symbols symbols) {
//        return new Context(this.source, this.settings, this.mathEnv, symbols, this.scope, this);
        return new Context(this.source, this.settings, this.mathEnv, symbols, Symbols.Concurrent.empty(), this);
    }
    
    public Context define(Symbol sym) {
        getScope().put(sym);
        return this;
    }
    
    public Context undefine(Symbol sym) {
        if (!scope.remove(sym)) {
            Log.low(Context.class, "undefine(Symbol)", "undefining ", sym, " from main Symbols");
            onSuccess(() -> symbols.remove(sym));
        }
        return this;
    }
    
    public Context onSuccess(Runnable action) {
        Objects.requireNonNull(action, "action");
        if (parent != null) {
            parent.onSuccess(action);
        } else {
            onSuccess.add(action);
        }
        return this;
    }
    
    public void success() {
        close();
    }
    
    @Override
    public void close() {
        synchronized (onSuccess) {
            if (isClosed) {
                throw new IllegalStateException();
            }
            isClosed = true;
            if (!onSuccess.isEmpty()) {
                onSuccess.forEach(Runnable::run);
                onSuccess.clear();
            }
            if (parent == null) {
                symbols.putAll(scope);
            }
        }
    }
    
    public int getStackDepth() {
        return stackDepth.getAsInt();
    }
    
    public Depth enter() {
        return stackDepth.open();
    }
    
    public static final class Depth implements AutoCloseable, IntSupplier {
        private final int maxDepth;
        // TODO: doesn't really make sense for stack depth
        //       to be shared between threads. think about
        //       the implications of this.
        private volatile int stackDepth = 0;
        
        private Depth(Settings settings) {
            this.maxDepth = settings.get(Setting.MAX_RECURSION_DEPTH);
        }
        
        @Override
        public int getAsInt() {
            return stackDepth;
        }
        
        public Depth open() {
            Worker.executing();
            int stackDepth = this.stackDepth;
            
            if (stackDepth == maxDepth) {
                // TODO: make a better message with context info?
                throw EvaluationException.fmt("maximum recursion depth exceeded (%d)", maxDepth);
            }
            
            this.stackDepth = stackDepth + 1;
            return this;
        }
        
        @Override
        public void close() {
            int stackDepth = this.stackDepth;
            
            if (stackDepth == 0) {
                throw Errors.newIllegalState(stackDepth);
            }
            
            this.stackDepth = stackDepth - 1;
        }
        
        @Override
        public String toString() {
            return "Context.Depth(" + stackDepth + ")";
        }
    }
    
    private static final ToString<Context> TO_STRING =
        ToString.builder(Context.class)
                .add("source",     Context::getSource)
                .add("mathEnv",    Context::getMathEnvironment)
                .add("settings",   Context::getSettings)
                .add("scope",      Context::getScope)
                .add("parent",     context -> context.parent)
                .add("onSuccess",  context -> context.onSuccess)
                .add("isClosed",   context -> context.isClosed)
                .add("stackDepth", Context::getStackDepth)
                .build();
    
    @Override
    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean multiline) {
        return TO_STRING.apply(this, multiline);
    }
    
    private static final ThreadLocal<Context> LOCAL_CONTEXT = new ThreadLocal<>();
    
    public static Context setLocal(Context context) {
        Context prev = LOCAL_CONTEXT.get();
        if (context == null) {
            LOCAL_CONTEXT.remove();
        } else {
            LOCAL_CONTEXT.set(context);
        }
        return prev;
    }
    
    public static Context local() {
        return LOCAL_CONTEXT.get();
    }
}