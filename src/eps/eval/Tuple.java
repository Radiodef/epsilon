/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.block.*;
import eps.util.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

interface TupleCommon extends Operand, Iterable<Operand> {
    int count();
    Operand get(int index);
    
    @Override
    default Kind getOperandKind() {
        return Kind.TUPLE;
    }
}

public final class Tuple implements TupleCommon {
    private final Operand[] elements;
    
    private Tuple() {
        elements = EMPTY_ELEMENTS;
    }
    
    private Tuple(Operand[] elements) {
        this.elements = elements;
    }
    
    @Override
    public Kind getOperandKind() {
        return Kind.TUPLE;
    }
    
    @Override
    public int count() {
        return elements.length;
    }
    
    @Override
    public Operand get(int index) {
        return elements[index];
    }
    
    public Tuple append(Operand op) {
        Objects.requireNonNull(op, "op");
        
        Operand[] elements = this.elements;
        Operand[] copy     = Arrays.copyOf(elements, elements.length + 1);
        copy[elements.length] = op;
        
        return new Tuple(copy);
    }
    
    public Tuple prepend(Operand op) {
        Objects.requireNonNull(op, "op");
        
        Operand[] elements = this.elements;
        Operand[] copy     = new Operand[1 + elements.length];
        System.arraycopy(elements, 0, copy, 1, elements.length);
        copy[0] = op;
        
        return new Tuple(copy);
    }
    
    public Tuple cat(Tuple that) {
        Objects.requireNonNull(that, "that");
        
        Operand[] elemsL = this.elements;
        Operand[] elemsR = that.elements;
        Operand[] copy   = Arrays.copyOf(elemsL, elemsL.length + elemsR.length);
        System.arraycopy(elemsR, 0, copy, elemsL.length, elemsR.length);
        
        return new Tuple(copy);
    }
    
    public Tuple map(Function<? super Operand, ? extends Operand> fn) {
        Objects.requireNonNull(fn, "fn");
        Operand[] elements = this.elements;
        int       count    = elements.length;
        Operand[] copy     = new Operand[count];
        for (int i = 0; i < count; ++i) {
            copy[i] = fn.apply(elements[i]);
        }
        return new Tuple(copy);
    }
    
    @Override
    public boolean isTruthy(Context context) {
        return elements.length != 0; // or stream(elements).allMatch(isTruthy)
    }
    
    @Override
    public String toString() {
        return Arrays.toString(elements);
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context c, AnswerFormatter f, MutableBoolean a) {
        b.append('[');
        
        Operand[] elements = this.elements;
        int       length   = elements.length;
        if (length != 0) {
            elements[0].toResultString(b, c, f, a);
            
            for (int i = 1; i < length; ++i) {
                b.append(',').append(' ');
                elements[i].toResultString(b, c, f, a);
            }
        }
        
        return b.append(']');
    }
    
    @Override
    public Block toResultBlock(StringBuilder temp, Context context, AnswerFormatter f, MutableBoolean a) {
        Operand[] elements = this.elements;
        int       count    = elements.length;
        
        JoinBlock join    = new JoinBlock(2*count + 1);
        Block     comma   = Block.of(", ");
        join.addBlock(Block.of('['));
        
        for (int i = 0; i < count; ++i) {
            if (i != 0) {
                join.addBlock(comma);
            }
            join.addBlock(elements[i].toResultBlock(temp, context, f, a));
        }
        
        join.addBlock(Block.of(']'));
        return join;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(elements);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Tuple) {
            return Arrays.equals(this.elements, ((Tuple) obj).elements);
        }
        return false;
    }
    
    private static final Operand[] EMPTY_ELEMENTS = new Operand[0];
    
    private static final Tuple EMPTY = new Tuple();
    
    public static Tuple empty() {
        return EMPTY;
    }
    
    public static Tuple of(Operand op) {
        Objects.requireNonNull(op, "op");
        return new Tuple(new Operand[] {op});
    }
    
    public static Tuple of(Operand lhs, Operand rhs) {
        Objects.requireNonNull(lhs, "lhs");
        Objects.requireNonNull(rhs, "rhs");
        return new Tuple(new Operand[] {lhs, rhs});
    }
    
    public static Tuple of(Operand... elements) {
        Operand[] copy = Arrays.copyOf(Misc.requireNonNull(elements, "elements"),
                                       elements.length,
                                       Operand[].class);
        return new Tuple(copy);
    }
    
    public static Tuple of(Collection<? extends Operand> elements) {
        Objects.requireNonNull(elements, "elements");
        Operand[] array = elements.toArray(EMPTY_ELEMENTS);
        int       count = array.length;
        for (int i = 0; i < count; ++i) {
            Objects.requireNonNull(array[i], "element");
        }
        return new Tuple(array);
    }
    
    public static Tuple of(eps.math.MathEnv env, long... values) {
        int       count = values.length;
        Operand[] array = new Operand[count];
        for (int i = 0; i < count; ++i) {
            array[i] = env.valueOf(values[i]);
        }
        return new Tuple(array);
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static Builder builder(int initialCapacity) {
        return new Builder(initialCapacity);
    }
    
    public static final class Builder implements TupleCommon, Cloneable {
        private Operand[] elements;
        private int       count;
        
        private Builder() {
            this(8);
        }
        
        private Builder(int initialCapacity) {
            this.elements = new Operand[initialCapacity];
        }
        
        @Override
        public boolean isIntermediate() {
            return true;
        }
        
        @Override
        public boolean isTruthy(Context context) {
            return count != 0; // or stream(elements).allMatch(isTruthy)
        }
        
        @Override
        public int count() {
            return count;
        }
        
        @Override
        public Operand get(int i) {
            return elements[i];
        }
        
        public Builder set(int i, Operand op) {
            Objects.requireNonNull(op, "op");
            elements[i] = op;
            return this;
        }
        
        public Builder append(Operand op) {
            Objects.requireNonNull(op, "op");
            int       count    = this.count;
            Operand[] elements = this.elements;
            
            if (count == elements.length) {
                this.elements = elements = Arrays.copyOf(elements, (3 * (count + 1)) >> 1);
            }
            
            elements[count] = op;
            this.count      = count + 1;
            return this;
        }
        
        public Builder appendAll(Builder that) {
            Objects.requireNonNull(that, "that");
            int       count    = that.count;
            Operand[] elements = that.elements;
            
            for (int i = 0; i < count; ++i) {
                append(elements[i]);
            }
            
            return this;
        }
        
        public Tuple build() {
            Operand[] elements = this.elements;
            int       count    = this.count;
            if (elements.length != count) {
                elements = Arrays.copyOf(elements, count);
            }
            return new Tuple(elements);
        }
        
        public Operand[] toArray() {
            return Arrays.copyOf(elements, count);
        }
        
        @Override
        public Builder clone() {
            try {
                Builder clone = (Builder) super.clone();
                clone.elements = elements.clone();
                return clone;
            } catch (CloneNotSupportedException x) {
                throw new AssertionError(null, x);
            }
        }
        
        @Override
        public String toString() {
            return Arrays.toString(elements);
        }
        
        @Override
        public StringBuilder toResultString(StringBuilder b, Context c, AnswerFormatter f, MutableBoolean a) {
            return build().toResultString(b, c, f, a);
        }
        
        @Override
        public Block toResultBlock(StringBuilder b, Context c, AnswerFormatter f, MutableBoolean a) {
            return build().toResultBlock(b, c, f, a);
        }
        
        @Override
        public Iterator<Operand> iterator() {
            return new TupleIterator(this);
        }
    }
    
    @Override
    public Iterator<Operand> iterator() {
        return new TupleIterator(this);
    }
    
    private static final class TupleIterator implements Iterator<Operand> {
        private final TupleCommon t;
        private int i;
        
        private TupleIterator(TupleCommon t) {
            this.t = Objects.requireNonNull(t, "t");
            this.i = 0;
        }
        
        @Override
        public boolean hasNext() {
            return i < t.count();
        }
        
        @Override
        public Operand next() {
            TupleCommon t = this.t;
            int i = this.i;
            if (i < t.count()) {
                this.i = i + 1;
                return t.get(i);
            }
            throw Errors.newNoSuchElement(i);
        }
    }
    
    private enum TupleCollector implements Collector<Operand, Builder, Tuple> {
        INSTANCE;
        
        @Override
        public Set<Characteristics> characteristics() {
            return Collections.emptySet();
        }
        
        @Override
        public Supplier<Builder> supplier() {
            return Builder::new;
        }
        
        @Override
        public BiConsumer<Builder, Operand> accumulator() {
            return Builder::append;
        }
        
        @Override
        public BinaryOperator<Builder> combiner() {
            return Builder::appendAll;
        }
        
        @Override
        public Function<Builder, Tuple> finisher() {
            return Builder::build;
        }
    }
    
    @SuppressWarnings("unchecked")
    public static <O extends Operand> Collector<O, ?, Tuple> collector() {
        return (Collector<O, ?, Tuple>) TupleCollector.INSTANCE;
    }
}