/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.block.*;
import eps.util.*;

public interface Operand {
    default boolean isIntermediate() {
        return false;
    }
    
    default String toResultString(Context context) {
        return this.toResultString(context, null, null);
    }
    
    default String toResultString(Context context, AnswerFormatter fmt, MutableBoolean approx) {
        return this.toResultString(new StringBuilder(), context, fmt, approx).toString();
    }
    
    StringBuilder toResultString(StringBuilder b, Context context, AnswerFormatter fmt, MutableBoolean approx);
    
    default Block toResultBlock(Context context, AnswerFormatter fmt, MutableBoolean approx) {
        return this.toResultBlock(new StringBuilder(), context, fmt, approx);
    }
    
    Block toResultBlock(StringBuilder b, Context context, AnswerFormatter fmt, MutableBoolean approx);
    
    default Block toSourceBlock(Context context, Node node) {
        return this.toSourceBlock(new StringBuilder(), context, node);
    }
    
    default Block toSourceBlock(StringBuilder temp, Context context, Node node) {
        return new TextBlock(node.getExpressionSource().toString()).setToken(node);
    }
    
    Kind getOperandKind();
    
    default boolean isUndefinition() {
        return this.getOperandKind().isUndefinition();
    }
    
    default boolean isEmpty() {
        return this.getOperandKind().isEmpty();
    }
    
    default boolean isStringLiteral() {
        return this.getOperandKind().isStringLiteral();
    }
    
    default boolean isReference() {
        return this.getOperandKind().isReference();
    }
    
    default boolean isNumber() {
        return this.getOperandKind().isNumber();
    }
    
    default boolean isVariable() {
        return this.getOperandKind().isVariable();
    }
    
    default boolean isTuple() {
        return this.getOperandKind().isTuple();
    }
    
    default boolean isSymbol() {
        return this.getOperandKind().isSymbol();
    }
    
    default Operand evaluate(Context context) {
        return this;
    }
    
    boolean isTruthy(Context context);
    
    enum Kind {
        UNDEFINITION,
        EMPTY,
        STRING_LITERAL("string"),
        REFERENCE,
        NUMBER,
//        @Deprecated
//        CONSTANT,
        VARIABLE,
        TUPLE;
        
        private final String lower;
        
        private Kind() {
            this.lower = name().replace('_', ' ').toLowerCase();
        }
        
        private Kind(String lower) {
            this.lower = java.util.Objects.requireNonNull(lower, "lower");
        }
        
        public String toLowerCase() {
            return lower;
        }
        
        public String toTypeOfString() {
            return lower;
        }
        
        public boolean isReference()     { return this == REFERENCE;      }
        public boolean isUndefinition()  { return this == UNDEFINITION;   }
        public boolean isEmpty()         { return this == EMPTY;          }
        public boolean isStringLiteral() { return this == STRING_LITERAL; }
        public boolean isNumber()        { return this == NUMBER;         }
//        @Deprecated
//        public boolean isConstant()      { return this == CONSTANT;       }
        public boolean isVariable()      { return this == VARIABLE;       }
        public boolean isTuple()         { return this == TUPLE;          }
        
        public boolean isSymbol() {
            return /*this.isConstant() ||*/ this.isVariable();
        }
        
        @Override
        public String toString() {
            return this.toLowerCase();
        }
    }
}