/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.block.*;
import eps.util.*;

public enum Undefinition implements Operand {
    INSTANCE;
    
    public static final Undefinition undefined = INSTANCE;
    
    @Override
    public Operand.Kind getOperandKind() {
        return Operand.Kind.UNDEFINITION;
    }
    
    @Override
    public boolean isUndefinition() {
        return true;
    }
    
    @Override
    public boolean isTruthy(Context context) {
        return false;
    }
    
    @Override
    public String toString() {
        return "undefined";
    }
    
    @Override
    public String toResultString(Context c) {
        return this.toString();
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context c, AnswerFormatter f, MutableBoolean a) {
        return b.append(toString());
    }
    
    @Override
    public Block toResultBlock(StringBuilder temp, Context context, AnswerFormatter f, MutableBoolean a) {
        return new StyleBlock(Block.of(this.toString())).set(Setting.NUMBER_STYLE);
    }
    
    @Override
    public Block toSourceBlock(StringBuilder temp, Context context, Node node) {
        return new StyleBlock(Operand.super.toSourceBlock(temp, context, node)).set(Setting.NUMBER_STYLE);
    }
}