/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.util.*;

import java.util.*;

public interface Computation {
    boolean isComplete();
    boolean hasResult();
    
    Item getItem();
    String getSource();
    Context getContext();
    Tree getTree();
    Operand getResult();
    AnswerFormatter getAnswerFormatter();
    PostComputation getPostComputation();
    List<EvaluationException> getErrors();
    
    static Computation create(Item                     item,
                              Context               context,
                              Tree                     tree,
                              Operand                result,
                              AnswerFormatter  answerFormat,
                              PostComputation      postComp) {
        return create(item, context, tree, result, answerFormat, postComp, (EvaluationException[]) null);
    }
    
    static Computation create(Item                     item,
                              Context               context,
                              Tree                     tree,
                              Operand                result,
                              AnswerFormatter  answerFormat,
                              PostComputation      postComp,
                              EvaluationException... errors) {
        return new CompleteComputation(item, context, tree, result, answerFormat, postComp, errors);
    }
    
    static Computation create(Item                    item,
                              Tree                    tree,
                              boolean            hasResult,
                              AnswerFormatter answerFormat,
                              PostComputation     postComp) {
        return new LazyComputation(item, tree, hasResult, answerFormat, postComp);
    }
}

final class CompleteComputation implements Computation {
    private final Item            item;
    private final Context         context;
    private final Tree            tree;
    private final Operand         result;
    private final AnswerFormatter answerFormat;
    private final PostComputation postComp;
    
    private final List<EvaluationException> errors;
    
    CompleteComputation(Item                     item,
                        Context               context,
                        Tree                     tree,
                        Operand                result,
                        AnswerFormatter  answerFormat,
                        PostComputation      postComp,
                        EvaluationException... errors) {
        this.item    = Objects.requireNonNull(item,    "item");
        this.context = Objects.requireNonNull(context, "context");
        this.tree    = Objects.requireNonNull(tree,    "tree");
        this.result  = result; // may be null
        
        this.answerFormat = AnswerFormatter.replnull(answerFormat);
        this.postComp     = PostComputation.replnull(postComp);
        
        if (errors == null || errors.length == 0) {
            this.errors = Collections.emptyList();
        } else {
            int count = errors.length;
            List<EvaluationException> list = new ArrayList<>(count);
            
            for (int i = 0; i < count; ++i) {
                list.add(Objects.requireNonNull(errors[i], "element in errors"));
            }
            
            this.errors = Collections.unmodifiableList(list);
        }
    }
    
    @Override
    public boolean isComplete() {
        return true;
    }
    
    @Override
    public Item getItem() {
        return this.item;
    }
    
    @Override
    public Context getContext() {
        return this.context;
    }
    
    @Override
    public String getSource() {
        return this.getContext().getSource();
    }
    
    @Override
    public Tree getTree() {
        return this.tree;
    }
    
    @Override
    public boolean hasResult() {
        return this.result != null;
    }
    
    @Override
    public Operand getResult() {
        return this.result;
    }
    
    @Override
    public AnswerFormatter getAnswerFormatter() {
        return answerFormat;
    }
    
    @Override
    public PostComputation getPostComputation() {
        return postComp;
    }
    
    @Override
    public List<EvaluationException> getErrors() {
        return this.errors;
    }
    
    private static final ToString<CompleteComputation> TO_STRING =
        ToString.builder(CompleteComputation.class)
                .add("source",       Computation::getSource)
                .add("result",       Computation::getResult)
                .add("answerFormat", Computation::getAnswerFormatter)
                .add("postComp",     Computation::getPostComputation)
                .add("errors",       Computation::getErrors)
                .build();
    
    @Override
    public String toString() {
        return TO_STRING.apply(this);
    }
}

final class LazyComputation implements Computation {
    private final Item            item;
    private final Tree            tree;
    private final boolean         hasResult;
    private final AnswerFormatter answerFormat;
    private final PostComputation postComp;
    
    private volatile boolean isComplete = false;
    
    private volatile Context context = null;
    private volatile Operand result  = null;
    
    private volatile List<EvaluationException> errors = null;
    
    LazyComputation(Item                    item,
                    Tree                    tree,
                    boolean            hasResult,
                    AnswerFormatter answerFormat,
                    PostComputation     postComp) {
        this.item         = Objects.requireNonNull(item, "item");
        this.tree         = Objects.requireNonNull(tree, "tree");
        this.hasResult    = hasResult;
        this.answerFormat = AnswerFormatter.replnull(answerFormat);
        this.postComp     = PostComputation.replnull(postComp);
    }
    
    @Override
    public boolean isComplete() {
        return isComplete;
    }
    
    @Override
    public String getSource() {
        return tree.getSource();
    }
    
    @Override
    public Item getItem() {
        return item;
    }
    
    @Override
    public Tree getTree() {
        return tree;
    }
    
    @Override
    public Context getContext() {
        rebuild();
        return context;
    }
    
    @Override
    public boolean hasResult() {
        return hasResult;
    }
    
    @Override
    public Operand getResult() {
        rebuild();
        return result;
    }
    
    @Override
    public AnswerFormatter getAnswerFormatter() {
        return answerFormat;
    }
    
    @Override
    public PostComputation getPostComputation() {
        return postComp;
    }
    
    @Override
    public List<EvaluationException> getErrors() {
        rebuild();
        return errors;
    }
    
    private synchronized void rebuild() {
        if (isComplete)
            return;
        
        Tree    tree    = this.tree;
        Context context = tree.createContext();
        Operand result  = null;
        
        List<EvaluationException> errors = Collections.emptyList();
        // Note: this evaluation here can halt indefinitely, hence
        //       we only attempt it if we had a prior result. It
        //       could still potentially halt if e.g. some symbols
        //       are now different, but this reduces the likelihood.
        if (hasResult) {
            Context prev = Context.setLocal(context);
            try {
                result = postComp.apply(tree.evaluate(context), context);
            } catch (EvaluationException x) {
                errors = Collections.singletonList(x);
            } finally {
                Context.setLocal(prev);
            }
        }
        
        this.context    = context;
        this.result     = result;
        this.errors     = errors;
        this.isComplete = true;
    }
    
    private static final ToString<LazyComputation> TO_STRING =
        ToString.builder(LazyComputation.class)
                .add("isComplete",   Computation::isComplete)
                .add("hasResult",    Computation::hasResult)
                .add("source",       Computation::getSource)
                .add("answerFormat", Computation::getAnswerFormatter)
                .add("postComp",     Computation::getPostComputation)
                .add("result",       comp -> comp.result)
                .add("errors",       comp -> comp.errors)
                .build();
    
    @Override
    public String toString() {
        return TO_STRING.apply(this);
    }
}