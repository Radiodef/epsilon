/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.block.*;
import eps.util.*;

import java.util.*;

public final class StringLiteral implements Operand, CharSequence, Comparable<StringLiteral> {
    private final String chars;
    
    public StringLiteral(String chars) {
        this.chars = Objects.requireNonNull(chars, "chars");
    }
    
    public StringLiteral(int code) {
        this.chars = new String(Character.toChars(code));
    }
    
    public StringLiteral(Context context, Operand operand) {
        this.chars = operand.toResultString(context, null, null);
    }
    
    public StringLiteral(Context context, Operand lhs, Operand rhs) {
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(lhs,     "lhs");
        Objects.requireNonNull(rhs,     "rhs");
        this.chars = lhs.toResultString(context, null, null)
                   + rhs.toResultString(context, null, null);
    }
    
    public static final StringLiteral EMPTY = new StringLiteral("");
    
    @Override
    public Operand.Kind getOperandKind() {
        return Operand.Kind.STRING_LITERAL;
    }
    
    @Override
    public boolean isTruthy(Context context) {
        return !this.chars.isEmpty();
    }
    
    @Override
    public int compareTo(StringLiteral that) {
        int lengthL = this.codeLength();
        int lengthR = that.codeLength();
        int length  = Math.min(lengthL, lengthR);
        
        String charsL = this.chars;
        String charsR = that.chars;
        
        for (int i = 0; i < length;) {
            int codeL = charsL.codePointAt(i);
            int codeR = charsR.codePointAt(i);
            int comp  = Integer.compare(codeL, codeR);
            if (comp != 0)
                return comp;
            i += Character.charCount(codeL);
        }
        
        return Integer.compare(lengthL, lengthR);
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context context, AnswerFormatter f, MutableBoolean a) {
        return b.append(this.chars);
    }
    
    @Override
    public Block toResultBlock(StringBuilder temp, Context context, AnswerFormatter f, MutableBoolean a) {
        temp.setLength(0);
        Block block = Block.of(temp.append('"').append(this.chars).append('"'));
        return new StyleBlock(block).set(Setting.LITERAL_STYLE);
    }
    
    @Override
    public Block toSourceBlock(StringBuilder temp, Context context, Node node) {
        Block block = Operand.super.toSourceBlock(temp, context, node);
        return new StyleBlock(block).set(Setting.LITERAL_STYLE);
    }
    
    @Override
    public String toString() {
        return this.chars;
    }
    
    @Override
    public int length() {
        return this.chars.length();
    }
    
    public int codeLength() {
        String chars = this.chars;
        return chars.codePointCount(0, chars.length());
    }
    
    @Override
    public char charAt(int index) {
        return this.chars.charAt(index);
    }
    
    public int codePointAt(int index) {
        return this.chars.codePointAt(index);
    }
    
    public int codePointBefore(int index) {
        return this.chars.codePointBefore(index);
    }
    
    @Override
    public StringLiteral subSequence(int start, int end) {
        return new StringLiteral(this.chars.substring(start, end));
    }
    
    public StringLiteral substring(int start, int end) {
        return this.subSequence(start, end);
    }
    
    public StringLiteral cat(CharSequence that) {
        return new StringLiteral(this.chars + that);
    }
    
    public StringLiteral cat(Context context, Operand that) {
        return new StringLiteral(context, this, that);
    }
    
    @Override
    public int hashCode() {
        return this.chars.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof StringLiteral) && this.chars.equals(((StringLiteral) obj).chars);
    }
}
// STOP!!! GO BACK!!!
/*
triangle(n) := {
    row(i, n) := {
        for (
            {i = 1}: "#",
            {i > 1}: "#" + row(i - 1, n),
            fail("i = " + i + ", n = " + n)
        )
    };
    rows(i, n) := {
        for (
            {i = n}: row(n, n),
            {0 < i and i < n}: row(i, n) + "
"            + rows(i + 1, n),
            fail("i = " + i + ", n = " + n)
        )
    };
    "
"    + rows(1, n)
}
*/
/*
reverse(s) := {
    s := s + "";
    for {
        {(sizeof s) <= 1}:
            s,
        f(s, i) := for {
            {i < (sizeof s)}:
                f(s, i + 1) + (s sub i),
            ""
        };
        f(s, 0)
    }
}
*/
/*
index_of(s, c) := {
    s := s + "";
    for {
        {s = ""}:
            -1,
        f(s, c, i) := for {
            {i = (sizeof s)}:
                -1,
            {(s sub i) = c}:
                i,
            f(s, c, i + 1)
        };
        f(s, c, 0)
    }
}
*/