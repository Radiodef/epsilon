/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;

import java.util.*;
import java.util.function.*;

public final class Trees {
    private Trees() {
    }
    
    public static boolean isAliasForward(Context c, Node lhs, Node rhs) {
        lhs = lhs.getExpressionNode();
        rhs = rhs.getExpressionNode();
        
        if (lhs.isUnaryOp() && rhs.isUnaryOp()) {
            return Trees.isAliasForward(c, (Node.OfUnaryOp) lhs, (Node.OfUnaryOp) rhs);
        }
        if (lhs.isSpanOp() && rhs.isSpanOp()) {
            return Trees.isAliasForward(c, (Node.OfSpanOp) lhs, (Node.OfSpanOp) rhs);
        }
        // Technically in the next two cases, only the right-hand needs to be
        // of the correct type of operator.
        // We could have something like (a + ...) := (a, ...) which should be
        // a valid alias creation for the + symbol to act like a comma, but
        // the left-hand side will be interpreted as a binary operator and the
        // right-hand side will be interpreted as an n-ary operator.
        if (lhs.isBinaryOp() || lhs.isNaryOp()) {
            if (rhs.isBinaryOp()) {
                return Trees.isAliasForward(c, (Node.OfOperator) lhs, (Node.OfBinaryOp) rhs);
            }
            if (rhs.isNaryOp()) {
                return Trees.isAliasForward(c, (Node.OfOperator) lhs, (Node.OfNaryOp) rhs);
            }
        }
        
        return false;
    }
    
    public static boolean isAliasForward(Context c, Node.OfUnaryOp lhs, Node.OfUnaryOp rhs) {
        return Trees.isSameVariable(lhs.getOperand(), rhs.getOperand());
    }
    
    public static boolean isAliasForward(Context c, Node.OfOperator lhs, Node.OfBinaryOp rhs) {
        List<Node> opsL = lhs.getOperands();
        if (opsL.size() != 2) {
            return false;
        }
        
        Node lhsL = opsL.get(0);
        Node rhsL = opsL.get(1);
        Node lhsR = rhs.getLeftHandSide();
        Node rhsR = rhs.getRightHandSide();
        // We have to filter out the case where either side is a binary
        // operator in n-ary form. In that case it may very well be intended
        // as a forward, but we can't create an alias.
        if (SpecialSyms.isEllipse(c, rhsL) || SpecialSyms.isEllipse(c, rhsR)) {
            return false;
        }
        
        return Trees.isSameVariable(lhsL, lhsR) && Trees.isSameVariable(rhsL, rhsR);
    }
    
    public static boolean isAliasForward(Context c, Node.OfOperator lhs, Node.OfNaryOp rhs) {
        List<Node> opsL = lhs.getOperands();
        List<Node> opsR = rhs.getOperands();
        
        if (opsL.size() != 2 || opsR.size() != 2) {
            return false;
        }
        
        if (!Trees.isSameVariable(opsL.get(0), opsR.get(0))) {
            return false;
        }
        
        // We have to filter out the case where either side is an n-ary
        // operator in binary form. In that case it may very well be intended
        // as a forward, but we can't create an alias.
        return SpecialSyms.isEllipse(c, opsL.get(1)) && SpecialSyms.isEllipse(c, opsR.get(1));
    }
    
    public static boolean isAliasForward(Context c, Node.OfSpanOp lhs, Node.OfSpanOp rhs) {
        return Trees.isSameVariable(lhs.getOperand(), rhs.getOperand());
    }
    
    private static boolean isSameVariable(Node lhs, Node rhs) {
        lhs = lhs.getExpressionNode();
        if (lhs.isOperand()) {
            rhs = rhs.getExpressionNode();
            if (rhs.isOperand()) {
                return Trees.isSameVariable((Node.OfOperand) lhs, (Node.OfOperand) rhs);
            }
        }
        return false;
    }
    
    public static boolean isSameVariable(Node.OfOperand lhs, Node.OfOperand rhs) {
        Operand opL = lhs.get();
        Operand opR = rhs.get();
        
        if (opL.isVariable() && opR.isVariable()) {
            return isSameVariable((Variable) opL, (Variable) opR);
        }
        
        return false;
    }
    
    private static boolean isSameVariable(Variable lhs, Variable rhs) {
        String nameL = lhs.getName();
        String nameR = rhs.getName();
        return nameL.equals(nameR);
    }
    
    private static final Map<Class<? extends Symbol>, Class<? extends Node.OfOperator>> OPERATOR_CLASSES;
    static {
        OPERATOR_CLASSES = new HashMap<>();
        OPERATOR_CLASSES.put(UnaryOp.class,  Node.OfUnaryOp.class);
        OPERATOR_CLASSES.put(BinaryOp.class, Node.OfBinaryOp.class);
        OPERATOR_CLASSES.put(SpanOp.class,   Node.OfSpanOp.class);
        OPERATOR_CLASSES.put(NaryOp.class,   Node.OfNaryOp.class);
    }
    
    public static <S extends Symbol> Optional<S> findFirstSymbol(Node node, Class<S> type) {
        if (type == Variable.class) {
            node = Trees.findFirst0(node, n -> n.isOperand() && ((Node.OfOperand) n).get().isVariable());
            if (node != null) {
                return Optional.of(type.cast( ((Node.OfOperand) node).get() ));
            }
        } else {
            Class<? extends Node.OfOperator> nodeType = OPERATOR_CLASSES.get(type);
            if (nodeType != null) {
                node = Trees.findFirst0(node, nodeType::isInstance);
                if (node != null) {
                    return Optional.of(type.cast( ((Node.OfOperator) node).getOperator() ));
                }
            }
        }
        return Optional.empty();
    }
    
    public static <N extends Node> Optional<N> findFirst(Node node, Class<N> type) {
        node = Trees.findFirst0(node, type::isInstance);
        if (node != null) {
            return Optional.of(type.cast(node));
        } else {
            return Optional.empty();
        }
    }
    
    public static Optional<Node> findFirst(Node node, Predicate<? super Node> p) {
        return Optional.ofNullable(Trees.findFirst0(node, p));
    }
    
    private static Node findFirst0(Node node, Predicate<? super Node> p) {
        if (p.test(node)) {
            return node;
        }
        switch (node.getNodeKind()) {
            case UNARY_OP:
            case BINARY_OP:
            case SPAN_OP:
            case NARY_OP:
                for (Node op : ((Node.OfOperator) node).getOperands()) {
                    Node found = Trees.findFirst0(op, p);
                    if (found != null) {
                        return found;
                    }
                }
                break;
            case OPERAND:
            default:
                break;
        }
        
        return null;
    }
    
    public static String getBody(Symbol sym) {
        return getTreeFunction(sym).getBody();
    }
    
    public static Substring getExpressionSource(Symbol sym) {
        return getTreeFunction(sym).getExpressionSource();
    }
    
    public static Substring getAssignmentSource(Symbol sym) {
        return getAssignment(sym).getExpressionSource();
    }
    
    public static Node getAssignment(Symbol sym) {
        return getTreeFunction(sym).getAssignment();
    }
    
    public static TreeFunction getTreeFunction(Symbol sym) {
        switch (sym.getSymbolKind()) {
            case VARIABLE:
                return ((Variable.FromTree) sym).getTreeFunction();
            case UNARY_OP:
                return ((UnaryOp.FromTree) sym).getTreeFunction();
            case BINARY_OP:
                return ((BinaryOp.FromTree) sym).getTreeFunction();
            case NARY_OP:
                return ((NaryOp.FromTree) sym).getTreeFunction();
            case SPAN_OP:
                return ((SpanOp.FromTree) sym).getTreeFunction();
            default:
                throw new IllegalArgumentException(sym.toString());
        }
    }
    
    public static Symbol getAliasForward(Symbol sym) {
        Log.entering(Trees.class, "getAliasForward(Symbol)");
        if (sym.isAlias()) {
            return sym.getDelegate();
        }
        try {
            TreeFunction func = getTreeFunction(sym);
            Tree         body = func.getBodyTree();
            Log.note(Trees.class, "getAliasForward", "body = ", body);
            
            if (body != null) {
                Node root = body.getRootIfAvailable();
                Log.note(Trees.class, "getAliasForward", "root = ", root);
                
                if (root != null) {
                    root = root.getExpressionNode();
                    
                    if (root.isOperator()) {
                        if (sym.isOperator()) {
                            return getAliasForward((Operator) sym, func, (Node.OfOperator) root);
                        }
                    } else if (root.isOperand()) {
                        if (sym.isVariable()) {
                            return getAliasForward((Variable) sym, (Node.OfOperand) root);
                        }
                    }
                }
            }
        } catch (ClassCastException | IllegalArgumentException x) {
            Log.caught(Trees.class, "getAliasForward", x, true);
        }
        return null;
    }
    
    private static Operator getAliasForward(Operator op, TreeFunction func, Node.OfOperator node) {
        Log.entering(Trees.class, "getAliasForward(Operator, TreeFunction, Node.OfOperator)");
        Symbol.Kind kind = op.getSymbolKind();
        
        if (kind == node.getOperator().getSymbolKind()) {
            TreeFunction.Parameters params = func.getParameters();
            
            switch (kind) {
                case BINARY_OP: {
                    if (params.count() != 2)
                        break;
                    TreeFunction.Parameter paramL = params.get(0);
                    TreeFunction.Parameter paramR = params.get(1);
                    if (!(paramL instanceof TreeFunction.VariableParameter))
                        break;
                    if (!(paramR instanceof TreeFunction.VariableParameter))
                        break;
                    Variable varL = ((TreeFunction.VariableParameter) paramL).getVariable();
                    Variable varR = ((TreeFunction.VariableParameter) paramR).getVariable();
                    
                    Node nodeL = node.getOperands().get(0).getExpressionNode();
                    Node nodeR = node.getOperands().get(1).getExpressionNode();
                    if (!nodeL.isOperand() || !nodeR.isOperand())
                        break;
                    Operand lhs = ((Node.OfOperand) nodeL).get();
                    Operand rhs = ((Node.OfOperand) nodeR).get();
                    if (!lhs.isVariable() || !rhs.isVariable())
                        break;
                    
                    if (isSameVariable(varL, (Variable) lhs) && isSameVariable(varR, (Variable) rhs))
                        return node.getOperator();
                    break;
                }
                case SPAN_OP: {
                    if (params.count() != 1)
                        break;
                    TreeFunction.Parameter param0 = params.get(0);
                    if (!(param0 instanceof TreeFunction.VariableParameter))
                        break;
                    Variable var0 = ((TreeFunction.VariableParameter) param0).getVariable();
                    
                    Node node0 = node.getOperands().get(0).getExpressionNode();
                    if (!node0.isOperand())
                        break;
                    Operand op0 = ((Node.OfOperand) node0).get();
                    if (!op0.isVariable())
                        break;
                    
                    if (isSameVariable(var0, (Variable) op0))
                        return node.getOperator();
                    break;
                }
                case NARY_OP: {
                    if (params.count() != 1)
                        break;
                    TreeFunction.Parameter param0 = params.get(0);
                    if (!(param0 instanceof TreeFunction.VariableParameter))
                        break;
                    Variable var0 = ((TreeFunction.VariableParameter) param0).getVariable();
                    
                    List<Node> operands = node.getOperands();
                    if (operands.size() != 2)
                        break;
                    Node node0 = operands.get(0).getExpressionNode();
                    if (!node0.isOperand())
                        break;
                    Operand op0 = ((Node.OfOperand) node0).get();
                    if (!op0.isVariable() || !isSameVariable(var0, (Variable) op0))
                        break;
                    
                    Node node1 = operands.get(1).getExpressionNode();
                    if (!node1.isOperand())
                        break;
                    Operand op1 = ((Node.OfOperand) node1).get();
                    if (!op1.isVariable() || ((Variable) op1).getDelegate() != SpecialSyms.ELLIPSE)
                        break;
                    
                    return node.getOperator();
                }
                case UNARY_OP: {
                    if (params.count() != 1)
                        break;
                    TreeFunction.Parameter param0 = params.get(0);
                    
                    int tupleCount = ((UnaryOp) node.getOperator()).getTupleCount();
                    
                    Node operands0 = node.getOperands().get(0).getExpressionNode();
                    
                    if (operands0.isOperand()) {
                        if (tupleCount > 0)
                            break;
                        if (param0 instanceof TreeFunction.ParameterGroup) {
                            TreeFunction.ParameterGroup group = (TreeFunction.ParameterGroup) param0;
                            if (group.count() != 1)
                                break;
                            param0 = group.get(0);
                        }
                        if (!(param0 instanceof TreeFunction.VariableParameter))
                            break;
                        Variable var = ((TreeFunction.VariableParameter) param0).getVariable();
                        
                        Operand operand = ((Node.OfOperand) operands0).get();
                        if (!operand.isVariable())
                            break;
                        
                        if (isSameVariable(var, (Variable) operand))
                            return node.getOperator();
                        
                    } else if (operands0.isNaryOp()) {
                        if (!(param0 instanceof TreeFunction.ParameterGroup))
                            break;
                        TreeFunction.ParameterGroup group = (TreeFunction.ParameterGroup) param0;
                        
                        Node.OfNaryOp nary0 = (Node.OfNaryOp) operands0;
                        
                        if (nary0.getOperator().getDelegate() != NaryOp.TUPLE)
                            break;
                        
                        List<Node> operands = nary0.getOperands();
                        int        count    = operands.size();
                        if (count != group.count())
                            break;
                        if (count != tupleCount && tupleCount >= 0)
                            break;
                        
                        forEachParam: {
                            for (int i = 0; i < count; ++i) {
                                TreeFunction.Parameter param_i = group.get(i);
                                if (!(param_i instanceof TreeFunction.VariableParameter))
                                    break forEachParam;
                                Variable var_i  = ((TreeFunction.VariableParameter) param_i).getVariable();
                                Node     node_i = operands.get(i).getExpressionNode();
                                if (!node_i.isOperand())
                                    break forEachParam;
                                Operand operand_i = ((Node.OfOperand) node_i).get();
                                if (!operand_i.isVariable())
                                    break forEachParam;
                                if (!isSameVariable(var_i, (Variable) operand_i))
                                    break forEachParam;
                            }
                            
                            return node.getOperator();
                        }
                    }
                    
                    break;
                }
                default:
                    assert false : "kind = " + kind + ", op = " + op;
                    break;
            }
        }
        
        return null;
    }
    
    private static Variable getAliasForward(Variable var, Node.OfOperand node) {
        Log.entering(Trees.class, "getAliasForward(Variable, Node.OfOperand)");
        Operand op = node.get();
        if (op.isVariable()) {
            return (Variable) op;
        }
        return null;
    }
}