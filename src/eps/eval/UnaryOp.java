/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.app.*;
import eps.math.*;
import eps.math.Number;
import eps.math.Integer;
import eps.json.*;
import eps.ui.*;
//import static eps.app.App.*;

import java.util.*;

// TODO: revisit passing tuple to unary function because of stuff like
//       x := (2, 32); log x
//       this should be an error; not the same as log(2, 32).
//       TreeFunction already behaves this way.
@JsonSerializable
public abstract class UnaryOp extends Operator {
    private final Affix affix;
    
    @JsonProperty
    private final int tupleCount;
    @JsonProperty
    private final boolean isFunctionLike;
    
    @JsonDefault("tupleCount")
    private static final int TUPLE_COUNT_DEFAULT = -1;
    @JsonDefault("isFunctionLike")
    private static final boolean IS_FUNCTION_LIKE_DEFAULT = false;
    
    public UnaryOp(Builder builder) {
        super(builder);
        this.affix          = Objects.requireNonNull(builder.affix, "affix");
        this.tupleCount     = builder.tupleCount;
        this.isFunctionLike = builder.isFunctionLike;
    }
    
    @JsonConstructor
    protected UnaryOp(String        name,
                      EchoFormatter echoFormatter,
                      Set<String>   preferredAliases,
                      int           precedence,
                      Associativity associativity,
                      int           tupleCount,
                      boolean       isFunctionLike) {
        super(name, echoFormatter, preferredAliases, precedence, associativity);
        this.affix          = Affix.valueOf(associativity);
        this.tupleCount     = tupleCount;
        this.isFunctionLike = isFunctionLike;
    }
    
    private UnaryOp(String name, EchoFormatter fmt, boolean isFunctionLike, UnaryOp delegate) {
        super(name, fmt, delegate);
        this.tupleCount     = delegate.getTupleCount();
        this.affix          = delegate.getAffix();
        this.isFunctionLike = isFunctionLike;
    }
    
    @Override
    protected EchoFormatter getDefaultEchoFormatter() {
        return EchoFormatter.OF_UNARY_OP;
    }
    
    @Override
    public boolean isEqualTo(Symbol sym) {
        if (super.isEqualTo(sym)) {
            UnaryOp that = (UnaryOp) sym;
            if (this.tupleCount != that.tupleCount)
                return false;
            if (this.affix != that.affix)
                return false;
            if (this.isFunctionLike != that.isFunctionLike)
                return false;
            return true;
        }
        return false;
    }
    
    @Override
    public Kind getSymbolKind() {
        return Kind.UNARY_OP;
    }
    
    @Override
    public String getKindDescription() {
        return affix.isPrefix() ? "unary prefix operator" : "unary suffix operator";
    }
    
    @Override
    public UnaryOp createAlias(String aliasName) {
        UnaryOp del = (UnaryOp) this.getDelegate();
        return new Alias(aliasName, del.getEchoFormatter(), del.isFunctionLike, del);
    }
    
    @Override
    public UnaryOp createAlias(String aliasName, EchoFormatter fmt) {
        return new Alias(aliasName, fmt, (UnaryOp) this.getDelegate());
    }
    
    public UnaryOp createAlias(String aliasName, boolean isFunctionLike) {
        return new Alias(aliasName, isFunctionLike, (UnaryOp) this.getDelegate());
    }
    
    public UnaryOp createAlias(String aliasName, EchoFormatter fmt, boolean isFunctionLike) {
        return new Alias(aliasName, fmt, isFunctionLike, (UnaryOp) this.getDelegate());
    }
    
    public int getTupleCount() {
        return tupleCount;
    }
    
    public boolean tuplesAsForEach() {
        return false;
    }
    
    public enum Affix {
        PREFIX,
        SUFFIX;
        
        public boolean isPrefix() {
            return this == PREFIX;
        }
        public boolean isSuffix() {
            return this == SUFFIX;
        }
        public Associativity getAssociativity() {
            return isPrefix() ? Associativity.RIGHT_TO_LEFT : Associativity.LEFT_TO_RIGHT;
        }
        
        public static Affix valueOf(Associativity a) {
            return (a == PREFIX.getAssociativity()) ? PREFIX : SUFFIX;
        }
    }
    
    public Affix getAffix() {
        return affix;
    }
    
    public boolean isPrefix() {
        return getAffix() == Affix.PREFIX;
    }
    
    public boolean isSuffix() {
        return getAffix() == Affix.SUFFIX;
    }
    
    public boolean isFunctionLike() {
        return this.isFunctionLike;
    }
    
    @Override
    public Setting<Style> getStyleSetting() {
        if (isFunctionLike()) {
            return Setting.FUNCTION_STYLE;
        }
        return super.getStyleSetting();
    }
    
    public Operand apply(Context context, Node operand) {
        return apply(context, operand.evaluate(context));
    }
    
    protected Operand apply(Context context, Operand operand) {
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(operand, "operand");
        
        operand = operand.evaluate(context);
        
        switch (operand.getOperandKind()) {
            case TUPLE:
                int tupleCount = getTupleCount();
                if (tupleCount == 0) {
                    break;
                }
                Tuple t = (Tuple) operand;
                if ((tupleCount > 0) && (tupleCount != t.count())) {
                    throw UnsupportedException.fmt("%s requires %d arguments; found %d", getName(), getTupleCount(), t.count());
                }
                if (tuplesAsForEach()) {
                    return t.map(e -> apply(context, e));
                } else {
                    return apply(context.getMathEnvironment(), t);
                }
                
            default:
                if (operand.isNumber()) {
                    return apply(context.getMathEnvironment(), (Number) operand);
                } else {
                    break;
                }
        }
        
        throw UnsupportedException.operandType(operand, getName());
    }
    
    protected Operand apply(MathEnv env, Number n) {
        throw UnsupportedException.operandType(n, getName());
    }
    
    protected Operand apply(MathEnv env, Tuple t) {
        throw UnsupportedException.operandType(t, getName());
    }
    
    @JsonSerializable
    public static class FromFunction extends UnaryOp {
        @JsonProperty
        private final OpOverload<Number> numberFn;
        @JsonProperty
        private final OpOverload<Tuple> tupleFn;
        @JsonProperty
        private final boolean tuplesAsForEach;
        
        public FromFunction(Builder builder) {
            super(builder);
            
            this.tuplesAsForEach = builder.tuplesAsForEach;
            
            this.numberFn = Objects.requireNonNull(builder.numberFn, "numberFn");
            this.tupleFn  = Objects.requireNonNull(builder.tupleFn,  "tupleFn");
        }
        
        private FromFunction(Builder            builder,
                             OpOverload<Number> numberFn,
                             OpOverload<Tuple>  tupleFn,
                             boolean            tuplesAsForEach) {
            super(builder);
            this.numberFn        = numberFn;
            this.tupleFn         = tupleFn;
            this.tuplesAsForEach = tuplesAsForEach;
        }
        
        @JsonConstructor
        protected FromFunction(String             name,
                               EchoFormatter      echoFormatter,
                               Set<String>        preferredAliases,
                               int                precedence,
                               Associativity      associativity,
                               int                tupleCount,
                               boolean            isFunctionLike,
                               OpOverload<Number> numberFn,
                               OpOverload<Tuple>  tupleFn,
                               boolean            tuplesAsForEach) {
            super(name, echoFormatter, preferredAliases, precedence, associativity, tupleCount, isFunctionLike);
            this.numberFn        = Objects.requireNonNull(numberFn, "numberFn");
            this.tupleFn         = Objects.requireNonNull(tupleFn,  "tupleFn");
            this.tuplesAsForEach = tuplesAsForEach;
        }
        
        @Override
        public FromFunction configure(Symbol.Builder<?, ?> b) {
            return new FromFunction((Builder) b, numberFn, tupleFn, tuplesAsForEach);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                FromFunction that = (FromFunction) sym;
                if (!Objects.equals(this.numberFn, that.numberFn))
                    return false;
                if (!Objects.equals(this.tupleFn, that.tupleFn))
                    return false;
                if (this.tuplesAsForEach != that.tuplesAsForEach)
                    return false;
                return true;
            }
            return false;
        }
        
        @Override
        protected Operand apply(MathEnv env, Number n) {
            return numberFn.apply(this, env, n);
        }
        
        @Override
        protected Operand apply(MathEnv env, Tuple t) {
            return tupleFn.apply(this, env, t);
        }
        
        @Override
        public boolean tuplesAsForEach() {
            return tuplesAsForEach;
        }
    }
    
    @JsonSerializable
    public static class UnaryError extends UnaryOp {
        public UnaryError(Builder builder) {
            super(builder);
        }
        
        @JsonConstructor
        protected UnaryError(String        name,
                             EchoFormatter echoFormatter,
                             Set<String>   preferredAliases,
                             int           precedence,
                             Associativity associativity,
                             int           tupleCount,
                             boolean       isFunctionLike) {
            super(name, echoFormatter, preferredAliases, precedence, associativity, tupleCount, isFunctionLike);
        }
        
        @Override
        public UnaryError configure(Symbol.Builder<?, ?> b) {
            return new UnaryError((Builder) b);
        }
        
        @Override
        public boolean isError() {
            return true;
        }
        
        @Override
        public Operand apply(Context c, Node n) {
            throw EvaluationException.unresolved(this);
        }
        
        @Override
        protected Operand apply(Context c, Operand o) {
            throw EvaluationException.unresolved(this);
        }
        
        @Override
        protected Operand apply(MathEnv e, Number n) {
            throw EvaluationException.unresolved(this);
        }
        
        @Override
        protected Operand apply(MathEnv e, Tuple t) {
            throw EvaluationException.unresolved(this);
        }
    }
    
    public static UnaryError createUnaryError(Token.OfUnresolved u, Affix affix) {
        return createUnaryError(u.getSource().toString(), affix);
    }
    
    public static UnaryError createUnaryError(String name, Affix affix) {
        return createBasicUnary(name, affix).buildError();
    }
    
    public static Builder createBasicUnary(String name, Affix affix) {
        boolean isFunctionLike = isFunctionLike(name);
        return new Builder().setName(name)
                            .setAffix(affix)
                            .setPrecedence(HIGHEST_PRECEDENCE)
                            .setFunctionLike(isFunctionLike)
                            .setEchoFormatter(isFunctionLike ? EchoFormatter.OF_FUNCTION_LIKE : EchoFormatter.OF_UNARY_OP)
                            .anyTuple();
    }
    
    /*
    @JsonSerializable
    public static class FromScope extends UnaryOp {
        public FromScope(Builder builder) {
            super(builder);
        }
        
        @JsonConstructor
        protected FromScope(String        name,
                            EchoFormatter echoFormatter,
                            Set<String>   preferredAliases,
                            int           precedence,
                            Associativity associativity,
                            int           tupleCount,
                            boolean       isFunctionLike) {
            super(name, echoFormatter, preferredAliases, precedence, associativity, tupleCount, isFunctionLike);
        }
        
        private UnaryOp get(Context c) {
            UnaryOp that = get(c.getScope());
            if (that != null) {
                return that;
            }
            throw EvaluationException.unresolved(this);
        }
        
        private UnaryOp get(Symbols syms) {
            Symbols.Group group = syms.get(getName());
            if (group != null) {
                UnaryOp that = (UnaryOp) group.get(Symbol.Kind.UNARY_OP);
                if (that != null) {
                    if (this != that) {
                        return that;
                    }
                    // prevent StackOverflowError
                    throw Errors.newIllegalState(this);
                }
            }
            return null;
        }
        
        @Override
        public Operand apply(Context c, Node n) {
            return get(c).apply(c, n);
        }
        
        @Override
        protected Operand apply(Context c, Operand o) {
            throw new AssertionError(this);
        }
        
        @Override
        protected Operand apply(MathEnv e, Number n) {
            throw new AssertionError(this);
        }
        
        @Override
        protected Operand apply(MathEnv e, Tuple t) {
            throw new AssertionError(this);
        }
    }
    
    public static FromScope createFromScope(Token.OfUnresolved u, Affix affix) {
        return createBasicUnary(u.getSource().toString(), affix).buildFromScope();
    }
    */
    
    @FunctionalInterface
    public interface OpOverload<O extends Operand> {
        Operand apply(UnaryOp self, MathEnv env, O arg);
    }
    @FunctionalInterface
    public interface OpOverloadNoSelf<O extends Operand> extends OpOverload<O> {
        Operand apply(MathEnv env, O arg);
        @Override
        default Operand apply(UnaryOp self, MathEnv env, O arg) {
            return apply(env, arg);
        }
    }
    
    public static class Alias extends UnaryOp {
        private final UnaryOp delegate;
        
        public Alias(String name, UnaryOp delegate) {
            this(name, delegate.isFunctionLike /*UnaryOp.isFunctionLike(name)*/, delegate);
        }
        
        public Alias(String name, EchoFormatter fmt, UnaryOp delegate) {
            this(name, fmt, (fmt != null) ? (fmt instanceof EchoFormatter.OfFunctionLike) : delegate.isFunctionLike, delegate);
        }
        
        public Alias(String name, boolean isFunctionLike, UnaryOp delegate) {
            this(name, isFunctionLike ? EchoFormatter.OF_FUNCTION_LIKE : EchoFormatter.OF_UNARY_OP, isFunctionLike, delegate);
        }
        
        public Alias(String name, EchoFormatter fmt, boolean isFunctionLike, UnaryOp delegate) {
            super(name, fmt, isFunctionLike, Objects.requireNonNull(delegate, "delegate"));
            this.delegate = delegate;
        }
        
        static {
            JsonObjectAdapter.put(UnaryOp.Alias.class,
                                  Symbol::aliasToMap,
                                  map -> (UnaryOp.Alias) Symbol.mapToAlias(map));
        }
        
        @Override
        public Alias configure(Symbol.Builder<?, ?> b) {
            return new Alias(b.getName(), b.getEchoFormatter(), ((Builder) b).isFunctionLike, delegate);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            return super.isEqualTo(sym) && Symbol.isEqual(this.delegate, ((Alias) sym).delegate);
        }
        
        @Override
        public Symbol getDelegate() {
            return delegate.getDelegate();
        }
        
        @Override
        public Operand apply(Context c, Node n) {
            return delegate.apply(c, n);
        }
        
        @Override
        protected Operand apply(Context c, Operand o) {
            return delegate.apply(c, o);
        }
        
        @Override
        protected Operand apply(MathEnv env, Number n) {
            return delegate.apply(env, n);
        }
        
        @Override
        protected Operand apply(MathEnv env, Tuple t) {
            return delegate.apply(env, t);
        }
    }
    
    public static boolean isFunctionLike(String name) {
        IntSet boundaries = Settings.getOrDefault(Setting.BOUNDARY_CHARS_AS_INT);
        int    length     = name.length();
        for (int i = 0; i < length;) {
            int code = name.codePointAt(i);
            if (boundaries.containsAsInt(code))
                return false;
            i += Character.charCount(code);
        }
        return true;
    }
    
    @JsonSerializable
    public static class FromTree extends UnaryOp {
        @JsonProperty(interimConverter = TreeFunction.INTERIM_CONVERTER,
                      deferInterimToConstructor = true)
        private final TreeFunction treeFn;
        
        private static Builder createBuilderForSuperConstructor(Node.OfUnaryOp decl, Node body) {
            UnaryOp u = decl.getOperator();
            return createBuilderForSuperConstructor(u.getName(), u.getAffix());
        }
        
        private static Builder createBuilderForSuperConstructor(String name, Affix affix) {
            boolean  isFunctionLike = /*(affix == Affix.PREFIX) &&*/ UnaryOp.isFunctionLike(name);
            EchoFormatter formatter = isFunctionLike ? EchoFormatter.OF_FUNCTION_LIKE : EchoFormatter.OF_UNARY_OP;
            
            return new Builder().setName(name)
                                .setFunctionLike(isFunctionLike)
                                .setEchoFormatter(formatter)
                                .setAffix(affix)
                                .setPrecedence(FUNCTION_PRECEDENCE) // TODO?
                                ;
        }
        
        public FromTree(Context c, Node.OfUnaryOp decl, Node body) {
            super(createBuilderForSuperConstructor(decl, body));
            
            this.treeFn = new TreeFunction(c, this, decl, body);
        }
        
        public FromTree(Builder builder, TreeAdapter<?, String> params, String body) {
            super(builder);
            
            this.treeFn = new TreeFunction(this, params, body);
        }
        
        @JsonConstructor
        protected FromTree(String        name,
                           EchoFormatter echoFormatter,
                           Set<String>   preferredAliases,
                           int           precedence,
                           Associativity associativity,
                           int           tupleCount,
                           boolean       isFunctionLike,
                           Object        treeFn) {
            super(name, echoFormatter, preferredAliases, precedence, associativity, tupleCount, isFunctionLike);
            this.treeFn = TreeFunction.fromInterimObject(this, treeFn);
        }
        
        private FromTree(Builder b, TreeFunction treeFn) {
            super(b);
            this.treeFn = treeFn.copy(this);
        }
        
        @Override
        public FromTree configure(Symbol.Builder<?, ?> b) {
            return new FromTree((Builder) b, treeFn);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                FromTree that = (FromTree) sym;
                return TreeFunction.isEqual(this.treeFn, that.treeFn);
            }
            return false;
        }
        
        TreeFunction getTreeFunction() {
            return treeFn;
        }
        
        @Override
        public Operand apply(Context c, Node n) {
            return treeFn.evaluate(c, n);
        }
        
        @Override
        protected Operand apply(Context c, Operand o) {
            throw new AssertionError(this);
        }
        
        @Override
        protected Operand apply(MathEnv env, Number n) {
            throw new AssertionError(this);
        }
        
        @Override
        protected Operand apply(MathEnv env, Tuple t) {
            throw new AssertionError(this);
        }
    }
    
    /*
    @Deprecated
    private static class FromTree_old extends UnaryOp {
        private final Tree tree;
        
        private final List<Variable> params;
        
        private static Builder createBuilderForSuperConstructor(Node.OfUnaryOp decl) {
            UnaryOp u = decl.getOperator();
            String  n = u.getName();
            
            IntSet boundaries = app().getSetting(Setting.BOUNDARY_CHARS_AS_INT);
            boolean isFunctionLike =
                n.codePoints().noneMatch(boundaries::containsAsInt);
            
            return new Builder().setName(n)
                                .addPreferredAlias(n)
                                .setEchoFormatter(isFunctionLike ? EchoFormatter.OF_FUNCTION_LIKE : EchoFormatter.OF_UNARY_OP)
                                .setAffix(u.getAffix())
                                .setPrecedence(FUNCTION_PRECEDENCE) // TODO?
                                ;
        }
        
        private static UnsupportedException badParameterDeclaration(Context c, Node n) {
            return UnsupportedException.fmt("invalid function declaration: %s in the parameter(s)", n.getExpressionSource());
        }
        
        private static Variable createParameter(Context c, Node all, Node node) {
            if (node.getNodeKind().isOperand()) {
                Operand val = ((Node.OfOperand) node).get();
                
                if (val.getOperandKind().isVariable()) {
                    return Variable.create((Variable) val);
                }
            }
            throw badParameterDeclaration(c, all);
        }
        
        public FromTree_old(Context c, Node.OfUnaryOp decl, Node body) {
            super(createBuilderForSuperConstructor(decl));
            ArrayList<Variable> params = new ArrayList<>(0);
            
            Node operand = decl.getOperand().getExpressionNode();
            switch (operand.getNodeKind()) {
                case OPERAND:
                    params.add(createParameter(c, operand, operand));
                    break;
                    
                case NARY_OP:
                    Node.OfNaryOp nary = (Node.OfNaryOp) operand;
                    
                    if (nary.getOperator().getDelegate() != NaryOp.TUPLE) {
                        throw badParameterDeclaration(c, nary);
                    }
                    
                    List<Node> operands = nary.getOperands();
                    int        count    = operands.size();
                    params.ensureCapacity(count);
                    for (int i = 0; i < count; ++i) {
                        params.add(createParameter(c, operand, operands.get(i)));
                    }
                    break;
                    
                default:
                    throw badParameterDeclaration(c, operand);
            }
            
            body.forEach(node -> {
                if (node.getNodeKind().isUnaryOp()) {
                    String name = ((Node.OfUnaryOp) node).getOperator().getName();
                    if (name.equals(getName())) {
                        throw UnsupportedException.fmt("found recursive function call of %s", name);
                    }
                }
            });
            
            this.params = params;
            this.tree   = Tree.fromNode(c, body, false); // TODO: are we static? (probably not...)
            
            Log.low(UnaryOp.class, "<init>", "declaring unary operator ", getName(), " with params = ", params);
        }
        
        @Override
        public Operand apply(Context c, Node n) {
            n = n.getExpressionNode();
            
            List<Node> args = new ArrayList<>();
            switch (n.getNodeKind()) {
                case NARY_OP:
                    NaryOp op = ((Node.OfNaryOp) n).getOperator();
                    if (op.getDelegate() != NaryOp.TUPLE) {
                        break;
                    }
                    args.addAll(((Node.OfNaryOp) n).getOperands());
                    break;
                default:
                    args.add(n);
                    break;
            }
            
            int count = args.size();
            if (count == params.size()) {
                Symbols scope = Symbols.Concurrent.empty();
                
                for (int i = 0; i < count; ++i) {
                    String   name = params.get(i).getName();
                    Node     arg  = args.get(i);
                    Operand  val  = arg.evaluate(c);
                    Variable var  = Variable.fromTree(name, c, arg, val);
                    scope.put(var);
                }
                
                Symbols prev = c.setScope(new Symbols.Overlay(scope, c.getSymbols()));
                try {
                    return tree.evaluate(c);
                } finally {
                    c.setScope(prev);
                }
            }
            
            throw UnsupportedException.fmt("operand(s) %s for operator %s (required: %d, found: %d)",
                                           n.evaluate(c), getName(), params.size(), count);
        }
        
        @Override
        public Operand apply(Context c, Operand o) {
            throw UnsupportedException.operandType(o);
        }
        
        @Override
        public Operand apply(MathEnv env, Number n) {
            throw UnsupportedException.operandType(n);
        }
        
        @Override
        public Operand apply(MathEnv env, Tuple t) {
            throw UnsupportedException.operandType(t);
        }
    }
    */
    
    public static class Builder extends Operator.Builder<UnaryOp, Builder> {
        private enum NoNumber implements OpOverload<Number> {
            INSTANCE;
            @Override
            public Operand apply(UnaryOp self, MathEnv env, Number n) {
                throw UnsupportedException.operandType(n, self.getName());
            }
        }
        private enum NoTuple implements OpOverload<Tuple> {
            INSTANCE;
            @Override
            public Operand apply(UnaryOp self, MathEnv env, Tuple t) {
                throw UnsupportedException.operandType(t, self.getName());
            }
        }
        
        private Affix   affix           = Affix.PREFIX;
        private int     tupleCount      = 0;
        private boolean tuplesAsForEach = false;
        private boolean isFunctionLike  = false;
        
        private OpOverload<Number> numberFn = NoNumber.INSTANCE;
        private OpOverload<Tuple>  tupleFn  = NoTuple.INSTANCE;
        
        public Builder() {
            super.setEchoFormatter(EchoFormatter.OF_UNARY_OP)
                 .setPrecedence(Operator.UNARY_NEGATION_PRECEDENCE);
        }
        
        @Override
        public Builder setAll(UnaryOp unary) {
            this.setAffix(unary.getAffix())
                .setTupleCount(unary.getTupleCount())
                .setTuplesAsForEach(unary.tuplesAsForEach())
                .setFunctionLike(unary.isFunctionLike());
            
            if (unary instanceof UnaryOp.FromFunction) {
                this.setNumberFunction(((UnaryOp.FromFunction) unary).numberFn)
                    .setTupleFunction(((UnaryOp.FromFunction) unary).tupleFn);
            }
            
            return super.setAll(unary);
        }
        
        @Override
        public Builder setAssociativity(Associativity a) {
            return this.setAffix(Affix.valueOf(a));
        }
        
        @Override
        public Associativity getAssociativity() {
            return affix.getAssociativity();
        }
        
        public Builder setAffix(Affix affix) {
            this.affix = Objects.requireNonNull(affix, "affix");
            return super.setAssociativity(affix.getAssociativity());
        }
        
        public Builder setNumberFunction(OpOverload<Number> numberFn) {
            this.numberFn = Objects.requireNonNull(numberFn, "numberFn");
            return self();
        }
        
        public Builder setNumberFunction(OpOverloadNoSelf<Number> numberFn) {
            return setNumberFunction((OpOverload<Number>) numberFn);
        }
        
        public Builder setTupleFunction(OpOverload<Tuple> tupleFn) {
            this.tupleFn = Objects.requireNonNull(tupleFn, "tupleFn");
            return self();
        }
        
        public Builder setTupleFunction(OpOverloadNoSelf<Tuple> tupleFn) {
            return setTupleFunction((OpOverload<Tuple>) tupleFn);
        }
        
        public Builder setTupleCount(int tupleCount) {
            this.tupleCount = tupleCount;
            return self();
        }
        
        public Builder anyTuple() {
            return setTupleCount(-1);
        }
        
        public Builder noTuples() {
            return setTupleCount(0);
        }
        
        public Builder setTuplesAsForEach(boolean tuplesAsForEach) {
            this.tuplesAsForEach = tuplesAsForEach;
            return anyTuple();
        }
        
        public Builder setFunctionLike(boolean isFunctionLike) {
            this.isFunctionLike = isFunctionLike;
            return this;
        }
        
        public Builder makeFunction() {
            return setPrecedence(FUNCTION_PRECEDENCE)
                  .setAffix(Affix.PREFIX)
                  .setFunctionLike(true)
                  .setEchoFormatter(EchoFormatter.OF_FUNCTION_LIKE);
        }
        
        @Override
        public UnaryOp build() {
            return new FromFunction(this);
        }
        
        @Override
        public UnaryError buildError() {
            return new UnaryError(this);
        }
        
        /*
        public UnaryOp.FromScope buildFromScope() {
            return new UnaryOp.FromScope(this);
        }
        */
        
        protected static final ToString<Builder> UNARY_OP_BUILDER_TO_STRING =
            OPERATOR_BUILDER_TO_STRING.derive(Builder.class)
                                      .add("affix",           b -> b.affix)
                                      .add("tupleCount",      b -> b.tupleCount)
                                      .add("tuplesAsForEach", b -> b.tuplesAsForEach)
                                      .add("isFunctionLike",  b -> b.isFunctionLike)
                                      .add("numberFn",        b -> b.numberFn)
                                      .add("tupleFn",         b -> b.tupleFn)
                                      .build();
        @Override
        public String toString(boolean multiline) {
            return UNARY_OP_BUILDER_TO_STRING.apply(this, multiline);
        }
    }
    
    public static final UnaryOp NEGATE =
        new Builder().setName("negate")
                     .makeIntrinsic()
                     .addPreferredAlias("-")
                     .setPrecedence(UNARY_NEGATION_PRECEDENCE)
                     .setAffix(Affix.PREFIX)
                     .setNumberFunction((env, n) -> n.negate(env))
                     .setTuplesAsForEach(true)
                     .build();
    
    public static final UnaryOp BOOLEAN_NOT =
        new Builder().setName("not")
                     .makeIntrinsic()
                     .addPreferredAlias("not")
                     .setPrecedence(UNARY_NEGATION_PRECEDENCE)
                     .setAffix(Affix.PREFIX)
                     .setNumberFunction(MathEnv::not)
                     .setTuplesAsForEach(true)
                     .build();
    
    public static final UnaryOp ABS =
        new Builder().setName("abs_fn")
                     .makeIntrinsic()
                     .addPreferredAlias("abs")
                     .makeFunction()
                     .setEchoFormatter(EchoFormatter.ofUnarySpan("|", "|"))
                     .noTuples()
                     .setNumberFunction((env, n) -> n.abs(env))
                     .build();
    
    public static final UnaryOp SIGNUM =
        new Builder().setName("signum")
                     .makeIntrinsic()
                     .addPreferredAlias("sgn")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction((env, n) -> env.valueOf(n.signum()))
                     .build();
    
    public static final UnaryOp FLOOR =
        new Builder().setName("floor")
                     .makeIntrinsic()
                     .addPreferredAlias("floor")
                     .makeFunction()
                     .setNumberFunction((env, n) -> n.floor(env))
                     .setTuplesAsForEach(true)
                     .build();
    
    public static final UnaryOp CEILING =
        new Builder().setName("ceiling")
                     .makeIntrinsic()
                     .addPreferredAlias("ceil")
                     .makeFunction()
                     .setNumberFunction((env, n) -> n.ceil(env))
                     .setTuplesAsForEach(true)
                     .build();
    
    public static final UnaryOp PRIME_AFTER =
        new Builder().setName("prime_after")
                     .makeIntrinsic()
                     .addPreferredAlias("prime_after")
                     .makeFunction()
                     .setNumberFunction((env, n) -> {
                         Integer asInt = n.toInteger(env);
                         if (asInt == null) {
                             n = n.floor(env);
                             asInt = n.toIntegerInexact(env);
                         }
                         return env.primes().getNextAfter(asInt);
                     })
                     .noTuples()
                     .build();
    
    public static final UnaryOp FACTORIAL =
        new Builder().setName("factorial")
                     .makeIntrinsic()
                     .addPreferredAlias("!")
                     .setPrecedence(FACTORIAL_PRECEDENCE)
                     .setAffix(Affix.SUFFIX)
                     .noTuples()
                     .setNumberFunction(MathEnv::factorial)
                     .build();
    
    public static final UnaryOp SQUARE_ROOT =
        new Builder().setName("sqrt")
                     .makeIntrinsic()
                     .addPreferredAliases("sqrt", "√")
                     .makeFunction()
                     .setEchoFormatter(EchoFormatter.ofUnary("√"))
                     .noTuples()
                     .setNumberFunction(MathEnv::sqrt)
                     .build();
    
    public static final UnaryOp NTH_ROOT =
        new Builder().setName("unary_nth_rt")
                     .makeIntrinsic()
                     .addPreferredAliases("nrt")
                     .makeFunction()
                     .setEchoFormatter(EchoFormatter.OF_NTH_ROOT)
                     .setNumberFunction((self, env, n) -> {
                         throw UnsupportedException.fmt("nth root requires 2 arguments (index, radicand). Found %s.", n);
                     })
                     .anyTuple()
                     .setTupleFunction((self, env, t) -> {
                        try {
                            if (t.count() == 2) {
                                return env.nrt((Number) t.get(0), (Number) t.get(1));
                            }
                        } catch (ClassCastException x) {
                            Log.caught(self.getClass(), "during nrt", x, true);
                        }
                        throw UnsupportedException.fmt("nth root requires 2 arguments (index, radicand). Found %s.", t);
                     })
                     .build();
    
    public static final UnaryOp NATURAL_LOG =
        new Builder().setName("ln")
                     .makeIntrinsic()
                     .addPreferredAlias("ln")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction(MathEnv::ln)
                     .build();
    
    public static final UnaryOp LOGARITHM =
        new Builder().setName("log")
                     .makeIntrinsic()
                     .addPreferredAlias("log")
                     .makeFunction()
                     .setNumberFunction((env, n) -> {
                         // Common log when only one argument.
                         return env.log10(n);
                     })
                     .anyTuple()
                     .setTupleFunction((self, env, t) -> {
                        try {
                            switch (t.count()) {
                                case 1: return self.apply(env, (Number) t.get(0));
                                case 2: return env.log((Number) t.get(0), (Number) t.get(1));
                            }
                        } catch (ClassCastException x) {
                            Log.caught(self.getClass(), "during log", x, true);
                        }

                        throw UnsupportedException.fmt("Logarithm requires 1 or 2 arguments. Found %s.", t);
                     })
                     .build();
    
    public static final UnaryOp SINE =
        new Builder().setName("sin")
                     .makeIntrinsic()
                     .addPreferredAlias("sin")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction(MathEnv::sin)
                     .build();
    
    public static final UnaryOp COSINE =
        new Builder().setName("cos")
                     .makeIntrinsic()
                     .addPreferredAlias("cos")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction(MathEnv::cos)
                     .build();
    
    public static final UnaryOp TANGENT =
        new Builder().setName("tan")
                     .makeIntrinsic()
                     .addPreferredAlias("tan")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction(MathEnv::tan)
                     .build();
    
    public static final UnaryOp COSECANT =
        new Builder().setName("csc")
                     .makeIntrinsic()
                     .addPreferredAlias("csc")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction(MathEnv::csc)
                     .build();
    
    public static final UnaryOp SECANT =
        new Builder().setName("sec")
                     .makeIntrinsic()
                     .addPreferredAlias("sec")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction(MathEnv::sec)
                     .build();
    
    public static final UnaryOp COTANGENT =
        new Builder().setName("cot")
                     .makeIntrinsic()
                     .addPreferredAlias("cot")
                     .makeFunction()
                     .noTuples()
                     .setNumberFunction(MathEnv::cot)
                     .build();
    
    public static final UnaryOp ARC_SINE =
        new Builder().setName("asin")
                     .makeIntrinsic()
                     .addPreferredAliases("asin", "sin⁻¹")
                     .makeFunction()
                     .setEchoFormatter(EchoFormatter.ofFunction("sin⁻¹"))
                     .noTuples()
                     .setNumberFunction(MathEnv::asin)
                     .build();
    
    public static final UnaryOp ARC_COSINE =
        new Builder().setName("acos")
                     .makeIntrinsic()
                     .addPreferredAliases("acos", "cos⁻¹")
                     .makeFunction()
                     .setEchoFormatter(EchoFormatter.ofFunction("cos⁻¹"))
                     .noTuples()
                     .setNumberFunction(MathEnv::acos)
                     .build();
    
    public static final UnaryOp ARC_TANGENT =
        new Builder().setName("atan")
                     .makeIntrinsic()
                     .addPreferredAliases("atan", "tan⁻¹")
                     .makeFunction()
                     .setEchoFormatter(EchoFormatter.ofFunction("tan⁻¹"))
                     .noTuples()
                     .setNumberFunction(MathEnv::atan)
                     .build();
    
    public static final UnaryOp ARC_TANGENT_2 =
        new Builder().setName("atan2")
                     .makeIntrinsic()
                     .addPreferredAliases("atan2")
                     .makeFunction()
                     .setEchoFormatter(EchoFormatter.OF_FUNCTION_LIKE)
                     .setNumberFunction((self, env, n) -> {
                         throw UnsupportedException.fmt("atan2 requires 2 arguments (y, x). Found %s.", n);
                     })
                     .setTupleCount(2)
                     .setTupleFunction((self, env, t) -> {
                         try {
                             return env.atan2((Number) t.get(0), (Number) t.get(1));
                         } catch (ClassCastException x) {
                             Log.caught(self.getClass(), "during atan2", x, true);
                         }
                         throw UnsupportedException.operandTypes(t.get(0), t.get(1), self.getName());
                     })
                     .build();
    
    public static final List<UnaryOp> DEFAULTS =
        Collections.unmodifiableList(Misc.collectConstants(UnaryOp.class));
}