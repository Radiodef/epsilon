/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.math.*;

import java.util.*;

// As it turns out, variables and constants serve pretty much the exact
// same role. Having both just makes things more complicated for essentially
// no good reason.
@Deprecated
abstract class Constant /*extends AbstractSymbol implements Operand*/ {
//    public Constant(Builder builder) {
//        super(builder);
//    }
//    
//    private Constant(String name, Constant delegate) {
//        super(name, delegate);
//    }
//    
//    @Override
//    public Operand.Kind getOperandKind() {
//        return Operand.Kind.CONSTANT;
//    }
//    
//    @Override
//    public Symbol.Kind getSymbolKind() {
//        return Symbol.Kind.CONSTANT;
//    }
//    
//    @Override
//    public boolean isVariable() {
//        return false;
//    }
//    
//    @Override
//    public Constant createAlias(String aliasName) {
//        return new Alias(aliasName, this);
//    }
//    
//    @Override
//    public boolean isTruthy(Context context) {
//        return this.evaluate(context).isTruthy(context);
//    }
//    
//    @Override
//    public Operand evaluate(Context context) {
//        // Local variables hide constants.
//        Symbols       scope = context.getScope();
//        Symbols.Group group = scope.get(getName());
//        if (group != null) {
//            Variable that = (Variable) group.get(Symbol.Kind.VARIABLE);
//            if (that != null) {
//                return that.evaluate(context);
//            }
//        } // else
//        return this.evaluateOtherwise(context);
//    }
//    
//    protected abstract Operand evaluateOtherwise(Context env);
//    
//    @Override
//    public StringBuilder toString(StringBuilder b, Context c) {
//        return b.append(getName());
//    }
//    
//    @Override
//    public eps.block.Block toSourceBlock(Context c, Node n) {
//        throw new AssertionError(this);
//    }
//    
//    @Override
//    public eps.block.Block toResultBlock(StringBuilder b, Context c) {
//        throw new AssertionError(this);
//    }
//    
//    @Override
//    public eps.block.Block toSourceBlock(StringBuilder b, Context c, Node n) {
//        throw new AssertionError(this);
//    }
//    
//    public static class FromFunction extends Constant {
//        private final ConstantFunction fn;
//        
//        public FromFunction(Builder builder) {
//            super(builder);
//            this.fn = Objects.requireNonNull(builder.fn, "fn");
//        }
//        
//        @Override
//        public Operand evaluateOtherwise(Context c) {
//            return fn.apply(this, c);
//        }
//    }
//    
//    public static class ConstantError extends Constant {
//        public ConstantError(Builder builder) {
//            super(builder);
//        }
//        
//        @Override
//        public Operand evaluateOtherwise(Context c) {
//            throw new EvaluationException("unresolved constant " + getName());
//        }
//    }
//    
//    public static Constant createErrorConstant(String name) {
//        return new Builder().setName(name)
//                            .buildError();
//    }
//    
//    public static class Alias extends Constant {
//        private final Constant delegate;
//        
//        public Alias(String name, Constant delegate) {
//            super(name, Objects.requireNonNull(delegate, "delegate"));
//            this.delegate = delegate;
//        }
//        
//        @Override
//        public Symbol getDelegate() {
//            return delegate.getDelegate();
//        }
//        
//        // This interferes with local variables hiding constants correctly.
////        @Override
////        public Operand evaluate(Context c) {
////            return delegate.evaluate(c);
////        }
//        
//        @Override
//        public Operand evaluateOtherwise(Context c) {
//            return delegate.evaluateOtherwise(c);
//        }
//    }
//    
//    @FunctionalInterface
//    public interface ConstantFunction {
//        Operand apply(Constant self, Context c);
//    }
//    @FunctionalInterface
//    public interface ConstantFunctionNoSelf extends ConstantFunction {
//        @Override
//        default Operand apply(Constant self, Context c) {
//            return apply(c);
//        }
//        Operand apply(Context c);
//    }
//    @FunctionalInterface
//    public interface ConstantFunctionMathEnv extends ConstantFunction {
//        @Override
//        default Operand apply(Constant self, Context c) {
//            return apply(c.getMathEnvironment());
//        }
//        Operand apply(MathEnv env);
//    }
//    
//    public static class Builder extends Symbol.Builder<Constant, Builder> {
//        private static final ConstantFunction NO_FUNCTION =
//            (ConstantFunction & java.io.Serializable) (s, c) -> { throw new IllegalStateException(); };
//        
//        private ConstantFunction fn = NO_FUNCTION;
//        
//        public Builder setFunction(ConstantFunction fn) {
//            this.fn = Objects.requireNonNull(fn, "fn");
//            return this;
//        }
//        
//        public Builder setFunction(ConstantFunctionNoSelf fn) {
//            return setFunction((ConstantFunction) fn);
//        }
//        
//        public Builder setFunction(ConstantFunctionMathEnv fn) {
//            return setFunction((ConstantFunction) fn);
//        }
//        
//        public ConstantFunction getFunction() {
//            return fn;
//        }
//        
//        @Override
//        public Constant build() {
//            return new Constant.FromFunction(this);
//        }
//        
//        @Override
//        public ConstantError buildError() {
//            return new ConstantError(this);
//        }
//    }
//    
//    // all default constants moved to variables
//    
//    @Deprecated
//    public static final Constant DEBUG_THROW =
//        new Builder().setName("throw")
//                     .makeIntrinsic()
//                     .addPreferredAlias("throw")
//                     .setFunction((self, env) -> {
//                         final class ThrowConstantException extends RuntimeException {
//                             ThrowConstantException() {
//                                 super("evaluation of __throw__ constant");
//                             }
//                         }
//                         throw new ThrowConstantException();
//                     })
//                     .build();
//    
//    @Deprecated
//    public static final Constant UNDEFINED =
//        new Builder().setName("undefined")
//                     .makeIntrinsic()
//                     .addPreferredAlias("undefined")
//                     .setFunction((self, env) -> Undefinition.INSTANCE)
//                     .build();
//    
//    @Deprecated
//    public static final Constant PI =
//        new Builder().setName("pi")
//                     .makeIntrinsic()
//                     .addPreferredAliases("pi", "π")
//                     .setFunction(MathEnv::pi)
//                     .build();
//    
//    @Deprecated
//    public static final Constant E =
//        new Builder().setName("e")
//                     .makeIntrinsic()
//                     .addPreferredAlias("e")
//                     .setFunction(MathEnv::e)
//                     .build();
//    
//    @Deprecated
//    public static final List<Constant> DEFAULTS =
//        Collections.unmodifiableList(Misc.collectConstants(Constant.class));
}