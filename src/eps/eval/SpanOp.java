/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.math.*;
import eps.math.Number;
import eps.util.*;
import eps.json.*;
import eps.ui.*;
import eps.app.*;
import static eps.util.Literals.*;

import java.util.*;

@JsonSerializable
public abstract class SpanOp extends Operator {
    private final SpanOp parent;
    
    @JsonProperty(interimConverter = "eps.eval.SpanOp$SpanOpNameInterimConverter",
                  deferInterimToConstructor = true)
    private final SpanOp left;
    @JsonProperty(interimConverter = "eps.eval.SpanOp$SpanOpNameInterimConverter",
                  deferInterimToConstructor = true)
    private final SpanOp right;
    
    @JsonProperty(interimConverter = "eps.json.InterimConverter$EmptySetConverter")
    private final Set<Pair<String, String>> preferredAliases2;
    
    @JsonProperty
    private final boolean isLiteral;
    @JsonProperty
    private final boolean allowsEmpty;
    
    @JsonDefault("isLiteral")
    private static final boolean IS_LITERAL_DEFAULT = false;
    @JsonDefault("allowsEmpty")
    private static final boolean ALLOWS_EMPTY_DEFAULT = false;
    
    @JsonDefault("preferredAliases2")
    private static final Set<Pair<String, String>> PREFERRED_ALIASES_2_DEFAULT = Collections.emptySet();
    
    // regular constructor
    public SpanOp(Builder builder) {
        super(builder);
        this.parent = this;
        
        String leftName  = builder.getLeftName();
        String rightName = builder.getRightName();
        
        if (leftName.equals(rightName)) {
            this.left  = this;
            this.right = this;
        } else {
            this.left  = builder.buildChild(this, true);
            this.right = builder.buildChild(this, false);
        }
        
        this.preferredAliases2 = new ArraySet<>(builder.preferredAliases);
        this.isLiteral         = builder.isLiteral;
        this.allowsEmpty       = builder.allowsEmpty;
    }
    
    // regular child constructor
    private SpanOp(SpanOp parent, Builder builder) {
        super(builder);
        this.parent = parent;
        this.left   = null;
        this.right  = null;
        
        this.preferredAliases2 = Collections.emptySet();
        this.isLiteral         = builder.isLiteral;
        this.allowsEmpty       = builder.allowsEmpty;
    }
    
    // symmetric alias constructor
    private SpanOp(String name, EchoFormatter fmt, SpanOp delegate) {
        super(name, fmt, delegate);
        this.parent = this;
        this.left   = this;
        this.right  = this;
        
        this.preferredAliases2 = Collections.emptySet();
        this.isLiteral         = delegate.isLiteral;
        this.allowsEmpty       = delegate.allowsEmpty;
    }
    
    // asymmetric alias constructor
    private SpanOp(String leftName, String rightName, EchoFormatter fmt, SpanOp delegate) {
        super(leftName + rightName, fmt, delegate);
        this.parent = this;
        
        assert !leftName.equals(rightName) : leftName;
        
        this.left  = new SpanOp.Alias(this, leftName,  fmt, delegate);
        this.right = new SpanOp.Alias(this, rightName, fmt, delegate);
        
        this.preferredAliases2 = Collections.emptySet();
        this.isLiteral         = delegate.isLiteral;
        this.allowsEmpty       = delegate.allowsEmpty;
    }
    
    // alias child constructor
    private SpanOp(SpanOp parent, String name, EchoFormatter fmt, SpanOp delegate) {
        super(name, fmt, delegate);
        this.parent = parent;
        this.left   = null;
        this.right  = null;
        
        this.preferredAliases2 = Collections.emptySet();
        this.isLiteral         = delegate.isLiteral;
        this.allowsEmpty       = delegate.allowsEmpty;
    }
    
    @JsonConstructor
    protected SpanOp(String        name,
                     EchoFormatter echoFormatter,
                     Set<String>   preferredSymmetricAliases,
                     int           precedence,
                     Associativity associativity,
                     String        leftName,
                     String        rightName,
                     Set<Pair<String, String>> preferredAssymetricAliases,
                     boolean       isLiteral,
                     boolean       allowsEmpty) {
        super(name, echoFormatter, preferredSymmetricAliases, precedence, associativity);
        this.parent = this;
        
        this.isLiteral         = isLiteral;
        this.allowsEmpty       = allowsEmpty;
        this.preferredAliases2 = Misc.requireNonNull(preferredAssymetricAliases, "preferredSymmetricAliases");
        
        if (leftName.equals(rightName)) {
            this.left  = this;
            this.right = this;
        } else {
            Builder builder =
                new Builder().setAll(this)
                             .setName(leftName, rightName);
            this.left  = builder.buildChild(this, true);
            this.right = builder.buildChild(this, false);
        }
    }
    
    private static final class SpanOpNameInterimConverter extends InterimConverter<SpanOp, String> {
        private SpanOpNameInterimConverter() {
            super(SpanOp.class, String.class);
        }
        @Override
        public String toInterim(Object obj) {
            return obj == null ? null : ((SpanOp) obj).getName();
        }
        @Override
        public SpanOp fromInterim(Object obj) {
            throw new UnsupportedOperationException();
        }
    }
    
    @Override
    protected EchoFormatter getDefaultEchoFormatter() {
        return EchoFormatter.OF_SPAN_OP;
    }
    
    @Override
    public boolean isEqualTo(Symbol sym) {
        return isEqualTo((SpanOp) sym, true);
    }
    
    protected boolean isEqualTo(Symbol sym, boolean testRelatives) {
        if (super.isEqualTo(sym)) {
            SpanOp that = (SpanOp) sym;
            if (this.isLiteral != that.isLiteral)
                return false;
            if (this.allowsEmpty != that.allowsEmpty)
                return false;
            if (!Objects.equals(this.preferredAliases2, that.preferredAliases2))
                return false;
            if (this.isParent() != that.isParent())
                return false;
            if (this.isLeft() != that.isLeft())
                return false;
            if (this.isRight() != that.isRight())
                return false;
            if (this.isSymmetric() != that.isSymmetric())
                return false;
            if (testRelatives) {
                if (!isParent() && !SpanOp.isEqual(this.getParent(), that.getParent(), false))
                    return false;
                if (!isLeft() && !SpanOp.isEqual(this.getLeft(), that.getLeft(), false))
                    return false;
                if (!isRight() && !SpanOp.isEqual(this.getRight(), that.getRight(), false))
                    return false;
            }
            return true;
        }
        return false;
    }
    
    protected static boolean isEqual(SpanOp lhs, SpanOp rhs, boolean testRelatives) {
        if (lhs == null) return rhs == null;
        if (rhs == null) return false;
        return lhs.isEqualTo(rhs, testRelatives);
    }
    
    public String getLeftName() {
        return this.getLeft().getName();
    }
    public String getRightName() {
        return this.getRight().getName();
    }
    
    public SpanOp getParent() { return parent;       }
    public SpanOp getLeft()   { return parent.left;  }
    public SpanOp getRight()  { return parent.right; }
    
    public boolean isParent() { return parent       == this; }
    public boolean isLeft()   { return parent.left  == this; }
    public boolean isRight()  { return parent.right == this; }
    
    public boolean isSymmetric() { return this.getLeft() == this.getRight(); }
    
    public boolean isParenthesis() {
        return ((SpanOp) this.getDelegate()).getParent() == PARENTHESIS;
    }
    
    public boolean isLiteral() {
        return this.isLiteral;
    }
    
    public boolean allowsEmpty() {
        return this.allowsEmpty;
    }
    
    @Override
    public Setting<Style> getStyleSetting() {
        if (isParenthesis()) {
            return Setting.BRACKET_STYLE;
        }
        if (isLiteral()) {
            return Setting.LITERAL_STYLE;
        }
        return super.getStyleSetting();
    }
    
    @Override
    public boolean isIntrinsic() {
        return Symbol.isIntrinsic(getLeftName()) || Symbol.isIntrinsic(getRightName());
    }
    
    @Override
    public boolean hasName(String name) {
        return getLeftName().equals(name) || getRightName().equals(name);
    }
    
    @Override
    public Collection<Symbol> getSymbols() {
        if (this.isSymmetric()) {
            return Collections.singleton(this.getParent());
        } else {
            return ListOf(this.getLeft(), this.getRight());
        }
    }
    
    @Override
    public SpanOp createAlias(Symbol sym, EchoFormatter fmt) {
        if (sym instanceof SpanOp) {
            SpanOp span = (SpanOp) sym;
            return this.createAlias(span.getLeftName(), span.getRightName(), fmt);
        } else {
            return this.createAlias(sym.getName(), fmt);
        }
    }
    
    @Override
    public SpanOp createAlias(String name, EchoFormatter fmt) {
        return new SpanOp.Alias(name, (SpanOp) this.getDelegate());
    }
    
    public SpanOp createAlias(String leftName, String rightName) {
        return createAlias(leftName, rightName, null);
    }
    
    public SpanOp createAlias(String leftName, String rightName, EchoFormatter fmt) {
        if (leftName.equals(rightName)) {
            return this.createAlias(leftName, fmt);
        } else {
            return new SpanOp.Alias(leftName, rightName, fmt, (SpanOp) this.getDelegate());
        }
    }
    
    @Override
    public Symbol[] createPreferredAliases() {
        List<Symbol> results = new ArrayList<>();
        
        for (String alias : getPreferredAliases()) {
            results.add(this.createAlias(alias));
        }
        for (Pair<String, String> alias : preferredAliases2) {
            results.add(this.createAlias(alias.left, alias.right));
        }
        
        return results.toArray(new Symbol[0]);
    }
    
    @Override
    public Symbol.Kind getSymbolKind() {
        return Symbol.Kind.SPAN_OP;
    }
    
    public String toBothString() {
        return getLeft().getName() + getRight().getName();
    }
    
    public Operand apply(Context context, Node operand) {
        SpanOp parent = this.getParent();
        
        if (!parent.allowsEmpty) {
            operand = operand.getExpressionNode();
            if (operand.isOperand()) {
                if (((Node.OfOperand) operand).get().isEmpty()) {
                    throw EvaluationException.emptySpan(this);
                }
            }
        }
        
        return parent.applyImpl(context, operand);
    }
    
    protected abstract Operand applyImpl(Context context, Node operand);
    
    public static class Alias extends SpanOp {
        private final SpanOp delegate;
        
        public Alias(String name, SpanOp delegate) {
            this(name, (EchoFormatter) null, delegate);
        }
        
        public Alias(String name, EchoFormatter fmt, SpanOp delegate) {
            super(name, fmt, Objects.requireNonNull(delegate, "delegate"));
            this.delegate = delegate;
        }
        
        public Alias(String leftName, String rightName, SpanOp delegate) {
            this(leftName, rightName, null, delegate);
        }
        
        public Alias(String leftName, String rightName, EchoFormatter fmt, SpanOp delegate) {
            super(Objects.requireNonNull(leftName,  "leftName"),
                  Objects.requireNonNull(rightName, "rightName"),
                  fmt,
                  Objects.requireNonNull(delegate,  "delegate"));
            this.delegate = delegate;
        }
        
        private Alias(SpanOp parent, String name, EchoFormatter fmt, SpanOp delegate) {
            super(parent, name, fmt, delegate);
            this.delegate = delegate;
        }
        
        static {
            JsonObjectAdapter.put(Alias.class, Symbol::aliasToMap, m -> (Alias) Symbol.mapToAlias(m));
        }
        
        @Override
        public Alias configure(Symbol.Builder<?, ?> b0) {
            Builder b = (Builder) b0;
            
            String left  = b.getLeftName();
            String right = b.getRightName();
            
            if (left.equals(right)) {
                return new Alias(left, b.getEchoFormatter(), delegate);
            } else {
                return new Alias(left, right, b.getEchoFormatter(), delegate);
            }
        }
        
        @Override
        protected boolean isEqualTo(Symbol sym, boolean testRelatives) {
            if (super.isEqualTo(sym, testRelatives)) {
                Alias that = (Alias) sym;
                return Symbol.isEqual(this.delegate, that.delegate);
            }
            return false;
        }
        
        @Override
        public Symbol getDelegate() {
            return delegate.getDelegate();
        }
        
        @Override
        protected Operand applyImpl(Context context, Node operand) {
            return delegate.apply(context, operand);
        }
    }
    
    @JsonSerializable
    public static class SpanError extends SpanOp {
        private SpanError(Builder builder) {
            super(builder);
        }
        
        @JsonConstructor
        protected SpanError(String        name,
                            EchoFormatter echoFormatter,
                            Set<String>   preferredSymmetricAliases,
                            int           precedence,
                            Associativity associativity,
                            String        leftName,
                            String        rightName,
                            Set<Pair<String, String>> preferredAssymetricAliases,
                            boolean       isLiteral,
                            boolean       allowsEmpty) {
            super(name,
                  echoFormatter,
                  preferredSymmetricAliases,
                  precedence,
                  associativity,
                  leftName,
                  rightName,
                  preferredAssymetricAliases,
                  isLiteral,
                  allowsEmpty);
        }
        
        @Override
        public SpanError configure(Symbol.Builder<?, ?> b) {
            return new SpanError((Builder) b);
        }
        
        @Override
        public boolean isError() {
            return true;
        }
        
        @Override
        protected Operand applyImpl(Context context, Node operand) {
            throw EvaluationException.unresolved(this);
        }
    }
    
    public static SpanOp createSpanError(Token.OfUnresolved left, Token.OfUnresolved right) {
        return createSpanError(left.getSource().toString(), right.getSource().toString());
    }
    
    public static SpanOp createSpanError(String left, String right) {
        return new Builder().setLeftName(left)
                            .setRightName(right)
                            .allowEmpty()
                            .buildError();
    }
    
    @FunctionalInterface
    public interface SpanOpFunction {
        Operand apply(SpanOp self, Context context, Node operand);
    }
    @FunctionalInterface
    public interface SpanOpMathFunction extends SpanOpFunction {
        @Override
        default Operand apply(SpanOp self, Context context, Node node) {
            Operand op = node.evaluate(context);
            
            if (op.getOperandKind().isNumber()) {
                return this.apply(context.getMathEnvironment(), (Number) op);
            }
            
            throw UnsupportedException.operandType(op, self.getName());
        }
        Operand apply(MathEnv env, Number n);
    }
    
    @JsonSerializable
    public static class FromFunction extends SpanOp {
        @JsonProperty
        private final SpanOpFunction fn;
        
        public FromFunction(Builder builder) {
            super(builder);
            this.fn = builder.fn;
        }
        
        private FromFunction(SpanOp parent, Builder builder) {
            super(parent, builder);
            this.fn = builder.fn;
        }
        
        @JsonConstructor
        protected FromFunction(String         name,
                               EchoFormatter  echoFormatter,
                               Set<String>    preferredSymmetricAliases,
                               int            precedence,
                               Associativity  associativity,
                               String         leftName,
                               String         rightName,
                               Set<Pair<String, String>> preferredAssymetricAliases,
                               boolean        isLiteral,
                               boolean        allowsEmpty,
                               SpanOpFunction fn) {
            super(name,
                  echoFormatter,
                  preferredSymmetricAliases,
                  precedence,
                  associativity,
                  leftName,
                  rightName,
                  preferredAssymetricAliases,
                  isLiteral,
                  allowsEmpty);
            this.fn = Objects.requireNonNull(fn, "fn");
        }
        
        private FromFunction(Builder b, SpanOpFunction fn) {
            super(b);
            this.fn = fn;
        }
        
        @Override
        public FromFunction configure(Symbol.Builder<?, ?> b) {
            return new FromFunction((Builder) b, fn);
        }
        
        @Override
        protected boolean isEqualTo(Symbol sym, boolean testRelatives) {
            if (super.isEqualTo(sym, testRelatives)) {
                FromFunction that = (FromFunction) sym;
                return Objects.equals(this.fn, that.fn);
            }
            return false;
        }
        
        @Override
        protected Operand applyImpl(Context context, Node operand) {
            return fn.apply(this, context, operand);
        }
    }
    
    @JsonSerializable
    public static class FromTree extends SpanOp {
        @JsonProperty(interimConverter = TreeFunction.INTERIM_CONVERTER,
                      deferInterimToConstructor = true)
        private final TreeFunction treeFn;
        
        private static Builder createBuilderForSuperConstructor(Node.OfSpanOp decl, Node body) {
            SpanOp span = decl.getOperator();
            
            return new Builder().setLeftName(span.getLeftName())
                                .setRightName(span.getRightName())
                                .setLiteral(span.isLiteral())
                                .setEchoFormatter(span.getEchoFormatter())
                                ;
        }
        
        public FromTree(Context context, Node.OfSpanOp decl, Node body) {
            super(createBuilderForSuperConstructor(decl, body));
            
            this.treeFn = new TreeFunction(context, this, decl, body);
        }
        
        public FromTree(String leftName, String rightName, String param, String body) {
            super(new Builder().setLeftName(leftName)
                               .setRightName(rightName)
                               .setLiteral(false));
            
            this.treeFn = new TreeFunction(this, param, body);
        }
        
        public FromTree(Builder builder, TreeAdapter<?, String> params, String body) {
            super(builder);
            
            this.treeFn = new TreeFunction(this, params, body);
        }
        
        @JsonConstructor
        protected FromTree(String        name,
                           EchoFormatter echoFormatter,
                           Set<String>   preferredSymmetricAliases,
                           int           precedence,
                           Associativity associativity,
                           String        leftName,
                           String        rightName,
                           Set<Pair<String, String>> preferredAssymetricAliases,
                           boolean       isLiteral,
                           boolean       allowsEmpty,
                           Object        treeFn) {
            super(name,
                  echoFormatter,
                  preferredSymmetricAliases,
                  precedence,
                  associativity,
                  leftName,
                  rightName,
                  preferredAssymetricAliases,
                  isLiteral,
                  allowsEmpty);
            this.treeFn = TreeFunction.fromInterimObject(this, treeFn);
        }
        
        private FromTree(Builder b, TreeFunction treeFn) {
            super(b);
            this.treeFn = treeFn.copy(this);
        }
        
        @Override
        public FromTree configure(Symbol.Builder<?, ?> b) {
            return new FromTree((Builder) b, treeFn);
        }
        
        @Override
        protected boolean isEqualTo(Symbol sym, boolean testRelatives) {
            if (super.isEqualTo(sym, testRelatives)) {
                FromTree that = (FromTree) sym;
                return TreeFunction.isEqual(this.treeFn, that.treeFn);
            }
            return false;
        }
        
        TreeFunction getTreeFunction() {
            return treeFn;
        }
        
        @Override
        protected Operand applyImpl(Context context, Node operand) {
            return treeFn.evaluate(context, operand);
        }
    }
    
    public static SpanOp.FromTree fromTree(Context context, Node.OfSpanOp decl, Node body) {
        return new SpanOp.FromTree(context, decl, body);
    }
    
    private enum SpanOpFunctions implements SpanOpFunction {
        UNRESOLVED {
            @Override
            public Operand apply(SpanOp self, Context context, Node operand) {
                throw EvaluationException.unresolved(self);
            }
        },
        PARENT {
            @Override
            public Operand apply(SpanOp self, Context context, Node operand) {
                return self.getParent().apply(context, operand);
            }
        }
    }
    
    public static final class Builder extends Operator.Builder<SpanOp, Builder> {
        private SpanOpFunction fn = SpanOpFunctions.UNRESOLVED;
        
        private String leftName  = super.getName();
        private String rightName = super.getName();
        
        private final Set<Pair<String, String>> preferredAliases = new ArraySet<>(0);
        
        private boolean isLiteral   = false;
        private boolean allowsEmpty = false;
        
        public Builder() {
            super.setAssociativity(Associativity.LEFT_TO_RIGHT);
            super.setPrecedence(HIGHEST_PRECEDENCE);
            super.setEchoFormatter(EchoFormatter.OF_SPAN_OP);
        }
        
        @Override
        public Builder setAll(SpanOp span) {
            super.setAll(span);
            
            if (span.isParent()) {
                SpanOp left  = span.getLeft();
                SpanOp right = span.getRight();
                if (left != null && right != null) {
                    this.setName(left.getName(), right.getName());
                }
            }
            
            this.setLiteral(span.isLiteral())
                .setAllowsEmpty(span.allowsEmpty());
            
            this.preferredAliases.clear();
            this.preferredAliases.addAll(span.preferredAliases2);
            
            return this;
        }
        
        @Override
        public Builder setAssociativity(Associativity a) {
            // SpanOps don't have associativity
            return this;
        }
        
        @Override
        public Builder setPrecedence(int precedence) {
            // SpanOps don't have precedence
            return this;
        }
        
        public Builder addPreferredAlias(String leftAlias, String rightAlias) {
            Objects.requireNonNull(leftAlias,  "leftAlias");
            Objects.requireNonNull(rightAlias, "rightAlias");
            
            if (leftAlias.equals(rightAlias)) {
                return this.addPreferredAlias(leftAlias);
                
            } else {
                preferredAliases.add(Pair.of(leftAlias, rightAlias));
                return this;
            }
        }
        
        public Builder setLiteral(boolean isLiteral) {
            this.isLiteral = isLiteral;
            return this;
        }
        
        public Builder makeLiteral() {
            return this.setLiteral(true);
        }
        
        public Builder setAllowsEmpty(boolean allowsEmpty) {
            this.allowsEmpty = allowsEmpty;
            return this;
        }
        
        public Builder allowEmpty()      { return this.setAllowsEmpty(true);  }
        public Builder requireNonEmpty() { return this.setAllowsEmpty(false); }
        
        @Override
        public Builder setName(String name) {
            super.setName(name);
            this.leftName = this.rightName = this.name;
            return this;
        }
        
        public Builder setName(String leftName, String rightName) {
            Objects.requireNonNull(leftName,  "leftName");
            Objects.requireNonNull(rightName, "rightName");
            
            if (leftName.equals(rightName)) {
                return this.setName(leftName);
                
            } else {
                boolean isIntrinsic = Symbol.isIntrinsic(leftName) || Symbol.isIntrinsic(rightName);
                
                leftName = Symbol.deintrinsify(leftName);
                rightName = Symbol.deintrinsify(rightName);
                
                this.leftName  = leftName;
                this.rightName = rightName;
                
                if (isIntrinsic) {
                    leftName  = Symbol.intrinsify(leftName);
                    rightName = Symbol.intrinsify(rightName);
                }
                
                return super.setName(leftName + rightName);
            }
        }
        
        public Builder setLeftName(String leftName) {
            return this.setName(leftName, rightName);
        }
        
        public Builder setRightName(String rightName) {
            return this.setName(leftName, rightName);
        }
        
        public String getLeftName() {
            return isIntrinsic() ? Symbol.intrinsify(leftName) : leftName;
        }
        
        public String getRightName() {
            return isIntrinsic() ? Symbol.intrinsify(rightName) : rightName;
        }
        
        @Override
        public String getName() {
            if (leftName.equals(rightName)) {
                return super.getName();
            }
            return getLeftName() + getRightName();
        }
        
        public Builder setFunction(SpanOpFunction fn) {
            this.fn = Objects.requireNonNull(fn, "fn");
            return this;
        }
        
        public Builder setFunction(SpanOpMathFunction fn) {
            return this.setFunction((SpanOpFunction) fn);
        }
        
        @Override
        public SpanOp build() {
            return new SpanOp.FromFunction(this);
        }
        
        @Override
        public SpanOp buildError() {
            return new SpanOp.SpanError(this);
        }
        
        private SpanOp buildChild(SpanOp parent, boolean isLeft) {
            String left  = getLeftName();
            String right = getRightName();
            assert !left.equals(right) : this;
            SpanOpFunction fn = this.fn;
            try {
                setName(isLeft ? left : right);
//                super.setName(isLeft ? left : right);
                this.fn = SpanOpFunctions.PARENT;
                return new SpanOp.FromFunction(parent, this);
            } finally {
                this.setName(left, right);
                this.fn = fn;
            }
        }
        
        protected static final ToString<Builder> SPAN_OP_BUILDER_TO_STRING =
            Operator.Builder.OPERATOR_BUILDER_TO_STRING.derive(Builder.class)
                    .add("fn",               b -> b.fn)
                    .add("leftName",         Builder::getLeftName)
                    .add("rightName",        Builder::getLeftName)
                    .add("preferredAliases", b -> b.preferredAliases)
                    .add("isLiteral",        b -> b.isLiteral)
                    .add("allowsEmpty",      b -> b.allowsEmpty)
                    .build();
        @Override
        public String toString(boolean m) {
            return SPAN_OP_BUILDER_TO_STRING.apply(this, m);
        }
    }
    
    public static final SpanOp PARENTHESIS =
        new Builder().setName("l_parenthesis", "r_parenthesis")
                     .makeIntrinsic()
                     .addPreferredAlias("(", ")")
                     .addPreferredAlias("{", "}")
                     .requireNonEmpty()
                     .setFunction((s, c, n) -> n.evaluate(c))
                     .build();
    
    public static class MakeTupleOp extends SpanOp {
        public MakeTupleOp(Builder b) {
            super(b);
        }
        @Override
        public MakeTupleOp configure(Symbol.Builder<?, ?> b) {
            return new MakeTupleOp((Builder) b);
        }
        @Override
        protected Operand applyImpl(Context context, Node node) {
            Operand operand = node.evaluate(context);
            if (operand.isEmpty()) {
                return Tuple.empty();
            }
            
            if (node.getExpressionNode().isNaryOp()) {
                return operand;
            }
            
            return Tuple.of(operand);
        }
    }
    
    public static final SpanOp MAKE_TUPLE =
        new MakeTupleOp(new Builder().setName("l_tuple", "r_tuple")
                                     .makeIntrinsic()
                                     .addPreferredAlias("[", "]")
//                                     .setEchoFormatter(EchoFormatter.OF_BRACKET_SPAN_OP)
                                     .allowEmpty());
    
    public static final SpanOp ABS =
        new Builder().setName("abs")
                     .makeIntrinsic()
                     .addPreferredAlias("|")
                     .requireNonEmpty()
                     .setFunction((env, n) -> n.abs(env))
                     .build();
    
    public static final SpanOp STRING_LITERAL =
        new Builder().setName("l_str", "r_str")
                     .makeIntrinsic()
                     .addPreferredAlias("'")
                     .addPreferredAlias("\"")
                     .makeLiteral()
                     .allowEmpty()
                     .setFunction((s, c, n) -> n.evaluate(c)) // TODO
                     .build();
    
    public static final List<SpanOp> DEFAULTS =
        Collections.unmodifiableList(Misc.collectConstants(SpanOp.class));
}