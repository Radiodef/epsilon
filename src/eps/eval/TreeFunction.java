/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.util.*;
import eps.json.*;
import static eps.util.Misc.*;

import java.util.*;

public final class TreeFunction {
    private final Symbol owner;
    private final Tree[] trees;
    private final Parameters params;
    
    private TreeFunction(Symbol owner, TreeFunction that) {
        this.owner  = owner;
        this.trees  = that.trees;
        this.params = that.params;
    }
    
    TreeFunction copy(Symbol owner) {
        Objects.requireNonNull(owner, "owner");
        if (owner.getSymbolKind() != this.owner.getSymbolKind()) {
            throw new IllegalArgumentException(owner.getSymbolKind().name());
        }
        return new TreeFunction(owner, this);
    }
    
    // regular variable constructor
    TreeFunction(Context context, Variable owner, Node body) {
        this(context, owner, (Node.OfOperator) null, Collections.singletonList(body), (Void) null);
    }
    
    // n-ary op parameter variable constructor
    TreeFunction(Context context, Variable owner, List<Node> bodies) {
        this(context, owner, (Node.OfOperator) null, bodies, (Void) null);
    }
    
    TreeFunction(Context context, Operator owner, Node.OfOperator decl, Node body) {
        this(context, owner, Objects.requireNonNull(decl, "decl"), Collections.singletonList(body), (Void) null);
    }
    
    private TreeFunction(Context context, Symbol owner, Node.OfOperator decl, List<Node> bodies, Void $private) {
        Objects.requireNonNull(context, "context");
        Objects.requireNonNull(bodies,  "bodies");
        
        boolean isVariable = owner.isVariable();
        
        this.owner = Objects.requireNonNull(owner, "owner");
        
        int bodyCount = bodies.size();
        if (bodyCount == 0) {
            throw Errors.newIllegalArgumentF("empty body for %s", owner);
        }
        
        Tree[] trees = new Tree[bodyCount];
        for (int i = 0; i < bodyCount; ++i) {
            Node body = Objects.requireNonNull(bodies.get(i), "body");
            trees[i]  = Tree.fromNode(context, body, isVariable);
        }
        
        this.trees  = trees;
        this.params = isVariable ? new Parameters(this)
                                 : createParameters(context, owner, decl);
        if (params.count() > 1) {
            params.validate(new HashSet<>(2 * params.count()));
        }
        
        Log.low(TreeFunction.class, "<init>",
                () -> f("created tree function for %s with params = %s, trees = %s", 
                        owner, params, Arrays.toString(trees)));
    }
    
    // variable from source constructor
    TreeFunction(Variable owner, String source) {
        this.owner  = Objects.requireNonNull(owner, "owner");
        this.trees  = new Tree[] { Tree.fromSource(source, true) };
        this.params = new Parameters(this);
    }
    
    // span/n-ary ops from source constructor
    TreeFunction(Operator owner, String param, String source) {
        this.owner = Objects.requireNonNull(owner, "owner");
        this.trees = new Tree[] { Tree.fromSource(source, false) };
        
        switch (owner.getSymbolKind()) {
            case SPAN_OP:
                this.params = new Parameters(this, new VariableParameter(this, param));
                break;
            case NARY_OP:
                this.params = new Parameters(this, new NaryVariableParameter(this, param));
                break;
            default:
                throw Errors.newIllegalArgument(f("%s: %s", owner.getSymbolKind(), owner));
        }
    }
    
    // other operators from source constructor
    TreeFunction(Operator owner, TreeAdapter<?, String> params, String source) {
        this.owner  = Objects.requireNonNull(owner, "owner");
        this.trees  = new Tree[] { Tree.fromSource(source, false) };
        
        Objects.requireNonNull(params, "params");
        this.params = createParameters(params.getRoot());
    }
    
    private Parameters createParameters(TreeAdapter.Node<?, String> root) {
        if (root == null) {
            return new Parameters(this);
        }
        if (owner.isNaryOp()) {
            if (!root.isEmpty()) {
                String name = root.getChildAt(0).getValue();
                if (name != null) {
                    return new Parameters(this, new NaryVariableParameter(this, name));
                }
            }
        } else if (root.allowsChildren()) {
            Parameters params = (Parameters) createParameter(root, true);
            boolean    valid  = true;
            switch (owner.getSymbolKind()) {
                case SPAN_OP:
                case NARY_OP:
                case UNARY_OP:
                    if (params.count() != 1)
                        valid = false;
                    break;
                case BINARY_OP:
                    if (params.count() != 2)
                        valid = false;
                    break;
                default:
                    assert false : this;
                    break;
            }
            if (!valid) {
                throw invalidParameterDeclaration(root);
            }
            return params;
        }
        throw invalidParameterDeclaration(root);
    }
    
    private Parameter createParameter(TreeAdapter.Node<?, String> node, boolean isRoot) {
        if (node.allowsChildren()) {
            int count = node.childCount();
            if (count == 0) {
                throw invalidParameterDeclaration(node);
            }
            Parameter[] group = new Parameter[count];
            for (int i = 0; i < count; ++i) {
                group[i] = createParameter(node.getChildAt(i), false);
            }
            if (isRoot) {
                return new Parameters(this, group);
            } else {
                return new ParameterGroup(this, group);
            }
        }
        if (!isRoot) {
            String name = node.getValue();
            if (name != null) {
                return new VariableParameter(this, name);
            }
        }
        throw invalidParameterDeclaration(node);
    }
    
    @JsonSerializable
    private static final class InterimContainer {
        @JsonProperty
        private final Tree[] trees;
        @JsonProperty
        private final Parameters params;
        
        @JsonConstructor
        private InterimContainer(Tree[] trees, Parameters params) {
            this.trees  = trees;
            this.params = params;
        }
    }
    
    static final String INTERIM_CONVERTER = "eps.eval.TreeFunction$TreeFunctionInterimConverter";
    
    private static final class TreeFunctionInterimConverter extends eps.json.InterimConverter<TreeFunction, Object> {
        TreeFunctionInterimConverter() {
            super(TreeFunction.class, Object.class);
        }
        @Override
        public Object toInterim(Object obj) {
            return ((TreeFunction) obj).toInterimObject();
        }
        @Override
        public TreeFunction fromInterim(Object obj) {
            throw new UnsupportedOperationException();
        }
    }
    
    private TreeFunction(Symbol owner, InterimContainer cont) {
        this.owner  = Objects.requireNonNull(owner,      "owner");
        this.trees  = Misc.requireNonNull(cont.trees, "trees");
        this.params = Objects.requireNonNull(cont.params, "params");
        for (Tree tree : trees) {
            Objects.requireNonNull(tree, "tree");
        }
        params.setSelf(this);
    }
    
    Object toInterimObject() {
        return new InterimContainer(this.trees, this.params);
    }
    
    static TreeFunction fromInterimObject(Symbol owner, Object obj) {
        return new TreeFunction(owner, (InterimContainer) obj);
    }
    
    Operand evaluate(Context context, Node... args) {
        return evaluate0(false, context, args).get();
    }
    
    Optional<Operand> evaluateIfRebuilt(Context context, Node... args) {
        return evaluate0(true, context, args);
    }
    
    Optional<Operand> evaluate(boolean onlyIfRebuilt, Context context, Node... args) {
        return evaluate0(onlyIfRebuilt, context, args);
    }
    
    private Optional<Operand> evaluate0(boolean onlyIfRebuilt, Context context, Node... args) {
        try (Context.Depth depth = context.enter()) {
            return evaluate1(onlyIfRebuilt, context, args);
        }
    }
    
    private Optional<Operand> evaluate1(boolean onlyIfRebuilt, Context context, Node[] args) {
        Parameters params = this.params;
        
        if (params.count() == 0) {
            // Note: this would be an error on the part of the method calling code.
            if (args != null && args.length != 0) {
                throw Errors.newIllegalArgumentF("%s with arguments %s", this, Arrays.toString(args));
            }
            // Early opt-out if we have zero arguments (don't need a new scope).
            return evaluate2(onlyIfRebuilt, context);
        }
        
        Symbols scope = Symbols.Concurrent.empty();
        params.assignArguments(context, scope, args);
        
        Symbols prev = context.getScope();
        context.setScope(new Symbols.Overlay(scope, prev));
        try {
            return evaluate2(onlyIfRebuilt, context);
        } finally {
            context.setScope(prev);
        }
    }
    
    private static final Operand NIL = new StringLiteral("__TreeFunction_internal_nil__"); // do not expose!
    
    // Creates a Tuple if the function was originally presented with
    // more than one body node, else simply evaluates the body.
    private Optional<Operand> evaluate2(boolean onlyIfRebuilt, Context context) {
        Tree[] trees = this.trees;
        int    count = trees.length;
//        Log.low(TreeFunction.class, "evaluate2", "this = ", this);
        
        if (count > 1) {
            Tuple.Builder builder = Tuple.builder(count);
            
            int computed = 0;
            
            for (int i = 0; i < count; ++i) {
//                Log.low(TreeFunction.class, "evaluate2", "trees[", i, "] = ", trees[i]);
                
                Optional<Operand> e = trees[i].useTree(onlyIfRebuilt, context, Node.EVALUATE);
                if (e.isPresent()) {
                    ++computed;
                    builder.append(e.get());
                } else {
                    builder.append(NIL);
                }
            }
            
            if (computed == 0) {
                assert onlyIfRebuilt : this;
                return Optional.empty();
            }
            
            // If onlyIfRebuilt is true and only some were rebuilt,
            // we must force them all to re-evaluate.
            if (computed != count) {
                assert onlyIfRebuilt : this;
                for (int i = 0; i < count; ++i) {
                    if (builder.get(i) == NIL) {
                        builder.set(i, trees[i].evaluate(context));
                    }
                }
            }
            
            return Optional.of(builder.build());
            
        } else {
            return trees[0].useTree(onlyIfRebuilt, context, Node.EVALUATE);
        }
    }
    
    private Parameters createParameters(Context context, Symbol owner, Node.OfOperator decl) {
        List<Node> operands = decl.getOperands();
        int        count    = operands.size();
        boolean    isNary   = false;
        
        if (owner.isNaryOp()) {
            assert count == 2 : decl;
            assert SpecialSyms.isEllipse(context, operands.get(1)) : decl;
            
            isNary = true;
            count  = 1;
        }
        
        Parameter[] params = new Parameter[count];
        
        for (int i = 0; i < count; ++i) {
            params[i] = createParameters(decl, operands.get(i), isNary);
        }
        
//        return isNary ? new NaryParameters(this, params) : new Parameters(this, params);
        return new Parameters(this, params);
    }
    
    private Parameter createParameters(Node.OfOperator decl, Node param, boolean isNary) {
        param = param.getExpressionNode();
        
        if (isNary) {
            // We don't want to create groups. That's probably meaningless,
            // considering how n-ary parameters work in parallel.
            return createVariableParameter(decl, param, true);
        }
        
        switch (param.getNodeKind()) {
            case NARY_OP:
                Node.OfNaryOp nary = (Node.OfNaryOp) param;

                if (nary.getOperator().getDelegate() != NaryOp.TUPLE) {
                    throw invalidParameterDeclaration(decl, nary);
                }

                List<Node>  ops   = nary.getOperands();
                int         count = ops.size();
                Parameter[] group = new Parameter[count];
                for (int i = 0; i < count; ++i) {
                    // This means somebody did something like
                    group[i] = createParameters(decl, ops.get(i), false);
                }

                return new ParameterGroup(this, group);
                
            default:
                return createVariableParameter(decl, param, false);
        }
    }
    
    private Parameter createVariableParameter(Node.OfOperator decl, Node node, boolean isNary) {
        node = node.getExpressionNode();
        
        if (node.getNodeKind().isOperand()) {
            Operand op = ((Node.OfOperand) node).get();
            
            switch (op.getOperandKind()) {
                case VARIABLE:
                    Variable var = Variable.create((Variable) op);
                    if (isNary) {
                        return new NaryVariableParameter(this, var);
                    } else {
                        return new VariableParameter(this, var);
                    }
                default:
                    break;
            }
        }
        
        throw invalidParameterDeclaration(decl, node);
    }
    
    public Node getAssignment() {
        Tree[] trees = this.trees;
        
        Node n = trees[0].getRoot(true);
        for (;;) {
            if (n.isBinaryOp()) {
                BinaryOp op = ((Node.OfBinaryOp) n).getOperator();
                if (op.getDelegate() == SpecialSyms.ASSIGN)
                    return n;
            }
            
            n = n.getParent();
            if (n == null)
                return null;
        }
    }
    
    public String getBody() {
        Tree[] trees = this.trees;
        if (trees.length == 1) {
            return trees[0].getSource();
        } else {
            return getExpressionSource().toString();
        }
    }
    
    public Tree getBodyTree() {
        Tree[] trees = this.trees;
        if (trees.length == 1) {
            return trees[0];
        }
        return null;
    }
    
    public Substring getExpressionSource() {
        Tree[]    trees = this.trees;
        int       count = trees.length;
        Substring first = trees[0].getRoot(true).getExpressionSource();
        if (count > 1) {
            return first.interpolate(trees[count - 1].getRoot(true).getExpressionSource());
        } else {
            return first;
        }
    }
    
    public String getExpressionSource(Context context) {
        Tree[]    trees = this.trees;
        int       count = trees.length;
        Substring first = trees[0].useTree(context, (r, c) -> r).getExpressionSource();
        if (count > 1) {
            return first.interpolate(trees[count - 1].useTree(context, (r, c) -> r).getExpressionSource()).toString();
        } else {
            return first.toString();
        }
    }
    
    private static final ToString<TreeFunction> TO_STRING =
        ToString.builder(TreeFunction.class)
                .add("owner",  tf -> tf.owner)
                .add("trees",  tf -> Arrays.toString(tf.trees))
                .add("params", tf -> tf.params)
                .build();
    
    @Override
    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean multiline) {
        return TO_STRING.apply(this, multiline);
    }
    
    public Parameters getParameters() {
        return params;
    }
    
    @JsonSerializable
    public static abstract class Parameter {
        volatile TreeFunction self;
        
        Parameter(TreeFunction self) {
            this.self = self;
        }
        
        @JsonConstructor
        Parameter() {
            this(null);
        }
        
        void setSelf(TreeFunction self) {
            if (this.self != null) {
                throw new IllegalStateException();
            }
            this.self = self;
        }
        
        public abstract int count();
        public abstract Parameter get(int index);
        
        abstract void assignArguments(Context context, Symbols scope, Node... args);
        
        abstract boolean multiIndexOf(Parameter param, int current, List<Object> indexes);
        abstract void validate(Set<String> names);
        
        void validate(Set<String> names, String name) {
            if (!names.add(name)) {
                throw EvaluationException.fmt("duplicate parameter name '%s' in declaration for %s", name, self.owner);
            }
        }
    }
    
    @JsonSerializable
    public static class Parameters extends Parameter {
        @JsonProperty
        final Parameter[] params;
        
        Parameters(TreeFunction self) {
            super(self);
            this.params = EMPTY_PARAMETERS;
        }
        
        Parameters(TreeFunction self, Parameter... params) {
            super(self);
            this.params = Misc.requireNonNull(params, "params");
        }
        
        Parameters(TreeFunction self, String... names) {
            super(self);
            int         count  = (names == null) ? 0 : names.length;
            Parameter[] params = new Parameter[count];
            for (int i = 0; i < count; ++i) {
                params[i] = new VariableParameter(self, names[i]);
            }
            this.params = params;
        }
        
        @JsonConstructor
        Parameters(Parameter[] params) {
            this(null, params);
        }
        
        @Override
        void setSelf(TreeFunction self) {
            super.setSelf(self);
            for (Parameter param : params)
                param.setSelf(self);
        }
        
        @Override
        boolean multiIndexOf(Parameter param, int current, List<Object> indexes) {
            assert current == 0 : current;
            int count = params.length;
            for (int i = 0; i < count; ++i) {
                if (params[i].multiIndexOf(param, i, indexes))
                    return true;
            }
            return false;
        }
        
        public Parameter[] getParameters() {
            return params.clone();
        }
        
        @Override
        public int count() {
            return params.length;
        }
        
        @Override
        public Parameter get(int index) {
            return params[index];
        }
        
        @Override
        void assignArguments(Context context, Symbols scope, Node... args) {
            Parameter[] params = this.params;
            int         count  = args.length;
            
            if (count != params.length) {
                // Note: This is an error on the part of the method calling code, not the user.
                throw Errors.newIllegalArgumentF("tree function for %s with %d parameters invoked with %d arguments",
                                                 self.owner.getName(), params.length, count);
            }
            
            for (int i = 0; i < count; ++i) {
                params[i].assignArguments(context, scope, args[i]);
            }
        }
        
        @Override
        void validate(Set<String> names) {
            for (Parameter p : params)
                p.validate(names);
        }
        
        @Override
        public String toString() {
            return Arrays.toString(params);
        }
    }
    
    @Deprecated // due to problems with parallel arguments
    private static class NaryParameters extends Parameters {
        NaryParameters(TreeFunction self, Parameter[] params) {
            super(self, params);
            assert false : this;
        }
        
        @Override
        void assignArguments(Context context, Symbols scope, Node... args) {
            Parameter[] params     = this.params;
            int         paramCount = params.length;
            
            if (paramCount == 1) {
                params[0].assignArguments(context, scope, args);
                return;
            }
            
            int    argCount = args.length;
            Node[] parallel = new Node[argCount];
            
            for (int i = 0; i < paramCount; ++i) {
                for (int j = 0; j < argCount; ++j) {
                    Node.OfNaryOp nary;
                    if (i == 0) {
                        Node arg = args[j].getExpressionNode();
                        if (!arg.isNaryOp()) {
                            throw self.illegalOperatorArgument(this, arg, 1);
                        }
                        
                        nary = (Node.OfNaryOp) arg;
                        if (nary.getOperator().getDelegate() != NaryOp.TUPLE) {
                            throw self.illegalOperatorArgument(this, arg, 1);
                        }
                    } else {
                        nary = (Node.OfNaryOp) args[j].getExpressionNode();
                    }
                    
                    List<Node> ops = nary.getOperands();
                    if (ops.size() != paramCount) {
                        throw self.illegalOperatorArgument(this, nary, ops.size());
                    }
                    
                    parallel[j] = ops.get(i);
                }
                
                params[i].assignArguments(context, scope, parallel);
            }
        }
    }
    
    @JsonSerializable
    public static class ParameterGroup extends Parameter {
        @JsonProperty
        final Parameter[] group;
        
        ParameterGroup(TreeFunction self, Parameter[] group) {
            super(self);
            this.group = Misc.requireNonNull(group, "group");
        }
        
        @JsonConstructor
        ParameterGroup(Parameter[] group) {
            this(null, group);
        }
        
        @Override
        void setSelf(TreeFunction self) {
            super.setSelf(self);
            for (Parameter param : group)
                param.setSelf(self);
        }
        
        public Parameter[] getGroup() {
            return group.clone();
        }
        
        @Override
        public int count() {
            return group.length;
        }
        
        @Override
        public Parameter get(int index) {
            return group[index];
        }
        
        @Override
        boolean multiIndexOf(Parameter param, int current, List<Object> indexes) {
            int last = indexes.size();
            indexes.add(current);
            if (this == param) {
                return true;
            }
            for (int i = 0; i < group.length; ++i)
                if (group[i].multiIndexOf(param, i, indexes))
                    return true;
            indexes.remove(last);
            return false;
        }
        
        @Override
        void assignArguments(Context context, Symbols scope, Node... args) {
            Parameter[] group = this.group;
            int         count = group.length;
            
            if (count > 1) {
                assert args.length == 1 : Arrays.toString(args);
                Node arg = args[0].getExpressionNode();
                
                // expecting a tuple; for example log(b, n) expects 2 arguments.
                if (!arg.getNodeKind().isNaryOp()) {
                    throw self.illegalOperatorArgument(this, arg, 1);
                }
                
                Node.OfNaryOp nary = (Node.OfNaryOp) arg;
                
                // somebody did something like log(2; 3) which is just log(3)
                if (nary.getOperator().getDelegate() != NaryOp.TUPLE) {
                    throw self.illegalOperatorArgument(this, arg, 1);
                }
                
                List<Node> ops = nary.getOperands();
                
                // log(2, 3, 4)
                if (ops.size() != count) {
                    throw self.illegalOperatorArgument(this, arg, ops.size());
                }
                
                Node[] temp = new Node[1];
                for (int j = 0; j < count; ++j) {
                    temp[0] = ops.get(j);
                    group[j].assignArguments(context, scope, temp);
                }
            } else {
                assert count == 1 : self;
                group[0].assignArguments(context, scope, args);
            }
        }
        
        @Override
        void validate(Set<String> names) {
            for (Parameter p : group)
                p.validate(names);
        }
        
        @Override
        public String toString() {
            return Arrays.toString(group);
        }
    }
    
    private static final class VariableInterimConverter extends InterimConverter<Variable, String> {
        private VariableInterimConverter() {
            super(Variable.class, String.class);
        }
        @Override
        public String toInterim(Object obj) {
            return ((Variable) obj).getName();
        }
        @Override
        public Variable fromInterim(Object obj) {
            return Variable.create((String) obj);
        }
    }
    
    @JsonSerializable
    public static class VariableParameter extends Parameter {
        @JsonProperty(interimConverter = "eps.eval.TreeFunction$VariableInterimConverter")
        final Variable var;
        
        VariableParameter(TreeFunction self, String name) {
            super(self);
            this.var = Variable.create(name);
        }
        
        VariableParameter(TreeFunction self, Variable var) {
            super(self);
            this.var = Objects.requireNonNull(var, "var");
        }
        
        @JsonConstructor
        VariableParameter(Variable var) {
            this(null, var);
        }
        
        public Variable getVariable() {
            return var;
        }
        
        @Override
        public int count() {
            return 1;
        }
        
        @Override
        public Parameter get(int index) {
            if (index == 0)
                return this;
            throw Errors.newIndexOutOfBounds("index", index);
        }
        
        @Override
        boolean multiIndexOf(Parameter param, int current, List<Object> indexes) {
            return (this == param) ? (indexes.add(current) | true) : false;
        }
        
        @Override
        void assignArguments(Context context, Symbols scope, Node... args) {
            assert args.length == 1 : Arrays.toString(args);
            Node arg = args[0].getExpressionNode();
            scope.put(Variable.fromTree(var.getName(), context, arg, arg.evaluate(context)));
        }
        
        @Override
        void validate(Set<String> names) {
            validate(names, var.getName());
        }
        
        @Override
        public String toString() {
            return var.toString();
        }
    }
    
    @JsonSerializable
    private static class NaryVariableParameter extends VariableParameter {
        NaryVariableParameter(TreeFunction self, Variable var) {
            super(self, var);
        }
        
        NaryVariableParameter(TreeFunction self, String name) {
            super(self, name);
        }
        
        @JsonConstructor
        NaryVariableParameter(Variable var) {
            super(var);
        }
        
        @Override
        void assignArguments(Context context, Symbols scope, Node... args) {
            Node.OfNaryOp nary = (Node.OfNaryOp) args[0].getExpressionNode();
            
            Variable arg;
            
            List<Node> operands = nary.getOperands();
            if (operands.size() == 2 && SpecialSyms.isEllipse(context, operands.get(1))) {
                // ellipse form x, ...
                Node lhs = operands.get(0);
                arg = Variable.fromTree(var.getName(), context, lhs, lhs.evaluate(context));
            } else {
                arg = Variable.fromTree(var.getName(), context, operands);
            }
            
            scope.put(arg);
        }
    }
    
    private EvaluationException invalidParameterDeclaration(Node.OfOperator decl, Node param) {
        Object expr = param.getExpressionSource();
        if (param.isOperand()) {
            Operand op = ((Node.OfOperand) param).get();
            if (op.isEmpty()) {
                expr = "empty operand";
            }
        }
        return invalidParameterDeclaration(expr, decl.getExpressionSource());
    }
    
    private EvaluationException invalidParameterDeclaration(TreeAdapter.Node<?, String> node) {
        Object expr = node;
        if (node.isEmpty()) {
            expr = "empty operand";
        }
        return invalidParameterDeclaration(expr, node.getRoot());
    }
    
    private EvaluationException invalidParameterDeclaration(Object expr, Object params) {
        return EvaluationException.fmt("invalid %s declaration; %s in the parameter(s): %s",
                                       owner.getSymbolKind().toLowerCase(),
                                       expr,
                                       params);
    }
    
    private EvaluationException illegalOperatorArgument(Parameter param, Node arg, int found) {
        Symbol.Kind symKind = owner.getSymbolKind();
        if (symKind.isNaryOp()) {
            return EvaluationException.fmt("arguments to %s must have %s element(s); found %d: (%s)",
                                           owner.getName(),
                                           params.count(),
                                           found,
                                           arg.getExpressionSource());
        }
        
        List<Object> indexes = new ArrayList<>(3);
        if (!params.multiIndexOf(param, 0, indexes)) {
            throw Errors.newIllegalArgumentF("param = %s, this = %s", param, this);
        }
        
        int index0 = (Integer) indexes.get(0);
        switch (symKind) {
            case UNARY_OP:
            case SPAN_OP:
                if (index0 != 0)
                    break;
                if (indexes.size() != 1) {
                    indexes.remove(0);
                    break;
                }
                return EvaluationException.fmt("required %d argument(s) to operator %s; found %d: (%s)",
                                               param.count(),
                                               owner.getName(),
                                               found,
                                               arg.getExpressionSource());
            case BINARY_OP:
                if (index0 != 0 && index0 != 1)
                    break;
                if (indexes.size() != 1) {
                    indexes.set(0, index0 == 0 ? "left" : "right");
                    break;
                }
                return EvaluationException.fmt("required %d argument(s) to %s-hand side of operator %s; found %d: (%s)",
                                               params.params[index0].count(),
                                               (index0 == 0 ? "left" : "right"),
                                               owner.getName(),
                                               found,
                                               arg.getExpressionSource());
        }
        return EvaluationException.fmt("required %d element(s) in argument [%s] to operator %s; found %d: (%s)",
                                       param.count(),
                                       j(indexes, ", ", Object::toString),
                                       owner.getName(),
                                       found,
                                       arg.getExpressionSource());
    }
    
    private static final Parameter[] EMPTY_PARAMETERS = new Parameter[0];
    
    public static boolean isEqual(TreeFunction lhs, TreeFunction rhs) {
        return Objects.equals(lhs, rhs);
    }
}

//@Deprecated
//final class TreeFunction_old {
//    private final Symbol       owner;
//    private final Tree         tree;
//    private final Variable[][] params;
//    
//    TreeFunction_old(Context context, Variable owner, Node body) {
//        this(context, owner, (Node.OfOperator) null, body, (Void) null);
//    }
//    
//    TreeFunction_old(Context context, Operator owner, Node.OfOperator decl, Node body) {
//        this(context, owner, Objects.requireNonNull(decl, "decl"), body, (Void) null);
//    }
//    
//    private TreeFunction_old(Context context, Symbol owner, Node.OfOperator decl, Node body, Void $private) {
//        Objects.requireNonNull(context, "context");
//        Objects.requireNonNull(body,    "body");
//        
//        boolean isVariable = owner.isVariable();
//        
//        this.owner  = Objects.requireNonNull(owner, "owner");
//        this.tree   = Tree.fromNode(context, body, isVariable);
//        this.params = isVariable ? new Variable[0][] : createParameters(context, owner, decl);
//        
//        Log.low(TreeFunction.class, "<init>",
//                () -> f("created tree function for %s with params = %s, tree = %s", 
//                        owner, Arrays.deepToString(params), tree));
//    }
//    
//    Tree getTree() {
//        return tree;
//    }
//    
//    Operand evaluate(Context context, Node... args) {
//        return this.evaluate(false, context, args).get();
//    }
//    
//    Optional<Operand> evaluateIfRebuilt(Context context, Node... args) {
//        return this.evaluate(true, context, args);
//    }
//    
//    Optional<Operand> evaluate(boolean onlyIfRebuilt, Context context, Node... args) {
//        try (Context.Depth depth = context.enter()) {
//            return this.evaluate0(onlyIfRebuilt, context, args);
//        }
//    }
//    
//    // TODO: the issue with n-ary operators is that we can't easily
//    //       make a variable from the Node.OfNaryOp because Variable.FromTree
//    //       simply evaluates the node. Potentially we could build a new tree
//    //       which creates a tuple from the List<Node> operands.
//    //       or a special kind of Variable which takes a Node.OfNaryOp and
//    //       produces a tuple. that could be better.
//    private Optional<Operand> evaluate0(boolean onlyIfRebuilt, Context context, Node[] args) {
//        int          count  = (args == null) ? 0 : args.length;
//        Variable[][] params = this.params;
//        
//        if (count != params.length) {
//            // Note: This is an error on the part of the method caller, not the user.
//            throw Errors.newIllegalArgumentF("tree function for %s with %d parameters invoked with %d arguments",
//                                             owner.getName(), params.length, count);
//        }
//        
//        if (count == 0) {
//            // Early opt-out if we have zero arguments (don't need a new scope).
//            return this.tree.useTree(onlyIfRebuilt, context, Node.EVALUATE);
//        }
//        
//        Symbols scope = Symbols.Concurrent.empty();
//        
//        for (int i = 0; i < count; ++i) {
//            Node arg = args[i].getExpressionNode();
//            
//            Variable[] params_i = params[i];
//            int        count_i  = params_i.length;
//            
//            if (count_i > 1) {
//                // expecting a tuple
//                if (!arg.getNodeKind().isNaryOp()) {
//                    throw this.illegalOperatorArgument(i, arg, 1);
//                }
//                
//                Node.OfNaryOp nary = (Node.OfNaryOp) arg;
//                
//                if (nary.getOperator().getDelegate() != NaryOp.TUPLE) {
//                    throw this.illegalOperatorArgument(i, arg, 1);
//                }
//                
//                List<Node> ops = nary.getOperands();
//                
//                if (ops.size() != count_i) {
//                    throw this.illegalOperatorArgument(i, arg, ops.size());
//                }
//                
//                for (int j = 0; j < count_i; ++j) {
//                    scope.put(createArgument(context, params_i[j], ops.get(j)));
//                }
//            } else {
//                scope.put(createArgument(context, params_i[0], arg));
//            }
//        }
//        
//        Symbols prev = context.getScope();
//        context.setScope(new Symbols.Overlay(scope, prev));
//        try {
//            return this.tree.useTree(onlyIfRebuilt, context, Node.EVALUATE);
//        } finally {
//            context.setScope(prev);
//        }
//    }
//    
//    private static Variable createArgument(Context context, Variable param, Node node) {
//        return Variable.fromTree(param.getName(),
//                                 context,
//                                 node,
//                                 node.evaluate(context));
//    }
//    
//    private static Variable createParameter(Node.OfOperator decl, Node node) {
//        node = node.getExpressionNode();
//        
//        if (node.getNodeKind().isOperand()) {
//            Operand op = ((Node.OfOperand) node).get();
//            
//            switch (op.getOperandKind()) {
//                case VARIABLE:
//                    return Variable.create((Variable) op);
//                default:
//                    break;
//            }
//        }
//        
//        throw invalidParameterDeclaration(decl, node);
//    }
//    
//    private static Variable[][] createParameters(Context context, Symbol owner, Node.OfOperator decl) {
//        List<Node> operands = decl.getOperands();
//        int        count    = operands.size();
//        
//        if (owner.isNaryOp()) {
//            assert count == 2 : decl;
//            assert SpecialSyms.isEllipse(context, operands.get(1)) : decl;
//            // Remove the ellipse.
//            operands = Collections.singletonList(operands.get(0));
//        }
//        
//        Variable[][] vars = new Variable[count][];
//        
//        for (int i = 0; i < count; ++i) {
//            vars[i] = createParameters(decl, operands.get(i));
//        }
//        
//        return vars;
//    }
//    
//    private static Variable[] createParameters(Node.OfOperator decl, Node param) {
//        param = param.getExpressionNode();
//        
//        switch (param.getNodeKind()) {
//            case NARY_OP:
//                Node.OfNaryOp nary = (Node.OfNaryOp) param;
//
//                if (nary.getOperator().getDelegate() != NaryOp.TUPLE) {
//                    throw invalidParameterDeclaration(decl, nary);
//                }
//
//                List<Node> ops    = nary.getOperands();
//                int        count  = ops.size();
//                Variable[] params = new Variable[count];
//                for (int i = 0; i < count; ++i) {
//                    params[i] = createParameter(decl, ops.get(i));
//                }
//
//                return params;
//                
//            default:
//                return new Variable[] { createParameter(decl, param) };
//        }
//    }
//    
//    @Override
//    public String toString() {
//        return tree.toString();
//    }
//    
//    private static EvaluationException invalidParameterDeclaration(Node.OfOperator decl, Node param) {
//        return EvaluationException.fmt("invalid %s declaration; %s in the parameter(s)",
//                                       decl.getOperator().getKindDescription(),
//                                       param.getExpressionSource());
//    }
//    
//    private EvaluationException illegalOperatorArgument(int index, Node arg, int found) {
//        switch (owner.getSymbolKind()) {
//            case UNARY_OP:
//            case SPAN_OP:
//                if (index != 0)
//                    break;
//                return EvaluationException.fmt("required %d argument(s) to operator %s; found %d: %s",
//                                               params[index].length,
//                                               owner.getName(),
//                                               found,
//                                               arg.getExpressionSource());
//                
//            case BINARY_OP:
//                if (index != 0 && index != 1)
//                    break;
//                return EvaluationException.fmt("required %d argument(s) to %s-hand side of operator %s; found %d: %s",
//                                               params[index].length,
//                                               (index == 0 ? "left" : "right"),
//                                               owner.getName(),
//                                               found,
//                                               arg.getExpressionSource());
//        }
//        
//        assert false : f("index = %d, arg = %s, found = %d", index, arg, found);
//        
//        return EvaluationException.fmt("required %d element(s) in argument %d to operator %s; found %d: %s",
//                                       params[index].length,
//                                       index,
//                                       owner.getName(),
//                                       found,
//                                       arg.getExpressionSource());
//    }
//    
//    @Deprecated
//    private void requireNoRecursiveCalls(Node body) {
//        body.forEach(node -> {
//            if (node.isOperator()) {
//                Operator op = ((Node.OfOperator) node).getOperator();
//                if (op.getSymbolKind() == owner.getSymbolKind()) {
//                    String name = op.getName();
//                    if (name.equals(owner.getName())) {
//                        throw UnsupportedException.fmt("found recursive call of %s", name);
//                    }
//                }
//            }
//        });
//    }
//}