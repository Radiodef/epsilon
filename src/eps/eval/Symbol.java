/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.block.*;
import eps.util.*;
import eps.ui.*;
import eps.app.*;
import static eps.app.App.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;

import java.util.*;

public interface Symbol {
    String getName();
    Kind getSymbolKind();
    
    default Set<String> getPreferredAliases() {
        return Collections.singleton(getName());
    }
    
    default Symbol[] createPreferredAliases() {
        Set<String>      names = getPreferredAliases();
        Iterator<String> iter  = names.iterator();
        
        Symbol[] aliases = new Symbol[names.size()];
        for (int i = 0; i < aliases.length; ++i) {
            aliases[i] = createAlias(iter.next());
        }
        
        return aliases;
    }
    
    default Symbol createAlias(Symbol sym) {
        return this.createAlias(sym, null);
    }
    
    default Symbol createAlias(Symbol sym, EchoFormatter echoFormatter) {
        return this.createAlias(sym.getName(), echoFormatter);
    }
    
    default Symbol createAlias(String aliasName) {
        return this.createAlias(aliasName, null);
    }
    
    Symbol createAlias(String aliasName, EchoFormatter echoFormatter);
    
    default boolean isAlias() {
        return this.getDelegate() != this;
    }
    
    default Symbol getDelegate() {
        return this;
    }
    
    Symbol configure(Builder<?, ?> builder);
    
    default Builder<?, ?> createBuilder() {
        Builder<?, ?> b = getSymbolKind().createBuilder();
        b.setAllUnchecked(this);
        return b;
    }
    
    default String toDebugString() {
        return createBuilder().toString();
    }
    
//    String synthesize(Symbols syms);
//    
//    default String synthesize() {
//        return synthesize(app().getSymbols());
//    }
    
    default Collection<Symbol> getSymbols() {
        return Collections.singleton(this);
    }
    
    default String getKindDescription() {
        return getSymbolKind().toLowerCase();
    }
    
    EchoFormatter getEchoFormatter();
    
    default StringBuilder toEchoString(StringBuilder b, Context context, Node node) {
        return this.getEchoFormatter().format(b, context, node);
    }
    
    default Block toBlock(Context context, Node node) {
        return this.toBlock(new StringBuilder(), context, node);
    }
    
    default Block toBlock(StringBuilder temp, Context context, Node node) {
        temp.setLength(0);
        return this.getEchoFormatter().toBlock(temp, context, node);
    }
    
    default boolean isVariable() { return getSymbolKind().isVariable(); }
    default boolean isOperator() { return getSymbolKind().isOperator(); }
    default boolean isUnaryOp()  { return getSymbolKind().isUnaryOp();  }
    default boolean isBinaryOp() { return getSymbolKind().isBinaryOp(); }
    default boolean isNaryOp()   { return getSymbolKind().isNaryOp();   }
    default boolean isSpanOp()   { return getSymbolKind().isSpanOp();   }
    
    default Setting<Style> getStyleSetting() {
        return null;
    }
    
    default boolean isSameKind(Symbol that) {
        return this.getSymbolKind() == that.getSymbolKind();
    }
    
    boolean isEqualTo(Symbol that);
    
    static boolean isEqual(Symbol lhs, Symbol rhs) {
        if (lhs == null)
            return rhs == null;
        if (rhs == null)
            return false;
        return lhs.isEqualTo(rhs);
    }
    
    static boolean isDuplicate(Iterable<? extends Symbol> syms, Symbol sym, String name) {
        return isDuplicate(syms, sym, null, name);
    }
    
    static boolean isDuplicate(Iterable<? extends Symbol> syms, Symbol sym, Kind kind, String name) {
        if (kind == null) {
            kind = Objects.requireNonNull(sym, "sym").getSymbolKind();
        }
        if (name == null) {
            name = Objects.requireNonNull(sym, "sym").getName();
        }
        if (syms == null) {
            return false;
        }
        for (Symbol elem : syms) {
            if (elem == sym)
                continue;
            if (elem == null)
                continue;
            if (elem.getSymbolKind() != kind)
                continue;
            if (Objects.equals(elem.getName(), name))
                return true;
            if (elem.isSpanOp()) {
                SpanOp span = (SpanOp) elem;
                if (!span.isSymmetric()) {
                    if (Objects.equals(span.getLeftName(), name))
                        return true;
                    if (Objects.equals(span.getRightName(), name))
                        return true;
                }
            }
        }
        return false;
    }
    
    default boolean hasName(String name) {
        return getName().equals(name);
    }
    
    default boolean hasName(Symbol that) {
        if (that.isSpanOp()) {
            SpanOp span = (SpanOp) that;
            if (!span.isSymmetric()) {
                return hasName(span.getLeftName()) || hasName(span.getRightName());
            }
        }
        return hasName(that.getName());
    }
    
    static boolean anySameName(Symbol lhs, Symbol rhs) {
        if (lhs == null || rhs == null)
            return false;
        return lhs.hasName(rhs);
    }
    
    public enum Kind {
        VARIABLE ("variable",  "variable",        "Variable"       ),
        UNARY_OP ("unary op",  "unary operator",  "Unary Operator" ),
        BINARY_OP("binary op", "binary operator", "Binary Operator"),
        NARY_OP  ("n-ary op",  "n-ary operator",  "N-ary Operator" ),
        SPAN_OP  ("span op",   "span operator",   "Span Operator"  );
        
        private final String small;
        private final String lower;
        private final String title;
        
        private Kind(String small, String lower, String title) {
            this.small = small;
            this.lower = lower;
            this.title = title;
        }
        
        public String toShortString() { return small; }
        public String toLowerCase()   { return lower; }
        public String toTitleCase()   { return title; }
        
        private static final String UNARY_PREFIX_LOWER = "unary prefix operator";
        private static final String UNARY_SUFFIX_LOWER = "unary suffix operator";
        private static final String LEFT_SPAN_LOWER    = "left span operator";
        private static final String RIGHT_SPAN_LOWER   = "right span operator";
        
        public String toLowerCase(Symbol sym) {
            if (sym != null) {
                switch (this) {
                case UNARY_OP:
                    if (sym.isUnaryOp()) {
                        UnaryOp unary = (UnaryOp) sym;
                        if (unary.isPrefix()) return UNARY_PREFIX_LOWER;
                        if (unary.isSuffix()) return UNARY_SUFFIX_LOWER;
                    }
                    break;
                case SPAN_OP:
                    if (sym.isSpanOp()) {
                        SpanOp span = (SpanOp) sym;
                        if (span.isLeft())  return LEFT_SPAN_LOWER;
                        if (span.isRight()) return RIGHT_SPAN_LOWER;
                    }
                    break;
                }
            }
            return toLowerCase();
        }
        
        public String toLowerCase(Token t) {
            if (t != null && t.isSymbol()) {
                switch (this) {
                case UNARY_OP:
                    if (t.isUnaryPrefix()) return UNARY_PREFIX_LOWER;
                    if (t.isUnarySuffix()) return UNARY_SUFFIX_LOWER;
                    break;
                case SPAN_OP:
                    if (t.isLeftSpan())  return LEFT_SPAN_LOWER;
                    if (t.isRightSpan()) return RIGHT_SPAN_LOWER;
                    break;
                }
            }
            return toLowerCase();
        }
        
        public boolean isVariable() { return this == VARIABLE;  }
        public boolean isUnaryOp()  { return this == UNARY_OP;  }
        public boolean isBinaryOp() { return this == BINARY_OP; }
        public boolean isNaryOp()   { return this == NARY_OP;   }
        public boolean isSpanOp()   { return this == SPAN_OP;   }
        
        public boolean isOperand() {
            return this == VARIABLE;
        }
        
        public boolean isOperator() {
            switch (this) {
                case UNARY_OP:
                case BINARY_OP:
                case NARY_OP:
                case SPAN_OP:
                    return true;
                default:
                    return false;
            }
        }
        
        public Symbol.Builder<?, ?> createBuilder() {
            switch (this) {
                case VARIABLE  : return new Variable.Builder();
                case UNARY_OP  : return new UnaryOp.Builder();
                case BINARY_OP : return new BinaryOp.Builder();
                case SPAN_OP   : return new SpanOp.Builder();
                case NARY_OP   : return new NaryOp.Builder();
                default        : throw new UnsupportedOperationException(name());
            }
        }
        
        private static final Map<Class<? extends Symbol>, Kind> BY_CLASS =
            LinkedHashMapOf(Variable.class, VARIABLE,
                            UnaryOp.class,  UNARY_OP,
                            BinaryOp.class, BINARY_OP,
                            SpanOp.class,   SPAN_OP,
                            NaryOp.class,   NARY_OP);
        
        public static Kind valueOf(Class<? extends Symbol> type) {
            Objects.requireNonNull(type, "type");
            Kind kind = BY_CLASS.get(type);
            if (kind == null) {
                for (Map.Entry<Class<? extends Symbol>, Kind> e : BY_CLASS.entrySet()) {
                    if (e.getKey().isAssignableFrom(type)) {
                        return e.getValue();
                    }
                }
                throw new IllegalArgumentException(type.toString());
            }
            return kind;
        }
    }
    
    static void forEachDefault(java.util.function.Consumer<? super Symbol> action) {
        Variable.DEFAULTS.forEach(action);
        UnaryOp.DEFAULTS.forEach(action);
        BinaryOp.DEFAULTS.forEach(action);
        NaryOp.DEFAULTS.forEach(action);
        SpanOp.DEFAULTS.forEach(action);
        SpecialSyms.DEFAULTS.forEach(action);
    }
    
    default boolean isError() {
        if (isAlias()) {
            return getDelegate().isError();
        }
        return false;
    }
    
    interface ErrorSymbol extends Symbol {
        default EvaluationException getCause() {
            return null;
        }
        @Override
        default boolean isError() {
            return true;
        }
    }
    
    static String intrinsify(String name) {
        return (name == null) ? name : ("__" + name + "__");
    }
    
    static String deintrinsify(String name) {
        if (isIntrinsic(name)) {
            name = name.substring(2, name.length() - 2);
        }
        return name;
    }
    
    default boolean isIntrinsic() {
        return isIntrinsic(this.getName());
    }
    
    static boolean isIntrinsic(String name) {
        return name != null && name.length() >= 4 && name.startsWith("__") && name.endsWith("__");
    }
    
    static String requireNonIntrinsic(String name) {
        if (isIntrinsic(name)) {
            throw EvaluationException.fmt("symbols of the form __name__ may not be reassigned (found %s)", name);
        }
        return name;
    }
    
    static Map<?, ?> aliasToMap(Symbol sym) {
        Symbol del = sym.getDelegate();
        if (del == sym) {
            throw new IllegalArgumentException(sym.getName() + " is not an alias");
        }
        
        Map<Object, Object> map = new LinkedHashMap<>(4);
        map.put("kind", sym.getSymbolKind());
        
        if (sym.isSpanOp()) {
            String left  = ((SpanOp) sym).getLeftName();
            String right = ((SpanOp) sym).getRightName();
            if (left.equals(right)) {
                map.put("name", left);
            } else {
                map.put("leftName",  left);
                map.put("rightName", right);
            }
        } else {
            map.put("name", sym.getName());
        }
        
        if (sym.isUnaryOp()) {
            map.put("functionLike", ((UnaryOp) sym).isFunctionLike());
        }
        
        map.put("echoFormatter", sym.getEchoFormatter());
        
        if (del.isIntrinsic()) {
            if (del.isSpanOp()) {
                String left  = ((SpanOp) del).getLeftName();
                String right = ((SpanOp) del).getRightName();
                if (left.equals(right)) {
                    map.put("delegateName", del.getName());
                } else {
                    map.put("delegateLeftName",  left);
                    map.put("delegateRightName", right);
                }
            } else {
                map.put("delegateName", del.getName());
            }
        } else {
            map.put("delegate", del);
        }
        
        return map;
    }
    
    static Symbol mapToAlias(Map<?, ?> map) {
        return mapToAlias(map, Symbols.getLocalSymbols());
//        return mapToAlias(map, app().getSymbols());
    }
    
    static Symbol mapToAlias(Map<?, ?> map, Symbols syms) {
        Symbol.Kind kind = (Symbol.Kind) map.get("kind");
        
        Symbol del = (Symbol) map.get("delegate");
        if (del == null) {
            String name = (String) map.get("delegateName");
            if (name == null) {
                name = (String) map.get("delegateLeftName");
            }
            Symbols.Group group = syms.get(name);
            if (group != null) {
                del = group.get(kind);
            }
        }
        
        Boolean       functionLike  = replnull((Boolean) map.get("functionLike"), false);
        EchoFormatter echoFormatter = (EchoFormatter) map.get("echoFormatter");
        
        if (del == null) {
            String delName = (String) map.get("delegateName");
            String name    = (String) map.get("name");
            switch (kind) {
                case VARIABLE:
                    return Variable.createVariableError(delName).createAlias(name, echoFormatter);
                case UNARY_OP:
                    return UnaryOp.createUnaryError(delName, UnaryOp.Affix.PREFIX).createAlias(name, echoFormatter, functionLike);
                case BINARY_OP:
                    return BinaryOp.createBinaryError(delName).createAlias(name, echoFormatter);
                case NARY_OP:
                    return NaryOp.createNaryError(delName).createAlias(name, echoFormatter);
                case SPAN_OP:
                    return SpanOp.createSpanError((delName == null) ? (String) map.get("delegateLeftName")  : delName,
                                                  (delName == null) ? (String) map.get("delegateRightName") : delName)
                                 .createAlias((name == null) ? (String) map.get("leftName")  : name,
                                              (name == null) ? (String) map.get("rightName") : name,
                                              echoFormatter);
                default:
                    throw new IllegalArgumentException("no pre-existing delegate for " + map.toString());
            }
        }
        
        if (del instanceof SpanOp) {
            del = ((SpanOp) del).getParent();
        }
        
        String name = (String) map.get("name");
        
        if (name == null) {
            return ((SpanOp) del).createAlias((String) map.get("leftName"),
                                              (String) map.get("rightName"),
                                              echoFormatter);
        } else if (del.isUnaryOp()) {
            return ((UnaryOp) del).createAlias(name, echoFormatter, functionLike);
        } else {
            return del.createAlias(name, echoFormatter);
        }
    }
    
    abstract class Builder<S extends Symbol, B extends Builder<S, B>> {
        protected     String       name        = "symbol";
        private       boolean      isIntrinsic = false;
        private final List<String> aliases     = new ArrayList<>(0);
        
        private EchoFormatter  echoFormatter;
        
        @SuppressWarnings("unchecked")
        protected final B self() {
            return (B) this;
        }
        
        @SuppressWarnings("unchecked")
        private void setAllUnchecked(Symbol sym) {
            setAll((S) sym);
        }
        
        public B setAll(S sym) {
            this.setName(sym.getName())
                .setEchoFormatter(sym.getEchoFormatter());
            aliases.clear();
            for (String alias : sym.getPreferredAliases()) {
                this.addPreferredAlias(alias);
            }
            return self();
        }
        
        public String getName() {
            return isIntrinsic ? intrinsify(name) : name;
        }
        
        public B setName(String name) {
            Objects.requireNonNull(name, "name");
            if (Symbol.isIntrinsic(name)) {
                name = Symbol.deintrinsify(name);
                this.isIntrinsic = true;
            } else {
                this.isIntrinsic = false;
            }
            this.name = name;
            return self();
        }
        
        public B setEchoFormatter(EchoFormatter echoFormatter) {
            this.echoFormatter = echoFormatter;
            return self();
        }
        
        public EchoFormatter getEchoFormatter() {
            return echoFormatter;
        }
        
        public boolean isIntrinsic() {
            return isIntrinsic;
        }
        
        public B setIntrinsic(boolean isIntrinsic) {
            this.isIntrinsic = isIntrinsic;
            return self();
        }
        
        public B makeIntrinsic() {
            return setIntrinsic(true);
        }
        
        public B addPreferredAlias(String prefAlias) {
            aliases.add(Objects.requireNonNull(prefAlias, "prefAlias"));
            return self();
        }
        
        public B addPreferredAliases(String... prefAliases) {
            for (String prefAlias : prefAliases) {
                addPreferredAlias(prefAlias);
            }
            return self();
        }
        
        public String[] getPreferredAliases() {
            return aliases.toArray(eps.util.Misc.EMPTY_STRING_ARRAY);
        }
        
        public abstract S build();
        
        public S buildError() {
            return buildError((EvaluationException) null);
        }
        
        public S buildError(EvaluationException cause) {
            throw new UnsupportedOperationException(getClass().toString());
        }
        
        @SuppressWarnings("unchecked")
        protected static final Class<Builder<?, ?>> SYMBOL_BUILDER_TYPE =
            (Class<Builder<?, ?>>) (Class<? super Builder<?, ?>>) Builder.class;
        
        protected static final ToString<Builder<?, ?>> SYMBOL_BUILDER_TO_STRING =
            ToString.builder(Builder.SYMBOL_BUILDER_TYPE)
                    .add("name",          Builder::getName)
                    .add("isIntrinsic",   Builder::isIntrinsic)
                    .add("aliases",       b -> b.aliases)
                    .add("echoFormatter", Builder::getEchoFormatter)
                    .build();
        
        @Override
        public String toString() {
            return toString(false);
        }
        
        public String toString(boolean multiline) {
            return SYMBOL_BUILDER_TO_STRING.apply(this, multiline);
        }
    }
}