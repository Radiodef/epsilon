/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.block.*;
import eps.app.*;

import java.util.*;
import java.util.function.*;

public final class Reference implements Operand, Supplier<Symbol> {
    private final Symbol referent;
    
    public Reference(Symbol referent) {
        this.referent = Objects.requireNonNull(referent, "referent");
    }
    
    @Override
    public Symbol get() {
        return referent;
    }
    
    @Override
    public Operand.Kind getOperandKind() {
        return Operand.Kind.REFERENCE;
    }
    
    @Override
    public boolean isTruthy(Context context) {
        if (referent.getSymbolKind().isOperand()) {
            return ((Operand) referent).isTruthy(context);
        }
        return true;
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context c, AnswerFormatter f, MutableBoolean a) {
        return b.append("reference to ").append(referent.getName());
    }
    
    @Override
    public Block toResultBlock(StringBuilder temp, Context context, AnswerFormatter f, MutableBoolean a) {
        Block block = new TextBlock(referent.getName());
        
        if (referent.isVariable()) {
            block = new StyleBlock(block).set(Setting.NUMBER_STYLE);
        } else if (referent.isUnaryOp() && ((UnaryOp) referent).isFunctionLike()) {
            block = new StyleBlock(block).set(Setting.FUNCTION_STYLE);
        }
        
        return Block.join(new TextBlock("reference to"), block).setDelimiter(' ');
    }
    
    @Override
    public Block toSourceBlock(StringBuilder temp, Context context, Node node) {
        return referent.toBlock(temp, context, node);
    }
    
    @Override
    public String toString() {
        return "reference to " + referent.getName();
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(referent);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Reference) {
            Reference that = (Reference) obj;
            return Objects.equals(this.referent, that.referent);
        }
        return false;
    }
}