/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.math.*;
import eps.util.*;
import eps.block.*;
import eps.json.*;
import eps.ui.*;

import java.util.*;

@JsonSerializable
public class Variable extends AbstractSymbol implements Operand {
    @JsonConstructor
    protected Variable(String        name,
                       EchoFormatter echoFormatter,
                       Set<String>   preferredAliases) {
        super(name, echoFormatter, preferredAliases);
    }
    
    public Variable(Builder builder) {
        super(builder);
    }
    
    private Variable(String name, EchoFormatter fmt, Variable delegate) {
        super(name, fmt, delegate);
    }
    
    @Override
    protected EchoFormatter getDefaultEchoFormatter() {
        return EchoFormatter.OF_VARIABLE_NAME;
    }
    
    public Variable resolve(Context context) {
        Symbols       scope = context.getScope();
        Symbols.Group group = scope.get(getName());
        if (group != null) {
            Variable that = (Variable) group.get(Symbol.Kind.VARIABLE);
            if (that != null) {
                return that;
            }
        } // else
        return this;
    }
    
    @Override
    public Operand evaluate(Context context) {
        Variable var = this.resolve(context);
        if (var != this) {
            return var.evaluate(context);
        } else {
            return this.evaluateOtherwise(context);
        }
    }
    
    protected Operand evaluateOtherwise(Context context) {
        throw EvaluationException.unresolved(this);
    }
    
    @Override
    public boolean isTruthy(Context context) {
        return this.evaluate(context).isTruthy(context);
    }
    
    @Override
    public Symbol.Kind getSymbolKind() {
        return Symbol.Kind.VARIABLE;
    }
    
    @Override
    public Operand.Kind getOperandKind() {
        return Operand.Kind.VARIABLE;
    }
    
    @Override
    public boolean isVariable() {
        return true;
    }
    
    @Override
    public Setting<Style> getStyleSetting() {
        return Setting.NUMBER_STYLE;
    }
    
    @Override
    public Variable configure(Symbol.Builder<?, ?> builder) {
        return new Variable((Builder) builder);
    }
    
    @Override
    public Variable createAlias(String aliasName, EchoFormatter fmt) {
        return new Alias(aliasName, fmt, (Variable) getDelegate());
//        return new Alias(aliasName, fmt, this);
    }
    
    @Override
    public StringBuilder toResultString(StringBuilder b, Context context, AnswerFormatter f, MutableBoolean a) {
        return this.evaluate(context).toResultString(b, context, f, a);
    }
    
    @Override
    public Block toResultBlock(StringBuilder temp, Context context, AnswerFormatter f, MutableBoolean a) {
        return this.evaluate(context).toResultBlock(temp, context, f, a);
    }
    
    @Override
    public Block toSourceBlock(Context context, Node node) {
        return super.toBlock(context, node);
    }
    
    @Override
    public Block toSourceBlock(StringBuilder temp, Context context, Node node) {
        return super.toBlock(temp, context, node);
    }
    
    public static class Alias extends Variable {
        private final Variable delegate;
        
        public Alias(String name, Variable delegate) {
            this(name, null, delegate);
        }
        
        public Alias(String name, EchoFormatter fmt, Variable delegate) {
            super(name, fmt, Objects.requireNonNull(delegate, "delegate"));
            this.delegate = delegate;
        }
        
        static {
            JsonObjectAdapter.put(Variable.Alias.class,
                                  Symbol::aliasToMap,
                                  map -> (Variable.Alias) Symbol.mapToAlias(map));
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                Alias that = (Alias) sym;
                return Symbol.isEqual(this.delegate, that.delegate);
            }
            return false;
        }
        
        @Override
        public Symbol getDelegate() {
            return delegate.getDelegate();
        }
        
        @Override
        public Alias configure(Symbol.Builder<?, ?> builder) {
            return new Alias(builder.getName(), builder.getEchoFormatter(), delegate);
        }
        
        // Removed; checks for local variables incorrectly. We want to
        // check for a local variable with the same name as this alias,
        // not the delegate.
//        @Override
//        public Operand evaluate(Context context) {
//            return delegate.evaluate(context);
//        }
        
        @Override
        public Operand evaluateOtherwise(Context context) {
            return delegate.evaluateOtherwise(context);
        }
    }
    
    @JsonSerializable
    public static class VariableError extends Variable implements ErrorSymbol {
        @JsonProperty
        private final EvaluationException cause;
        @JsonDefault("cause")
        private static final EvaluationException DEFAULT_CAUSE = null;
        
        @JsonConstructor
        protected VariableError(String              name,
                                EchoFormatter       echoFormatter,
                                Set<String>         preferredAliases,
                                EvaluationException cause) {
            super(name, echoFormatter, preferredAliases);
            this.cause = cause;
        }
        
        public VariableError(Variable.Builder builder) {
            this(builder, null);
        }
        
        public VariableError(Variable.Builder builder, EvaluationException cause) {
            super(builder);
            this.cause = cause;
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                VariableError that = (VariableError) sym;
                return Objects.equals(this.cause, that.cause); // (bad?) compares object identity
            }
            return false;
        }
        
        @Override
        public EvaluationException getCause() {
            return cause;
        }
        
        @Override
        public boolean isError() {
            return true;
        }
        
        @Override
        public VariableError configure(Symbol.Builder<?, ?> builder) {
            return new VariableError((Variable.Builder) builder, cause);
        }
        
        @Override
        public Operand evaluateOtherwise(Context context) {
            if (cause != null) {
                throw new EvaluationException(cause);
            }
            throw EvaluationException.unresolved(this);
        }
    }
    
    public static Variable create(Token.OfUnresolved u) {
        return create(u.getSource().toString());
    }
    
    public static Variable create(Variable nameToCopy) {
        return create(nameToCopy.getName());
    }
    
    public static Variable create(String name) {
        return new Builder().setName(name).build();
    }
    
    public static VariableError createVariableError(String name) {
        return createVariableError(name, null);
    }
    
    public static VariableError createVariableError(String name, EvaluationException cause) {
        return new Builder().setName(name).buildError(cause);
    }
    
    public static Variable fromTree(String  name,
                                    Context context,
                                    Node    node,
                                    Operand initialResult) {
        return fromTree(name).build(context, node, initialResult);
    }
    
    public static Variable fromTree(String     name,
                                    Context    context,
                                    List<Node> nodes) {
        return fromTree(name).build(context, nodes);
    }
    
    private static Builder fromTree(String name) {
        return new Builder().setName(name);
    }
    
    public static Variable.Temp createTemp(Variable nameToCopy,
                                           Operand  initialValue) {
        return new Builder().setName(nameToCopy.getName()).buildTemp(initialValue);
    }
    
    @JsonSerializable
    public static class FromTree extends Variable {
        @JsonProperty(interimConverter = TreeFunction.INTERIM_CONVERTER,
                      deferInterimToConstructor = true)
        private final TreeFunction treeFn;
        
        private volatile Operand cachedValue;
        
        @JsonConstructor
        protected FromTree(String        name,
                           EchoFormatter echoFormatter,
                           Set<String>   preferredAliases,
                           Object        treeFn) {
            super(name, echoFormatter, preferredAliases);
            this.treeFn = TreeFunction.fromInterimObject(this, treeFn);
        }
        
        public FromTree(Builder builder, Context context, Node node, Operand initialResult) {
            super(builder);
            treeFn      = new TreeFunction(context, this, node);
            cachedValue = initialResult;
            Log.low(FromTree.class, "<init>", "constructing ", node);
        }
        
        // n-ary op parameter variable constructor
        public FromTree(Builder builder, Context context, List<Node> nodes) {
            super(builder);
            treeFn = new TreeFunction(context, this, nodes);
            Log.low(FromTree.class, "<init>", "constructing ", nodes);
        }
        
        public FromTree(String name, String body) {
            this(new Builder().setName(name), body);
        }
        
        public FromTree(Builder builder, String body) {
            super(builder);
            
            treeFn = new TreeFunction(this, body);
        }
        
        private FromTree(Builder builder, TreeFunction treeFn) {
            super(builder);
            
            this.treeFn = treeFn.copy(this);
        }
        
        @Override
        public FromTree configure(Symbol.Builder<?, ?> builder) {
            return new FromTree((Builder) builder, treeFn);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            if (super.isEqualTo(sym)) {
                FromTree that = (FromTree) sym;
                if (!Objects.equals(this.cachedValue, that.cachedValue))
                    return false;
                if (!TreeFunction.isEqual(this.treeFn, that.treeFn))
                    return false;
                return true;
            }
            return false;
        }
        
        TreeFunction getTreeFunction() {
            return treeFn;
        }
        
        public Operand getCachedValue() {
            return cachedValue;
        }
        
        @Override
        public Operand evaluateOtherwise(Context context) {
            Operand cachedValue = this.cachedValue;
            
            Operand result =
                treeFn.evaluate(cachedValue != null, context, (Node[]) null)
                      .orElse(cachedValue);
            this.cachedValue = result;
            
            return result;
        }
    }
    
    // Temp is just a mutable variable
    // Note: This will become an issue if it's ever possible for functions
    //       to have side-effects, such that a Variable.Temp is assigned to
    //       a global variable. If that happens, something else will have to
    //       be done.
    public static class Temp extends Variable {
        private volatile Operand value;
        
        public Temp(Builder builder, Operand value) {
            super(builder);
            this.value = Objects.requireNonNull(value, "value");
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            return super.isEqualTo(sym) && Objects.equals(this.value, ((Temp) sym).value);
        }
        
        @Override
        public Temp configure(Symbol.Builder<?, ?> b) {
            return new Temp((Builder) b, value);
        }
        
        public Operand setValue(Operand value) {
            Operand old = this.value;
            this.value  = Objects.requireNonNull(value, "value");
            return old;
        }
        
        @Override
        public Operand evaluateOtherwise(Context context) {
            return this.value;
        }
    }
    
    @FunctionalInterface
    public interface VariableFunction {
        Operand apply(Variable self, Context c);
    }
    @FunctionalInterface
    public interface VariableFunctionNoSelf extends VariableFunction {
        @Override
        default Operand apply(Variable self, Context c) {
            return apply(c);
        }
        Operand apply(Context c);
    }
    @FunctionalInterface
    public interface VariableFunctionMathEnv extends VariableFunction {
        @Override
        default Operand apply(Variable self, Context c) {
            return apply(c.getMathEnvironment());
        }
        Operand apply(MathEnv env);
    }
    
    @JsonSerializable
    public static class FromFunction extends Variable {
        @JsonProperty
        private final VariableFunction fn;
        
        @JsonConstructor
        protected FromFunction(String           name,
                               EchoFormatter    echoFormatter,
                               Set<String>      preferredAliases,
                               VariableFunction fn) {
            super(name, echoFormatter, preferredAliases);
            this.fn = Objects.requireNonNull(fn, "fn");
        }
        
        public FromFunction(Builder builder) {
            this(builder, builder.fn);
        }
        
        private FromFunction(Builder builder, VariableFunction fn) {
            super(builder);
            this.fn = fn;
        }
        
        @Override
        public FromFunction configure(Symbol.Builder<?, ?> b) {
            return new FromFunction((Builder) b, fn);
        }
        
        @Override
        public boolean isEqualTo(Symbol sym) {
            return super.isEqualTo(sym) && Objects.equals(this.fn, ((FromFunction) sym).fn);
        }
        
        @Override
        public Operand evaluateOtherwise(Context c) {
            return fn.apply(this, c);
        }
    }
    
    public static class Builder extends Symbol.Builder<Variable, Builder> {
        private VariableFunction fn = ConstantFunction.UNRESOLVED;
        
        public Builder() {
            super.setEchoFormatter(EchoFormatter.OF_VARIABLE_NAME);
        }
        
        @Override
        public Builder setAll(Variable var) {
            if (var instanceof Variable.FromFunction) {
                this.setFunction(((Variable.FromFunction) var).fn);
            }
            return super.setAll(var);
        }
        
        public Builder setFunction(VariableFunction fn) {
            this.fn = Objects.requireNonNull(fn ,"fn");
            return this;
        }
        
        public Builder setFunction(VariableFunctionNoSelf fn) {
            return setFunction((VariableFunction) fn);
        }
        
        public Builder setFunction(VariableFunctionMathEnv fn) {
            return setFunction((VariableFunction) fn);
        }
        
        @Override
        public Variable build() {
            return new Variable.FromFunction(this);
        }
        
        public FromTree build(Context context, Node node, Operand initialResult) {
            return new Variable.FromTree(this, context, node, initialResult);
        }
        
        public FromTree build(Context context, List<Node> nodes) {
            return new Variable.FromTree(this, context, nodes);
        }
        
        public Temp buildTemp(Operand value) {
            return new Variable.Temp(this, value);
        }
        
        @Override
        public VariableError buildError() {
            return buildError(null);
        }
        
        @Override
        public VariableError buildError(EvaluationException cause) {
            return new VariableError(this, cause);
        }
        
        protected static final ToString<Builder> VARIABLE_BUILDER_TO_STRING =
            Symbol.Builder.SYMBOL_BUILDER_TO_STRING.derive(Builder.class)
                  .add("fn", b -> b.fn)
                  .build();
        
        @Override
        public String toString(boolean multiline) {
            return VARIABLE_BUILDER_TO_STRING.apply(this, multiline);
        }
    }
    
    // Not strictly necessary since we aren't actually
    // serializing intrinsic symbols except by name.
    public enum ConstantFunction implements VariableFunction {
        UNRESOLVED {
            @Override
            public Operand apply(Variable self, Context c) {
                throw EvaluationException.unresolved(self);
            }
        },
        PI {
            @Override
            public Operand apply(Variable self, Context c) {
                return c.getMathEnvironment().pi();
            }
        },
        E {
            @Override
            public Operand apply(Variable self, Context c) {
                return c.getMathEnvironment().e();
            }
        },
        TRUE {
            @Override
            public Operand apply(Variable self, Context c) {
                return eps.math.Boolean.TRUE;
            }
        },
        FALSE {
            @Override
            public Operand apply(Variable self, Context c) {
                return eps.math.Boolean.FALSE;
            }
        }
    }
    
    public static final Variable PI =
        new Builder().setName("pi")
                     .makeIntrinsic()
                     .addPreferredAliases("pi", "π")
                     .setEchoFormatter(EchoFormatter.ofVariable("π"))
                     .setFunction(ConstantFunction.PI)
                     .build();
    
    public static final Variable E =
        new Builder().setName("e")
                     .makeIntrinsic()
                     .addPreferredAlias("e")
                     .setFunction(ConstantFunction.E)
                     .build();
    
    public static final Variable TRUE =
        new Builder().setName("true")
                     .makeIntrinsic()
                     .addPreferredAlias("true")
                     .setFunction(ConstantFunction.TRUE)
                     .build();
    
    public static final Variable FALSE =
        new Builder().setName("false")
                     .makeIntrinsic()
                     .addPreferredAlias("false")
                     .setFunction(ConstantFunction.FALSE)
                     .build();
    
    public static final List<Variable> DEFAULTS =
        Collections.unmodifiableList(Misc.collectConstants(Variable.class));
}