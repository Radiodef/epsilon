/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.test.Tools.*;

import java.util.stream.*;

import org.junit.*;
import static org.junit.Assert.*;

public class SyntaxErrorTest {
    public SyntaxErrorTest() {
    }
    
    @Test
    public void testSingleCharacters() {
        testing();
        
        for (String token : new String[] {
            "{", "}", "[", "]", ",", ":", "\"",
            ".", "+", "-", "e",
            "X", "Y", "Z",
            " ", "\n", "\t",
        }) {
            try {
                JsonValue j = Json.parse(token);
                fail(token);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    @Test
    public void testOnlyWhitespace() {
        testing();
        try {
            JsonValue j = Json.parse("   \r\n   \t   ");
            fail();
        } catch (IllegalArgumentException x) {
        }
    }
    
    @Test
    public void testBrokenConstructs() {
        testing();
        
        for (String broken : new String[] {
            "][",                           // reverse array
            "]0[",                          // reverse array
            "] 1, 2, 3 [",                  // reverse array
            "1, 2, 3",                      // missing brackets
            "1, 2, 3,",                     // missing brackets
            "[[]",                          // missing end bracket
            "[{}",                          // missing end bracket
            "[ 1, 2, 3",                    // missing end bracket
            "[]]",                          // missing start bracket
            "{}]",                          // missing start bracket
            "1, 2, 3]",                     // missing start bracket
            "[ 1 2 ]",                      // missing comma
            "[ , ]",                        // missing array element
            "[ , 1 ]",                      // missing array element
            "[ , 1, ]",                     // missing array element
            "[ 1,, ]",                      // missing array element
            "[ 1, , ]",                     // missing array element
            "} {",                          // reverse object
            "} \"x\": true {",              // reverse object
            "{ \"x\": true",                // missing end brace
            "\"x\": true",                  // missing braces
            "\"x\": true }",                // missing start brace
            "{ \"x: true }",                // missing end quote
            "{ x\": true }",                // missing start quote
            "{ true }",                     // missing key/colon
            "{ {} }",                       // missing key/colon
            "{ \"x\" }",                    // missing colon/value
            "{ \"x\": }",                   // missing value
            "{ \"x\": true \"y\": false }", // missing comma
            "{ \"x\" true }",               // missing colon
            "{ \"x true }",                 // missing colon/quote
            "{ x\" true }",                 // missing colon/quote
            "{ : true }",                   // missing key name
            "{ : }",                        // missing key/value
            "{ \": true }",                 // missing key name/quote
            "{ \" true }",                  // missing key name/quote
            "{ \"true }",                   // missing key name/quote
            "[ \"x\": 0 ]",                 // object-style property in an array
        }) {
            try {
                JsonValue j = Json.parse(broken);
                fail("'" + broken + "' = " + j);
            } catch (IllegalArgumentException x) {
                System.out.printf("    '%s' = %s%n", broken, x.getMessage());
            } catch (Throwable x) {
                System.out.printf("    failed at '%s'%n", broken);
                throw x;
            }
        }
    }
    
    @Test
    public void testNoQuoteObjectKey() {
        testing();
        // we don't support this form
        try {
            JsonValue j = Json.parse("{ x: true }");
            fail();
        } catch (IllegalArgumentException x) {
        }
    }
    
    @Test
    public void testBadNumbers() {
        testing();
        
        for (String badNum : new String[] {
            "+1", // malformed/illegal signs
            "++1",
            "+ 1",
            "--1",
            "- 1",
            "+-1",
            "-+1",
            
            "1.0.", // malformed fractions
            "1..",
            "..1",
            "0..1",
            "1.0.0",
            "1.2.3.4",
            
            " 2 e 2",  " 2e 2",  " 2 e2",
            " 2 e -2", " 2e -2", " 2 e-2",
            " 2 e +2", " 2e +2", " 2 e+2",
            "-2 e 2",  "-2e 2",  "-2 e2",
            "-2 e -2", "-2e -2", "-2 e-2",
            "-2 e +2", "-2e +2", "-2 e+2",
            
            " 1e-1.5", " 1e+1.5", " 1e1.5", // fraction in exponent not allowed
            "-1e-1.5", "-1e+1.5", "-1e1.5",
            
            " 01", // numbers can not start with a leading 0
            " 001",
            " 00.1",
            " 00e1",
            "-01",
            "-001",
            "-00.1",
            "-00e1",
            
            " NaN", // NaN, Infinity, etc. not allowed
            "-NaN",
            " nan",
            "-nan",
            " NAN",
            "-NAN",
            " Infinity",
            "-Infinity",
            " infinity",
            "-infinity",
            " INFINITY",
            "-INFINITY",
            " ∞",
            "-∞",
        }) {
            try {
                JsonValue j = Json.parse(badNum);
                fail("'" + badNum + "' = " + j);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    @Test
    public void testLargeObject() {
        testing();
        final String i = Json.INDENT;
        
        final String json =
             i+i+ "{"                 +"\n"
            +i+i+i+ "\"\": {},"       +"\n"
            +i+i+i+ "\"かをり\": {"    +"\n"
            +i+i+i+i+ "\"か\": 0.5,"  +"\n"
            +i+i+i+i+ "\"を\": 0.25," +"\n"
            +i+i+i+i+ "\"り\": 0.125" +"\n"
            +i+i+i+ "},"              +"\n"
            +i+i+i+ "\"P\": {"        +"\n"
            +i+i+i+i+ "\"Abc\": {},"  +"\n"
            +i+i+i+i+ "\"Xyz\": {}"   +"\n"
            +i+i+i+ "},"              +"\n"
            +i+i+i+ "\"array_A\": ["  +"\n"
            +i+i+i+i+ "true,"         +"\n"
            +i+i+i+i+ "false,"        +"\n"
            +i+i+i+i+ "[],"           +"\n"
            +i+i+i+i+ "{}"            +"\n"
            +i+i+i+ "],"              +"\n"
            +i+i+i+ "\"array_B\": ["  +"\n"
            +i+i+i+i+ "{"                 +"\n"
            +i+i+i+i+i+ "\"bool\": true"  +"\n"
            +i+i+i+i+ "},"                +"\n"
            +i+i+i+i+ "{},"               +"\n"
            +i+i+i+i+ "{"                 +"\n"
            +i+i+i+i+i+ "\"bool\": false" +"\n"
            +i+i+i+i+ "}"                 +"\n"
            +i+i+i+ "]"                   +"\n"
            +i+i+ "}";
        
        assertEquals(Json.parse(json), Json.parse(Misc.despace(json)));
        
        int[] indexes =
            IntStream.range(0, json.length())
                     .filter(index -> {
                         // Note that this works because
                         // there are no strings in the
                         // object with these characters.
                         switch (json.charAt(index)) {
                             case '{': case '}':
                             case '[': case ']':
                             case '"':
                             case ':':
                             case ',':
                                 return true;
                             default:
                                 return false;
                         }
                     }).toArray();
        
        for (int index : indexes) {
            String bad = new StringBuilder(json).deleteCharAt(index).toString();
            try {
                JsonValue val = Json.parse(bad);
                fail(json.charAt(index) + " at index " + index + " starting '" + json.substring(index) + "'");
            } catch (IllegalArgumentException x) {
            }
        }
    }
}