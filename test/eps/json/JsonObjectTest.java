/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.util.Literals.*;
import static eps.test.Tools.*;

import java.util.*;
import static java.util.stream.Collectors.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonObjectTest {
    public JsonObjectTest() {
    }
    
    @Test
    public void testConstructor() {
        testing();
        JsonObject o = new JsonObject();
        
        assertEquals(0, o.count());
        assertEquals(Collections.emptyMap(), o.asMap());
    }
    
    @Test
    public void testPutGetAndRemove() {
        testing();
        JsonObject o = new JsonObject();
        
        assertNull(o.get("かをり"));
        assertNull(o.remove("かをり"));
        assertNull(o.put("かをり", JsonBoolean.TRUE));
        
        assertEquals(JsonBoolean.TRUE,  o.get("かをり"));
        assertEquals(JsonBoolean.TRUE,  o.put("かをり", JsonBoolean.FALSE));
        assertEquals(JsonBoolean.FALSE, o.get("かをり"));
        
        assertEquals(JsonBoolean.FALSE, o.remove("かをり"));
        assertNull(o.get("かをり"));
        assertNull(o.remove("かをり"));
        
        try {
            o.put(null, JsonValue.NULL);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            o.put("", null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            o.put(null, null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            o.remove(null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testAsMap() {
        testing();
        JsonObject o = new JsonObject();
        
        o.put("か", new JsonNumber(1));
        o.put("を", new JsonNumber(2));
        o.put("り", new JsonNumber(3));
        
        Map<?, ?> expect = MapOf("か", new JsonNumber(1),
                                 "を", new JsonNumber(2),
                                 "り", new JsonNumber(3));
        Map<?, ?> actual = o.asMap();
        assertEquals(expect, actual);
        
        try {
            actual.remove("か");
            fail();
        } catch (UnsupportedOperationException x) {
        }
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        JsonObject a, b;
        
        a = new JsonObject();
        b = new JsonObject();
        
        assertTrue(a.equals(b));
        assertTrue(b.equals(a));
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
        
        LinkedHashMapOf("か", new JsonNumber(1),
                        "を", new JsonNumber(2),
                        "り", new JsonNumber(3)).forEach((k, v) -> {
            a.put(k, v);
            b.put(k, v);
        });
        
        assertTrue(a.equals(b));
        assertTrue(b.equals(a));
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
        
        b.put("を", new JsonNumber(0));
        assertFalse(a.equals(b));
        assertFalse(b.equals(a));
    }
    
    @Test
    public void testSerial() {
        testing();
        assertEquals(new JsonObject(), Json.parse("{}"));
        assertEquals(new JsonObject(), Json.parse(" { \n } "));
        assertEquals("{}", new JsonObject().toString());
        
        JsonObject o = new JsonObject();
        o.put("か", new JsonNumber(0.5));
        o.put("を", new JsonNumber(0.25));
        o.put("り", new JsonNumber(0.125));
        
        String expect = "{ \"か\": 0.5, \"を\": 0.25, \"り\": 0.125 }";
        String actual = o.toString();
        assertEquals(expect, actual);
        assertEquals(o, Json.parse(expect));
        
        JsonObject p = new JsonObject();
        p.put("Abc", new JsonObject());
        p.put("Xyz", new JsonObject());
        
        JsonObject q = new JsonObject();
        
        q.put("", new JsonObject());
        q.put("かをり", o);
        q.put("P", p);
        
        JsonArray a = new JsonArray();
        a.add(JsonBoolean.TRUE);
        a.add(JsonBoolean.FALSE);
        a.add(new JsonArray());
        a.add(new JsonObject());
        
        q.put("array_A", a);
        
        JsonArray b = new JsonArray();
        
        JsonObject bTrue = new JsonObject();
        bTrue.put("bool", JsonBoolean.TRUE);
        
        JsonObject bFalse = new JsonObject();
        bFalse.put("bool", JsonBoolean.FALSE);
        
        b.add(bTrue);
        b.add(new JsonObject());
        b.add(bFalse);
        
        q.put("array_B", b);
        
        final String i = Json.INDENT;
        
        expect =
             i+i+ "{"                 +"\n"
            +i+i+i+ "\"\": {},"       +"\n"
            +i+i+i+ "\"かをり\": {"    +"\n"
            +i+i+i+i+ "\"か\": 0.5,"  +"\n"
            +i+i+i+i+ "\"を\": 0.25," +"\n"
            +i+i+i+i+ "\"り\": 0.125" +"\n"
            +i+i+i+ "},"              +"\n"
            +i+i+i+ "\"P\": {"        +"\n"
            +i+i+i+i+ "\"Abc\": {},"  +"\n"
            +i+i+i+i+ "\"Xyz\": {}"   +"\n"
            +i+i+i+ "},"              +"\n"
            +i+i+i+ "\"array_A\": ["  +"\n"
            +i+i+i+i+ "true,"         +"\n"
            +i+i+i+i+ "false,"        +"\n"
            +i+i+i+i+ "[],"           +"\n"
            +i+i+i+i+ "{}"            +"\n"
            +i+i+i+ "],"              +"\n"
            +i+i+i+ "\"array_B\": ["  +"\n"
            +i+i+i+i+ "{"                 +"\n"
            +i+i+i+i+i+ "\"bool\": true"  +"\n"
            +i+i+i+i+ "},"                +"\n"
            +i+i+i+i+ "{},"               +"\n"
            +i+i+i+i+ "{"                 +"\n"
            +i+i+i+i+i+ "\"bool\": false" +"\n"
            +i+i+i+i+ "}"                 +"\n"
            +i+i+i+ "]"                   +"\n"
            +i+i+ "}";
        actual = q.toString(2);
        System.out.println(actual);
        
        assertEquals(expect, actual);
        assertEquals(q, Json.parse(actual));
        
        // testing escapes in object keys
        
        o = new JsonObject();
        
        o.put("\"", JsonBoolean.FALSE);
        
        expect = "{ \"\\u0022\": false }";
        actual = o.toString();
        assertEquals(expect, actual);
        assertEquals(o, Json.parse(actual));
        
        o = new JsonObject();
        
        for (String s : JsonString.ESCAPED_CHARACTERS) {
            o.put(s, JsonBoolean.TRUE);
        }
        
        expect =
            JsonString.ESCAPED_CHARACTERS.stream()
                      .map(JsonString::escape)
                      .map(k -> "\"" + k + "\":true")
                      .collect(joining(",", "{", "}"));
        actual = Misc.despace(o.toString());
        
        assertEquals(expect, actual);
        assertEquals(o, Json.parse(actual));
    }
}