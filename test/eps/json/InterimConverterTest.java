/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

public class InterimConverterTest {
    public InterimConverterTest() {
    }
    
    @JsonSerializable
    static class TestObject {
        @JsonProperty(interimConverter = "eps.json.InterimConverterTest$TestObject$TestPropertyConverter")
        final TestProperty prop;
        @JsonConstructor
        TestObject(TestProperty prop) { this.prop = prop; }
        
        static final MutableBoolean INVOKED = new MutableBoolean();
        
        private static final class TestPropertyConverter
        extends InterimConverter<TestProperty, String> {
            private TestPropertyConverter() {
                super(TestProperty.class, String.class);
            }
            @Override
            public String toInterim(Object prop) {
                assertFalse(INVOKED.value);
                INVOKED.set(true);
                return ((TestProperty) prop).val;
            }
            @Override
            public TestProperty fromInterim(Object val) {
                assertFalse(INVOKED.value);
                INVOKED.set(true);
                return new TestProperty((String) val);
            }
        }
    }
    
    static class TestProperty {
        final String val;
        TestProperty(String val) { this.val = val; }
    }
    
    @Test
    public void test() {
        testing();
        assertEquals(TestProperty.class, new TestObject.TestPropertyConverter().getSourceType());
        assertEquals(String.class,       new TestObject.TestPropertyConverter().getInterimType());
        
        TestObject obj = new TestObject(new TestProperty("words"));
        
        TestObject.INVOKED.set(false);
        String json = Json.stringify(obj, false);
        assertTrue(TestObject.INVOKED.value);
        
        TestObject.INVOKED.set(false);
        Object result = Json.objectify(json);
        assertTrue(TestObject.INVOKED.value);
        
        assertNotSame(obj, result);
        assertNotNull(result);
        assertEquals(TestObject.class, result.getClass());
        
        TestProperty prop = ((TestObject) result).prop;
        assertNotNull(prop);
        assertEquals("words", prop.val);
    }
    
    @JsonSerializable
    static class BadClassName {
        @JsonProperty(interimConverter = "abc.pqr.xyz.Converter")
        final int x;
        @JsonConstructor BadClassName(int x) {
            this.x = x;
        }
    }
    
    @Test
    public void testBadClassName() throws ReflectiveOperationException {
        testing();
        BadClassName obj = new BadClassName(1);
        
        try {
            String json = Json.stringify(obj, false);
            fail(json);
        } catch (IllegalArgumentException x) {
            // try to make sure it didn't fail for some other reason
            String msg = x.getMessage();
            String val = BadClassName.class.getDeclaredField("x").getAnnotation(JsonProperty.class).interimConverter();
            assertTrue(msg.contains(val));
        }
    }
    
    @JsonSerializable
    static class Defers {
        @JsonProperty(interimConverter = "eps.json.InterimConverterTest$Defers$OneWayConverter",
                      deferInterimToConstructor = true)
        final TestProperty prop;
        
        Defers(TestProperty prop) {
            this.prop = prop;
        }
        
        @JsonConstructor
        Defers(Object prop) {
            assertNotNull(prop);
            assertEquals(String.class, prop.getClass());
            
            this.prop = new TestProperty((String) prop);
        }
        
        static final MutableBoolean INVOKED = new MutableBoolean();
        
        static final class OneWayConverter
        extends InterimConverter<TestProperty, String> {
            private OneWayConverter() {
                super(TestProperty.class, String.class);
            }
            @Override
            public String toInterim(Object prop) {
                assertFalse(INVOKED.value);
                INVOKED.set(true);
                return ((TestProperty) prop).val;
            }
            @Override
            public TestProperty fromInterim(Object val) {
                throw new AssertionError(val);
            }
        }
    }
    
    @Test
    public void testDefer() {
        testing();
        Defers d = new Defers(new TestProperty("teeeeeeeext"));
        
        Defers.INVOKED.set(false);
        String json = Json.stringify(d, true);
        assertTrue(Defers.INVOKED.value);
        
        Defers.INVOKED.set(false);
        Object o = Json.objectify(json);
        assertFalse("note: the converter should not be called for deserialization",
                    Defers.INVOKED.value);
        
        assertNotNull(o);
        assertEquals(Defers.class, o.getClass());
        assertNotNull(((Defers) o).prop);
        assertEquals("teeeeeeeext", ((Defers) o).prop.val);
    }
    
    static class InterimConverterConstructorException extends RuntimeException {}
    
    @JsonSerializable
    static class ObjectWithBadConverter {
        @JsonProperty(interimConverter = "eps.json.InterimConverterTest$ObjectWithBadConverter$BadConverter")
        final Object x;
        @JsonConstructor
        ObjectWithBadConverter(Object x) { this.x = x; }
        
        static final class BadConverter
        extends InterimConverter<Object, Object> {
            private BadConverter() {
                super(Object.class, Object.class);
                throw new InterimConverterConstructorException();
            }
            @Override
            public Object toInterim(Object a) {
                throw new AssertionError(a);
            }
            @Override
            public Object fromInterim(Object a) {
                throw new AssertionError(a);
            }
        }
    }
    
    @Test
    public void testBadConverter() {
        testing();
        try {
            Json.stringify(new ObjectWithBadConverter(null), false);
            fail();
        } catch (IllegalArgumentException original) {
            // I guess we just want to find that the exception is in the
            // causes of the one actually thrown.
            Throwable x = original;
            
            for (;;) {
                if (x instanceof InterimConverterConstructorException) {
                    break;
                }
                Throwable c = x.getCause();
                if (c == null) {
                    original.printStackTrace(System.out);
                    fail(original.toString());
                    break;
                }
                x = c;
            }
        }
    }
    
    @Test
    public void testInvariants() {
        testing();
        try {
            InterimConverter.forName(null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            InterimConverter.forName(Object.class.getName());
            fail();
        } catch (IllegalArgumentException x) {
        }
        try {
            new InterimConverter<Object, Object>(null, Object.class) {
                @Override public Object   toInterim(Object o) { return o; }
                @Override public Object fromInterim(Object o) { return o; }
            };
            fail();
        } catch (NullPointerException x) {
        }
        try {
            new InterimConverter<Object, Object>(Object.class, null) {
                @Override public Object   toInterim(Object o) { return o; }
                @Override public Object fromInterim(Object o) { return o; }
            };
            fail();
        } catch (NullPointerException x) {
        }
    }
}