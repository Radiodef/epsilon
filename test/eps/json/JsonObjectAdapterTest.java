/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.test.Tools.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;

import java.util.*;
import java.util.function.*;
import static java.util.Collections.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonObjectAdapterTest {
    public JsonObjectAdapterTest() {
    }
    
    @Test
    public void testStackTraceAdapter() {
        testing();
        
        JsonObjectAdapter<StackTraceElement> a = JsonObjectAdapter.get(StackTraceElement.class);
        assertSame(JsonObjectAdapter.StackTraceAdapter.INSTANCE, a);
        
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        for (StackTraceElement e : trace) {
            Map<?, ?> m = a.toMap(e);
            assertNotNull(m);
            StackTraceElement f = a.toObject(m);
            assertNotNull(f);
            assertNotSame(e, f);
            assertEquals(e, f);
        }
        
        String json = Json.stringify(trace, true);
        assertArrayEquals(trace, (StackTraceElement[]) Json.objectify(json));
    }
    
    static class Gadget {
        final String label;
        final double angle;
        Gadget(String label, double angle) {
            this.label = label;
            this.angle = angle;
        }
    }
    
    @Test
    public void testCustomAdapter() {
        testing();
        JsonObjectAdapterPrivate.resetRegistry();
        
        MutableBoolean invoked = new MutableBoolean();
        
        JsonObjectAdapter<Gadget> a =
                new JsonObjectAdapter<Gadget>() {
            @Override
            public Class<Gadget> getType() {
                return Gadget.class;
            }
            @Override
            public Map<?, ?> toMap(Gadget g) {
                assertFalse(invoked.value);
                invoked.set(true);
                return LinkedHashMapOf("label", g.label, "angle", g.angle);
            }
            @Override
            public Gadget toObject(Map<?, ?> m) {
                assertFalse(invoked.value);
                invoked.set(true);
                return new Gadget((String) m.get("label"), (Double) m.get("angle"));
            }
        };
        
        assertNull(JsonObjectAdapter.put(a));
        assertSame(a, JsonObjectAdapter.get(Gadget.class));
        
        Gadget expectGadget = new Gadget("foo", 123657.123);
        
        String expectString = f("{ \"%s\": \"%s\", \"%s\": \"1\", \"%s\": [ " +
                                "{ \"%s\": \"label\", \"%s\": \"%s\" }, " +
                                "{ \"%s\": \"angle\", \"%s\": { \"%s\": \"java.lang.Double\", \"%s\": \"%s\" } } " +
                                "] }",
                                Json.CLASS,
                                Gadget.class.getName(),
                                Json.REF,
                                Json.PROPS,
                                Json.KEY,
                                Json.VAL,
                                expectGadget.label,
                                Json.KEY,
                                Json.VAL,
                                Json.CLASS,
                                Json.VAL,
                                expectGadget.angle);
        invoked.set(false);
        String actualString = Json.stringify(expectGadget, false);
        assertTrue(invoked.value);
        
        assertEquals(expectString, actualString);
        
        invoked.set(false);
        Object actualGadget = Json.objectify(actualString);
        assertTrue(invoked.value);
        
        assertNotNull(actualGadget);
        assertEquals(Gadget.class, actualGadget.getClass());
        
        assertEquals(expectGadget.label, ((Gadget) actualGadget).label);
        assertEquals(expectGadget.angle, ((Gadget) actualGadget).angle, 0.0);
        
        // just testing the return value
        assertSame(a, JsonObjectAdapter.put(Gadget.class, g -> emptyMap(), m -> new Gadget("", 0)));
    }
    
    static class Point {
        final double x, y;
        Point(double x, double y) { this.x = x; this.y = y; }
    }
    
    @Test
    public void testAdapterFromFunction() {
        testing();
        JsonObjectAdapterPrivate.resetRegistry();
        
        JsonObjectAdapter<Point> a =
            JsonObjectAdapter.create(Point.class,
                                     p -> LinkedHashMapOf("x", p.x, "y", p.y),
                                     m -> new Point((Double) m.get("x"), (Double) m.get("y")));
        assertNull(JsonObjectAdapter.put(a));
        assertSame(a, JsonObjectAdapter.get(Point.class));
        
        Point p = new Point(0.125, 0.0625);
        
        String json = Json.stringify(p, true);
        Object obj  = Json.objectify(json);
        
        assertNotNull(obj);
        assertEquals(Point.class, obj.getClass());
        assertEquals(p.x, ((Point) obj).x, 0.0);
        assertEquals(p.y, ((Point) obj).y, 0.0);
        
        // just testing the return value
        assertSame(a, JsonObjectAdapter.put(Point.class, q -> emptyMap(), m -> new Point(0, 0)));
    }
    
    @Test
    public void testInvariants() {
        testing();
        try {
            JsonObjectAdapter.put(null);
            fail();
        } catch (NullPointerException x) {
        }
        testing();
        try {
            JsonObjectAdapter.get(null);
            fail();
        } catch (NullPointerException x) {
        }
        class Foo {}
        class Args {
            final Class<Foo> a;
            final Function<Foo, Map<?, ?>> b;
            final Function<Map<?, ?>, Foo> c;
            Args(Class<Foo> a,
                 Function<Foo, Map<?, ?>> b,
                 Function<Map<?, ?>, Foo> c) {
                this.a = a;
                this.b = b;
                this.c = c;
            }
        }
        Args[] args = {
            new Args(null,      f -> emptyMap(), m -> new Foo()),
            new Args(Foo.class, null,            m -> new Foo()),
            new Args(Foo.class, f -> emptyMap(), null          ),
        };
        for (Args a : args) {
            try {
                JsonObjectAdapter.create(a.a, a.b, a.c);
                fail();
            } catch (NullPointerException x) {
            }
        }
        for (Args a : args) {
            try {
                JsonObjectAdapter.put(a.a, a.b, a.c);
                fail();
            } catch (NullPointerException x) {
            }
        }
        for (Args a : args) {
            try {
                new JsonObjectAdapter.FromFunction<>(a.a, a.b, a.c);
                fail();
            } catch (NullPointerException x) {
            }
        }
    }
}