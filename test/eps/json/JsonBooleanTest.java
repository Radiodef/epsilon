/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonBooleanTest {
    public JsonBooleanTest() {
    }
    
    @Test
    public void testValueOf() {
        testing();
        assertEquals(JsonBoolean.TRUE,  JsonBoolean.valueOf(true));
        assertEquals(JsonBoolean.FALSE, JsonBoolean.valueOf(false));
    }
    
    @Test
    public void testBooleanValueOf() {
        testing();
        assertTrue(JsonBoolean.TRUE.booleanValue());
        assertFalse(JsonBoolean.FALSE.booleanValue());
        assertTrue(new JsonBoolean(true).booleanValue());
        assertFalse(new JsonBoolean(false).booleanValue());
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        assertEquals   ( new JsonBoolean(true ), new JsonBoolean(true ) );
        assertEquals   ( new JsonBoolean(false), new JsonBoolean(false) );
        assertNotEquals( new JsonBoolean(false), new JsonBoolean(true ) );
        assertNotEquals( new JsonBoolean(true ), new JsonBoolean(false) );
        
        assertEquals   ( new JsonBoolean(true ).hashCode(), new JsonBoolean(true ).hashCode() );
        assertEquals   ( new JsonBoolean(false).hashCode(), new JsonBoolean(false).hashCode() );
        assertNotEquals( new JsonBoolean(false).hashCode(), new JsonBoolean(true ).hashCode() );
        assertNotEquals( new JsonBoolean(true ).hashCode(), new JsonBoolean(false).hashCode() );
    }
    
    @Test
    public void testSerial() {
        testing();
        assertEquals(JsonBoolean.TRUE,  Json.parse("true" ));
        assertEquals(JsonBoolean.FALSE, Json.parse("false"));
        
        assertEquals("true",  JsonBoolean.TRUE.toString());
        assertEquals("false", JsonBoolean.FALSE.toString());
        
        assertEquals(Json.INDENT + "true", JsonBoolean.TRUE.toString(1));
        assertEquals(Json.INDENT + Json.INDENT + "true", JsonBoolean.TRUE.toString(2));
    }
}