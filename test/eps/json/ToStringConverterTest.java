/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.test.Tools.*;
import static eps.util.Literals.*;
import static eps.util.Misc.*;

import java.util.*;
import java.util.function.*;

import org.junit.*;
import static org.junit.Assert.*;

public class ToStringConverterTest {
    public ToStringConverterTest() {
    }
    
    @Test
    public void testSimpleConverter() {
        testing();
        ToStringConverter<Short> c = ToStringConverter.get(Short.class);
        assertNotNull(c);
        assertEquals(Short.class, c.getType());
        
        for (int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; ++i) {
            Short s = (short) i;
            assertEquals(s.toString(), c.serialize(s));
            assertEquals(s, c.deserialize(s.toString()));
        }
        
        try {
            c.serialize((Short) null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            c.deserialize((String) null);
            fail();
        } catch (NumberFormatException x) {
            // Note: Integer.parseInt etc. don't throw
            // NullPointerException when null
        }
    }
    
    @Test
    public void testCustomConverter() {
        testing();
        MutableBoolean invoked = new MutableBoolean();
        
        ToStringConverter<StringBuffer> c =
                new ToStringConverter<StringBuffer>() {
            @Override
            public Class<StringBuffer> getType() {
                return StringBuffer.class;
            }
            @Override
            public String serialize(StringBuffer b) {
                assertFalse(invoked.value);
                invoked.set(true);
                return b.toString();
            }
            @Override
            public StringBuffer deserialize(String s) {
                assertFalse(invoked.value);
                invoked.set(true);
                return new StringBuffer(s);
            }
        };
        assertNull(ToStringConverter.put(c));
        assertSame(c, ToStringConverter.get(StringBuffer.class));
        
        invoked.set(false);
        String expect = f("{ \"%s\": \"java.lang.StringBuffer\", \"%s\": \"chars\" }", Json.CLASS, Json.VAL);
        String actual = Json.stringify(new StringBuffer("chars"), false);
        assertEquals(expect, actual);
        assertTrue(invoked.value);
        
        invoked.set(false);
        Object obj = Json.objectify(actual);
        assertNotNull(obj);
        assertEquals(StringBuffer.class, obj.getClass());
        assertEquals("chars", obj.toString());
        assertTrue(invoked.value);
        
        invoked.set(false);
        assertEquals("c h a r s", ToStringConverter.toString(new StringBuffer("c h a r s")));
        assertTrue(invoked.value);
        
        invoked.set(false);
        assertEquals("c h a r s", ToStringConverter.toObject(StringBuffer.class, "c h a r s").toString());
        assertTrue(invoked.value);
    }
    
    @Test
    public void testPlainSerialization() {
        testing();
        Float  f = 0.125f;
        String s = Json.stringify(f, true);
        assertNotEquals("this should serialize as a JSON object, with the class saved", "0.125", s);
        
        JsonObject json = (JsonObject) Json.parse(s);
        assertEquals(SetOf(new JsonString("java.lang.Float"),
                           new JsonString("0.125")),
                     new HashSet<>(json.asMap().values()));
        
        System.out.println(s);
        
        Object o = Json.objectify(s);
        assertNotNull(o);
        assertEquals(Float.class, o.getClass());
        assertEquals(f, (Float) o);
    }
    
    @JsonSerializable
    static class Longs {
        @JsonProperty    final Long   val;
        @JsonProperty    final Object obj; // note: declared field type shouldn't matter
        @JsonConstructor Longs(Long val, Object obj) { this.val = val;
                                                       this.obj = obj; }
    }
    
    @Test
    public void testSerializationAsField() {
        testing();
        Longs expect = new Longs(Long.MIN_VALUE, Long.MAX_VALUE);
        
        String json = Json.stringify(expect, true);
        System.out.println(json);
        
        Object actual = Json.objectify(json);
        assertNotNull(actual);
        assertEquals(Longs.class, actual.getClass());
        assertEquals((Long) Long.MIN_VALUE, ((Longs) actual).val);
        assertEquals((Long) Long.MAX_VALUE, ((Longs) actual).obj);
    }
    
    @Test
    public void testPutAndGet() {
        testing();
        try {
            ToStringConverter.put(null);
            fail();
        } catch (NullPointerException x) {
        }
        testing();
        try {
            ToStringConverter.get(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        class Local {}
        
        ToStringConverter<Local> c =
                new ToStringConverter<Local>() {
            @Override
            public Class<Local> getType() {
                return Local.class;
            }
            @Override
            public String serialize(Local l) {
                return l.toString();
            }
            @Override
            public Local deserialize(String s) {
                return new Local();
            }
        };
        
        assertNull(ToStringConverter.put(c));
        assertSame(c, ToStringConverter.get(Local.class));
    }
    
    static class Foo {}
    static class Bar {}
    
    @Test
    @SuppressWarnings("unchecked")
    public void testToStringConverterFromFunction() {
        testing();
        
        for (Object[] args : new Object[][] {
            {null,         (Function<?, ?>) x -> x, (Function<?, ?>) x -> x},
            {String.class, (Function<?, ?>) null,   (Function<?, ?>) x -> x},
            {String.class, (Function<?, ?>) x -> x, (Function<?, ?>) null  },
        }) {
            try {
                ToStringConverter.create((Class<String>) args[0], (Function<String, String>) args[1], (Function<String, String>) args[2]);
                fail();
            } catch (NullPointerException x) {
            }
        }
        
        // here, we're testing that the methods are actually called
        // when we think they ought to be
        
        class FooSerializationException extends RuntimeException {}
        class FooDeserializationException extends RuntimeException {}
        
        ToStringConverter<Foo> fooConverter =
            ToStringConverter.create(
                Foo.class,
                (Foo foo) -> {
                    assertNotNull(foo);
                    throw new FooSerializationException();
                },
                (String str) -> {
                    assertEquals("aFoo", str);
                    throw new FooDeserializationException();
                });
        assertNotNull(fooConverter);
        assertEquals(Foo.class, fooConverter.getType());
        ToStringConverter.put(fooConverter);
        assertSame(fooConverter, ToStringConverter.get(Foo.class));
        
        try {
            String json = Json.stringify(new Foo(), false);
            fail();
        } catch (FooSerializationException x) {
        }
        try {
            String json = f("{ \"%s\": \"%s\", \"%s\": \"%s\" }", Json.CLASS, Foo.class.getName(), Json.VAL, "aFoo");
            Object obj  = Json.objectify(json);
            fail();
        } catch (FooDeserializationException x) {
        }
        
        // actually test a serializer
        
        ToStringConverter<Bar> barConverter =
            ToStringConverter.create(Bar.class,
                                     bar -> "aBar",
                                     str -> new Bar());
        assertNotNull(barConverter);
        assertEquals(Bar.class, barConverter.getType());
        ToStringConverter.put(barConverter);
        assertSame(barConverter, ToStringConverter.get(Bar.class));
        
        String expect = f("{ \"%s\": \"%s\", \"%s\": \"%s\" }", Json.CLASS, Bar.class.getName(), Json.VAL, "aBar");
        String actual = Json.stringify(new Bar(), false);
        assertEquals(expect, actual);
        
        Object result = Json.objectify(actual);
        assertNotNull(result);
        assertEquals(Bar.class, result.getClass());
    }
    
    @Test
    public void testToStringAndToObject() {
        testing();
        // note: this is not allowed, because we are trying to
        //       save type information
        try {
            ToStringConverter.toString(null);
            fail();
        } catch (NullPointerException x) {
        }
        // we wouldn't know what to convert this result to,
        // and the cast would be unsafe
        try {
            ToStringConverter.toObject(null, "0");
            fail();
        } catch (NullPointerException x) {
        }
        // this would just be nonsense, really. if some serializer did want
        // to produce null results, it would expect the String "null", never
        // an actual null String.
        try {
            ToStringConverter.toObject(Integer.class, null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertEquals("789", ToStringConverter.toString(789L));
        assertEquals((Long) 789L, ToStringConverter.toObject(Long.class, "789"));
        
        class LocalA {}
        class LocalB extends LocalA {}
        
        ToStringConverter.put(ToStringConverter.create(LocalA.class, a -> "Local", s -> new LocalA()));
        // this should find the LocalA converter
        assertEquals("Local", ToStringConverter.toString(new LocalB()));
        
        // we can do some subtyping, but the exact type
        // of the result is unspecified here
        Number n = ToStringConverter.toObject(Number.class, "2");
        assertNotNull(n);
        assertEquals(2, n.intValue());
        
        // returns null when there's not a suitable serializer
        assertNull(ToStringConverter.toString(new Object()));
        assertNull(ToStringConverter.toObject(List.class, ""));
        
        try {
            ToStringConverter.toObject(Integer.class, "X");
            fail();
        } catch (NumberFormatException x) {
        }
    }
}