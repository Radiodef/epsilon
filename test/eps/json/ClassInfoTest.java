/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.ui.*;
import eps.util.*;
import static eps.test.Tools.*;
import static eps.util.Literals.*;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;
import java.util.concurrent.*;
import java.lang.reflect.*;
import static java.util.stream.Collectors.*;

import org.junit.*;
import static org.junit.Assert.*;

@JsonSerializable
class TopLevelClass {
    @JsonConstructor
    TopLevelClass() {
    }
}

public class ClassInfoTest {
    public ClassInfoTest() {
    }
    
    @Test
    public void testConstructor() throws Exception {
        testing();
        ClassInfo<TopLevelClass> info = new ClassInfo<>(TopLevelClass.class);
        
        assertEquals(TopLevelClass.class, info.getType());
        assertEquals(Collections.emptyList(), info.getPropertyFields());
        assertEquals(TopLevelClass.class.getDeclaredConstructor(), info.getConstructor());
        assertEquals(Collections.emptyMap(), info.getDefaults());
    }
    
    @JsonSerializable
    static class StaticNestedClass {
        @JsonConstructor
        StaticNestedClass() {}
    }
    @JsonSerializable
    private static class PrivateNestedClass {
        @JsonConstructor
        private PrivateNestedClass() {}
    }
    @JsonSerializable
    static abstract class AbstractClass {
        @JsonConstructor
        AbstractClass() {}
    }
    // Note: @JsonSerializable is @Inherited
    static class InheritingClass extends AbstractClass {
        @JsonConstructor
        InheritingClass() {}
    }
    
    @Test
    public void testLegalTypes() {
        testing();
        
        for (Class<?> legal : new Class<?>[] {
            TopLevelClass.class,
            StaticNestedClass.class,
            PrivateNestedClass.class,
            AbstractClass.class,
            InheritingClass.class,
            BoundingBox.class, // class from some other package
        }) {
            ClassInfo<?> info = new ClassInfo<>(legal);
            assertEquals(legal, info.getType());
        }
    }
    
    static class NotJsonSerializable {
        @JsonConstructor
        NotJsonSerializable() {}
    }
    @JsonSerializable
    class InnerClass {
        @JsonConstructor
        InnerClass() {}
    }
    @JsonSerializable
    enum EnumType {
        A, B, C;
        @JsonConstructor
        EnumType() {}
    }
    @JsonSerializable
    interface InterfaceType {
    }
    @JsonSerializable
    @interface AnnotationType {
    }
    
    @Test
    public void testIllegalTypes() {
        testing();
        
        try {
            ClassInfo<?> info = new ClassInfo<>(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        for (Class<?> illegal : new Class<?>[] {
            NotJsonSerializable.class,
            InnerClass.class,
            EnumType.class,
            InterfaceType.class,
            AnnotationType.class,
            new AbstractClass() {}.getClass(), // anonymous
            Object.class,
            String.class,
            ArrayList.class,
        }) {
            try {
                ClassInfo<?> info = new ClassInfo<>(illegal);
                fail(info.toString());
            } catch (IllegalArgumentException x) {
                System.out.printf("    %s: %s%n", Misc.getSimpleName(illegal), x.getMessage());
            }
        }
    }
    
    @JsonSerializable
    static class ClassWithFields {
        @JsonProperty
        final double a;
        @JsonProperty
        final String b;
        @JsonProperty
        final Object c;
        final Set<?> d;
        @JsonConstructor
        ClassWithFields(double a, String b, Object c) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = null;
        }
    }
    
    @Test
    public void testPropertyFields() throws Exception {
        testing();
        List<Field> expect = ListOf(ClassWithFields.class.getDeclaredField("a"),
                                    ClassWithFields.class.getDeclaredField("b"),
                                    ClassWithFields.class.getDeclaredField("c"));
        ClassInfo<ClassWithFields> info = new ClassInfo<>(ClassWithFields.class);
        
        assertEquals(expect, info.getPropertyFields());
        
        assertEquals(ClassWithFields.class.getDeclaredConstructor(double.class, String.class, Object.class),
                     info.getConstructor());
        
        assertEquals(ClassWithFields.class.getDeclaredField("a"), info.getPropertyField("a"));
        assertEquals(ClassWithFields.class.getDeclaredField("b"), info.getPropertyField("b"));
        assertEquals(ClassWithFields.class.getDeclaredField("c"), info.getPropertyField("c"));
        
        try {
            Field f = info.getPropertyField("d");
            fail();
        } catch (IllegalArgumentException x) {
        }
        try {
            Field f = info.getPropertyField("absolutelyNotAField");
            fail();
        } catch (IllegalArgumentException x) {
        }
        try {
            Field f = info.getPropertyField(null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testIndexOfPropertyField() {
        testing();
        ClassInfo<ClassWithFields> info = new ClassInfo<>(ClassWithFields.class);
        
        assertEquals( 0, info.indexOfPropertyField("a"));
        assertEquals( 1, info.indexOfPropertyField("b"));
        assertEquals( 2, info.indexOfPropertyField("c"));
        assertEquals(-1, info.indexOfPropertyField("d"));
        
        try {
            info.indexOfPropertyField(null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @JsonSerializable
    static class TransientField {
        @JsonProperty
        transient short s;
        @JsonConstructor
        TransientField(short s) { this.s = s; }
    }
    
    @Test
    public void testTransientField() throws Exception {
        testing();
        ClassInfo<TransientField> info = new ClassInfo<>(TransientField.class);
        assertEquals(ListOf(TransientField.class.getDeclaredField("s")),
                     info.getPropertyFields());
    }
    
    @JsonSerializable
    static class StaticField {
        @JsonProperty
        static byte b = 7;
        @JsonConstructor
        StaticField() {}
    }
    
    @Test
    public void testStaticField() {
        testing();
        try {
            ClassInfo<StaticField> info = new ClassInfo<>(StaticField.class);
            fail(info.toString());
        } catch (IllegalArgumentException x) {
        }
    }
    
    static class BannedNames {
        @JsonSerializable
        static class $Class {
            @JsonProperty
            final int $class;
            @JsonConstructor
            $Class(int a) { $class = a; }
        }
        @JsonSerializable
        static class $Ref {
            @JsonProperty
            final int $ref;
            @JsonConstructor
            $Ref(int a) { $ref = a; }
        }
        @JsonSerializable
        static class $Props {
            @JsonProperty
            final int $props;
            @JsonConstructor
            $Props(int a) { $props = a; }
        }
        @JsonSerializable
        static class $Enum {
            @JsonProperty
            final int $enum;
            @JsonConstructor
            $Enum(int a) { $enum = a; }
        }
        @JsonSerializable
        static class $Name {
            @JsonProperty
            final int $name;
            @JsonConstructor
            $Name(int a) { $name = a; }
        }
        @JsonSerializable
        static class $Key {
            @JsonProperty
            final int $key;
            @JsonConstructor
            $Key(int a) { $key = a; }
        }
        @JsonSerializable
        static class $Val {
            @JsonProperty
            final int $val;
            @JsonConstructor
            $Val(int a) { $val = a; }
        }
        @JsonSerializable
        static class $Elems {
            @JsonProperty
            final int $elems;
            @JsonConstructor
            $Elems(int a) { $elems = a; }
        }
    }
    
    @Test
    public void testBannedFieldNames() {
        testing();
        List<Class<?>> classes =
            ListOf(BannedNames.$Class.class,
                   BannedNames.$Ref.class,
                   BannedNames.$Props.class,
                   BannedNames.$Enum.class,
                   BannedNames.$Name.class,
                   BannedNames.$Key.class,
                   BannedNames.$Val.class,
                   BannedNames.$Elems.class);
        
        Set<String> names = new HashSet<>(JsonProperty.BANNED_FIELD_NAMES);
        classes.stream()
               .map(cls -> {
                   try {
                       return cls.getDeclaredFields()[0];
                   } catch (Exception x) {
                       throw new RuntimeException(x);
                   }
               })
               .map(Field::getName)
               .forEach(names::remove);
        assertEquals("add more tests if this assertion is ever hit",
                     Collections.emptySet(), names);
        
        for (Class<?> cls : classes) {
            try {
                ClassInfo<?> info = new ClassInfo<>(cls);
                fail(info.toString());
            } catch (IllegalArgumentException x) {
                assertTrue("some other exception is being thrown if this assertion is hit",
                           x.getMessage().contains("field name"));
            }
        }
    }
    
    @JsonSerializable
    static class SuperClass {
        @JsonProperty
        private final long val;
        @JsonConstructor
        SuperClass(long val) { this.val = val; }
    }
    @JsonSerializable
    static class IllegalSubClass extends SuperClass {
        @JsonProperty
        private final char val;
        @JsonConstructor
        IllegalSubClass(long val1, char val2) { super(val1); val = val2; }
    }
    
    @Test
    public void testDuplicateFieldNames() {
        testing();
        try {
            ClassInfo<IllegalSubClass> info = new ClassInfo<>(IllegalSubClass.class);
            fail(info.toString());
        } catch (IllegalArgumentException x) {
        }
    }
    
    @JsonSerializable
    static class UnmarkedNullaryConstructorA {
        UnmarkedNullaryConstructorA() {
        }
    }
    @JsonSerializable
    static class UnmarkedNullaryConstructorB {
    }
    @JsonSerializable
    static class UnmarkedConstructorWithArguments {
        @JsonProperty
        final byte b;
        @JsonProperty
        final char c;
        // Note: we could choose to support this.
        UnmarkedConstructorWithArguments(byte b, char c) {
            this.b = b;
            this.c = c;
        }
    }
    @JsonSerializable
    static class ExtraUnmarkedConstructors {
        @JsonProperty
        final byte b;
        @JsonProperty
        final char c;
        // this currently doesn't cause a problem, but maybe it should
        @JsonConstructor
        ExtraUnmarkedConstructors(char c)                 { this.b = 0;
                                                            this.c = c; }
        // (this is the actual constructor)
        @JsonConstructor
        ExtraUnmarkedConstructors(byte b, char c)         { this.b = b;
                                                            this.c = c; }
        // (put an extra marked constructor on either side to make
        // sure we actually encounter one during the search)
        @JsonConstructor
        ExtraUnmarkedConstructors(byte b)                 { this.b = b;
                                                            this.c = 0; }
        // these shouldn't cause a problem, of course
        ExtraUnmarkedConstructors()                       { this.b = 0;
                                                            this.c = 0; }
        ExtraUnmarkedConstructors(char c, byte b)         { this.b = b;
                                                            this.c = c; }
        ExtraUnmarkedConstructors(byte b, char c, long l) { this.b = b;
                                                            this.c = c; }
    }
    
    @Test
    public void testUnmarkedConstructors() throws Exception {
        testing();
        ClassInfo<?> infoA = new ClassInfo<>(UnmarkedNullaryConstructorA.class);
        assertEquals(UnmarkedNullaryConstructorA.class.getDeclaredConstructor(),
                     infoA.getConstructor());
        
        ClassInfo<?> infoB = new ClassInfo<>(UnmarkedNullaryConstructorB.class);
        assertEquals(UnmarkedNullaryConstructorB.class.getDeclaredConstructor(),
                     infoB.getConstructor());
        
        try {
            ClassInfo<?> infoC = new ClassInfo<>(UnmarkedConstructorWithArguments.class);
            fail(infoC.toString());
        } catch (IllegalArgumentException x) {
        }
        
        ClassInfo<?> infoD = new ClassInfo<>(ExtraUnmarkedConstructors.class);
        assertEquals(ExtraUnmarkedConstructors.class.getDeclaredConstructor(byte.class, char.class),
                     infoD.getConstructor());
    }
    
    @JsonSerializable
    static class BadConstructorTypes {
        @JsonProperty final Number l;
        @JsonProperty final Number b;
        @JsonConstructor
        BadConstructorTypes(Long l, Byte b) {
            this.l = l;
            this.b = b;
        }
    }
    @JsonSerializable
    static class GoodConstructorTypes {
        @JsonProperty final Long l;
        @JsonProperty final Byte b;
        @JsonConstructor
        GoodConstructorTypes(Number l, Number b) {
            this.l = l.longValue();
            this.b = b.byteValue();
        }
    }
    
    @Test
    public void testConstructorsWithDifferentTypes() throws Exception {
        testing();
        try {
            ClassInfo<BadConstructorTypes> info = new ClassInfo<>(BadConstructorTypes.class);
            fail(info.toString());
        } catch (IllegalArgumentException x) {
        }
        ClassInfo<GoodConstructorTypes> info = new ClassInfo<>(GoodConstructorTypes.class);
        assertEquals(GoodConstructorTypes.class.getDeclaredConstructor(Number.class, Number.class),
                     info.getConstructor());
    }
    
    @JsonSerializable
    static class DefaultTest {
        @JsonProperty
        final String a;
        @JsonDefault("a")
        static final String A_DEFAULT = "A";
        
        @JsonProperty
        final String b;
        // bound by the field name
        @JsonDefault
        static final String bDefault = "B";
        
        @JsonProperty
        final String c;
        // private is fine
        @JsonDefault("c")
        private static final String C_DEFAULT = "C";
        
        @JsonProperty
        final String d;
        @JsonDefault("d")
        static String getDDefault() { return "D"; }
        
        @JsonProperty
        final String e;
        // bound by the method name
        @JsonDefault
        private static String eDefault() { return "E"; }
        
        @JsonProperty
        final String f;
        // private is fine
        @JsonDefault("f")
        private static String getFDefault() { return "F"; }
        
        @JsonProperty
        final String p;
        // this will not be found, because no property is
        // specified and the name is wrong
        @JsonDefault
        static final String P_DEFAULT = "P";
        
        @JsonProperty
        final String q;
        // this will not be found, because the field is not static
        @JsonDefault("q")
        final String qDefault = "Q";
        
        @JsonProperty
        final String r;
        // this will not be found, because no property is
        // specified and the name is wrong
        @JsonDefault
        static String getRDefault() { return "R"; }
        
        @JsonProperty
        final String s;
        // this will not be found, because the method is not static
        @JsonDefault("s")
        String sDefault() { return "S"; }
        
        @JsonConstructor
        DefaultTest(String a, String b, String c, String d, String e, String f, String p, String q, String r, String s) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.p = p;
            this.q = q;
            this.r = r;
            this.s = s;
        }
    }
    
    @Test
    public void testDefaults() throws Exception {
        testing();
        final char min = 'a';
        final char max = 'f';
        
        ClassInfo<DefaultTest> info = new ClassInfo<>(DefaultTest.class);
        
        Map<String, Object> expect =
            IntStream.rangeClosed(min, max)
                     .mapToObj(c -> Character.toString((char) c))
                     .collect(toMap(c -> c,
                                    c -> c.toUpperCase(Locale.ROOT)));
        
        Map<String, Object> actual = info.getDefaults();
        
        assertEquals(expect, actual);
        
        for (Field field : DefaultTest.class.getDeclaredFields()) {
            if (!field.isAnnotationPresent(JsonProperty.class)) {
                continue;
            }
            
            Supplier<?> supplier;
            
            String key = field.getName();
            String val = key.toUpperCase(Locale.ROOT);
            
            assertEquals(1, key.length());
            
            if (key.charAt(0) <= max) {
                supplier = info.getDefault(key);
                assertNotNull(key, supplier);
                assertEquals(key, val, supplier.get());
                supplier = info.getDefault(field);
                assertNotNull(key, supplier);
                assertEquals(key, val, supplier.get());
            } else {
                assertNull(key, info.getDefault(key));
                assertNull(key, info.getDefault(field));
            }
        }
        
        try {
            info.getDefault((String) null);
            fail();
        } catch (NullPointerException x) {}
        try {
            info.getDefault((Field) null);
            fail();
        } catch (NullPointerException x) {}
        
        // this should possibly be an error
        class Local { int a; }
        Supplier<?> supplier = info.getDefault(Local.class.getDeclaredField("a"));
        assertNotNull(supplier);
        assertEquals("A", supplier.get());
    }
    
    @Test
    public void testCache() throws Exception {
        testing();
        final ClassInfo<ClassWithFields> info = ClassInfo.get(ClassWithFields.class);
        assertNotNull(info);
        assertEquals(ClassWithFields.class, info.getType());
        
        assertSame(info, ClassInfo.get(ClassWithFields.class));
        
        assertNull(ClassInfo.get(Object.class));
        assertNull(ClassInfo.get(String.class));
        assertNull(ClassInfo.get(Double.class));
        assertNull(ClassInfo.get(Number.class));
        assertNull(ClassInfo.get(double.class));
        
        // not that the following test really proves anything
        
        ExecutorService e = Executors.newFixedThreadPool(20);
        
        class Task implements Callable<ClassInfo<ClassWithFields>> {
            @Override
            public ClassInfo<ClassWithFields> call() {
                return ClassInfo.get(ClassWithFields.class);
            }
        }
        
        List<Task> tasks = Stream.generate(() -> new Task()).limit(200).collect(toList());
        
        for (Future<ClassInfo<ClassWithFields>> f : e.invokeAll(tasks)) {
            assertSame(info, f.get());
        }
    }
}