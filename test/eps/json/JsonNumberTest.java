/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.test.Tools.*;

import java.util.*;
import java.math.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonNumberTest {
    public JsonNumberTest() {
    }
    
    @Test
    public void testConstructor() {
        testing();
        JsonNumber n;
        
        for (double bad : new double[] { Double.NaN, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY }) {
            try {
                n = new JsonNumber(bad);
                fail(n.toString());
            } catch (IllegalArgumentException x) {
            }
        }
        
        n = new JsonNumber(Math.PI);
        assertEquals(Math.PI, n.doubleValue(), 0);
        
        assertEquals(3, n.intValue());
    }
    
    @Test
    public void testValueOf() {
        testing();
        
        for (Number n : new Number[] {
            Byte.MAX_VALUE,
            Short.MAX_VALUE,
            Integer.MAX_VALUE,
            Long.MAX_VALUE,
            Float.MAX_VALUE,
            Double.MAX_VALUE,
        }) {
            assertEquals(!(n instanceof Long), JsonNumber.isExactlyConvertible(n));
            try {
                JsonNumber j = JsonNumber.valueOf(n);
                if (n instanceof Long) {
                    fail(j.toString());
                } else {
                    assertEquals(n.doubleValue(), j.doubleValue(), 0);
                }
            } catch (IllegalArgumentException x) {
                assertTrue(x.getMessage(), n instanceof Long);
            }
        }
        
        for (Object o : new Object[] {
            null,
            new Object(),
            'X',
            "X",
            Double.class,
            new ArrayList<>(),
        }) {
            assertFalse(JsonNumber.isExactlyConvertible(o));
        }
    }
    
    @Test
    public void testAs() {
        testing();
        JsonNumber j = new JsonNumber(Math.E);
        
        try {
            Object o = j.as((Class<?>) null);
            fail();
        } catch (NullPointerException x) {
        }
        
        for (Class<?> bad : new Class<?>[] {
            long.class,
            Long.class,
            Object.class,
            BigInteger.class,
            BigDecimal.class,
            Class.class,
            ArrayList.class,
        }) {
            try {
                Object o = j.as(bad);
                fail(bad.toString());
            } catch (IllegalArgumentException x) {
            }
        }
        
        assertEquals(Byte.valueOf((byte) 2), j.as(byte.class));
        assertEquals(Byte.valueOf((byte) 2), j.as(Byte.class));
        
        assertEquals(Short.valueOf((short) 2), j.as(short.class));
        assertEquals(Short.valueOf((short) 2), j.as(Short.class));
        
        assertEquals(Integer.valueOf(2), j.as(int.class));
        assertEquals(Integer.valueOf(2), j.as(Integer.class));
        
        assertEquals(Float.valueOf((float) Math.E), j.as(float.class));
        assertEquals(Float.valueOf((float) Math.E), j.as(Float.class));
        
        assertEquals(Double.valueOf(Math.E), j.as(double.class));
        assertEquals(Double.valueOf(Math.E), j.as(Double.class));
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        JsonNumber a, b;
        
        a = new JsonNumber(Math.E);
        b = new JsonNumber(Math.E);
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertFalse(a.equals((Object) "Math.E"));
        
        b = new JsonNumber(Math.PI);
        assertNotEquals(a, b);
        assertNotEquals(b, a);
    }
    
    @Test
    public void testSerial() {
        testing();
        
        for (double d : new double[] {
            Math.E,
            Math.PI,
            Double.MIN_VALUE,
            Double.MAX_VALUE,
            -1024.5,
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        }) {
            String s = Double.toString(d);
            
            assertEquals(new JsonNumber(d), Json.parse(s));
            assertEquals(s, new JsonNumber(d).toString());
            
            assertEquals(Json.INDENT + s, new JsonNumber(d).toString(1));
            assertEquals(Json.INDENT + Json.INDENT + s, new JsonNumber(d).toString(2));
        }
        
        for (String zero : new String[] {
            " 0",
            "-0",
            " 0.0",
            "-0.0",
            " 0e1",
            "-0e1",
            " 0e+1",
            "-0e+1",
            " 0e-1",
            "-0e-1",
            " 0.0e1",
            "-0.0e1",
            " 0.0e+1",
            "-0.0e+1",
            " 0.0e-1",
            "-0.0e-1",
        }) {
            assertEquals(new JsonNumber(0), Json.parse(zero));
        }
        
        // trailing zeroes allowed in fraction
        assertEquals(new JsonNumber( 0.5), Json.parse(" 0.5000000"));
        assertEquals(new JsonNumber(-0.5), Json.parse("-0.5000000"));
        // JSON allows leading zeroes in the exponent for some reason
        assertEquals(new JsonNumber( 200), Json.parse(" 2e002"));
        assertEquals(new JsonNumber(-200), Json.parse("-2e002"));
        assertEquals(new JsonNumber( 0.5), Json.parse(" 5e-001"));
        assertEquals(new JsonNumber(-0.5), Json.parse("-5e-001"));
        
        // exponential form
        assertEquals(new JsonNumber( 2e-16), Json.parse(" 2e-16"));
        assertEquals(new JsonNumber( 2e+16), Json.parse(" 2e+16"));
        assertEquals(new JsonNumber( 2e16 ), Json.parse(" 2e16 "));
        assertEquals(new JsonNumber(-2e-16), Json.parse("-2e-16"));
        assertEquals(new JsonNumber(-2e+16), Json.parse("-2e+16"));
        assertEquals(new JsonNumber(-2e16 ), Json.parse("-2e16 "));
        assertEquals(new JsonNumber( 0.5e-3), Json.parse(" 0.5e-3"));
        assertEquals(new JsonNumber( 0.5e+3), Json.parse(" 0.5e+3"));
        assertEquals(new JsonNumber( 0.5e3 ), Json.parse(" 0.5e3 "));
        assertEquals(new JsonNumber(-0.5e-3), Json.parse("-0.5e-3"));
        assertEquals(new JsonNumber(-0.5e+3), Json.parse("-0.5e+3"));
        assertEquals(new JsonNumber(-0.5e3 ), Json.parse("-0.5e3 "));
        
        // uppercase E should work too
        assertEquals(new JsonNumber( 500), Json.parse(" 5E2"));
        assertEquals(new JsonNumber(-500), Json.parse("-5E2"));
        assertEquals(new JsonNumber( 0.5), Json.parse(" 5E-1"));
        assertEquals(new JsonNumber(-0.5), Json.parse("-5E-1"));
        
        // some zero exponents
        assertEquals(new JsonNumber( 2), Json.parse(" 2E0 "));
        assertEquals(new JsonNumber(-3), Json.parse("-3E0 "));
        assertEquals(new JsonNumber( 4), Json.parse(" 4E+0"));
        assertEquals(new JsonNumber(-5), Json.parse("-5E-0"));
    }
}