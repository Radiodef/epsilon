/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.test.Tools.*;

import java.util.*;
import java.util.stream.*;
import static java.util.stream.Collectors.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonDestructurerTest {
    public JsonDestructurerTest() {
    }
    
    private String makeJson(String... keys) {
        return IntStream.range(0, keys.length)
                        .mapToObj(i -> "\"" + keys[i] + "\": \"" + i + "\"")
                        .collect(joining(", ", "{ ", " }"));
    }
    
    @Test
    public void testTreeMapOf() {
        testing();
        
        assertEquals(Collections.emptyMap(), JsonDestructurer.TreeMapOf((String[]) null));
        assertEquals(Collections.emptyMap(), JsonDestructurer.TreeMapOf(new String[0]));
        assertEquals(Collections.emptyMap(), JsonDestructurer.TreeMapOf((Comparator<String>) null, (String[]) null));
        assertEquals(Collections.emptyMap(), JsonDestructurer.TreeMapOf((Comparator<String>) null, new String[0]));
        
        // testing odd-numbered arguments
        for (String[] entries : new String[][] {
            {"X"},
            {"X", "Y", "Z"},
            {"X", "Y", "Z", "P", "Q"},
        }) {
            try {
                Map<?, ?> m = JsonDestructurer.TreeMapOf(entries);
                fail("m = " + m);
            } catch (IllegalArgumentException x) {
            }
        }
        
        try {
            Map<?, ?> m = JsonDestructurer.TreeMapOf("X", "Y", "Z", (String) null);
            fail("m = " + m);
        } catch (NullPointerException x) {
        }
        try {
            Map<?, ?> m = JsonDestructurer.TreeMapOf("X", "Y", (String) null, "Z");
            fail("m = " + m);
        } catch (NullPointerException x) {
        }
        
        TreeMap<String, String> expect = new TreeMap<>();
        expect.put("A", "X");
        expect.put("B", "Y");
        expect.put("C", "Z");
        
        assertEquals(expect, JsonDestructurer.TreeMapOf("A", "X", "B", "Y", "C", "Z"));
        // note: null means no comparator
        assertEquals(expect, JsonDestructurer.TreeMapOf((Comparator<String>) null, "A", "X", "B", "Y", "C", "Z"));
        
        expect = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        expect.put("A", "X");
        expect.put("a", "x");
        expect.put("B", "Y");
        expect.put("b", "y");
        expect.put("C", "Z");
        expect.put("c", "z");
        assertEquals(3, expect.size());
        
        assertEquals(expect, JsonDestructurer.TreeMapOf(String.CASE_INSENSITIVE_ORDER,
                                                        "A", "X",
                                                        "a", "x",
                                                        "B", "Y",
                                                        "b", "y",
                                                        "C", "Z",
                                                        "c", "z"));
    }
    
    @Test
    public void testDestructure() {
        testing();
        JsonDestructurer d = new JsonDestructurer() {
            @Override
            protected String mapKey(String k) {
                return k;
            }
            @Override
            protected Iterable<String> keys() {
                return Arrays.asList("A", "B", "C");
            }
        };
        
        try {
            Map<?, ?> m = d.destructure((String) null);
            fail("m = " + m);
        } catch (NullPointerException x) {
        }
        try {
            Map<?, ?> m = d.destructure((JsonValue) null);
            fail("m = " + m);
        } catch (NullPointerException x) {
        }
        try {
            // must be a JsonObject
            Map<?, ?> m = d.destructure(JsonBoolean.TRUE);
            fail("m = " + m);
        } catch (IllegalArgumentException x) {
        }
        
        String json;
        
        for (String[] keys : new String[][] {
            {},
            {"A"},
            {"B"},
            {"C"},
            {"A", "B"},
            {"A", "C"},
            {"B", "C"},
            {"a", "b", "c"},
            {"A", "B", "Q"},
        }) {
            json = makeJson(keys);
            System.out.println("    " + json);
            try {
                Map<?, ?> m = d.destructure(json);
                fail(String.format("json = \"%s\", found %s", json, m));
            } catch (IllegalArgumentException x) {
            }
        }
        
        json = makeJson("A", "B", "C");
        
        Map<String, JsonValue> m = d.destructure(json);
        assertNotNull(m);
        assertEquals(3, m.size());
        
        assertEquals(new JsonString("0"), m.get("A"));
        assertEquals(new JsonString("1"), m.get("B"));
        assertEquals(new JsonString("2"), m.get("C"));
    }
    
    @Test
    public void testIsOptional() {
        testing();
        JsonDestructurer d = new JsonDestructurer() {
            @Override
            protected String mapKey(String k) {
                return k;
            }
            @Override
            protected Iterable<String> keys() {
                return Arrays.asList("A", "B", "C");
            }
            @Override
            protected boolean isOptional(String k) {
                return true;
            }
        };
        
        String json;
        
        for (String[] keys : new String[][] {
            {},
            {"A"},
            {"B"},
            {"C"},
            {"A", "B"},
            {"A", "C"},
            {"B", "C"},
            {"A", "B", "C"},
        }) {
            json = makeJson(keys);
            System.out.println("    " + json);
            Map<?, ?> m = d.destructure(json);
            
            assertEquals(keys.length, m.size());
            
            for (int i = 0; i < keys.length; ++i) {
                assertEquals(keys[i], JsonString.valueOf(i), m.get(keys[i]));
            }
        }
    }
    
    @Test
    public void testMinimumAndMaximumSizes() {
        testing();
        JsonDestructurer d = new JsonDestructurer() {
            @Override
            protected String mapKey(String k) {
                return k;
            }
            @Override
            protected Iterable<String> keys() {
                return Arrays.asList("A", "B", "C");
            }
            @Override
            protected boolean isOptional(String k) {
                return true;
            }
            @Override
            protected int minimumSize() {
                return 1;
            }
            @Override
            protected int maximumSize() {
                return 2;
            }
        };
        
        String json;
        
        System.out.println("  bad:");
        for (String[] keys : new String[][] {
            {},
            {"A", "B", "C"},
        }) {
            json = makeJson(keys);
            System.out.println("    " + json);
            try {
                Map<?, ?> m = d.destructure(json);
                fail(String.format("json = \"%s\", found %s", json, m));
            } catch (IllegalArgumentException x) {
            }
        }
        
        System.out.println("  good:");
        for (String[] keys : new String[][] {
            {"A"},
            {"B"},
            {"C"},
            {"A", "B"},
            {"A", "C"},
            {"B", "C"},
        }) {
            json = makeJson(keys);
            System.out.println("    " + json);
            Map<?, ?> m = d.destructure(json);
            
            assertEquals(keys.length, m.size());
            
            for (int i = 0; i < keys.length; ++i) {
                assertEquals(keys[i], JsonString.valueOf(i), m.get(keys[i]));
            }
        }
    }
    
    @Test
    public void testCaseInsensitiveKeys() {
        testing();
        JsonDestructurer d = new JsonDestructurer() {
            @Override
            protected String mapKey(String k) {
                return k.toLowerCase(Locale.ROOT);
            }
            @Override
            protected Iterable<String> keys() {
                return Arrays.asList("a", "b", "c");
            }
            @Override
            protected boolean isOptional(String k) {
                return true;
            }
        };
        
        String json;
        
        System.out.println("  bad:");
        for (String[] keys : new String[][] {
            // testing duplicate keys
            {"A", "a"},
            {"A", "B", "b"},
            {"b", "c", "C"},
        }) {
            json = makeJson(keys);
            System.out.println("    " + json);
            try {
                Map<?, ?> m = d.destructure(json);
                fail(String.format("json = \"%s\", found %s", json, m));
            } catch (IllegalArgumentException x) {
            }
        }
        
        System.out.println("  good:");
        for (String[] keys : new String[][] {
            {"A"}, {"a"},
            {"B"}, {"b"},
            {"C"}, {"c"},
            {"a", "B"}, {"A", "b"},
            {"a", "C"}, {"A", "c"},
            {"b", "C"}, {"B", "c"},
            {"A", "B", "c"},
            {"a", "b", "C"},
        }) {
            json = makeJson(keys);
            System.out.println("    " + json);
            Map<?, ?> m = d.destructure(json);
            
            assertEquals(keys.length, m.size());
            
            for (int i = 0; i < keys.length; ++i) {
                assertEquals(keys[i], JsonString.valueOf(i), m.get(keys[i].toLowerCase(Locale.ROOT)));
            }
        }
    }
}