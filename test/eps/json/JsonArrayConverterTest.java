/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.test.Tools.*;
import static eps.util.Literals.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonArrayConverterTest {
    public JsonArrayConverterTest() {
    }
    
    @Before
    public void resetCache() {
        JsonArrayConverter.resetCache();
    }
    
    public static class NestedList<T> extends ArrayList<T> {
        public NestedList(Collection<T> c) { }
    }
    public static class NotIterable<T> {
        public NotIterable(Collection<T> c) { }
    }
    
    public interface NestedListSubtype<T> extends List<T> {
    }
    
    public class InnerList<T> extends ArrayList<T> {
        public InnerList(Collection<T> c) { }
    }
    private static class PrivateList<T> extends ArrayList<T> {
        public PrivateList(Collection<T> c) { }
    }
    public static class PrivateConstructor<T> extends ArrayList<T> {
        private PrivateConstructor(Collection<T> c) { }
    }
    public static class NoCollectionConstructor<T> extends ArrayList<T> {
        public NoCollectionConstructor() { }
    }
    
    @Test
    public void testConstructorInfoTypes() throws ReflectiveOperationException {
        testing();
        
        try {
            JsonArrayConverter.getConstructorInfo(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        for (Class<?> good : new Class<?>[] {
            ArrayList.class,
            LinkedList.class,
            HashSet.class,
            LinkedHashSet.class,
            TreeSet.class,
            NestedList.class,
            NotIterable.class, // Note: we can pull info for this one, but it's not convertible (see below)
        }) {
            JsonArrayConverter.ConstructorInfo info = JsonArrayConverter.getConstructorInfo(good);
            assertEquals(good, info.type);
            assertTrue(info.isPublic);
            assertNotNull(info.ctor);
            assertEquals(good.getConstructor(Collection.class), info.ctor);
        }
        
        for (Class<?> bad : new Class<?>[] {
            InnerList.class,
            PrivateList.class,
            PrivateConstructor.class,
            NoCollectionConstructor.class,
            PublicListSubtype.class,
            NestedListSubtype.class,
            HashMap.class,
            String.class,
            double.class,
            Object[].class,
        }) {
            JsonArrayConverter.ConstructorInfo info = JsonArrayConverter.getConstructorInfo(bad);
            assertEquals(bad, info.type);
            assertFalse(info.isPublic);
            assertNull(info.ctor);
        }
        
        LinkedHashMapOf(List.class,       ArrayList.class,
                        Set.class,        LinkedHashSet.class,
                        SortedSet.class,  TreeSet.class,
                        Collection.class, ArrayList.class)
                .forEach((key, val) -> {
            JsonArrayConverter.ConstructorInfo info = JsonArrayConverter.getConstructorInfo(key);
            assertEquals(val, info.type);
            assertTrue(info.isPublic);
            assertEquals(get(() -> val.getConstructor(Collection.class)), info.ctor);
        });
    }
    
    @Test
    public void testGetConstructor() throws ReflectiveOperationException {
        testing();
        try {
            JsonArrayConverter.getConstructor(null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            JsonArrayConverter.getConstructor(Iterable.class);
            fail();
        } catch (IllegalArgumentException x) {
        }
        assertEquals(ArrayList.class.getConstructor(Collection.class),
                     JsonArrayConverter.getConstructor(ArrayList.class));
        assertEquals(ArrayList.class.getConstructor(Collection.class),
                     JsonArrayConverter.getConstructor(List.class));
    }
    
    @Test
    public void testIsConvertible() {
        testing();
        try {
            JsonArrayConverter.isConvertible(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        for (Object good : new Object[] {
            new ArrayList<>(),
            new HashSet<>(),
            new NestedList<>(null),
        }) {
            assertTrue(good.getClass().toString(), JsonArrayConverter.isConvertible(good));
        }
        
        for (Object bad : new Object[] {
            "a String",
            new HashMap<>(),
            Collections.emptySet(),
            (Iterable<?>) Collections.emptySet()::iterator,
            new ArrayList<Object>() {},
            new Object[0],
            new InnerList<>(null),
            new PrivateList<>(null),
            new PrivateConstructor<>(null),
            new NoCollectionConstructor<>(),
            new NotIterable<>(null),
        }) {
            assertFalse(bad.getClass().toString(), JsonArrayConverter.isConvertible(bad));
        }
    }
    
    @Test
    public void testCache() {
        testing();
        for (Class<?> c : new Class<?>[] {
            ArrayList.class,
            LinkedHashSet.class,
            TreeMap.class,
        }) {
            assertSame(JsonArrayConverter.getConstructorInfo(c),
                       JsonArrayConverter.getConstructorInfo(c));
        }
    }
    
    @Test
    public void testDirectSerial() {
        testing();
        
        ArrayList<String> a = new ArrayList<>();
        Collections.addAll(a, "1", "2", "3");
        String jsonA   = Json.stringify(a, true);
        Object resultA = Json.objectify(jsonA);
        assertEquals(a, resultA);
        assertEquals(a.getClass(), resultA.getClass());
        
        HashSet<Double> b = new HashSet<>();
        Collections.addAll(b, 1.0, 2.0, 3.0);
        String jsonB   = Json.stringify(b, true);
        Object resultB = Json.objectify(jsonB);
        assertEquals(b, resultB);
        assertEquals(b.getClass(), resultB.getClass());
    }
    
    @JsonSerializable
    static class CollectionFields {
        @JsonProperty final List<Long>  longs;
        @JsonProperty final Set<Short>  shorts;
        @JsonProperty final Set<Object> objects;
        @JsonConstructor
        CollectionFields(List<Long>  longs,
                         Set<Short>  shorts,
                         Set<Object> objects) {
            this.longs   = longs;
            this.shorts  = shorts;
            this.objects = objects;
        }
    }
    
    @Test
    public void testFieldSerial() {
        testing();
        
        List<Long> longs = new LinkedList<>();
        Collections.addAll(longs, 7L, 8L, 9L);
        Set<Short> shorts = new TreeSet<>();
        Collections.addAll(shorts, Short.MIN_VALUE, Short.MAX_VALUE);
        Set<Object> objects = new LinkedHashSet<>();
        Collections.addAll(objects, "aString", null);
        
        CollectionFields expect = new CollectionFields(longs, shorts, objects);
        
        String json   = Json.stringify(expect, true);
        Object actual = Json.objectify(json);
        
        assertNotNull(actual);
        assertEquals(CollectionFields.class, actual.getClass());
        
        assertEquals(longs,   ((CollectionFields) actual).longs);
        assertEquals(shorts,  ((CollectionFields) actual).shorts);
        assertEquals(objects, ((CollectionFields) actual).objects);
        
        assertEquals(longs.getClass(),   ((CollectionFields) actual).longs.getClass());
        assertEquals(shorts.getClass(),  ((CollectionFields) actual).shorts.getClass());
        assertEquals(objects.getClass(), ((CollectionFields) actual).objects.getClass());
        
        class BadSet<T> extends HashSet<T> {
        }
        
        Set<Short> badSet = new BadSet<>();
        Collections.addAll(badSet, (short) 601);
        
        expect = new CollectionFields(longs, badSet, objects);
        
        try {
            json = Json.stringify(expect, true);
            fail(json);
        } catch (IllegalArgumentException x) {
        }
        
        assertFalse(JsonArrayConverter.getConstructorInfo(BadSet.class).isPublic);
        
        Set<Object> badObjects = new LinkedHashSet<>();
        Collections.addAll(badObjects, "aString", new Object());
        
        expect = new CollectionFields(longs, shorts, badObjects);
        
        try {
            json = Json.stringify(expect, true);
            fail(json);
        } catch (IllegalArgumentException x) {
        }
    }
}