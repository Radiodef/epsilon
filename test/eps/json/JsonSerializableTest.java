/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.math.Math2;
import static eps.test.Tools.*;

import java.lang.reflect.*;
import java.lang.annotation.*;
import java.util.concurrent.*;

import org.junit.*;
import static org.junit.Assert.*;

@JsonSerializable
class SimpleTestObject {
    @JsonProperty
    final boolean booleanValue;
    @JsonProperty
    final char charValue;
    @JsonProperty
    final byte byteValue;
    @JsonProperty
    final short shortValue;
    @JsonProperty
    final int intValue;
    @JsonProperty
    final long longValue;
    @JsonProperty
    final float floatValue;
    @JsonProperty
    final double doubleValue;
    @JsonProperty
    final String stringValue;
    @JsonProperty
    final Enum<?> enumValue;
    
    String notPropertyValue;
    
    @JsonConstructor
    SimpleTestObject(
        boolean booleanValue,
        char charValue,
        byte byteValue,
        short shortValue,
        int intValue,
        long longValue,
        float floatValue,
        double doubleValue,
        String stringValue,
        Enum<?> enumValue)
    {
        this.booleanValue = booleanValue;
        this.charValue = charValue;
        this.byteValue = byteValue;
        this.shortValue = shortValue;
        this.intValue = intValue;
        this.longValue = longValue;
        this.floatValue = floatValue;
        this.doubleValue = doubleValue;
        this.stringValue = stringValue;
        this.enumValue = enumValue;
    }
}

public class JsonSerializableTest {
    public JsonSerializableTest() {
    }
    
    @Test
    public void testSimpleObject() throws ReflectiveOperationException {
        testing();
        
        SimpleTestObject expect =
            new SimpleTestObject(
                true,
                'C',
                (byte) 1,
                (short) 2,
                3,
                4L,
                5.0f,
                6.0,
                "S",
                TimeUnit.DAYS);
        
        expect.notPropertyValue = "a String";
        
        String json = Json.stringify(expect, true);
        
        SimpleTestObject actual = (SimpleTestObject) Json.objectify(json);
        assertNotNull(actual);
        
        assertNull(actual.notPropertyValue);
        
        for (Field field : SimpleTestObject.class.getDeclaredFields()) {
            if (field.isAnnotationPresent(JsonProperty.class)) {
                field.setAccessible(true);
                assertEquals(field.getName(), field.get(expect), field.get(actual));
            }
        }
    }
    
    @JsonSerializable
    static class NestedObject {
        @JsonProperty
        final NestedObject a;
        @JsonProperty
        final NestedObject b;
        
        @JsonConstructor
        NestedObject(NestedObject a,
                     NestedObject b) {
            this.a = a;
            this.b = b;
        }
        
        // see testLoop()
        NestedObject() {
            this.a = new NestedObject(this, null);
            this.b = null;
        }
    }
    
    @Test
    public void testObjectNesting() {
        testing();
        // serialization/deserialization should preserve object identities
        // within the specified object
        
        NestedObject childP = new NestedObject(null,   null  );
        NestedObject childQ = new NestedObject(null,   childP);
        NestedObject expect = new NestedObject(childQ, childQ);
        
        String s = Json.stringify(expect, true);
        
        NestedObject actual = (NestedObject) Json.objectify(s);
        assertNotNull(actual);
        assertNotNull(actual.a);
        assertNotNull(actual.b);
        
        assertTrue(actual.a == actual.b);
        
        assertNull(actual.a.a);
        assertNotNull(actual.a.b);
        
        assertNull(actual.a.b.a);
        assertNull(actual.a.b.b);
    }
    
    @Test
    public void testLoop() {
        testing();
        // we don't handle loops in the graph
        NestedObject loop = new NestedObject();
        // no reason for serialization to not work
        String s = Json.stringify(loop, true);
        try {
            // this should fail
            Object o = Json.objectify(s);
            fail();
        } catch (IllegalArgumentException x) {
        }
    }
    
    @Test
    public void testSpecialJsonValues() {
        testing();
        assertEquals("null",  Json.stringify(null,  false));
        assertEquals("true",  Json.stringify(true,  false));
        assertEquals("false", Json.stringify(false, false));
        assertEquals("\"X\"", Json.stringify("X",   false));
    }
    
    @Test
    public void testNotSerializable() {
        testing();
        try {
            String s = Json.stringify(new Object(), false);
            fail(s);
        } catch (IllegalArgumentException x) {
        }
    }
    
    @JsonSerializable
    class Inner {
        @JsonProperty
        final int x;
        @JsonConstructor
        Inner(int x) {
            this.x = x;
        }
    }
    
    @Test
    public void testInnerClass() {
        testing();
        try {
            String s = Json.stringify(new Inner(0), false);
            fail(s);
        } catch (IllegalArgumentException x) {
        }
    }
    
    @JsonSerializable
    static class Nested {
        @JsonProperty
        final int x;
        @JsonConstructor
        Nested(int x) {
            this.x = x;
        }
    }
    
    @Test
    public void testStaticNestedClass() {
        testing();
        String s = Json.stringify(new Nested(5), false);
        Object o = Json.objectify(s);
        assertNotNull(o);
        assertEquals(Nested.class, o.getClass());
        assertEquals(5, ((Nested) o).x);
    }
    
    @JsonSerializable
    static abstract class Abstract {
        @JsonProperty
        final int x;
        @JsonConstructor
        Abstract(int x) {
            this.x = x;
        }
    }
    @JsonSerializable
    interface Interface {
    }
    @JsonSerializable
    @Retention(RetentionPolicy.RUNTIME)
    @interface Ann {
    }
    
    @Test
    @Ann
    public void testAnonymousClass() throws ReflectiveOperationException {
        testing();
        // technically these are fails just because the
        // (maybe anon) subclass is not annotated
        try {
            String s = Json.stringify(new Abstract(0) {}, false);
            fail(s);
        } catch (IllegalArgumentException x) {
        }
        try {
            String s = Json.stringify(new Interface() {}, false);
            fail(s);
        } catch (IllegalArgumentException x) {
        }
        Ann ann = getClass().getMethod("testAnonymousClass").getAnnotation(Ann.class);
        assertNotNull(ann);
        try {
            String s = Json.stringify(ann, false);
            fail(s);
        } catch (IllegalArgumentException x) {
        }
    }
    
    @JsonSerializable
    private static class Private {
        @JsonProperty
        private final int x;
        @JsonConstructor
        private Private(int x) {
            this.x = x;
        }
    }
    
    @Test
    public void testPrivateClass() {
        testing();
        String s = Json.stringify(new Private(9), false);
        Object o = Json.objectify(s);
        assertNotNull(o);
        assertEquals(Private.class, o.getClass());
        assertEquals(9, ((Private) o).x);
    }
    
    // ClassInfo rejects enums, but they are always serializable
    // even if they are not annotated.
//    @JsonSerializable
//    enum TestEnum {
//        A, B;
//        @JsonConstructor
//        TestEnum() {
//        }
//    }
//    
//    @Test
//    public void testEnum() {
//        testing();
//        try {
//            String s = Json.stringify(TestEnum.A, false);
//            fail(s);
//        } catch (IllegalArgumentException x) {
//        }
//    }
    
    @JsonSerializable
    static class Super {
        @JsonProperty
        final int a;
        @JsonConstructor
        Super(int a) {
            this.a = a;
        }
    }
    @JsonSerializable
    static class Sub extends Super {
        @JsonProperty
        final int b;
        @JsonConstructor
        Sub(int a, int b) {
            super(a);
            this.b = b;
        }
    }
    
    @Test
    public void testInheritance() {
        testing();
        Sub before = new Sub(7, 8);
        String s = Json.stringify(before, true);
        Object after = Json.objectify(s);
        
        assertTrue(after instanceof Sub);
        assertEquals(7, ((Super) after).a);
        assertEquals(8, ((Sub) after).b);
    }
    
    @JsonSerializable
    static class NoMarkedConstructor {
        @JsonProperty
        final int x;
        // no annotation
        NoMarkedConstructor(int x) {
            this.x = x;
        }
    }
    @JsonSerializable
    static class NoProperties {
        NoProperties() {
        }
    }
    
    @JsonSerializable
    static class WrongCount {
        final int a;
        final int b;
        @JsonConstructor
        WrongCount(int a, int b, int c) {
            this.a = a;
            this.b = b;
        }
    }
    
    @JsonSerializable
    static class WrongPrimitiveTypes {
        @JsonProperty
        final int a;
        @JsonProperty
        final int b;
        @JsonConstructor
        WrongPrimitiveTypes(long a, long b) {
            this.a = (int) a;
            this.b = (int) b;
        }
    }
    @JsonSerializable
    static class WrongReferenceTypes {
        @JsonProperty
        final Comparable<?> a;
        @JsonProperty
        final Comparable<?> b;
        @JsonConstructor
        WrongReferenceTypes(String a, Double b) {
            this.a = a;
            this.b = b;
        }
    }
    @JsonSerializable
    static class AssignableTypes {
        @JsonProperty
        final Long a;
        @JsonProperty
        final Double b;
        @JsonConstructor
        AssignableTypes(Number a, Number b) {
            this.a = a.longValue();
            this.b = b.doubleValue();
        }
    }
    
    @Test
    public void testConstructorSpecialCases() {
        testing();
        String s;
        
        try {
            s = Json.stringify(new NoMarkedConstructor(0), false);
            fail("constructors must be marked if there are properties");
        } catch (IllegalArgumentException x) {
        }
        
        s = Json.stringify(new NoProperties(), false);
        assertTrue("unmarked is fine if there are no properties",
                   Json.objectify(s) instanceof NoProperties);
        
        try {
            s = Json.stringify(new WrongCount(1, 2, 3), false);
            fail("constructor must have same number of parameters as properties");
        } catch (IllegalArgumentException x) {
        }
        
        try {
            s = Json.stringify(new WrongPrimitiveTypes(1, 2), false);
            fail("constructor types must be assignable from property types");
        } catch (IllegalArgumentException x) {
        }
        try {
            s = Json.stringify(new WrongReferenceTypes("a String", 0.5), false);
            fail("constructor types must be assignable from property types");
        } catch (IllegalArgumentException x) {
        }
        
        s = Json.stringify(new AssignableTypes(42, Math.E), false);
        AssignableTypes at = (AssignableTypes) Json.objectify(s);
        assertEquals(new Long(42L), at.a);
        assertEquals(new Double(Math.E), at.b);
    }
    
    @JsonSerializable
    static class ArrayTest {
        @JsonProperty
        final int[] ints;
        @JsonProperty
        final Number[] nums;
        @JsonProperty
        final Object[] objs;
        
        @JsonConstructor
        ArrayTest(int[] ints, Number[] nums, Object[] objs) {
            this.ints = ints;
            this.nums = nums;
            this.objs = objs;
        }
    }
    
    @JsonSerializable
    static class Vector2 {
        @JsonProperty
        final double x;
        @JsonProperty
        final double y;
        @JsonConstructor
        Vector2(double x, double y) {
            this.x = x;
            this.y = y;
        }
        @Override
        public int hashCode() {
            return Math2.bitHash(x) ^ Math2.bitHash(y);
        }
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Vector2) {
                Vector2 that = (Vector2) obj;
                return Math2.bitEquals(this.x, that.x) && Math2.bitEquals(this.y, that.y);
            }
            return false;
        }
    }
    
    @Test
    public void testArrays() {
        testing();
        int[]    ints = {1, 2, 3};
        Number[] nums = new Long[] {4L, 5L, 6L};
        Object[] objs = { new Vector2(0.125, 0.0625), null, "Hello, @JsonSerializable!" };
        
        ArrayTest expect = new ArrayTest(ints, nums, objs);
        String    json   = Json.stringify(expect, true);
        ArrayTest actual = (ArrayTest) Json.objectify(json);
        
        assertArrayEquals(ints, actual.ints);
        assertArrayEquals(nums, actual.nums);
        assertEquals("actual type should be preserved", Long[].class, actual.nums.getClass());
        assertArrayEquals(objs, actual.objs);
        
        // we should also handle plain arrays, not just as fields
        boolean[] bools = {true, false};
        assertArrayEquals(bools, (boolean[]) Json.objectify(Json.stringify(bools, true)));
    }
    
    // TODO: lists, collections, etc., as needed
}