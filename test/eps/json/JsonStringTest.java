/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import static eps.test.Tools.*;

import java.util.stream.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonStringTest {
    public JsonStringTest() {
    }
    
    @Test
    public void test() {
        testing();
        JsonString s = new JsonString("くもり");
        assertEquals("くもり", s.getChars());
    }
    
    @Test
    public void testValueOf() {
        testing();
        JsonString s;
        
        s = JsonString.valueOf(Math.PI);
        assertEquals(Double.toString(Math.PI), s.getChars());
        
        s = JsonString.valueOf((Object) null);
        assertEquals("null", s.getChars());
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        JsonString a, b;
        
        a = new JsonString("the quick brown fox jumped over the lazy dog");
        b = new JsonString("the quick brown fox jumped over the lazy dog");
        
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
        
        b = new JsonString("The Quick Brown Fox Jumped Over The Lazy Dog");
        assertNotEquals(a, b);
        assertNotEquals(b, a);
    }
    
    @Test
    public void testSerial() {
        testing();
        
        assertEquals(new JsonString("くもり"), Json.parse("\"くもり\""));
        assertEquals("\"くもり\"", new JsonString("くもり").toString());
        
        assertEquals(Json.INDENT + "\"くもり\"", new JsonString("くもり").toString(1));
        assertEquals(Json.INDENT + Json.INDENT + "\"くもり\"", new JsonString("くもり").toString(2));
    }
    
    @Test
    public void testEscapes() {
        testing();
        char[] chars = {
            '"', '\\',
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
            0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
            0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
            0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
        };
        
        assertEquals(chars.length, JsonString.ESCAPED_CHARACTERS.size());
        
        for (int i = 0; i < JsonString.ESCAPED_CHARACTERS.size(); ++i) {
            assertEquals(Character.toString(chars[i]), JsonString.ESCAPED_CHARACTERS.get(i));
        }
        
        String[] escapes = {
            "\\u0022", "\\u005C",
            "\\u0000", "\\u0001", "\\u0002", "\\u0003", "\\u0004",
            "\\u0005", "\\u0006", "\\u0007", "\\u0008", "\\u0009",
            "\\u000A", "\\u000B", "\\u000C", "\\u000D", "\\u000E", "\\u000F",
            "\\u0010", "\\u0011", "\\u0012", "\\u0013", "\\u0014",
            "\\u0015", "\\u0016", "\\u0017", "\\u0018", "\\u0019",
            "\\u001A", "\\u001B", "\\u001C", "\\u001D", "\\u001E", "\\u001F",
        };
        
        assertEquals(chars.length, escapes.length);
        
        for (int i = 0; i < chars.length; ++i) {
            String esc = JsonString.escape(chars[i]);
            assertNotNull(esc);
            assertEquals(escapes[i], esc);
        }
        
        String joined = new String(chars);
        String expect = Stream.of(escapes).collect(Collectors.joining("", "\"", "\""));
        String actual = new JsonString(joined).toString();
        assertEquals(expect, actual);
        
        for (int i = 0; i <= Character.MAX_VALUE; ++i) {
            String escape = JsonString.escape((char) i);
            
            boolean shouldBeEscaped = false;
            for (char c : chars) {
                if (i == c) {
                    shouldBeEscaped = true;
                    break;
                }
            }
            
            assertEquals(shouldBeEscaped, escape != null);
        }
    }
}