/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.json;

import eps.util.*;
import static eps.test.Tools.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class JsonArrayTest {
    public JsonArrayTest() {
    }
    
    @Test
    public void testConstructor() {
        testing();
        try {
            JsonArray a = new JsonArray(-1);
            fail();
        } catch (IllegalArgumentException x) {
        }
        
        for (JsonArray a : new JsonArray[] {
            new JsonArray(),
            new JsonArray(10),
        }) {
            assertTrue(a.isEmpty());
            assertEquals(0, a.size());
            assertFalse(a.iterator().hasNext());
        }
    }
    
    @Test
    public void testAdd() {
        testing();
        JsonArray a = new JsonArray();
        
        try {
            a.add(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        a.add(JsonValue.NULL);
        assertFalse(a.isEmpty());
        assertEquals(1, a.size());
        
        a.add(JsonBoolean.TRUE);
        assertFalse(a.isEmpty());
        assertEquals(2, a.size());
        
        a.add(JsonBoolean.FALSE);
        assertFalse(a.isEmpty());
        assertEquals(3, a.size());
    }
    
    @Test
    public void testGetAndSet() {
        testing();
        JsonArray a = new JsonArray();
        
        try {
            a.get(0);
            fail();
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            a.set(0, JsonValue.NULL);
            fail();
        } catch (IndexOutOfBoundsException x) {
        }
        
        a.add(JsonValue.NULL);
        
        assertEquals(JsonValue.NULL, a.get(0));
        
        try {
            a.set(0, null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertEquals(JsonValue.NULL, a.set(0, JsonBoolean.TRUE));
        assertEquals(JsonBoolean.TRUE, a.get(0));
        assertEquals(JsonBoolean.TRUE, a.set(0, JsonValue.NULL));
        
        for (int badIndex : new int[] { -1, 1, Integer.MIN_VALUE, Integer.MAX_VALUE, }) {
            try {
                a.get(badIndex);
                fail();
            } catch (IndexOutOfBoundsException x) {
            }
            try {
                a.set(badIndex, JsonValue.NULL);
                fail();
            } catch (IndexOutOfBoundsException x) {
            }
        }
        
        JsonValue[] vals = {
            JsonValue.NULL,
            JsonBoolean.TRUE,
            JsonBoolean.FALSE,
            new JsonString(""),
            new JsonNumber(Math.E),
            new JsonNumber(Math.PI),
        };
        
        while (a.size() < vals.length) {
            a.add(JsonValue.NULL);
        }
        
        assertEquals(vals.length, a.size());
        
        for (int i = 0; i < vals.length; ++i) {
            assertEquals(JsonValue.NULL, a.set(i, vals[i]));
        }
        
        for (int i = 0; i < vals.length; ++i) {
            assertEquals(vals[i], a.get(i));
            assertEquals(vals[i], a.set(i, new JsonString("")));
        }
    }
    
    @Test
    public void testIterator() {
        testing();
        JsonArray a = new JsonArray();
        
        JsonValue[] vals = {
            JsonValue.NULL,
            JsonBoolean.TRUE,
            JsonBoolean.FALSE,
            new JsonString(""),
            new JsonNumber(Math.E),
            new JsonNumber(Math.PI),
        };
        
        for (JsonValue val : vals) {
            a.add(val);
        }
        
        Iterator<JsonValue> it = a.iterator();
        assertNotNull(it);
        assertFalse(it == a.iterator());
        
        for (int i = 0;; ++i) {
            assertEquals(i < vals.length, it.hasNext());
            if (!it.hasNext())
                break;
            JsonValue val = it.next();
            assertNotNull(val);
            assertEquals(vals[i], val);
        }
        
        try {
            it.next();
            fail();
        } catch (NoSuchElementException x) {
        }
        
        it = a.iterator();
        try {
            it.remove();
            fail();
        } catch (IllegalStateException x) {
        }
        
        it.next(); // null
        it.next(); // true
        it.remove();
        it.next(); // false
        it.remove();
        
        assertEquals(vals.length - 2, a.size());
        
        for (JsonValue val : a) {
            assertFalse(val instanceof JsonBoolean);
        }
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        JsonArray a, b;
        
        a = new JsonArray();
        b = new JsonArray();
        
        assertTrue(a.equals(b));
        assertTrue(b.equals(a));
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
        
        for (JsonValue val : new JsonValue[] {
            JsonBoolean.TRUE,
            new JsonNumber(Math.E),
            JsonBoolean.FALSE,
            new JsonString("Xyz"),
        }) {
            a.add(val);
            b.add(val);
        }
        
        assertTrue(a.equals(b));
        assertTrue(b.equals(a));
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
        
        b.set(2, new JsonString(""));
        
        assertFalse(a.equals(b));
        assertFalse(b.equals(a));
    }
    
    @Test
    public void testSerial() {
        testing();
        assertEquals(new JsonArray(), Json.parse("[]"));
        assertEquals(new JsonArray(), Json.parse(" [ \n ] "));
        assertEquals("[]", new JsonArray().toString());
        
        JsonValue[] vals = {
            JsonBoolean.TRUE,
            JsonBoolean.FALSE,
            new JsonNumber(0.5),
            new JsonString("くだまの"),
        };
        JsonArray a = new JsonArray();
        for (JsonValue v : vals)
            a.add(v);
        
        assertEquals("[ true, false, 0.5, \"くだまの\" ]", a.toString());
        assertEquals(a, Json.parse("[ true, false, 0.5, \"くだまの\" ]"));
        
        JsonArray b = new JsonArray();
        b.add(a);
        b.add(new JsonArray());
        
        assertEquals("[ [ true, false, 0.5, \"くだまの\" ], [] ]", b.toString());
        assertEquals(b, Json.parse("[ [ true, false, 0.5, \"くだまの\" ], [] ]"));
        
        final String i = Json.INDENT;
        
        String expect =
              i + "["                   + "\n"
            + i + i + "["               + "\n"
            + i + i + i + "true,"       + "\n"
            + i + i + i + "false,"      + "\n"
            + i + i + i + "0.5,"        + "\n"
            + i + i + i + "\"くだまの\"" + "\n"
            + i + i + "],"              + "\n"
            + i + i + "[]"              + "\n"
            + i + "]";
        String actual = b.toString(1);
        System.out.println(actual);
        
        assertEquals(expect, actual);
        assertEquals(b, Json.parse(actual));
    }
    
    @Test
    public void testTrailingComma() {
        testing();
        JsonArray a = new JsonArray();
        
        a.add(JsonBoolean.TRUE);
        assertEquals(a, Json.parse("[ true, ]"));
        
        a.add(JsonBoolean.FALSE);
        assertEquals(a, Json.parse("[ true, false, ]"));
    }
}