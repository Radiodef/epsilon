/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import static eps.test.Tools.*;

import java.util.function.*;

import org.junit.*;
import static org.junit.Assert.*;

// property list:
// foreground
// background
// bold
// italic
// strike
// underline
// family
// size
public class StyleTest {
    public StyleTest() {
    }
    
    @Test
    public void test() {
        testing();
        Style s = new Style(ArgbColor.RED, true, true);
        
        assertEquals(ArgbColor.RED, s.getForeground());
        assertEquals(Boolean.TRUE, s.isBold());
        assertEquals(Boolean.TRUE, s.isItalic());
        
        assertNull(s.getBackground());
        assertNull(s.isStrike());
        assertNull(s.isUnderline());
        assertNull(s.getFamily());
        assertNull(s.getSize());
        
        // test foreground set
        
        s.setForeground(ArgbColor.BLACK);
        assertEquals(ArgbColor.RED, s.getForeground());
        s = s.setForeground(ArgbColor.BLACK);
        assertEquals(ArgbColor.BLACK, s.getForeground());
        
        // test bold set
        
        s.setBold(null);
        assertEquals(Boolean.TRUE, s.isBold());
        s = s.setBold(null);
        assertNull(s.isBold());
        
        // test italic set
        
        s.setItalic(false);
        assertEquals(Boolean.TRUE, s.isItalic());
        s = s.setItalic(false);
        assertEquals(Boolean.FALSE, s.isItalic());
        
        // test background set
        
        s.setBackground(ArgbColor.WHITE);
        assertNull(s.getBackground());
        s = s.setBackground(ArgbColor.WHITE);
        assertEquals(ArgbColor.WHITE, s.getBackground());
        
        // test strike set
        
        s.setStrike(false);
        assertNull(s.isStrike());
        s = s.setStrike(false);
        assertEquals(Boolean.FALSE, s.isStrike());
        
        // test underline set
        
        s.setUnderline(true);
        assertNull(s.isUnderline());
        s = s.setUnderline(true);
        assertEquals(Boolean.TRUE, s.isUnderline());
        
        // test family set
        
        s.setFamily("TestFamily");
        assertNull(s.getFamily());
        s = s.setFamily("TestFamily");
        assertEquals("TestFamily", s.getFamily());
        
        // test size set
        
        s.setSize(new Size(10));
        assertNull(s.getSize());
        s = s.setSize(new Size(10));
        assertEquals(new Size(10), s.getSize());
    }
    
    @Test
    public void testStringParsing() {
        testing();
        try {
            Style s = new Style((String) null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            Style s = Style.parse((String) null);
            fail();
        } catch (NullPointerException x) {
        }
        
        for (String bad : new String[] {
            "",
            "    \t ",
            "cheese",
            "{ \"X\": 0 }",
            "{ \"size\": {} }",
        }) {
            try {
                Style s = Style.parse(bad);
                fail();
            } catch (IllegalArgumentException x) {
            }
        }
        
        assertNull(Style.parse("null"));
        assertNull(Style.parse("  null \t"));
        
        Style s;
        
        s = Style.parse(Style.EMPTY.toJsonString());
        assertEquals(Style.EMPTY, s);
        
        s = Style.parse("{}");
        assertEquals(Style.EMPTY, s);
        
        Style expect =
            new Style.Builder()
                     .setFamily("Arial")
                     .setBold(true)
                     .setSize(new Size(1, 10))
                     .setForeground(ArgbColor.RED)
                     .build();
        assertEquals(expect, Style.parse(expect.toJsonString()));
        
        s = Style.parse("{"
                      + "    \"family\"     : \"Arial\","
                      + "    \"bold\"       : true,"
                      + "    \"italic\"     : null,"
                      + "    \"size\"       : \"+10\","
                      + "    \"fg\"         : { \"r\": 255, \"g\": 0, \"b\": 0 },"
                      + "    \"background\" : null,"
                      + "}");
        assertEquals(expect, s);
        
        expect =
            new Style.Builder()
                     .setSize(12)
                     .setBackground(ArgbColor.BLACK)
                     .build();
        assertEquals(expect, Style.parse(expect.toJsonString()));
        
        s = Style.parse("{"
                      + "    \"size\"  : 12,"
                      + "    \"bg\"    : \"000000\","
                      + "}");
        assertEquals(expect, s);
    }
    
    @Test
    public void testSetByName() {
        testing();
        Style s = Style.EMPTY;
        
        try {
            s = s.setByName(null, 2);
            fail("s = " + s);
        } catch (NullPointerException x) {
        }
        try {
            s = s.setByName(null, null);
            fail("s = " + s);
        } catch (NullPointerException x) {
        }
        
        for (String bad : new String[] {
            "",
            "      ",
            "xyz",
            "foobar",
            "The Quick Brown Fox Jumped Over The Lazy Dog",
        }) {
            try {
                s = s.setByName(bad, null);
                fail("s = " + s);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    static final class ByNameTest implements Consumer<Style> {
        final String   property;
        final Object   value;
        final Class<?> xType;
        
        ByNameTest(String property, Object value, Class<?> xType) {
            this.property = property;
            this.value    = value;
            this.xType    = xType;
        }
        
        @Override
        public void accept(Style style) {
            try {
                style = style.setByName(property, value);
                if (xType != null) {
                    fail("expected " + xType + ", got " + style);
                } else {
                    assertEquals(value, style.getByName(property));
                }
            } catch (RuntimeException x) {
                assertNotNull(x.toString(), xType);
                assertEquals(x.toString(), xType, x.getClass());
            }
        }
    }
    
    static ByNameTest byName(String property, Object value) {
        return new ByNameTest(property, value, null);
    }
    static ByNameTest byName(String property, Object value, Class<?> xType) {
        return new ByNameTest(property, value, xType);
    }
    
    @Test
    public void testSetForegroundByName() {
        testing();
        Style s = Style.EMPTY.setForeground(ArgbColor.BLACK);
        
        for (ByNameTest test : new ByNameTest[] {
            byName("foreground",       1,      IllegalArgumentException.class),
            byName("foreground",       "fail", IllegalArgumentException.class),
            byName("foreground",       null),
            byName("foreground",       ArgbColor.RED),
            byName("FOREGROUND",       ArgbColor.RED),
            byName("ForeGroUnD",       ArgbColor.RED),
            byName("fG",               ArgbColor.RED),
            byName("foregroundColor",  ArgbColor.RED),
            byName("foreground-color", ArgbColor.RED),
            byName("foreground_color", ArgbColor.RED),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testSetBackgroundByName() {
        testing();
        Style s = Style.EMPTY.setBackground(ArgbColor.BLACK);
        
        for (ByNameTest test : new ByNameTest[] {
            byName("background",       1,      IllegalArgumentException.class),
            byName("background",       "fail", IllegalArgumentException.class),
            byName("background",       null),
            byName("background",       ArgbColor.RED),
            byName("BACKGROUND",       ArgbColor.RED),
            byName("bAcKGROUnd",       ArgbColor.RED),
            byName("Bg",               ArgbColor.RED),
            byName("backgroundColor",  ArgbColor.RED),
            byName("background-color", ArgbColor.RED),
            byName("background_color", ArgbColor.RED),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testSetBoldByName() {
        testing();
        Style s = Style.EMPTY.setBold(false);
        
        for (ByNameTest test : new ByNameTest[] {
            byName("bold",    1,      IllegalArgumentException.class),
            byName("bold",    "fail", IllegalArgumentException.class),
            byName("bold",    null),
            byName("bold",    true),
            byName("BOLD",    true),
            byName("BoLd",    true),
            byName("B",       true),
            byName("b",       true),
            byName("isBold",  true),
            byName("is-bold", true),
            byName("is_bold", true),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testSetItalicByName() {
        testing();
        Style s = Style.EMPTY.setItalic(false);
        
        for (ByNameTest test : new ByNameTest[] {
            byName("italic",     1,      IllegalArgumentException.class),
            byName("italic",     "fail", IllegalArgumentException.class),
            byName("italic",     null),
            byName("italic",     true),
            byName("ITALIC",     true),
            byName("ItAlIc",     true),
            byName("I",          true),
            byName("i",          true),
            byName("isItalic",   true),
            byName("italics",    true),
            byName("IsItalics",  true),
            byName("is-italic",  true),
            byName("is_italic",  true),
            byName("is-italics", true),
            byName("is_italics", true),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testSetStrikeByName() {
        testing();
        Style s = Style.EMPTY.setStrike(false);
        
        for (ByNameTest test : new ByNameTest[] {
            byName("strike",    1,      IllegalArgumentException.class),
            byName("strike",    "fail", IllegalArgumentException.class),
            byName("strike",    null),
            byName("strike",    true),
            byName("STRIKE",    true),
            byName("sTrIkE",    true),
            byName("isStrike",  true),
            byName("is-strike", true),
            byName("is_strike", true),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testSetUnderlineByName() {
        testing();
        Style s = Style.EMPTY.setUnderline(false);
        
        for (ByNameTest test : new ByNameTest[] {
            byName("underline",     1,      IllegalArgumentException.class),
            byName("underline",     "fail", IllegalArgumentException.class),
            byName("underline",     null),
            byName("underline",     true),
            byName("UNDERLINE",     true),
            byName("UnDeRlInE",     true),
            byName("U",             true),
            byName("u",             true),
            byName("isUnderline",   true),
            byName("underlined",    true),
            byName("IsUnderlined",  true),
            byName("is-underline",  true),
            byName("is_underline",  true),
            byName("is-underlined", true),
            byName("is_underlined", true),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testSetFamilyByName() {
        testing();
        Style s = Style.EMPTY.setFamily("Courier");
        
        for (ByNameTest test : new ByNameTest[] {
            byName("family",      1,    IllegalArgumentException.class),
            byName("family",      true, IllegalArgumentException.class),
            byName("family",      null),
            byName("family",      "Arial"),
            byName("FAMILY",      "Arial"),
            byName("FaMiLy",      "Arial"),
            byName("fontFamily",  "Arial"),
            byName("font-family", "Arial"),
            byName("font_family", "Arial"),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testSetSizeByName() {
        testing();
        Style s = Style.EMPTY.setSize(new Size(10));
        
        for (ByNameTest test : new ByNameTest[] {
            byName("size",      1,      IllegalArgumentException.class),
            byName("size",      "fail", IllegalArgumentException.class),
            byName("size",      null),
            byName("size",      new Size(20)),
            byName("SIZE",      new Size(20)),
            byName("SiZe",      new Size(20)),
            byName("fontSize",  new Size(20)),
            byName("font-size", new Size(20)),
            byName("font_size", new Size(20)),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseByName() {
        testing();
        Style s = Style.EMPTY;
        
        try {
            s = s.parseByName(null, "");
            fail("s = " + s);
        } catch (NullPointerException x) {
        }
        try {
            s = s.parseByName("size", null);
            fail("s = " + s);
        } catch (NullPointerException x) {
        }
        
        for (String bad : new String[] {
            "",
            "      ",
            "xyz",
            "foobar",
            "The Quick Brown Fox Jumped Over The Lazy Dog",
        }) {
            try {
                s = s.parseByName(bad, "null");
                fail("s = " + s);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    static final class ParseTest implements Consumer<Style> {
        final String   property;
        final String   value;
        final Object   expected;
        final Class<?> xType;
        
        ParseTest(String property, String value, Object expected, Class<?> xType) {
            this.property = property;
            this.value    = value;
            this.expected = expected;
            this.xType    = xType;
        }
        
        @Override
        public void accept(Style style) {
            try {
                style = style.parseByName(property, value);
                if (xType != null) {
                    fail("expected " + xType + ", got " + style);
                } else {
                    assertEquals(expected, style.getByName(property));
                }
            } catch (RuntimeException x) {
                assertNotNull(x.toString(), xType);
                assertTrue(x.toString(), xType.isInstance(x));
            }
        }
    }
    
    static ParseTest parse(String property, String value, Object expected) {
        return new ParseTest(property, value, expected, null);
    }
    
    static ParseTest parse(String property, String value, Object expected, Class<?> xType) {
        return new ParseTest(property, value, expected, xType);
    }
    
    @Test
    public void testParseForegroundByName() {
        testing();
        Style s = Style.EMPTY.setForeground(ArgbColor.BLACK);
        
        for (ParseTest test : new ParseTest[] {
            parse("foreground",       "",           null, IllegalArgumentException.class),
            parse("foreground",       "fail",       null, IllegalArgumentException.class),
            parse("foreground",       "null",       null),
            parse("foreground",       "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
            parse("FOREGROUND",       "FFAABBCC",   new ArgbColor(0xFFAABBCC)),
            parse("ForeGroUnD",       "{ \"r\": 32, \"g\": 64, \"b\": 96 }", new ArgbColor(32, 64, 96)),
            parse("fG",               "0XffaaBBe3", new ArgbColor(0xFFAABBE3)),
            parse("foregroundColor",  "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
            parse("foreground-color", "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
            parse("foreground_color", "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseBackgroundByName() {
        testing();
        Style s = Style.EMPTY.setBackground(ArgbColor.BLACK);
        
        for (ParseTest test : new ParseTest[] {
            parse("background",       "",           null, IllegalArgumentException.class),
            parse("background",       "fail",       null, IllegalArgumentException.class),
            parse("background",       "null",       null),
            parse("background",       "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
            parse("BACKGROUND",       "FFAABBCC",   new ArgbColor(0xFFAABBCC)),
            parse("bAcKgRoUnD",       "{ \"r\": 32, \"g\": 64, \"b\": 96 }", new ArgbColor(32, 64, 96)),
            parse("Bg",               "0XffaaBBe3", new ArgbColor(0xFFAABBE3)),
            parse("backgroundColor",  "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
            parse("background-color", "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
            parse("background_color", "0xFFAABBCC", new ArgbColor(0xFFAABBCC)),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseBoldByName() {
        testing();
        Style s = Style.EMPTY.setBold(false);
        
        for (ParseTest test : new ParseTest[] {
            parse("bold",    "4",     null, IllegalArgumentException.class),
            parse("bold",    "fail",  null, IllegalArgumentException.class),
            parse("bold",    "null",  null),
            parse("bold",    "true",  true),
            parse("BOLD",    "false", false),
            parse("BoLd",    "yes",   true),
            parse("B",       "no",    false),
            parse("b",       "1",     true),
            parse("isBold",  "0",     false),
            parse("is-bold", "on",    true),
            parse("is_bold", "off",   false),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseItalicByName() {
        testing();
        Style s = Style.EMPTY.setItalic(false);
        
        for (ParseTest test : new ParseTest[] {
            parse("italic",     "4",     null, IllegalArgumentException.class),
            parse("italic",     "fail",  null, IllegalArgumentException.class),
            parse("italic",     "null",  null),
            parse("italic",     "true",  true),
            parse("ITALIC",     "false", false),
            parse("ItAlIC",     "yes",   true),
            parse("I",          "no",    false),
            parse("i",          "1",     true),
            parse("isItalic",   "0",     false),
            parse("italics",    "0",     false),
            parse("is-italic",  "on",    true),
            parse("is_italic",  "off",   false),
            parse("is_italic",  "true",  true),
            parse("is-italics", "true",  true),
            parse("is_italics", "true",  true),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseStrikeByName() {
        testing();
        Style s = Style.EMPTY.setStrike(false);
        
        for (ParseTest test : new ParseTest[] {
            parse("strike",    "X",     null, IllegalArgumentException.class),
            parse("strike",    "fail",  null, IllegalArgumentException.class),
            parse("strike",    "null",  null),
            parse("strike",    "true",  true),
            parse("STRIKE",    "false", false),
            parse("sTrIkE",    "1",     true),
            parse("isStrike",  "0",     false),
            parse("is-strike", "on",    true),
            parse("is_strike", "off",   false),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseUnderlineByName() {
        testing();
        Style s = Style.EMPTY.setUnderline(false);
        
        for (ParseTest test : new ParseTest[] {
            parse("underline",     "X",        null, IllegalArgumentException.class),
            parse("underline",     "fail",     null, IllegalArgumentException.class),
            parse("underline",     "null",     null),
            parse("underline",     "true",     true),
            parse("UNDERLINE",     "false",    false),
            parse("UnDeRlInE",     "1",        true),
            parse("U",             "0",        false),
            parse("u",             "enable",   true),
            parse("isUnderline",   "disable",  false),
            parse("underlined",    "enabled",  true),
            parse("IsUnderlined",  "disabled", false),
            parse("is-underline",  "Y",        true),
            parse("is_underline",  "N",        false),
            parse("is-underlined", "t",        true),
            parse("is_underlined", "f",        false),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseFamilyByName() {
        testing();
        Style s = Style.EMPTY.setFamily("Courier");
        
        for (ParseTest test : new ParseTest[] {
            // (no illegal arguments; accepts any String)
            parse("family",      "null",      null),
            parse("family",      "Arial",     "Arial"),
            parse("FAMILY",      "Courier",   "Courier"),
            parse("FaMiLy",      "Times",     "Times"),
            parse("fontFamily",  "SansSerif", "SansSerif"),
            parse("font-family", "Arial",     "Arial"),
            parse("font_family", "Arial",     "Arial"),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testParseSizeByName() {
        testing();
        Style s = Style.EMPTY.setSize(new Size(10));
        
        for (ParseTest test : new ParseTest[] {
            parse("size",      "X",    null, IllegalArgumentException.class),
            parse("size",      "fail", null, IllegalArgumentException.class),
            parse("size",      "null", null),
            parse("size",      "20",   new Size(20)),
            parse("SIZE",      "-3",   new Size(-1, 3)),
            parse("SiZe",      "+100", new Size(1, 100)),
            parse("fontSize",  "20",   new Size(20)),
            parse("font-size", "20",   new Size(20)),
            parse("font_size", "20",   new Size(20)),
        }) {
            test.accept(s);
        }
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        Style a, b, c;
        
        a = b = Style.EMPTY;
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertTrue(a.hashCode() == a.hashCode());
        
        a = new Style(ArgbColor.RED, true, false);
        b = new Style(ArgbColor.RED, true, false);
        c = new Style(ArgbColor.RED, true, false);
        assertEquals("A == B", a, b);
        assertEquals("B == A", b, a);
        assertEquals("A == C", a, c);
        assertEquals("C == A", c, a);
        assertEquals("B == C", b, c);
        assertEquals("C == B", c, b);
        assertEquals(a.hashCode(), b.hashCode());
        assertEquals(a.hashCode(), c.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertTrue(a.hashCode() == a.hashCode());
        
        a = new Style(ArgbColor.WHITE, false, null);
        b = new Style(ArgbColor.BLACK, false, null);
        assertNotEquals(a, b);
        assertNotEquals(b, a);
        
        class Test {
            final String property;
            final Object value;
            
            Test(String property, Object value) {
                this.property = property;
                this.value    = value;
            }
        }
        
        Style x = Style.EMPTY;
        
        for (Test t : new Test[] {
            new Test("foreground", ArgbColor.RED ),
            new Test("background", ArgbColor.BLUE),
            new Test("bold",       true          ),
            new Test("italic",     false         ),
            new Test("strike",     true          ),
            new Test("underline",  false         ),
            new Test("family",     "Courier"     ),
            new Test("size",       new Size(12)  ),
        }) {
            assertNull(x.getByName(t.property));
            Style y = x.setByName(t.property, t.value);
            
            assertNotEquals(x, y);
            assertNotEquals(y, x);
        }
    }
    
    @Test
    public void testDerive() {
        testing();
        try {
            Style.EMPTY.derive(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        Style styleA, styleB, actual, expect;
        
        styleA = Style.EMPTY;
        styleB = Style.builder()
                      .setForeground(ArgbColor.WHITE)
                      .setBackground(ArgbColor.BLACK)
                      .setFamily("Monaco")
                      .setSize(new Size(-1, 4))
                      .setBold(true)
                      .setItalic(false)
                      .setStrike(true)
                      .setUnderline(true)
                      .build();
        expect = styleB;
        actual = styleA.derive(styleB);
        
        assertEquals(expect, actual);
        
        styleA = Style.builder()
                      .setForeground(ArgbColor.PURPLE)
                      .setSize(new Size(12))
                      .setBold(true)
                      .build();
        styleB = Style.builder()
                      .setForeground(ArgbColor.MAGENTA) // set prop which is already set
                      .setFamily("Courier New")         // set prop not already set
                      .setSize(new Size(+1, 4))         // derive Size
                      .setBold(null)                    // (has no effect)
                      .build();
        expect = Style.builder()
                      .setForeground(ArgbColor.MAGENTA)
                      .setFamily("Courier New")
                      .setSize(new Size(16))
                      .setBold(true)
                      .build();
        actual = styleA.derive(styleB);
        
        assertEquals(expect, actual);
    }
    
    @Test
    public void testToBuilder() {
        testing();
        Style s = Style.builder()
                       .setForeground(ArgbColor.WHITE)
                       .setBackground(ArgbColor.BLACK)
                       .setFamily("Monaco")
                       .setSize(new Size(-1, 4))
                       .setBold(true)
                       .setItalic(false)
                       .setStrike(true)
                       .setUnderline(true)
                       .build();
        Style.Builder b = s.toBuilder();
        
        assertEquals(s.getForeground(), b.getForeground());
        assertEquals(s.getBackground(), b.getBackground());
        assertEquals(s.getFamily(),     b.getFamily()    );
        assertEquals(s.getSize(),       b.getSize()      );
        assertEquals(s.isBold(),        b.isBold()       );
        assertEquals(s.isItalic(),      b.isItalic()     );
        assertEquals(s.isStrike(),      b.isStrike()     );
        assertEquals(s.isUnderline(),   b.isUnderline()  );
        
        assertEquals(s, b.build());
    }
}