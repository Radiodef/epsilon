/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.json.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

// property list:
// foreground
// background
// bold
// italic
// strike
// underline
// family
// size
public class StyleBuilderTest {
    public StyleBuilderTest() {
    }
    
    @Test
    public void testNullaryConstructor() {
        testing();
        Style.Builder b = new Style.Builder();
        
        assertNull(b.getForeground());
        assertNull(b.getBackground());
        assertNull(b.isBold());
        assertNull(b.isItalic());
        assertNull(b.isStrike());
        assertNull(b.isUnderline());
        assertNull(b.getFamily());
        assertNull(b.getSize());
        
        assertEquals(Style.EMPTY, b.build());
    }
    
    @Test
    public void testSettersAndGetters() {
        testing();
        Style.Builder b = new Style.Builder();
        
        b.setForeground(ArgbColor.BLACK);
        assertEquals(ArgbColor.BLACK, b.getForeground());
        b.setBackground(ArgbColor.WHITE);
        assertEquals(ArgbColor.WHITE, b.getBackground());
        b.setBold(true);
        assertEquals(Boolean.TRUE, b.isBold());
        b.setItalic(false);
        assertEquals(Boolean.FALSE, b.isItalic());
        b.setStrike(true);
        assertEquals(Boolean.TRUE, b.isStrike());
        b.setUnderline(false);
        assertEquals(Boolean.FALSE, b.isUnderline());
        b.setFamily("Arial");
        assertEquals("Arial", b.getFamily());
        b.setSize(new Size(+1, 12));
        assertEquals(new Size(+1, 12), b.getSize());
        
        Style s = b.build();
        assertEquals(ArgbColor.BLACK,  s.getForeground());
        assertEquals(ArgbColor.WHITE,  s.getBackground());
        assertEquals(Boolean.TRUE,     s.isBold());
        assertEquals(Boolean.FALSE,    s.isItalic());
        assertEquals(Boolean.TRUE,     s.isStrike());
        assertEquals(Boolean.FALSE,    s.isUnderline());
        assertEquals("Arial",          s.getFamily());
        assertEquals(new Size(+1, 12), s.getSize());
        
        b.setForeground(null)
         .setBackground(null)
         .setBold(null)
         .setItalic(null)
         .setStrike(null)
         .setUnderline(null)
         .setFamily(null)
         .setSize((Size) null);
        assertEquals(Style.EMPTY, b.build());
    }
    
    private static final Style ALL_PROPERTIES_SET =
        new Style.Builder()
                 .setForeground(ArgbColor.WHITE)
                 .setBackground(ArgbColor.BLACK)
                 .setBold(true)
                 .setItalic(true)
                 .setStrike(true)
                 .setUnderline(true)
                 .setFamily("Monaco")
                 .setSize(new Size(18))
                 .build();
    
    @Test
    public void testSetToStyle() {
        testing();
        Style.Builder b = new Style.Builder();
        
        b.set(ALL_PROPERTIES_SET);
        assertEquals(ALL_PROPERTIES_SET, b.build());
        
        b.set((Style) null);
        assertEquals(Style.EMPTY, b.build());
    }
    
    @Test
    public void testClear() {
        testing();
        Style.Builder b = new Style.Builder().set(ALL_PROPERTIES_SET);
        
        assertEquals(ALL_PROPERTIES_SET, b.build());
        assertNotEquals(Style.EMPTY, b.build());
        
        b.clear();
        
        assertNotEquals(ALL_PROPERTIES_SET, b.build());
        assertEquals(Style.EMPTY, b.build());
    }
    
    @Test
    public void testSetIfPresent() {
        testing();
        Style.Builder b = new Style.Builder();
        
        b.setIfPresent(ALL_PROPERTIES_SET);
        assertEquals(ALL_PROPERTIES_SET, b.build());
        
        b.setIfPresent(Style.EMPTY);
        assertEquals(ALL_PROPERTIES_SET, b.build());
        
        b.setIfPresent((Style) null);
        assertEquals(ALL_PROPERTIES_SET, b.build());
        
        b.clear();
        Style s;
        
        s = Style.builder().setForeground(ArgbColor.WHITE).build();
        b.setIfPresent(s);
        assertEquals(ArgbColor.WHITE, b.getForeground());
        b.setForeground(null);
        assertEquals(Style.EMPTY, b.build());
        
        s = Style.builder().setBackground(ArgbColor.BLACK).build();
        b.setIfPresent(s);
        assertEquals(ArgbColor.BLACK, b.getBackground());
        b.setBackground(null);
        assertEquals(Style.EMPTY, b.build());
        
        s = Style.builder().setBold(true).build();
        b.setIfPresent(s);
        assertEquals(Boolean.TRUE, b.isBold());
        b.setBold(null);
        assertEquals(Style.EMPTY, b.build());
        
        s = Style.builder().setItalic(true).build();
        b.setIfPresent(s);
        assertEquals(Boolean.TRUE, b.isItalic());
        b.setItalic(null);
        assertEquals(Style.EMPTY, b.build());
        
        s = Style.builder().setStrike(true).build();
        b.setIfPresent(s);
        assertEquals(Boolean.TRUE, b.isStrike());
        b.setStrike(null);
        assertEquals(Style.EMPTY, b.build());
        
        s = Style.builder().setUnderline(true).build();
        b.setIfPresent(s);
        assertEquals(Boolean.TRUE, b.isUnderline());
        b.setUnderline(null);
        assertEquals(Style.EMPTY, b.build());
        
        s = Style.builder().setFamily("Natsu").build();
        b.setIfPresent(s);
        assertEquals("Natsu", b.getFamily());
        b.setFamily(null);
        assertEquals(Style.EMPTY, b.build());
        
        s = Style.builder().setSize(new Size(14)).build();
        b.setIfPresent(s);
        assertEquals(new Size(14), b.getSize());
        b.setSize((Size) null);
        assertEquals(Style.EMPTY, b.build());
    }
    
    @Test
    public void testSetAndGetByName() {
        testing();
        Style.Builder b = new Style.Builder();
        
        // Note: We tested this more rigorously in StyleTest.
        //       These tests just establish that each property
        //       setter and getter is active and generally functional.
        
        b.setByName("foreground", ArgbColor.WHITE);
        assertEquals(ArgbColor.WHITE, b.getForeground());
        assertEquals(ArgbColor.WHITE, b.getByName("foreground"));
        
        b.setByName("background", ArgbColor.BLACK);
        assertEquals(ArgbColor.BLACK, b.getBackground());
        assertEquals(ArgbColor.BLACK, b.getByName("background"));
        
        b.setByName("bold", true);
        assertEquals(Boolean.TRUE, b.isBold());
        assertEquals(Boolean.TRUE, b.getByName("bold"));
        
        b.setByName("italic", false);
        assertEquals(Boolean.FALSE, b.isItalic());
        assertEquals(Boolean.FALSE, b.getByName("italic"));
        
        b.setByName("strike", false);
        assertEquals(Boolean.FALSE, b.isStrike());
        assertEquals(Boolean.FALSE, b.getByName("strike"));
        
        b.setByName("underline", false);
        assertEquals(Boolean.FALSE, b.isUnderline());
        assertEquals(Boolean.FALSE, b.getByName("underline"));
        
        b.setByName("family", "ふゆ");
        assertEquals("ふゆ", b.getFamily());
        assertEquals("ふゆ", b.getByName("family"));
        
        b.setByName("size", new Size(18));
        assertEquals(new Size(18), b.getSize());
        assertEquals(new Size(18), b.getByName("size"));
    }
    
    @Test
    public void testParseByName() {
        testing();
        Style.Builder b = new Style.Builder();
        
        // Note: We tested this more rigorously in StyleTest.
        //       These tests just establish that each property
        //       parser is active and generally functional.
        
        b.parseByName("foreground", "AABBCCDD");
        assertEquals(new ArgbColor(0xAABBCCDD), b.getForeground());
        
        b.parseByName("background", "{ \"r\": 255, \"g\": 255, \"b\": 255 }");
        assertEquals(ArgbColor.WHITE, b.getBackground());
        
        b.parseByName("family", "ゆき");
        assertEquals("ゆき", b.getFamily());
        
        b.parseByName("size", "+0.625");
        assertEquals(new Size(+1, 0.625f), b.getSize());
        
        b.parseByName("bold", "true");
        assertEquals(Boolean.TRUE, b.isBold());
        
        b.parseByName("italic", "F");
        assertEquals(Boolean.FALSE, b.isItalic());
        
        b.parseByName("strike", "disabled");
        assertEquals(Boolean.FALSE, b.isStrike());
        
        b.parseByName("underline", "no");
        assertEquals(Boolean.FALSE, b.isUnderline());
    }
    
    @Test
    public void testClone() {
        testing();
        Style.Builder b;
        
        b = ALL_PROPERTIES_SET.toBuilder();
        assertFalse(b == b.clone());
        assertEquals(b.build(), b.clone().build());
        
        b = Style.EMPTY.toBuilder();
        assertFalse(b == b.clone());
        assertEquals(b.build(), b.clone().build());
    }
    
    @Test
    public void testJsonSerialization() {
        testing();
        Style.Builder a, b;
        
        a = ALL_PROPERTIES_SET.toBuilder();
        b = (Style.Builder) Json.objectify(Json.stringify(a, false));
        
        assertFalse(a == b);
        assertEquals(a.build(), b.build());
        
        a = Style.EMPTY.toBuilder();
        b = (Style.Builder) Json.objectify(Json.stringify(a, false));
        
        assertFalse(a == b);
        assertEquals(a.build(), b.build());
    }
}