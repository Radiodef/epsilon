/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.json.*;
import static eps.test.Tools.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundingBoxTest {
    public BoundingBoxTest() {
    }
    
    @Test
    public void testNumericConstructor() {
        testing();
        BoundingBox b;
        
        b = new BoundingBox(1, -2, 3, -5);
        
        assertEquals( 1, b.north, 0);
        assertEquals(-2, b.east,  0);
        assertEquals( 3, b.south, 0);
        assertEquals(-5, b.west,  0);
        
        assertEquals(3, b.width(),  0);
        assertEquals(2, b.height(), 0);
        
        for (double[] args : new double[][] {
            { Double.NaN, 1, 1, 1 },
            { 1, Double.NaN, 1, 1 },
            { 1, 1, Double.NaN, 1 },
            { 1, 1, 1, Double.NaN },
            { Double.POSITIVE_INFINITY, 1, 1, 1 },
            { 1, Double.POSITIVE_INFINITY, 1, 1 },
            { 1, 1, Double.POSITIVE_INFINITY, 1 },
            { 1, 1, 1, Double.POSITIVE_INFINITY },
            { Double.NEGATIVE_INFINITY, 1, 1, 1 },
            { 1, Double.NEGATIVE_INFINITY, 1, 1 },
            { 1, 1, Double.NEGATIVE_INFINITY, 1 },
            { 1, 1, 1, Double.NEGATIVE_INFINITY },
        }) {
            try {
                b = new BoundingBox(args[0], args[1], args[2], args[3]);
                fail(Arrays.toString(args));
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    @Test
    public void testStringConstructor() {
        testing();
        BoundingBox b;
        
        try {
            b = new BoundingBox((String) null);
            fail("b = " + b);
        } catch (NullPointerException x) {
        }
        try {
            b = BoundingBox.parse((String) null);
            fail("b = " + b);
        } catch (NullPointerException x) {
        }
        
        assertNull(BoundingBox.parse("null"));
        assertNull(BoundingBox.parse("  null \n\t "));
        
        for (String s : new String[] {
            Json.stringify(new BoundingBox(10, 20, 30, 40), false),
            Json.stringify(new BoundingBox(10, 20, 30, 40), true),
            new BoundingBox(10, 20, 30, 40).toJsonString(),
            "{ \"N\": 10, \"E\": 20, \"S\": 30, \"west\": 40 }",
            " {\"north\":10, \"East\": 20,\t \"sOuTh\": 30, \"W\":\n 40 }  ",
        }) {
            b = new BoundingBox(s);
            
            assertEquals(10, b.north, 0);
            assertEquals(20, b.east,  0);
            assertEquals(30, b.south, 0);
            assertEquals(40, b.west,  0);
        }
        
        for (String bad : new String[] {
            "{}",
            "[]",
            "{ \"N\": 0, \"E\": 0, \"S\": 0, \"W\": null }",
            "{ \"N\": 0, \"E\": 0, \"S\": 0, \"W\": NaN }",
            "{ \"N\": 0, \"E\": 0, \"S\": 0, \"W\": Infinity }",
            "{ \"N\": 0, \"E\": 0, \"S\": 0 }",
        }) {
            try {
                b = new BoundingBox(bad);
                fail("b = " + b);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    @Test
    public void testHashCodeAndEquals() {
        testing();
        BoundingBox a, b;
        
        a = b = new BoundingBox(1, 2, 3, 4);
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertFalse(a.equals(null));
        
        a = new BoundingBox(12, 34, 56, 78);
        b = new BoundingBox(12, 34, 56, 78);
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertFalse(a.equals(null));
        
        a = new BoundingBox(0, 0, 0, 0);
        
        for (BoundingBox c : new BoundingBox[] {
            new BoundingBox(1, 0, 0, 0),
            new BoundingBox(0, 1, 0, 0),
            new BoundingBox(0, 0, 1, 0),
            new BoundingBox(0, 0, 0, 1),
        }) {
            assertNotEquals(a, c);
            assertNotEquals(c, a);
        }
    }
}