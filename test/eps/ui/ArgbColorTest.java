/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.json.*;
import static eps.test.Tools.*;
import static eps.util.Literals.*;

import org.junit.*;
import static org.junit.Assert.*;

public class ArgbColorTest {
    public ArgbColorTest() {
    }
    
    @Test
    public void testIntConstructor() {
        testing();
        
        for (ArgbColor c : ListOf(new ArgbColor(0xAA_BB_CC_DD),
                                  new ArgbColor(0xAA, 0xBB, 0xCC, 0xDD))) {
            assertEquals(0xAA_BB_CC_DD, c.getAsInt());
            
            assertEquals(0xAA, c.getAlpha());
            assertEquals(0xBB, c.getRed());
            assertEquals(0xCC, c.getGreen());
            assertEquals(0xDD, c.getBlue());
            
            assertEquals("AABBCCDD", c.toHexArgbString());
            assertEquals(  "BBCCDD", c.toHexRgbString());
            
            String json = "{ \"alpha\": 170, \"red\": 187, \"green\": 204, \"blue\": 221 }";
            assertEquals(json, c.toJsonString());
        }
        
        ArgbColor truncated =
            new ArgbColor(0xFF_FF_FF_AA,
                          0xFF_FF_FF_BB,
                          0xFF_FF_FF_CC,
                          0xFF_FF_FF_DD);
        assertNotEquals(0xFF_FF_FF_FF, truncated.getAsInt());
        assertEquals(0xAA_BB_CC_DD, truncated.getAsInt());
    }
    
    @Test
    public void testStringConstructor() {
        testing();
        try {
            ArgbColor c = new ArgbColor((String) null);
            fail("c = " + c);
        } catch (NullPointerException x) {
        }
        try {
            ArgbColor c = ArgbColor.parse((String) null);
            fail("c = " + c);
        } catch (NullPointerException x) {
        }
        try {
            ArgbColor c = new ArgbColor("null");
            fail("c = " + c);
        } catch (NullPointerException x) {
        }
        
        assertNull(ArgbColor.parse("null"));
        assertNull(ArgbColor.parse("  null \n"));
        
        for (String bad : new String[] {
            "",
            " ",
            "X",
            "the quick brown fox jumped over the lazy dog",
            "1.0",
            "1",
            "-3",
            "-ABBCCDD",
            "0xXXYYZZ00",
            "0xAA_BB_CC_DD", // underscore not handled (TODO?)
            Json.stringify("aabbccdd", false),
            "{ \"A\": 0, \"R\": 0, \"G\": 0 }", // missing value (all must be specified)
            "{ \"R\": 0, \"G\": 0, \"B\": null }",
            "{ \"A\": 0, \"R\": 0, \"G\": 0, \"B\": 0, \"extra_value\": 1 }",
        }) {
            try {
                ArgbColor c = new ArgbColor(bad);
                fail("c = " + c);
            } catch (IllegalArgumentException x) {
            }
        }
        
        assertEquals(ArgbColor.RED,   new ArgbColor("RED"));
        assertEquals(ArgbColor.BLUE,  new ArgbColor("blue"));
        assertEquals(ArgbColor.WHITE, new ArgbColor("   WhIte \n"));
        
        for (String hex : new String[] {
            "0xFFBBCCDD",
              "FFBBCCDD",
            "0xFfBbCcDd",
                "bbccdd",
              "0XBBCCDD",
            "\tFFBBCCDD  \r\n",
        }) {
            assertEquals(0xFF_BB_CC_DD, new ArgbColor(hex).getAsInt());
        }
        
        for (String json : new String[] {
            Json.stringify(new ArgbColor(0x11_22_33_44), false),
            Json.stringify(new ArgbColor(0x11_22_33_44), true),
            new ArgbColor(0x11_22_33_44).toJsonString(),
            "{ \"A\": "+0x11+", \"r\": "+0x22+", \"green\": "+0x33+", \"BLUE\": "+0x44+" }",
            "  {  \"Alpha\"  :\n "+0x11+", \"rEd\": "+0x22+", \"G\": "+0x33+", \"b\": "+0x44+"\t } \r\n",
        }) {
            assertEquals(0x11_22_33_44, new ArgbColor(json).getAsInt());
        }
        for (String json : new String[] {
            "{ \"R\": 0, \"G\": 128, \"B\": 8 }",
            "{ \"red\": 0, \"green\": 128, \"blue\": 8 }",
        }) {
            assertEquals("default alpha is 0xFF", 0xFF_00_80_08, new ArgbColor(json).getAsInt());
        }
    }
    
    @Test
    public void testOpposite() {
        testing();
        
        ArgbColor c = new ArgbColor(0x80, 255, 0, 128);
        ArgbColor o = c.getOpposite();
        
        assertEquals(0x80, o.getAlpha());
        assertEquals(   0, o.getRed());
        assertEquals( 255, o.getGreen());
        assertEquals( 127, o.getBlue());
        
        assertEquals(c, o.getOpposite());
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        ArgbColor a, b;
        
        a = b = ArgbColor.RED;
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
        
        a = new ArgbColor(0xAA_BB_CC_DD);
        b = new ArgbColor(0xAA_BB_CC_DD);
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        a = ArgbColor.RED;
        b = ArgbColor.BLUE;
        assertNotEquals(a, b);
        assertNotEquals(b, a);
        assertNotEquals(a.hashCode(), b.hashCode());
    }
}