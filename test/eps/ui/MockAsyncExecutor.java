/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.util.*;

import java.util.*;

public final class MockAsyncExecutor implements AsyncExecutor {
    private final Thread          thread;
    private final Object          monitor;
    private final Deque<Runnable> tasks;
    
    private volatile boolean isRunning = true;
    
    public MockAsyncExecutor() {
        monitor = new Object();
        tasks   = new ArrayDeque<>();
        thread  = new Thread(this::run);
        thread.setName(getClass().getName() + ".thread");
        thread.setDaemon(true);
        thread.start();
    }
    
    public Thread getThread() {
        return thread;
    }
    
    public MockAsyncExecutor stop() {
        synchronized (monitor) {
            isRunning = false;
            monitor.notifyAll();
        }
        return this;
    }
    
    public MockAsyncExecutor join() {
        Misc.join(thread);
        return this;
    }
    
    private void run() {
        List<Runnable> list = new ArrayList<>();
        while (isRunning) {
            runTasks(list, true);
        }
        runTasks(list, false);
    }
    
    private void runTasks(List<Runnable> list, boolean wait) {
        assert list.isEmpty() : list;
        synchronized (monitor) {
            if (wait) {
                while (isRunning && tasks.isEmpty()) {
                    Misc.wait(monitor);
                }
            }
            while (!tasks.isEmpty()) {
                list.add(tasks.removeFirst());
            }
        }
        Iterator<Runnable> it = list.iterator();
        while (it.hasNext()) {
            Runnable task = it.next();
            it.remove();
            try {
                task.run();
            } catch (Throwable x) {
                Thread t = Thread.currentThread();
                Thread.UncaughtExceptionHandler handler = t.getUncaughtExceptionHandler();
                if (handler == null) {
                    throw x;
                }
                handler.uncaughtException(t, x);
            }
        }
    }
    
    @Override
    public boolean isInterfaceThread() {
        return Thread.currentThread() == thread;
    }
    
    @Override
    public void executeLater(Runnable task) {
        Objects.requireNonNull(task, "task");
        synchronized (monitor) {
            tasks.addLast(task);
            monitor.notifyAll();
        }
    }
}