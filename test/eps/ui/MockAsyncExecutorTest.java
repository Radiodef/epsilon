/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.util.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.*;
import java.util.concurrent.atomic.*;

public class MockAsyncExecutorTest {
    public MockAsyncExecutorTest() {
    }
    
    @Test
    public void testExecuteLater() {
        testing();
        MockAsyncExecutor e = new MockAsyncExecutor();
        
        AtomicInteger v = new AtomicInteger(0);
        
        for (int i = 0; i < 10; ++i) {
//            System.out.println("    i = " + i);
            e.executeLater(v::incrementAndGet);
        }
        
        e.stop().join();
        assertFalse(e.getThread().isAlive());
        
        assertEquals(10, v.get());
    }
    
    @Test
    public void testExecuteNow() {
        testing();
        MockAsyncExecutor e = new MockAsyncExecutor();
        
        AtomicInteger v = new AtomicInteger(0);
        
        for (int i = 0; i < Short.MAX_VALUE; ++i) {
//            System.out.println("    i = " + i);
            e.executeNow(v::incrementAndGet);
            assertEquals(i + 1, v.get());
        }
        
        e.stop().join();
        assertFalse(e.getThread().isAlive());
    }
    
    @Test
    public void testExceptionHandling() {
        testing();
        MockAsyncExecutor e = new MockAsyncExecutor();
        
        List<RuntimeException> excepts = Collections.synchronizedList(new ArrayList<>());
        
        excepts.add(new ClassCastException("0"));
        excepts.add(new IndexOutOfBoundsException("1"));
        excepts.add(new IllegalArgumentException("2"));
        excepts.add(new NullPointerException("3"));
        
        Thread.UncaughtExceptionHandler handler = (t, x) -> {
            int i = Integer.parseInt(x.getMessage());
            assertEquals(excepts.get(i), x);
            excepts.set(i, null);
        };
        
        e.getThread().setUncaughtExceptionHandler(handler);
        
        for (RuntimeException x : excepts) {
            e.executeLater(() -> { throw x; });
        }
        
        e.stop().join();
        assertFalse(e.getThread().isAlive());
        
        for (int i = 0; i < 4; ++i) {
            assertNull(excepts.get(i));
        }
    }
}