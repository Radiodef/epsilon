/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import static eps.test.Tools.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class SizeTest {
    public SizeTest() {
    }
    
    @Test
    public void testNumericConstructor() {
        testing();
        Size size = null;
        
        for (Object[] args : new Object[][] {
            { 0, Float.POSITIVE_INFINITY, },
            { 0, Float.NEGATIVE_INFINITY, },
            { 0, Float.NaN,               },
            { 0, -1f,                     },
        }) {
            try {
                size = new Size((int) args[0], (float) args[1]);
                fail(Arrays.toString(args));
            } catch (IllegalArgumentException x) {
            }
        }
        
        assertNull(size);
        
        // absolute
        
        size = new Size(0, 4);
        
        assertEquals(0,  size.getRelativeSign());
        assertEquals(4f, size.getAbsoluteValue(), 0);
        
        assertTrue(size.isAbsolute());
        assertFalse(size.isRelative());
        
        assertEquals(4f, size.getAbsoluteSize(),  0);
        assertEquals(0f, size.getRelativeValue(), 0);
        
        // relative -
        
        size = new Size(-1, 7);
        
        assertEquals(-1,  size.getRelativeSign());
        assertEquals( 7f, size.getAbsoluteValue(), 0);
        
        assertFalse(size.isAbsolute());
        assertTrue(size.isRelative());
        
        try {
            float abs = size.getAbsoluteSize();
            fail("abs = " + abs);
        } catch (UnsupportedOperationException x) {
        }
        
        assertEquals(-7f, size.getRelativeValue(), 0);
        
        // relative +
        
        size = new Size(1, 12);
        
        assertEquals( 1,  size.getRelativeSign());
        assertEquals(12f, size.getAbsoluteValue(), 0);
        
        assertFalse(size.isAbsolute());
        assertTrue(size.isRelative());
        
        try {
            float abs = size.getAbsoluteSize();
            fail("abs = " + abs);
        } catch (UnsupportedOperationException x) {
        }
        
        assertEquals(12f, size.getRelativeValue(), 0);
        
        // relative sign range limiting
        
        size = new Size(-1221234, 1);
        assertTrue(size.isRelative());
        assertEquals(-1,  size.getRelativeSign());
        assertEquals(-1f, size.getRelativeValue(), 0);
        
        size = new Size(5675675, 3);
        assertTrue(size.isRelative());
        assertEquals(1 , size.getRelativeSign());
        assertEquals(3f, size.getRelativeValue(), 0);
    }
    
    @Test
    public void testStringConstructor() {
        testing();
        Size size;
        
        // null handling
        
        try {
            size = new Size((String) null);
            fail("size = " + size);
        } catch (NullPointerException x) {
        }
        try {
            size = Size.parse((String) null);
            fail("size = " + size);
        } catch (NullPointerException x) {
        }
        
        size = Size.parse("null");
        assertNull(size);
        size = Size.parse("  null ");
        assertNull(size);
        
        // some bad inputs
        
        for (String bad : new String[] {
            "",
            " ",
            " \r\n",
            "Abc",
            "-1f",
            "#2"
        }) {
            try {
                size = new Size(bad);
                fail("size = " + size);
            } catch (IllegalArgumentException x) {
            }
        }
        
        float epsilon = 1f / 1024f;
        
        // absolute
        
        size = new Size("1.5");
        
        assertTrue(size.isAbsolute());
        assertFalse(size.isRelative());
        
        assertEquals(1.5f, size.getAbsoluteValue(), epsilon);
        
        // relative +
        
        size = new Size("+4.25");
        
        assertFalse(size.isAbsolute());
        assertTrue(size.isRelative());
        
        assertEquals(4.25f, size.getRelativeValue(), epsilon);
        
        // relative -
        
        size = new Size("-0.5");
        
        assertFalse(size.isAbsolute());
        assertTrue(size.isRelative());
        
        assertEquals(-0.5f, size.getRelativeValue(), epsilon);
        
        // others
        
        size = new Size(".2");
        assertEquals(0.2f, size.getAbsoluteValue(), epsilon);
        
        size = new Size("3.");
        assertTrue(size.isAbsolute());
        assertEquals("must be exact", 3f, size.getAbsoluteValue(), 0);
        
        size = new Size(" - \n 4.5 ");
        assertEquals(-4.5f, size.getRelativeValue(), epsilon);
    }
    
    @Test
    public void testDerive() {
        testing();
        Size size;
        
        try {
            size = new Size(10).derive(null);
            fail("size = " + size);
        } catch (NullPointerException x) {
        }
        
        // original is absolute
        
        size = new Size(100).derive(new Size(50));
        assertTrue(size.isAbsolute());
        assertEquals(50f, size.getAbsoluteValue(), 0);
        
        size = new Size(100).derive(new Size(-1, 25));
        assertTrue(size.isAbsolute());
        assertEquals(75f, size.getAbsoluteValue(), 0);
        
        size = new Size(100).derive(new Size(1, 50));
        assertTrue(size.isAbsolute());
        assertEquals(150f, size.getAbsoluteValue(), 0);
        
        // original is relative +
        
        size = new Size(1, 12).derive(new Size(10));
        assertTrue(size.isAbsolute());
        assertEquals(10f, size.getAbsoluteValue(), 0);
        
        size = new Size(1, 12).derive(new Size(1, 4));
        assertTrue(size.isRelative());
        assertEquals(16f, size.getRelativeValue(), 0);
        
        size = new Size(1, 12).derive(new Size(-1, 4));
        assertTrue(size.isRelative());
        assertEquals(8f, size.getRelativeValue(), 0);
        
        // sign switch + to -
        
        size = new Size(1, 12).derive(new Size(-1, 14));
        assertTrue(size.isRelative());
        assertEquals(-1, size.getRelativeSign());
        assertEquals(-2f, size.getRelativeValue(), 0);
        
        // original is relative -
        
        size = new Size(-1, 12).derive(new Size(10));
        assertTrue(size.isAbsolute());
        assertEquals(10f, size.getAbsoluteValue(), 0);
        
        size = new Size(-1, 12).derive(new Size(1, 4));
        assertTrue(size.isRelative());
        assertEquals(-8f, size.getRelativeValue(), 0);
        
        size = new Size(-1, 12).derive(new Size(-1, 4));
        assertTrue(size.isRelative());
        assertEquals(-16f, size.getRelativeValue(), 0);
        
        // sign switch - to +
        
        size = new Size(-1, 12).derive(new Size(1, 14));
        assertTrue(size.isRelative());
        assertEquals(1, size.getRelativeSign());
        assertEquals(2f, size.getRelativeValue(), 0);
        
        // absolute min is 0
        
        size = new Size(12).derive(new Size(-1, 14));
        assertTrue(size.isAbsolute());
        assertEquals(0f, size.getAbsoluteValue(), 0);
    }
    
    @Test
    public void testGetAbsoluteSize() {
        testing();
        Size abs = new Size(100f);
        Size rel = new Size(1, 10f);
        float size;
        
        // null tests
        
        try {
            size = new Size(1, 50f).getAbsoluteSize(null);
            fail("size = " + size);
        } catch (NullPointerException x) {
        }
        try {
            size = new Size(-1, 50f).getAbsoluteSize(null);
            fail("size = " + size);
        } catch (NullPointerException x) {
        }
        
        // null is fine if the size is absolute
        
        size = new Size(50f).getAbsoluteSize(null);
        assertEquals(50f, size, 0);
        
        // regular operation tests
        
        size = new Size(50f).getAbsoluteSize(abs::getAbsoluteSize);
        assertEquals(50f, size, 0);
        
        size = new Size(-1, 10f).getAbsoluteSize(abs::getAbsoluteSize);
        assertEquals(90f, size, 0);
        
        size = new Size(+1, 10f).getAbsoluteSize(abs::getAbsoluteSize);
        assertEquals(110f, size, 0);
        
        // passing a relative size test
        
        try {
            size = new Size(1, 10f).getAbsoluteSize(rel::getAbsoluteSize);
            fail("size = " + size);
        } catch (UnsupportedOperationException x) {
        }
        try {
            size = new Size(-1, 10f).getAbsoluteSize(rel::getAbsoluteSize);
            fail("size = " + size);
        } catch (UnsupportedOperationException x) {
        }
        
        // passing a relative size is fine if the size is absolute
        
        size = new Size(67f).getAbsoluteSize(rel::getAbsoluteSize);
        assertEquals(67f, size, 0);
    }
    
    @Test
    public void testToString() {
        testing();
        
        assertEquals( "50", new Size( "50").toString());
        assertEquals("+50", new Size("+50").toString());
        assertEquals("-50", new Size("-50").toString());
        
        assertEquals("-0.125", new Size("-0.125").toString());
        
        // truncation of 0.0009765625
        assertEquals("0", new Size(1f / 1024f).toString());
        // truncation of 3.14159...
        assertEquals("-3.141", new Size(-1, (float) Math.PI).toString());
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        Size a, b;
        
        a = b = new Size(1, 10);
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        
        a = new Size(-1, 203);
        b = new Size(-1, 203);
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        
        a = new Size(-1, 203);
        b = new Size(-1, 204);
        assertNotEquals(a, b);
        assertNotEquals(b, a);
        
        a = new Size(-1, 200);
        b = new Size(+1, 200);
        assertNotEquals(a, b);
        assertNotEquals(b, a);
        
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
    }
}