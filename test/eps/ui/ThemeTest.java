/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.util.*;
import eps.app.*;
import eps.json.*;
import eps.ui.Theme.*;
import static eps.test.Tools.*;

import java.util.*;
import java.lang.reflect.*;

import org.junit.*;
import static org.junit.Assert.*;

// property list:
//  name
//  background color
//  caret color
//  basic style
//  note style
//  command style
//  number style
//  literal style
//  function style
//  bracket style
//  button style
//  button selected style
//  unexpected error style
public class ThemeTest {
    public ThemeTest() {
    }
    
    @BeforeClass
    public static void themeTestInit() {
        Style defaultStyle =
            Style.builder()
                 .setForeground(ArgbColor.WHITE)
                 .setFamily("Monaco")
                 .setSize(new Size(16))
                 .build();
        Theme.setDefaultStyleSupplier(() -> defaultStyle);
    }
    
    static final String    TEST_NAME             = "TestTheme";
    static final ArgbColor TEST_BACKGROUND_COLOR = ArgbColor.BLACK;
    static final ArgbColor TEST_CARET_COLOR      = ArgbColor.DUSTY_BLUE;
    static final Style     TEST_BASIC_STYLE      = new Style(ArgbColor.WHITE);
    static final Style     TEST_NOTE_STYLE       = new Style(ArgbColor.GRAY, false, true);
    static final Style     TEST_COMMAND_STYLE    = new Style(ArgbColor.LIGHT_GRAY, false, true);
    static final Style     TEST_NUMBER_STYLE     = new Style(ArgbColor.BLUE, true, false);
    static final Style     TEST_LITERAL_STYLE    = new Style(ArgbColor.PURPLE, false, true);
    static final Style     TEST_FUNCTION_STYLE   = new Style(ArgbColor.LIME, true, false);
    static final Style     TEST_BRACKET_STYLE    = new Style(ArgbColor.LIGHT_GRAY);
    static final Style     TEST_BUTTON_STYLE     = new Style(ArgbColor.WHITE);
    static final Style     TEST_BUTTON_SEL_STYLE = new Style(ArgbColor.LIGHT_GRAY);
    static final Style     TEST_ERROR_STYLE      = new Style(ArgbColor.RED, true, false);
    
    private static final Object[] TEST_CONSTRUCTOR_ARGS = {
        TEST_NAME,
        TEST_BACKGROUND_COLOR,
        TEST_CARET_COLOR,
        TEST_BASIC_STYLE,
        TEST_NOTE_STYLE,
        TEST_COMMAND_STYLE,
        TEST_NUMBER_STYLE,
        TEST_LITERAL_STYLE,
        TEST_FUNCTION_STYLE,
        TEST_BRACKET_STYLE,
        TEST_BUTTON_STYLE,
        TEST_BUTTON_SEL_STYLE,
        TEST_ERROR_STYLE,
    };
    private static final Class<?>[] TEST_CONSTRUCTOR_TYPES =
        Arrays.stream(TEST_CONSTRUCTOR_ARGS)
              .map(Object::getClass)
              .toArray(Class<?>[]::new);
    private static final Constructor<Theme> THEME_CONSTRUCTOR;
    static {
        try {
            THEME_CONSTRUCTOR = Theme.class.getConstructor(TEST_CONSTRUCTOR_TYPES);
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
    
    private static Theme createTheme(Object[] args) {
        try {
            return THEME_CONSTRUCTOR.newInstance(args);
        } catch (InvocationTargetException x) {
            return Misc.rethrowCause(x);
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
    
    static Theme createTestTheme() {
        return new Theme(TEST_NAME,
                         TEST_BACKGROUND_COLOR,
                         TEST_CARET_COLOR,
                         TEST_BASIC_STYLE,
                         TEST_NOTE_STYLE,
                         TEST_COMMAND_STYLE,
                         TEST_NUMBER_STYLE,
                         TEST_LITERAL_STYLE,
                         TEST_FUNCTION_STYLE,
                         TEST_BRACKET_STYLE,
                         TEST_BUTTON_STYLE,
                         TEST_BUTTON_SEL_STYLE,
                         TEST_ERROR_STYLE);
    }
    
    @Test
    public void testConstructor() throws ReflectiveOperationException {
        testing();
        Theme t = createTestTheme();
        
        assertEquals(TEST_NAME,             t.getName());
        assertEquals(TEST_BACKGROUND_COLOR, t.getBackgroundColor());
        assertEquals(TEST_CARET_COLOR,      t.getCaretColor());
        assertEquals(TEST_BASIC_STYLE,      t.getBasicStyle());
        assertEquals(TEST_NOTE_STYLE,       t.getNoteStyle());
        assertEquals(TEST_COMMAND_STYLE,    t.getCommandStyle());
        assertEquals(TEST_NUMBER_STYLE,     t.getNumberStyle());
        assertEquals(TEST_LITERAL_STYLE,    t.getLiteralStyle());
        assertEquals(TEST_FUNCTION_STYLE,   t.getFunctionStyle());
        assertEquals(TEST_BRACKET_STYLE,    t.getBracketStyle());
        assertEquals(TEST_BUTTON_STYLE,     t.getButtonStyle());
        assertEquals(TEST_BUTTON_SEL_STYLE, t.getButtonSelectedStyle());
        assertEquals(TEST_ERROR_STYLE,      t.getUnexpectedErrorStyle());
        
        assertEquals(t, createTheme(TEST_CONSTRUCTOR_ARGS));
        
        for (int i = 0; i < TEST_CONSTRUCTOR_ARGS.length; ++i) {
            // note: 2 == caret color index
            
            Object[] args = TEST_CONSTRUCTOR_ARGS.clone();
            args[i] = null;
            
            try {
                Theme u = createTheme(args);
                if (i != 2) {
                    fail("null should not be allowed except for caret color; args = " + Arrays.toString(args));
                }
            } catch (NullPointerException x) {
                if (i == 2) {
                    fail("null caret color should be allowed; args = " + Arrays.toString(args));
                }
            }
        }
        
        {
            Object[] args = TEST_CONSTRUCTOR_ARGS.clone();
            // note: 3 == basic style index
            args[3] = Style.builder().setSize(new Size(+1, 4)).build();
            
            try {
                Theme u = createTheme(args);
                fail("relative basic style sizes not allowed. found " + u);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    @Test
    public void testSettingsConstructor() throws ReflectiveOperationException {
        testing();
        try {
            Theme t = new Theme((Settings) null);
            fail();
        } catch (NullPointerException x) {
        }
        
        Settings sets = Settings.Concurrent.ofDefaults();
        
        sets.set(Setting.THEME_NAME,       TEST_NAME);
        sets.set(Setting.BACKGROUND_COLOR, TEST_BACKGROUND_COLOR);
        sets.set(Setting.CARET_COLOR,      TEST_CARET_COLOR);
        sets.set(Setting.BASIC_STYLE,      TEST_BASIC_STYLE);
        sets.set(Setting.NOTE_STYLE,       TEST_NOTE_STYLE);
        sets.set(Setting.COMMAND_STYLE,    TEST_COMMAND_STYLE);
        sets.set(Setting.NUMBER_STYLE,     TEST_NUMBER_STYLE);
        sets.set(Setting.LITERAL_STYLE,    TEST_LITERAL_STYLE);
        sets.set(Setting.FUNCTION_STYLE,   TEST_FUNCTION_STYLE);
        sets.set(Setting.BRACKET_STYLE,    TEST_BRACKET_STYLE);
        sets.set(Setting.BUTTON_STYLE,     TEST_BUTTON_STYLE);
        sets.set(Setting.BUTTON_SELECTED_STYLE,  TEST_BUTTON_SEL_STYLE);
        sets.set(Setting.UNEXPECTED_ERROR_STYLE, TEST_ERROR_STYLE);
        
        assertEquals(createTestTheme(), new Theme(sets));
        
        // At the moment, there's really no reason to add a remove
        // method, so this is just a way to remove settings to test
        // the constructor setting-by-setting. (Settings instances
        // can be created which are invalid for this constructor by
        // using the copyOf family of methods, but those are not as
        // good for testing.)
        
        Field field = Settings.Concurrent.class.getDeclaredField("map");
        field.setAccessible(true);
        Map<?, ?> map = (Map<?, ?>) field.get(sets);
        
        for (Setting<?> set : new Setting<?>[] {
            Setting.THEME_NAME,
            Setting.BACKGROUND_COLOR,
            Setting.CARET_COLOR,
            Setting.BASIC_STYLE,
            Setting.NOTE_STYLE,
            Setting.COMMAND_STYLE,
            Setting.NUMBER_STYLE,
            Setting.LITERAL_STYLE,
            Setting.FUNCTION_STYLE,
            Setting.BRACKET_STYLE,
            Setting.BUTTON_STYLE,
            Setting.BUTTON_SELECTED_STYLE,
            Setting.UNEXPECTED_ERROR_STYLE,
        }) {
            assertTrue(sets.contains(set));
            Object val = map.remove(set);
            assertFalse(sets.contains(set));
            try {
                Theme t = new Theme(sets);
                if (set != Setting.CARET_COLOR) {
                    fail("set = " + set);
                }
            } catch (IllegalArgumentException x) {
                if (set == Setting.CARET_COLOR) {
                    fail(x.toString());
                }
            } finally {
                @SuppressWarnings("unchecked")
                Setting<Object> unchecked = (Setting<Object>) set;
                sets.set(unchecked, val);
            }
        }
    }
    
    @Test
    public void testCaretColor() {
        testing();
        Theme t;
        Object[] args = TEST_CONSTRUCTOR_ARGS.clone();
        
        t = createTheme(args);
        assertEquals(TEST_CARET_COLOR, t.getCaretColor());
        assertEquals(TEST_CARET_COLOR, t.getAbsoluteCaretColor());
        assertEquals(TEST_BACKGROUND_COLOR.getOpposite(), t.getComputedCaretColor());
        
        args[2] = null;
        t = createTheme(args);
        assertNull(t.getCaretColor());
        assertEquals(TEST_BACKGROUND_COLOR.getOpposite(), t.getAbsoluteCaretColor());
        assertEquals(TEST_BACKGROUND_COLOR.getOpposite(), t.getComputedCaretColor());
    }
    
    @Test
    public void testGetStyle() {
        testing();
        Theme t = createTestTheme();
        
        try {
            Style s = t.getStyle((Setting<Style>) null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            Style s = t.getStyle((Key) null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertEquals(TEST_BASIC_STYLE,      t.getStyle(Setting.BASIC_STYLE));
        assertEquals(TEST_BASIC_STYLE,      t.getStyle(Key.BASIC));
        assertEquals(TEST_NOTE_STYLE,       t.getStyle(Setting.NOTE_STYLE));
        assertEquals(TEST_NOTE_STYLE,       t.getStyle(Key.NOTE));
        assertEquals(TEST_COMMAND_STYLE,    t.getStyle(Setting.COMMAND_STYLE));
        assertEquals(TEST_COMMAND_STYLE,    t.getStyle(Key.COMMAND));
        assertEquals(TEST_NUMBER_STYLE,     t.getStyle(Setting.NUMBER_STYLE));
        assertEquals(TEST_NUMBER_STYLE,     t.getStyle(Key.NUMBER));
        assertEquals(TEST_LITERAL_STYLE,    t.getStyle(Setting.LITERAL_STYLE));
        assertEquals(TEST_LITERAL_STYLE,    t.getStyle(Key.LITERAL));
        assertEquals(TEST_FUNCTION_STYLE,   t.getStyle(Setting.FUNCTION_STYLE));
        assertEquals(TEST_FUNCTION_STYLE,   t.getStyle(Key.FUNCTION));
        assertEquals(TEST_BRACKET_STYLE,    t.getStyle(Setting.BRACKET_STYLE));
        assertEquals(TEST_BRACKET_STYLE,    t.getStyle(Key.BRACKET));
        assertEquals(TEST_BUTTON_STYLE,     t.getStyle(Setting.BUTTON_STYLE));
        assertEquals(TEST_BUTTON_STYLE,     t.getStyle(Key.BUTTON));
        assertEquals(TEST_BUTTON_SEL_STYLE, t.getStyle(Setting.BUTTON_SELECTED_STYLE));
        assertEquals(TEST_BUTTON_SEL_STYLE, t.getStyle(Key.BUTTON_SELECTED));
        assertEquals(TEST_ERROR_STYLE,      t.getStyle(Setting.UNEXPECTED_ERROR_STYLE));
        assertEquals(TEST_ERROR_STYLE,      t.getStyle(Key.INTERNAL_ERROR));
    }
    
    @Test
    public void testInstanceGetAbsoluteStyle() {
        testing();
        Theme t = createTestTheme();
        Style d = Theme.getDefaultStyle();
        Style b = d.derive(t.getBasicStyle());
        
        try {
            Style s = t.getAbsoluteStyle((Key) null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertEquals(b,                              t.getAbsoluteStyle(Key.BASIC));
        assertEquals(b.derive(t.getNoteStyle()),     t.getAbsoluteStyle(Key.NOTE));
        assertEquals(b.derive(t.getCommandStyle()),  t.getAbsoluteStyle(Key.COMMAND));
        assertEquals(b.derive(t.getNumberStyle()),   t.getAbsoluteStyle(Key.NUMBER));
        assertEquals(b.derive(t.getLiteralStyle()),  t.getAbsoluteStyle(Key.LITERAL));
        assertEquals(b.derive(t.getFunctionStyle()), t.getAbsoluteStyle(Key.FUNCTION));
        assertEquals(b.derive(t.getBracketStyle()),  t.getAbsoluteStyle(Key.BRACKET));
        assertEquals(b.derive(t.getButtonStyle()),   t.getAbsoluteStyle(Key.BUTTON));
        assertEquals(b.derive(t.getButtonStyle()).derive(t.getButtonSelectedStyle()), t.getAbsoluteStyle(Key.BUTTON_SELECTED));
        assertEquals(b.derive(t.getUnexpectedErrorStyle()), t.getAbsoluteStyle(Key.INTERNAL_ERROR));
    }
    
    @Test
    public void testStaticGetAbsoluteStyle() {
        testing();
        Theme t = createTestTheme();
        Style d = Theme.getDefaultStyle();
        Style b = d.derive(t.getBasicStyle());
        
        Settings sets = Settings.Concurrent.ofDefaults();
        MiniSettings.setLocal(sets);
        
        sets.set(Setting.THEME_NAME, t.getName());
        sets.set(Setting.BACKGROUND_COLOR, t.getBackgroundColor());
        sets.set(Setting.CARET_COLOR, t.getCaretColor());
        for (Key key : Key.values()) {
            sets.set(key.getSetting(), t.getStyle(key));
        }
        
        try {
            Style s = Theme.getAbsoluteStyle((Setting<Style>) null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertEquals(b,                              Theme.getAbsoluteStyle(Setting.BASIC_STYLE));
        assertEquals(b.derive(t.getNoteStyle()),     Theme.getAbsoluteStyle(Setting.NOTE_STYLE));
        assertEquals(b.derive(t.getCommandStyle()),  Theme.getAbsoluteStyle(Setting.COMMAND_STYLE));
        assertEquals(b.derive(t.getNumberStyle()),   Theme.getAbsoluteStyle(Setting.NUMBER_STYLE));
        assertEquals(b.derive(t.getLiteralStyle()),  Theme.getAbsoluteStyle(Setting.LITERAL_STYLE));
        assertEquals(b.derive(t.getFunctionStyle()), Theme.getAbsoluteStyle(Setting.FUNCTION_STYLE));
        assertEquals(b.derive(t.getBracketStyle()),  Theme.getAbsoluteStyle(Setting.BRACKET_STYLE));
        assertEquals(b.derive(t.getButtonStyle()),   Theme.getAbsoluteStyle(Setting.BUTTON_STYLE));
        assertEquals(b.derive(t.getButtonStyle()).derive(t.getButtonSelectedStyle()), Theme.getAbsoluteStyle(Setting.BUTTON_SELECTED_STYLE));
        assertEquals(b.derive(t.getUnexpectedErrorStyle()), Theme.getAbsoluteStyle(Setting.UNEXPECTED_ERROR_STYLE));
    }
    
    @Test
    public void testDerive() {
        testing();
        Theme t = createTestTheme();
        Style s = new Style(ArgbColor.ORCHID).setFamily("ひかり").setItalic(true);
        
        for (Key key : Key.values()) {
            Theme u = t.derive(key, s);
            assertEquals(s, u.getStyle(key));
            assertNotEquals(t, u);
        }
        
        try {
            Theme u = t.derive(Key.BASIC, null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            Theme u = t.derive(null, Style.EMPTY);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testRename() {
        testing();
        Theme t = createTestTheme();
        Theme u = t.rename("そら");
        
        assertEquals("そら", u.getName());
        
        assertEquals(t.getBackgroundColor(), u.getBackgroundColor());
        assertEquals(t.getCaretColor(),      u.getCaretColor());
        for (Key key : Key.values()) {
            assertEquals(key.name(), t.getStyle(key), u.getStyle(key));
        }
        
        assertEquals(t, u.rename(t.getName()));
        
        try {
            u = t.rename(null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testSetAll() {
        testing();
        Theme t = createTestTheme();
        
        for (boolean ws : in(true, false)) {
            try {
                t.setAll(null, ws);
                fail();
            } catch (NullPointerException x) {
            }
        }
        
        Settings sets = Settings.Concurrent.ofDefaults();
        Runnable reset = () -> {
            sets.set(Setting.THEME_NAME,       "よる");
            sets.set(Setting.BACKGROUND_COLOR, ArgbColor.FUSCHIA);
            sets.set(Setting.CARET_COLOR,      ArgbColor.MAROON);
            for (Key k : Key.values()) {
                sets.set(k.getSetting(), Style.EMPTY);
            }
        };
        reset.run();
        
        t.setAll(sets, false);
        assertEquals(t.getName(),            sets.get(Setting.THEME_NAME));
        assertEquals(t.getBackgroundColor(), sets.get(Setting.BACKGROUND_COLOR));
        assertEquals(t.getCaretColor(),      sets.get(Setting.CARET_COLOR));
        for (Key k : Key.values()) {
            assertEquals(k.name(), t.getStyle(k), sets.get(k.getSetting()));
        }
        
        reset.run();
        for (Key k : Key.values()) {
            Style s = sets.get(k.getSetting());
            sets.set(k.getSetting(), s.setSize(new Size(123)));
        }
        
        t.setAll(sets, true);
        assertEquals(t.getName(),            sets.get(Setting.THEME_NAME));
        assertEquals(t.getBackgroundColor(), sets.get(Setting.BACKGROUND_COLOR));
        assertEquals(t.getCaretColor(),      sets.get(Setting.CARET_COLOR));
        for (Key k : Key.values()) {
            assertEquals(k.name(), t.getStyle(k).setSize(new Size(123)), sets.get(k.getSetting()));
        }
    }
    
    @Test
    public void testIsPreset() {
        testing();
        
        for (Theme t : Theme.presets()) {
            assertTrue(t.isPreset());
        }
        
        assertFalse(createTestTheme().isPreset());
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        Theme a, b;
        
        a = createTestTheme();
        b = createTestTheme();
        assertEquals(a, b);
        assertEquals(b, a);
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertTrue(a.hashCode() == a.hashCode());
        
        int length = TEST_CONSTRUCTOR_ARGS.length;
        Object[] alts = new Object[length];
        alts[0] = "Not Test Theme Name";
        alts[1] = ArgbColor.BLUE;
        alts[2] = null;
        Arrays.fill(alts, 3, length, Style.EMPTY);
        
        for (int i = 0; i < length; ++i) {
            assertNotEquals(TEST_CONSTRUCTOR_ARGS[i], alts[i]);
        }
        
        for (int i = 0; i < length; ++i) {
            Object[] args = TEST_CONSTRUCTOR_ARGS.clone();
            args[i] = alts[i];
            
            b = createTheme(args);
            assertNotEquals(a, b);
            assertNotEquals(b, a);
        }
    }
    
    @Test
    public void testJsonSerialization() {
        testing();
        Set<Theme> presets = Theme.presets();
        assertFalse(presets.isEmpty());
        
        for (Theme t : presets) {
            String s = Json.stringify(t, true);
            Theme  u = (Theme) Json.objectify(s);
            assertFalse(t == u);
            assertEquals(t, u);
            assertEquals(u, t);
        }
    }
}