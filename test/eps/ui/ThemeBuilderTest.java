/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.ui;

import eps.ui.Theme.*;
import static eps.test.Tools.*;
import static eps.ui.ThemeTest.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

// property list:
//  name
//  background color
//  caret color
//  basic style
//  note style
//  command style
//  number style
//  literal style
//  function style
//  bracket style
//  button style
//  button selected style
//  unexpected error style
public class ThemeBuilderTest {
    public ThemeBuilderTest() {
    }
    
    @Test
    public void testStepBuilder() {
        testing();
        NameStep nameStep = Theme.builder();
        
        // we know it's actually a build step, and there
        // should be no way this could actually cause an
        // error which goes undiscovered.
        try {
            ((BuildStep) nameStep).build();
            fail();
        } catch (NullPointerException x) {
        }
        
        try {
            nameStep.setName(null);
            fail();
        } catch (NullPointerException x) {}
        BackgroundColorStep backgroundStep = nameStep.setName(TEST_NAME);
        
        try {
            backgroundStep.setBackgroundColor(null);
            fail();
        } catch (NullPointerException x) {}
        CaretColorStep caretStep = backgroundStep.setBackgroundColor(TEST_BACKGROUND_COLOR);
        
        caretStep.setCaretColor(null); // allowed
        BasicStyleStep basicStep = caretStep.setCaretColor(TEST_CARET_COLOR);
        
        try {
            basicStep.setBasicStyle(null);
            fail();
        } catch (NullPointerException x) {}
        NoteStyleStep noteStep = basicStep.setBasicStyle(TEST_BASIC_STYLE);
        
        try {
            noteStep.setNoteStyle(null);
            fail();
        } catch (NullPointerException x) {}
        CommandStyleStep commandStep = noteStep.setNoteStyle(TEST_NOTE_STYLE);
        
        try {
            commandStep.setCommandStyle(null);
            fail();
        } catch (NullPointerException x) {}
        NumberStyleStep numberStep = commandStep.setCommandStyle(TEST_COMMAND_STYLE);
        
        try {
            numberStep.setNumberStyle(null);
            fail();
        } catch (NullPointerException x) {}
        LiteralStyleStep literalStep = numberStep.setNumberStyle(TEST_NUMBER_STYLE);
        
        try {
            literalStep.setLiteralStyle(null);
            fail();
        } catch (NullPointerException x) {}
        FunctionStyleStep functionStep = literalStep.setLiteralStyle(TEST_LITERAL_STYLE);
        
        try {
            functionStep.setFunctionStyle(null);
            fail();
        } catch (NullPointerException x) {}
        BracketStyleStep bracketStep = functionStep.setFunctionStyle(TEST_FUNCTION_STYLE);
        
        try {
            bracketStep.setBracketStyle(null);
            fail();
        } catch (NullPointerException x) {}
        ButtonStyleStep buttonStep = bracketStep.setBracketStyle(TEST_BRACKET_STYLE);
        
        try {
            buttonStep.setButtonStyle(null);
            fail();
        } catch (NullPointerException x) {}
        ButtonSelectedStyleStep buttonSelStep = buttonStep.setButtonStyle(TEST_BUTTON_STYLE);
        
        try {
            buttonSelStep.setButtonSelectedStyle(null);
            fail();
        } catch (NullPointerException x) {}
        UnexpectedErrorStyleStep errorStep = buttonSelStep.setButtonSelectedStyle(TEST_BUTTON_SEL_STYLE);
        
        try {
            errorStep.setUnexpectedErrorStyle(null);
            fail();
        } catch (NullPointerException x) {}
        BuildStep buildStep = errorStep.setUnexpectedErrorStyle(TEST_ERROR_STYLE);
        
        assertEquals(createTestTheme(), buildStep.build());
        
        // testing continuity
        
        Theme a = Theme.AURORA;
        nameStep.setName                ( a.getName()                 )
                .setBackgroundColor     ( a.getBackgroundColor()      )
                .setCaretColor          ( a.getCaretColor()           )
                .setBasicStyle          ( a.getBasicStyle()           )
                .setNoteStyle           ( a.getNoteStyle()            )
                .setCommandStyle        ( a.getCommandStyle()         )
                .setNumberStyle         ( a.getNumberStyle()          )
                .setLiteralStyle        ( a.getLiteralStyle()         )
                .setFunctionStyle       ( a.getFunctionStyle()        )
                .setBracketStyle        ( a.getBracketStyle()         )
                .setButtonStyle         ( a.getButtonStyle()          )
                .setButtonSelectedStyle ( a.getButtonSelectedStyle()  )
                .setUnexpectedErrorStyle( a.getUnexpectedErrorStyle() );
        
        assertEquals(a, buildStep.build());
    }
    
    @Test
    public void testFreeBuilderSetAll() {
        testing();
        FreeBuilder b = new FreeBuilder(createTestTheme());
        assertEquals(createTestTheme(), b.build());
        
        Set<Theme> presets = Theme.presets();
        assertFalse(presets.isEmpty());
        for (Theme p : presets) {
            b.setAll(p);
            assertEquals(p, b.build());
        }
    }
    
    @Test
    public void testFreeBuilderSetters() {
        testing();
        Theme t = Theme.AURORA;
        
        FreeBuilder b = new FreeBuilder(createTestTheme());
        
        try {
            b.setName(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        b.setName(t.getName());
        assertEquals(t.getName(), b.build().getName());
        
        try {
            b.setBackgroundColor(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        b.setBackgroundColor(t.getBackgroundColor());
        assertEquals(t.getBackgroundColor(), b.build().getBackgroundColor());
        
        b.setCaretColor(null); // allowed
        assertNull(b.build().getCaretColor());
        
        assertNotNull(t.getCaretColor());
        b.setCaretColor(t.getCaretColor());
        assertEquals(t.getCaretColor(), b.build().getCaretColor());
        
        try {
            b.setStyle(null, Style.EMPTY);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            b.setStyle(null, null);
            fail();
        } catch (NullPointerException x) {
        }
        
        for (Key k : Key.values()) {
            try {
                b.setStyle(k, null);
            } catch (NullPointerException x) {
            }
            
            Style s = t.getStyle(k);
            b.setStyle(k, s);
            assertEquals(s, b.build().getStyle(k));
        }
        
        assertEquals(t, b.build());
    }
}