/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import static eps.test.Tools.*;

import java.lang.Integer;
import java.math.*;

import org.junit.*;
import static org.junit.Assert.*;

public class Math2Test {
    public Math2Test() {
    }
    
    @Test
    public void testPow2() {
        testing();
        
        try {
            int p = Math2.pow2(-1);
            fail("2^(-1) = " + p);
        } catch (ArithmeticException x) {
        }
        
        for (int i = 0; i < Integer.SIZE; ++i) {
            try {
                assertEquals((int) Math.pow(2, i), Math2.pow2(i));
            } catch (ArithmeticException x) {
                assertTrue(Math.pow(2, i) > Integer.MAX_VALUE);
            }
        }
    }
    
    @Test
    public void testCeilDiv() {
        testing();
        
        assertEquals( 0, Math2.ceilDiv( 0,  1));
        assertEquals( 2, Math2.ceilDiv( 4,  2));
        assertEquals( 3, Math2.ceilDiv( 7,  3));
        assertEquals(10, Math2.ceilDiv(91, 10));
        assertEquals(-1, Math2.ceilDiv(-1,  2));
        assertEquals( 1, Math2.ceilDiv(-1, -2));
        
        try {
            int n = Math2.ceilDiv(1, 0);
            fail("1/0 = " + n);
        } catch (ArithmeticException x) {
        }
        try {
            int n = Math2.ceilDiv(0, 0);
            fail("0/0 = " + n);
        } catch (ArithmeticException x) {
        }
    }
    
    @Test
    public void testDivideExact() {
        testing();
        
        for (int dividend = Byte.MIN_VALUE; dividend <= Byte.MAX_VALUE; ++dividend) {
            for (int divisor = Byte.MIN_VALUE; divisor <= Byte.MAX_VALUE; ++divisor) {
                if (divisor == 0) {
                    try {
                        long quotient = Math2.divideExact(dividend, divisor);
                        fail(dividend + "/" + divisor + " = " + quotient);
                    } catch (ArithmeticException x) {
                    }
                } else {
                    try {
                        long quotient = Math2.divideExact(dividend, divisor);
                        assertEquals(0, dividend % divisor);
                        assertEquals(dividend / divisor, quotient);
                    } catch (ArithmeticException x) {
                        assertNotEquals(0, dividend % divisor);
                    }
                }
            }
        }
    }
    
    @Test
    public void testSquareExact() {
        testing();
        
        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; ++i) {
            assertEquals((long) i * (long) i, Math2.squareExact(i));
        }
        
        long maxSqrt = (long) Math.sqrt(Long.MAX_VALUE);
        
        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; ++i) {
            long sqrt = maxSqrt + i;
            long sq   = Math2.squareExact(sqrt);
            if (sqrt <= maxSqrt) {
                assertEquals(sqrt * sqrt, sq);
            } else {
                assertTrue(sq < 0);
            }
        }
        
        assertTrue(Math2.squareExact(Long.MAX_VALUE) < 0);
        assertTrue(Math2.squareExact(Long.MIN_VALUE) < 0);
    }
    
    @Test
    public void testTrailingZeroCount() {
        testing();
        
        // int
        
        assertEquals(0, Math2.trailingZeroCount(0));
        assertEquals(0, Math2.trailingZeroCount(Integer.MAX_VALUE));
        assertEquals(0, Math2.trailingZeroCount(Integer.MIN_VALUE));
        
        for (int i = 1, count = 0; i > 0; i = Math2.multiplyUnsigned(i, 10), ++count) {
            assertEquals(count, Math2.trailingZeroCount(i));
            assertEquals(count, Math2.trailingZeroCount(-i));
            assertEquals(0, Math2.trailingZeroCount(i + 1));
        }
        
        for (int s : in(1, -1)) {
            assertEquals(3, Math2.trailingZeroCount(s * 9000));
            assertEquals(2, Math2.trailingZeroCount(s * 2400));
            assertEquals(1, Math2.trailingZeroCount(s * 6010));
            assertEquals(0, Math2.trailingZeroCount(s * 3002));
        }
        
        // long
        
        assertEquals(0L, Math2.trailingZeroCount(0L));
        assertEquals(0L, Math2.trailingZeroCount(Long.MAX_VALUE));
        assertEquals(0L, Math2.trailingZeroCount(Long.MIN_VALUE));
        
        for (long i = 1L, count = 0; i > 0L; i = Math2.multiplyUnsigned(i, 10L), ++count) {
            assertEquals(count, Math2.trailingZeroCount(i));
            assertEquals(count, Math2.trailingZeroCount(-i));
            assertEquals(0L, Math2.trailingZeroCount(i + 1L));
        }
        
        for (long s : in(1L, -1L)) {
            assertEquals(3L, Math2.trailingZeroCount(s * 9000L));
            assertEquals(2L, Math2.trailingZeroCount(s * 2400L));
            assertEquals(1L, Math2.trailingZeroCount(s * 6010L));
            assertEquals(0L, Math2.trailingZeroCount(s * 3002L));
        }
    }
    
    @Test
    public void testToLongExact() {
        testing();
        
        double dMin = Long.MIN_VALUE;
        assertEquals(Long.MIN_VALUE, new BigDecimal(dMin).toBigIntegerExact().longValueExact());
        
        assertEquals(Long.MIN_VALUE, Math2.toLongExact(dMin));
        
        try {
            long exact = Math2.toLongExact(Math.nextDown(dMin));
            fail("exact = " + exact);
        } catch (ArithmeticException x) {
        }
        
        double dMax = -dMin;
        // this should be a fail, because it's larger than Long.MAX_VALUE, of course
        try {
            long exact = Math2.toLongExact(dMax);
            fail("exact = " + exact);
        } catch (ArithmeticException x) {
        }
        
        // whatever is next down, it must be less than or equal to Long.MAX_VALUE
        dMax = Math.nextDown(dMax);
        
        assertEquals((long) dMax, Math2.toLongExact(dMax));
        
        // also, of course, passing a non-integer must fail
        try {
            long exact = Math2.toLongExact(Math.PI);
            fail("exact = " + exact);
        } catch (ArithmeticException x) {
        }
        
        // of course these should be fine
        for (int i = -10; i <= +10; ++i) {
            assertEquals((long) i, Math2.toLongExact((double) i));
        }
    }
    
    @Test
    public void testToDoubleExact() {
        testing();
        
        // double stops storing odd numbers after 2^53,
        // so we are just testing that boundary.
        
        double dMax = Math.pow(2.0, 53.0);
        double dMin = -dMax;
        long   lMax = 2L << (53 - 1);
        long   lMin = Math.negateExact(lMax);
        
        assertEquals((long) dMax, lMax);
        assertEquals((long) dMin, lMin);
        assertEquals(dMax, (double) lMax, 0.0);
        assertEquals(dMin, (double) lMin, 0.0);
        
        assertEquals(dMax, Math2.toDoubleExact(lMax), 0.0);
        assertEquals(dMin, Math2.toDoubleExact(lMin), 0.0);
        
        // these are fine
        
        long nextDownFromMax = Math.subtractExact(lMax, 1L);
        long nextUpFromMin   = Math.addExact(lMin, 1L);
        
        assertEquals(Math.nextDown(dMax), Math2.toDoubleExact(nextDownFromMax), 0.0);
        assertEquals(Math.nextUp(dMin),   Math2.toDoubleExact(nextUpFromMin),   0.0);
        
        // these are not fine
        
        long firstBadPositive = Math.addExact(lMax, 1L);
        
        try {
            double exact = Math2.toDoubleExact(firstBadPositive);
            fail("exact = " + exact);
        } catch (ArithmeticException x) {
        }
        
        long firstBadNegative = Math.subtractExact(lMin, 1L);
        
        try {
            double exact = Math2.toDoubleExact(firstBadNegative);
            fail("exact = " + exact);
        } catch (ArithmeticException x) {
        }
        
        // of course these should be fine
        for (int i = -10; i <= +10; ++i) {
            assertEquals((double) i, Math2.toDoubleExact((long) i), 0.0);
        }
    }
}