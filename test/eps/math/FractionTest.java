/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import static eps.test.Tools.*;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class FractionTest {
    private final MathEnv env;
    
    public FractionTest(Class<?> mathEnvClass) {
        env = MathEnv.instance(mathEnvClass);
    }
    
    @Parameters(name = "{index}: env = {0}")
    public static Object[][] params() {
        return new Object[][] {
            { MathEnv64.class },
        };
    }
    
    private Integer in(long n) {
        return env.valueOf(n);
    }
    private Float fl(double n) {
        return env.valueOf(n);
    }
    private Float exact(double n) {
        return env.exactValueOf(n);
    }
    private Float approx(double n) {
        return env.approxValueOf(n);
    }
    private Integer one() {
        return env.one();
    }
    private Integer zero() {
        return env.zero();
    }
    private Fraction fr(Number n, Number d) {
        return Fraction.create(n, d, env);
    }
    private Fraction fr(long n, long d) {
        return Fraction.create(env.valueOf(n), env.valueOf(d), env);
    }
    
    @Test
    public void testCreateUnchecked() {
        testing();
        try {
            Fraction f = Fraction.createUnchecked(null, one());
            fail(String.valueOf(f));
        } catch (NullPointerException x) {
        }
        try {
            Fraction f = Fraction.createUnchecked(one(), null);
            fail(String.valueOf(f));
        } catch (NullPointerException x) {
        }
        
        Fraction f;
        
        f = Fraction.createUnchecked(in(6), in(7));
        assertNotNull(f);
        assertEquals(in(6), f.getNumerator());
        assertEquals(in(7), f.getDenominator());
        
        // Fraction.create(Number, Number, MathEnv) would throw for this.
        f = Fraction.createUnchecked(zero(), zero());
        assertNotNull(f);
        assertEquals(zero(), f.getNumerator());
        assertEquals(zero(), f.getDenominator());
        
        // Fraction.create(Number, Number, MathEnv) would collapse this to a float.
        f = Fraction.createUnchecked(fl(0.5), fl(0.125));
        assertNotNull(f);
        assertEquals(fl(0.5),   f.getNumerator());
        assertEquals(fl(0.125), f.getDenominator());
    }
    
    @Test
    public void testCreate() {
        testing();
        try {
            Fraction f = Fraction.create(null, one(), env);
            fail(String.valueOf(f));
        } catch (NullPointerException x) {
        }
        try {
            Fraction f = Fraction.create(one(), null, env);
            fail(String.valueOf(f));
        } catch (NullPointerException x) {
        }
        try {
            Fraction f = Fraction.create(one(), one(), null);
            fail(String.valueOf(f));
        } catch (NullPointerException x) {
        }
        
        try {
            // integer 1/0
            Fraction f = Fraction.create(one(), zero(), env);
            fail(String.valueOf(f));
        } catch (UndefinedException x) {
        }
        try {
            // floating 1/0
            Fraction f = Fraction.create(one(), env.valueOf(0.0), env);
            fail(String.valueOf(f));
        } catch (UndefinedException x) {
        }
        
        class Test {
            final Number numer;
            final Number denom;
            final Number expectNumer;
            final Number expectDenom;
            Test(Number numer, Number denom) {
                this(numer, denom, numer, denom);
            }
            Test(Number numer, Number denom, Number expectNumer, Number expectDenom) {
                this.numer = numer;
                this.denom = denom;
                this.expectNumer = expectNumer;
                this.expectDenom = expectDenom;
            }
            @Override
            public String toString() {
                return numer + "/" + denom;
            }
        }
        
        for (Test test : new Test[] {
            // normal integer tests
            new Test(one(),  one()), // 1/1
            new Test(zero(), one()), // 0/1
            new Test(in(1),  in(4)), // 1/4
            new Test(in(4),  in(1)), // 4/1
            
            new Test(zero(), in(10), zero(), one()), // 0/10 == 0/1
            // sign tests
            new Test(in(-1), in( 4), in(-1), in(4)), // -1 /  4 == -1 / 4
            new Test(in(-1), in(-4), in( 1), in(4)), // -1 / -4 ==  1 / 4
            new Test(in( 1), in(-4), in(-1), in(4)), //  1 / -4 == -1 / 4
            new Test(in( 0), in(-1), in( 0), in(1)), //  0 / -1 ==  0 / 1
            
            // reduction tests
            new Test(in(  4), in(2), in( 2), in(1)), //  4 / 2 ==  2 / 1
            new Test(in( 10), in(6), in( 5), in(3)), // 10 / 6 ==  5 / 3
            
            // reduction and sign tests
            new Test(in( -4), in( 2), in(-2), in(1)), //  -4 /  2 == -2 / 1
            new Test(in(-10), in( 6), in(-5), in(3)), // -10 /  6 == -5 / 3
            new Test(in( -4), in(-2), in( 2), in(1)), //  -4 / -2 ==  2 / 1
            new Test(in(-10), in(-6), in( 5), in(3)), // -10 / -6 ==  5 / 3
            new Test(in(  4), in(-2), in(-2), in(1)), //   4 / -2 == -2 / 1
            new Test(in( 10), in(-6), in(-5), in(3)), //  10 / -6 == -5 / 3
            
            // float tests
            new Test(fl(-1), fl( 4), fl(-1), fl(4)), // -1 /  4 == -1 / 4
            new Test(fl(-1), fl(-4), fl( 1), fl(4)), // -1 / -4 ==  1 / 4
            new Test(fl( 1), fl(-4), fl(-1), fl(4)), //  1 / -4 == -1 / 4
            new Test(fl( 0), fl(-1), fl( 0), fl(1)), //  0 / -1 ==  0 / 1
            // note: floats don't get reduced
            new Test(fl(4.0), fl(2.0)),
            new Test(fl(0.0), fl(9.0)),
            
            // fraction of fraction tests
            new Test(fr(1, 2), fr(1, 2), in(1),  in(1)), // (1/2) / (1/2) == 1/1
            new Test(fr(3, 4), in(1),    in(3),  in(4)), // (3/4) /  1    == 3/4
            new Test(in(6),    fr(1, 3), in(18), in(1)), //  6    / (1/3) == 18/1
            
            // oddball mixed tests
            // if either is a float, we do floating division
            new Test(fr(1, 2), fl( 3.0), env.divide(fr(1, 2), fl( 3.0)), one()),
            new Test(fl( 0.5), fr(2, 3), env.divide(fl( 0.5), fr(2, 3)), one()),
        }) {
            String str = test.toString();
            
            Fraction f = Fraction.create(test.numer, test.denom, env);
            assertEquals(str, test.expectNumer, f.getNumerator());
            assertEquals(str, test.expectDenom, f.getDenominator());
        }
    }
    
    @Test
    public void testCollapseIfInteger() {
        testing();
        Fraction f;
        Number   c;
        
        f = Fraction.createUnchecked(one(), one());
        try {
            c = f.collapseIfInteger(null);
            fail(String.valueOf(c));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(in(4), one(), env);
        c = f.collapseIfInteger(env);
        assertEquals("4/1 should be 4", in(4), c);
        assertEquals(Number.Kind.INTEGER, c.getNumberKind());
        
        f = Fraction.create(zero(), one(), env);
        c = f.collapseIfInteger(env);
        assertEquals("0/1 should be 0", zero(), c);
        assertEquals(Number.Kind.INTEGER, c.getNumberKind());
        
        f = Fraction.create(in(7), in(5), env);
        c = f.collapseIfInteger(env);
        assertSame("7/5 is not an integer", f, c);
        assertEquals(Number.Kind.FRACTION, c.getNumberKind());
        
        f = Fraction.create(fl(9.0), fl(3.0), env);
        c = f.collapseIfInteger(env);
        assertSame("9.0/3.0 would be a float", f, c);
        assertEquals(Number.Kind.FRACTION, c.getNumberKind());
        
        // Fraction.create would reduce this to -3/1
        f = Fraction.createUnchecked(in(-27), in(9));
        c = f.collapseIfInteger(env);
        assertEquals("-27/9 should be -3", in(-3), c);
        assertEquals(Number.Kind.INTEGER, c.getNumberKind());
        
        // Fraction.create would reduce this to 10/1
        f = Fraction.createUnchecked(in(-50), in(-5));
        c = f.collapseIfInteger(env);
        assertEquals("-50/-5 should be 10", in(10), c);
        assertEquals(Number.Kind.INTEGER, c.getNumberKind());
        
        // Fraction.create would reduce this to -4/1
        f = Fraction.createUnchecked(in(12), in(-3));
        c = f.collapseIfInteger(env);
        assertEquals("12/-3 should be -4", in(-4), c);
        assertEquals(Number.Kind.INTEGER, c.getNumberKind());
    }
    
    @Test
    public void testCollapseIfFloat() {
        testing();
        Fraction f;
        Number   c;
        
        f = Fraction.createUnchecked(one(), one());
        try {
            c = f.collapseIfFloat(null);
            fail(String.valueOf(c));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(one(), one(), env);
        c = f.collapseIfFloat(env);
        assertSame("1/1 has no floats", c, f);
        assertEquals(Number.Kind.FRACTION, c.getNumberKind());
        
        f = Fraction.create(fl(1.0), one(), env);
        c = f.collapseIfFloat(env);
        assertEquals("1.0/1 has a float", fl(1.0), c);
        assertEquals(Number.Kind.FLOAT, c.getNumberKind());
        
        f = Fraction.create(in(4), fl(3.0), env);
        c = f.collapseIfFloat(env);
        assertEquals("4/3.0 has a float", env.divide(in(4), fl(3.0)), c);
        assertEquals(Number.Kind.FLOAT, c.getNumberKind());
        
        f = Fraction.create(fl(7.0), fl(0.5), env);
        c = f.collapseIfFloat(env);
        assertEquals("7.0/0.5 has floats", env.divide(fl(7.0), fl(0.5)), c);
        assertEquals(Number.Kind.FLOAT, c.getNumberKind());
    }
    
    @Test
    public void testCollapse() {
        testing();
        Fraction f;
        Number   c;
        
        f = Fraction.createUnchecked(one(), one());
        try {
            c = f.collapse(null);
            fail(String.valueOf(c));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(in(6), in(7), env);
        c = f.collapse(env);
        assertSame("6/7 should stay as a fraction", f, c);
        assertEquals(Number.Kind.FRACTION, c.getNumberKind());
        
        f = Fraction.create(in(14), in(2), env);
        c = f.collapse(env);
        assertEquals("14/2 is an integer", in(7), c);
        assertEquals(Number.Kind.INTEGER, c.getNumberKind());
        
        f = Fraction.create(fl(14.0), fl(3.0), env);
        c = f.collapse(env);
        assertEquals("14.0/3.0 has floats", env.divide(fl(14.0), fl(3.0)), c);
        assertEquals(Number.Kind.FLOAT, c.getNumberKind());
    }
    
    @Test
    public void testToInteger() {
        testing();
        Fraction f;
        Integer  i;
        
        f = Fraction.createUnchecked(one(), one());
        try {
            i = f.toInteger(null);
            fail(String.valueOf(i));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(zero(), one(), env);
        i = f.toInteger(env);
        assertEquals("0/1 should be 0", zero(), i);
        
        f = Fraction.create(in(8), in(4), env);
        i = f.toInteger(env);
        assertEquals("8/4 should be 2", in(2), i);
        
        f = Fraction.create(in(-9), in(3), env);
        i = f.toInteger(env);
        assertEquals("-9/3 should be -3", in(-3), i);
        
        f = Fraction.create(in(6), in(7), env);
        i = f.toInteger(env);
        assertNull("6/7 is not an integer", i);
        
        // don't reduce floats
        f = Fraction.create(fl(1.0), fl(2.0), env);
        i = f.toInteger(env);
        assertNull("1.0/2.0 has floats", i);
        
        // Fraction.create would reduce this
        f = Fraction.createUnchecked(fr(9, 3), fr(3, 2));
        i = f.toInteger(env);
        assertEquals("(9/3)/(3/2) should be 2", in(2), i);
        
        // Fraction.create would reduce this
        f = Fraction.createUnchecked(fr(2, 1), fl(1.0));
        i = f.toInteger(env);
        assertNull("(2/1)/(1.0) has a float", i);
    }
    
    @Test
    public void testToIntegerInexact() {
        testing();
        Fraction f;
        Integer  i;
        
        f = Fraction.createUnchecked(one(), one());
        try {
            i = f.toIntegerInexact(null);
            fail(String.valueOf(i));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(zero(), one(), env);
        i = f.toIntegerInexact(env);
        assertEquals("0/1 should be 0 exactly", zero(), i);
        
        f = Fraction.create(in(8), in(4), env);
        i = f.toIntegerInexact(env);
        assertEquals("8/4 should be 2 exactly", in(2), i);
        
        f = Fraction.create(in(-9), in(3), env);
        i = f.toIntegerInexact(env);
        assertEquals("-9/3 should be -3 exactly", in(-3), i);
        
        f = Fraction.create(in(6), in(7), env);
        i = f.toIntegerInexact(env);
        assertEquals("6/7 should be rounded to 0", zero(), i);
        
        f = Fraction.create(fr(3, 2), fl(0.5), env);
        i = f.toIntegerInexact(env);
        assertEquals("(3/2)/0.5 should divide as float, then round to 3", in(3), i);
        
        // Fraction.create would reduce this
        f = Fraction.createUnchecked(fr(10, 3), fr(3, 2));
        i = f.toIntegerInexact(env);
        assertEquals("(10/3)/(3/2) should divide as fractions, then divide again and round to 2", in(2), i);
    }
    
    @Test
    public void testToFloat() {
        testing();
        Fraction fr;
        Float    fl;
        
        fr = Fraction.create(one(), one(), env);
        try {
            fl = fr.toFloat(null);
            fail(String.valueOf(fl));
        } catch (NullPointerException x) {
        }
        
        fr = Fraction.create(one(), one(), env);
        fl = fr.toFloat(env);
        assertEquals("1/1 is 1", fl(1.0), fl);
        
        fr = Fraction.create(zero(), one(), env);
        fl = fr.toFloat(env);
        assertEquals("0/1 is 0", fl(0.0), fl);
        
        fr = Fraction.create(in(-8), in(4), env);
        fl = fr.toFloat(env);
        assertEquals("-8/4 is -2", fl(-2.0), fl);
        
        fr = Fraction.create(in(6), in(7), env);
        fl = fr.toFloat(env);
        // i.e. it should convert 6 and 7 to float before dividing
        assertEquals("6/7 is 0.857142...", env.divide(fl(6.0), fl(7.0)), fl);
        
        fr = Fraction.create(fl(7.5), fl(0.5), env);
        fl = fr.toFloat(env);
        assertEquals("7.5/0.5 is 15", env.divide(fl(7.5), fl(0.5)), fl);
        
        // Fraction.create would divide this
        fr = Fraction.createUnchecked(fr(1, 2), fl(0.25));
        fl = fr.toFloat(env);
        assertEquals("(1/2)/0.25 is 2", env.divide(env.divide(fl(1.0), fl(2.0)), fl(0.25)), fl);
        
        // only valid for MathEnv64
        if (env instanceof MathEnv64) {
            fr = Fraction.create(in(Long.MAX_VALUE), one(), env);
            fl = fr.toFloat(env);
            assertNull("Long.MAX_VALUE has no exact double representation", fl);
        }
    }
    
    @Test
    public void testToFloatInexact() {
        testing();
        Fraction fr;
        Float    fl;
        
        fr = Fraction.create(one(), one(), env);
        try {
            fl = fr.toFloatInexact(null);
            fail(String.valueOf(fl));
        } catch (NullPointerException x) {
        }
        
        fr = Fraction.create(one(), one(), env);
        fl = fr.toFloatInexact(env);
        assertEquals("1/1 is 1", fl(1.0), fl);
        
        fr = Fraction.create(zero(), one(), env);
        fl = fr.toFloatInexact(env);
        assertEquals("0/1 is 0", fl(0.0), fl);
        
        fr = Fraction.create(in(-8), in(4), env);
        fl = fr.toFloatInexact(env);
        assertEquals("-8/4 is -2", fl(-2.0), fl);
        
        fr = Fraction.create(in(6), in(7), env);
        fl = fr.toFloatInexact(env);
        // i.e. it should convert 6 and 7 to float before dividing
        assertEquals("6/7 is 0.857142...", env.divide(fl(6.0), fl(7.0)), fl);
        
        fr = Fraction.create(fl(7.5), fl(0.5), env);
        fl = fr.toFloatInexact(env);
        assertEquals("7.5/0.5 is 15", env.divide(fl(7.5), fl(0.5)), fl);
        
        // Fraction.create would divide this
        fr = Fraction.createUnchecked(fr(1, 2), fl(0.25));
        fl = fr.toFloatInexact(env);
        assertEquals("(1/2)/0.25 is 2", env.divide(env.divide(fl(1.0), fl(2.0)), fl(0.25)), fl);
        
        // only valid for MathEnv64
        if (env instanceof MathEnv64) {
            fr = Fraction.create(in(Long.MAX_VALUE), one(), env);
            fl = fr.toFloatInexact(env);
            assertEquals("Long.MAX_VALUE to double", fl((double) Long.MAX_VALUE), fl);
        }
    }
    
    @Test
    public void testSignum() {
        testing();
        Fraction fr;
        int     sgn;
        
        for (long[] args : new long[][] {
            {  0,  1 },
            {  0, -1 },
            {  1,  1 },
            { -1, -1 },
            { -1,  1 },
            {  1, -1 },
        }) {
            fr  = Fraction.create(in(args[0]), in(args[1]), env);
            sgn = fr.signum();
            assertEquals(args[0] * args[1], sgn);
        }
        
        assertTrue(fl(0.0).isApproximate());
        fr  = Fraction.create(fl(0.0), fl(1.0), env);
        sgn = fr.signum();
        assertEquals("sign of approximate float 0 should still be 0", 0, sgn);
    }
    
    @Test
    public void testNegate() {
        testing();
        Fraction f;
        Fraction n;
        
        // Note that Fraction.negate might return something that's
        // not a fraction, such as in the case of a MathEnv64 fraction
        // that is (Long.MIN_VALUE / 1). We don't really care about that
        // for this simple test. Those cases should work correctly if
        // their constituents do.
        
        f = Fraction.create(one(), one(), env);
        try {
            n = (Fraction) f.negate(null);
            fail(String.valueOf(n));
        } catch (NullPointerException x) {
        }
        
        // 6/7
        
        f = Fraction.create(in(6), in(7), env);
        n = (Fraction) f.negate(env);
        
        assertEquals(-1, n.signum());
        
        assertEquals(in(-6), n.getNumerator());
        assertEquals(in( 7), n.getDenominator());
        
        // -6/7
        
        f = Fraction.create(in(-6), in(7), env);
        n = (Fraction) f.negate(env);
        
        assertEquals(1, n.signum());
        
        assertEquals(in(6), n.getNumerator());
        assertEquals(in(7), n.getDenominator());
        
        // 6/-7
        
        // Fraction.create would normalize the signs
        f = Fraction.createUnchecked(in(6), in(-7));
        n = (Fraction) f.negate(env);
        
        assertEquals(1, n.signum());
        
        assertEquals(in(6), n.getNumerator());
        assertEquals(in(7), n.getDenominator());
        
        // -6/-7
        
        // Fraction.create would normalize the signs
        f = Fraction.createUnchecked(in(-6), in(-7));
        n = (Fraction) f.negate(env);
        
        assertEquals(-1, n.signum());
        
        // note: negate normalizes the signs
        assertEquals(in(-6), n.getNumerator());
        assertEquals(in( 7), n.getDenominator());
        
        // 0
        
        f = Fraction.create(zero(), one(), env);
        
        // this gets collapsed to an int
        Integer z = zero();
        assertEquals(z, f.negate(env));
    }
    
    @Test
    public void testAbs() {
        testing();
        Fraction f;
        Fraction a;
        
        // Note: same as above (see note in testNegate).
        
        f = Fraction.create(one(), one(), env);
        try {
            a = (Fraction) f.abs(null);
            fail(String.valueOf(a));
        } catch (NullPointerException x) {
        }
        
        // 6/7
        
        f = Fraction.create(in(6), in(7), env);
        a = (Fraction) f.abs(env);
        
        assertEquals(1, a.signum());
        
        assertEquals(in(6), a.getNumerator());
        assertEquals(in(7), a.getDenominator());
        
        // -6/7
        
        f = Fraction.create(in(-6), in(7), env);
        a = (Fraction) f.abs(env);
        
        assertEquals(1, a.signum());
        
        assertEquals(in(6), a.getNumerator());
        assertEquals(in(7), a.getDenominator());
        
        // 6/-7
        
        // Fraction.create would normalize the signs
        f = Fraction.createUnchecked(in(6), in(-7));
        a = (Fraction) f.abs(env);
        
        assertEquals(1, a.signum());
        
        assertEquals(in(6), a.getNumerator());
        assertEquals(in(7), a.getDenominator());
        
        // -6/-7
        
        // Fraction.create would normalize the signs
        f = Fraction.createUnchecked(in(-6), in(-7));
        a = (Fraction) f.abs(env);
        
        assertEquals(1, a.signum());
        
        // note: abs always normalizes the signs, because so does negate
        assertEquals(in(6), a.getNumerator());
        assertEquals(in(7), a.getDenominator());
        
        // 0
        
        f = Fraction.create(zero(), one(), env);
        // this gets collapsed to an int
        Integer z = zero();
        assertEquals(z, f.abs(env));
    }
    
    @Test
    public void testFloor() {
        testing();
        Fraction f;
        Number   r;
        
        f = Fraction.create(one(), one(), env);
        try {
            r = f.floor(null);
            fail(String.valueOf(r));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(one(), one(), env);
        r = f.floor(env);
        assertEquals("1/1 = 1", one(), r);
        
        f = Fraction.create(zero(), one(), env);
        r = f.floor(env);
        assertEquals("0/1 = 0", zero(), r);
        
        f = Fraction.create(in(3), in(2), env);
        r = f.floor(env);
        assertEquals("floor(3/2) = floor(1.5) = 1", one(), r);
        
        f = Fraction.create(in(-3), in(2), env);
        r = f.floor(env);
        assertEquals("floor(-3/2) = floor(-1.5) = -2", in(-2), r);
        
        f = Fraction.create(fl(3.5), in(2), env);
        r = f.floor(env);
        assertEquals("floor(3.5/2) = floor(1.75) = 1", fl(1.0), r);
        
        f = Fraction.create(fl(-3.5), in(2), env);
        r = f.floor(env);
        assertEquals("floor(-3.5/2) = floor(-1.75) = -2", fl(-2.0), r);
    }
    
    @Test
    public void testCeil() {
        testing();
        Fraction f;
        Number   r;
        
        f = Fraction.create(one(), one(), env);
        try {
            r = f.ceil(null);
            fail(String.valueOf(r));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(one(), one(), env);
        r = f.ceil(env);
        assertEquals("1/1 = 1", one(), r);
        
        f = Fraction.create(zero(), one(), env);
        r = f.ceil(env);
        assertEquals("0/1 = 0", zero(), r);
        
        f = Fraction.create(in(3), in(2), env);
        r = f.ceil(env);
        assertEquals("ceil(3/2) = ceil(1.5) = 2", in(2), r);
        
        f = Fraction.create(in(-3), in(2), env);
        r = f.ceil(env);
        assertEquals("ceil(-3/2) = ceil(-1.5) = -1", in(-1), r);
        
        f = Fraction.create(fl(3.5), in(2), env);
        r = f.ceil(env);
        assertEquals("ceil(3.5/2) = ceil(1.75) = 2", fl(2.0), r);
        
        f = Fraction.create(fl(-3.5), in(2), env);
        r = f.ceil(env);
        assertEquals("ceil(-3.5/2) = ceil(-1.75) = -1", fl(-1.0), r);
    }
    
    @Test
    public void testReciprocal() {
        testing();
        Fraction f;
        Number   r;
        
        f = Fraction.create(one(), one(), env);
        try {
            r = f.reciprocal(null);
            fail(String.valueOf(r));
        } catch (NullPointerException x) {
        }
        
        f = Fraction.create(one(), one(), env);
        r = f.reciprocal(env);
        assertEquals(one(), r);
        
        f = Fraction.create(zero(), one(), env);
        try {
            r = f.reciprocal(env);
            fail("1/0, found " + r);
        } catch (UndefinedException x) {
        }
        
        f = Fraction.create(in(1), in(2), env);
        r = f.reciprocal(env);
        assertEquals(in(2), r);
        
        f = Fraction.create(in(2), in(1), env);
        r = f.reciprocal(env);
        assertTrue(r instanceof Fraction);
        assertEquals(in(1), ((Fraction) r).getNumerator());
        assertEquals(in(2), ((Fraction) r).getDenominator());
        
        f = Fraction.create(in(-3), in(2), env);
        r = f.reciprocal(env);
        assertTrue(r instanceof Fraction);
        assertEquals(in(-2), ((Fraction) r).getNumerator());
        assertEquals(in( 3), ((Fraction) r).getDenominator());
        
        f = Fraction.create(in(1), fl(1.5), env);
        r = f.reciprocal(env);
        assertEquals(fl(1.5), r);
    }
    
    @Test
    public void testRelateTo() {
        testing();
        Fraction a;
        Fraction b;
        int      r;
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        try {
            r = a.relateTo(null, env);
            fail(String.valueOf(r));
        } catch (NullPointerException x) {
        }
        try {
            r = a.relateTo(b, null);
            fail(String.valueOf(r));
        } catch (NullPointerException x) {
        }
        
        // identity tests
        
        assertEquals("a = a", 0, a.relateTo(a, env));
        assertEquals("b = b", 0, b.relateTo(b, env));
        assertEquals("1 = 1", 0, a.relateTo(b, env));
        assertEquals("1 = 1", 0, b.relateTo(a, env));
        
        // zero tests
        
        a = Fraction.create(zero(), one(), env);
        b = Fraction.create(zero(), one(), env);
        assertEquals("0 = 0", 0, a.relateTo(b, env));
        assertEquals("0 = 0", 0, b.relateTo(a, env));
        
        b = Fraction.create(one(), one(), env);
        assertEquals("0 < 1", -1, a.relateTo(b, env));
        assertEquals("1 > 0", +1, b.relateTo(a, env));
        
        b = Fraction.create(in(-1), one(), env);
        assertEquals(" 0 > -1", +1, a.relateTo(b, env));
        assertEquals("-1 <  0", -1, b.relateTo(a, env));
        
        // integer tests
        
        a = Fraction.create(in(3), in(5), env);
        b = Fraction.create(in(4), in(5), env);
        assertEquals("3/5 < 4/5", -1, a.relateTo(b, env));
        assertEquals("4/5 > 3/5", +1, b.relateTo(a, env));
        
        a = Fraction.create(in(2), in(3), env);
        b = Fraction.create(in(3), in(2), env);
        assertEquals("2/3 < 3/2", -1, a.relateTo(b, env));
        assertEquals("3/2 > 2/3", +1, b.relateTo(a, env));
        
        a = Fraction.create(in(+1), in(2), env);
        b = Fraction.create(in(-1), in(2), env);
        assertEquals(" 1/2 > -1/2", +1, a.relateTo(b, env));
        assertEquals("-1/2 <  1/2", -1, b.relateTo(a, env));
        
        // float tests
        
        a = Fraction.create(fl(3.0), fl(5.0), env);
        b = Fraction.create(fl(4.0), fl(5.0), env);
        assertEquals("3/5 < 4/5", -1, a.relateTo(b, env));
        assertEquals("4/5 > 3/5", +1, b.relateTo(a, env));
        
        a = Fraction.create(fl(2.0), fl(3.0), env);
        b = Fraction.create(fl(3.0), fl(2.0), env);
        assertEquals("2/3 < 3/2", -1, a.relateTo(b, env));
        assertEquals("3/2 > 2/3", +1, b.relateTo(a, env));
        
        a = Fraction.create(fl(+1.0), fl(2.0), env);
        b = Fraction.create(fl(-1.0), fl(2.0), env);
        assertEquals(" 1/2 > -1/2", +1, a.relateTo(b, env));
        assertEquals("-1/2 <  1/2", -1, b.relateTo(a, env));
    }
    
    @Test
    public void testAdd() {
        testing();
        Fraction a, b;
        Number   s;
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        try {
            s = a.add(null, env);
            fail(String.valueOf(s));
        } catch (NullPointerException x) {
        }
        try {
            s = a.add(b, null);
            fail(String.valueOf(s));
        } catch (NullPointerException x) {
        }
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        s = a.add(b, env);
        assertEquals("1+1 = 2", in(2), s);
        s = b.add(a, env);
        assertEquals("1+1 = 2", in(2), s);
        
        a = Fraction.create(one(),  one(), env);
        b = Fraction.create(zero(), one(), env);
        s = a.add(b, env);
        assertEquals("1+0 = 1", one(), s);
        s = b.add(a, env);
        assertEquals("1+0 = 1", one(), s);
        
        a = Fraction.create(in(2), in(3), env);
        b = Fraction.create(in(3), in(2), env);
        s = a.add(b, env);
        assertEquals("(2/3)+(3/2) = (4/6)+(9/6) = 13/6", Fraction.create(in(13), in(6), env), s);
        s = b.add(a, env);
        assertEquals("(3/2)+(2/3) = (9/6)+(4/6) = 13/6", Fraction.create(in(13), in(6), env), s);
        
        a = Fraction.create(in( 2), in(3), env);
        b = Fraction.create(in(-1), in(3), env);
        s = a.add(b, env);
        assertEquals("( 2/3)+(-1/3) = 1/3", Fraction.create(in(1), in(3), env), s);
        s = b.add(a, env);
        assertEquals("(-1/3)+( 2/3) = 1/3", Fraction.create(in(1), in(3), env), s);
        
        a = Fraction.create(in( 1), in(3), env);
        b = Fraction.create(in(-2), in(3), env);
        s = a.add(b, env);
        assertEquals("( 1/3)+(-2/3) = -1/3", Fraction.create(in(-1), in(3), env), s);
        s = b.add(a, env);
        assertEquals("(-2/3)+( 1/3) = -1/3", Fraction.create(in(-1), in(3), env), s);
        
        a = Fraction.create(in(1),   in(2),   env);
        b = Fraction.create(fl(1.0), fl(2.0), env);
        s = a.add(b, env);
        assertEquals("(1/2)+(1.0/2.0) = 1.0", fl(1.0), s);
        s = b.add(a, env);
        assertEquals("(1.0/2.0)+(1/2) = 1.0", fl(1.0), s);
    }
    
    @Test
    public void testSubtract() {
        testing();
        Fraction a, b;
        Number   s;
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        try {
            s = a.subtract(null, env);
            fail(String.valueOf(s));
        } catch (NullPointerException x) {
        }
        try {
            s = a.subtract(b, null);
            fail(String.valueOf(s));
        } catch (NullPointerException x) {
        }
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        s = a.subtract(b, env);
        assertEquals("1-1 = 0", zero(), s);
        s = b.subtract(a, env);
        assertEquals("1-1 = 0", zero(), s);
        
        a = Fraction.create(in(2), one(), env);
        b = Fraction.create(in(1), one(), env);
        s = a.subtract(b, env);
        assertEquals("2-1 =  1", in( 1), s);
        s = b.subtract(a, env);
        assertEquals("1-2 = -1", in(-1), s);
        
        a = Fraction.create(one(),  one(), env);
        b = Fraction.create(zero(), one(), env);
        s = a.subtract(b, env);
        assertEquals("1-0 =  1", in( 1), s);
        s = b.subtract(a, env);
        assertEquals("0-1 = -1", in(-1), s);
        
        a = Fraction.create(in(2), in(3), env);
        b = Fraction.create(in(3), in(2), env);
        s = a.subtract(b, env);
        assertEquals("(2/3)-(3/2) = (4/6)-(9/6) = -5/6", Fraction.create(in(-5), in(6), env), s);
        s = b.subtract(a, env);
        assertEquals("(3/2)-(2/3) = (9/6)-(4/6) =  5/6", Fraction.create(in( 5), in(6), env), s);
        
        a = Fraction.create(in( 1), in(2), env);
        b = Fraction.create(in(-1), in(4), env);
        s = a.subtract(b, env);
        assertEquals("( 1/2)-(-1/4) =  3/4", Fraction.create(in( 3), in(4), env), s);
        s = b.subtract(a, env);
        assertEquals("(-1/4)-( 1/2) = -3/4", Fraction.create(in(-3), in(4), env), s);
        
        a = Fraction.create(in( 1), in(4), env);
        b = Fraction.create(in(-1), in(2), env);
        s = a.subtract(b, env);
        assertEquals("( 1/4)-(-1/2) =  3/4", Fraction.create(in( 3), in(4), env), s);
        s = b.subtract(a, env);
        assertEquals("(-1/2)-( 1/4) = -3/4", Fraction.create(in(-3), in(4), env), s);
        
        a = Fraction.create(in(1),   in(4),   env);
        b = Fraction.create(fl(1.0), fl(8.0), env);
        s = a.subtract(b, env);
        assertEquals("(1/4)-(1.0/8.0) =  1.0/8.0 =  0.125", fl( 0.125), s);
        s = b.subtract(a, env);
        assertEquals("(1.0/8.0)-(1/4) = -1.0/8.0 = -0.125", fl(-0.125), s);
    }
    
    @Test
    public void testMultiply() {
        testing();
        Fraction a, b;
        Number   p;
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        try {
            p = a.multiply(null, env);
            fail(String.valueOf(p));
        } catch (NullPointerException x) {
        }
        try {
            p = a.multiply(b, null);
            fail(String.valueOf(p));
        } catch (NullPointerException x) {
        }
        
        // 1*1
        p = a.multiply(b, env);
        assertEquals("1*1 = 1", one(), p);
        p = b.multiply(a, env);
        assertEquals("1*1 = 1", one(), p);
        
        // x*1
        a = Fraction.create(in(3), in(5), env);
        b = Fraction.create(one(), one(), env);
        p = a.multiply(b, env);
        assertEquals("(3/5)*1 = 3/5", fr(3, 5), p);
        p = b.multiply(a, env);
        assertEquals("1*(3/5) = 3/5", fr(3, 5), p);
        
        // x*-1
        a = Fraction.create(in( 3), in(5), env);
        b = Fraction.create(in(-1), one(), env);
        p = a.multiply(b, env);
        assertEquals("(3/5)*-1 = -3/5", fr(-3, 5), p);
        p = b.multiply(a, env);
        assertEquals("-1*(3/5) = -3/5", fr(-3, 5), p);
        
        // 1*0
        a = Fraction.create(one(),  one(), env);
        b = Fraction.create(zero(), one(), env);
        p = a.multiply(b, env);
        assertEquals("1*0 = 0", zero(), p);
        p = b.multiply(a, env);
        assertEquals("0*1 = 0", zero(), p);
        
        // x*0
        a = Fraction.create(in(1), in(2), env);
        b = Fraction.create(zero(), one(), env);
        p = a.multiply(b, env);
        assertEquals("(1/2)*0 = 0", zero(), p);
        p = b.multiply(a, env);
        assertEquals("0*(1/2) = 0", zero(), p);
        
        // x*(1/x)
        a = Fraction.create(in(1), in(2), env);
        b = Fraction.create(in(2), in(1), env);
        p = a.multiply(b, env);
        assertEquals("(1/2)*(2/1) = 1", one(), p);
        p = b.multiply(a, env);
        assertEquals("(2/1)*(1/2) = 1", one(), p);
        
        // a*b
        a = Fraction.create(in(6), in(7), env);
        b = Fraction.create(in(3), in(2), env);
        p = a.multiply(b, env);
        assertEquals("(6/7)*(3/2) = 18/14 = 9/7", fr(9, 7), p);
        p = b.multiply(a, env);
        assertEquals("(3/2)*(6/7) = 18/14 = 9/7", fr(9, 7), p);
        
        // -a*b
        a = Fraction.create(in(-6), in(7), env);
        b = Fraction.create(in( 3), in(2), env);
        p = a.multiply(b, env);
        assertEquals("(-6/7)*(3/2) = -18/14 = -9/7", fr(-9, 7), p);
        p = b.multiply(a, env);
        assertEquals("(3/2)*(-6/7) = -18/14 = -9/7", fr(-9, 7), p);
        
        // -a*-b
        a = Fraction.create(in(-6), in(7), env);
        b = Fraction.create(in(-3), in(2), env);
        p = a.multiply(b, env);
        assertEquals("(-6/7)*(-3/2) = 18/14 = 9/7", fr(9, 7), p);
        p = b.multiply(a, env);
        assertEquals("(-3/2)*(-6/7) = 18/14 = 9/7", fr(9, 7), p);
        
        // a.x*b.y
        a = Fraction.create(fl(1.5), fl(0.5), env);
        b = Fraction.create(fl(6.5), fl(6.2), env);
        Float expect = (Float) env.multiply(env.divide(a.getNumerator(), a.getDenominator()),
                                            env.divide(b.getNumerator(), b.getDenominator()));
        p = a.multiply(b, env);
        assertEquals(expect, p);
        p = b.multiply(a, env);
        assertEquals(expect, p);
    }
    
    @Test
    public void testDivide() {
        testing();
        Fraction a, b;
        Number   q;
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        try {
            q = a.divide(null, env);
            fail(String.valueOf(q));
        } catch (NullPointerException x) {
        }
        try {
            q = a.divide(b, null);
            fail(String.valueOf(q));
        } catch (NullPointerException x) {
        }
        
        a = Fraction.create(one(),  one(), env);
        b = Fraction.create(zero(), one(), env);
        try {
            q = a.divide(b, env);
            fail("1/0 = " + q);
        } catch (UndefinedException x) {
        }
        a = Fraction.create(one(),   one(), env);
        b = Fraction.create(fl(0.0), one(), env);
        try {
            q = a.divide(b, env);
            fail("1/0.0 = " + q);
        } catch (UndefinedException x) {
        }
        
        // x/1, 1/x
        for (long x = -10; x <= 10; ++x) {
            a = Fraction.create(in(x), one(), env);
            b = Fraction.create(one(), one(), env);
            q = a.divide(b, env);
            assertEquals(x + "/1 = 1", in(x), q);
            if (x != 0) {
                q = b.divide(a, env);
                assertEquals("1/" + x + " = 1/" + x, fr(1, x).collapse(env), q);
            }
        }
        
        // a/b, b/a
        a = Fraction.create(in(2), in(3), env);
        b = Fraction.create(in(3), in(2), env);
        q = a.divide(b, env);
        assertEquals("(2/3)/(3/2) = (2/3)*(2/3) = 4/9", fr(4, 9), q);
        q = b.divide(a, env);
        assertEquals("(3/2)/(2/3) = (3/2)*(3/2) = 9/4", fr(9, 4), q);
        
        a = Fraction.create(in(1), in(2), env);
        b = Fraction.create(in(1), in(4), env);
        q = a.divide(b, env);
        assertEquals("(1/2)/(1/4) = (1/2)*(4/1) = 4/2 = 2", in(2), q);
        q = b.divide(a, env);
        assertEquals("(1/4)/(1/2) = (1/4)*(2/1) = 2/4 = 1/2", fr(1, 2), q);
        
        // -a/b, b/-a
        a = Fraction.create(in(-2), in(3), env);
        b = Fraction.create(in( 3), in(2), env);
        q = a.divide(b, env);
        assertEquals("(-2/3)/( 3/2) = (-2/3)*( 2/3) = -4/9", fr(-4, 9), q);
        q = b.divide(a, env);
        assertEquals("( 3/2)/(-2/3) = ( 3/2)*(-3/2) = -9/4", fr(-9, 4), q);
        
        a = Fraction.create(in(-1), in(2), env);
        b = Fraction.create(in( 1), in(4), env);
        q = a.divide(b, env);
        assertEquals("(-1/2)/( 1/4) = (-1/2)*( 4/1) = -4/2 = -2", in(-2), q);
        q = b.divide(a, env);
        assertEquals("( 1/4)/(-1/2) = ( 1/4)*(-2/1) = -2/4 = -1/2", fr(-1, 2), q);
        
        // -a/-b, -b/-a
        a = Fraction.create(in(-2), in(3), env);
        b = Fraction.create(in(-3), in(2), env);
        q = a.divide(b, env);
        assertEquals("(-2/3)/(-3/2) = (-2/3)*(-2/3) = 4/9", fr(4, 9), q);
        q = b.divide(a, env);
        assertEquals("(-3/2)/(-2/3) = (-3/2)*(-3/2) = 9/4", fr(9, 4), q);
        
        a = Fraction.create(in(-1), in(2), env);
        b = Fraction.create(in(-1), in(4), env);
        q = a.divide(b, env);
        assertEquals("(-1/2)/(-1/4) = (-1/2)*(-4/1) = 4/2 = 2", in(2), q);
        q = b.divide(a, env);
        assertEquals("(-1/4)/(-1/2) = (-1/4)*(-2/1) = 2/4 = 1/2", fr(1, 2), q);
        
        Number e;
        // a.x/b.y
        a = Fraction.create(fl(0.5 ), fl(4.2), env);
        b = Fraction.create(fl(0.25), fl(3.6), env);
        e = env.divide(env.divide(fl(0.5 ), fl(4.2)),
                       env.divide(fl(0.25), fl(3.6)));
        q = a.divide(b, env);
        assertEquals(e, q);
        e = env.divide(env.divide(fl(0.25), fl(3.6)),
                       env.divide(fl(0.5 ), fl(4.2)));
        q = b.divide(a, env);
        assertEquals(e, q);
    }
    
    @Test
    public void testMod() {
        testing();
        Fraction a, b;
        Number   r;
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        try {
            r = a.mod(null, env);
            fail(String.valueOf(r));
        } catch (NullPointerException x) {
        }
        try {
            r = a.mod(b, null);
            fail(String.valueOf(r));
        } catch (NullPointerException x) {
        }
        
        a = Fraction.create(one(),  one(), env);
        b = Fraction.create(zero(), one(), env);
        try {
            r = a.mod(b, env);
            fail("1%0 = " + r);
        } catch (UndefinedException x) {
        }
        a = Fraction.create(one(),   one(), env);
        b = Fraction.create(fl(0.0), one(), env);
        try {
            r = a.mod(b, env);
            fail("1%0.0 = " + r);
        } catch (UndefinedException x) {
        }
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        r = a.mod(b, env);
        assertEquals("1%1 = 0", zero(), r);
        
        a = Fraction.create(zero(), one(), env);
        b = Fraction.create(one(),  one(), env);
        r = a.mod(b, env);
        assertEquals("0%1 = 0", zero(), r);
        
        a = Fraction.create(in(17), in(1), env);
        b = Fraction.create(in( 5), in(1), env);
        r = a.mod(b, env);
        assertEquals("17%5 = 2", in(2), r);
        r = b.mod(a, env);
        assertEquals("5%17 = 5", in(5), r);
        
        a = Fraction.create(in(33), in(10), env); // 3.3
        b = Fraction.create(in( 1), in( 2), env); // 0.5
        r = a.mod(b, env);
        assertEquals("3.3 % 0.5 = 0.3", fr(3, 10), r);
        r = b.mod(a, env);
        assertEquals("0.5 % 3.3 = 0.5", fr(1, 2), r);
        
        a = Fraction.create(in(-62), in(10), env); // -6.2
        b = Fraction.create(in(  1), in( 4), env); //  0.25
        r = a.mod(b, env);
        assertEquals("-6.2 % 0.25 = -0.2", fr(-1, 5), r); // round(-6.2 / 0.25) * 0.25 = -6.0, -6.0 + -0.2 = -6.2
        assertEquals("should behave the same as Java mod", -0.2, -6.2 % 0.25, Math.ulp(6.2));
        
        a = Fraction.create(in(62), in(10), env); //  6.2
        b = Fraction.create(in(-1), in( 4), env); // -0.25
        r = a.mod(b, env);
        assertEquals("6.2 % -0.25 = 0.2", fr(1, 5), r); // round(6.2 / -0.25) * -0.25 = 6.0, 6.0 + 0.2 = 6.2
        assertEquals("should behave the same as Java mod", 0.2, 6.2 % -0.25, Math.ulp(6.2));
        
        a = Fraction.create(in(-62), in(10), env); // -6.2
        b = Fraction.create(in( -1), in( 4), env); // -0.25
        r = a.mod(b, env);
        assertEquals("-6.2 % -0.25 = -0.2", fr(-1, 5), r); // round(-6.2 / -0.25) * -0.25 = -6.0, -6.0 + -0.2 = -6.2
        assertEquals("should behave the same as Java mod", -0.2, -6.2 % -0.25, Math.ulp(6.2));
        
        Float e;
        
        a = Fraction.create(fl(-62.0), fl(10.0), env); // -6.2
        b = Fraction.create(fl(  1.0), fl( 4.0), env); //  0.25
        r = a.mod(b, env);
        e = (Float) env.mod(env.divide(a.getNumerator(), a.getDenominator()),
                            env.divide(b.getNumerator(), b.getDenominator()));
        assertEquals("-6.2 % 0.25 = -0.2", e, r);
        
        a = Fraction.create(fl(62.0), fl(10.0), env); //  6.2
        b = Fraction.create(fl(-1.0), fl( 4.0), env); // -0.25
        r = a.mod(b, env);
        e = (Float) env.mod(env.divide(a.getNumerator(), a.getDenominator()),
                            env.divide(b.getNumerator(), b.getDenominator()));
        assertEquals("6.2 % -0.25 = 0.2", e, r);
        
        a = Fraction.create(fl(-62.0), fl(10.0), env); // -6.2
        b = Fraction.create(fl( -1.0), fl( 4.0), env); // -0.25
        r = a.mod(b, env);
        e = (Float) env.mod(env.divide(a.getNumerator(), a.getDenominator()),
                            env.divide(b.getNumerator(), b.getDenominator()));
        assertEquals("-6.2 % -0.25 = -0.2", e, r);
    }
    
    @Test
    public void testIsTruthy() {
        testing();
        Fraction f = fr(1, 1);
        try {
            f.isTruthy((MathEnv) null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertTrue (Fraction.create(Boolean.TRUE,  Boolean.TRUE, env).isTruthy(env));
        assertFalse(Fraction.create(Boolean.FALSE, Boolean.TRUE, env).isTruthy(env));
        
        assertTrue(Fraction.create(in( 1), in(1), env).isTruthy(env));
        assertTrue(Fraction.create(in( 2), in(1), env).isTruthy(env));
        assertTrue(Fraction.create(in( 1), in(2), env).isTruthy(env));
        assertTrue(Fraction.create(in(-1), in(1), env).isTruthy(env));
        assertTrue(Fraction.create(in(-2), in(1), env).isTruthy(env));
        assertTrue(Fraction.create(in(-1), in(2), env).isTruthy(env));
        
        assertFalse(Fraction.create(in(0  ), in(1  ), env).isTruthy(env));
        assertFalse(Fraction.create(in(0  ), fl(1.0), env).isTruthy(env));
        assertFalse(Fraction.create(fl(0.0), in(1  ), env).isTruthy(env));
        assertFalse(Fraction.create(fl(0.0), fl(1.0), env).isTruthy(env));
    }
    
    @Test
    public void testIsOne() {
        testing();
        
        try {
            fr(1, 1).isOne(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertTrue (fr(1, 1).isOne(env));
        assertFalse(fr(0, 1).isOne(env));
        
        for (long n = -10; n <= 10; ++n) {
            for (long d = -10; d <= 10; ++d) {
                if (d != 0) {
                    Fraction f = Fraction.create(in(n), in(d), env);
                    assertEquals(n == d, f.isOne(env));
                    f = Fraction.createUnchecked(in(n), in(d));
                    assertEquals(n == d, f.isOne(env));
                }
            }
        }
        
        // one should get converted to the other
        assertTrue(Fraction.create(fl(3.0), fl(3.0), env).isOne(env));
        assertTrue(Fraction.create(in(3  ), fl(3.0), env).isOne(env));
        assertTrue(Fraction.create(fl(3.0), in(3  ), env).isOne(env));
        
        // one should get converted to the other
        assertFalse(Fraction.create(fl(3.0), fl(4.0), env).isOne(env));
        assertFalse(Fraction.create(in(3  ), fl(4.0), env).isOne(env));
        assertFalse(Fraction.create(fl(3.0), in(4  ), env).isOne(env));
        
        // one should get converted to the other
        assertFalse(Fraction.create(fl(-3.0), fl(3.0), env).isOne(env));
        assertFalse(Fraction.create(in(-3  ), fl(3.0), env).isOne(env));
        assertFalse(Fraction.create(fl(-3.0), in(3  ), env).isOne(env));
        
        // one should get converted to the other
        assertFalse(Fraction.create(fl(-3.0), fl(4.0), env).isOne(env));
        assertFalse(Fraction.create(in(-3  ), fl(4.0), env).isOne(env));
        assertFalse(Fraction.create(fl(-3.0), in(4  ), env).isOne(env));
    }
    
    @Test
    public void testIsZero() {
        testing();
        
        try {
            fr(0, 1).isZero(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertTrue (fr(0, 1).isZero(env));
        assertFalse(fr(1, 1).isZero(env));
        
        for (long n = -10; n <= 10; ++n) {
            for (long d = -10; d <= 10; ++d) {
                if (d != 0) {
                    Fraction f = Fraction.create(in(n), in(d), env);
                    assertEquals(n == 0, f.isZero(env));
                    f = Fraction.createUnchecked(in(n), in(d));
                    assertEquals(n == 0, f.isZero(env));
                }
            }
        }
        
        // one should get converted to the other
        assertTrue(Fraction.create(fl(0.0), fl(1.0), env).isZero(env));
        assertTrue(Fraction.create(in(0  ), fl(1.0), env).isZero(env));
        assertTrue(Fraction.create(fl(0.0), in(1  ), env).isZero(env));
        
        // one should get converted to the other
        assertFalse(Fraction.create(fl(1.0), fl(2.0), env).isZero(env));
        assertFalse(Fraction.create(in(1  ), fl(2.0), env).isZero(env));
        assertFalse(Fraction.create(fl(1.0), in(2  ), env).isZero(env));
    }
    
    @Test
    public void testIsDefined() {
        testing();
        
        try {
            fr(1, 1).isDefined(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertTrue(Fraction.createUnchecked(one(),  one()).isDefined(env));
        assertTrue(Fraction.createUnchecked(zero(), one()).isDefined(env));
        
        assertFalse(Fraction.createUnchecked(one(),  zero()).isDefined(env));
        assertFalse(Fraction.createUnchecked(zero(), zero()).isDefined(env));
        
        assertFalse(Fraction.createUnchecked(fl(1.0), fl(0.0)).isDefined(env));
        assertFalse(Fraction.createUnchecked(fl(0.0), fl(0.0)).isDefined(env));
    }
    
    @Test
    public void testIsInteger() {
        testing();
        
        try {
            fr(1, 1).isInteger(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertTrue(fr(Boolean.TRUE,  Boolean.TRUE).isInteger(env));
        assertTrue(fr(Boolean.FALSE, Boolean.TRUE).isInteger(env));
        
        assertTrue(fr(-2, 1).isInteger(env));
        assertTrue(fr(-1, 1).isInteger(env));
        assertTrue(fr( 0, 1).isInteger(env));
        assertTrue(fr( 1, 1).isInteger(env));
        assertTrue(fr( 2, 1).isInteger(env));
        
        assertFalse(fr(-1, 2).isInteger(env));
        assertFalse(fr( 1, 2).isInteger(env));
        
        assertTrue(fr(exact(-2.0), exact(1.0)).isInteger(env));
        assertTrue(fr(exact(-1.0), exact(1.0)).isInteger(env));
        assertTrue(fr(exact( 0.0), exact(1.0)).isInteger(env));
        assertTrue(fr(exact( 1.0), exact(1.0)).isInteger(env));
        assertTrue(fr(exact( 2.0), exact(1.0)).isInteger(env));
        
        assertFalse(fr(exact(-1.0), exact(2.0)).isInteger(env));
        assertFalse(fr(exact( 1.0), exact(2.0)).isInteger(env));
        
        assertFalse(fr(approx(1.0), approx(1.0)).isInteger(env));
        assertFalse(fr(exact(0.25), exact(0.50)).isInteger(env));
    }
    
    @Test
    public void testIsApproximate() {
        testing();
        
        assertFalse(fr(0, 1).isApproximate());
        assertFalse(fr(1, 2).isApproximate());
        assertFalse(fr(1, 1).isApproximate());
        assertFalse(fr(2, 1).isApproximate());
        
        assertFalse(fr(exact(0), exact(1)).isApproximate());
        assertFalse(fr(exact(1), exact(2)).isApproximate());
        assertFalse(fr(exact(1), exact(1)).isApproximate());
        assertFalse(fr(exact(2), exact(1)).isApproximate());
        
        assertFalse(fr(in(0), exact(1)).isApproximate());
        assertFalse(fr(exact(1), in(2)).isApproximate());
        assertFalse(fr(in(1), exact(1)).isApproximate());
        assertFalse(fr(exact(2), in(1)).isApproximate());
        
        assertTrue(fr(approx(0), approx(1)).isApproximate());
        assertTrue(fr(approx(1), approx(2)).isApproximate());
        assertTrue(fr(approx(1), approx(1)).isApproximate());
        assertTrue(fr(approx(2), approx(1)).isApproximate());
        
        assertTrue(fr(exact(0), approx(1)).isApproximate());
        assertTrue(fr(approx(1), exact(2)).isApproximate());
        assertTrue(fr(exact(1), approx(1)).isApproximate());
        assertTrue(fr(approx(2), exact(1)).isApproximate());
        
        assertTrue(fr(in(0), approx(1)).isApproximate());
        assertTrue(fr(approx(1), in(2)).isApproximate());
        assertTrue(fr(in(1), approx(1)).isApproximate());
        assertTrue(fr(approx(2), in(1)).isApproximate());
    }
    
    @Test
    public void testEqualsAndHashCode() {
        testing();
        Fraction a, b;
        
        a = Fraction.create(one(), one(), env);
        b = Fraction.create(one(), one(), env);
        
        assertEquals(a, b);
        assertEquals(b, a);
        
        assertEquals(a.hashCode(), b.hashCode());
        
        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
        assertFalse(a.equals(new Object()));
        
        assertEquals(0, env.relate(a, one()));
        assertFalse(((Number) a).equals((Number) one()));
        
        a = Fraction.create(in(1), in(2), env);
        b = Fraction.create(in(2), in(1), env);
        assertNotEquals(a, b);
        assertNotEquals(b, a);
        
        a = Fraction.createUnchecked(in(1),    in(4)   );
        b = Fraction.createUnchecked(exact(1), exact(4)); // Fraction.create would convert these to integer.
        assertNotEquals(a, b);
        assertNotEquals(b, a);
        
        a = Fraction.create(exact(1), exact(4), env);
        b = Fraction.create(exact(1), exact(4), env);
        assertEquals(a, b);
        assertEquals(b, a);
        
        a = Fraction.create(approx(1), approx(4), env);
        b = Fraction.create(approx(1), approx(4), env);
        assertEquals(a, b);
        assertEquals(b, a);
        
        // Note: This will fail if Float.equals is changed to consider
        //       whether this.isApproximate() == that.isApproximate().
        a = Fraction.createUnchecked(approx(1), approx(4));
        b = Fraction.createUnchecked(exact(1),  exact(4) ); // Fraction.create would convert these to integer.
        assertEquals(a, b);
        assertEquals(b, a);
    }
}