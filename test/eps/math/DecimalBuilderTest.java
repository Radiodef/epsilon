/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.app.*;
import eps.math.Decimal.*;
import static eps.test.Tools.*;
import static eps.util.Misc.*;

import java.lang.Integer;

import org.junit.*;
import static org.junit.Assert.*;

public class DecimalBuilderTest {
    public DecimalBuilderTest() {
    }
    
    private FieldSetter logField;
    
    @Before
    public void before() {
        logField = disableLog();
    }
    
    @After
    public void after() {
        if (logField != null) {
            logField.close();
            logField = null;
        }
    }
    
    @Test
    public void testStaticFactories() {
        testing();
        
        Builder b = Decimal.builder();
        
        assertEquals(Decimal.ZERO, b.build());
        assertFalse(b.isNegative());
        assertEquals(0, b.signum());
        
        for (int i = 0; i <= Byte.MAX_VALUE; ++i) {
            b = Decimal.builder(i);
            
            assertEquals(Decimal.ZERO, b.build());
            assertFalse(b.isNegative());
            assertEquals(0, b.signum());
        }
        
        try {
            b = Decimal.builder(-1);
            fail(String.valueOf(b));
        } catch (NegativeArraySizeException x) {
        }
        try {
            b = Decimal.builder(-8);
            fail(String.valueOf(b));
        } catch (NegativeArraySizeException x) {
        }
    }
    
    @Test
    public void testAppend() {
        testing();
        Builder b = Decimal.builder();
        
        for (int digit : in(-1, 10, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            try {
                b.append(digit);
                fail(b.toString());
            } catch (IllegalArgumentException x) {
            }
        }
        
        assertEquals("failure should not mutate", Decimal.ZERO, b.build());
        
        String s = "";
        for (int i = 0; i < 50; ++i) {
            for (int digit = 1; digit <= 9; ++digit) {
                assertEquals(s += digit, b.append(digit).build().toString());
            }
        }
        
        assertEquals("123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789"
                    +"123456789"+"123456789"+"123456789"+"123456789"+"123456789",
                     b.build().toString());
        
        assertEquals(9 * 50, b.width());
        assertEquals(0, b.scale());
        assertEquals(1, b.signum());
        assertFalse(b.isNegative());
    }
    
    @Test
    public void testSignAndApproximate() {
        testing();
        Builder b = Decimal.builder();
        
        assertFalse(b.isNegative());
        assertFalse(b.isApproximate());
        assertFalse(b.build().isNegative());
        assertFalse(b.build().isApproximate());
        assertEquals(Decimal.ZERO, b.build());
        
        b.setNegative(true);
        assertTrue(b.isNegative());
        assertFalse("never -0", b.build().isNegative());
        assertEquals(Decimal.ZERO, b.build());
        
        b.setNegative(false).setApproximate(true);
        assertFalse(b.isNegative());
        assertTrue(b.isApproximate());
        assertFalse(b.build().isNegative());
        assertTrue(b.build().isApproximate());
        assertEquals(Decimal.APPROX_ZERO, b.build());
        
        b.setNegative(true);
        assertTrue(b.isNegative());
        assertFalse("never -0", b.build().isNegative());
        assertEquals(Decimal.APPROX_ZERO, b.build());
        
        b.setApproximate(false);
        assertFalse(b.isApproximate());
        assertFalse(b.build().isApproximate());
        
        b.appendAll(1, 2, 3, 4, 5, 6, 7, 8, 9);
        assertTrue(b.isNegative());
        assertTrue(b.build().isNegative());
        assertEquals("-123456789", b.build().toString());
        
        b.setNegative(false);
        assertFalse(b.isNegative());
        assertFalse(b.build().isNegative());
        assertEquals("123456789", b.build().toString());
        
        b.setApproximate(true);
        assertTrue(b.isApproximate());
        assertTrue(b.build().isApproximate());
        assertEquals("123456789.0", b.build().toString());
        
        b.setNegative(true);
        assertTrue(b.isNegative());
        assertTrue(b.build().isNegative());
        assertEquals("-123456789.0", b.build().toString());
    }
    
    @Test
    public void testSetScale() {
        testing();
        
        Builder db = Decimal.builder();
        
        db.setScale(10);
        assertEquals(10, db.scale());
        assertEquals(Decimal.ZERO, db.build());
        
        db.setScale(-10);
        assertEquals(-10, db.scale());
        assertEquals(Decimal.ZERO, db.build());
        
        db.setScale(Integer.MAX_VALUE);
        assertEquals(Integer.MAX_VALUE, db.scale());
        assertEquals(Decimal.ZERO, db.build());
        
        db.setScale(Integer.MIN_VALUE);
        assertEquals(Integer.MIN_VALUE, db.scale());
        assertEquals(Decimal.ZERO, db.build());
        
        db = Decimal.builder();
        
        db.appendAll(1, 2, 3);
        assertEquals(0, db.scale());
        assertEquals("123", db.build().toString());
        db.makeApproximate();
        assertEquals("123.0", db.build().toString());
        db.makeExact();
        db.setScale(-1);
        assertEquals(-1, db.scale());
        assertEquals("12.3", db.build().toString());
        db.makeApproximate();
        assertEquals("12.3...", db.build().toString());
        db.makeExact();
        db.setScale(-2);
        assertEquals(-2, db.scale());
        assertEquals("1.23", db.build().toString());
        db.makeApproximate();
        assertEquals("1.23...", db.build().toString());
        db.makeExact();
        db.setScale(-3);
        assertEquals(-3, db.scale());
        assertEquals("0.123", db.build().toString());
        db.makeApproximate();
        assertEquals("0.123...", db.build().toString());
        db.makeExact();
        db.setScale(-4);
        assertEquals(-4, db.scale());
        assertEquals("0.0123", db.build().toString());
        db.makeApproximate();
        assertEquals("0.0123...", db.build().toString());
        db.makeExact();
        db.setScale(1);
        assertEquals(1, db.scale());
        assertEquals("1230", db.build().toString());
        db.makeApproximate();
        assertEquals("1230.0", db.build().toString());
        db.makeExact();
        db.setScale(2);
        assertEquals(2, db.scale());
        assertEquals("12300", db.build().toString());
        db.makeApproximate();
        assertEquals("12300.0", db.build().toString());
        db.makeExact();
        db.setScale(0);
        assertEquals(0, db.scale());
        assertEquals("123", db.build().toString());
        db.makeApproximate();
        assertEquals("123.0", db.build().toString());
        
        int count = 99;
        
        db = Decimal.builder(count);
        
        StringBuilder sb = new StringBuilder(count);
        
        for (int i = 0; i < (count / 9); ++i) {
            for (int digit = 1; digit <= 9; ++digit) {
                db.append(digit);
                sb.append(Character.forDigit(digit, 10));
            }
        }
        
        assertEquals(sb.toString(), db.build().toString());
        
        try {
            MiniSettings.setLocal(MiniSettings.of(Setting.MAX_FRACTION_DIGITS, Integer.MAX_VALUE));
            
            // do this a few times to verify that setting the
            // scale doesn't do anything weird to the backing
            // array.
            // (i.e. it should be a reversible operation)
            for (int n = 0; n < 3; ++n) {
                for (int scale = -count; scale <= count; ++scale) {
                    db.setScale(scale);
                    
                    String minus;
                    if ((n & 1) == 0) {
                        minus = "";
                        db.setNegative(false);
                    } else {
                        minus = "-";
                        db.setNegative(true);
                    }
                    
                    boolean failed = true;
                    String expect = "";
                    
                    try {
                        if (scale < 0) {
                            int    split  = count + scale;
                            String before = sb.substring(0, split);
                            String after  = sb.substring(split, count);
                            if (before.isEmpty())
                                before = "0";
                            
                            expect = minus + before + "." + after;
                            assertEquals(expect, db.build().toString());
                            
                            failed = false;
                            
                        } else if (scale > 0) {
                            StringBuilder copy = new StringBuilder(2 * sb.length());
                            copy.append(minus).append(sb);
                            
                            for (int i = 0; i < scale; ++i)
                                copy.append('0');
                            
                            expect = copy.toString();
                            assertEquals(expect, db.build().toString());
                            
                            failed = false;
                            
                        } else {
                            expect = minus + sb;
                            assertEquals(expect, db.build().toString());
                            
                            failed = false;
                        }
                    } finally {
                        if (failed) {
                            System.out.println("    failed at n = " + n + ", scale = " + scale);
                            System.out.println("    expect  = " + expect);
                            System.out.println("    builder = " + db);
                            System.out.println("    result  = " + db.build());
                        }
                    }
                }
            }
        } finally {
            MiniSettings.setLocal(null);
        }
    }
    
    @Test
    public void testSimpleSetWidth() {
        testing();
        Builder b = Decimal.builder();
        
        try {
            b.setWidth(-1);
            fail(b.toString());
        } catch (IllegalArgumentException x) {
        }
        try {
            b.setWidth(Integer.MIN_VALUE);
            fail(b.toString());
        } catch (IllegalArgumentException x) {
        }
        
        assertEquals(Decimal.ZERO, b.build());
        
        b.setWidth(10);
        assertEquals(10, b.width());
        System.out.println("    " + b);
        assertEquals(Decimal.ZERO, b.build());
        
        b.append(1);
        assertEquals(11, b.width());
        System.out.println("    " + b);
        assertEquals(Decimal.ONE, b.build());
        
        b.setWidth(12);
        assertEquals(12, b.width());
        System.out.println("    " + b);
        assertEquals(Decimal.valueOf(10), b.build());
        
        b.setWidth(11);
        assertEquals(11, b.width());
        System.out.println("    " + b);
        assertEquals(Decimal.ONE, b.build());
        
        b.setWidth(10);
        assertEquals(10, b.width());
        System.out.println("    " + b);
        assertEquals(Decimal.ZERO, b.build());
    }
    
    @Test
    public void testSimpleSetWidthAndScale() {
        testing();
        Builder b = Decimal.builder();
        
        b.setWidth(10);
        b.setScale(-5);
        System.out.println("    " + b);
        assertEquals(Decimal.ZERO, b.build());
        
        // always appends to the right, increasing the width
        // and not changing the scale
        
        b.append(1);
        assertEquals(11, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals("0.00001", b.build().toString());
        
        b.append(2);
        assertEquals(12, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals("0.00012", b.build().toString());
        
        b.appendAll(3, 4, 5);
        assertEquals(15, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals("0.12345", b.build().toString());
        
        // increasing the width appends zeroes to the
        // right, otherwise leaves the scale and old
        // digits unchanged
        
        b.setWidth(20);
        assertEquals(20, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals("12345", b.build().toString());
        
        b.append(1);
        assertEquals(21, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals("123450.00001", b.build().toString());
        
        // decreasing the width deletes digits on the
        // right, and leaves the scale unchanged
        
        b.setWidth(11);
        assertEquals(11, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals("0.00001", b.build().toString());
        
        b.setWidth(1);
        assertEquals( 1, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals(Decimal.ZERO, b.build());
        
        b.setWidth(0);
        assertEquals( 0, b.width());
        assertEquals(-5, b.scale());
        System.out.println("    " + b);
        assertEquals(Decimal.ZERO, b.build());
    }
    
    @Test
    public void testResetToZero() {
        testing();
        Builder b = Decimal.builder();
        
        b.appendAll(1, 2, 3, 4, 5);
        b.resetToZero();
        assertEquals(0, b.width());
        assertEquals(Decimal.ZERO, b.build());
        
        b.appendAll(1, 2, 3, 4, 5);
        b.setScale(-2);
        b.makeNegative();
        b.makeApproximate();
        b.resetToZero();
        assertFalse(b.isNegative());
        assertFalse(b.isApproximate());
        assertEquals(0, b.width());
        assertEquals(0, b.scale());
        assertEquals(Decimal.ZERO, b.build());
        
        b.setWidth(2);
        b.resetToZero();
        assertEquals(0, b.width());
        assertEquals(Decimal.ZERO, b.build());
    }
    
    @Test
    public void testDelete() {
        testing();
        Builder b;
        
        b = Decimal.builder();
        
        b.appendAll(1, 2, 3, 4);
        assertEquals(4, b.width());
        assertEquals("1234", b.build().toString());
        
        for (int oob : in(-1, 4, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            try {
                b.delete(oob);
                fail(b.toString());
            } catch (IndexOutOfBoundsException x) {
            }
        }
        
        assertEquals("failure should not mutate", 4, b.width());
        assertEquals("failure should not mutate", "1234", b.build().toString());
        
        b.delete(2);
        assertEquals(3, b.width());
        assertEquals("134", b.build().toString());
        
        b.delete(1);
        assertEquals(2, b.width());
        assertEquals("14", b.build().toString());
        
        b.delete(1);
        assertEquals(1, b.width());
        assertEquals("4", b.build().toString());
        
        b.delete(0);
        assertEquals(0, b.width());
        assertEquals(Decimal.ZERO, b.build());
        
        // width is 0, but delete(0) should work (and have no effect)
        b.delete(0);
        assertEquals(0, b.width());
        assertEquals(Decimal.ZERO, b.build());
        
//        b = Decimal.builder();
        
        StringBuilder expect = new StringBuilder();
        
        int count = 10;
        
        for (int i = 0; i < count; ++i) {
            for (int digit = 1; digit <= 9; ++digit) {
                b.append(digit);
                expect.append(digit);
            }
        }
        
        assertEquals(expect.toString(), b.build().toString());
        
        while (b.width() > 0) {
            b.delete(b.maxColumn());
            expect.deleteCharAt(0);
            if (expect.length() == 0)
                expect.append(0);
            assertEquals(expect.toString(), b.build().toString());
        }
        
        assertEquals(Decimal.ZERO, b.build());
        
        b.appendAll(1, 2, 3, 4, 5, 6, 7, 8, 9);
        assertEquals(9, b.width());
        assertEquals("123456789", b.build().toString());
        
        b.setScale(-5);
        assertEquals(9, b.width());
        assertEquals("1234.56789", b.build().toString());
        
        b.delete(-1);
        assertEquals(8, b.width());
        assertEquals("123.46789", b.build().toString());
        
        b.delete(-5);
        assertEquals(7, b.width());
        assertEquals("12.34678", b.build().toString());
        
        b.delete(1);
        assertEquals(6, b.width());
        assertEquals("2.34678", b.build().toString());
        
        b.delete(0);
        assertEquals(5, b.width());
        assertEquals("0.34678", b.build().toString());
        
        for (int i = -1; i >= -5; --i)
            b.delete(i);
        
        assertEquals(0, b.width());
        assertEquals(Decimal.ZERO, b.build());
        
        b.append(1);
        assertEquals(1, b.width());
        assertEquals( ".00001", b.toString());
        assertEquals("0.00001", b.build().toString());
        
        for (int i = -1; i >= -4; --i) {
            b.delete(i);
            assertEquals(1, b.width());
            assertEquals("0.00001", b.build().toString());
        }
        
        try {
            b.delete(0);
            fail(b.toString());
        } catch (IndexOutOfBoundsException x) {
        }
        
        b.delete(-5);
        assertEquals(0, b.width());
        assertEquals(Decimal.ZERO, b.build());
        
        b.setScale(0);
        b.append(1);
        assertEquals(1, b.width());
        assertEquals(Decimal.ONE, b.build());
        
        b.setScale(2);
        assertEquals("100", b.build().toString());
        
        b.delete(0);
        assertEquals("10", b.build().toString());
        b.delete(0);
        assertEquals(Decimal.ONE, b.build());
        
        b = Decimal.builder();
        
        b.appendAll(0, 0, 1);
        b.setScale(-3);
        assertEquals(3, b.width());
        assertEquals("0.001", b.build().toString());
        
        b.delete(-1);
        assertEquals(2, b.width());
        assertEquals("0.001", b.build().toString());
        
        b.delete(-2);
        assertEquals(1, b.width());
        assertEquals("0.001", b.build().toString());
        
        b.setScale(0);
        assertEquals(Decimal.ONE, b.build());
    }
    
    @Test
    public void testSetDigit() {
        testing();
        Builder b = Decimal.builder();
        
        // strict out of bounds
        for (int oob : in(-1, 1, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            try {
                b.setDigit(oob, 1);
                fail(f("setDigit(%d, 1) == %s", oob, b));
            } catch (IndexOutOfBoundsException x) {
            }
        }
        
        // width == 0, but setting index 0.
        // should be allowed because there
        // is an implied digit at index 0 (0)
        b.setDigit(0, 1);
        assertEquals(1, b.width());
        assertEquals(0, b.scale());
        assertEquals(Decimal.ONE, b.build());
        
        b.setScale(3);
        assertEquals("1000", b.build().toString());
        
        b.setDigit(3, 2);
        assertEquals(1, b.width());
        assertEquals(3, b.scale());
        assertEquals("2000", b.build().toString());
        
        // array out of bounds testing
        b.setDigit(2, 3);
        assertEquals(2, b.width());
        assertEquals(2, b.scale());
        assertEquals("2300", b.build().toString());
        
        b.setDigit(0, 5);
        assertEquals(4, b.width());
        assertEquals(0, b.scale());
        assertEquals("2305", b.build().toString());
        
        b.setDigit(1, 4);
        assertEquals(4, b.width());
        assertEquals(0, b.scale());
        assertEquals("2345", b.build().toString());
        
        for (int oob : in(4, -1, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            try {
                b.setDigit(oob, 1);
                fail(f("setDigit(%d, 1) == %s", oob, b));
            } catch (IndexOutOfBoundsException x) {
            }
        }
        
        assertEquals("failure should not mutate", 4, b.width());
        assertEquals("failure should not mutate", 0, b.scale());
        assertEquals("failure should not mutate", "2345", b.build().toString());
        
        // array out of bounds testing when width == 0
        b = Decimal.builder();
        b.setWidth(3);
        assertEquals(Decimal.ZERO, b.build());
        
        b.setDigit(2, 1);
        assertEquals(3, b.width());
        assertEquals(0, b.scale());
        assertEquals("100", b.build().toString());
        
        // negative scale testing
        b = Decimal.builder();
        
        b.setDigit(0, 1);
        b.setScale(-4);
        assertEquals("0.0001", b.build().toString());
        
        b.setDigit(-4, 2);
        assertEquals( 1, b.width());
        assertEquals(-4, b.scale());
        assertEquals("0.0002", b.build().toString());
        
        // negative scale array oob testing
        b.setDigit(-3, 3);
        assertEquals( 2, b.width());
        assertEquals(-4, b.scale());
        assertEquals("0.0032", b.build().toString());
        
        b.setDigit(-1, 5);
        assertEquals( 4, b.width());
        assertEquals(-4, b.scale());
        assertEquals("0.5032", b.build().toString());
        
        b.setDigit(-2, 4);
        assertEquals( 4, b.width());
        assertEquals(-4, b.scale());
        assertEquals("0.5432", b.build().toString());
        
        for (int oob : in(0, -5, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            try {
                b.setDigit(oob, 1);
                fail(f("setDigit(%d, 1) == %s", oob, b));
            } catch (IndexOutOfBoundsException x) {
            }
        }
        
        assertEquals("failure should not mutate", 4, b.width());
        assertEquals("failure should not mutate",-4, b.scale());
        assertEquals("failure should not mutate","0.5432", b.build().toString());
    }
    
    @Test
    public void testSignum() {
        testing();
        for (int sign : in(1, -1)) {
            Builder b = Decimal.builder();
            b.setNegative(sign < 0);
            
            String m = (sign < 0) ? "-" : "";
            
            assertEquals(0, b.signum());
            
            b.setWidth(10);
            assertEquals(0, b.signum());
            assertEquals("0", b.build().toString());
            
            b.append(1);
            assertEquals(sign, b.signum());
            assertEquals(m + "1", b.build().toString());
            
            b.delete(0);
            assertEquals(0, b.signum());
            assertEquals("0", b.build().toString());
            
            b.appendAll(1, 0, 0);
            assertEquals(sign, b.signum());
            assertEquals(m + "100", b.build().toString());
            
            b.setScale(-5);
            assertEquals(sign, b.signum());
            assertEquals(m + "0.001", b.build().toString());
            
            b.setWidth(0);
            assertEquals(0, b.signum());
            assertEquals("0", b.build().toString());
        }
    }
    
    @Test
    public void testReverse() {
        testing();
        Builder b = Decimal.builder();
        
        b.appendAll(1, 2, 3, 4, 5, 6, 7, 8, 9);
        b.reverse();
        assertEquals("987654321", b.build().toString());
        b.reverse();
        assertEquals("123456789", b.build().toString());
        
        b.resetToZero();
        
        b.appendAll(0, 0, 1);
        assertEquals("1", b.build().toString());
        b.reverse();
        assertEquals("100", b.build().toString());
        b.reverse();
        assertEquals("1", b.build().toString());
        
        b.resetToZero();
        
        b.appendAll(0, 0, 1, 0, 0);
        assertEquals("100", b.build().toString());
        b.reverse();
        assertEquals("100", b.build().toString());
        b.reverse();
        assertEquals("100", b.build().toString());
        
        b.resetToZero();
        
        b.appendAll(0, 0, 1);
        b.setScale(-3);
        assertEquals("0.001", b.build().toString());
        b.reverse();
        assertEquals("0.1", b.build().toString());
        b.reverse();
        assertEquals("0.001", b.build().toString());
        
        b.resetToZero();
        
        b.appendAll(0, 0, 0, 1);
        b.setScale(-2);
        assertEquals("0.01", b.build().toString());
        b.reverse();
        assertEquals("10", b.build().toString());
        b.reverse();
        assertEquals("0.01", b.build().toString());
    }
    
    @Test
    public void testCopy() {
        testing();
        Builder src = Decimal.builder();
        Builder dst = Decimal.builder();
        
        try {
            dst.copy(null);
            fail(dst.toString());
        } catch (NullPointerException x) {
            assertEquals("failure should not mutate", Decimal.ZERO, dst.build());
        }
        
        src.appendAll(1, 2, 3, 4);
        dst.copy(src);
        
        assertEquals("1234", dst.build().toString());
        
        src.appendAll(5, 6, 7, 8, 9).makeNegative();
        dst.copy(src);
        
        assertEquals("-123456789", dst.build().toString());
        
        src.resetToZero();
        dst.resetToZero();
        
        src.appendAll(1, 0, 1).setScale(-2).makeApproximate();
        dst.copy(src);
        
        assertEquals("1.01...", dst.build().toString());
    }
}