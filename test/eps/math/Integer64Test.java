/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.test.*;
import static eps.test.Tools.*;

import java.util.stream.*;

import org.junit.*;
import static org.junit.Assert.*;

public class Integer64Test {
    private final MathEnv64 env = MathEnvPrivate.mathEnv64Instance();
    
    public Integer64Test() {
    }
    
    private FieldSetter logField;
    
    @Before
    public void before() {
        logField = disableLog();
    }
    
    @After
    public void after() {
        if (logField != null) {
            logField.close();
            logField = null;
        }
    }
    
    private int[] getPrimes(int count) {
        return PrimeNumbers.FIRST_10_000_PRIMES.stream().mapToInt(i -> i).limit(count).toArray();
    }
    
    private Tuple tupleOf(long... longs) {
        return Tuple.of(env, longs);
    }
    
    @Test
    public void testSimpleFactoring() {
        testing();
        
        try {
            Tuple t = env.zero().factor(null);
            fail(String.valueOf(t));
        } catch (NullPointerException x) {
        }
        
        assertEquals(Tuple.empty(), env.zero().factor(env));
        assertEquals(tupleOf( 1), env.one().factor(env));
        assertEquals(tupleOf(-1), env.negativeOne().factor(env));
        
        assertEquals(tupleOf( 2, 3,  5), env.valueOf( 30).factor(env));
        assertEquals(tupleOf( 3, 3,  3), env.valueOf( 27).factor(env));
        assertEquals(tupleOf( 2, 7, 11), env.valueOf(154).factor(env));
        assertEquals(tupleOf( 2, 2, 13), env.valueOf( 52).factor(env));
        assertEquals(tupleOf(-1, 3, 17), env.valueOf(-51).factor(env));
        
        Tuple factors = Stream.generate(() -> env.valueOf(2)).limit(24).collect(Tuple.collector());
        assertEquals(factors, env.valueOf(16_777_216).factor(env));
    }
    
    @Test
    public void testSimplePrimeFactoring() {
        testing();
        
        for (int p : getPrimes(1_000)) {
            Integer n = env.valueOf(p);
            
            Tuple factors = n.factor(env);
            
            assertEquals(1, factors.count());
            assertEquals(n, factors.get(0));
        }
    }
    
    @Test
    public void testSimpleNegativePrimeFactoring() {
        testing();
        
        for (int p : getPrimes(1_000)) {
            Tuple factors = env.valueOf(-p).factor(env);
            
            assertEquals(2, factors.count());
            assertEquals(env.negativeOne(), factors.get(0));
            assertEquals(env.valueOf(p), factors.get(1));
        }
    }
}