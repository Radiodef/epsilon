/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.app.*;
import eps.test.*;
import static eps.test.Tools.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class Primes64Test {
    private final MathEnv64 env = MathEnvPrivate.mathEnv64Instance();
    
    public Primes64Test() {
    }
    
    private Primes64 newPrimes64() {
        return new Primes64(env);
    }
    
    private FieldSetter logField;
    
    @Before
    public void before() {
        logField = disableLog();
    }
    
    @After
    public void after() {
        if (logField != null) {
            logField.close();
            logField = null;
        }
    }
    
    @Test
    public void testConstructor() {
        testing();
        
        Primes64 primes = newPrimes64();
        
        try {
            primes = new Primes64(null);
            fail(primes.toString());
        } catch (NullPointerException x) {
        }
    }
    
    private final int[] cacheSizes = { 0, 1, 2, 3, 7, 10, 31, 64, 1_024, 65_536, 104_729, 16_777_216, };
    
    private void testWithCacheSizes(Runnable test) {
        assertFalse(Setting.PRIME_CACHE_SIZE.isValue(-1));
        
        try {
            for (int cacheSize : cacheSizes) {
                MiniSettings.setLocal(MiniSettings.of(Setting.PRIME_CACHE_SIZE, cacheSize));
                System.out.print("    testing cache size = ");
                System.out.println(cacheSize);
                
                test.run();
            }
        } finally {
            MiniSettings.setLocal(null);
        }
    }
    
    @Test
    public void testGetNextAfter() {
        testing();
        testWithCacheSizes(this::testGetNextAfterImpl);
    }
    
    private void testGetNextAfterImpl() {
        Primes64 primes;
        
        primes = newPrimes64();
        
        Integer n = env.zero();
        for (int p : PrimeNumbers.FIRST_10_000_PRIMES) {
            n = primes.getNextAfter(n);
            assertEquals(env.valueOf(p), n);
        }
        
        primes = newPrimes64();
        assertEquals(env.valueOf(31), primes.getNextAfter(30));
        assertEquals(env.valueOf(37), primes.getNextAfter(31));
        assertEquals(env.valueOf(5), primes.getNextAfter(3));
        
        primes = newPrimes64();
        assertEquals(env.valueOf(101), primes.getNextAfter(100));
        assertEquals(env.valueOf(103), primes.getNextAfter(101));
        assertEquals(env.valueOf(5), primes.getNextAfter(3));
        
        primes = newPrimes64();
        assertEquals(env.valueOf(104723), primes.getNextAfter(104717));
        assertEquals(env.valueOf(104729), primes.getNextAfter(104723));
        assertEquals(env.valueOf(5), primes.getNextAfter(3));
        
        primes = newPrimes64();
        assertEquals(env.valueOf(2), primes.getNextAfter(Math2.LONG_MIN));
        assertEquals(env.valueOf(2), primes.getNextAfter(Math2.INT_MIN));
        assertEquals(env.valueOf(5), primes.getNextAfter(3));
    }
    
    @Test
    public void testIterator() {
        testing();
        
        Iterator<Integer> it = newPrimes64().iterator();
        
        for (int prime : PrimeNumbers.FIRST_10_000_PRIMES) {
            assertTrue(it.hasNext());
            
            Integer next = it.next();
            assertEquals(prime, ((Integer64) next).longValue());
            
            try {
                it.remove();
                fail("at " + next);
            } catch (UnsupportedOperationException x) {
            }
        }
    }
    
    @Test
    public void testStream() {
        testing();
        
        Iterator<java.lang.Integer> it = PrimeNumbers.FIRST_10_000_PRIMES.iterator();
        
        newPrimes64().stream()
                     .limit(PrimeNumbers.FIRST_10_000_PRIMES.size())
                     .forEach(p -> {
            assertEquals((int) it.next(), ((Integer64) p).longValue());
        });
    }
}