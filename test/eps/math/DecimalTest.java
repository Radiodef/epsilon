/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.app.*;
import eps.test.*;
import eps.util.*;
import eps.math.Decimal.*;
import static eps.test.Tools.*;

import java.util.*;
import java.math.*;
import java.lang.Integer;

import org.junit.*;
import static org.junit.Assert.*;

public class DecimalTest {
    public DecimalTest() {
    }
    
    private static FieldSetter logSetter;
    
    @BeforeClass
    public static void disableLog() {
        if (logSetter != null) {
            enableLog();
        }
        logSetter = Tools.disableLog();
    }
    
    @AfterClass
    public static void enableLog() {
        if (logSetter != null) {
            logSetter.close();
            logSetter = null;
        }
    }
    
    private String toString(long n) {
        return String.valueOf(n);
    }
    
    private String decimalOf(long n, long d) {
        return Decimal.valueOf(n, d).toString();
    }
    
    private String decimalOf(long n) {
        return Decimal.valueOf(n).toString();
    }
    
    private String decimalOf(double d) {
        return Decimal.valueOf(d).toString();
    }
    
    private String decimalOf(BigInteger n, BigInteger d) {
        return Decimal.valueOf(n, d).toString();
    }
    
    @Test
    public void testIntegerValueOfs() {
        testing();
        
        for (int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; ++i) {
            Decimal d = Decimal.valueOf(i);
            
            assertEquals(Math2.signum(i), d.signum());
            assertEquals(i < 0, d.isNegative());
            assertEquals(i > 0, d.isPositive());
            assertEquals(i == 0, d.isZero());
            
            assertTrue(d.isInteger());
            assertFalse(d.isApproximate());
            
            assertEquals(toString(i), d.toString());
        }
        
        for (long longValue : in(Long.MIN_VALUE, Long.MAX_VALUE)) {
            assertEquals(toString(longValue), decimalOf(longValue));
        }
        
        for (int i = -8; i <= 8; ++i) {
            assertEquals(i + ".0", Decimal.valueOf(i).setApproximate(true).toString());
        }
        
        BigInteger prime = BigInteger.valueOf(127);
        
        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; ++i) {
            BigInteger big =
                BigInteger.valueOf(i < 0 ? Long.MIN_VALUE : Long.MAX_VALUE)
                          .multiply(prime)
                          .add(BigInteger.valueOf(i))
                          .multiply(prime);
            Decimal d = Decimal.valueOf(big);
            
            assertEquals(big.signum(), d.signum());
            assertEquals(big.signum() < 0, d.isNegative());
            assertEquals(big.signum() > 0, d.isPositive());
            assertEquals(big.signum() == 0, d.isZero());
            
            assertTrue(d.isInteger());
            assertFalse(d.isApproximate());
            
            assertEquals(big.toString(), d.toString());
        }
        
        try {
            Decimal d = Decimal.valueOf((BigInteger) null);
            fail(String.valueOf(d));
        } catch (NullPointerException x) {
        }
    }
    
    private String toString(String s, boolean isInteger) {
        int dot = s.indexOf(".");
//        if (dot >= 0) {
//            for (;;) {
//                int last = s.length() - 1;
//                if (last <= dot + 1 || s.charAt(last) != '0')
//                    break;
//                s = s.substring(0, last);
//            }
//        }
        if (isInteger) {
            if (dot < 0) {
                s += ".0";
            }
            return s;
        }
        return s + "...";
    }
    
    private boolean isInteger(double d) {
        return Math.rint(d) == d;
    }
    
    private String toString(double d) {
        return toString(BigDecimal.valueOf(d).toPlainString(), isInteger(d));
    }
    
    private boolean isInteger(BigDecimal d) {
        if (d.scale() <= 0)
            return true;
        try {
            d.toBigIntegerExact();
            return true;
        } catch (ArithmeticException x) {
            return false;
        }
    }
    
    private String toString(BigDecimal d) {
        d = d.stripTrailingZeros();
        return toString(d.toPlainString(), isInteger(d));
    }
    
    @Test
    public void testFloatingValueOfs() {
        testing();
        
        for (double d : in(0.0,
                           -0.0,
                           1.0,
                           10.0,
                           256.0,
                           65536.0,
                           16777216.0,
                           2147483647.0,
                           0.625,
                           8.5,
                           Math.PI,
                           Math.E,
                           Math2.TAU,
                           Math2.SQRT_2)) {
            for (int sign : in(1, -1)) {
                double val = sign * d;
                
                Decimal dec = Decimal.valueOf(val);
                
                assertEquals(Math2.signum(val), dec.signum());
                assertEquals(val < 0, dec.isNegative());
                assertEquals(val > 0, dec.isPositive());
                assertEquals(val == 0, dec.isZero());
                
                assertEquals(isInteger(val), dec.isInteger());
                assertTrue(dec.isApproximate());
                
                assertEquals(toString(val), dec.toString());
            }
        }
        
        assertEquals(Decimal.valueOf(0.0), Decimal.valueOf(-0.0));
        
        for (double d : in(Math.PI,
                           Math.E,
                           Math2.TAU,
                           Math2.SQRT_2)) {
            for (int sign : in(1, -1)) {
                double val = sign * d;
                assertEquals(Double.toString(val), Decimal.valueOf(val).setApproximate(false).toString());
            }
        }
        
        Random rand = new Random();
        
        for (int i = 0; i < Short.MAX_VALUE; ++i) {
            double d = rand.nextDouble() * 65_536 - 32_768;
            assertEquals(toString(d), decimalOf(d));
        }
        
        BigDecimal prime = BigDecimal.valueOf(127);
        BigDecimal pi    = BigDecimal.valueOf(Math.PI);
        
        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; ++i) {
            for (double base : in(Long.MAX_VALUE, Double.MAX_VALUE)) {
                BigDecimal big =
                    // start with a really big number, then scramble it
                    // a bit to add pseudo-random fractional values
                    BigDecimal.valueOf(Math.signum(i) * base)
                              .add(BigDecimal.valueOf((double) i / (double) Byte.MAX_VALUE * (double) Long.MAX_VALUE))
                              .multiply(prime)
                              .add(BigDecimal.valueOf((i * 100.0) + (i / 100.0)))
                              .multiply(pi)
                              .multiply(prime);
                Decimal d = Decimal.valueOf(big);
                
                assertEquals(big.signum(), d.signum());
                assertEquals(big.signum() < 0, d.isNegative());
                assertEquals(big.signum() > 0, d.isPositive());
                assertEquals(big.signum() == 0, d.isZero());
                
                assertEquals(isInteger(big), d.isInteger());
                assertTrue(d.isApproximate());
                
//                System.out.println("    " + big.stripTrailingZeros().toPlainString());
                assertEquals(toString(big), d.toString());
            }
        }
    }
    
    @Test
    public void testSimpleDivision() {
        testing();
        
        assertEquals("0", decimalOf(0,  1));
        assertEquals("0", decimalOf(0, -1));
        
        try {
            Decimal d = Decimal.valueOf(1, 0);
            fail("1/0 = " + d);
        } catch (UndefinedException x) {
        }
        try {
            Decimal d = Decimal.valueOf(0, 0);
            fail("0/0 = " + d);
        } catch (UndefinedException x) {
        }
        
        for (int[] signs : new int[][] {
            {  1,  1, },
            { -1,  1, },
            {  1, -1, },
            { -1, -1, },
        }) {
            int   ns = signs[0];
            int   ds = signs[1];
            String m = ((ns * ds) < 0) ? "-" : "";
            
            assertEquals(m + "0.5",   decimalOf(ns * 1, ds * 2));
            assertEquals(m + "0.25",  decimalOf(ns * 1, ds * 4));
            assertEquals(m + "0.2",   decimalOf(ns * 1, ds * 5));
            assertEquals(m + "0.125", decimalOf(ns * 1, ds * 8));
            
            assertEquals(m + "1", decimalOf(ns * 7,  ds * 7));
            assertEquals(m + "2", decimalOf(ns * 2,  ds * 1));
            assertEquals(m + "3", decimalOf(ns * 6,  ds * 2));
            assertEquals(m + "4", decimalOf(ns * 16, ds * 4));
            assertEquals(m + "5", decimalOf(ns * 15, ds * 3));
            assertEquals(m + "6", decimalOf(ns * 36, ds * 6));
            assertEquals(m + "7", decimalOf(ns * 14, ds * 2));
            assertEquals(m + "8", decimalOf(ns * 32, ds * 4));
            assertEquals(m + "9", decimalOf(ns * 18, ds * 2));
            
            assertEquals(m + "9.25",    decimalOf(ns * 37, ds * 4));
            assertEquals(m + "6.25",    decimalOf(ns * 50, ds * 8));
            assertEquals(m + "3.375",   decimalOf(ns * 27, ds * 8));
            assertEquals(m + "5.1875",  decimalOf(ns * 83, ds * 16));
            assertEquals(m + "3.09375", decimalOf(ns * 99, ds * 32));
            
            assertEquals(m + "281.53125", decimalOf(ns * 9009,  ds * 32));
            assertEquals(m + "3003.7",    decimalOf(ns * 60074, ds * 20));
            assertEquals(m + "52.09375",  decimalOf(ns * 3334,  ds * 64));
        }
        
        for (BigInteger[] args : new BigInteger[][] {
            { BigInteger.ONE, null           },
            { null,           BigInteger.ONE },
            { null,           null           },
        }) {
            try {
                Decimal d = Decimal.valueOf(args[0], args[1]);
                fail(Arrays.toString(args) + " = " + d);
            } catch (NullPointerException x) {
            }
        }
        
        try {
            Decimal d = Decimal.valueOf(BigInteger.ONE, BigInteger.ZERO);
            fail("1/0 = " + d);
        } catch (UndefinedException x) {
        }
        try {
            Decimal d = Decimal.valueOf(BigInteger.ZERO, BigInteger.ZERO);
            fail("0/0 = " + d);
        } catch (UndefinedException x) {
        }
        
        assertEquals( "0", decimalOf(BigInteger.ZERO, BigInteger.ONE));
        assertEquals( "0", decimalOf(BigInteger.ZERO, BigInteger.ONE.negate()));
        assertEquals( "1", decimalOf(BigInteger.ONE,  BigInteger.ONE));
        assertEquals("-1", decimalOf(BigInteger.ONE,  BigInteger.ONE.negate()));
        assertEquals("-1", decimalOf(BigInteger.ONE.negate(), BigInteger.ONE));
        assertEquals( "1", decimalOf(BigInteger.ONE.negate(), BigInteger.ONE.negate()));
        
        for (String[] values : new String[][] {
            { "1" +"000"+"000"+"000"+"000"+"000"+"000"+"001",
              "1" +"000"+"000"+"000"+"000"+"000"+"000"+"000",
              "1."+"000"+"000"+"000"+"000"+"000"+"000"+"001", },
            { "1" +"234"+"456"+"789"+"123"+"456"+"789"+"123",
              "1" +"000"+"000"+"000"+"000"+"000"+"000"+"000",
              "1."+"234"+"456"+"789"+"123"+"456"+"789"+"123", },
        }) {
            for (int[] signs : new int[][] {
                {  1,  1, },
                { -1,  1, },
                {  1, -1, },
                { -1, -1, },
            }) {
                BigInteger n = new BigInteger(values[0]);
                BigInteger d = new BigInteger(values[1]);
                
                String expect = values[2];
                
                if (signs[0] < 0)
                    n = n.negate();
                if (signs[1] < 0)
                    d = d.negate();
                if (signs[0] * signs[1] < 0)
                    expect = "-" + expect;
                
                assertEquals(expect, decimalOf(n, d));
            }
        }
    }
    
    // construct a String like e.g. 0.00001 or -12.00002
    private String createDecimalString(boolean negative, String integer, int zeroCount, String end) {
        StringBuilder b =
            new StringBuilder((negative ? 1 : 0) + integer.length() + 1 + zeroCount + end.length());
        
        if (negative) {
            b.append('-');
        }
        
        b.append(integer).append('.');
        
        while (zeroCount > 0) {
            b.append('0');
            --zeroCount;
        }
        
        b.append(end);
        
        return b.toString();
    }
    
    @Test
    public void testPowersOf10Division() {
        testing();
        // 0.1
        // 1.1
        // 0.01
        // 1.01
        // 0.001
        // 1.001
        // 0.....
        // 1.....
        for (long i = 10L; i > 0L; i *= 10L) {
            int zeroCount = Math2.floorLog10(i - 1);
            String expect;
            Decimal actual;
            
            for (long sign : in(-1L, 1L)) {
                // e.g. ±0.01
                expect = createDecimalString(sign < 0, "0", zeroCount, "1");
                actual = Decimal.valueOf(sign * 1, i);
                
                assertEquals(  zeroCount + 1,  actual.width());
                assertEquals(-(zeroCount + 1), actual.scale());
                
                assertEquals(expect, actual.toString());
                
                if (i <= 1000) {
                    System.out.print("    ");
                    System.out.println(expect);
                }
                
                // e.g. ±1.01
                expect = createDecimalString(sign < 0, "1", zeroCount, "1");
                actual = Decimal.valueOf(sign * (i + 1), i);
                
                assertEquals(  zeroCount + 2,  actual.width());
                assertEquals(-(zeroCount + 1), actual.scale());
                
                assertEquals(expect, actual.toString());
                
                if (i <= 1000) {
                    System.out.print("    ");
                    System.out.println(expect);
                }
            }
        }
    }
    
    // construct string like 123.456456456456...
    private String createRepeatingString(String integer, int repeats, String rep) {
        StringBuilder b =
            new StringBuilder(integer.length() + 1 + repeats + 3);
        
        b.append(integer).append('.');
        
        for (int i = 0; i < repeats; ++i) {
            b.append(rep.charAt(i % rep.length()));
        }
        
        for (;;) {
            int last = b.length() - 1;
            if (b.charAt(last) != '0')
                break;
            b.setLength(last);
        }
        
        b.append("...");
        
        return b.toString();
    }
    
    // construct string like 127012701270
    private String createRepeatingString(int repeats, String rep) {
        char[] chars = new char[repeats];
        
        for (int i = 0; i < repeats; ++i) {
            chars[i] = rep.charAt(i % rep.length());
        }
        
        return newString(chars);
    }
    
//    private final List<Integer> maxDigitsTested = new ArrayList<>();
    
    @Test
    public void testRepeating() {
        testing();
//        maxDigitsTested.clear();
        
        testRepeatingBattery(true); // default max digits
        
        try {
            List<Integer> values = new ArrayList<>(Primes.FIRST_100_PRIMES);
            values.add(12); // test some numbers which have more factors,
            values.add(24); // in case it matters for some reason
            values.add(36);
            values.add(60);
            values.add(1200);
            
            for (Integer value : values) {
                MiniSettings.setLocal(new MiniSettings() {
                    @Override
                    public <T> T getSetting(Setting<T> key) {
                        return (key == Setting.MAX_FRACTION_DIGITS)
                            ? key.cast(value)
                            : key.getDefaultValue();
                    }
                });
                
                testRepeatingBattery(false);
            }
        } finally {
            MiniSettings.setLocal(null);
        }
        
//        System.out.printf("    max digits tested = %s%n", maxDigitsTested);
    }
    
    private void testRepeatingBattery(boolean print) {
        int maxDigits = Settings.getMaxFractionDigits();
//        maxDigitsTested.add(maxDigits);
        
        // 1/3 = 0.3333...
        // 2/3 = 0.6666...
        // 4/3 = 1.3333...
        // 5/3 = 1.6666...
        // 7/3 = 2.3333...
        // 8/3 = 2.6666...
        for (int i = 1; i < 300; ++i) {
            if (i % 3 == 0) continue;
            String integer  = String.valueOf(i / 3);
            String fraction = (i % 3) == 1 ? "3" : "6";
            String expect   = createRepeatingString(integer, maxDigits, fraction);
            
            assertEquals(expect, decimalOf(i, 3));
            
            if (print && i <= 8) {
                System.out.print("    ");
                System.out.println(expect);
            }
        }
        
        //     1/11 =   0.090909...
        //    10/11 =   0.909090...
        //   100/11 =   9.090909...
        //  1000/11 =  90.909090...
        // 10000/11 = 909.090909...
        for (long i = 1L; i > 0L; i *= 10L) {
            int    log10    = Math2.floorLog10(i);
            String integer  = log10 < 2 ? "0" : createRepeatingString(log10 - 1, "90");
            String fraction = (log10 & 1) == 0 ? "09" : "90";
            String expect   = createRepeatingString(integer, maxDigits, fraction);
            
            assertEquals(expect, decimalOf(i, 11));
            
            if (print && i <= 100000) {
                System.out.print("    ");
                System.out.println(expect);
            }
        }
        
        String[] fraction7 = {
            null, // unused: not repeating
            "142857",
            "285714",
            "428571",
            "571428",
            "714285",
            "857142",
        };
        // 1/7 = 0.142857142857...
        // 2/7 = 0.285714285714...
        // 3/7 = 0.428571428571...
        // 4/7 = 0.571428571428...
        // 5/7 = 0.714285714285...
        // 6/7 = 0.857142857142...
        for (int i = 1; i < 70; ++i) {
            if (i % 7 == 0) continue;
            
            String integer  = String.valueOf(i / 7);
            String fraction = fraction7[i % 7];
            String expect   = createRepeatingString(integer, maxDigits, fraction);
            
            assertEquals(expect, decimalOf(i, 7));
            
            if (print && i < 7) {
                System.out.print("    ");
                System.out.println(expect);
            }
        }
    }
    
    @Test
    public void testDecimalMark() {
        testing();
        MiniSettings.setLocal(MiniSettings.of(Setting.DECIMAL_MARK, CodePoint.valueOf(',')));
        try {
            assertEquals("0,0",  decimalOf(0.0));
            
            assertEquals("0,5",  decimalOf(1,   2));
            assertEquals("0,25", decimalOf(1,   4));
            assertEquals("3,14", decimalOf(314, 100));
            assertEquals("1,52", decimalOf(152, 100));
            
            for (double d : in(Math.PI, Math.E, Math2.TAU, Math2.SQRT_2)) {
                String expect = Double.toString(d).replace('.', ',') + "...";
                assertEquals(expect, decimalOf(d));
            }
        } finally {
            MiniSettings.setLocal(null);
        }
    }
    
    @Test
    public void testMaxDigitsCornerCases() {
        testing();
        assertFalse(Setting.MAX_FRACTION_DIGITS.isValue(-1));
        try {
            for (int m : in(0, 1)) {
                MiniSettings.setLocal(MiniSettings.of(Setting.MAX_FRACTION_DIGITS, m));
                
                assertEquals("1.0", decimalOf(1.0));
                
                // would it be better to have something like 0...
                // or just omit the fraction part altogether?
                assertEquals(m == 0 ? "0.0" : "0.5",    decimalOf(1, 2));
                assertEquals(m == 0 ? "1.0" : "1.2...", decimalOf(5, 4));
                assertEquals(m == 0 ? "2.0" : "2.3...", decimalOf(7, 3));
            }
        } finally {
            MiniSettings.setLocal(null);
        }
    }
    
    @Test
    public void testWidthAndScale() {
        testing();
        Decimal d;
        
        d = Decimal.valueOf(0); // exact
        assertFalse(d.isApproximate());
        assertEquals(0, d.width());
        assertEquals(0, d.scale());
        
        d = Decimal.valueOf(0.0); // approx.
        assertTrue(d.isApproximate());
        assertEquals(0, d.width());
        assertEquals(0, d.scale());
        
        d = Decimal.valueOf(1, 4);
        assertEquals( 2, d.width());
        assertEquals(-2, d.scale());
        
        for (int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; ++i) {
            int width = (i == 0) ? 0 : 1 + Math2.floorLog10(Math.abs(i));
            int scale = 0;
            
            d = Decimal.valueOf(i);
            
            assertEquals(width, d.width());
            assertEquals(scale, d.scale());
            
            for (long div = 1; div <= 1_000_000_000; div *= 10) {
                d = Decimal.valueOf(i, div);
                
                int w;
                int s;
                if (i == 0) {
                    w = 0; // always
                    s = 0; // always
                } else {
                    // e.g. 1/100 => 0.01 (scale == -2, width == 2)
                    w = Math.max(width, -scale);
                    s = scale;
                    // e.g. 3000/100 => 30.00 (scale = 0, width = 2)
                    int trailing0s = Math.min(Math2.trailingZeroCount(i),
                                              Math2.trailingZeroCount(div));
                    w -= trailing0s;
                    s += trailing0s;
                }
                
                boolean failed = true;
                try {
                    assertEquals(w, d.width());
                    assertEquals(s, d.scale());
                    failed = false;
                } finally {
                    if (failed) {
                        System.out.println("    failed at " + d.toDebugString());
                        System.out.println("    i = " + i + ", div = " + div);
                    }
                }
                
                --scale;
            }
        }
    }
    
    @Test
    public void testGetDigit() {
        testing();
        Decimal d;
        
        d = Decimal.valueOf(601);
        assertEquals("601", d.toString());
        
        assertEquals(0, d.scale());
        assertEquals(3, d.width());
        assertEquals(2, d.maxColumn());
        assertEquals(0, d.minColumn());
        
        assertEquals(6, d.getDigit(2));
        assertEquals(0, d.getDigit(1));
        assertEquals(1, d.getDigit(0));
        
        // getDigit does not throw when OOB; returns 0 instead
        assertEquals(0, d.getDigit( 3));
        assertEquals(0, d.getDigit(-1));
        assertEquals(0, d.getDigit(Integer.MAX_VALUE));
        assertEquals(0, d.getDigit(Integer.MIN_VALUE));
        
        d = Decimal.valueOf(3_14, 1_00);
        assertEquals("3.14", d.toString());
        
        assertEquals(-2, d.scale());
        assertEquals( 3, d.width());
        assertEquals( 0, d.maxColumn());
        assertEquals(-2, d.minColumn());
        
        assertEquals(3, d.getDigit( 0));
        assertEquals(1, d.getDigit(-1));
        assertEquals(4, d.getDigit(-2));
        
        assertEquals(0, d.getDigit( 1));
        assertEquals(0, d.getDigit(-3));
        assertEquals(0, d.getDigit(Integer.MAX_VALUE));
        assertEquals(0, d.getDigit(Integer.MIN_VALUE));
        
        d = Decimal.valueOf(-987654321);
        assertEquals("-987654321", d.toString());
        
        assertEquals(0, d.scale());
        assertEquals(9, d.width());
        assertEquals(8, d.maxColumn());
        assertEquals(0, d.minColumn());
        
        for (int i = d.maxColumn(), digit = 9; i >= d.minColumn(); --i) {
            assertEquals(digit--, d.getDigit(i));
        }
        
        assertEquals(0, d.getDigit( 9));
        assertEquals(0, d.getDigit(-1));
        
        d = Decimal.valueOf(1234_56789, 100_000);
        assertEquals("1234.56789", d.toString());
        
        assertEquals(-5, d.scale());
        assertEquals( 9, d.width());
        assertEquals( 3, d.maxColumn());
        assertEquals(-5, d.minColumn());
        
        for (int i = d.minColumn(), digit = 1; i >= d.maxColumn(); --i) {
            assertEquals(digit++, d.getDigit(i));
        }
        
        assertEquals(0, d.getDigit( 4));
        assertEquals(0, d.getDigit(-6));
        
        d = Decimal.valueOf(-1_00001, 100_000);
        assertEquals("-1.00001", d.toString());
        
        assertEquals(-5, d.scale());
        assertEquals( 6, d.width());
        assertEquals( 0, d.maxColumn());
        assertEquals(-5, d.minColumn());
        
        for (int i = (-5 - 1); i <= (0 + 1); ++i) {
            assertEquals((i == 0 || i == -5) ? 1 : 0, d.getDigit(i));
        }
        
        d = Decimal.valueOf(1_000_000_000);
        assertEquals("1"+"000"+"000"+"000", d.toString());
        
        assertEquals( 0, d.scale());
        assertEquals(10, d.width());
        assertEquals( 9, d.maxColumn());
        assertEquals( 0, d.minColumn());
        
        for (int i = (0 - 1); i <= (9 + 1); ++i) {
            assertEquals((i == 9) ? 1 : 0, d.getDigit(i));
        }
        
        d = Decimal.valueOf(2_000, 1_000);
        assertEquals("2", d.toString());
        
        assertEquals(0, d.scale());
        assertEquals(1, d.width());
        assertEquals(0, d.maxColumn());
        assertEquals(0, d.minColumn());
        
        assertEquals(0, d.getDigit( 1));
        assertEquals(2, d.getDigit( 0));
        assertEquals(0, d.getDigit(-1));
    }
    
    @Test
    public void testDigitIterator() {
        testing();
        Decimal d;
        DigitIterator it;
        int digit;
        
        assertFalse(Decimal.ONE.digits() == Decimal.ONE.digits());
        it = Decimal.ONE.digits();
        assertTrue(it.hasNext());
        
        try {
            it.remove();
            fail();
        } catch (UnsupportedOperationException x) {
        } catch (IllegalStateException x) {
            fail(x.toString());
        }
        
        assertEquals(1, it.nextInt());
        try {
            it.remove();
            fail();
        } catch (UnsupportedOperationException x) {
        }
        
        d = Decimal.valueOf(123_456_789);
        assertEquals("123456789", d.toString());
        
        assertEquals(9, d.width());
        assertEquals(0, d.scale());
        
        it = d.digits();
        digit = 1;
        for (int i = 0; i <= 9; ++i) {
            assertEquals(i < 9, it.hasNext());
            if (!it.hasNext()) {
                try {
                    int next = it.nextInt();
                    fail(Integer.toString(next));
                } catch (NoSuchElementException x) {
                }
                break;
            }
            assertFalse(it.isTenthsColumn());
            assertEquals(digit++, it.nextInt());
            try {
                it.remove();
                fail();
            } catch (UnsupportedOperationException x) {
            }
        }
        
        d = Decimal.valueOf(1_234_567_890, 1_000_000_000);
        assertEquals("1.23456789", d.toString());
        
        assertEquals( 9, d.width());
        assertEquals(-8, d.scale());
        
        it = d.digits();
        digit = 1;
        for (int i = 0; i <= 9; ++i) {
            assertEquals(i < 9, it.hasNext());
            if (!it.hasNext()) {
                try {
                    int next = it.nextInt();
                    fail(Integer.toString(next));
                } catch (NoSuchElementException x) {
                }
                break;
            }
            assertEquals(digit == 2, it.isTenthsColumn());
            assertEquals(digit++,    it.nextInt());
            try {
                it.remove();
                fail();
            } catch (UnsupportedOperationException x) {
            }
        }
        
        d = Decimal.valueOf(1, 10_000_000_000L);
        assertEquals("0."+"000"+"000"+"000"+"1", d.toString());
        
        assertEquals( 10, d.width());
        assertEquals(-10, d.scale());
        
        it = d.digits();
        for (int i = 0; i <= 10; ++i) {
            assertEquals(i < 10, it.hasNext());
            if (!it.hasNext()) {
                try {
                    int next = it.nextInt();
                    fail(Integer.toString(next));
                } catch (NoSuchElementException x) {
                }
                break;
            }
            assertEquals( i == 0, it.isTenthsColumn());
            assertEquals((i == 9) ? 1 : 0, it.nextInt());
            try {
                it.remove();
                fail();
            } catch (UnsupportedOperationException x) {
            }
        }
    }
    
    @Test
    public void testCompareTo() {
        testing();
        
        // 1 == 1
        assertEquals( 0, Decimal.ONE.compareTo(Decimal.ONE));
        // 1 > 0
        assertEquals( 1, Decimal.ONE.compareTo(Decimal.ZERO));
        // 0 < 1
        assertEquals(-1, Decimal.ZERO.compareTo(Decimal.ONE));
        // 0 == 0
        assertEquals( 0, Decimal.ZERO.compareTo(Decimal.ZERO));
        
        Decimal d3_14 = Decimal.valueOf(3_14, 1_00);
        Decimal d3_15 = Decimal.valueOf(3_15, 1_00);
        
        // 3.14 == 3.14
        assertEquals( 0, d3_14.compareTo(d3_14));
        // 3.15 > 3.14
        assertEquals( 1, d3_15.compareTo(d3_14));
        // 3.14 < 3.15
        assertEquals(-1, d3_14.compareTo(d3_15));
        
        Decimal d31_4 = Decimal.valueOf(31_4, 10);
        
        // 31.4 == 31.4
        assertEquals( 0, d31_4.compareTo(d31_4));
        // 31.4 > 3.14
        assertEquals( 1, d31_4.compareTo(d3_14));
        // 3.14 < 31.4
        assertEquals(-1, d3_14.compareTo(d31_4));
        // 31.4 > 3.15
        assertEquals( 1, d31_4.compareTo(d3_15));
        // 3.15 < 31.4
        assertEquals(-1, d3_15.compareTo(d31_4));
        
        // -1 == -1
        assertEquals( 0, Decimal.ONE.negate().compareTo(Decimal.ONE.negate()));
        // -1 < 1
        assertEquals(-1, Decimal.ONE.negate().compareTo(Decimal.ONE));
        // 1 > -1
        assertEquals( 1, Decimal.ONE.compareTo(Decimal.ONE.negate()));
        // -1 < 0
        assertEquals(-1, Decimal.ONE.negate().compareTo(Decimal.ZERO));
        // 0 > -1
        assertEquals( 1, Decimal.ZERO.compareTo(Decimal.ONE.negate()));
        
        // -3.14 > -3.15
        assertEquals( 1, d3_14.negate().compareTo(d3_15.negate()));
        // -3.15 < -3.14
        assertEquals(-1, d3_15.negate().compareTo(d3_14.negate()));
        
        // 3.14 > -3.14
        assertEquals( 1, d3_14.compareTo(d3_14.negate()));
        // -3.14 > 3.14
        assertEquals(-1, d3_14.negate().compareTo(d3_14));
        
        int smallCount = 16;
        int largeCount = 65_536;
        
        List<Decimal> list = new ArrayList<>(largeCount);
        
        Collections.addAll(list, Decimal.valueOf(   -1000.5),
                                 Decimal.valueOf(    -88.25),
                                 Decimal.valueOf(    -1.625),
                                 Decimal.valueOf(    -1.125),
                                 Decimal.valueOf(        -1),
                                 Decimal.valueOf(         0),
                                 Decimal.valueOf( 123,  456),
                                 Decimal.valueOf(       0.5),
                                 Decimal.valueOf(     1.625),
                                 Decimal.valueOf(3_14, 1_00),
                                 Decimal.valueOf(    200.25));
        List<Decimal> expect = new ArrayList<>(list);
        do {
            Collections.shuffle(list);
        } while (expect.equals(list));
        
        list.sort(Comparator.naturalOrder());
        assertEquals(expect, list);
        expect.clear();
        
        for (int count : in(smallCount, largeCount)) {
            list.clear();
            int half = count / 2;
            int max  = half - 1;
            int min  = -half;
            
            for (int n = max; n >= min; --n) {
                list.add(Decimal.valueOf(n));
            }
            
            list.sort(Comparator.naturalOrder());
            assertThat(list, Sorting.isSorted());
            
            Collections.shuffle(list);
            
            list.sort(Comparator.naturalOrder());
            assertThat(list, Sorting.isSorted());
            
            if (count == smallCount) {
                System.out.println("  integers sorted:");
                for (Decimal d : list) {
                    System.out.print("    ");
                    System.out.println(d);
                }
            }
            
            for (int n = min, i = 0; n <= max; ++n) {
                assertEquals(Decimal.valueOf(n), list.get(i++));
            }
        }
        
        Random rand = new Random();
        List<Double> doubles = new ArrayList<>(largeCount);
        
        for (int count : in(smallCount, largeCount)) {
            list.clear();
            doubles.clear();
            
            for (int i = 0; i < count; ++i) {
                double d = 65_536 * rand.nextDouble() - 32_768;
                doubles.add(d);
                list.add(Decimal.valueOf(d));
            }
            
            doubles.sort(Comparator.naturalOrder());
            list.sort(Comparator.naturalOrder());
            
            assertThat(list, Sorting.isSorted());
            
            if (count == smallCount) {
                System.out.println("  floats sorted:");
                for (Decimal d : list) {
                    System.out.print("    ");
                    System.out.println(d);
                }
            }
            
            for (int i = 0; i < count; ++i) {
                assertEquals(Decimal.valueOf(doubles.get(i)), list.get(i));
            }
        }
        
        list.clear();
        doubles.clear();
    }
}