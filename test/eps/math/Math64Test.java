/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.math;

import eps.eval.*;
import eps.util.*;
import eps.test.*;
import eps.test.Tools.*;
import static eps.test.Tools.*;
import static eps.util.Misc.*;
//import static eps.util.Literals.*;

import java.util.*;
import java.util.function.*;
//import java.util.stream.*;

import org.junit.*;
import static org.junit.Assert.*;
//import static org.junit.Assume.*;

public class Math64Test {
    final MathEnv64 env;
    
    public Math64Test() {
        try {
            env = MathEnvPrivate.mathEnv64Instance();
        } catch (Throwable x) {
            x.printStackTrace(System.out);
            throw x;
        }
    }
    
    private FieldSetter logSetter;
    
    @Before
    public void disableLog() {
        logSetter = Tools.disableLog();
    }
    
    @After
    public void enableLog() {
        if (logSetter != null) {
            logSetter.close();
            logSetter = null;
        }
    }
    
    private long longValue(Number n) {
        return ((Integer64) n).longValue();
    }
    
    private double doubleValue(Number n) {
        return ((Float64) n).doubleValue();
    }
    
    @Test
    public void testConstants() {
        testing();
        assertEquals(Math.E, doubleValue(env.e()), 0);
        assertEquals(Math.PI, doubleValue(env.pi()), 0);
        
        assertEquals(0, longValue(env.zero()));
        assertEquals(1, longValue(env.one()));
        assertEquals(-1, longValue(env.negativeOne()));
    }
    
    @Test
    public void testFractionValueOf() {
        testing();
        
        for (Number[] args : new Number[][] {
            { env.one(), null      },
            { null,      env.one() },
            { null,      null      }}) {
            try {
                Number r = env.valueOf(args[0], args[1]);
                fail(Arrays.toString(args) + " = " + r);
            } catch (NullPointerException ignored) {
            }
        }
        
        // 5/7 = 5/7
        assertEquals(Fraction.create(5, 7, env), env.valueOf(env.valueOf(5), env.valueOf(7)));
        
        // 4/4 = 1
        assertEquals(Integer64.ONE, env.valueOf(env.valueOf(4), env.valueOf(4)));
        
        // 3/6 = 1/2
        assertEquals(Fraction.create(1, 2, env), env.valueOf(env.valueOf(3), env.valueOf(6)));
        
        // 0/1 = 0
        assertEquals(Integer64.ZERO, env.valueOf(env.zero(), env.one()));
        
        try {
            // 1/0 = err
            env.valueOf(env.one(), env.zero());
            fail();
        } catch (UndefinedException ignored) {
        }
        
        // 5.0/7.0 = 0.7142857142857143.0
        assertEquals(Float64.approx(5.0 / 7.0), env.valueOf(env.valueOf(5.0), env.valueOf(7.0)));
        
        // 3.0/6.0 = 0.5
        assertEquals(Float64.approx(3.0 / 6.0), env.valueOf(env.valueOf(3.0), env.valueOf(6.0)));
        
        // 0.0/1.0 = 0.0
        assertEquals(Float64.APPROX_ZERO, env.valueOf(Float64.APPROX_ZERO, Float64.APPROX_ONE));
        
        try {
            // 1.0/0.0 = err
            env.valueOf(Float64.APPROX_ONE, Float64.APPROX_ZERO);
            fail();
        } catch (UndefinedException ignored) {
        }
        
        // (1/2)/(3/4) = (1/2)*(4/3) = 4/6 = 2/3
        assertEquals(Fraction.create(2, 3, env),
                     env.valueOf(Fraction.create(1, 2, env), Fraction.create(3, 4, env)));
    }
    
    @Test
    public void testFactorial() {
        testing();
        try {
            @SuppressWarnings("ConstantConditions")
            Number n = env.factorial(null);
            fail(String.valueOf(n));
        } catch (NullPointerException ignored) {
        }
        try {
            Number n = env.factorial(env.pi());
            fail(String.valueOf(n));
        } catch (UndefinedException ignored) {
        }
        try {
            Number n = env.factorial(env.valueOf(-16));
            fail(String.valueOf(n));
        } catch (UndefinedException ignored) {
        }
        try {
            Number n = env.factorial(env.valueOf(Long.MAX_VALUE));
            fail(String.valueOf(n));
        } catch (UnsupportedException x) {
            String expect = "number too large: 25300281663413826863479626594436341300000"
                          + "00000000000000000000000000000000000000000000000000000000000"
                          + "00000000000000000000000000000000000000000000000000000000000"
                          + "00000000000000000000000000000000000000000000000000000000000"
                          + "00000000000000000000000000000000000000000000000000000000000"
                          + "0000000000000000000000000000000000000000000000";
            assertEquals(expect, x.getMessage());
        }
        
        long count = 0;
        long max   = Long.MAX_VALUE;
        
        for (long n = 0;; ++n) {
            Number fact = env.factorial(env.valueOf(n));
            
            if (fact instanceof Integer64) {
                long result = 1;
                for (long i = n; i > 1; --i) {
                    result *= i;
                }
                assertEquals(result, longValue(fact));
            } else if (fact instanceof Float64) {
                if (max == Long.MAX_VALUE) {
                    max = 2 * count;
                }
                
                long   longResult   = 1;
                double doubleResult = 1;
                for (long i = n; i > 1; --i) {
                    if (doubleResult == 1) {
                        try {
                            longResult = Math.multiplyExact(longResult, i);
                            continue;
                        } catch (ArithmeticException ignored) {
                        }
                        doubleResult = longResult;
                    }
                    doubleResult *= (double) i;
                }
                
                if (doubleResult == 1) {
                    doubleResult = longResult;
                }
                if (Double.isInfinite(doubleResult)) {
                    assert false : n;
                    count = max - 1;
                } else {
                    assertEquals(doubleResult, doubleValue(fact), 0);
                }
            } else {
                fail(String.valueOf(fact));
            }
            
            ++count;
            if (count >= max) {
                System.out.println("    stopped at factorial(" + n + ") = " + env.toDecimal(fact));
                break;
            }
        }
    }
    
    @Test
    public void testSqrt() {
        testing();
        
        try {
            Number n = env.sqrt(env.negativeOne());
            fail(String.valueOf(n));
        } catch (UndefinedException ignored) {
        }
        try {
            @SuppressWarnings("ConstantConditions")
            Number n = env.sqrt(null);
            fail(String.valueOf(n));
        } catch (NullPointerException ignored) {
        }
        
        assertEquals(env.zero(), env.sqrt(Boolean.FALSE));
        assertEquals(env.one(), env.sqrt(Boolean.TRUE));
        
        for (long i = 0; i <= Short.MAX_VALUE; ++i) {
            long sq = i * i;
            assertEquals(i, longValue(env.sqrt(new Integer64(sq))));
        }
        
        Random rand = new Random();
        
        for (long i = 0; i <= Short.MAX_VALUE; ++i) {
            double d = rand.nextDouble() * Double.MAX_VALUE;
            
            if (Double.isInfinite(d)) {
                d = Double.MAX_VALUE;
            }
            
            assertEquals(Math.sqrt(d), doubleValue(env.sqrt(env.valueOf(d))), 0);
        }
        
        assertEquals(new Integer64(0), env.sqrt(Fraction.create(new Integer64(0), new Integer64(9), env)));
        
        try {
            Number sqrt = env.sqrt(Fraction.create(env.valueOf(-4), env.valueOf(1), env));
            fail("sqrt(-4/1) = " + sqrt);
        } catch (UndefinedException ignored) {
        }
        try {
            Number sqrt = env.sqrt(Fraction.create(env.valueOf(4), env.valueOf(-1), env));
            fail("sqrt(4/-1) = " + sqrt);
        } catch (UndefinedException ignored) {
        }
        try {
            Number sqrt = env.sqrt(Fraction.create(env.valueOf(-4), env.valueOf(-1), env));
            assertEquals(new Integer64(2), sqrt);
        } catch (UndefinedException x) {
            fail(x.toString());
        }
        
        for (long d = 1; d <= Byte.MAX_VALUE; ++d) {
            Fraction sq   = Fraction.create(env.one(), env.valueOf(d * d), env);
            Number   sqrt = env.sqrt(sq);
            
            if (d == 1) {
                assertEquals(1, longValue(sqrt));
            } else {
                Fraction f = (Fraction) sqrt;
                assertEquals(1, longValue(f.getNumerator()));
                assertEquals(d, longValue(f.getDenominator()));
            }
        }
        
        // use primes so the fractions never reduce
        for (int i : Primes.FIRST_100_PRIMES) {
            for (int j : Primes.FIRST_100_PRIMES) {
                if (i == j) {
                    continue;
                }
                
                long n = i;
                long d = j;
                
                Number sq = env.valueOf(env.valueOf(n * n), env.valueOf(d * d));
                if (!(sq instanceof Fraction)) {
                    fail(f("n = %d, d = %d, sq = %s", n, d, sq));
                }
                
                Fraction rt = (Fraction) env.sqrt(sq);
                
                assertEquals(n, longValue(rt.getNumerator()));
                assertEquals(d, longValue(rt.getDenominator()));
            }
        }
    }
    
    // have already verified entire range of exact outputs
    // @Test
    @SuppressWarnings("unused")
    public void testIntegerSqrt() {
        testing();
        
        LongConsumer stopped = n -> {
            System.out.printf("    stopped at n = %d (n * n = %d)%n", n, n * n);
        };
        
        long estimatedMax = (long) Math.sqrt(Long.MAX_VALUE);
        System.out.println("    estimated max = " + estimatedMax);
        
        long start   = System.currentTimeMillis();
//        long timeout = 1_000;
//        long timeout = 600_000;
        long timeout = 0;
        
        double totalIterations = 0;
        long   nCalculations   = 0;
        int    maxIterations   = 0;
        
        /*try (FieldSetter fs = disableLog())*/ {
            MutableInt iterations = new MutableInt();
            
            for (long n = 0;; ++n) {
                if ((n % 10_000) == 0) {
                    if ((n % 1_000_000_000) == 0) {
                        System.out.println("    n = " + n);
                    }
                    // noinspection ConstantConditions
                    if (timeout != 0) {
                        if ((System.currentTimeMillis() - start) > timeout) {
                            stopped.accept(n);
                            break;
                        }
                    }
                }
                
                long sq;
                try {
                    sq = Math.multiplyExact(n, n);
                } catch (ArithmeticException x) {
                    stopped.accept(n);
                    System.out.println("    stopped naturally");
                    break;
                }
                if (n > estimatedMax) {
                    fail(Long.toString(n));
                }
                
                long sqrt;
                try {
                    sqrt = env.integerSqrt(sq, iterations);
                } catch (AssertionError x) {
                    stopped.accept(n);
                    throw x;
                }
                
                assertEquals(n, sqrt);
                
                int nIterations = iterations.value;
                
                if (nIterations > Long.SIZE) {
                    fail("n = " + n + ", sq = " + sq + ", iterations = " + nIterations);
                }
                
                totalIterations += nIterations;
                ++nCalculations;
                if (nIterations > maxIterations) {
                    maxIterations = nIterations;
                }
            }
            
            System.out.println("    avg iterations = " + Math.round(totalIterations / nCalculations));
            System.out.println("    max iterations = " + maxIterations);
        }
    }
    
    @Test
    public void testNthRoot() {
        testing();
        
        // test NPE
        for (Number[] args : new Number[][]
           {{ env.one(), null      },
            { null,      env.one() },
            { null,      null      }}) {
            try {
                Number rt = env.nrt(args[0], args[1]);
                fail(String.valueOf(rt));
            } catch (NullPointerException ignored) {
            }
        }
        
        // test 0th root
        for (int rad = -32; rad <= 32; ++rad) {
            try {
                Number rt = env.nrt(env.zero(), env.valueOf(rad));
                fail("0th root should be undefined. found: " + rt);
            } catch (UndefinedException ignored) {
            }
        }
        
        // test root of 0
        for (int i = 1; i <= 32; ++i) {
            assertEquals("i'th root of 0 should be 0", Integer64.ZERO, env.nrt(env.valueOf(i), env.zero()));
        }
        
        // test cube root variety (should be exact)
        Integer64 three = new Integer64(3);
        
        for (long n = Byte.MIN_VALUE; n <= Byte.MAX_VALUE; ++n) {
            long cube = n * n * n;
            
            Number cbrt = env.nrt(three, env.valueOf(cube));
            assertEquals(n, longValue(cbrt));
        }
        
        // test floating root preconditions
        
        // negative float
        //
        // This should fail because the index is approximate. We therefore
        // can't prove whether or not the result here is actually (in this case)
        // -3 or some complex number, because the index 3 could be the
        // result of some rounding.
        //
        try {
            Number rt = env.nrt(Float64.approx(3), Float64.approx(-27));
            fail("approximate 3rd root of -81 should be indeterminate. found: " + rt);
        } catch (UndefinedException ignored) {
        }
        
        // float zero index
        try {
            Number rt = env.nrt(Float64.APPROX_ZERO, Float64.approx(81));
            fail("0th root should be undefined. found: " + rt);
        } catch (UndefinedException ignored) {
        }
        // float zero radicand
        for (int i = 1; i < 10; ++i) {
            Number rt = env.nrt(Float64.approx(i), env.zero());
            assertEquals("i'th root of 0 should be 0", Float64.APPROX_ZERO, rt);
        }
        
        // float one index
        for (int i = 1; i < 10; ++i) {
            Number rt = env.nrt(Float64.approx(i), env.one());
            assertEquals("i'th root of 1 should be 1", Float64.APPROX_ONE, rt);
        }
        // float one radicand
        for (int i = 1; i < 10; ++i) {
            Number rt = env.nrt(env.one(), Float64.approx(i));
            assertEquals("1st root of i should be i", i, doubleValue(rt), 0);
        }
        
        // general integer testing
        for (long ind = 1; ind <= 32; ++ind) {
            for (long exp = -32; exp <= 32; ++exp) {
                boolean overflow = false;
                
                long rad = 1;
                for (int i = 0; i < ind; ++i) {
                    try {
                        rad = Math.multiplyExact(rad, exp);
                    } catch (ArithmeticException x) {
                        overflow = true;
                        break;
                    }
                }
                
                if (overflow) {
                    continue;
                }
                // special case, because integerNrt uses Math.negateExact on it
                if (rad == Long.MIN_VALUE) {
                    continue;
                }
                
                long expect = ((ind & 1) == 1) ? exp : Math.abs(exp);
                
                boolean fail = true;
                try {
                    Number result = env.nrt(env.valueOf(ind), env.valueOf(rad));
                    assertEquals(expect, longValue(result));
                    
                    fail = false;
                } finally {
                    if (fail) {
                        System.out.printf("    %d root of %d failed: expected %d%n", ind, rad, expect);
                    }
                }
            }
        }
        
        // general float testing
        Random rand = new Random();
//        double epsilon = 1.0 / 1024.0;
        
        for (int i = 0; i < Short.MAX_VALUE; ++i) {
            double ind;
            double exp;
            double rad;
            do {
                ind = rand.nextDouble() * Byte.MAX_VALUE;
                exp = rand.nextDouble() * Byte.MAX_VALUE;
                rad = Math.pow(exp, ind);
                exp = Math.pow(rad, 1 / ind); // note: epsilon not required, with this line
            } while (!allMatch(n -> n != 0 && n != 1 && Math2.isReal(n), ind, exp, rad));
            
            boolean fail = true;
            try {
                Number result = env.nrt(env.valueOf(ind), env.valueOf(rad));
                assertEquals(exp, doubleValue(result), 0);
                
                fail = false;
            } finally {
                if (fail) {
                    System.out.printf("    %f root of %f failed: expected %f%n", ind, rad, exp);
                }
            }
        }
        
        // general fraction testing (with integers)
        
        for (int i : Primes.FIRST_100_PRIMES) {
            for (int j : Primes.FIRST_100_PRIMES) {
                if (i == j) continue;
                
                for (int k = 3; k <= 6; ++k) {
                    long n = 1;
                    long d = 1;
                    
                    for (int h = 0; h < k; ++h) {
                        n = Math.multiplyExact(n, i);
                        d = Math.multiplyExact(d, j);
                    }
                    
                    Fraction frac = Fraction.create(env.valueOf(n), env.valueOf(d), env);
                    frac = (Fraction) env.nrt(env.valueOf(k), frac);
                    
                    assertEquals(i, longValue(frac.getNumerator()));
                    assertEquals(j, longValue(frac.getDenominator()));
                }
            }
        }
        
        // general reality checks
        assertEquals(-3, longValue(env.nrt(env.valueOf(3), env.valueOf(-27))));
        assertEquals( 3, longValue(env.nrt(env.valueOf(4), env.valueOf( 81))));
        assertEquals( 2, longValue(env.nrt(env.valueOf(4), env.valueOf( 16))));
        assertEquals( 5, longValue(env.nrt(env.valueOf(3), env.valueOf(125))));
    }
    
    @SafeVarargs
    private final <T> List<T[]> combos2(T... args) {
        List<T[]> combos = new ArrayList<>();
        
        for (int i = 0; i < args.length; ++i) {
            for (int j = 0; j < args.length; ++j) {
                T[] arr = Arrays.copyOf(args, 2);
                arr[0] = args[i];
                arr[1] = args[j];
                combos.add(arr);
            }
        }
        
        return combos;
    }
    
    @Test
    public void testPow() {
        testing();
        
        // general preconditions:
        
        // NPE
        for (Number[] args : new Number[][]
               {{ env.one(), null      },
                { null,      env.one() },
                { null,      null      }}) {
            try {
                Number result = env.pow(args[0], args[1]);
                fail("should throw npe. found: " + result);
            } catch (NullPointerException ignored) {
            }
        }
        
        Fraction intZeroOnOne   = Fraction.create(Integer64.ZERO, env.one(), env);
        Fraction floatZeroOnOne = Fraction.create(Float64.APPROX_ZERO, env.one(), env);
        
        // 0^0
        for (Number[] args : combos2(Integer64.ZERO,
                                     Float64.APPROX_ZERO,
                                     intZeroOnOne,
                                     floatZeroOnOne)) {
            try {
                Number result = env.pow(args[0], args[1]);
                fail("0^0 = " + result);
            } catch (UndefinedException ignored) {
            }
        }
        
        // 0^-1, which is 1/0
        for (Number zero : new Number[] {
                Integer64.ZERO,
                Float64.APPROX_ZERO,
                intZeroOnOne,
                floatZeroOnOne }) {
            try {
                Number result = env.pow(zero, env.negativeOne());
                fail("0^-1 = 1/0 = " + result);
            } catch (UndefinedException ignored) {
            }
        }
        
        final long min = -8;
        final long max = +8;
        {
            // other special cases
            // (old and covered elsewhere, but remain for posterity)
            
            // 0^x (int)
            for (long exp = min; exp <= max; ++exp) {
                try {
                    Number result = env.pow(env.zero(), env.valueOf(exp));
                    assertEquals(0, longValue(result));
                } catch (UndefinedException x) {
                    assertTrue("0^-1 or 0^0", exp <= 0);
                }
            }
            // 0^x (float)
            for (long exp = min; exp <= max; ++exp) {
                try {
                    Number result = env.pow(Float64.APPROX_ZERO, env.valueOf(exp / 2.0));
                    assertEquals(0, doubleValue(result), 0);
                } catch (UndefinedException x) {
                    assertTrue("0^-1 or 0^0", exp <= 0);
                }
            }
            // x^0 (int)
            for (long base = min; base <= max; ++base) {
                if (base == 0) continue; // throws
                Number result = env.pow(env.valueOf(base), env.zero());
                assertEquals(1, longValue(result));
            }
            // x^0 (float)
            for (long base = min; base <= max; ++base) {
                if (base == 0) continue; // throws
                Number result = env.pow(env.valueOf(base / 2.0), Float64.APPROX_ZERO);
                assertEquals(1, doubleValue(result), 0);
            }
            // 1^x (int)
            for (long exp = min; exp <= max; ++exp) {
                Number result = env.pow(env.one(), env.valueOf(exp));
                assertEquals(1, longValue(result));
            }
            // 1^x (float)
            for (long exp = min; exp <= max; ++exp) {
                Number result = env.pow(Float64.APPROX_ONE, env.valueOf(exp / 2.0));
                assertEquals(1, doubleValue(result), 0);
            }
            // x^1 (int)
            for (long base = min; base <= max; ++base) {
                Number result = env.pow(env.valueOf(base), env.one());
                assertEquals(base, longValue(result));
            }
            // x^1 (float)
            for (long base = min; base <= max; ++base) {
                try {
                    Number result = env.pow(env.valueOf(base / 2.0), Float64.APPROX_ONE);
                    assertEquals(base / 2.0, doubleValue(result), 0);
                } catch (UndefinedException x) {
                    assertTrue("possible root of a negative number", base < 0);
                }
            }
        }
        
        // The next 3 loops cover exact integer cases:
        
        // 1. general exact integer cases with positive exponent
        for (long base = min; base <= max; ++base) {
            for (long exp = 1; exp <= max; ++exp) {
                boolean failed = true;
                try {
                    Number result = env.pow(env.valueOf(base), env.valueOf(exp));
                    assertEquals(naivePow(base, exp), longValue(result));
                    
                    failed = false;
                } finally {
                    if (failed) {
                        System.out.printf("    failed at %d^%d%n", base, exp);
                    }
                }
            }
        }
        
        // 2. general exact integer cases with a zero exponent
        for (long base = min; base <= max; ++base) {
            if (base == 0) {
                continue; // throws
            }
            
            Number result = env.pow(env.valueOf(base), env.zero());
            assertEquals(1, longValue(result));
        }
        
        // 3. general exact integer cases with negative exponent
        //
        //    should result in a reciprocal fraction, except for the
        //    special cases of 1^-x and -1^-x which still result in
        //    an integer
        for (long base = min; base <= max; ++base) {
            for (long exp = min; exp < 0; ++exp) {
                if (base == 0) {
                    continue; // throws
                }
                
                boolean failed = true;
                try {
                    Number result = env.pow(env.valueOf(base), env.valueOf(exp));
                    if (base == -1 || base == 1) {
                        assertEquals(naivePow(base, -exp), longValue(result));
                    } else {
                        Fraction fract = (Fraction) result;
                        result = fract.getDenominator();
                        assertEquals(naivePow(base, -exp), fract.signum() * longValue(result));
                    }
                    failed = false;
                } finally {
                    if (failed) {
                        System.out.printf("    failed at %d^%d%n", base, exp);
                    }
                }
            }
        }
        
        // integer overflow boundary cases:
        //      2^64
        assertEquals(Math.pow(2, 64), doubleValue(env.pow(env.valueOf(2), env.valueOf(64))), 0);
        //      2^-64
        assertEquals(1 / Math.pow(2, 64), doubleValue(env.pow(env.valueOf(2), env.valueOf(-64))), 0);
        //     -2^65
        assertEquals(Math.pow(-2, 65), doubleValue(env.pow(env.valueOf(-2), env.valueOf(65))), 0);
        //     -2^-65
        assertEquals(1 / Math.pow(-2, 65), doubleValue(env.pow(env.valueOf(-2), env.valueOf(-65))), 0);
        
        // The next 4 loops cover floating-point cases:
        
        // negative float bases should all fail, because they
        // could entail the root of a negative number.
        //
        // for example, suppose we have -1^(1.0 / 3.0). this could round
        // to either of the following:
        //
        // -1^0.333 = -1^(333 / 1000) = (-1^333)^(1/1000) = 1000√(-1^333) = 1000√(-1) = i
        //
        // -1^0.334 = -1^(334 / 1000) = (-1^334)^(1/1000) = 1000√(-1^334) = 1000√(1)  = 1
        //
        // in other words, if we are given something like e.g. pow(-1, 0.333),
        // the actual result could be 1 or i, but we don't know because 0.333
        // is approximate, so we throw
        //
        // only special case is -x^0 which is still treated as (approximately) 1
        //
        for (double base = min; base < 0; ++base) {
            for (double exp = min; exp <= max; ++exp) {
                try {
                    Number result = env.pow(env.valueOf(base), env.valueOf(exp));
                    if (exp != 0) {
                        fail(base + "^" + exp + " = " + result);
                    } else {
                        assertEquals(1, doubleValue(result), 0);
                    }
                } catch (UndefinedException x) {
                    assertTrue(exp != 0);
                }
            }
        }
        
        // 0^x should be 0, unless x is negative or 0
        for (double exp = min; exp <= max; ++exp) {
            try {
                Number result = env.pow(Float64.APPROX_ZERO, env.valueOf(exp));
                if (exp <= 0) {
                    fail("0^" + exp + " = " + result);
                } else {
                    assertEquals(Float64.APPROX_ZERO, result);
                }
            } catch (UndefinedException x) {
                assertTrue(exp <= 0);
            }
        }
        
        // otherwise, floating-point pow is straightforward
        for (double base = 1; base <= max; ++base) {
            for (double exp = min; exp <= max; ++exp) {
                
                double expect = Math.pow(base, exp);
                Number result = env.pow(env.valueOf(base), env.valueOf(exp));
                assertEquals(expect, doubleValue(result), 0);
            }
        }
        
        // test some random fractional values
        Random rand = new Random();
        for (int i = 0; i < Short.MAX_VALUE; ++i) {
            double base;
            double exp;
            do {
                base = rand.nextInt(16);
                exp  = 7 - rand.nextInt(16);
            } while (base == 0 || exp == 0);
            base *= rand.nextDouble();
            exp  *= rand.nextDouble();
            
            double expect;
            if (exp < 0) {
                // MathEnv64.pow(...) actually computes it this
                // way, so this lets us use 0 as epsilon.
                expect = 1 / Math.pow(base, -exp);
            } else {
                expect = Math.pow(base, exp);
            }
            
            Number  result = env.pow(env.valueOf(base), env.valueOf(exp));
            boolean failed = true;
            try {
                assertEquals(expect, doubleValue(result), 0); // 1.0 / 128.0);
                failed = false;
            } finally {
                if (failed) {
                    System.out.printf("    failed at %f^%f%n", base, exp);
                }
            }
        }
        
        // quick special case
        try {
            Number maxVal = env.valueOf((double) Long.MAX_VALUE);
            Number result = env.pow(maxVal, maxVal);
            fail("this should result in IEEE infinity. found: " + result);
        } catch (UndefinedException ignored) {
        }
        
        // general fractions
        
        Fraction one2  = Fraction.create(1,  2, env);
        Fraction one4  = Fraction.create(1,  4, env);
        Fraction one8  = Fraction.create(1,  8, env);
        Fraction one16 = Fraction.create(1, 16, env);
        
        // (1/2)^4 = 1/16
        assertEquals(one16, env.pow(one2, env.valueOf(4)));
        // (1/2)^(-4) = 16
        assertEquals(env.valueOf(16), env.pow(one2, env.valueOf(-4)));
        // (1/16)^(1/4) = 1/2
        assertEquals(one2, env.pow(one16, one4));
        
        for (int i : Primes.FIRST_100_PRIMES) {
            for (int j : Primes.FIRST_100_PRIMES) {
                if (i == j) continue;
                for (int k = 2; k <= 6; ++k) {
                    Fraction fract  = Fraction.create(env.valueOf(i), env.valueOf(j), env);
                    Fraction result = (Fraction) env.pow(fract, env.valueOf(k));
                    
                    assertEquals(naivePow(i, k), longValue(result.getNumerator()));
                    assertEquals(naivePow(j, k), longValue(result.getDenominator()));
                }
            }
        }
        
        // 32^(-3/5) = 1/8
        assertEquals(one8, env.pow(env.valueOf(32), env.valueOf(-3, 5)));
        
        // this last test became convoluted, but it tests
        // all cases in a contiguous range of input
        for (long base = -32; base <= 32; ++base) {
            for (long exp = -32; exp <= 32; ++exp) {
                if (base == 0 && exp <= 0) {
                    // undefined
                    try {
                        Number result = env.pow(env.valueOf(base), env.valueOf(exp));
                        fail(result.toString());
                    } catch (UndefinedException ignored) {
                    }
                    continue;
                }
                
                boolean failed = true;
                try {
                    Number result = env.pow(env.valueOf(base), env.valueOf(exp));
                    
                    if (exp > 0) {
                        // if the exponent is > 0, the method should attempt integer
                        // pow, unless it overflows, in which case it should forward
                        // to Math.pow
                        try {
                            assertEquals(naivePow(base, exp), longValue(result));
                        } catch (ArithmeticException x) {
                            assertEquals(Math.pow(base, exp), doubleValue(result), 0);
                        }
                    } else if (exp < 0) {
                        // if the exponent is < 0, the method should attempt integer
                        // pow and then return a fraction reciprocal, unless integer
                        // pow overflows, in which case it should forward to Math.pow
                        if (result instanceof Fraction) {
                            int signum = result.signum();
                            result     = ((Fraction) result).getDenominator();
                            // Fraction always stores the sign in the numerator
                            long longV = longValue(result) * signum;
                            assertEquals(naivePow(base, -exp), longV);
                        } else if (base == 1 || base == -1) {
                            // also, the result is an exact integer, if the
                            // base was 0, 1 or -1
                            long expect = (base > 0 || ((-exp) & 1) == 0) ? 1 : -1;
                            assertEquals(expect, longValue(result));
                        } else {
                            assertNotEquals(0, base);
                            assertEquals(1 / Math.pow(base, -exp), doubleValue(result), 0);
                        }
                    } else {
                        assertEquals(0, exp);
                        // if the exponent is 0, the method should always return 1
                        assertEquals(1, longValue(result));
                    }
                    
                    failed = false;
                } finally {
                    if (failed) {
                        System.out.printf("    failed at %d^%d%n", base, exp);
                    }
                }
            }
        }
    }
    
    private long naivePow(long base, long exponent) {
        assert exponent >= 0 : exponent;
        long result = 1;
        while (exponent > 0) {
            result = Math.multiplyExact(result, base);
            --exponent;
        }
        return result;
    }
    
    @Test
    public void testPowBySquaring() {
        testing();
        
        try {
            long result = env.powBySquaring(1, -1);
            fail("1^-1 = " + result);
        } catch (ArithmeticException ignored) {
        }
        
        assertEquals(1, env.powBySquaring(0, 0));
        
        for (long e = 1; e <= 32; ++e) {
            assertEquals(Long.toString(e), 0, env.powBySquaring(0, e));
        }
        
        for (long e = 0; e <= 32; ++e) {
            assertEquals(Long.toString(e), 1, env.powBySquaring(1, e));
        }
        
//        System.out.println("    " + env.integerNrt(3, Long.MAX_VALUE));
//        System.out.println("    " + env.integerNrt(2, Long.MAX_VALUE));
        
        for (int sign = 1; Math.abs(sign) == 1; sign -= 2) {
            forEachBase:
            for (long absBase = 2;; ++absBase) {
                // forEachExponent:
                for (long exponent = 0;; ++exponent) {
                    long base = absBase * sign;
                    boolean fail = false;
                    try {
                        long result = env.powBySquaring(base, exponent);
                        fail = true;
                        assertEquals(naivePow(base, exponent), result);
                        fail = false;
                    } catch (ArithmeticException x) {
//                        System.out.printf("    overflow at %d^%d%n", base, exponent);
                        // note: all valid inputs have been tested with exponents
                        //       down to cubes, but there's not really any point
                        //       to that
                        if (exponent == 6 /*3*/) {
                            break forEachBase;
                        } else {
                            break;
                        }
                    } finally {
                        if (fail) {
                            System.out.printf("    failed at %d^%d%n", base, exponent);
                        }
                    }
                }
            }
        }
        
        assertEquals( 256, env.powBySquaring( 2, 8));
        assertEquals(  81, env.powBySquaring( 3, 4));
        assertEquals( 625, env.powBySquaring( 5, 4));
        assertEquals(-125, env.powBySquaring(-5, 3));
    }
}