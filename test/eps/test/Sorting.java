/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.test;

import static eps.util.Literals.*;

import java.util.*;
import static java.util.Comparator.*;

import org.hamcrest.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.core.IsNot.*;

public final class Sorting {
    public Sorting() {
    }
    
    public static class IsSortedMatcher<T> extends BaseMatcher<List<T>> {
        private final Comparator<? super T> comp;
        
        public IsSortedMatcher(Comparator<? super T> comp) {
            this.comp = Objects.requireNonNull(comp, "comp");
        }
        
        @Override
        public boolean matches(Object obj) {
            if (obj instanceof List<?>) {
                List<?> list = (List<?>) obj;
                
                @SuppressWarnings("unchecked")
                Comparator<Object> comp = (Comparator<Object>) this.comp;
                
                Iterator<?> it = list.iterator();
                if (it.hasNext()) {
                    Object prev = it.next();
                    
                    while (it.hasNext()) {
                        Object next = it.next();
                        
                        try {
                            if (comp.compare(prev, next) > 0) {
                                return false;
                            }
                        } catch (ClassCastException | NullPointerException x) {
                            return false;
                        }
                        
                        prev = next;
                    }
                }
                
                return true;
            }
            return false;
        }
        
        @Override
        public void describeTo(Description desc) {
            desc.appendText(getClass().getSimpleName())
                .appendText("{comp = ")
                .appendValue(comp)
                .appendText("}");
        }
        
        @Override
        public void describeMismatch(Object obj, Description desc) {
            if (!(obj instanceof List<?>)) {
                desc.appendText("value is not a List: ").appendValue(obj);
            } else {
                desc.appendText("List is not sorted: ").appendValue(obj);
            }
        }
    }
    
    public static <T> IsSortedMatcher<T> isSorted(Comparator<? super T> comp) {
        return new IsSortedMatcher<>(comp);
    }
    
    public static <T extends Comparable<? super T>> IsSortedMatcher<T> isSorted() {
        return new IsSortedMatcher<>(naturalOrder());
    }
    
    @Test
    public void testIsSorted() {
        List<Integer> ints = new ArrayList<>();
        for (int i = 0; i < 50; ++i) {
            ints.add(i);
        }
        Collections.shuffle(ints);
        ints.sort(naturalOrder());
        assertThat(ints, isSorted());
        
        assertThat(ListOf(1, 4, 2), not(isSorted()));
        assertThat(ListOf(1, null), not(isSorted()));
        
        List<String> caseins = new ArrayList<>();
        caseins.add("abc");
        caseins.add("ABC");
        caseins.add("aBc");
        caseins.add("Abc");
        caseins.add("AbC");
        caseins.sort(naturalOrder());
        Collections.reverse(caseins);
        
        assertThat(caseins, isSorted(String.CASE_INSENSITIVE_ORDER));
    }
}