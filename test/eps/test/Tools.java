/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.test;

import eps.app.*;
import eps.util.*;

import java.util.*;
import java.util.function.*;
import java.lang.reflect.*;

public final class Tools {
    private Tools() {
    }
    
    public static final long GLOBAL_TIMEOUT = 10_000;
    
    public static String randomString(Random random, int length) {
        char[] chars = new char[length];
        
        for (int i = 0; i < length; ++i) {
            char c = (char) ('A' + random.nextInt(('Z' - 'A') + 1));
            chars[i] = c;
        }
        
        return new String(chars);
    }
    
    public static void testing() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        StackTraceElement   call  = trace[2];
        System.out.println("@Test " + call.getClassName() + "." + call.getMethodName());
    }
    
    private static final Constructor<String> NEW_STRING;
    static {
        Constructor<String> ctor;
        try {
            ctor = String.class.getDeclaredConstructor(char[].class, boolean.class);
            ctor.setAccessible(true);
        } catch (ReflectiveOperationException x) {
            eps.app.Log.caught(Tools.class, "getting private String constructor", x, false);
            ctor = null;
        }
        NEW_STRING = ctor;
    }
    
    public static String newString(char[] chars) {
        if (NEW_STRING != null) {
            try {
                return NEW_STRING.newInstance(chars, true);
            } catch (ReflectiveOperationException x) {
                eps.app.Log.caught(Tools.class, "creating String instance", x, false);
            }
        }
        return new String(chars);
    }
    
    public static void setStaticField(Class<?> decl, String name, Object value) {
        try {
            Field field = decl.getDeclaredField(name);
            field.setAccessible(true);
            setField(null, field, value);
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
    
    public static void setField(Object instance, Field field, Object value) {
        try {
            Field modifiers = Field.class.getDeclaredField("modifiers");
            modifiers.setAccessible(true);
            modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            field.setAccessible(true);
            assert !Modifier.isFinal(field.getModifiers()) : field;
            field.set(instance, value);
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
    
    public static final class FieldSetter implements AutoCloseable {
        private final Object instance;
        private final Field  field;
        private final Object value;
        
        private FieldSetter(Object instance, Field field, Object value) {
            this.instance = instance;
            this.field    = field;
            this.value    = value;
        }
        
        @Override
        public void close() {
            try {
                field.set(instance, value);
            } catch (ReflectiveOperationException x) {
                throw new UncheckedReflectiveOperationException(x);
            }
        }
    }
    
    public static FieldSetter setStaticFieldTemporarily(Class<?> decl, String name, Object value) {
        try {
            Field field = decl.getDeclaredField(name);
            Field mods  = Field.class.getDeclaredField("modifiers");
            mods.setAccessible(true);
            mods.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            field.setAccessible(true);
            Object prev = field.get(null);
            setField(null, field, value);
            return new FieldSetter(null, field, prev);
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
    
    public static FieldSetter disableLog() {
        return setStaticFieldTemporarily(Log.class, "ENABLED", false);
    }
    
    public static void setLogEnabled(boolean enabled) {
        setStaticField(Log.class, "ENABLED", enabled);
    }
    
    public static final class MessageSupplier implements Supplier<String> {
        private final Supplier<?> supplier;
        
        public MessageSupplier(Supplier<?> supplier) {
            this.supplier = supplier;
        }
        
        @Override
        public String get() {
            return toString();
        }
        
        @Override
        public String toString() {
            if (supplier == null)
                return "null";
            return String.valueOf(supplier.get());
        }
    }
    
    public static Object msg(Supplier<?> supplier) {
        return new MessageSupplier(supplier);
    }
    
    public static int[] in(int... values) {
        return values;
    }
    
    public static long[] in(long... values) {
        return values;
    }
    
    public static double[] in(double... values) {
        return values;
    }
    
    public static boolean[] in(boolean... values) {
        return values;
    }
    
    public static <T> Supplier<T> getting(ReflectiveSupplier<T> supplier) {
        return supplier;
    }
    
    public static <T> T get(ReflectiveSupplier<T> supplier) {
        return supplier.get();
    }
}