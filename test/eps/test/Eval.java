/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.test;

import eps.app.*;
import eps.eval.*;
import eps.math.*;
import eps.util.*;

import java.util.concurrent.atomic.*;

public final class Eval {
    private Eval() {
    }
    
    private static final Symbols  DEFAULT_SYMS = Symbols.Concurrent.ofDefaults();
    private static final Settings DEFAULT_SETS = Settings.Concurrent.ofDefaults();
    
    public static Context newContext(String source, MathEnv env) {
        Symbols syms = DEFAULT_SYMS.copy();
        return newContext(source, env, syms);
    }
    
    @SuppressWarnings("unchecked")
    public static Context newContext(String source, MathEnv env, Symbols syms) {
        Settings sets = DEFAULT_SETS;
        
        MiniSettings mini = MiniSettings.instance();
        if (mini != MiniSettings.DEFAULT) {
            sets = Settings.Concurrent.ofDefaults();
            for (Setting<?> set : Setting.values()) {
                sets.set((Setting<Object>) set, mini.getSetting(set));
            }
        }
        
        return newContext(source, sets, env, syms);
    }
    
    public static Context newContext(String source, Settings sets, MathEnv env, Symbols syms) {
        return new Context(source, sets, env, syms);
    }
    
    private static final AtomicReference<MathEnv> MATH_ENV = new AtomicReference<>();
    
    public static MathEnv setMathEnv(MathEnv env) {
        return MATH_ENV.getAndSet(env);
    }
    
    public static MathEnv getMathEnv() {
        MathEnv env = MATH_ENV.get();
        if (env == null)
            throw new IllegalStateException();
        return env;
    }
    
    public static Operand eval(String source) {
        return eval(source, getMathEnv());
    }
    
    public static Operand eval(String source, MathEnv env) {
        Context context = newContext(source, env);
        return eval(context);
    }
    
    public static Operand eval(String source, MathEnv env, Symbols syms) {
        Context context = newContext(source, env, syms);
        return eval(context);
    }
    
    public static Operand eval(Context context) {
        java.util.Objects.requireNonNull(context, "context");
        Context previous = Context.setLocal(context);
        try (Context _context = context) {
            Tree    tree   = Tree.create(context);
            Operand result = tree.evaluate(context);
            return result;
        } catch (EvaluationException x) {
//            x.printStackTrace(System.out);
            throw x;
        } finally {
            Context.setLocal(previous);
        }
    }
    
    public static class Test {
        public final Operand result;
        public final String  source;
        
        public Test(Operand result, String source) {
            this.result = result;
            this.source = source;
        }
        
        protected static final ToString<Test> TEST_TO_STRING =
            ToString.builder(Test.class)
                    .add("source", test -> test.source)
                    .add("result", test -> test.result)
                    .build();
        @Override
        public String toString() {
            return TEST_TO_STRING.apply(this);
        }
    }
    
    public static Test newTest(Operand result, String source) {
        return new Test(result, source);
    }
    
    public static Test newTest(long result, String source) {
        return newTest(getMathEnv().valueOf(result), source);
    }
    
    public static Test newTest(double result, String source) {
        return newTest(getMathEnv().valueOf(result), source);
    }
    
    public static Test newTest(long resultN, long resultD, String source) {
        return newTest(getMathEnv().valueOf(resultN, resultD), source);
    }
    
    public static Test newTest(String result, String source) {
        return newTest(new StringLiteral(result), source);
    }
    
    public static Test newTest(boolean result, String source) {
        return newTest(eps.math.Boolean.valueOf(result), source);
    }
}