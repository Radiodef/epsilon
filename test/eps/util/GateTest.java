/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;

import java.util.*;
import java.util.concurrent.atomic.*;
import java.util.stream.*;
import static java.util.stream.Collectors.*;

import org.junit.*;
import static org.junit.Assert.*;

public class GateTest {
    public GateTest() {
    }
    
    private static final long TIMEOUT = 10_000;
    
    private void sleepRand() {
        Misc.sleep(1 + (long) (Math.random() * 100));
    }
    
    @Test(timeout = TIMEOUT)
    public void testSharedResourceProtection() {
        testing();
        
        List<String>  list = new ArrayList<>();
        Gate.Consumer gate = new Gate.Consumer();
        
        int count = 10;
        
        Thread[] threads = new Thread[count];
        
        for (int i = 0; i < count; ++i) {
            int num = i;
            
            Thread t = new Thread(() -> {
                sleepRand();
                gate.accept(() -> list.add(String.valueOf(num)));
            });
            
            t.setName("testSharedResourceProtection.Thread" + i);
            threads[i] = t;
        }
        
        try (Gate blocked = gate.block()) {
            assertTrue("gate should be blocked", gate.isBlocked());
            assertFalse("threads not started", gate.hasActionsWaiting());
            
            for (Thread t : threads) {
                t.start();
                assertTrue("gate should have blocked", list.isEmpty());
            }
            for (Thread t : threads) {
                Misc.join(t);
                assertTrue("gate should have blocked", list.isEmpty());
            }
            
            assertTrue("gate should be blocked", gate.isBlocked());
            assertTrue("threads should be done", gate.hasActionsWaiting());
        }
        
        assertFalse("gate should not be blocked", gate.isBlocked());
        assertFalse("actions should be flushed", gate.hasActionsWaiting());
        
        assertEquals("all actions should be complete", count, list.size());
        
        Set<String> expect = IntStream.range(0, count).mapToObj(String::valueOf).collect(toSet());
        Set<String> actual = new HashSet<>(list);
        assertTrue(actual.containsAll(list));
        
        assertEquals("all actions should be complete", expect, actual);
    }
    
    @Test(timeout = TIMEOUT)
    public void testExceptionHandling() {
        testing();
        
        Gate.Consumer gate = new Gate.Consumer();
        
        int count = 10;
        
        Thread[] threads = new Thread[count];
        
        for (int i = 0; i < count; ++i) {
            Integer num = i;
            
            Thread t = new Thread(() -> {
                sleepRand();
                gate.accept(() -> { throw new ClassCastException(num.toString()); });
            });
            
            t.setName("testExceptionHandling.Thread" + i);
            threads[i] = t;
        }
        
        Throwable caught = null;
        
        try (Gate blocked = gate.block()) {
            for (Thread t : threads)
                t.start();
            for (Thread t : threads)
                Misc.join(t);
            
            assertTrue("threads are done", gate.hasActionsWaiting());
            
        } catch (ClassCastException x) {
            caught = x;
        }
        
        assertNotNull("should have caught the last exception", caught);
        assertFalse("gate should not be blocked regardless", gate.isBlocked());
        assertFalse("all actions should have been completed regardless", gate.hasActionsWaiting());
        
        int suppressed = 0;
        
        for (;;) {
            Throwable[] s = caught.getSuppressed();
            
            if (s.length == 0) {
                assertEquals("we shouldn't have lost any exceptions", count - 1, suppressed);
                break;
            } else {
                assertEquals("there should be only one", 1, s.length);
            }
            
            ++suppressed;
            caught = s[0];
        }
    }
    
    @Test(timeout = TIMEOUT)
    public void testWeakBlocking() {
        testing();
        
        Gate.Consumer gate = new Gate.Consumer();
        
        int count = 5;
        
        Thread[] threads = new Thread[count];
        
        for (int i = 0; i < count; ++i) {
            Thread t = new Thread(gate::block) {
                @Override
                protected void finalize() throws Throwable {
                    System.out.println(getName() + " finalized");
                    super.finalize();
                }
            };
            t.setName("testWeakBlocking.Thread" + i);
            threads[i] = t;
            t.start();
        }
        
        assertTrue("gate should be blocked", gate.isBlocked());
        
        AtomicBoolean flag = new AtomicBoolean(false);
        gate.accept(() -> flag.set(true));
        
        assertTrue("gate should be blocked", gate.hasActionsWaiting());
        Arrays.fill(threads, null);
        
        Thread current = Thread.currentThread();
        
        new Thread(() -> {
            for (;;) {
                switch (current.getState()) {
                case WAITING:
                case TIMED_WAITING:
                    System.gc();
                    System.out.println("    System.gc(); invoked");
                    return;
                default:
                    Misc.sleep(50);
                    break;
                }
            }
        }).start();
        
        try {
            long start = System.currentTimeMillis();
            int  max   = 10_000;
            
            // note: this can fail if the system is simply slow to collect
            assertTrue("threads should be garbage collected; should be unblocked",
                       gate.await(max));
            
            long elapsed = System.currentTimeMillis() - start;
            System.out.printf("    waited for %dms%n", elapsed);
            
            assertTrue("queue monitor should have notified", elapsed < max);
            
        } catch (InterruptedException x) {
            fail(x.toString());
        }
        
        assertTrue("await should clear the queue", flag.get());
    }
    
    @Test(timeout = TIMEOUT)
    public void testBlockAndUnblock() {
        testing();
        
        Gate gate = new Gate.Consumer();
        
        try {
            gate.unblock();
            fail("block/unblock must be in pairs");
        } catch (IllegalStateException x) {
        }
        
        try {
            testNesting(gate, 100);
        } catch (IllegalStateException x) {
            fail(x.toString());
        }
    }
    
    private void testNesting(Gate gate, int i) {
        if (i > 0) {
            try (Gate blocked = gate.block()) {
                testNesting(gate, i - 1);
            }
        }
    }
}