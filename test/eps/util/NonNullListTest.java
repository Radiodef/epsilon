/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.util.Literals.*;
import static eps.test.Tools.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class NonNullListTest {
    public NonNullListTest() {
    }
    
    @Test
    public void testConstructors() {
        testing();
        List<Object> list;
        
        list = new NonNullList<>();
        assertTrue("new list should be empty", list.isEmpty());
        assertEquals("new list should be empty", 0, list.size());
        
        list = new NonNullList<>(LinkedHashSetOf("a", "b"));
        assertEquals(ListOf("a", "b"), list);
        try {
            list = new NonNullList<>((Set<?>) null);
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        try {
            list = new NonNullList<>(SetOf("a", null, "c"));
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        
        List<Object> del = new LinkedList<>();
        del.add("a");
        del.add("b");
        list = new NonNullList<>(del);
        del.add("c");
        assertEquals("list should actually wrap the delegate", ListOf("a", "b", "c"), del);
        
        try {
            list = new NonNullList<>((List<Object>) null);
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        try {
            list = new NonNullList<>(ListOf("x", null));
            fail(list.toString());
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testStaticFactories() {
        testing();
        
        List<?> list = NonNullList.empty();
        assertTrue("empty list should be empty", list.isEmpty());
        assertEquals("empty list should be empty", 0, list.size());
        
        assertTrue("multiple invocations should return multiple lists",
                   NonNullList.empty() != NonNullList.empty());
        
        List<Integer> del = new LinkedList<>();
        List<Integer> ints = NonNullList.of(del);
        
        del.add(0);
        assertEquals("result should wrap the argument", ListOf(0), ints);
    }
    
    @Test
    public void testSize() {
        testing();
        
        List<Object> list = new NonNullList<>();
        assertTrue(list.isEmpty());
        assertEquals(0, list.size());
        
        for (int i = 0; i < 10; ++i) {
            list.add(new Object());
            assertFalse(list.isEmpty());
            assertEquals(i + 1, list.size());
        }
        
        list.clear();
        assertTrue(list.isEmpty());
        assertEquals(0, list.size());
    }
    
    @Test
    public void testAdd() {
        testing();
        
        List<Object> list = new NonNullList<>();
        list.add("x");
        list.add("y");
        list.add("z");
        assertEquals(ListOf("x", "y", "z"), list);
        
        try {
            list.add(null);
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        
        list.add(0, "c");
        list.add(0, "b");
        list.add(0, "a");
        assertEquals(ListOf("a", "b", "c", "x", "y", "z"), list);
        
        try {
            list.add(0, null);
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        
        try {
            list.add(-1, "xyz");
            fail(list.toString());
        } catch (IndexOutOfBoundsException x) {
        }
        
        try {
            list.add(-1, null);
            fail(list.toString());
        } catch (IndexOutOfBoundsException x) {
            fail(x.toString());
        } catch (NullPointerException x) {
        }
        
        try {
            list.add(list.size(), "");
        } catch (IndexOutOfBoundsException x) {
            fail(x.toString());
        }
        
        assertEquals("", list.get(list.size() - 1));
        assertEquals(7, list.size());
        
        try {
            list.add(list.size() + 1, "");
            fail(list.toString());
        } catch (IndexOutOfBoundsException x) {
        }
    }
    
    @Test
    public void testAddAll() {
        testing();
        
        List<Object> list = NonNullList.empty();
        Collections.addAll(list, 1, 2, 3);
        
        List<Integer> oneToSix = ListOf(1, 2, 3, 4, 5, 6);
        
        assertTrue(list.addAll(LinkedHashSetOf(4, 5, 6)));
        assertEquals(oneToSix, list);
        
        assertFalse(list.addAll(Collections.emptyList()));
        assertEquals(oneToSix, list);
        
        try {
            list.addAll(null);
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        try {
            list.addAll(ListOf(7, null));
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        
        assertEquals("list should be unchanged", oneToSix, list);
        
        assertTrue(list.addAll(oneToSix));
        assertEquals(ListOf(1, 2, 3, 4, 5, 6), oneToSix);
    }
    
    @Test
    public void testAddAllAtIndex() {
        testing();
        
        List<Object> list = NonNullList.empty();
        Collections.addAll(list, 1, 2, 3);
        
        list.addAll(0, ListOf(4, 5, 6));
        assertEquals(ListOf(4, 5, 6, 1, 2, 3), list);
        
        list.addAll(3, LinkedHashSetOf(7, 8, 9));
        assertEquals(ListOf(4, 5, 6, 7, 8, 9, 1, 2, 3), list);
        
        list.clear();
        Collections.addAll(list, 1, 2, 3);
        
        try {
            list.addAll(0, ListOf(2, null));
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        try {
            list.addAll(0, null);
            fail(list.toString());
        } catch (NullPointerException x) {
        }
        
        assertEquals(ListOf(1, 2, 3), list);
        
        try {
            list.addAll(-1, ListOf(0));
            fail(list.toString());
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            list.addAll(list.size() + 1, ListOf(0));
            fail(list.toString());
        } catch (IndexOutOfBoundsException x) {
        }
        
        try {
            list.addAll(list.size(), ListOf(0));
        } catch (IndexOutOfBoundsException x) {
            fail(x.toString());
        }
        
        assertEquals(ListOf(1, 2, 3, 0), list);
    }
}