/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;

import java.math.*;

import org.junit.*;
import static org.junit.Assert.*;

public class CounterTest {
    public CounterTest() {
    }
    
    private static BigInteger big(long val) {
        return BigInteger.valueOf(val);
    }
    
    @Test
    public void testConstructors() {
        testing();
        
        Counter c;
        
        c = new Counter();
        assertTrue("default ctor should be 0", c.isZero());
        assertFalse("default ctor should be 0", c.isBig());
        assertEquals("default ctor should be 0", BigInteger.ZERO, c.get());
        
        c = new Counter(0);
        assertTrue(c.isZero());
        assertFalse(c.isBig());
        assertEquals(BigInteger.ZERO, c.get());
        
        c = new Counter(Long.MAX_VALUE);
        assertFalse(c.isZero());
        assertFalse(c.isBig());
        assertEquals(big(Long.MAX_VALUE), c.get());
        
        c = new Counter(BigInteger.ZERO);
        assertTrue(c.isZero());
        assertFalse(c.isBig());
        assertEquals(BigInteger.ZERO, c.get());
        
        c = new Counter(big(Short.MAX_VALUE));
        assertFalse(c.isZero());
        assertFalse(c.isBig());
        assertEquals(big(Short.MAX_VALUE), c.get());
        
        BigInteger two96 = new BigDecimal(Math.pow(2, 96)).toBigIntegerExact();
        
        c = new Counter(two96);
        assertFalse(c.isZero());
        assertTrue(c.isBig());
        assertEquals(two96, c.get());
        
        try {
            c = new Counter(-1);
            fail("constructor accepted -1");
        } catch (IllegalArgumentException x) {
        }
        
        try {
            c = new Counter(BigInteger.ONE.negate());
            fail("constructor accepted -1");
        } catch (IllegalArgumentException x) {
        }
        
        try {
            c = new Counter(null);
            fail("constructor accepted null");
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testIncrementAndDecrement() {
        testing();
        
        Counter c = new Counter();
        
        // basic test of long increment and decrement
        
        for (int i = 0; i < 100; ++i) {
            assertFalse(c.isBig());
            assertEquals(i == 0, c.isZero());
            assertEquals(BigInteger.valueOf(i), c.get());
            c.increment();
        }
        
        for (int i = 100 - 1; i >= 0; --i) {
            c.decrement();
            assertEquals(BigInteger.valueOf(i), c.get());
            assertEquals(i == 0, c.isZero());
            assertFalse(c.isBig());
        }
        
        assertTrue(c.isZero());
        try {
            c.decrement();
            fail();
        } catch (IllegalStateException x) {
        }
        
        // testing boundaries between long and BigInteger
        
        c = new Counter(Long.MAX_VALUE - 1);
        assertFalse(c.isBig());
        assertEquals(big(Long.MAX_VALUE - 1), c.get());
        
        c.increment();
        assertFalse(c.isBig());
        assertEquals(big(Long.MAX_VALUE), c.get());
        
        c.increment();
        assertTrue(c.isBig());
        assertEquals(big(Long.MAX_VALUE).add(BigInteger.ONE), c.get());
        
        c.increment();
        assertTrue(c.isBig());
        assertEquals(big(Long.MAX_VALUE).add(BigInteger.ONE).add(BigInteger.ONE), c.get());
        
        c.decrement();
        assertTrue(c.isBig());
        assertEquals(big(Long.MAX_VALUE).add(BigInteger.ONE), c.get());
        
        c.decrement();
        assertFalse(c.isBig());
        assertEquals(big(Long.MAX_VALUE), c.get());
        
        c.decrement();
        assertFalse(c.isBig());
        assertEquals(big(Long.MAX_VALUE - 1), c.get());
        
        c.decrement();
        assertFalse(c.isBig());
        assertEquals(big(Long.MAX_VALUE - 2), c.get());
        
        // basic test of BigInteger increment and decrement
        
        BigInteger big = big(Long.MAX_VALUE);
        c = new Counter(big);
        
        for (int i = 0; i < 100; ++i) {
            assertEquals(big, c.get());
            big = big.add(BigInteger.ONE);
            c.increment();
        }
        
        for (int i = 0; i < 100; ++i) {
            big = big.subtract(BigInteger.ONE);
            c.decrement();
            assertEquals(big, c.get());
        }
    }
}