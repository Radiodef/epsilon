/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;
import static eps.test.Sorting.*;

import java.util.*;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;
import static org.junit.Assert.*;
//import static org.junit.Assume.*;

@RunWith(Parameterized.class)
public class ListDequeTest {
    private static final int STRING_LENGTH = 8;
    private static final int S_SIZE = 100;
    private static final int X_SIZE = 1_000;
    
    private final boolean isFirst;
    private final int     initialCap;
    private final int     size;
    private final boolean isSmall;
    private final boolean isExtreme;
    private final Random  rand;
    
    public ListDequeTest(boolean isFirst, int initialCap, int size) {
        if (size % 2 != 0) {
            throw new IllegalArgumentException(Integer.toString(size));
        }
        this.isFirst    = isFirst;
        this.initialCap = initialCap;
        this.size       = size;
        this.isSmall    = size <= S_SIZE;
        this.isExtreme  = size > X_SIZE;
        this.rand       = new Random();
    }
    
    @Parameters(name = "{index}: initialCap = {0}, size = {1}")
    public static Object[][] params() {
        return new Object[][] {
            { true,        0,     100 },
            { false,       0,   1_000 },
            { false,      10,   1_000 },
            { false,       0,  10_000 },
            { false,   1_000,  10_000 },
            { false,       0, 100_000 },
            { false, 100_000, 100_000 },
        };
    }
    
    @Test
    public void testBasicListFunctionality() {
        testing();
        
        int     initialCap = this.initialCap;
        int     size       = this.size;
        Random  rand       = this.rand;
        boolean isExtreme  = this.isExtreme;
        
        ListDeque<String> list = new ListDeque<>(initialCap);
        List<String> expect = new ArrayList<>(initialCap);
        
        assertTrue("new list should be empty", list.isEmpty());
        assertEquals("new list should be empty", 0, list.size());
        assertEquals("list should be constructed with initialCap", initialCap, list.capacity());
        
        try {
            String s = list.getFirst();
            fail("new list should be empty: " + s);
        } catch (NoSuchElementException x){
        }
        try {
            String s = list.getLast();
            fail("new list should be empty: " + s);
        } catch (NoSuchElementException x){
        }
        
        for (int i = -1; i <= 1; ++i) {
            try {
                list.get(i);
                fail("new list should be empty; index = " + i);
            } catch (IndexOutOfBoundsException x) {
            }
        }
        
        for (int i = 0; i < size; ++i) {
            String str = randomString(rand, STRING_LENGTH);
            list.add(str);
            expect.add(str);
            
            assertFalse("list should not be empty", list.isEmpty());
            assertEquals("list size should have increased", i + 1, list.size());
            
            if (i < X_SIZE || !isExtreme) {
                assertTrue("list should contain this element", list.contains(str));
            }
            
            assertEquals("list should contain this element", i, list.lastIndexOf(str));
            assertEquals("list should contain this element", str, list.get(i));
            assertEquals("last should be this element", str, list.getLast());
        }
        
        int capacity = list.capacity();
        
        assertTrue("list should have capacity at least as large as its size",
                   list.capacity() >= list.size());
        
        assertEquals("consistency", expect, list);
        
        for (int i = 0; i < size; ++i) {
            int last = size - i - 1;
            
            list.remove(last);
            assertEquals("list size should have decreased", last, list.size());
        }
        
        assertTrue("list should now be empty", list.isEmpty());
        assertEquals("list should now be empty", 0, list.size());
        
        for (int i = 0; i < size; ++i) {
            String str = expect.get(i);
            list.addFirst(str);
            assertEquals("first should be this element", str, list.getFirst());
        }
        
        assertEquals("the list should not have been resized", capacity, list.capacity());
        
        Collections.reverse(expect);
        assertEquals("the list should now be in reverse", expect, list);
        
        if (!isExtreme) {
            list.sort(Comparator.naturalOrder());
            assertThat("sorting should work via get/set", list, isSorted());
        }
    }
    
    @Test
    public void testRemove() {
        testing();
        
        int size = this.size;
        
        ListDeque<Integer> list = new ListDeque<>(initialCap);
        
        int div = 4;
        for (int i = 0; i < div; ++i) {
            int count = size / div;
            for (int j = 0; j < count; ++j) {
                list.add(j);
            }
        }
        
        Collections.shuffle(list);
        
        for (int i = 0; i < div; ++i) {
            list.removeFirstOccurrence(0);
        }
        
        assertFalse("removed all 0s", list.contains(0));
    }
    
    @Test
    public void testBasicQueueFunctionality() {
        testing();
        
        ListDeque<Integer> queue = new ListDeque<>(initialCap);
        
        int size = this.size;
        for (int i = 0; i < size; ++i) {
            assertTrue("offer should never fail", queue.offerLast(i));
            assertEquals("last should be i", i, queue.peekLast().intValue());
        }
        
        assertEquals("queue size", size, queue.size());
        
        List<Integer> copy = new ArrayList<>(queue);
        assertEquals("copy should be identical", queue, copy);
        assertEquals("copy should be identical", queue.hashCode(), copy.hashCode());
        
        // just shuffle elements from the back to the front circularly
        // so the queue ends up in the same arrangement as before
        
        for (int i = size - 1; i >= 0; --i) {
            Integer last;
            switch (i % 2) {
                case 0: last = queue.pollLast();
                        assertNotNull("last should be i", last);
                        break;
                case 1: last = queue.removeLast();
                        break;
                default:
                        fail(Integer.toString(i));
                        return;
            }
            assertEquals("last should be i", i, last.intValue());
            assertTrue("offer should never fail", queue.offerFirst(last));
        }
        
        assertEquals("copy should still be identical", queue, copy);
        
        assert (size & 1) == 0 : size;
        int half = size / 2;
        
        // interleave test
        for (int i = 0; i < half; ++i) {
            // first
            Integer first = queue.pollFirst();
            assertNotNull("there should be a first element", first);
            assertEquals("first should be i", i, first.intValue());
            // last
            Integer last = queue.pollLast();
            assertNotNull("there should be a last element", last);
            assertEquals("last should be (size - i - 1)", size - i - 1, last.intValue());
        }
        
        assertTrue("queue should be empty", queue.isEmpty());
        
        assertNull("queue should be empty", queue.pollFirst());
        assertNull("queue should be empty", queue.pollLast());
    }
    
    @Test
    public void testBasicStackFunctionality() {
        testing();
        
        ListDeque<Integer> stack = new ListDeque<>(initialCap);
        
        int size = this.size;
        
        for (int i = 0; i < size; ++i) {
            stack.push(i);
            assertEquals("the top should be i", i, stack.peek().intValue());
        }
        
        assertEquals("stack size", size, stack.size());
        
        for (int i = size - 1; i >= -1; --i) {
            assertEquals("stack size", i == -1, stack.isEmpty());
            if (i == -1)
                break;
            assertEquals("the top should be i", i, stack.pop().intValue());
        }
        
        for (int i = 0; i < 10; ++i) {
            assertTrue("stack size", stack.isEmpty());
            stack.push(i);
            assertEquals("the top should be i", i, stack.pop().intValue());
        }
        
        try {
            Object v = stack.pop();
            fail("empty stack: " + v);
        } catch (NoSuchElementException x) {
        }
    }
    
    @Test
    public void testBasicIteratorFunctionality() {
        testing();
        
        ListDeque<Integer> list = new ListDeque<>(initialCap);
        
        int size = this.size;
        
        // result:
        // ..., -7, -5, -3, -1, 0, 2, 4, 6, ...
        
        for (int i = 0; i < size; ++i) {
            if ((i & 1) == 0) {
                list.addLast(i);
            } else {
                list.addFirst(-i);
            }
        }
        
        assertThat(list, isSorted());
        
        // test ascending iteration
        
        Iterator<Integer> ascend = list.iterator();
        
        int half = size / 2;
        
        for (int i = 0; i <= size; ++i) {
            assertEquals(i < size, ascend.hasNext());
            if (!ascend.hasNext())
                break;
            Integer next = ascend.next();
            assertEquals("odd should be negative", (next & 1) == 1, next < 0);
            assertEquals("first half should be odd/negative", i < half, next < 0);
        }
        
        // test descending iteration
        
        ListDeque<Integer> reverse = new ListDeque<>(initialCap);
        for (Integer val : list) {
            reverse.addFirst(val);
        }
        
        Iterator<Integer> descend = list.descendingIterator();
        ListIterator<Integer> lit = list.listIterator(list.size());
        
        for (Integer val : reverse) {
            assertTrue(descend.hasNext());
            assertTrue(lit.hasPrevious());
            
            assertEquals(val, descend.next());
            assertEquals(val, lit.previous());
        }
        
        // reorder the list
        
        lit = list.listIterator();
        
        for (int i = 0; i < size; ++i) {
            lit.next();
            lit.set((i & 1) == 0 ? i : -1);
        }
        
        for (int i = 0; i < size; ++i) {
            assertEquals("iterator should modify the list", (i & 1) == 0 ? i : -1, list.get(i).intValue());
        }
        
        // test some removal
        
        ascend = list.iterator();
        
        try {
            ascend.remove();
            fail("remove() with no next()");
        } catch (IllegalStateException x) {
        }
        
        for (int i = 0; i < size; ++i) {
            assertTrue(ascend.hasNext());
            ascend.next();
            
            if ((i & 1) == 1) {
                ascend.remove();
                
                if ((i % 100) == 0) {
                    try {
                        ascend.remove();
                        fail("consecutive remove()");
                    } catch (IllegalStateException x) {
                    }
                }
            }
        }
        
        assertEquals("half of the elements should be removed", half, list.size());
        
        for (Integer val : list) {
            assertTrue("all negative numbers should be removed", val >= 0);
            assertTrue("all remaining elements should be even", (val & 1) == 0);
        }
    }
}