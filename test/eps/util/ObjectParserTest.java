/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.json.*;
import eps.util.ObjectParser.*;
import static eps.test.Tools.*;

import java.util.*;
import static java.util.Arrays.asList;

import org.junit.*;
import static org.junit.Assert.*;

public class ObjectParserTest {
    public ObjectParserTest() {
    }
    
    @Test
    public void testSuperclass() {
        try {
            ObjectParser<Object> p = new ObjectParser<Object>(null) {
                @Override
                public Object parse(String s) {
                    throw new AssertionError(s);
                }
            };
            fail(p.toString());
        } catch (NullPointerException x) {
        }
        
        try {
            ObjectParser<Integer> p = new ObjectParser<Integer>(int.class) {
                @Override
                public Integer parse(String s) {
                    throw new AssertionError(s);
                }
            };
            fail(p.toString());
        } catch (IllegalArgumentException x) {
        }
        
        ObjectParser<String> p = new ObjectParser<String>(String.class) {
            @Override
            public String parse(String s) {
                return s;
            }
        };
        assertEquals(String.class, p.getType());
    }
    
    @Test
    public void testSimpleParser() {
        testing();
        
        assertEquals(-1, ObjectParser.ofSimple(Integer.class).parse("-1").intValue());
        
        ObjectParser<Integer> decode = ObjectParser.ofSimple(Integer.class, "decode");
        assertEquals(Integer.class, decode.getType());
        
        for (String str : asList("0",
                                 "1",
                                 "-1",
                                 "123456",
                                 "-0xFab",
                                 null,
                                 "null")) {
            Integer a;
            try {
                a = Integer.decode(str);
                
            } catch (NullPointerException x) {
                Integer b;
                try {
                    b = decode.parse(str);
                } catch (NullPointerException y) {
                    continue;
                }
                fail(String.valueOf(b));
                throw new AssertionError(b);
                
            } catch (RuntimeException x) {
                Integer b;
                try {
                    b = decode.parse(str);
                } catch (BadStringException y) {
                    assertEquals(x.getClass(), y.getCause().getClass());
                    assertEquals(x.getMessage(), y.getCause().getMessage());
                    continue;
                }
                fail(String.valueOf(b));
                throw new AssertionError(b);
            }
            
            Integer b = decode.parse(str);
            assertEquals(str, a, b);
        }
        
        try {
            Integer n = decode.parse(null);
            fail(String.valueOf(n));
        } catch (NullPointerException x) {
        }
        
        try {
            Integer n = decode.nullable().parse(null);
            assertNull(n);
        } catch (NullPointerException x) {
            fail(x.toString());
        }
        try {
            Integer n = decode.nullable().parse("null");
            assertNull(n);
        } catch (RuntimeException x) {
            fail(x.toString());
        }
        
        ObjectParser<Integer> neg = decode.requiring(n -> n < 0);
        assertEquals(-1, neg.parse("-1").intValue());
        
        for (String str : asList("0", "1", Integer.toString(Integer.MAX_VALUE))) {
            try {
                Integer n = neg.parse(str);
                fail(n.toString());
            } catch (BadStringException x) {
            }
        }
        
        try {
            ObjectParser<?> serial = (ObjectParser<?>) Json.thru(decode, false);
        } catch (RuntimeException x) {
            fail(x.toString());
        }
    }
    
    @Test
    public void testBooleanParser() {
        assertEquals(ObjectParser.ofBoolean(), ObjectParser.ofBoolean());
        
        ObjectParser<Boolean> p = ObjectParser.ofBoolean();
        
        for (String str : asList("true", "yes", "TrUE", "  yes")) {
            assertEquals(true, p.parse(str));
        }
        for (String str : asList("false", "no", "FAlsE", "  no")) {
            assertEquals(false, p.parse(str));
        }
        
        try {
            Boolean b = p.parse(null);
            fail(String.valueOf(b));
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testClassParser() {
        ObjectParser<Class<?>> p = ObjectParser.ofClass();
        
        class Pair {
            final String str;
            final Class<?> cls;
            Pair(String str, Class<?> cls) {
                this.str = str;
                this.cls = cls;
            }
        }
        
        class Local {
        }
        
        for (Pair pair : asList(new Pair("java.lang.Object", Object.class),
                                new Pair("java.util.Map$Entry", Map.Entry.class),
                                new Pair("java.lang.Class", Class.class),
                                new Pair("eps.util.ObjectParserTest$1Local", Local.class))) {
            assertEquals(pair.str, pair.cls, p.parse(pair.str));
        }
        
        try {
            Class<?> c = p.parse(null);
            fail(String.valueOf(c));
        } catch (NullPointerException x) {
        }
        
        try {
            Class<?> c = p.parse("int");
            fail(String.valueOf(c));
        } catch (BadStringException x) {
        }
        
        try {
            ObjectParser<?> serial = (ObjectParser<?>) Json.thru(p, false);
            try {
                assertEquals(StringBuilder.class, serial.parse("java.lang.StringBuilder"));
            } catch (RuntimeException x) {
                fail(x.toString());
            }
        } catch (RuntimeException x) {
            fail(x.toString());
        }
        
        try {
            Class<?> c = ObjectParser.ofSubtypesOf(Number.class).parse("java.lang.String");
            fail(String.valueOf(c));
        } catch (BadStringException x) {
        }
    }
    
    @Test
    public void testCollectionParser() {
        ObjectParser<Integer> intParser = ObjectParser.ofSimple(Integer.class);
        
        ObjectParser<List<Integer>> p = intParser.asCollection(ArrayList::new);
        
        List<Integer> list;
        
        try {
            list = p.parse("[0, 1, 2]"); // TODO at some point
            fail(String.valueOf(list));
        } catch (BadStringException x) {
        }
        
        list = p.parse("[\"0\", \"1\", \"2\"]");
        assertEquals(asList(0, 1, 2), list);
        assertEquals(ArrayList.class, list.getClass());
        
        try {
            list = p.parse("[null, null]");
            fail(String.valueOf(list));
        } catch (BadStringException x) {
        }
        try {
            list = intParser.nullable().asCollection(ArrayList::new).parse("[null, null]");
            assertEquals(asList(null, null), list);
        } catch (NullPointerException x) {
            fail(x.toString());
        }
        
        try {
            list = p.parse("{\"val\": true}");
            fail(String.valueOf(list));
        } catch (BadStringException x) {
        }
    }
}