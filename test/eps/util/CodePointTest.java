/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.json.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

public class CodePointTest {
    public CodePointTest() {
    }
    
    @Test
    public void testCodePoint() {
        testing();
        
        CodePoint c = CodePoint.valueOf('A');
        assertEquals("A", c.toString());
        
        assertEquals(1, c.length());
        assertEquals('A', c.charAt(0));
        assertEquals('A', c.getAsInt());
        
        testOob(c, -1);
        testOob(c, 1);
        testOob(c, Short.MAX_VALUE);
        
        assertEquals(c, Json.thru(c, false));
        
        c = CodePoint.valueOf("𝄐".codePointAt(0));
        assertEquals("𝄐", c.toString());
        
        assertEquals(2, c.length());
        assertEquals(0x1D110, c.getAsInt());
        assertEquals("𝄐".charAt(0), c.charAt(0));
        assertEquals("𝄐".charAt(1), c.charAt(1));
        
        testOob(c, -1);
        testOob(c, 2);
        testOob(c, Short.MAX_VALUE);
        
        assertEquals(c, Json.thru(c, false));
        
        try {
            new CodePoint("abc");
            fail();
        } catch (IllegalArgumentException x) {
        }
        try {
            new CodePoint("x");
        } catch (IllegalArgumentException x) {
            fail(x.toString());
        }
    }
    
    private void testOob(CodePoint c, int i) {
        try {
            c.charAt(i);
            fail();
        } catch (IndexOutOfBoundsException x) {
        }
    }
}