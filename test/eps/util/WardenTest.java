/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;

import java.util.*;
import java.util.stream.*;

import org.junit.*;
import static org.junit.Assert.*;

public class WardenTest {
    public WardenTest() {
    }
    
    @Test(timeout = 10_000)
    public void test() {
        testing();
        Warden w = new Warden("TestWarden");
        
        class Task implements Runnable {
            final Warden.Permit p = w.acquire();
            @Override
            public void run() {
                Misc.sleep((int) (Math.random() * 1_000) + 1);
                p.close();
            }
        }
        
        int count = 1_000;
        
        Thread[] threads =
            Stream.generate(() -> new Task())
                  .limit(count)
                  .peek(t -> assertTrue(t.p.isValid()))
                  .map(Thread::new)
                  .toArray(Thread[]::new);
        
        assertEquals(count, w.getPermitCount());
        assertEquals(count, w.getValidPermitCount());
        assertTrue(w.hasPermits());
        assertTrue(w.hasValidPermits());
        
        Arrays.stream(threads).forEach(Thread::start);
        w.await();
        
        assertEquals(0, w.getPermitCount());
        assertEquals(0, w.getValidPermitCount());
        assertFalse(w.hasPermits());
        assertFalse(w.hasValidPermits());
        
        w.close();
        
        try (Warden.Permit p = w.acquire()) {
            assertFalse(p.isValid());
            assertEquals(1, w.getPermitCount());
            assertEquals(0, w.getValidPermitCount());
            assertTrue(w.hasPermits());
            assertFalse(w.hasValidPermits());
        }
    }
}