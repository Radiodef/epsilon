/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;

import java.util.*;
import java.util.function.*;
import java.util.Map.*;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.*;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class IntHashMapTest {
    public static final long TIMEOUT = 40_000;
    
    public static final class Arguments {
        final boolean isExtreme;
        final int     initialCap;
        final double  loadFactor;
        final int     maxSize;
        
        private Arguments(boolean isExtreme, int initialCap, double loadFactor, int maxSize) {
            this.isExtreme  = isExtreme;
            this.initialCap = initialCap;
            this.loadFactor = loadFactor;
            this.maxSize    = maxSize;
        }
        
        private static final ToString<Arguments> TO_STRING =
            ToString.builder(Arguments.class)
                    .add("isExtreme",  args -> args.isExtreme)
                    .add("initialCap", args -> args.initialCap)
                    .add("loadFactor", args -> args.loadFactor)
                    .add("maxSize",    args -> args.maxSize)
                    .build();
        
        @Override
        public String toString() {
            return TO_STRING.apply(this);
        }
        
        public String toString(int spaces) {
            return TO_STRING.apply(this, true, spaces);
        }
    }
    
    @Parameters
    public static Arguments[] arguments() {
        return new Arguments[] {
            new Arguments(false,     0,  0.75,     100),
            new Arguments(true,      0,  1.5,  100_000),
            new Arguments(false, 1_000, 10.0,    1_000),
            new Arguments(false,    10,  0.125,    100),
            new Arguments(false,     0, Double.POSITIVE_INFINITY,  1_000),
            new Arguments(true,      0, Double.POSITIVE_INFINITY, 10_000),
            // initially threw OOM, which kind of made sense.
            // the table always got resized because the new size after
            // an addition is ALWAYS larger than table.length * MIN_VALUE.
            // because of this, IntHashMap.MIN_LOAD_FACTOR was added
            // to prevent this from happening.
            new Arguments(false,     0, Double.MIN_VALUE,  1_000),
            new Arguments(true,      0, Double.MIN_VALUE, 10_000),
        };
    }
    
    private final Arguments args;
    
    private final boolean isExtreme;
    private final int     initialCap;
    private final double  loadFactor;
    private final int     maxSize;
    
    public IntHashMapTest(Arguments args) {
        this.args       = Objects.requireNonNull(args, "args");
        this.isExtreme  = args.isExtreme;
        this.initialCap = args.initialCap;
        this.loadFactor = args.loadFactor;
        this.maxSize    = args.maxSize;
    }
    
    @Test(timeout = TIMEOUT)
    public void testBasicFunctionality() {
        testing();
        
        // test empty constructor
        
        long startTime = System.currentTimeMillis();
        
        IntHashMap<String> map = new IntHashMap<>(this.initialCap, this.loadFactor);
        
        assertTrue("new map should be empty", map.isEmpty());
        assertEquals("new map should be empty", 0, map.size());
        
        int     maxSize   = this.maxSize;
        // calls to containsValue are very slow in extreme cases
        // so we don't do it all the time
        boolean isExtreme = this.isExtreme;
        int     xCutoff   = 1_000;
        
        // note: using inclusive 1...max loops because 0 == -0
        
        for (int i = 1; i <= maxSize; ++i) {
            assertFalse("new map should be empty", map.containsKeyAsInt(i));
        }
        
        // test initial put
        
        for (int i = 1; i <= maxSize; ++i) {
            String value = Integer.toString(i);
            
            assertEquals("map should not contain this", null, map.putAsInt(i, value));
            assertEquals("size should have increased", i, map.size());
            
            assertTrue("this entry should have been added", map.containsKeyAsInt(i));
            
            if (!isExtreme || i <= xCutoff) {
                assertTrue("this entry should have been added", map.containsValue(value));
            }
            
            assertEquals("this entry should have been added", value, map.getAsInt(i));
        }
        
        // readout
        printReadout("testBasicFunctionality", map);
        
        assertFalse("size should have increased", map.isEmpty());
        assertEquals("all entries should have been added", maxSize, map.size());
        
        // test replacement put
        
        for (int i = 1; i <= maxSize; ++i) {
            String value = Integer.toString(-i);
            String old   = Integer.toString(i);
            
            assertEquals("map should already contain this", old, map.putAsInt(i, value));
            assertEquals("size should not have increased", maxSize, map.size());
            
            if (!isExtreme || i <= xCutoff) {
                assertFalse("this entry should have been removed", map.containsValue(old));
            }
            
            assertTrue("this entry should have been added", map.containsKeyAsInt(i));
            
            if (!isExtreme || i <= xCutoff) {
                assertTrue("this entry should have been added", map.containsValue(value));
            }
            
            assertEquals("this entry should have been added", value, map.getAsInt(i));
        }
        
        assertFalse("size should not have decreased", map.isEmpty());
        assertEquals("size should not have increased", maxSize, map.size());
        
        // test removal
        
        for (int i = 1; i <= maxSize; ++i) {
            String value = Integer.toString(-i);
            
            assertEquals("map should have contained this", value, map.removeAsInt(i));
            assertEquals("size should have decreased", maxSize - i, map.size());
            
            assertFalse("entry should have been removed", map.containsKeyAsInt(i));
            
            if (!isExtreme || i <= xCutoff) {
                assertFalse("entry should have been removed", map.containsValue(value));
            }
            
            assertEquals("entry should have been removed", null, map.getAsInt(i));
        }
        
        assertTrue("all entries should have been removed", map.isEmpty());
        assertEquals("all entries should have been removed", 0, map.size());
        
        printElapsedSeconds(startTime);
    }
    
    @Test(timeout = TIMEOUT)
    public void testClear() {
        testing();
        
        IntHashMap<Object> map = new IntHashMap<>(this.initialCap, this.loadFactor);
        assertTrue("new map should be empty", map.isEmpty());
        
        map.clear();
        assertTrue("new map should be empty", map.isEmpty());
        
        Object val = new Object();
        
        int maxSize = this.maxSize;
        
        for (int i = 0; i < maxSize; ++i) {
            assertEquals("new map should be empty", null, map.putAsInt(i, val));
        }
        
        assertEquals("map should not be empty", maxSize, map.size());
        
        map.clear();
        assertTrue("cleared map should be empty", map.isEmpty());
        assertFalse("cleared map should be empty", map.containsValue(val));
        
        for (int i = 0; i < maxSize; ++i) {
            assertFalse("cleared map should be empty", map.containsKeyAsInt(i));
            assertEquals("cleared map should be empty", null, map.putAsInt(i, val));
            assertEquals("map should now contain this", val, map.getAsInt(i));
        }
        
        assertEquals("all entries should have been added", maxSize, map.size());
    }
    
    @Test(timeout = TIMEOUT)
    public void testKeySetValuesAndEntrySet() {
        testing();
        
        // not a huge deal since really only the entrySet() iterator() is
        // custom code
        // everything else is implemented by AbstractSet/AbstractMap
        
        long startTime = System.currentTimeMillis();
        
        HashMap<Integer, String> expect = new HashMap<>();
        IntHashMap<String>       actual = new IntHashMap<>(this.initialCap, this.loadFactor);
        
        Random rand = new Random();
        
        fillRandomMaps(expect, actual, rand, 8);
        assertTrue("actual size", (actual.size() % 2) == 0);
        
        // readout
        printReadout("testKeySetValuesAndEntrySet", actual);
        
        // test keySet()
        
        testCollections("keySet()", expect, actual, Map::keySet, key -> {
            assertTrue("map should contain all keys", actual.containsKey(key));
        });
        
        // test values()
        
        // containsValue(...) just too slow in extreme cases
        Predicate<String> containsValue =
            (this.isExtreme && this.maxSize > 1_000) ? new HashSet<>(actual.values())::contains
                                                     : actual::containsValue;
        
        testCollections("values()", expect, actual, Map::values, val -> {
            assertTrue("map should contain all values", containsValue.test(val));
        });
        
        // test entrySet()
        
        testCollections("entrySet()", expect, actual, Map::entrySet, entry -> {
            Integer key = entry.getKey();
            
            assertEquals("entries should be the same", entry.getValue(), actual.get(entry.getKey()));
        });
        
        // test mutations
        
        int size = actual.size();
        int half = size / 2;
        assertTrue("actual size", (size % 2) == 0);
        
        Set<Integer> keySet = actual.keySet();
        assertEquals("keySet size", 2 * half, keySet.size());
        
        try {
            keySet.add(-1);
            fail("additions should not be supported");
        } catch (UnsupportedOperationException x) {
        }
        
        // remove half of the entries by way of the keySet()
        
        Iterator<Integer> expectKeys = expect.keySet().iterator();
        for (int i = 0; i < half; ++i) {
            Integer key = expectKeys.next();
            expectKeys.remove();
            
            assertTrue("key sets should be the same; remove should be supported", keySet.remove(key));
            assertFalse("remove should be successful", keySet.contains(key));
            
            assertFalse("mutations should show through", actual.containsKey(key));
            assertEquals("mutations should show through", size - i - 1, actual.size());
        }
        
        assertEquals("there should be exactly 1/2 entries at this point", half, actual.size());
        
        // remove the other half by way of the entrySet() iterator
        
        Set<Entry<Integer, String>> entrySet = actual.entrySet();
        
        Iterator<Entry<Integer, String>> actualEntries = entrySet.iterator();
        for (int i = 0;; ++i) {
            assertEquals("there should be exactly 1/2 entries", actualEntries.hasNext(), i < half);
            if (i == half)
                break;
            
            Entry<Integer, String> entry = actualEntries.next();
            actualEntries.remove();
            
            assertFalse("mutations should show through", actual.containsKey(entry.getKey()));
            assertNotEquals("mutations should show through", entry.getValue(), actual.get(entry.getKey()));
        }
        
        assertTrue("map should be empty at this point", actual.isEmpty());
        
        printElapsedSeconds(startTime);
    }
    
    private void fillRandomMaps(Map<Integer, String> expect,
                                Map<Integer, String> actual,
                                Random rand,
                                int len) {
        int maxSize = this.maxSize;
        if ((maxSize & 1) == 1) ++maxSize; // we depend on divisibility by 2 later
        
        assertEquals("maps should be identical", expect, actual);
        assertEquals("maps should be identical", expect.size(), actual.size());
        expect.clear();
        actual.clear();
        assertEquals("maps should be identical", expect, actual);
        assertEquals("maps should be identical", expect.size(), actual.size());
        
        int size0 = maxSize;
        int half  = maxSize / 2;
        
        boolean isExtreme = this.isExtreme;
        
        IntPredicate forWhile =
            i -> (i < size0) || (expect.size() < half) || ((expect.size() & 1) == 1);
        
        for (int i = 0; forWhile.test(i); ++i) {
            Integer key = rand.nextInt();
            String  val = randomString(rand, len);
            
            assertEquals("maps should be identical", expect.put(key, val), actual.put(key, val));
            
            if (!isExtreme || actual.size() <= 1_000) {
                // this is seriously slow in certain extreme cases
                assertEquals("maps should be identical", expect, actual);
                // this one not so much, but we don't do it anyway (avoid n^2)
                assertEquals("maps should be identical", expect.hashCode(), actual.hashCode());
            }
            
            assertEquals("maps should be identical", expect.size(), actual.size());
        }
        
        // mainly for the extreme cases, because we skip the
        // entry-by-entry equals comparisons (see loop above)
        assertEquals("maps should be identical", expect, actual);
        assertEquals("maps should be identical", expect.hashCode(), actual.hashCode());
    }
    
    private boolean shouldTestEqualsAndHashCode(Collection<?> c) {
        return c instanceof List<?> || c instanceof Set<?> || c instanceof Map<?, ?>;
    }
    
    private <T, U> void testCollections(String name,
                                        Map<Integer, T> expectMap,
                                        Map<Integer, T> actualMap,
                                        Function<Map<Integer, T>, Collection<U>> cFunc,
                                        Consumer<U> action) {
        Collection<U> expect = cFunc.apply(expectMap);
        Collection<U> actual = cFunc.apply(actualMap);
        
        assertEquals(name + " should have the same size", expect.size(), actual.size());
        assertEquals("map and " + name + " should have the same size", actualMap.size(), actual.size());
        
        if (shouldTestEqualsAndHashCode(actual)) {
            assertEquals(name + " should be identical", expect, actual);
            assertEquals(name + " should be identical", expect.hashCode(), actual.hashCode());
            assertEquals(name + " should be identical across invocations", actual, cFunc.apply(actualMap));
        }
        
        Collection<U> expectForLater = expect;
        Collection<U> actualForLater = actual;
        
        if (this.isExtreme && "values()".equals(name)) {
            // containsAll is just too slow in extreme cases.
            // this way we do at least test the contents.
            expect = new LinkedHashSet<>(expect);
            actual = new LinkedHashSet<>(actual);
        }
        
        assertTrue(name + " should be identical", expect.containsAll(actual));
        assertTrue(name + " should be identical", actual.containsAll(expect));
        assertTrue(name + " should be identical across invocations", actual.containsAll(cFunc.apply(actualMap)));
        
        expect = expectForLater;
        actual = actualForLater;
        
        testIterators(name, expect, actual, action);
    }
    
    private <T> void testIterators(String name, Iterable<T> expect, Iterable<T> actual, Consumer<T> action) {
        Iterator<T> expIt = expect.iterator();
        Iterator<T> actIt = actual.iterator();
        
        String hasNextMessage = name + " iterators should have the same count";
        
        for (;;) {
            assertEquals(hasNextMessage, expIt.hasNext(), actIt.hasNext());
            if (!actIt.hasNext())
                break;
            // note: elements may be different
            expIt.next();
            T next = actIt.next();
            action.accept(next);
        }
    }
    
    private void printReadout(String method, IntHashMap<?> map) {
        int    collisions = map.countCollisions();
        int    size       = map.size();
        double load       = (double) collisions / size;
        
        System.out.print("    ");
        System.out.print(method);
        System.out.println(":");
        
        System.out.println(args.toString(8));
        
        System.out.printf("        size       = %d%n", size);
        System.out.printf("        capacity   = %d%n", map.capacity());
        System.out.printf("        collisions = %d%n", collisions);
        System.out.printf("        load       = %f%n", load);
        
//        System.out.printf("    %s: args = %s, size = %d, capacity = %d, collisions = %d, load = %f%n",
//                           method, args,      size,  map.capacity(),    collisions,      load);
    }
    
    private void printElapsedSeconds(long startMillis) {
        long elapsedMillis = System.currentTimeMillis() - startMillis;
        System.out.printf("    ...%f seconds elapsed%n", (elapsedMillis / 1_000.0));
    }
}