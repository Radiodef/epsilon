/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;
import static eps.util.Literals.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
//import javax.script.*;

import org.junit.*;
import static org.junit.Assert.*;

public class ListDequeTestOnce {
    public ListDequeTestOnce() {
    }
    
    @Test
    public void testConstructors() {
        ListDeque<Object> list;
        
        list = new ListDeque<>();
        assertTrue("empty list", list.isEmpty());
        
        list = new ListDeque<>(10);
        assertTrue("empty list", list.isEmpty());
        
        try {
            list = new ListDeque<>(-1);
            fail("-1 should be illegal");
        } catch (IllegalArgumentException x) {
        }
    }
    
    @Test
    public void testEnsureCapacity() {
        ListDeque<Object> list = new ListDeque<>();
        
        assertTrue("capacity should increase", list.ensureCapacity(100));
        assertTrue("capacity should increase", list.capacity() >= 100);
        int cap = list.capacity();
        
        assertFalse("capacity should not increase", list.ensureCapacity(50));
        assertEquals("capacity should not increase", cap, list.capacity());
        
        assertFalse("this should do nothing", list.ensureCapacity(-100));
        
        list = new ListDeque<>();
        Collections.addAll(list, "abc", "def", "ghi");
        list.ensureCapacity(100);
        
        assertEquals("contents should not have changed", ListOf("abc", "def", "ghi"), list);
    }
    
    @Test
    public void testSize() {
        ListDeque<Object> list = new ListDeque<>();
        
        assertTrue("new list should be empty", list.isEmpty());
        assertEquals("new list should be empty", 0, list.size());
        
        list.add("X");
        assertEquals("size should increase", 1, list.size());
        list.remove(0);
        assertEquals("size should decrease", 0, list.size());
    }
    
    @Test
    public void testAdd() {
        // these should all do exactly the same thing
        testAddLast(ListDeque::add);
        testVoidAddLast(ListDeque::addLast);
        testAddLast(ListDeque::offer);
        testAddLast(ListDeque::offerLast);
        // these should all do exactly the same thing
        testAddVoidFirst(ListDeque::addFirst);
        testAddFirst(ListDeque::offerFirst);
        testAddVoidFirst(ListDeque::push);
    }
    
    private void testVoidAddLast(BiConsumer<ListDeque<Object>, Object> add) {
        testAddLast((list, e) -> { add.accept(list, e); return true; });
    }
    
    private void testAddLast(BiPredicate<ListDeque<Object>, Object> add) {
        testAdd(add, list -> list.size() - 1);
    }
    
    private void testAddVoidFirst(BiConsumer<ListDeque<Object>, Object> add) {
        testAddFirst((list, e) -> { add.accept(list, e); return true; });
    }
    
    private void testAddFirst(BiPredicate<ListDeque<Object>, Object> add) {
        testAdd(add, list -> 0);
    }
    
    private void testAdd(BiPredicate<ListDeque<Object>, Object> add, ToIntFunction<ListDeque<?>> indexFunc) {
        ListDeque<Object> list = new ListDeque<>();
        
        int count = 100;
        
        for (int i = 0; i < count; ++i) {
            assertTrue("this should always succeed", add.test(list, i));
            assertEquals("i should be this element", i, list.get(indexFunc.applyAsInt(list)));
        }
        for (int i = 0; i < count; ++i) {
            assertTrue("all elements should have been added", list.contains(i));
        }
        
        assertTrue("this should be fine", add.test(list, null));
    }
    
    @Test
    public void testAddAtIndex() {
        testing();
        ListDeque<Object> list = new ListDeque<>();
        list.add("abc");
        list.add("def");
        try {
            list.add(1, "ghi");
            fail("TODO: write tests for this if ever implemented");
        } catch (UnsupportedOperationException x) {
        }
    }
    
    @Test
    public void testPeek() {
        // these two should be the same
        testPeekFirst(ListDeque::peek);
        testPeekFirst(ListDeque::peekFirst);
        // 
        testPeekLast(ListDeque::peekLast);
    }
    
    private void testPeekFirst(Function<ListDeque<Object>, Object> peek) {
        testGet(ListDeque::addFirst, peek, true);
    }
    
    private void testPeekLast(Function<ListDeque<Object>, Object> peek) {
        testGet(ListDeque::addLast, peek, true);
    }
    
    @Test
    public void testGetFirstAndLast() {
        // these two should be the same
        testGetFirst(ListDeque::element);
        testGetFirst(ListDeque::getFirst);
        // 
        testGetLast(ListDeque::getLast);
    }
    
    private void testGetFirst(Function<ListDeque<Object>, Object> get) {
        testGet(ListDeque::addFirst, get, false);
    }
    
    private void testGetLast(Function<ListDeque<Object>, Object> get) {
        testGet(ListDeque::addLast, get, false);
    }
    
    private void testGet(BiConsumer<ListDeque<Object>, Object> add, Function<ListDeque<Object>, Object> get, boolean isPeek) {
        ListDeque<Object> list = new ListDeque<>();
        
        if (isPeek) {
            assertNull("peek should return null when there are no elements", get.apply(list));
        } else {
            try {
                get.apply(list);
                fail("empty list should throw");
            } catch (NoSuchElementException x) {
            }
        }
        
        int count = 100;
        
        for (int i = 0; i < count; ++i) {
            add.accept(list, i);
            assertEquals("i should be the last element", i, get.apply(list));
        }
    }
    
    @Test
    public void testSetAndGet() {
        ListDeque<Object> list = new ListDeque<>();
        
        int count = 100;
        
        for (int i = 0; i < count; ++i) {
            try {
                list.set(i, null);
                fail("this should be out of bounds; list is empty");
            } catch (IndexOutOfBoundsException x) {
            }
        }
        
        assertTrue(list.isEmpty());
        
        for (int i = 0; i < count; ++i) {
            list.add(null);
        }
        
        assertEquals(count, list.size());
        
        for (int i = 0; i < count; ++i) {
            assertNull("all should be null", list.get(i));
        }
        
        for (int i = 0; i < count; ++i) {
            assertNull("should return the previous value (null)", list.set(i, i));
            assertTrue("list now should contain i", list.contains(i));
        }
        
        for (int i = 0; i < count; ++i) {
            assertEquals("element at i should be i", i, list.get(i));
        }
        
        try {
            list.set(-1, "");
            fail("negative should be out of bounds");
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            list.set(list.size(), "");
            fail("size should be out of bounds");
        } catch (IndexOutOfBoundsException x) {
        }
        
        try {
            Object e = list.get(-1);
            fail("negative should be out of bounds: " + e);
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            Object e = list.get(list.size());
            fail("size should be out of bounds: " + e);
        } catch (IndexOutOfBoundsException x) {
        }
    }
    
    @Test
    public void testRemoveAtIndex() {
        ListDeque<Object> list = new ListDeque<>();
        
        try {
            Object e = list.remove(0);
            fail("empty list should throw. found: " + e);
        } catch (IndexOutOfBoundsException x) {
        }
        
        Collections.addAll(list, "abc", "def", "ghi");
        
        assertEquals("def", list.remove(1));
        assertFalse(list.contains("def"));
        assertEquals(ListOf("abc", "ghi"), list);
        
        assertEquals("ghi", list.remove(1));
        assertEquals("abc", list.remove(0));
        
        Collections.addAll(list, "abc", "def", "ghi");
        
        try {
            Object e = list.remove(-1);
            fail("negative index should throw. found: " + e);
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            Object e = list.remove(list.size());
            fail("size index should throw. found: " + e);
        } catch (IndexOutOfBoundsException x) {
        }
        
        // remove(int) causes the elements to be shifted,
        // so we want to do a bit more rigorous testing here
        
        int count = 1_000;
        
        // increasing: always remove(0)
        testRemoveAtIndex(count, IntStream.range(0, count).map(i -> 0));
        // decreasing: remove(size - 1)
        testRemoveAtIndex(count, IntStream.rangeClosed(1, count).map(i -> count - i));
        // interleaved: remove evens first, then odds with remove(0)
        testRemoveAtIndex(count, IntStream.range(0, count).map(i -> (i < (count / 2)) ? i : 0));
        // random
        testRemoveAtIndex(count, IntStream.range(0, count).map(i -> count - i).map(new Random()::nextInt));
    }
    
    private void testRemoveAtIndex(int count, IntStream indexes) {
        testRemoveAtIndex(count, indexes.iterator());
    }
    
    private void testRemoveAtIndex(int count, PrimitiveIterator.OfInt indexes) {
        List<Object>      expect = new ArrayList<>();
        ListDeque<Object> actual = new ListDeque<>();
        
        for (int i = 0; i < count; ++i) {
            expect.add(i);
            actual.add(i);
        }
        
        int size = count;
        
        while (indexes.hasNext()) {
            int    index   = indexes.nextInt();
            Object removed = actual.remove(index);
            --size;
            
            assertEquals("removed element should be the index", expect.remove(index), removed);
            assertFalse("list should not contain the removed element", actual.contains(removed));
            assertEquals("size should decrease", size, actual.size());
        }
        
        assertTrue("list should be empty at this point", actual.isEmpty());
    }
    
    @Test
    public void testRemoveFirstOccurrence() {
        ListDeque<Object> list = new ListDeque<>();
        Collections.addAll(list, "abc", "def", "ghi", "abc");
        
        assertTrue("abc should be removed", list.removeFirstOccurrence("abc"));
        assertEquals("first abc should be removed", ListOf("def", "ghi", "abc"), list);
        assertFalse("xyz is not in the list", list.removeFirstOccurrence("xyz"));
        assertFalse("this should be fine", list.removeFirstOccurrence(null));
    }
    
    @Test
    public void testRemoveLastOccurrence() {
        ListDeque<Object> list = new ListDeque<>();
        Collections.addAll(list, "abc", "def", "ghi", "abc");
        
        assertTrue("abc should be removed", list.removeLastOccurrence("abc"));
        assertEquals("last abc should be removed", ListOf("abc", "def", "ghi"), list);
        assertFalse("xyz is not in the list", list.removeLastOccurrence("xyz"));
        assertFalse("this should be fine", list.removeLastOccurrence(null));
    }
    
    @Test
    public void testRemove() {
        // these 3 should be the same
        testRemoveFirst(ListDeque::pop, false);
        testRemoveFirst(ListDeque::remove, false);
        testRemoveFirst(ListDeque::removeFirst, false);
        
        testRemoveLast(ListDeque::removeLast, false);
    }
    
    @Test
    public void testPoll() {
        // these 2 should be the same
        testRemoveFirst(ListDeque::poll, true);
        testRemoveFirst(ListDeque::pollFirst, true);
        
        testRemoveLast(ListDeque::pollLast, true);
    }
    
    private void testRemoveFirst(Function<ListDeque<Object>, Object> remove, boolean isPoll) {
        testRemove(ListDeque::addFirst, remove, isPoll);
    }
    
    private void testRemoveLast(Function<ListDeque<Object>, Object> remove, boolean isPoll) {
        testRemove(ListDeque::addLast, remove, isPoll);
    }
    
    private void testRemove(BiConsumer<ListDeque<Object>, Object> add, Function<ListDeque<Object>, Object> remove, boolean isPoll) {
        ListDeque<Object> list = new ListDeque<>();
        
        if (isPoll) {
            assertNull("empty list should return null", remove.apply(list));
        } else {
            try {
                Object e = remove.apply(list);
                fail("empty list should throw. found: " + e);
            } catch (NoSuchElementException x) {
            }
        }
        
        int count = 10;
        
        for (int i = 0; i < count; ++i) {
            add.accept(list, i);
        }
        
        for (int i = count - 1; i >= 0; --i) {
            assertEquals("element should be i", i, remove.apply(list));
        }
        
        assertTrue("list should now be empty", list.isEmpty());
        
        for (int i = 0; i < count; ++i) {
            add.accept(list, i);
            assertEquals("element should be i", i, remove.apply(list));
        }
        
        assertTrue("list should now be empty", list.isEmpty());
        
        if (isPoll) {
            assertNull("empty list should return null", remove.apply(list));
        } else {
            try {
                Object e = remove.apply(list);
                fail("empty list should throw. found: " + e);
            } catch (NoSuchElementException x) {
            }
        }
    }
    
    @Test
    public void testDescendingIterator() {
        ListDeque<Object> list = new ListDeque<>();
        
        int count = 100;
        for (int i = 0; i < count; ++i) {
            list.add(i);
        }
        
        Iterator<Object> it = list.descendingIterator();
        for (int i = count - 1;; --i) {
            assertEquals(i >= 0, it.hasNext());
            if (!it.hasNext())
                break;
            assertEquals(i, it.next());
        }
        
        it = list.descendingIterator();
        
        try {
            it.remove();
            fail("remove() before next()");
        } catch (IllegalStateException x) {
        }
        
        List<Object> copy = new ArrayList<>(list);
        
        while (it.hasNext()) {
            Object next = it.next();
            Object last = copy.remove(copy.size() - 1);
            assertEquals("next() should return the last element", last, next);
            it.remove();
            assertEquals("remove should remove the last element", copy, list);
        }
        
        try {
            it.remove();
            fail("consecutive remove()");
        } catch (IllegalStateException x) {
        }
        
        assertTrue("all elements should be removed", list.isEmpty());
    }
    
    @Test
    public void testStackWithStringReversal() {
        testing();
        
        Random rand = new Random();
        
        String[] strings = new String[10];
        for (int i = 0; i < strings.length; ++i) {
            strings[i] = randomString(rand, 10 * (i + 1));
        }
        
        ListDeque<Character> chars = new ListDeque<>();
        StringBuilder buf1 = new StringBuilder();
        StringBuilder buf2 = new StringBuilder();
        
        for (String str : strings) {
            System.out.print("    ");
            System.out.println(str);
            
            assertTrue("chars should be empty already", chars.isEmpty());
            
            for (int i = 0; i < str.length(); ++i) {
                Character c = str.charAt(i);
                chars.push(c);
                assertEquals(c, chars.peek());
            }
            
            buf1.setLength(0);
            while (!chars.isEmpty()) {
                buf1.append(chars.pop());
            }
            
            buf2.setLength(0);
            buf2.append(str).reverse();
            
            assertEquals("string should be reversed", buf1.toString(), buf2.toString());
        }
    }
    
    @Test
    public void testStackWithInfixEvaluation() {
        testing();
        
        eval("2+2", 2+2);
        eval("9-2", 9-2);
        eval("6*6", 6*6);
        eval("8/4", 8/4);
        
        eval("1+2+3+4+5", 1+2+3+4+5);
        eval("2*9-4+1*2/3", 2*9-4+1*2/3);
        eval("1*2+3*4+5*6+7*8-9/3", 1*2+3*4+5*6+7*8-9/3);
        
        eval("3-9*4/2+8*6-6-5-2+3*2/3*1+2+3*4-7+1+2-3*1/4-5*9",
              3-9*4/2+8*6-6-5-2+3*2/3*1+2+3*4-7+1+2-3*1/4-5*9);
        
        int bigSize = 100_000;
        
        char[] chars = new char[1 + 2 * bigSize];
        Random rand = new Random();
        
        chars[0] = '1';
        for (int i = 0; i < bigSize; ++i) {
            
            char op = (char) rand.nextInt(4);
            switch (op) {
                case 0: op = '+'; break;
                case 1: op = '-'; break;
                case 2: op = '*'; break;
                case 3: op = '/'; break;
                default: throw new AssertionError((int) op);
            }
            
            char digit = (char) ((op == '/') ? ('1' + rand.nextInt(9))
                                             : ('0' + rand.nextInt(10)));
            
            int index = 1 + 2 * i;
            chars[index] = op;
            chars[index + 1] = digit;
        }
        
        String  bigString = newString(chars);
        Integer bigResult = null;
        
        // Nashorn throws a StackOverflowError
//        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
//        if (engine != null) {
//            String transformed = bigString.replaceAll("\\d/\\d", "($0|0)");
//            try {
//                bigResult = ((Number) engine.eval(transformed)).intValue();
//            } catch (ScriptException x) {
//                eps.app.Log.caught(getClass(), "", x, false);
//                bigResult = null;
//            }
//        } else {
//            System.out.println("    no script engine");
//        }
        
        eval(bigString, bigResult);
    }
    
    private int precedence(char op) {
        switch (op) {
            case '+':
            case '-': return 1;
            case '*':
            case '/': return 2;
            default : throw new AssertionError(op);
        }
    }
    
    private int compare(char opL, char opR) {
        return Integer.compare(precedence(opL), precedence(opR));
    }
    
    private void eval(String infix, Integer expect) {
        ListDeque<Character> operators = new ListDeque<>();
        ListDeque<Character> postfix   = new ListDeque<>();
        
        for (int i = 0; i < infix.length(); ++i) {
            char c = infix.charAt(i);
            
            if ('0' <= c && c <= '9') {
                postfix.add(c);
            } else {
                while (!operators.isEmpty()
                        && compare(c, operators.peek()) <= 0) {
                    postfix.add(operators.pop());
                }
                operators.push(c);
            }
        }
        
        while (!operators.isEmpty()) {
            postfix.add(operators.pop());
        }
        
        ListDeque<Integer> operands = new ListDeque<>();
        
        for (Character elem : postfix) {
            if ('0' <= elem && elem <= '9') {
                operands.push(elem - '0');
                
            } else {
                Integer b = operands.pop();
                Integer a = operands.pop();
                Integer c;
                
                switch (elem) {
                    case '+': c = a + b; break;
                    case '-': c = a - b; break;
                    case '*': c = a * b; break;
                    case '/': c = a / b; break;
                    default : throw new AssertionError(elem);
                }
                
                operands.push(c);
            }
        }
        
        assertEquals("singular operand is the result", 1, operands.size());
        int result = operands.pop();
        
        if (expect != null) {
            assertEquals(infix, expect.intValue(), result);
            System.out.print("    ");
            System.out.print(infix);
            System.out.print(" = ");
            System.out.println(expect);
        } else {
            System.out.print("    result = ");
            System.out.println(result);
        }
    }
}