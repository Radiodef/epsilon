/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.util.Literals.*;
import static eps.util.Misc.*;
import static eps.test.Tools.*;

import java.util.*;
import java.util.stream.*;
import static java.util.stream.Collectors.*;

import org.junit.*;
import static org.junit.Assert.*;

public class IntHashSetTest {
    public IntHashSetTest() {
    }
    
    private String message(String json) {
        return "json = \"" + json + "\"";
    }
    
    @Test
    public void testJson() {
        testing();
        
        String json;
        IntHashSet set;
        
        json = "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]";
        set  = new IntHashSet(json);
        
        assertEquals(message(json), IntStream.rangeClosed(0, 9).boxed().collect(toSet()), set);
        
        json = "[]";
        set  = new IntHashSet(json);
        
        assertEquals(message(json), Collections.emptySet(), set);
        
        json = j(ListOf(-1.0, 1.5, Math.PI, -Double.MAX_VALUE, Double.MIN_VALUE), ", ", "[", "]", d -> d);
        set  = new IntHashSet(json);
        
        assertEquals(message(json), SetOf(-1, 1, 3, Integer.MIN_VALUE, 0), set);
        
        for (String bad : ListOf(null,
                                 "null",
                                 "{}",
                                 "[\"a\", \"b\", \"c\"]",
                                 "[[0]]",
                                 "[true, false, null]",
                                 "601")) {
            try {
                set = new IntHashSet(bad);
                fail(message(bad));
            } catch (IllegalArgumentException | NullPointerException x) {
                System.out.println("    " + Misc.createSimpleMessage(x));
            }
        }
    }
}