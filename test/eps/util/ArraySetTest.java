/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;
import static eps.util.Literals.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class ArraySetTest {
    public ArraySetTest() {
    }
    
    @Test
    public void testSize() {
        testing();
        
        Set<Integer> s = new ArraySet<>();
        
        int max = 1000;
        
        for (int i = 0; i < max; ++i) {
            assertTrue(s.add(i));
            assertEquals(i + 1, s.size());
        }
        for (int i = 0; i < max; ++i) {
            assertFalse(s.add(i));
            assertEquals(max, s.size());
        }
        
        s.clear();
        assertEquals(0, s.size());
        
        assertTrue(s.addAll(SetOf(100, 200, 300)));
        assertEquals(3, s.size());
    }
    
    @Test
    public void testAddGetAndSet() {
        testing();
        
        ArraySet<Integer> s = new ArraySet<>();
        
        int max = 100;
        
        for (int i = 0; i < max; ++i) {
            assertTrue(s.add(i));
            assertTrue(s.contains(i));
        }
        for (int i = 0; i < max; ++i) {
            assertEquals((Integer) i, s.get(i));
        }
        
        try {
            s.get(-1);
            fail();
        } catch (IndexOutOfBoundsException x) {
//            System.out.println(x);
        }
        try {
            s.get(max);
            fail();
        } catch (IndexOutOfBoundsException x) {
//            System.out.println(x);
        }
        
        for (int i = 0; i < max; ++i) {
            assertFalse(s.set(i, max - 1 - i));
        }
        for (int i = 0; i < max; ++i) {
            assertTrue(s.set(i, max + i));
        }
        
        try {
            s.set(-1, 0);
            fail();
        } catch (IndexOutOfBoundsException x) {
//            System.out.println(x);
        }
        try {
            s.set(max, 0);
            fail();
        } catch (IndexOutOfBoundsException x) {
//            System.out.println(x);
        }
        
        for (int i = 0; i < max; ++i) {
            assertEquals((Integer) (max + i), s.get(i));
        }
        
        assertEquals(max, s.size());
    }
    
    @Test
    public void testRemove() {
        testing();
        
        int max = 100;
        
        ArraySet<Integer> ints = new ArraySet<>(max);
        
        for (int i = 0; i < max; ++i) {
            assertTrue(ints.add(i));
            assertTrue(ints.contains(i));
        }
        
        try {
            ints.remove(-1);
            fail();
        } catch (IndexOutOfBoundsException x) {
//            System.out.println(x);
        }
        try {
            ints.remove(max);
            fail();
        } catch (IndexOutOfBoundsException x) {
//            System.out.println(x);
        }
        
        for (int i = 0; i < max; ++i) {
            assertEquals((Integer) i, ints.remove(0));
            assertFalse(ints.contains(i));
            assertFalse(ints.remove((Integer) i));
        }
        
        ArraySet<String> strs = new ArraySet<>(max);
        
        for (int i = 0; i < max; ++i) {
            assertTrue(strs.add(String.valueOf(i)));
        }
        
        List<String> rem = new ArrayList<>(max / 2);
        
        for (int i = max / 2; i < max; ++i) {
            rem.add(String.valueOf(i));
        }
        
        assertTrue(strs.removeAll(rem));
        assertTrue(Collections.disjoint(strs, rem));
    }
    
    @Test
    public void testIterator() {
        testing();
        
        int max = 100;
        
        ArraySet<Double> doubles = new ArraySet<>(max);
        
        for (int i = max; i > 0; --i) {
            doubles.add(Double.valueOf(i));
        }
        
        Iterator<Double> iter = doubles.iterator();
        assertNotNull(iter);
        
        for (int i = max; i > 0; --i) {
            assertTrue(iter.hasNext());
            assertEquals((double) i, (double) iter.next(), 0);
        }
        
        assertFalse(iter.hasNext());
        
        for (int i = 0; i < 3; ++i) {
            try {
                iter.next();
                fail();
            } catch (NoSuchElementException x) {
//                System.out.println(x);
            }
        }
        
        iter = doubles.iterator();
        
        for (int i = max; i > 0; --i) {
            assertTrue(iter.hasNext());
            assertEquals((double) i, (double) iter.next(), 0);
            try {
                iter.remove();
            } catch (IllegalStateException x) {
                fail(x.toString());
            }
            try {
                iter.remove();
                fail();
            } catch (IllegalStateException x) {
//                System.out.println(x);
            }
        }
    }
}
