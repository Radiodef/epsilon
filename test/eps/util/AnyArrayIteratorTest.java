/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;

import java.util.*;
import java.util.function.*;

import org.junit.*;
import static org.junit.Assert.*;

public class AnyArrayIteratorTest {
    public AnyArrayIteratorTest() {
    }
    
    @Test
    public void testIterator() {
        testing();
        
        List<String> strings = new ArrayList<>();
        
        for (char c = 'a'; c <= 'z'; ++c) {
            strings.add(String.valueOf(c));
        }
        
        testIterators(strings::iterator, () -> new AnyArrayIterator(strings.toArray()));
        
        List<Character> chars = new ArrayList<>();
        char[] arr = new char[26];
        
        for (char c = 'A', i = 0; c <= 'Z'; ++c) {
            chars.add(c);
            arr[i++] = c;
        }
        
        testIterators(chars::listIterator, () -> new AnyArrayIterator(arr));
        
        try {
            new AnyArrayIterator(new int[0], 0, 1);
            fail();
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            new AnyArrayIterator(new int[1], 1, 0);
            fail();
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            new AnyArrayIterator(new int[4], -1, 1);
            fail();
        } catch (IndexOutOfBoundsException x) {
        }
        try {
            new AnyArrayIterator(new int[10], 1, 8);
        } catch (IndexOutOfBoundsException x) {
            fail(x.toString());
        }
        
        try {
            new AnyArrayIterator(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        try {
            new AnyArrayIterator("abc");
            fail();
        } catch (IllegalArgumentException x) {
        }
    }
    
    private void testIterators(Supplier<Iterator<?>> expect, Supplier<Iterator<?>> actual) {
        Iterator<?> itE = expect.get();
        Iterator<?> itA = actual.get();
        
        for (;;) {
            assertEquals(itE.hasNext(), itA.hasNext());
            
            if (!itE.hasNext()) {
                try {
                    itA.next();
                    fail();
                } catch (NoSuchElementException x) {
                }
                
                break;
            }
            
            assertEquals(itE.next(), itA.next());
        }
        
        itE = expect.get();
        itA = actual.get();
        
        try {
            itA.remove();
            fail();
        } catch (UnsupportedOperationException x) {
            return;
        } catch (IllegalStateException x) {
        }
        
        for (;;) {
            assertEquals(itE.hasNext(), itA.hasNext());
            
            itE.next();
            itA.next();
            
            itE.remove();
            itA.remove();
            
            try {
                itA.remove();
                fail();
            } catch (IllegalStateException x) {
            }
        }
    }
}