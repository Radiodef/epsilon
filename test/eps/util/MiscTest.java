/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import eps.app.*;
import static eps.util.Literals.*;
import static eps.test.Tools.*;

import java.util.*;
import java.util.function.*;
import java.lang.reflect.*;
import static java.util.Arrays.asList;

import org.junit.*;
import static org.junit.Assert.*;

public class MiscTest {
    public MiscTest() {
    }
    
    @Test
    public void testToHexLiteral() {
        testing();
        
        assertEquals("0x00000000", Misc.toHexLiteral(0));
        assertEquals("0x00000001", Misc.toHexLiteral(1));
        assertEquals("0xFFFFFFFF", Misc.toHexLiteral(-1));
        assertEquals("0x12345678", Misc.toHexLiteral(0x12345678));
        assertEquals("0xDEADBEEF", Misc.toHexLiteral(0xDEADBEEF));
    }
    
    @Test
    public void testGetCaller() {
        testing();
        
        StackTraceElement elem;
        elem = Misc.getCaller();
        assertEquals(getClass().getName(), elem.getClassName());
        assertEquals("testGetCaller", elem.getMethodName());
        elem = getCaller1();
        assertEquals(getClass().getName(), elem.getClassName());
        assertEquals("testGetCaller", elem.getMethodName());
        
        try {
            elem = Misc.getCaller(-1);
            fail("negative value should be illegal");
        } catch (IllegalArgumentException x) {
        }
        try {
            elem = Misc.getCaller(1 << 24);
            fail("2^24 should be large enough to be out of bounds");
        } catch (IllegalArgumentException x) {
        }
    }
    
    private StackTraceElement getCaller1() {
        return Misc.getCaller(1);
    }
    
    public static final String CONSTANT_0 = "Constant 0";
    public static final String CONSTANT_1 = "Constant 1";
    public static final String CONSTANT_2 = "Constant 2";
    
    private static String NOT_CONSTANT = "Not Constant";
    
    @IfDeveloper
    public static final Double DOUBLE_CONSTANT = 0.0;
    
    public static class Foo {}
    public static class Bar extends Foo {}
    public static class Baz extends Foo {}
    
    public static final Foo FOO = new Foo();
    public static final Bar BAR = new Bar();
    public static final Baz BAZ = new Baz();
    
    @Test
    public void testCollectConstants() {
        testing();
        
        assertTrue("no constants of this type defined", Misc.collectConstants(MiscTest.class).isEmpty());
        
        List<String> constants = Misc.collectConstants(MiscTest.class, String.class);
        assertEquals(ListOf(CONSTANT_0, CONSTANT_1, CONSTANT_2), constants);
        
        assertFalse("private static non-final field should not have been collected",
                    constants.contains(NOT_CONSTANT));
        
        List<Double> doubleConstants = Misc.collectConstants(MiscTest.class, Double.class);
        assertEquals(EpsilonMain.isDeveloper(), doubleConstants.size() == 1);
        
        List<Foo> fooConstants = Misc.collectConstants(MiscTest.class, Foo.class);
        assertEquals("should collect subtypes", ListOf(FOO, BAR, BAZ), fooConstants);
        
        try {
            Misc.collectConstants(null);
            fail("null");
        } catch (NullPointerException x) {
        }
        
        for (Class<?>[] args : new Class<?>[][] {
                { null,         null         },
                { Object.class, null         },
                { null,         Object.class }}) {
            try {
                List<?> list = Misc.collectConstants(args[0], args[1]);
                fail(Arrays.toString(args) + " = " + list);
            } catch (NullPointerException x) {
            }
        }
    }
    
    private MiscTest(Float arg0) {
        throw new AssertionError("private");
    }
    
    @Test
    public void testCreateConstructorFunction() {
        testing();
        
        Function<String, Integer> newInteger = Misc.createConstructorFunction(Integer.class, String.class);
        
        for (String str : Arrays.asList("0",
                                        "1",
                                        "-1",
                                        "2.2",
                                        "xyz",
                                        "",
                                        null,
                                        "null",
                                        "123456789",
                                        "314159")) {
            Integer a;
            try {
                a = new Integer(str);
            } catch (RuntimeException x) {
                Integer b;
                try {
                    b = newInteger.apply(str);
                } catch (RuntimeException y) {
                    assertEquals(x.getClass(), y.getClass());
                    assertEquals(x.getMessage(), y.getMessage());
                    continue;
                }
                fail(String.valueOf(b));
                throw new AssertionError(b);
            }
            assertEquals(str, a, newInteger.apply(str));
        }
        
        try {
            Function<String, MiscTest> func = Misc.createConstructorFunction(MiscTest.class, String.class);
            fail(func.toString());
        } catch (IllegalArgumentException x) {
        }
        try {
            Function<Float, MiscTest> func = Misc.createConstructorFunction(MiscTest.class, Float.class);
            fail(func.toString());
        } catch (IllegalArgumentException x) {
        }
        
        for (Class<?>[] args : new Class<?>[][] {
                { null,         null         },
                { Object.class, null         },
                { null,         Object.class }}) {
            try {
                Function<?, ?> func = Misc.createConstructorFunction(args[0], args[1]);
                fail(Arrays.toString(args) + " = " + func);
            } catch (NullPointerException x) {
            }
        }
    }
    
    private String doPrivate() {
        throw new AssertionError("private");
    }
    private static String doStatic() {
        throw new AssertionError("static");
    }
    
    @Test
    public void testCreateInstanceFunction() {
        testing();
        
        Function<Integer, String> toString = Misc.createInstanceFunction(Integer.class, "toString", String.class);
        
        for (Integer n : ListOf(0, 1, -1,
                                null,
                                123456789,
                                314159,
                                Integer.MIN_VALUE,
                                Integer.MAX_VALUE)) {
//        for (long i = Integer.MIN_VALUE; i <= Integer.MAX_VALUE; ++i) {
//            Integer n = (int) i;
            String a;
            try {
                a = n.toString();
            } catch (RuntimeException x) {
                String b;
                try {
                    b = toString.apply(n);
                } catch (RuntimeException y) {
                    assertEquals(x.getClass(), y.getClass());
                    assertEquals(x.getMessage(), y.getMessage());
                    continue;
                }
                fail(b);
                throw new AssertionError(b);
            }
            assertEquals(a, toString.apply(n));
        }
        
        final class Args {
            final Class<?> arg0;
            final String   arg1;
            final Class<?> arg2;
            Args(int bits) {
                arg0 = (bits & 0b001) == 0 ? null : String.class;
                arg1 = (bits & 0b010) == 0 ? null : "toString";
                arg2 = (bits & 0b100) == 0 ? null : String.class;
            }
            Args(Class<?> arg0, String arg1, Class<?> arg2) {
                this.arg0 = arg0;
                this.arg1 = arg1;
                this.arg2 = arg2;
            }
            @Override
            public String toString() {
                return Arrays.asList(arg0, arg1, arg2).toString();
            }
        }
        
        for (int bits = 0b000; bits < 0b111; ++bits) {
            Args args = new Args(bits);
            try {
                Function<?, ?> func = Misc.createInstanceFunction(args.arg0, args.arg1, args.arg2);
                fail(args + " = " + func);
            } catch (NullPointerException x) {
            }
        }
        
        for (Args args : ListOf(new Args(MiscTest.class, "not_method", Object.class),
                                new Args(MiscTest.class, "toString",   Double.class),
                                new Args(MiscTest.class, "doPrivate",  String.class),
                                new Args(MiscTest.class, "doStatic",   String.class))) {
            try {
                Function<?, ?> func = Misc.createInstanceFunction(args.arg0, args.arg1, args.arg2);
                fail(args + " = " + func);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    private Foo doPrivate(Foo foo) {
        throw new AssertionError("private");
    }
    private Foo doInstance(Foo foo) {
        throw new AssertionError("instance");
    }
    private static Foo toFoo(Bar bar) {
        throw new AssertionError(bar);
    }
    
    @Test
    public void testCreateStaticFunction() {
        testing();
        
        Function<String, Integer> decode = Misc.createStaticFunction(Integer.class, "decode", String.class, Integer.class);
        
        for (String str : ListOf("0",
                                 "1",
                                 "-1",
                                 null,
                                 "null",
                                 "",
                                 " 0 ",
                                 "0xDEADBEAF",
                                 "-0x4Ed",
                                 "#ffe1",
                                 "0b10101010",
                                 "0123")) {
            Integer a;
            try {
                a = Integer.decode(str);
            } catch (RuntimeException x) {
                Integer b;
                try {
                    b = decode.apply(str);
                } catch (RuntimeException y) {
                    assertEquals(x.getClass(), y.getClass());
                    assertEquals(x.getMessage(), y.getMessage());
                    continue;
                }
                fail("\"" + str + "\" = " + b);
                continue;
            }
            
            assertEquals(a, decode.apply(str));
        }
        
        final class Args {
            final Class<?> arg0;
            final String   arg1;
            final Class<?> arg2;
            final Class<?> arg3;
            Args(int bits) {
                arg0 = (bits & 0b0001) == 0 ? null : Math.class;
                arg1 = (bits & 0b0010) == 0 ? null : "sin";
                arg2 = (bits & 0b0100) == 0 ? null : double.class;
                arg3 = (bits & 0b1000) == 0 ? null : double.class;
            }
            Args(Class<?> arg0, String arg1, Class<?> arg2, Class<?> arg3) {
                this.arg0 = arg0;
                this.arg1 = arg1;
                this.arg2 = arg2;
                this.arg3 = arg3;
            }
            @Override
            public String toString() {
                return ListOf(arg0, arg1, arg2, arg3).toString();
            }
        }
        
        for (int bits = 0b0000; bits < 0b1111; ++bits) {
            Args args = new Args(bits);
            try {
                Function<?, ?> func = Misc.createStaticFunction(args.arg0, args.arg1, args.arg2, args.arg3);
                fail(args + " = " + func);
            } catch (NullPointerException x) {
            }
        }
        
        for (Args args : ListOf(new Args(MiscTest.class, "not_method", Foo.class, Foo.class),
                                new Args(MiscTest.class, "doPrivate",  Foo.class, Foo.class),
                                new Args(MiscTest.class, "doInstance", Foo.class, Foo.class),
                                new Args(MiscTest.class, "toFoo",      Foo.class, Baz.class))) {
            try {
                Function<?, ?> func = Misc.createStaticFunction(args.arg0, args.arg1, args.arg2, args.arg3);
                fail(args + " = " + func);
            } catch (IllegalArgumentException x) {
            }
        }
    }
    
    @Test
    public void testRangeEquals() {
        testing();
        
        assertTrue(Misc.rangeEquals("abcdef", 1, "bcd"));
        assertTrue(Misc.rangeEquals("abcdef", 0, "abc"));
        assertTrue(Misc.rangeEquals("abcdef", 3, "def"));
        assertTrue(Misc.rangeEquals("abcdef", 0, "abcdef"));
        assertTrue(Misc.rangeEquals("", 0, ""));
        assertTrue(Misc.rangeEquals("X", 0, ""));
        assertTrue(Misc.rangeEquals("X", 1, ""));
        
        assertFalse(Misc.rangeEquals("abcdef", 0, "bcd"));
        assertFalse(Misc.rangeEquals("abcdef", 3, "abc"));
        assertFalse(Misc.rangeEquals("abcdef", 3, "defa"));
        assertFalse(Misc.rangeEquals("abc", -1, "c"));
        assertFalse(Misc.rangeEquals("abc", 3, "a"));
        
        try {
            Misc.rangeEquals(null, 0, "abc");
            fail();
        } catch (NullPointerException x) {
        }
        try {
            Misc.rangeEquals("abc", 0, null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            Misc.rangeEquals(null, 0, null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testRangeBeforeEquals() {
        testing();
        
        assertTrue(Misc.rangeBeforeEquals("abcdef", 0, ""));
        assertTrue(Misc.rangeBeforeEquals("abcdef", 1, "a"));
        assertTrue(Misc.rangeBeforeEquals("abcdef", 2, "b"));
        assertTrue(Misc.rangeBeforeEquals("abcdef", 3, "abc"));
        assertTrue(Misc.rangeBeforeEquals("abcdef", 4, "cd"));
        assertTrue(Misc.rangeBeforeEquals("abcdef", 5, "bcde"));
        assertTrue(Misc.rangeBeforeEquals("abcdef", 6, "def"));
        assertTrue(Misc.rangeBeforeEquals("", 0, ""));
        assertTrue(Misc.rangeBeforeEquals("X", 0, ""));
        assertTrue(Misc.rangeBeforeEquals("X", 1, ""));
        
        assertFalse(Misc.rangeBeforeEquals("abcdef", 3, "def"));
        assertFalse(Misc.rangeBeforeEquals("abcdef", 6, "defa"));
        assertFalse(Misc.rangeBeforeEquals("abcdef", 3, "fabc"));
        assertFalse(Misc.rangeBeforeEquals("abcdef", -1, "def"));
        assertFalse(Misc.rangeBeforeEquals("abcdef", 7, "defa"));
        assertFalse(Misc.rangeBeforeEquals("", -1, ""));
        assertFalse(Misc.rangeBeforeEquals("X", 2, ""));
        
        try {
            Misc.rangeBeforeEquals(null, 0, "abc");
            fail();
        } catch (NullPointerException x) {
        }
        try {
            Misc.rangeBeforeEquals("abc", 0, null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            Misc.rangeBeforeEquals(null, 0, null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    public static class NoArgumentException extends Exception {
    }
    public static class NoCauseException extends Exception {
        public NoCauseException(String message) {
            super(message);
        }
    }
    static class PackagePrivateException extends Exception {
    }
    
    @Test
    public void testSetThrowableMessage() {
        testing();
        
        NullPointerException nullPtr1 = new NullPointerException("1");
        NullPointerException nullPtr2 = Misc.setMessage(nullPtr1, "2");
        
        assertNotEquals("there is a (String, Throwable) constructor", nullPtr1, nullPtr2);
        assertEquals("nullPtr1 should be the cause", nullPtr1, nullPtr2.getCause());
        assertEquals("the message should be as specified", "2", nullPtr2.getMessage());
        
        nullPtr2 = Misc.setMessage(nullPtr1, null);
        assertEquals("the message should be as specified", "null", nullPtr2.getMessage());
        
        NoArgumentException noArg1 = new NoArgumentException();
        NoArgumentException noArg2 = Misc.setMessage(noArg1, "2");
        
        assertNotEquals("a new instance still should be created to preserve a stack trace", noArg1, noArg2);
        assertEquals("cause should still be set", noArg1, noArg2.getCause());
        assertNotEquals("no message should be set", "2", noArg2.getMessage());
        
        try {
            Misc.setMessage(null, "message");
            fail("null Throwable");
        } catch (NullPointerException x) {
        }
        
        NoCauseException noCause1 = new NoCauseException("1");
        NoCauseException noCause2 = Misc.setMessage(noCause1, "2");
        assertNotEquals("there is a (String) constructor", noCause1, noCause2);
        assertEquals("cause should still be set", noCause1, noCause2.getCause());
        assertEquals("message should be as specified", "2", noCause2.getMessage());
        
        PackagePrivateException ppe = new PackagePrivateException();
        assertEquals("no constructor accessible", ppe, Misc.setMessage(ppe, "message"));
    }
    
    @Test
    public void testArrayAppend() {
        testing();
        
        String[] arr = new String[0];
        arr = Misc.append(arr, "abc");
        
        assertEquals("length should have grown", 1, arr.length);
        assertEquals("element should be added", arr[0], "abc");
        
        arr = Misc.append(arr, "def");
        
        assertEquals("length should have grown", 2, arr.length);
        assertEquals("element should be present", arr[0], "abc");
        assertEquals("element should be added", arr[1], "def");
        
        try {
            arr = Misc.append(arr, null);
        } catch (NullPointerException x) {
            fail(x.toString());
        }
        
        String[] copy = arr.clone();
        
        try {
            arr = Misc.append(null, "abc");
            fail(String.valueOf(arr));
        } catch (NullPointerException x) {
        }
        
        assertArrayEquals("failure should not mutate", copy, arr);
        
        Integer[] ints = new Integer[0];
        
        for (int i = 0; i < 100; ++i) {
            assertEquals(i, ints.length);
            Integer[] prev = ints;
            Integer[] clone = prev.clone();
            
            ints = Misc.append(ints, i);
            
            assertTrue("result should be a different array", prev != ints);
            assertArrayEquals("previous should not have changed", clone, prev);
            
            assertEquals("array should have grown", i + 1, ints.length);
            assertEquals("element should have been appended", i, ints[i].intValue());
        }
    }
    
    @Test
    public void testArrayInsert() {
        testing();
        String[] arr = new String[0];
        
        arr = Misc.insert(arr, 0, "abc");
        assertArrayEquals(new String[] {"abc"}, arr);
        
        arr = Misc.insert(arr, 0, "def");
        assertArrayEquals(new String[] {"def", "abc"}, arr);
        
        arr = Misc.insert(arr, 1, "ghi");
        assertArrayEquals(new String[] {"def", "ghi", "abc"}, arr);
        
        String[] copy = arr.clone();
        
        try {
            arr = Misc.insert(arr, -1, "xyz");
            fail("oob index should throw");
        } catch (IndexOutOfBoundsException x) {
        }
        
        assertArrayEquals("failure should not mutate", copy, arr);
        
        try {
            arr = Misc.insert(arr, arr.length + 1, "xyz");
            fail("oob index should throw");
        } catch (IndexOutOfBoundsException x) {
        }
        
        assertArrayEquals("failure should not mutate", copy, arr);
        
        try {
            Misc.insert(null, 0, "xyz");
            fail("null should throw");
        } catch (NullPointerException x) {
        }
        
        try {
            arr = Misc.insert(arr, 0, null);
        } catch (NullPointerException x) {
            fail(x.toString());
        }
        
        Float[] empty = new Float[0];
        
        Float[] floats = new Float[0];
        List<Float> list = new ArrayList<>();
        
        Random rand = new Random();
        
        for (int i = 0; i < 100; ++i) {
            assertEquals(i, floats.length);
            
            int index = rand.nextInt(i + 1);
            float f = i;
            
            Float[] prev = floats;
            Float[] clone = prev.clone();
            
            floats = Misc.insert(floats, index, f);
            assertTrue("the result should be a different array", prev != floats);
            assertArrayEquals("the argument should be unchanged", clone, prev);
            
            list.add(index, f);
            assertArrayEquals(list.toArray(empty), floats);
        }
    }
    
    @Test
    public void testArrayRemoveElement() {
        testing();
        String[] arr = {"a", "b", "c", "d", "e", "f"};
        
        assertTrue("failure should do nothing", arr == Misc.remove(arr, "x"));
        
        arr = Misc.remove(arr, "d");
        assertArrayEquals(new String[] {"a", "b", "c", "e", "f"}, arr);
        
        arr = Misc.remove(arr, "b");
        assertArrayEquals(new String[] {"a", "c", "e", "f"}, arr);
        
        try {
            Misc.remove(null, "x");
            fail("null should be an error");
        } catch (NullPointerException x) {
        }
        
        try {
            arr = Misc.remove(arr, null);
        } catch (NullPointerException x) {
            fail(x.toString());
        }
        
        arr = Misc.insert(arr, 1, null);
        
        try {
            arr = Misc.remove(arr, null);
        } catch (NullPointerException x) {
            fail(x.toString());
        }
        
        assertArrayEquals(new String[] {"a", "c", "e", "f"}, arr);
        
        Byte[]     empty = new Byte[0];
        Byte[]     bytes = new Byte[100];
        List<Byte> list  = new ArrayList<>();
        
        Random rand = new Random();
        
        for (int i = 0; i < bytes.length; ++i) {
            Byte b = (byte) rand.nextInt();
            bytes[i] = b;
            list.add(b);
        }
        
        while (!list.isEmpty()) {
            int index = rand.nextInt(bytes.length);
            Byte b = list.get(index);
            
            Byte[] before = bytes;
            Byte[] clone  = before.clone();
            
            bytes = Misc.remove(bytes, b);
            
            assertTrue("the result should be a different array", bytes != before);
            assertArrayEquals("the argument should be unchanged", clone, before);
            
            list.remove(b);
            assertArrayEquals(list.toArray(empty), bytes);
        }
    }
    
    @Test
    public void testArrayRemoveAtIndex() {
        testing();
        
        String[] arr = {"a", "b", "c"};
        
        arr = Misc.remove(arr, 0);
        assertArrayEquals(new String[] {"b", "c"}, arr);
        
        arr = Misc.remove(arr, 1);
        assertArrayEquals(new String[] {"b"}, arr);
        
        try {
            Misc.remove(null, 0);
            fail("null should be an error");
        } catch (NullPointerException x) {
        }
        
        arr = new String[] {"a", "b", "c"};
        
        String[] copy = arr.clone();
        
        try {
            arr = Misc.remove(arr, -1);
            fail("oob should be an error");
        } catch (IndexOutOfBoundsException x) {
        }
        
        assertArrayEquals("error should not mutate", copy, arr);
        
        try {
            arr = Misc.remove(arr, arr.length);
            fail("oob should be an error");
        } catch (IndexOutOfBoundsException x) {
        }
        
        assertArrayEquals("error should not mutate", copy, arr);
        
        Short[]     empty  = new Short[0];
        Short[]     shorts = new Short[100];
        List<Short> list   = new ArrayList<>();
        
        Random rand = new Random();
        
        for (int i = 0; i < shorts.length; ++i) {
            Short s = (short) rand.nextInt();
            shorts[i] = s;
            list.add(s);
        }
        
        while (!list.isEmpty()) {
            int index = rand.nextInt(shorts.length);
            
            Short[] before = shorts;
            Short[] clone  = before.clone();
            
            shorts = Misc.remove(shorts, index);
            
            assertTrue("result should be a different array", shorts != before);
            assertArrayEquals("the argument should be unchanged", clone, before);
            
            list.remove(index);
            
            assertArrayEquals(list.toArray(empty), shorts);
        }
    }
    
    class Inner {}
    
    @Test
    public void testGetSimpleName() {
        testing();
        
        try {
            Misc.getSimpleName(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        assertEquals("HashMap", Misc.getSimpleName(HashMap.class));
        assertEquals("Entry", Misc.getSimpleName(Map.Entry.class));
        assertEquals("int", Misc.getSimpleName(int.class));
        assertEquals("void", Misc.getSimpleName(void.class));
        assertEquals("Inner", Misc.getSimpleName(Inner.class));
        
        class Local {}
        assertEquals("Local", Misc.getSimpleName(Local.class));
        
        Class<?> anonCls = new Cloneable() {}.getClass();
        assertEquals("MiscTest$1", Misc.getSimpleName(anonCls));
        
        // Not really sure what the result of this ought to be anyway,
        // as long as there's no problem getting it.
        Class<?> lambdaCls  = ((Runnable) () -> {}).getClass();
        String   lambdaName = Misc.getSimpleName(lambdaCls);
        assertNotNull(lambdaName);
        
        assertEquals("String[]", Misc.getSimpleName(String[].class));
        assertEquals("Entry[][]", Misc.getSimpleName(Map.Entry[][].class));
        assertEquals("int[][]", Misc.getSimpleName(int[][].class));
        assertEquals("Inner[]", Misc.getSimpleName(Inner[].class));
        assertEquals("Local[]", Misc.getSimpleName(Local[].class));
        
        Class<?> anonArr = Misc.getArrayClass(anonCls);
        assertEquals("MiscTest$1[]", Misc.getSimpleName(anonArr));
        
        Class<?> lambdaArr = Misc.getArrayClass(lambdaCls, 3);
        assertEquals(lambdaName + "[][][]", Misc.getSimpleName(lambdaArr));
    }
    
    @Test
    public void testConcatIterators() {
        testing();
        
        Iterator<Integer> itA = Arrays.asList(1, 2, 3).iterator();
        Iterator<Integer> itB = Arrays.asList(4, 5, 6).iterator();
        
        Iterator<Integer> cat = Misc.cat(itA, itB);
        
        for (int i = 1; i <= 7; ++i) {
            assertEquals(i < 7, cat.hasNext());
            if (!cat.hasNext())
                break;
            assertEquals(i, cat.next().intValue());
        }
        
        try {
            cat.next();
            fail();
        } catch (NoSuchElementException x) {
        }
        
        assertFalse(Misc.cat(Collections.emptyIterator(), Collections.emptyIterator()).hasNext());
        
        try {
            Misc.cat(null, null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testArrayClass() {
        testing();
        
        try {
            Class<?> arr = Misc.getArrayClass(null);
            fail(String.valueOf(arr));
        } catch (NullPointerException x) {
        }
        try {
            Class<Integer[]> arr = Misc.getArrayClass(int.class);
            fail(String.valueOf(arr));
        } catch (IllegalArgumentException x) {
        }
        try {
            Class<?> arr = Misc.getArrayClass(void.class);
            fail(String.valueOf(arr));
        } catch (IllegalArgumentException x) {
        }
        
        Class<int[][]> multi = Misc.getArrayClass(int[].class);
        assertEquals(int[][].class, multi);
        
        assertEquals(String[].class, Misc.getArrayClass(String.class));
        assertEquals(Object[][][].class, Misc.getArrayClass(Object[][].class));
        assertEquals(Float[][].class, Misc.getArrayClass(new Float[0].getClass()));
        
        try {
            Class<?> arr = Misc.getArrayClass(null, 0);
            fail(String.valueOf(arr));
        } catch (NullPointerException x) {
        }
        try {
            Class<?> arr = Misc.getArrayClass(String.class, -1);
            fail(String.valueOf(arr));
        } catch (IllegalArgumentException x) {
        }
        
        assertEquals(Double.class, Misc.getArrayClass(Double.class, 0));
        assertEquals(double[].class, Misc.getArrayClass(double.class, 1));
        assertEquals(double[][].class, Misc.getArrayClass(double.class, 2));
        
        for (int i = 1; i < 255; ++i) {
            Object arr = Array.newInstance(Boolean.class, new int[i]);
            assertEquals(arr.getClass(), Misc.getArrayClass(Boolean.class, i));
        }
        
        try {
            Class<?> arr = Misc.getArrayClass(int.class, 255);
        } catch (IllegalArgumentException x) {
            fail(x.toString());
        }
        try {
            Class<?> arr = Misc.getArrayClass(int.class, 256);
            fail(String.valueOf(arr));
        } catch (IllegalArgumentException x) {
        }
        try {
            Class<?> arr = Misc.getArrayClass(void.class, 10);
            fail(String.valueOf(arr));
        } catch (IllegalArgumentException x) {
        }
    }
    
    private static String getPrivate() {
        return "private";
    }
    private static String mapPrivate(String str) {
        return str + " private";
    }
    private static String getInstance() {
        return "instance";
    }
    
    private static class Sub extends MiscTest {
    }
    
    @Test
    public void testGetDeclaredMethod() throws ReflectiveOperationException {
        testing();
        Method m;
        
        m = Misc.getDeclaredMethod(MiscTest.class, "getPrivate");
        assertEquals("private", m.invoke(null));
        
        m = Misc.getDeclaredMethod(Sub.class, "mapPrivate", String.class);
        assertEquals("map private", m.invoke(null, "map"));
        
        assertEquals(Misc.getDeclaredMethod(MiscTest.class, "mapPrivate", String.class), m);
        
        m = Misc.getDeclaredMethod(Sub.class, "getInstance", (Class<?>[]) null);
        assertEquals("instance", m.invoke(new Sub()));
        assertEquals("instance", m.invoke(new MiscTest()));
        
        assertEquals(Misc.getDeclaredMethod(MiscTest.class, "getInstance"), m);
        
        try {
            m = Misc.getDeclaredMethod(null, "m");
            fail(String.valueOf(m));
        } catch (NullPointerException x) {
        }
        try {
            m = Misc.getDeclaredMethod(MiscTest.class, null);
            fail(String.valueOf(m));
        } catch (NullPointerException x) {
        }
        try {
            m = Misc.getDeclaredMethod(MiscTest.class, "mapPrivate", null, null);
            fail(String.valueOf(m));
        } catch (NullPointerException x) {
        }
        
        try {
            m = Misc.getDeclaredMethod(MiscTest.class, "not_method", float.class, int.class);
            fail(String.valueOf(m));
        } catch (NoSuchMethodException x) {
        }
        try {
            m = Misc.getDeclaredMethod(Sub.class, "getStatic", String.class);
            fail(String.valueOf(m));
        } catch (NoSuchMethodException x) {
        }
    }
    
    @Test
    public void testDespace() {
        testing();
        
        int i = 0;
        for (String space : asList(" ",
                                   "\n",
                                   "\r",
                                   "\t",
                                   "\f",
                                   "\u000B",
                                   "\u001C",
                                   "\u001D",
                                   "\u001E",
                                   "\u001F",
                                   Character.toString((char) Character.SPACE_SEPARATOR),
                                   Character.toString((char) Character.LINE_SEPARATOR),
                                   Character.toString((char) Character.PARAGRAPH_SEPARATOR))) {
            char   char0 = space.charAt(0);
            String msg   = (i++) + " = " + Character.getName(char0);
            
            if (Character.isWhitespace(char0)) {
                assertEquals(msg, "", Misc.despace(space));
            } else {
                System.out.println("    Character.isWhitespace JavaDoc said this is whitespace: " + msg);
            }
        }
        for (String nbsp : ListOf("\u00A0",
                                  "\u2007",
                                  "\u202F")) {
            char   char0 = nbsp.charAt(0);
            String msg   = (i++) + " = " + Character.getName(char0);
            
            if (!Character.isWhitespace(char0)) {
                assertEquals(msg, nbsp, Misc.despace(nbsp));
            } else {
                System.out.println("    Character.isWhitespace JavaDoc said this is not whitespace: " + msg);
            }
        }
        
        String s;
        
        s = Misc.despace("    \n\r\n\t  ");
        assertEquals("", s);
        
        s = Misc.despace(" a b c d e f g h\ni\tj k l m n o p q r s t u v w x y z ");
        assertEquals("abcdefghijklmnopqrstuvwxyz", s);
        
        s = Misc.despace("abc");
        assertEquals("abc", s);
        assertTrue(s == "abc");
        s = Misc.despace("X");
        assertEquals("X", s);
        assertTrue(s == "X");
        
        try {
            s = Misc.despace(null);
            fail(String.valueOf(s));
        } catch (NullPointerException x) {
        }
        
        s = Misc.despace("the  quick  brown  fox  jumped  over  the  lazy  dog");
        assertEquals("thequickbrownfoxjumpedoverthelazydog", s);
    }
    
    @Test
    public void testUnquote() {
        testing();
        assertNull(Misc.unquote(null));
        assertEquals("abc", Misc.unquote("\"abc\""));
        for (String s : new String[] {
            "abc",
            "\"abc",
            "abc\"",
            " \"abc\" ",
            " abc\" ",
            " \"abc ",
            "'abc'",
        }) {
            assertEquals(s, Misc.unquote(s));
        }
    }
}