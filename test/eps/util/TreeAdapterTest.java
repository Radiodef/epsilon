/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.util;

import static eps.test.Tools.*;
import static eps.util.Literals.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class TreeAdapterTest {
    public TreeAdapterTest() {
    }
    
    static final Object NO_DATA = new Object();
    
    static class TestNode implements Cloneable {
        Object data;
        List<TestNode> children;
        TestNode() {
            data = NO_DATA;
        }
        TestNode(String data) {
            this.data = data;
        }
        TestNode add(TestNode child) {
            if (children == null)
                children = new ArrayList<>();
            children.add(child);
            return this;
        }
        @Override
        public TestNode clone() {
            try {
                TestNode clone = (TestNode) super.clone();
                if (children != null) {
                    clone.children = new ArrayList<>(children);
                    clone.children.replaceAll(TestNode::clone);
                }
                return clone;
            } catch (CloneNotSupportedException x) {
                throw new AssertionError("clone", x);
            }
        }
    }
    
    static class TestTree extends TreeAdapter<TestNode, String> {
        TestTree() {
        }
        TestTree(TestNode root) {
            super(root);
        }
        @Override
        protected String getValue(TestNode node) {
            return (String) node.data;
        }
        @Override
        protected int getChildCount(TestNode node) {
            return Misc.size(node.children);
        }
        @Override
        protected TestNode getChildAt(TestNode node, int i) {
            return node.children.get(i);
        }
        @Override
        protected boolean allowsValue(TestNode node) {
            return node.data != NO_DATA;
        }
        @Override
        protected boolean allowsChildren(TestNode node) {
            return node.children != null;
        }
    }
    
    @Test
    public void test1() {
        testing();
        TestNode root = new TestNode();
        
        root.add(new TestNode("a"))
            .add(new TestNode("b"))
            .add(new TestNode("c").add(new TestNode("d"))
                                  .add(new TestNode("e"))
                                  .add(new TestNode("f")))
            .add(new TestNode("x").add(new TestNode("y").add(new TestNode("z"))));
        
        TestTree tree = new TestTree(root);
        
        assertTrue(root == tree.getRoot().getObject());
        assertEquals(9, tree.valueCount());
        
        testNode(NO_DATA, 4, tree.getRoot());
        testNode("a", 0, tree.getRoot().getChildAt(0));
        testNode("b", 0, tree.getRoot().getChildAt(1));
        testNode("c", 3, tree.getRoot().getChildAt(2));
        testNode("d", 0, tree.getRoot().getChildAt(2).getChildAt(0));
        testNode("e", 0, tree.getRoot().getChildAt(2).getChildAt(1));
        testNode("f", 0, tree.getRoot().getChildAt(2).getChildAt(2));
        testNode("x", 1, tree.getRoot().getChildAt(3));
        testNode("y", 1, tree.getRoot().getChildAt(3).getChildAt(0));
        testNode("z", 0, tree.getRoot().getChildAt(3).getChildAt(0).getChildAt(0));
        
        // note: forEach/toList are depth first
        List<String> expect =
            ListOf("a",
                   "b",
                        "d", "e", "f",
                   "c",
                             "z",
                        "y",
                   "x");
        assertEquals(expect, tree.toList());
        
        assertEquals(tree, new TestTree(root.clone()));
        assertEquals(tree.hashCode(), new TestTree(root.clone()).hashCode());
        
        root.add(new TestNode("1").add(new TestNode("2").add(new TestNode(null))));
        
        assertEquals(expect, tree.toList());
        tree.rebuild();
        assertNotEquals(expect, tree.toList());
        
        assertEquals(12, tree.valueCount());
        
        expect =
            ListOf("a",
                   "b",
                        "d", "e", "f",
                   "c",
                             "z",
                        "y",
                   "x",
                             null,
                        "2",
                   "1");
        assertEquals(expect, tree.toList());
        
        assertEquals(tree, new TestTree(root.clone()));
        assertEquals(tree.hashCode(), new TestTree(root.clone()).hashCode());
        
        root.add(new TestNode() {{ children = new ArrayList<>(); }});
        root.add(new TestNode("#") {{ children = new ArrayList<>(); }});
        root.add(new TestNode());
        tree.rebuild();
        System.out.println(tree.toString(2, "  "));
    }
    
    private void testNode(Object value, int childCount, TreeAdapter.Node<TestNode, String> node) {
        assertEquals(value != NO_DATA, node.allowsValue());
        assertEquals(value == NO_DATA ? null : value, node.getValue());
        
        assertEquals(childCount != 0, node.allowsChildren());
        assertEquals(childCount, node.childCount());
        assertEquals(childCount, node.getChildren().size());
        try {
            node.getChildren().add(null);
            fail(node.toString());
        } catch (UnsupportedOperationException x) {
        }
    }
}