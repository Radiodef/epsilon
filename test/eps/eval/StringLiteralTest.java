/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.test.*;
import eps.math.*;
import static eps.test.Eval.eval;
import static eps.test.Eval.newTest;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

public class StringLiteralTest extends EvalTester {
    public StringLiteralTest() {
        super(MathEnv64.class);
    }
    
    @Test
    public void testBasicStringLiteral() {
        testing();
        
        for (String bad : new String[] {
            "'\"",
            "\"'",
        }) {
            try {
                Operand result = eval(bad);
                fail(bad + " = " + result);
            } catch (EvaluationException x) {
            }
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest("abc123", "\"abc123\""),
            newTest("Hello, Epsilon!", "\"Hello, Epsilon!\""),
            newTest("Hello, Epsilon!", "'Hello, Epsilon!'"),
            newTest("null", "\"null\""),
            newTest("2 + 2", "\"2 + 2\""),
            newTest("", "\"\""),
            newTest("", "''"),
            newTest("'", "\"'\""),
            newTest("\"", "'\"'"),
            newTest("''", "\"''\""),
            newTest("\"\"", "'\"\"'"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testConcatenation() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest("", "'' + ''"),
            newTest("abc123", "'abc' + '123'"),
            newTest("abc123", "'abc' + 123"),
            newTest("12", "1 + '' + 2"),
            newTest("1/4+1/2", "0.25 + '+' + 0.5"),
            newTest("undefined", "'' + undefined"),
            newTest("[]", "'' + []"),
            newTest("[0, 1]", "([0] + [1]) + ''"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSubscript() {
        testing();
        
        for (String bad : new String[] {
            "'Abc' sub (-1)",
            "'Abc' sub 3",
            "'' sub 0",
        }) {
            boolean failed = true;
            try {
                Operand result = eval(bad);
                fail(bad + " = " + result);
            } catch (EvaluationException x) {
                failed = false;
            } finally {
                if (failed) {
                    System.out.println("    failed at " + bad);
                }
            }
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest("A", "'ABC' sub 0"),
            newTest("B", "'ABC' sub 1"),
            newTest("C", "'ABC' sub 2"),
            newTest("XZ", "('XYZ' sub 0) + ('XYZ' sub 2)"),
            newTest("Abc", "('ABC' sub 0) + 'bc'"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSizeOf() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(0, "sizeof \"\""),
            newTest(0, "sizeof ''"),
            newTest(1, "sizeof 'X'"),
            newTest(3, "sizeof 'xyz'"),
            newTest("Hello, Epsilon!".length(), "sizeof 'Hello, Epsilon!'"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
}