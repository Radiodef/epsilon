/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.test.*;
import eps.math.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Explanation:
 * <p>
 * Outer scopes should be "read-only", that is, inner scopes can read
 * the symbols from outer scopes, but a write to a symbol of the same
 * name creates a symbol in the inner scope which hides the outer symbol.
 * 
 * @author David
 */
public class NestedScopeTest extends SymbolTester {
    public NestedScopeTest() {
        super(MathEnv64.class);
    }
    
    @Test
    public void testNestedVariable() {
        testing();
        Symbols syms = newSymbols();
        Operand res;
        String  src;
        
        eval("x := 1", syms);
        assertTrue(syms.contains("x", Symbol.Kind.VARIABLE));
        
        eval("f(a) := (y := x; x := a; (x, y))", syms);
        assertTrue(syms.contains("f", Symbol.Kind.UNARY_OP));
        
        src = "f(7)";
        res = eval(src, syms);
        assertEquals(src, Tuple.of(env, 7, 1), res);
        assertFalse(syms.contains("y", Symbol.Kind.VARIABLE));
        
        assertEquals("global variable x should be unchanged", env.one(), eval("x", syms));
    }
    
    @Test
    public void testNestedOperator() {
        testing();
        Symbols syms = newSymbols();
        Operand res;
        String  src;
        
        src = "f(n) := ("
            + "    g(i) := for("
            + "        (i < n): ("
            + "            i + g(i + 1)"
            + "        ),"
            + "        i"
            + "    );"
            + "    g(1)"
            + ")";
        
        eval(src, syms);
        assertTrue(syms.contains("f", Symbol.Kind.UNARY_OP));
        assertFalse(syms.contains("g", Symbol.Kind.UNARY_OP));
        
        src = "f(5)";
        res = eval(src, syms); // 1+2+3+4+5 = 15
        assertEquals(src, env.valueOf(15), res);
        
        assertTrue(syms.contains("f", Symbol.Kind.UNARY_OP));
        assertFalse(syms.contains("g", Symbol.Kind.UNARY_OP));
    }
    
    @Test
    public void testDeepNesting() {
        testing();
        Symbols syms = newSymbols();
        Operand res;
        String  src;
        
        src = "f(a) := (" // a=1
            + "    x := 1;"
            + "    f(b) := (" // b=2
            + "        x := 2;"
            + "        f(c) := (" // c=3
            + "            x := 3;"
            + "            f(d) := (" // d=4
            + "                a * b * c * d" // 1*2*3*4 = 24
            + "            );"
            + "            y := f(c + 1);" // y=24
            + "            x + y" // 3+24 = 27
            + "        );"
            + "        y := f(b + 1);" // y=27
            + "        x + y" // 2+27 = 29
            + "    );"
            + "    y := f(a + 1);" // y=29
            + "    x + y" // 1+29 = 30
            + ")";
        eval(src, syms);
        
        Runnable containsAsserts = () -> {
            assertTrue(syms.contains("f", Symbol.Kind.UNARY_OP));
            assertFalse(syms.contains("x", Symbol.Kind.VARIABLE));
            assertFalse(syms.contains("a", Symbol.Kind.VARIABLE));
            assertFalse(syms.contains("b", Symbol.Kind.VARIABLE));
            assertFalse(syms.contains("c", Symbol.Kind.VARIABLE));
            assertFalse(syms.contains("d", Symbol.Kind.VARIABLE));
        };
        containsAsserts.run();
        
        src = "f(1)";
        res = eval(src, syms);
        assertEquals(src, env.valueOf(30), res);
        
        containsAsserts.run();
        assertEquals("(evaluation shouldn't change the function)", env.valueOf(30), eval(src, syms));
    }
}