/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.test.*;
import eps.math.*;
import static eps.test.Tools.*;
import static eps.eval.Undefinition.*;

import org.junit.*;
import static org.junit.Assert.*;

public class BinaryOpTest extends SymbolTester {
    public BinaryOpTest() {
        super(MathEnv64.class);
    }
    
    @Test
    public void testSimpleBinaryOp1() {
        testing();
        Symbols  syms = newSymbols();
        String   src  = "(a) op (b) := 2*a + 2*b";
        Operand  res  = eval(src, syms);
        BinaryOp op   = syms.get("op", BinaryOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        
        src = "4 op 7"; // 2*4 + 2*7 = 8 + 14 = 22
        assertEquals(src, env.valueOf(22), eval(src, syms));
        
        src = "x op y := (x + 1) * (y - 1)";
        res = eval(src, syms);
        op  = syms.get("op", BinaryOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        
        src = "-6 op 9"; // (-6 + 1) * (9 - 1) = -5 * 8 = -40
        assertEquals(src, env.valueOf(-40), eval(src, syms));
        
        assertEquals(undefined, eval("p op q := undefined", syms));
        assertFalse(syms.contains("op", Symbol.Kind.BINARY_OP));
    }
    
    @Test
    public void testSimpleBinaryOp2() {
        testing();
        Symbols  syms = newSymbols();
        String   src  = "m $ n := m + 100*n";
        Operand  res  = eval(src, syms);
        BinaryOp op   = syms.get("$", BinaryOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        
        src = "4 $ 7"; // 4 + 100*7 = 704
        assertEquals(src, env.valueOf(704), eval(src, syms));
        
        src = "t $ u := -t*4 - u";
        res = eval(src, syms);
        op  = syms.get("$", BinaryOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        
        src = "2 $ 96"; // -2*4 - 96 = -104
        assertEquals(src, env.valueOf(-104), eval(src, syms));
        
        assertEquals(undefined, eval("(c) $ (d) := undefined", syms));
        assertFalse(syms.contains("$", Symbol.Kind.BINARY_OP));
    }
    
    @Test
    public void testMultipleParameters() {
        testing();
        Symbols  syms = newSymbols();
        String   src  = "((a, b), x) $ ((c, d), y) := (a + c, b + d) * (x + y)";
        Operand  res  = eval(src, syms);
        BinaryOp op   = syms.get("$", BinaryOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        
        TreeFunction.Parameters params = ((BinaryOp.FromTree) op).getTreeFunction().getParameters();
        assertEquals(2, params.count());
        assertEquals(2, params.get(0).count());
        assertEquals(2, params.get(1).count());
        assertEquals(2, params.get(0).get(0).count());
        assertEquals(2, params.get(1).get(0).count());
        assertEquals(1, params.get(0).get(1).count());
        assertEquals(1, params.get(1).get(1).count());
        
        src = "((1, 2), -10) $ ((3, 4), 1)"; // (1 + 3, 2 + 4) * (-10 + 1) = (4, 6) * -9 = (-36, -54)
        assertEquals(src, Tuple.of(env, -36, -54), eval(src, syms));
        
        assertEquals(undefined, eval("a $ b := undefined", syms));
        assertFalse(syms.contains("$", Symbol.Kind.BINARY_OP));
    }
}