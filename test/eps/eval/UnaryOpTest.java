/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.math.*;
import eps.test.*;
import static eps.util.Literals.*;
import static eps.test.Tools.*;
import static eps.eval.Undefinition.undefined;

import org.junit.*;
import static org.junit.Assert.*;

public class UnaryOpTest extends SymbolTester {
    public UnaryOpTest() {
        super(MathEnv64.class);
    }
    
    @Test
    public void testUnaryFunction() {
        testing();
        Symbols syms   = newSymbols();
        String  source = "func(x) := 2*x";
        Operand result = eval(source, syms);
        UnaryOp unary  = syms.get("func", UnaryOp.class);
        
        assertEquals(UnaryOp.Affix.PREFIX, unary.getAffix());
        assertTrue(unary.isFunctionLike());
        
        assertEquals(source, unary, ((Reference) result).get());
        
        for (String test : ListOf("func(10)", "(func 10)", "func 10")) {
            assertEquals(test, env.valueOf(20), eval(test, syms));
        }
        
        source = "func(y) := y - 3";
        result = eval(source, syms);
        unary  = syms.get("func", UnaryOp.class);
        
        assertEquals(UnaryOp.Affix.PREFIX, unary.getAffix());
        assertTrue(unary.isFunctionLike());
        
        assertEquals(source, unary, ((Reference) result).get());
        String test = "func 77";
        assertEquals(test, env.valueOf(74), eval(test, syms));
        
        assertEquals(undefined, eval("func(z) := undefined", syms));
        assertFalse(syms.contains("func", Symbol.Kind.UNARY_OP));
    }
    
    @Test
    public void testUnaryPrefix() {
        testing();
        Symbols syms   = newSymbols();
        String  source = "~a := 100 - a";
        Operand result = eval(source, syms);
        UnaryOp unary  = syms.get("~", UnaryOp.class);
        
        assertEquals(UnaryOp.Affix.PREFIX, unary.getAffix());
        assertFalse(unary.isFunctionLike());
        
        assertEquals(source, unary, ((Reference) result).get());
        assertEquals(source, env.valueOf(-5), eval("~105", syms));
        
        source = "~b := not b";
        result = eval(source, syms);
        unary  = syms.get("~", UnaryOp.class);
        
        assertEquals(UnaryOp.Affix.PREFIX, unary.getAffix());
        assertFalse(unary.isFunctionLike());
        
        assertEquals(source, unary, ((Reference) result).get());
        assertEquals(source, eps.math.Boolean.FALSE, eval("~1", syms));
        
        assertEquals(undefined, eval("~x := undefined", syms));
        assertFalse(syms.contains("~", Symbol.Kind.UNARY_OP));
    }
    
    @Test
    public void testUnarySuffix() {
        testing();
        Symbols syms   = newSymbols();
        String  source = "p% := p / 100";
        Operand result = eval(source, syms);
        UnaryOp unary  = syms.get("%", UnaryOp.class);
        
        assertEquals(UnaryOp.Affix.SUFFIX, unary.getAffix());
        assertFalse(unary.isFunctionLike());
        
        assertEquals(source, unary, ((Reference) result).get());
        assertEquals(source, env.valueOf(1, 2), eval("50%", syms));
        assertEquals(source, env.valueOf(1, 4), eval("25%", syms));
        
        source = "q% := q * 100";
        result = eval(source, syms);
        unary  = syms.get("%", UnaryOp.class);
        
        assertEquals(UnaryOp.Affix.SUFFIX, unary.getAffix());
        assertFalse(unary.isFunctionLike());
        
        assertEquals(source, unary, ((Reference) result).get());
        assertEquals(source, env.valueOf(50), eval("0.5%", syms));
        assertEquals(source, env.valueOf(25), eval("0.25%", syms));
        
        assertEquals(undefined, eval("r% := undefined", syms));
        assertFalse(syms.contains("%", Symbol.Kind.UNARY_OP));
    }
    
    @Test
    public void testMultipleParameters() {
        testing();
        Symbols syms   = newSymbols();
        String  source = "f(a, b, c) := a * (b + c)";
        Operand result = eval(source, syms);
        UnaryOp unary  = syms.get("f", UnaryOp.class);
        
        TreeFunction.Parameters params = ((UnaryOp.FromTree) unary).getTreeFunction().getParameters();
        assertEquals(1, params.count());
        assertEquals(3, params.get(0).count());
        
        assertEquals(env.valueOf(14), eval("f(2, 3, 4)", syms)); // 2 * (3 + 4) = 14
        assertEquals(Tuple.of(env, 12, 9), eval("f([4, 3], 2, 1)", syms));
        
        for (String bad : new String[] {
            "f(0)",
            "f(1, 2)",
            "f(1, 2, 3, 4)",
            "(args := (2, 3, 4)); (f args)",
        }) {
            try {
                Operand r = eval(bad, syms);
                fail(bad + " = " + r);
            } catch (EvaluationException x) {
            }
        }
    }
    
    @Test
    public void testNestedParameters() {
        testing();
        Symbols syms   = newSymbols();
        String  source = "scale(a, (x, y)) := a * (x, y)";
        Operand result = eval(source, syms);
        UnaryOp unary  = syms.get("scale", UnaryOp.class);
        
        TreeFunction.Parameters params = ((UnaryOp.FromTree) unary).getTreeFunction().getParameters();
        assertEquals(1, params.count());
        assertEquals(2, params.get(0).count());
        assertEquals(1, params.get(0).get(0).count());
        assertEquals(2, params.get(0).get(1).count());
        
        assertEquals(Tuple.of(env, 18, 21), eval("scale(3, (6, 7))", syms));
    }
}