/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.test.*;
import eps.math.*;
import static eps.test.Eval.eval;
import static eps.test.Eval.newTest;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

public class SpecialExpressionsTest extends EvalTester {
    public SpecialExpressionsTest() {
        super(MathEnv64.class);
    }
    
    private final Operand undef = Undefinition.INSTANCE;
    
    @Test
    public void testSemicolon() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(2, "1; 2"),
            newTest(3, "1; 2; 3"),
            newTest(4, "1 + 1; 2 + 2"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testUndefined() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(undef, "undefined"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testConditionDo() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(1, "true: 1"),
            newTest(undef, "false: 1"),
            newTest(5, "2+2: 5"),
            newTest(undef, "2-2: 5"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testForConditions() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(Tuple.empty(), "for([])"), // only evaluates conditions if the operand is an explicit comma node
            newTest(1, "for(true: 1, 2)"),
            newTest(2, "for(false: 1, 2)"),
            newTest(3, "for(false: 1, false: 2, 3)"),
            newTest(3, "for(false: 1, false: 2, true: 3)"),
            newTest(undef, "for(false: 1, false: 2)"),
            newTest(3, "for(1+1: 3, 2+2: 5)"),
            newTest(5, "for(1-1: 3, 2+2: 5)"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testEmptyMultiply() {
        testing();
        
        try {
            for (boolean enabled : in(true, false)) {
                MiniSettings.setLocal(Setting.EMPTY_MULTIPLY, enabled);
                
                for (Eval.Test test : new Eval.Test[] {
                    newTest(env.multiply(env.valueOf(2), env.pi()), "2pi"),
                    newTest(env.multiply(env.valueOf(2), env.pi()), "pi(1 + 1)"),
                    newTest(env.multiply(env.valueOf(2), env.pi()), "(0.5 + 1.5)pi"),
                    newTest(env.valueOf(18), "3(2 + 4)"),
                    newTest(env.valueOf(18), "(2 + 4)3"),
                    newTest(env.valueOf(54), "3(2 + 4)3"),
                    newTest(env.valueOf(21), "(1 + 2)(3 + 4)"),
                    newTest(env.valueOf(264), "(11) (1 + 1) (1 + 11)"),
                    newTest(Tuple.of(env, 3, 6), "3(1, 2)"),
                    newTest(Tuple.of(env, 3, 6), "(1, 2)3"),
                    newTest(Tuple.of(env, 9, 18), "3(1, 2)3"),
                    newTest(env.valueOf(6), "2 sqrt 9"),
                    newTest(env.valueOf(360), "5! 3"),
                    newTest(env.valueOf(6), "2 3"), // a bit weird, but no particular reason it should fail
                    newTest(env.multiply(env.e(), env.pi()), "e pi"), // same as above
                }) {
                    try {
                        assertEquals(test.source, test.result, eval(test.source));
                        if (!enabled) {
                            fail(test.toString());
                        }
                    } catch (EvaluationException x) {
                        if (enabled) {
                            x.printStackTrace(System.out);
                            fail(test.toString());
                        }
                    }
                }
            }
        } finally {
            MiniSettings.setLocal(null);
        }
    }
    
    // TODO?
    //  sum
    //  product
    //  truthy
    //  relationals
}