/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.test.*;
import eps.math.*;
import static eps.test.Tools.*;
import static eps.eval.Undefinition.*;

import org.junit.*;
import static org.junit.Assert.*;

public class SpanOpTest extends SymbolTester {
    public SpanOpTest() {
        super(MathEnv64.class);
    }
    
    @Test
    public void testSymmetricSpanOp() {
        testing();
        Symbols syms = newSymbols();
        String  src  = "#x# := x + 1";
        Operand res  = eval(src, syms);
        SpanOp  op   = syms.get("#", SpanOp.class);
        
        assertFalse(syms.contains("##", Symbol.Kind.SPAN_OP));
        
        assertNotNull(op);
        assertTrue(op.isSymmetric());
        assertEquals(src, op, ((Reference) res).get());
        
        src = "#1 + 2#";
        assertEquals(env.valueOf(4), eval(src, syms));
        
        eval("#y# := y - 3", syms);
        src = "#8#";
        assertEquals(src, env.valueOf(5), eval(src, syms));
        src = "#(#10#)#";
        assertEquals(src, env.valueOf(4), eval(src, syms));
        
        assertEquals(undefined, eval("#z# := undefined", syms));
        assertFalse(syms.contains("#", Symbol.Kind.SPAN_OP));
    }
    
    @Test
    public void testAsymmetricSpanOp() {
        testing();
        Symbols syms  = newSymbols();
        String  src   = "XD(x)DX := x*2 + 1";
        Operand res   = eval(src, syms);
        SpanOp  left  = syms.get("XD", SpanOp.class);
        SpanOp  right = syms.get("DX", SpanOp.class);
        
        assertFalse(syms.contains("XDDX", Symbol.Kind.SPAN_OP));
        
        assertNotNull(left);
        assertNotNull(right);
        assertTrue(left.isLeft());
        assertTrue(right.isRight());
        assertEquals(left.getParent(), right.getParent());
        assertFalse(left.getParent().isSymmetric());
        assertEquals(src, left.getParent(), ((Reference) res).get());
        
        src = "XD4DX";
        assertEquals(src, env.valueOf(9), eval(src, syms));
        src = "XD XD 10 DX DX";
        assertEquals(src, env.valueOf(43), eval(src, syms));
        
        src = "XD y DX := (y + y) * y";
        eval(src, syms);
        src = "XD 2 DX";
        assertEquals(src, env.valueOf(8), eval(src, syms));
        
        assertEquals(undefined, eval("XD(z)DX := undefined", syms));
        assertFalse(syms.contains("XD", Symbol.Kind.SPAN_OP));
        assertFalse(syms.contains("DX", Symbol.Kind.SPAN_OP));
    }
    
    @Test
    public void testStringLiteral() {
        testing();
        Symbols syms = newSymbols();
        String  src  = "#val# := 0 + val + 0";
        Operand res  = eval(src, syms);
        SpanOp  op   = syms.get("#", SpanOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        assertFalse(op.isLiteral());
        
        // no way to do this through evaluation
        op = (SpanOp) op.configure(((SpanOp.Builder) op.createBuilder()).makeLiteral());
        assertTrue(op.isLiteral());
        syms.put(op);
        assertEquals(op, syms.get("#", SpanOp.class));
        
        src = "#chars#";
        assertEquals(src, new StringLiteral("0chars0"), eval(src, syms));
        
        src = "L(chars)R := chars + chars";
        res = eval(src, syms);
        
        op = syms.get("L", SpanOp.class).getParent();
        op = (SpanOp) op.configure(((SpanOp.Builder) op.createBuilder()).makeLiteral());
        syms.put(op);
        
        SpanOp left  = syms.get("L", SpanOp.class);
        SpanOp right = syms.get("R", SpanOp.class);
        
        assertNotNull(left);
        assertNotNull(right);
        assertEquals(left.getParent(), right.getParent());
        assertTrue(left.getParent().isLiteral());
        
        src = "L4R";
        assertEquals(src, new StringLiteral("44"), eval(src, syms));
        src = "L xD R";
        assertEquals(src, new StringLiteral(" xD  xD "), eval(src, syms));
        
        assertEquals(undefined, eval("#x# := undefined", syms));
        assertFalse(syms.contains("#", Symbol.Kind.SPAN_OP));
        
        assertEquals(undefined, eval("L x R := undefined", syms));
        assertFalse(syms.contains("L", Symbol.Kind.SPAN_OP));
        assertFalse(syms.contains("R", Symbol.Kind.SPAN_OP));
    }
    
    @Test
    public void testMultipleParameters() {
        testing();
        Symbols syms = newSymbols();
        String  src  = "#((a, b), (c, d))# := (a + c, b + d)";
        Operand res  = eval(src, syms);
        SpanOp  op   = syms.get("#", SpanOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        
        TreeFunction.Parameters params = ((SpanOp.FromTree) op).getTreeFunction().getParameters();
        assertEquals(1, params.count());
        assertEquals(2, params.get(0).count());
        assertEquals(2, params.get(0).get(0).count());
        assertEquals(2, params.get(0).get(1).count());
        assertEquals(1, params.get(0).get(0).get(0).count());
        assertEquals(1, params.get(0).get(0).get(1).count());
        assertEquals(1, params.get(0).get(1).get(0).count());
        assertEquals(1, params.get(0).get(1).get(1).count());
        
        src = "#(1, 2), (3, 4)#";
        assertEquals(src, Tuple.of(env, 4, 6), eval(src, syms));
        
        assertEquals(undefined, eval("#x# := undefined", syms));
        assertFalse(syms.contains("#", Symbol.Kind.SPAN_OP));
    }
}