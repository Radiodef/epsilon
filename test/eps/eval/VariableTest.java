/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.app.*;
import eps.test.*;
import eps.math.*;
import eps.math.Number;
import eps.util.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

public class VariableTest extends SymbolTester {
    public VariableTest() {
        super(MathEnv64.class);
    }
    
    public static class VarTest extends Eval.Test {
        public final String name;
        
        public VarTest(Operand result, String source, String name) {
            super(result, source);
            this.name = name;
        }
        
        protected static final ToString<VarTest> VAR_TEST_TO_STRING =
            TEST_TO_STRING.derive(VarTest.class)
                          .add("name", t -> t.name)
                          .build();
        @Override
        public String toString() {
            return VAR_TEST_TO_STRING.apply(this);
        }
    }
    
    private VarTest newTest(Operand result, String source, String name) {
        return new VarTest(result, source, name);
    }
    
    private VarTest newTest(long result, String source, String name) {
        return newTest(env.valueOf(result), source, name);
    }
    
    private Eval.Test newTest(long result, String source) {
        return Eval.newTest(result, source);
    }
    
    private Eval.Test newTest(Operand result, String source) {
        return Eval.newTest(result, source);
    }
    
    private Operand getValue(Symbol sym) {
        return ((Variable.FromTree) sym).getCachedValue();
    }
    
    @Test
    public void testSimpleVariables() {
        testing();
        Symbols syms = newSymbols();
        
        for (VarTest test : new VarTest[] {
            newTest( 1, "x := 1",     "x"),
            newTest( 4, "y := 2 + 2", "y"),
            newTest( 5, "z := x + y", "z"),
            newTest( 2, "x := 2",     "x"),
            newTest( 8, "y := 4 + 4", "y"),
            newTest(10, "z := x + y", "z"),
        }) {
            Operand result = eval(test.source, syms);
            assertTrue(test.source, result instanceof Reference);
            
            Variable var = syms.get(test.name, Variable.class);
            
            assertTrue(test.source, var instanceof Variable.FromTree);
            assertEquals(test.source, test.result, getValue(var));
            assertEquals(test.source, var, ((Reference) result).get());
        }
        
        for (VarTest test : new VarTest[] {
            newTest( 2, "x := undefined", "x"),
            newTest( 8, "y := undefined", "y"),
            newTest(10, "z := undefined", "z"),
        }) {
            assertEquals(test.source, test.result, eval(test.name, syms));
            
            Operand result = eval(test.source, syms);
            assertTrue(test.source, result.isUndefinition());
            
            Variable var = syms.get(test.name, Variable.class);
            assertNull(test.source, var);
            
            try {
                result = eval(test.name, syms);
                fail(test.name + " = " + result);
            } catch (EvaluationException x) {
            }
        }
    }
    
    @Test
    public void testMultipleVariables() {
        testing();
        
        Symbols syms = newSymbols();
        String  src  = "(a := 2); (b := 4); (c := 6)";
        
        Operand result = eval(src, syms);
        assertTrue(src, result instanceof Reference);
        
        Variable a = syms.get("a", Variable.class);
        Variable b = syms.get("b", Variable.class);
        Variable c = syms.get("c", Variable.class);
        
        assertTrue(src, a instanceof Variable.FromTree);
        assertTrue(src, b instanceof Variable.FromTree);
        assertTrue(src, c instanceof Variable.FromTree);
        
        assertEquals(src, c, ((Reference) result).get());
        
        assertEquals(src, env.valueOf(2), getValue(a));
        assertEquals(src, env.valueOf(4), getValue(b));
        assertEquals(src, env.valueOf(6), getValue(c));
    }
    
    @Test
    public void testUndefinition() {
        testing();
        Symbols syms = newSymbols();
        Operand result;
        
        result = eval("delete := __undefined__", syms);
        assertTrue(result instanceof Reference);
        
        Variable del = syms.get("delete", Variable.class);
        assertEquals(Undefinition.INSTANCE, getValue(del));
        assertEquals(del, ((Reference) result).get());
        
        result = eval("Abc := 601", syms);
        
        Variable abc = syms.get("Abc", Variable.class);
        assertEquals(env.valueOf(601), getValue(abc));
        
        result = eval("Abc := delete", syms);
        assertEquals(Undefinition.INSTANCE, result);
        
        assertFalse(syms.contains("Abc", Symbol.Kind.VARIABLE));
    }
    
    @Test
    public void testAmbiguousVariables() {
        testing();
        Symbols syms   = newSymbols();
        Operand result = eval("+ := 3", syms);
        Number  three  = env.valueOf(3);
        
        Variable plus = syms.get("+", Variable.class);
        assertEquals(three, getValue(plus));
        assertEquals(plus, ((Reference) result).get());
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 3,  "+"),
            newTest(-3, "-+"),
            newTest( 6, "+!"),
            newTest(env.ln(three), "ln +"),
            //
            newTest( 6, "+++"),
            newTest( 6, "++ +"),
            newTest( 6, "+ ++"),
            newTest( 6, "+ + +"),
            newTest( 9, "+++++"),
            newTest(12, "+++++++"),
            newTest(15, "+++++++++"),
            //
            newTest(9, "+*+"),
            newTest(9, "+ *+"),
            newTest(9, "+* +"),
            newTest(9, "+ * +"),
            //
            newTest(0, "++-+"),
            newTest(0, "++ -+"),
            newTest(0, "+ + -+"),
            //
            newTest(-6, "-++-+"),
            newTest(-6, "-+ + -+"),
            //
            newTest(0, "+-+"),
            newTest(0, "+ - +"),
            newTest(6, "+--+"),
            newTest(6, "+ - -+"),
        }) {
            assertEquals(test.source, test.result, eval(test.source, syms));
        }
        
        try {
            for (boolean enabled : in(true, false)) {
                MiniSettings.setLocal(Setting.EMPTY_MULTIPLY, enabled);
                try {
                    Operand r = eval("++", syms);
                    if (enabled) {
                        assertEquals("++", env.valueOf(9), r);
                    } else {
                        fail("++ was " + r);
                    }
                } catch (EvaluationException x) {
                    if (enabled) {
                        x.printStackTrace(System.err);
                        fail("++ failed");
                    }
                }
            }
        } finally {
            MiniSettings.setLocal(null);
        }
        
        assertEquals(Undefinition.INSTANCE, eval("+ := undefined", syms));
        assertFalse(syms.contains("+", Symbol.Kind.VARIABLE));
        
        try {
            Operand r = eval("+", syms);
            fail("+ was " + r);
        } catch (EvaluationException x) {
        }
    }
}