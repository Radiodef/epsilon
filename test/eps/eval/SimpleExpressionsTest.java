/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.util.*;
import eps.app.*;
import eps.math.*;
import eps.test.*;
import eps.math.Number;
import static eps.test.Eval.eval;
import static eps.test.Eval.newTest;
import static eps.test.Tools.*;
import static eps.util.Literals.*;

import org.junit.*;
import static org.junit.Assert.*;

public class SimpleExpressionsTest extends EvalTester {
    public SimpleExpressionsTest() {
        super(MathEnv64.class);
    }
    
    private Number valueOf(long n) {
        return env.valueOf(n);
    }
    
    private Number valueOf(long n, long d) {
        return env.valueOf(valueOf(n), valueOf(d));
    }
    
    private Tuple tupleOf(long... values) {
        return Tuple.of(env, values);
    }
    
    @Test
    public void testSimpleNumbers() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 0,  "0"),
            newTest( 0, "-0"),
            newTest( 0,  " 0 "),
            newTest( 0,  "00000"),
            newTest( 0,  "00.00"),
            newTest( 0,  "0."),
            newTest( 0,   ".0"),
            newTest( 0,  "0.0"),
            newTest( 1,  "1"),
            newTest( 1,  "1."),
            newTest( 1,  "1.0"),
            newTest( 1,  "1.00"),
            newTest(-1, "-1"),
            newTest(-1, "-1."),
            newTest(-1, "-1.0"),
            newTest(-1, "-1.00"),
            //
            newTest(-1, "- 1"),
            newTest( 1, "--1"),
            //
            newTest( 1, 4, "0.25"),
            newTest( 1, 4, "0.250"),
            newTest(-1, 2, "-0.5"),
            newTest( 1, 2,  "0.500"),
            //
            newTest(       100, "10_0"),
            newTest(10_000_000, "10_000_000"),
            newTest(         0, "_0"),
            newTest(         0, "0_"),
            newTest(         0, "_0_"),
            newTest(         1, "_1"),
            newTest(         1, "1_"),
            newTest(         1, "_1_"),
            newTest(       100, "1_0_0"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
        
        for (int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; ++i) {
            String source = String.valueOf(i);
            assertEquals(source, valueOf(i), eval(source));
        }
        
        for (String bad : ListOf("_", ".")) {
            try {
                Operand result = eval(bad);
                fail(bad + " = " + result);
            } catch (EvaluationException x) {
            }
        }
    }
    
    @Test
    public void testDecimalMark() {
        testing();
        
        try {
            MiniSettings.setLocal(MiniSettings.of(Setting.DECIMAL_MARK, CodePoint.valueOf(',')));
            
            assertEquals(valueOf(1, 2), eval("0,5"));
            assertEquals(valueOf(5, 4), eval("1,25"));
            assertEquals(valueOf(1),    eval("1,"));
            assertEquals(valueOf(1),    eval("1,0"));
            assertEquals(valueOf(-10),  eval("-10,0"));
            
            assertEquals(tupleOf(1, 1), eval("1,0,1"));
            assertEquals(tupleOf(1, 1), eval("1,0,1,0"));
            
            try {
                Operand result = eval(",");
                fail(String.valueOf(result));
            } catch (EvaluationException x) {
            }
        } finally {
            MiniSettings.setLocal(null);
        }
    }
    
    @Test
    public void testBinaryAndHexLiterals() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 0,  "0b0"),
            newTest( 1,  "0b1"),
            newTest( 1,  "0b01"),
            newTest( 0, "-0b0"),
            newTest(-1, "-0b1"),
            newTest(-1, "-0b01"),
            newTest(0b10, "0b10"),
            newTest(0b1000_0001, "0b1000_0001"),
            newTest(0b1111_0001, "0b1111_0001"),
            //
            newTest( 0,  "0x0"),
            newTest( 1,  "0x1"),
            newTest( 1,  "0x01"),
            newTest( 0, "-0x0"),
            newTest(-1, "-0x1"),
            newTest(-1, "-0x01"),
            newTest(0x10, "0x10"),
            newTest(0xDEAD_BEEFL, "0xDead_BEEF"),
            newTest(0x1111_FFFFL, "0x1111_FfFf"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
        
        for (String bad : new String[] {
            "0b",
            "0b_",
            "0b2",
            "0x",
            "0x_",
            "0xG",
        }) {
            try {
                Operand result = eval(bad);
                fail(bad + " = " + result);
            } catch (EvaluationException x) {
            }
        }
    }
    
    @Test
    public void testSimpleAddition() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(0, "0 + 0"),
            newTest(2, "1 + 1"),
            newTest(1, "1 + 0"),
            newTest(1, "0 + 1"),
            newTest(4, "2 + 2"),
            newTest(9, "4 + 5"),
            //
            newTest(  2,  "4 + -2"),
            newTest(-10, "-8 + -2"),
            newTest( -3, "-6 +  3"),
            //
            newTest( 9, "2 + 3 + 4"),
            newTest(26, "5 + 6 + 7 + 8"),
            //
            newTest(   1, "0.5 + 0.5"),
            newTest(3, 4, "0.5 + 0.25"),
        }) {
            boolean failed = true;
            try {
                assertEquals(test.source, test.result, eval(test.source));
                failed = false;
            } finally {
                if (failed) {
                    System.out.println("    failed at " + test);
                }
            }
        }
    }
    
    @Test
    public void testSimpleSubtraction() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 0,  "0 - 0"),
            newTest( 0,  "1 - 1"),
            newTest( 1,  "1 - 0"),
            newTest(-1,  "0 - 1"),
            newTest( 1,  "3 - 2"),
            newTest(-1,  "2 - 3"),
            newTest( 9, "15 - 6"),
            //
            newTest(-5, "-2 -  3"),
            newTest( 2, "-2 - -4"),
            newTest( 7,  "2 - -5"),
            //
            newTest(-6, "9 - 8 - 7"),
            newTest(-6, "6 - 5 - 4 - 3"),
            //
            newTest(   1, "1.5 - 0.5"),
            newTest(1, 4, "0.5 - 0.25"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimpleMultiplication() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 0, "0 * 0"),
            newTest( 1, "1 * 1"),
            newTest( 0, "1 * 0"),
            newTest( 0, "0 * 1"),
            newTest( 6, "3 * 2"),
            newTest( 9, "9 * 1"),
            newTest(24, "4 * 6"),
            //
            newTest( -6, "-2 *  3"),
            newTest(  8, "-2 * -4"),
            newTest(-10,  "2 * -5"),
            //
            newTest(  24, "2 * 3 * 4"),
            newTest(1680, "5 * 6 * 7 * 8"),
            //
            newTest(   2, "4 * 0.5"),
            newTest(1, 8, "0.5 * 0.25"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimpleDivision() {
        testing();
        
        try {
            Operand op = eval("1 / 0");
            fail("1 / 0 = " + op);
        } catch (EvaluationException x) {
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 0, "0 / 1"),
            newTest( 1, "1 / 1"),
            newTest( 1, "2 / 2"),
            newTest( 3, "6 / 2"),
            newTest( 3, "9 / 3"),
            //
            newTest(-2, "-8 /  4"),
            newTest( 2, "-8 / -4"),
            newTest(-2,  "8 / -4"),
            //
            newTest(2,  "20 / 5 / 2"),
            newTest(5, "100 / 2 / 5 / 2"),
            //
            newTest(1, 2, "1 / 2"),
            newTest(1, 2, "4 / 8"),
            newTest(2, "0.5 / 0.25"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimpleExponentiation() {
        testing();
        
        try {
            Operand op = eval("0^0");
            fail("0^0 = " + op);
        } catch (EvaluationException x) {
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 0,  "0 ^ 1"),
            newTest( 0,  "0 ^ 10"),
            newTest( 1,  "1 ^ 1"),
            newTest( 8,  "2 ^ 3"),
            newTest(-8, "-2 ^ 3"),
            newTest( 1,  "6 ^ 0"),
            newTest(81,  "3 ^ 4"),
            //
            newTest(262_144, "4 ^ 3 ^ 2"),
            newTest( 65_536, "2 ^ 2 ^ 2 ^ 2"),
            //
            newTest(1,  8, "2 ^ -3"),
            newTest(1, 16, "0.5 ^ 4"),
            newTest(9, "81 ^ 0.5"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimpleFactorial() {
        testing();
        
        try {
            Operand op = eval("(-1)!");
            fail("(-1)! = " + op);
        } catch (EvaluationException x) {
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(  1, "0!"),
            newTest(  1, "1!"),
            newTest(  2, "2!"),
            newTest(  6, "3!"),
            newTest( 24, "4!"),
            newTest(120, "5!"),
            newTest(720, "6!"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimpleAbsoluteValue() {
        testing();
        
        for (String bad : ListOf("|", "||", "|1", "1|", "|-1", "-1|")) {
            try {
                Operand r = eval(bad);
                fail(bad + " = " + r);
            } catch (EvaluationException x) {
            }
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest( 0,   "|0|"),
            newTest( 1,   "|1|"),
            newTest(10,  "|10|"),
            newTest( 1,  "|-1|"),
            newTest(10, "|-10|"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimplePrecedence() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(    -79, "2 - 3 ^ 4"),
            newTest(     17, "4 + 7 * 2 - 1"),
            newTest(     42, "6 + 4 * 3 ^ 2"),
            newTest(     25, "4 * 4 + 3 * 3"),
            newTest(     14, "2 + 3! * 2"),
            newTest( 1, 576, "4! ^ -2"),
            newTest(   -120, "-5!"),
            newTest(    -48, "6 + 2 * -3 ^ 3"),  // note: -3^3 == -(3^3)
            newTest(-40_320, "-2 ^ 3!"),         // note: -2^3! == -((2^3)!)
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimpleVariables() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(env.pi(), "pi"),
            newTest(env.e(),  "e"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSimpleFunctions() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(0, "log(1)"),
            newTest(1, "log(10)"),
            newTest(2, "log 100"),
            newTest(3, "log 1000"),
            newTest(4, "log10000"), // should split the tokens during parsing
            //
            newTest(  0, "log(2, 1)"),
            newTest(  1, "log(2, 2)"),
            newTest(  5, "log(2, 32)"),
            newTest(  4, "log(3, 81)"),
            newTest(  4, "log(4, 256)"),
            //
            newTest(0, "sqrt(0)"),
            newTest(1, "sqrt(1)"),
            newTest(2, "sqrt(4)"),
            newTest(3, "sqrt 9"),
            newTest(4, "sqrt 16"),
            newTest(5, "sqrt 25"),
            newTest(6, "sqrt36"),       // should split the tokens during parsing
            //
            newTest(6, "sqrt(9)!"),     // should always group like (sqrt(9))!
            newTest(6, "log 100  ^ 3"), // precedence(^) > precedence(log)
            newTest(8, "log(100) ^ 3"), // should always group like (log(100))^3
            //
            newTest(8, "sqrt 16 + sqrt 16"), // grouped like (sqrt 16) + (sqrt 16)
            newTest(6, "log 100 ^ sqrt 9"),  // grouped like log(100 ^ (sqrt 9))
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
        
        for (String bad : new String[] {
            "sqrt(-1)",
            "log(-1)",
            "log( 0)",
            "log( 0,  1)",
            "log( 1,  1)",
            "log(-1,  1)",
            "log( 2,  0)",
            "log( 2, -1)",
        }) {
            try {
                Operand r = eval(bad);
                fail(bad + " = " + r);
            } catch (EvaluationException x) {
            }
        }
    }
    
    @Test
    public void testFailures() {
        testing();
        
        for (String bad : new String[] {
            "x",
            "$",
            "-",
            "1, ",
            "1-",
            "1 - ",
            "1+",
            "1 + ",
            "+1",
            "+1-",
            "-1+",
            "(1",
            "1)",
        }) {
            try {
                Operand r = eval(bad);
                fail(bad + " = " + r);
            } catch (EvaluationException x) {
            }
        }
    }
}