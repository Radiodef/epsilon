/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.test.*;
import eps.math.*;
import static eps.test.Eval.eval;
import static eps.test.Eval.newTest;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

public class TupleTest extends EvalTester {
    public TupleTest() {
        super(MathEnv64.class);
    }
    
    private Tuple tupleOf(long... values) {
        return Tuple.of(env, values);
    }
    
    @Test
    public void testBasicTuple() {
        testing();
        
        for (String bad : new String[] {
            "[",
            "]",
            ",",
            "[,",
            ",]",
        }) {
            try {
                Operand result = eval(bad);
                fail(bad + " = " + result);
            } catch (EvaluationException ignored) {
            }
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(tupleOf(), "[]"),
            newTest(tupleOf(0), "[0]"),
            newTest(tupleOf(1, 2), "1, 2"),
            newTest(tupleOf(1, 2), "(1, 2)"),
            newTest(tupleOf(1, 2), "[1, 2]"),
            newTest(Tuple.of(tupleOf()), "[[]]"),
            newTest(Tuple.of(tupleOf(1, 2)), "[[1, 2]]"),
            newTest(Tuple.of(tupleOf(1), tupleOf(2)), "[[1], [2]]"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testConcatenation() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(tupleOf(), "[] + []"),
            newTest(tupleOf(1), "[] + 1"),
            newTest(tupleOf(1, 2, 3), "(1, 2) + 3"),
            newTest(tupleOf(1, 2, 3), "[1] + [2] + [3]"),
            newTest(tupleOf(1, 2, 3), "[1] + [2, 3]"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testOperations() {
        testing();
        
        for (String bad : new String[] {
            "[] * []",
            "[1] * [2]",
            "2 / [1]",
        }) {
            try {
                Operand result = eval(bad);
                fail(bad + " = " + result);
            } catch (EvaluationException ignored) {
            }
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(tupleOf(2, 4, 6), "2 * (1, 2, 3)"),
            newTest(tupleOf(1, 2, 3), "(2, 4, 6) / 2"),
            newTest(tupleOf(32), "[16] * 2"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSizeOf() {
        testing();
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(0, "sizeof []"),
            newTest(1, "sizeof [1]"),
            newTest(2, "sizeof (1, 2)"),
            newTest(2, "sizeof [1, 2]"),
            newTest(3, "sizeof (1, 2, 3)"),
            newTest(3, "sizeof ([1] + [2] + [3])"),
            newTest(3, "sizeof ((1, 2) + 3)"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testSubscript() {
        testing();
        
        for (String bad : new String[] {
            "[] sub 0",
            "[1, 2] sub (-1)",
            "[1, 2] sub 2",
        }) {
            try {
                Operand result = eval(bad);
                fail(bad + " = " + result);
            } catch (EvaluationException ignored) {
            }
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(0, "[0] sub 0"),
            newTest(1, "(1, 2, 3) sub 0"),
            newTest(2, "(1, 2, 3) sub 1"),
            newTest(3, "(1, 2, 3) sub 2"),
            newTest(2, "((1, 2) + 3) sub 1"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
    
    @Test
    public void testMap() {
        testing();
        
        try {
            Operand r = eval("(1, 2) -> (2 + e)");
            fail(String.valueOf(r));
        } catch (EvaluationException ignored) {
        }
        
        for (Eval.Test test : new Eval.Test[] {
            newTest(tupleOf(1, 2, 3, 4), "(1, 2, 3, 4) -> (a := a)"),
            newTest(tupleOf(3, 4, 5, 6), "(1, 2, 3, 4) -> (b := b + 2)"),
            newTest(tupleOf(2, 3, 4, 5), "(4, 9, 16, 25) -> (c := sqrt c)"),
        }) {
            assertEquals(test.source, test.result, eval(test.source));
        }
    }
}