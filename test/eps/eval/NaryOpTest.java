/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.eval;

import eps.test.*;
import eps.math.*;
import static eps.math.Boolean.*;
import static eps.test.Tools.*;
import static eps.eval.Undefinition.*;

import org.junit.*;
import static org.junit.Assert.*;

public class NaryOpTest extends SymbolTester {
    public NaryOpTest() {
        super(MathEnv64.class);
    }
    
    @Test
    public void testSimpleNaryOp() {
        testing();
        Symbols syms = newSymbols();
        String  src  = "t ~ ... := -t";
        Operand res  = eval(src, syms);
        NaryOp  op   = syms.get("~", NaryOp.class);
        
        assertNotNull(op);
        assertEquals(src, op, ((Reference) res).get());
        
        src = "1 ~ 2 ~ 3 ~ 4";
        assertEquals(Tuple.of(env, -1, -2, -3, -4), eval(src, syms));
        
        src = "(5, 6, 7, 8) ~ ...";
        assertEquals(Tuple.of(env, -5, -6, -7, -8), eval(src, syms));
        
        src = "[] ~ ...";
        assertEquals(Tuple.empty(), eval(src, syms));
        
        eval("u ~ ... := (u -> (e := e?))", syms);
        src = "0 ~ -2 ~ pi ~ 1*0";
        assertEquals(src, Tuple.of(FALSE, TRUE, TRUE, FALSE), eval(src, syms));
        
        assertEquals(undefined, eval("v ~ ... := undefined", syms));
        assertFalse(syms.contains("~", Symbol.Kind.NARY_OP));
    }
    
    @Test
    public void testMultipleParameters() {
        testing();
        // not allowed presently (should fail)
        Symbols syms = newSymbols();
        String  src  = "((a, b) $ ...) := 0";
        try {
            Operand res = eval(src, syms);
            fail(src + " = " + res);
        } catch (EvaluationException x) {
        }
        assertFalse(syms.contains("$", Symbol.Kind.NARY_OP));
    }
}