/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import eps.json.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PersistentObjectFileTest {
    public PersistentObjectFileTest() {
    }
    
    @Test
    public void testConstructor() {
        testing();
        
        PersistentDirectory dir = new PersistentDirectory("dir");
        
        try {
            new PersistentObjectFile(dir, "test", null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            new PersistentObjectFile(dir, null, String.class);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            new PersistentObjectFile(dir, "", String.class);
            fail();
        } catch (IllegalArgumentException x) {
        }
        
        new PersistentObjectFile(null, "test", String.class);
    }
    
    @Test
    public void testCreation() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        class TestStringFile extends PersistentObjectFile {
            TestStringFile() {
                super("test_string_file", String.class);
            }
            @Override
            protected String getDefaultValue() {
                return "Hello, world!";
            }
        }
        TestStringFile pf;
        
        pf = new TestStringFile();
        
        Path expectedPath = Paths.get(pf.getName() + "." + pf.getExtension());
        assertFalse(fs.exists(expectedPath));
        
        Mutable<Object> contents = new Mutable<>();
        
        assertTrue("testing initial creation", pf.readOrCreate(contents::set));
        assertEquals(pf.getDefaultValue(), contents.value);
        
        assertTrue(fs.exists(expectedPath));
        assertFalse(fs.isDirectory(expectedPath));
        assertEquals(expectedPath, pf.getPath());
        assertTrue(pf.getPath() == pf.getPath());
        
        assertTrue("testing subsequent read", pf.readOrCreate(contents::set));
        assertEquals(pf.getDefaultValue(), contents.value);
        
        pf = new TestStringFile();
        
        assertTrue("testing new initializing read after creation", pf.readOrCreate(contents::set));
        assertEquals(pf.getDefaultValue(), contents.value);
        
        String newValue = "Hello, eps.io!";
        fs.write(expectedPath, Json.toBytes(newValue, true));
        
        pf = new TestStringFile();
        
        assertTrue("testing new initializing read after modification", pf.readOrCreate(contents::set));
        assertEquals(newValue, contents.value);
        
        assertTrue(fs.exists(expectedPath));
        assertEquals(expectedPath, pf.getPath());
    }
    
    @Test
    public void testDynamicNaming() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        String testFileName = "test_integer_file";
        
        class TestIntegerFile extends PersistentObjectFile {
            private final boolean isNullValid;
            TestIntegerFile(boolean isNullValid) {
                super(testFileName, Integer.class);
                this.isNullValid = isNullValid;
            }
            @Override
            protected Integer getDefaultValue() {
                return 0;
            }
            @Override
            protected boolean isNullValid() {
                return isNullValid;
            }
        }
        
        String dext = PersistentObjectFile.DOT_EXTENSION;
        
        fs.createDirectory(Paths.get(testFileName + dext));
        fs.write(Paths.get(testFileName + "1" + dext), Json.toBytes("abc123", true));
        fs.write(Paths.get(testFileName + "2" + dext), new byte[0]);
        fs.write(Paths.get(testFileName + "3" + dext), Json.toBytes(null, true));
        
        Path expectedPath = Paths.get(testFileName + "4" + dext);
        assertFalse(fs.exists(expectedPath));
        
        TestIntegerFile pf = new TestIntegerFile(false);
        
        Mutable<Object> contents = new Mutable<>();
        
        assertTrue("testing initial creation", pf.readOrCreate(contents::set));
        assertEquals(pf.getDefaultValue(), contents.value);
        assertEquals(expectedPath, pf.getPath());
        assertTrue(fs.exists(expectedPath));
        
        pf = new TestIntegerFile(false);
        
        assertTrue("testing subsequent creation", pf.readOrCreate(contents::set));
        assertEquals(pf.getDefaultValue(), contents.value);
        assertEquals(expectedPath, pf.getPath());
        
        pf = new TestIntegerFile(true);
        
        assertTrue("testing subsequent creation while allowing null", pf.readOrCreate(contents::set));
        assertEquals(null, contents.value);
        assertEquals("this should change which file is accepted", Paths.get(testFileName + "3" + dext), pf.getPath());
        
        fs.printFileSystem(1);
    }
    
    @Test
    public void testFailure() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        String testFileName = "test_file_name";
        
        class TestObjectFile extends PersistentObjectFile {
            TestObjectFile() {
                super(testFileName, Object.class);
            }
            @Override
            protected Object getDefaultValue() {
                return "";
            }
        }
        
        fs.putExcept("write", new IOException("test thrown except 1"));
        
        TestObjectFile pf = new TestObjectFile();
        
        Mutable<Object> contents = new Mutable<>();
        
        assertFalse("write should throw", pf.readOrCreate(contents::set));
        assertNull(contents.value);
        assertNull(pf.getPath());
        
        Path expectedPath = Paths.get(testFileName + PersistentObjectFile.DOT_EXTENSION);
        
        fs.write(expectedPath, Json.toBytes("X", false));
        
        assertFalse("subsequent reads should fail, even if the file now exists", pf.readOrCreate(contents::set));
        assertNull(contents.value);
        assertNull(pf.getPath());
        
        fs.putExcept("readAllBytes", new IOException("test thrown except 2"));
        
        pf = new TestObjectFile();
        
        assertFalse("read should throw", pf.readOrCreate(contents::set));
        assertNull(contents.value);
        assertNull(pf.getPath());
        
        pf = new TestObjectFile();
        
        assertTrue("finally, this ought to work", pf.readOrCreate(contents::set));
        assertEquals("X", contents.value);
        assertEquals(expectedPath, pf.getPath());
    }
    
    @Test
    public void testWrite() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        String testFileName = "test_file_name";
        
        class TestObjectFile extends PersistentObjectFile {
            TestObjectFile() {
                super(testFileName, Object.class);
            }
        }
        
        Path expectedPath = Paths.get(testFileName + TestObjectFile.DOT_EXTENSION);
        assertFalse(fs.exists(expectedPath));
        
        TestObjectFile file = new TestObjectFile();
        assertNull("default is null", file.getContents());
        
        assertEquals(expectedPath, file.getPath());
        assertTrue(fs.isRegularFile(expectedPath));
        
        String helloWorld = "Hello, world!";
        
        Mutable<BackupFile> backup = new Mutable<>();
        
        file.putContents(helloWorld, backup::set);
        assertEquals(helloWorld, file.getContents());
        
        assertNotNull(backup.value);
        assertTrue(backup.value.backupSuccess());
        assertFalse(fs.exists(backup.value.getBackup()));
        
        assertTrue(fs.deleteIfExists(expectedPath));
        assertFalse(fs.exists(expectedPath));
        
        Integer zero = 0;
        
        backup.set(null);
        
        assertFalse("putContents shouldn't write to a non-existent file", file.putContents(zero, backup::set));
        assertTrue(file.errorIsSet());
        assertNull(file.getPath());
        
        assertNotNull("an attempt to backup should occur", backup.value);
        assertFalse("but no backup actually performed", backup.value.backupSuccess());
        assertNull(backup.value.getBackup());
        
        file = new TestObjectFile();
        assertNull("default is null", file.getContents());
        
        assertEquals(expectedPath, file.getPath());
        assertTrue(fs.isRegularFile(expectedPath));
        
        backup.set(null);
        
        assertTrue(file.putContents(zero, backup::set));
        assertEquals(zero, file.getContents());
        
        assertNotNull(backup.value);
        assertTrue(backup.value.backupSuccess());
        assertNotNull(backup.value.getBackup());
        assertFalse(fs.exists(backup.value.getBackup()));
        
        backup.set(null);
        
        fs.putExcept("write", new IOException("write failed"));
        assertFalse("write should fail per above", file.putContents(helloWorld, backup::set));
        
        assertNotNull(backup.value);
        assertTrue(backup.value.backupSuccess());
        assertNotNull(backup.value.getBackup());
        assertEquals(zero, Json.fromBytes(fs.readAllBytes(backup.value.getBackup())));
        
        assertTrue(file.errorIsSet());
        assertNull(file.getPath());
        
        backup.set(null);
        
        assertNull("read should be skipped with error; default is null", file.getContents());
        assertFalse("subsequent writes should fail as well", file.putContents(helloWorld, backup::set));
        
        assertNull("failure should occur before attempted backup", backup.value);
        
        assertEquals("file should be unchanged", zero, Json.fromBytes(fs.readAllBytes(expectedPath)));
        assertEquals("file should be unchanged", zero, new TestObjectFile().getContents());
        
        file = new TestObjectFile();
        assertEquals(zero, file.getContents());
        
        class NotSerializable {
        }
        
        backup.set(null);
        
        assertFalse(file.putContents(new NotSerializable(), backup::set));
        assertTrue(file.errorIsSet());
        
        assertNull("JSON conversion failure should occur before the backup", backup.value);
    }
}