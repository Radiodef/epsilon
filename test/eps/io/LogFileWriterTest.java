/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import eps.app.Log.*;
import static eps.test.Tools.*;
import static eps.util.Literals.*;
import static eps.io.IoMisc.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import static java.util.stream.Collectors.*;

import java.nio.file.Path;
import java.io.IOException;

@SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
public class LogFileWriterTest {
    public LogFileWriterTest() {
    }
    
    private MockFileSystem fs;
    
    @Before
    public void initFileSystem() {
        FileSystem.setInstance(fs = new MockFileSystem());
        PersistentFilesTest.resetConstants();
    }
    
    @After
    public void clearFileSystem() {
        FileSystem.setInstance(fs = null);
    }
    
    private static final String LN_SEP = System.lineSeparator();
    
    private static String line(String line) {
        return line + LN_SEP;
    }
    
    private static String join(List<String> lines) {
        return lines.stream().map(LogFileWriterTest::line).collect(joining());
    }
    
    private static String joinAndEnd(List<String> lines) {
        return join(lines) + line("LogFileWriter.close() invoked");
    }
    
    private static final ConsumerConsumer EMPTY_CONSUMER = action -> {};
    
    @Test(timeout = 30_000)
    public void test() throws IOException {
        testing();
        
        List<String> lines =
            ListOf("hello, world!",
                   "hello, log!",
                   "hello, goodbye!");
        
        ConsumerConsumer fakeConsumer = new ConsumerConsumer() {
            ListDeque<Line> listDeque = new ListDeque<>();
            {
                for (String line : lines) {
                    listDeque.add(new Line("FakeName", line));
                }
            }
            @Override
            public synchronized void accept(Consumer<? super ListDeque<Line>> cons) {
                if (listDeque != null) {
                    cons.accept(listDeque);
                    listDeque = null;
                }
            }
        };
        
        Path path;
        
        LogFileWriter lfw = new LogFileWriter(fakeConsumer);
        try {
            assertFalse(lfw.isAlive());
            assertTrue(lfw.isOpen());
            
            lfw.start();
            assertTrue(lfw.isAlive());
            assertTrue(lfw.isOpen());
            
            path = lfw.getPath();
            assertNotNull(path);
            System.out.println("    path: " + abs(path));
        } finally {
            lfw.close();
        }
        
        assertFalse(lfw.isAlive());
        assertFalse(lfw.isOpen());
        
        assertTrue(fs.exists(path));
        assertTrue(fs.isRegularFile(path));
        assertTrue(LogFileWriter.isLogFile(path));
        
        String expect = joinAndEnd(lines);
        String actual = fs.readString(path);
        
        assertEquals(expect, actual);
        
        System.out.println("log file contents:");
        System.out.print(actual);
        
        lfw.close();
        lfw.close();
        assertEquals(expect, fs.readString(path));
    }
    
    @Test(timeout = 30_000)
    public void testMultiLine() throws IOException {
        testing();
        
        List<String> lines = IntStream.range(0, 100).mapToObj(i -> "line" + i).collect(toList());
        MutableInt   index = new MutableInt(0);
        
        ConsumerConsumer fakeConsumer = action -> {
            synchronized (index) {
                if (index.value < lines.size()) {
                    ListDeque<Line> line = new ListDeque<>(1);
                    line.add(new Line("FakeName", lines.get(index.value)));
                    action.accept(line);
                    index.increment();
                } else {
                    index.notifyAll();
                }
            }
        };
        
        Path path;
        
        try (LogFileWriter lfw = new LogFileWriter(fs, 10, fakeConsumer)) {
            path = lfw.getPath();
            
            synchronized (index) {
                lfw.start();
                
                while (index.value < lines.size()) {
                    Misc.wait(index);
                }
            }
        }
        
        assertTrue(fs.isRegularFile(path));
        assertTrue(LogFileWriter.isLogFile(path));
        
        assertEquals(joinAndEnd(lines), fs.readString(path));
    }
    
    @Test(timeout = 30_000)
    public void testMultipleFiles() throws IOException {
        testing();
        System.out.println("    pattern: " + LogFileWriter.getLogFilePattern());
        
        List<Path> paths = new ArrayList<>();
        assertEquals(paths, LogFileWriter.getLogFiles());
        
        int count = 100;
        try (FieldSetter ignored = disableLog()) {
            for (int i = 0; i < count; ++i) {
                try (LogFileWriter lfw = new LogFileWriter(EMPTY_CONSUMER)) {
                    lfw.start();
                    
                    Path path = lfw.getPath();
                    assertNotNull(path);
                    paths.add(abs(path));
                    
                    if (i > count / 2) {
                        Misc.sleep(20);
                    }
                }
            }
        }
        
        for (Path path : paths) {
            assertTrue(fs.isRegularFile(path));
            assertTrue(path.toString(), LogFileWriter.isLogFile(path));
            assertEquals(joinAndEnd(Collections.emptyList()), fs.readString(path));
        }
        
        // [ invalid log file setup start ]
        
        Path logDir = PersistentFiles.LOG_DIRECTORY.getPath();
        assertNotNull(logDir);
        
        // set up for creating a directory with a name which is a valid log file name
        String logFileName;
        do {
            logFileName = LogFileWriter.createFileNameForNow() + LogFileWriter.DOT_EXTENSION;
            assertTrue(LogFileWriter.isLogFile(logFileName));
            // note:
            //  it's possible we're still in the same second as the last log file created above
            Misc.sleep(100);
        } while (fs.exists(logDir.resolve(logFileName)));
        
        // none of these should be valid log files
        Path notLogA = logDir.resolve("not_log_file");                                        // bad name
        Path notLogB = logDir.resolve(LogFileWriter.FILE_NAME);                               // bad name
        Path notLogC = logDir.resolve(LogFileWriter.FILE_NAME + LogFileWriter.DOT_EXTENSION); // no date
        Path notLogD = logDir.resolve(logFileName);                                           // directory
        Path notLogE = logDir.resolve(logFileName).resolve(logFileName);                      // in subdirectory
        
        fs.write(notLogA, new byte[0]);
        fs.write(notLogB, line("abc").getBytes());
        fs.write(notLogC, line("123").getBytes(fs.getDefaultCharset()));
        fs.createDirectory(notLogD);
        fs.write(notLogE, line("xyz").getBytes(fs.getDefaultCharset()));
        
        List<Path> notLogs = ListOf(notLogA, notLogB, notLogC, notLogD, notLogE);
        
        for (Path notLog : notLogs) {
            assertFalse(LogFileWriter.isLogFile(notLog));
        }
        
        // [ invalid log file setup end ]
        
        List<Path> logs = LogFileWriter.getLogFiles().stream().map(IoMisc::abs).collect(toList());
        assertEquals(count, logs.size());
        
        for (Path notLog : notLogs) {
            assertTrue(logs.stream().noneMatch(notLog::equals));
        }
        
        assertEquals(new HashSet<>(paths), new HashSet<>(logs));
    }
    
    @Test(timeout = 30_000)
    public void testWriteFailure() throws IOException {
        testing();
        
        List<String> lines =
            ListOf("line 1",
                   "line 2",
                   "line 3",
                   "line 4",
                   "line 5",
                   "line 6");
        
        ConsumerConsumer fakeConsumer = new ConsumerConsumer() {
            int i = 0;
            @Override
            public synchronized void accept(Consumer<? super ListDeque<Line>> action) {
                if (i >= lines.size() / 2) {
                    fs.putExcept("OutputStream.write", new IOException(Integer.toString(i)));
                }
                if (i < lines.size()) {
                    ListDeque<Line> deque = new ListDeque<>();
                    deque.add(new Line("FakeName", lines.get(i++)));
                    action.accept(deque);
                }
            }
        };
        
        Path path;
        
        try (LogFileWriter lfw = new LogFileWriter(fs, 10, fakeConsumer)) {
            path = lfw.getPath();
            synchronized (fakeConsumer) {
                lfw.start();
                assertTrue(lfw.isAlive());
            }
            
            while (lfw.isAlive()) {
                Misc.sleep(50);
            }
            
            assertTrue(lfw.isOpen());
        }
        
        assertEquals(joinAndEnd(lines.subList(0, lines.size() / 2)), fs.readString(path));
    }
    
    @Test(timeout = 30_000)
    public void testCloseFailure() throws IOException {
        testing();
        
        List<String> lines = IntStream.range(0, 10).mapToObj(i -> "line" + i).collect(toList());
        
        Mutable<ListDeque<Line>> deque = new Mutable<>();
        deque.value = lines.stream().map(ln -> new Line("Name", ln)).collect(toCollection(ListDeque::new));
        
        ConsumerConsumer fakeConsumer = new ConsumerConsumer() {
            @Override
            public synchronized void accept(Consumer<? super ListDeque<Line>> action) {
                if (deque.value != null) {
                    action.accept(deque.value);
                    deque.value = null;
                    notifyAll();
                }
            }
        };
        
        fs.putExcept("OutputStream.close", new IOException("close failed"));
        
        LogFileWriter lfw = new LogFileWriter(fs, 10, fakeConsumer);
        try {
            synchronized (fakeConsumer) {
                lfw.start();
                
                while (deque.value != null) {
                    Misc.wait(fakeConsumer);
                }
            }
        } finally {
            lfw.close();
        }
        
        for (int i = 0; i < 2; ++i) {
            assertFalse(lfw.isOpen());
            assertFalse(lfw.isAlive());
            assertEquals(joinAndEnd(lines), fs.readString(lfw.getPath()));
            
            MockFileSystem fs = this.fs;
            synchronized (fs) {
                // this is a roundabout way to show that close() did actually fail
                MockFileSystem.File file = (MockFileSystem.File) fs.getFile(lfw.getPath());
                assertNotNull(file.out);
                assertTrue(file.out.isOpen);
            }
            
            lfw.close(); // this should have no effect
        }
    }
}