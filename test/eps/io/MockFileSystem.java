/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import static eps.io.IoMisc.abs;

import java.util.*;
import java.util.stream.*;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.NotDirectoryException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.LinkOption;
import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;

public class MockFileSystem implements FileSystem {
    protected final Directory root;
    protected final Directory workingDir;
    
    protected final Map<String, Throwable> excepts = new HashMap<>();
    
    public MockFileSystem() {
        synchronized (this) {
            Path workingPath = Objects.requireNonNull(IoMisc.getWorkingDirectory(), "working directory");
            
            Iterator<Path> elems = workingPath.toAbsolutePath().iterator();
            if (elems.hasNext()) {
                Directory dir = new Directory(elems.next().toString());
                root = dir;
                
                while (elems.hasNext()) {
                    Directory next = new Directory(elems.next().toString());
                    dir.addFile(next);
                    dir = next;
                }
                
                workingDir = dir;
                
            } else {
                root = workingDir = new Directory("dir");
            }
        }
    }
    
    public synchronized void putExcept(String name, Throwable x) {
        excepts.put(Objects.requireNonNull(name, "name"),
                    Objects.requireNonNull(x,    "x"));
    }
    
    protected synchronized void tryThrow(String name) throws IOException {
        Throwable x = excepts.remove(name);
        if (x instanceof IOException) {
            throw (IOException) x;
        }
        if (x instanceof RuntimeException) {
            throw (RuntimeException) x;
        }
        if (x instanceof Error) {
            throw (Error) x;
        }
        if (x != null) {
            excepts.put(name, x);
            assert false : excepts;
        }
    }
    
    protected synchronized AbstractFile getFile(Path path) {
        Iterator<Path> elems = path.toAbsolutePath().iterator();
        if (elems.hasNext()) {
            Path next = elems.next();
            if (root.isName(next)) {
                return getFile(root, elems);
            }
        }
        return null;
    }
    
    protected synchronized AbstractFile getFile(AbstractFile parent, Iterator<Path> elems) {
        if (!elems.hasNext()) {
            return parent;
        }
        if (parent instanceof Directory) {
            Directory dir  = (Directory) parent;
            Path      next = elems.next();
            for (AbstractFile file : dir.getFiles()) {
                if (file.isName(next)) {
                    return getFile(file, elems);
                }
            }
        }
        return null;
    }
    
    protected synchronized Directory getParentDirectory(Path path) throws IOException {
        AbstractFile file = getFile(path.toAbsolutePath().getParent());
        if (file instanceof Directory) {
            return (Directory) file;
        }
        throw new IOException("no parent directory for " + path);
    }
    
    @Override
    public synchronized boolean exists(Path path) {
        return getFile(path) != null;
    }
    
    @Override
    public synchronized boolean isDirectory(Path path, LinkOption... opts) {
        return getFile(path) instanceof Directory;
    }
    
    @Override
    public synchronized boolean isRegularFile(Path path, LinkOption... opts) {
        return getFile(path) instanceof File;
    }
    
    @Override
    public synchronized Stream<Path> list(Path path) throws IOException {
        tryThrow("list");
        AbstractFile file = getFile(path);
        
        if (file instanceof Directory) {
            return ((Directory) file).getFiles().stream().map(f -> path.resolve(f.getName()));
        }
        
        Class<?> cls = file == null ? null : file.getClass();
        throw new NotDirectoryException(cls + " " + path.toAbsolutePath());
    }
    
    @Override
    public synchronized void createDirectory(Path path) throws IOException {
        tryThrow("createDirectory");
        
        Path parent = path.toAbsolutePath().getParent();
        AbstractFile file = getFile(parent);
        
        if (file instanceof Directory) {
            Directory dir = (Directory) file;
            Path fileName = path.getFileName();
            
            if (dir.getFiles().stream().anyMatch(f -> f.isName(path))) {
                throw new FileAlreadyExistsException(path.toString());
            }
            
            dir.addFile(new Directory(fileName.toString()));
            
        } else {
            throw new IOException("parent does not exist: " + parent);
        }
    }
    
    private static final Set<OpenOption> DEFAULT_WRITE_OPTIONS =
        Collections.unmodifiableSet(EnumSet.of(StandardOpenOption.CREATE,
                                                StandardOpenOption.TRUNCATE_EXISTING,
                                                StandardOpenOption.WRITE));
    
    @Override
    public synchronized void write(Path path, byte[] bytes, OpenOption... opts) throws IOException {
        tryThrow("write");
        Set<OpenOption> set = DEFAULT_WRITE_OPTIONS;
        if (opts != null && opts.length > 0) {
            set = new HashSet<>();
            Collections.addAll(set, opts);
        }
        
        if (set.contains(StandardOpenOption.CREATE_NEW)) {
            if (set.contains(StandardOpenOption.CREATE)) {
                set.remove(StandardOpenOption.CREATE);
            }
        }
        
        // Note: Files.write doesn't seem to care about this, so we don't either.
//        if (!set.contains(StandardOpenOption.WRITE)) {
//            throw new IOException("cannot write to " + path);
//        }
        
        AbstractFile file = getFile(path);
        
        if (file == null) {
            if (!set.contains(StandardOpenOption.CREATE) && !set.contains(StandardOpenOption.CREATE_NEW)) {
                throw new IOException("cannot create new file at " + path);
            }
            Directory parent = getParentDirectory(path);
            parent.addFile(new File(path, bytes));
            return;
        }
        
        if (set.contains(StandardOpenOption.CREATE_NEW)) {
            throw new FileAlreadyExistsException("specified file already existed " + path);
        }
        
        if (file instanceof File) {
            if (set.contains(StandardOpenOption.APPEND)) {
                byte[] old = ((File) file).getBytes();
                byte[] cat = Arrays.copyOf(old, old.length + bytes.length);
                System.arraycopy(bytes, 0, cat, old.length, bytes.length);
                ((File) file).setBytes(cat);
            } else if (set.contains(StandardOpenOption.TRUNCATE_EXISTING)) {
                ((File) file).setBytes(bytes);
            } else {
                byte[] old = ((File) file).getBytes();
                if (old.length >= bytes.length) {
                    System.arraycopy(bytes, 0, old, 0, bytes.length);
                    ((File) file).setBytes(old);
                } else {
                    ((File) file).setBytes(bytes);
                }
            }
        } else {
            throw new IOException("specified file was a directory " + path);
//            if (!set.contains(StandardOpenOption.CREATE)) {
//                throw new IOException("specified file was a directory " + path);
//            }
//            Directory parent = getParentDirectory(path);
//            parent.removeFile(file);
//            parent.addFile(new File(path.getFileName().toString(), bytes));
        }
    }
    
    @Override
    public synchronized byte[] readAllBytes(Path path) throws IOException {
        tryThrow("readAllBytes");
        AbstractFile file = getFile(path);
        
        if (file instanceof File) {
            return ((File) file).getBytes();
        }
        
        throw new IOException(path.toString());
    }
    
    @Override
    public synchronized boolean deleteIfExists(Path path) throws IOException {
        tryThrow("deleteIfExists");
        AbstractFile file = getFile(path);
        
        if (file == null) {
            return false;
        }
        
        if (file instanceof Directory) {
            Directory dir = (Directory) file;
            if (!dir.getFiles().isEmpty()) {
                throw new DirectoryNotEmptyException(path.toString());
            }
        }
        
        file.getParent().removeFile(file);
        return true;
    }
    
    @Override
    public synchronized Path copy(Path from, Path to, CopyOption... opts) throws IOException {
        tryThrow("copy");
        AbstractFile src = getFile(from);
        AbstractFile dst = getFile(to);
        
        if (src == null) {
            throw new IOException("source does not exist " + from);
        }
        
        if (dst != null) {
            if (Arrays.asList(opts).contains(StandardCopyOption.REPLACE_EXISTING)) {
                dst.getParent().removeFile(dst);
            } else if (src == dst) {
                return to;
            } else {
                throw new FileAlreadyExistsException(to.toString());
            }
        }
        
        AbstractFile parent = getFile(to.toAbsolutePath().getParent());
        if (!(parent instanceof Directory)) {
            throw new IOException("parent directory does not exist for " + to);
        }
        
        if (src instanceof Directory) {
            ((Directory) parent).addFile(new Directory(to));
        } else if (src instanceof File) {
            ((Directory) parent).addFile(new File(to, ((File) src).getBytes()));
        } else {
            assert false : src.getClass();
        }
        
        return to;
    }
    
    private static final Set<OpenOption> DEFAULT_NEW_OUTPUT_STREAM_OPTIONS =
        Collections.unmodifiableSet(EnumSet.of(StandardOpenOption.CREATE,
                                               StandardOpenOption.TRUNCATE_EXISTING,
                                               StandardOpenOption.WRITE));
    
    @Override
    public synchronized OutputStream newOutputStream(Path path, OpenOption... opts) throws IOException {
        tryThrow("newOutputStream");
        Set<OpenOption> set = DEFAULT_NEW_OUTPUT_STREAM_OPTIONS;
        if (opts != null && opts.length > 0) {
            set = new LinkedHashSet<>();
            Collections.addAll(set, opts);
            if (set.contains(StandardOpenOption.CREATE_NEW)) {
                set.remove(StandardOpenOption.CREATE);
            }
        }
        
        AbstractFile file = getFile(path);
        
        int cursor = 0;
        
        if (file instanceof Directory) {
            throw new IOException("cannot open output stream to directory " + abs(path));
        }
        if (file instanceof File) {
            if (set.contains(StandardOpenOption.CREATE_NEW)) {
                throw new FileAlreadyExistsException(abs(path).toString());
            }
            if (set.contains(StandardOpenOption.TRUNCATE_EXISTING)) {
                ((File) file).setBytes(new byte[0]);
            } else if (set.contains(StandardOpenOption.APPEND)) {
                cursor = ((File) file).getSize();
            }
        } else {
            if (!set.contains(StandardOpenOption.CREATE) && !set.contains(StandardOpenOption.CREATE_NEW)) {
                throw new IOException("cannot create new file at " + path);
            }
            AbstractFile parent = getFile(abs(path).getParent());
            if (!(parent instanceof Directory)) {
                Class<?> type = parent == null ? null : parent.getClass();
                throw new IOException("cannot create new file at " + path + " because parent is " + type);
            }
            
            File f = new File(path, new byte[0]);
            ((Directory) parent).addFile(f);
            file = f;
        }
        
        File.FileOutput out = ((File) file).newOutputStream(set.contains(StandardOpenOption.DELETE_ON_CLOSE));
        out.seek(cursor);
        return out;
    }
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ *//* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    
    protected abstract class AbstractFile {
        protected Directory parent;
        protected final String name;
        
        protected AbstractFile(String name) {
            this.name = Objects.requireNonNull(name, "name");
        }
        
        protected AbstractFile(Path path) {
            this(Objects.requireNonNull(path, "path").getFileName().toString());
        }
        
        protected String getName() {
            return name;
        }
        
        protected boolean isName(Path path) {
            return getName().equals(path.toString());
        }
        
        protected Directory getParent() {
            synchronized (MockFileSystem.this) {
                return parent;
            }
        }
    }
    
    protected class File extends AbstractFile {
        protected byte[] bytes;
        
        protected FileOutput out;
        
        protected File(String name, byte[] bytes) {
            super(name);
            setBytesImpl(bytes);
        }
        
        protected File(Path path, byte[] bytes) {
            super(path);
            setBytesImpl(bytes);
        }
        
        protected void requireNoOutput() throws IOException {
            synchronized (MockFileSystem.this) {
                if (out != null) {
                    throw new IOException("cannot write to " + name);
                }
            }
        }
        
        protected void setBytes(byte[] bytes) throws IOException {
            synchronized (MockFileSystem.this) {
                requireNoOutput();
                setBytesImpl(bytes);
            }
        }
        
        protected void setBytesImpl(byte[] bytes) {
            synchronized (MockFileSystem.this) {
                this.bytes = Objects.requireNonNull(bytes, "bytes").clone();
            }
        }
        
        protected int getSize() {
            synchronized (MockFileSystem.this) {
                return bytes.length;
            }
        }
        
        protected byte[] getBytes() {
            synchronized (MockFileSystem.this) {
                return bytes.clone();
            }
        }
        
        protected FileOutput newOutputStream(boolean deleteOnClose) throws IOException {
            synchronized (MockFileSystem.this) {
                requireNoOutput();
                return out = new FileOutput(deleteOnClose);
            }
        }
        
        protected class FileOutput extends OutputStream {
            protected final boolean deleteOnClose;
            protected boolean isOpen = true;
            
            protected int cursor = 0;
            
            protected FileOutput(boolean deleteOnClose) {
                this.deleteOnClose = deleteOnClose;
            }
            
            protected void seek(int cursor) {
                synchronized (MockFileSystem.this) {
                    this.cursor = cursor;
                }
            }
            
            protected void requireOpen() throws IOException {
                synchronized (MockFileSystem.this) {
                    if (!isOpen) {
                        throw new IOException("stream is closed");
                    }
                }
            }
            
            @Override
            public void write(int bIn) throws IOException {
                synchronized (MockFileSystem.this) {
                    requireOpen();
                    tryThrow("OutputStream.write");
                    
                    byte b = (byte) (bIn & 0xFF);
                    
                    byte[] bytes = File.this.bytes;
                    if (cursor < bytes.length) {
                        bytes[cursor++] = b;
                        
                    } else {
                        int dif = (cursor + 1) - bytes.length;
                        byte[] copy = Arrays.copyOf(bytes, bytes.length + dif);
                        
                        copy[cursor++] = b;
                        File.this.bytes = copy;
                    }
                }
            }
            
            @Override
            public void close() throws IOException {
                synchronized (MockFileSystem.this) {
                    tryThrow("OutputStream.close");
                    
                    if (isOpen) {
                        if (File.this.out != this) {
                            throw new IOException(toString());
                        }
                        
                        isOpen = false;
                        File.this.out = null;
                        
                        if (deleteOnClose) {
                            Directory parent = File.this.parent;
                            if (parent != null) {
                                parent.removeFile(File.this);
                            }
                        }
                    }
                }
            }
        }
    }
    
    protected class Directory extends AbstractFile {
        protected final Set<AbstractFile> files = new LinkedHashSet<>();
        
        protected Directory(String name) {
            super(name);
        }
        
        protected Directory(Path path) {
            super(path);
        }
        
        protected void addFile(AbstractFile file) {
            synchronized (MockFileSystem.this) {
                assert file.parent == null : file.parent;
                if (files.add(Objects.requireNonNull(file, "file"))) {
                    file.parent = this;
                }
            }
        }
        
        protected void removeFile(AbstractFile file) {
            synchronized (MockFileSystem.this) {
                if (files.remove(file)) {
                    file.parent = null;
                }
            }
        }
        
        protected int count() {
            synchronized (MockFileSystem.this) {
                return files.size();
            }
        }
        
        protected Set<AbstractFile> getFiles() {
            synchronized (MockFileSystem.this) {
                return new LinkedHashSet<>(files);
            }
        }
    }
    
    /* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ *//* ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ ~~~~ */
    
    public synchronized void printFileSystem() {
        printFileSystem(System.out);
    }
    
    public synchronized void printFileSystem(PrintStream out) {
        printFileSystem(out, 0);
    }
    
    public synchronized void printFileSystem(int indents) {
        printFileSystem(System.out, indents);
    }
    
    public synchronized void printFileSystem(PrintStream out, int indents) {
        printFileSystem(out, indents, root);
    }
    
    protected synchronized void printFileSystem(PrintStream out, int indents, AbstractFile current) {
        for (int i = 0; i < indents; ++i) {
            out.print("    ");
        }
        out.print(current.getName());
        
        if (current instanceof Directory) {
            Directory dir = (Directory) current;
            
            out.printf(" (%d files):", dir.count());
            out.println();
            
            for (AbstractFile child : dir.getFiles()) {
                printFileSystem(out, indents + 1, child);
            }
            
        } else if (current instanceof File) {
            out.printf(" (%d bytes)", ((File) current).getSize());
            out.println();
        } else {
            out.println();
        }
    }
}