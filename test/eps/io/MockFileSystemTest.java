/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.stream.*;
import java.util.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.FileAlreadyExistsException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.OutputStream;

public class MockFileSystemTest {
    private MockFileSystem fs;
    
    public MockFileSystemTest() {
    }
    
    @Before
    public void initFileSystem() {
        fs = new MockFileSystem();
        FileSystem.setInstance(fs);
    }
    
    @After
    public void clearFileSystem() {
        fs = null;
        FileSystem.setInstance(null);
    }
    
    @Test
    public void testMockFileSystem() throws IOException {
        testing();
        fs.printFileSystem(1);
        
        Path testPath = Paths.get("test_path_abc123");
        
        assertFalse(fs.exists(testPath));
        assertFalse(fs.isDirectory(testPath));
        
        fs.createDirectory(testPath);
        
        assertTrue(fs.exists(testPath));
        assertTrue(fs.isDirectory(testPath));
        
        byte[] abc123 = {'a', 'b', 'c', 1, 2, 3};
        
        Path testFile = testPath.resolve("test.bytes");
        
        assertFalse(fs.exists(testFile));
        assertFalse(fs.isDirectory(testFile));
        
        assertFalse(fs.deleteIfExists(testFile));
        
        fs.write(testFile, abc123);
        
        assertTrue(fs.exists(testFile));
        assertTrue(fs.isRegularFile(testFile));
        
        assertTrue(fs.deleteIfExists(testFile));
        assertFalse(fs.exists(testFile));
        assertFalse(fs.isRegularFile(testFile));
        
        fs.write(testFile, abc123);
        
        assertTrue(fs.exists(testFile));
        assertTrue(fs.isRegularFile(testFile));
        
        byte[] bytes = fs.readAllBytes(testFile);
        assertArrayEquals(abc123, bytes);
        
        Path largeDir = testPath.resolve("large_dir");
        
        assertFalse(fs.exists(largeDir));
        assertFalse(fs.isDirectory(largeDir));
        
        fs.createDirectory(largeDir);
        
        assertTrue(fs.exists(largeDir));
        assertTrue(fs.isDirectory(largeDir));
        
        int count = 10;
        
        for (int i = 0; i < count; ++i) {
            String n = "file" + i;
            Path   p = largeDir.resolve(n);
            assertFalse(fs.exists(p));
            fs.write(p, n.getBytes());
            assertTrue(fs.exists(p));
            assertTrue(fs.isRegularFile(p));
        }
        
        try (Stream<Path> list = fs.list(largeDir)) {
            MutableInt i = new MutableInt(0);
            
            list.forEach(p -> {
                assertTrue(fs.exists(p));
                try {
                    byte[] readBytes = fs.readAllBytes(p);
                    assertEquals("file" + i.value, new String(readBytes));
                } catch (IOException x) {
                    throw new UncheckedIOException(x);
                }
                i.increment();
            });
            
            assertEquals(count, i.value);
        }
        
        fs.printFileSystem(1);
    }
    
    @Test
    public void testPutExcept() throws IOException {
        testing();
        
        Path file = Paths.get("test_file_123");
        fs.write(file, new byte[] {-1});
        
        IOException ioe = new IOException();
        fs.putExcept("readAllBytes", ioe);
        
        try {
            byte[] bytes = fs.readAllBytes(file);
            fail("found " + Arrays.toString(bytes));
        } catch (IOException x) {
            assertEquals(ioe, x);
            assertTrue(x == ioe);
        }
        
        byte[] bytes = fs.readAllBytes(file);
        assertArrayEquals(new byte[] {-1}, bytes);
    }
    
    @Test
    public void testCopy() throws IOException {
        testing();
        
        Path testFile = Paths.get("abc123.txt");
        byte[] abc123 = "abc123".getBytes();
        
        fs.write(testFile, abc123);
        assertArrayEquals(abc123, fs.readAllBytes(testFile));
        
        Path testDest = Paths.get("abc123_copy.txt");
        assertFalse(fs.exists(testDest));
        fs.copy(testFile, testDest);
        
        assertTrue(fs.isRegularFile(testDest));
        assertArrayEquals(abc123, fs.readAllBytes(testDest));
        
        Path testFile2 = Paths.get("def456.txt");
        byte[] def456 = "def456".getBytes();
        fs.write(testFile2, def456);
        
        try {
            fs.copy(testFile2, testDest);
            fail();
        } catch (IOException x) {
        }
        
        fs.copy(testFile2, testDest, StandardCopyOption.REPLACE_EXISTING);
        assertArrayEquals(def456, fs.readAllBytes(testDest));
        
        Path testDir = Paths.get("test_directory");
        assertFalse(fs.exists(testDir));
        fs.createDirectory(testDir);
        assertTrue(fs.isDirectory(testDir));
        assertEquals(0, IoMisc.countFiles(testDir));
        
        fs.write(testDir.resolve("test_file.blah"), new byte[0]);
        assertEquals(1, IoMisc.countFiles(testDir));
        
        testDest = Paths.get("test_directory_copy");
        assertFalse(fs.exists(testDest));
        
        fs.copy(testDir, testDest);
        assertTrue(fs.isDirectory(testDest));
        assertEquals(0, IoMisc.countFiles(testDest));
    }
    
    @Test
    public void testWriteOptions() throws IOException {
        testing();
        
        Path pathA = Paths.get("test_file_A");
        assertFalse(fs.exists(pathA));
        
        try {
            fs.write(pathA, new byte[3], StandardOpenOption.WRITE);
            fail("write to non-existent file should fail without create option");
        } catch (IOException x) {
        }
        
        fs.write(pathA, new byte[3], StandardOpenOption.CREATE_NEW);
        assertArrayEquals(new byte[3], fs.readAllBytes(pathA));
        
        try {
            fs.write(pathA, new byte[3], StandardOpenOption.CREATE_NEW);
            fail("CREATE_NEW requires that the file does not exist");
        } catch (FileAlreadyExistsException x) {
        }
        
        fs.write(pathA, new byte[] {1}, StandardOpenOption.WRITE);
        assertArrayEquals(new byte[] {1, 0, 0}, fs.readAllBytes(pathA));
        
        fs.write(pathA, new byte[] {2}, StandardOpenOption.TRUNCATE_EXISTING);
        assertArrayEquals(new byte[] {2}, fs.readAllBytes(pathA));
        
        fs.write(pathA, new byte[] {3}, StandardOpenOption.APPEND);
        assertArrayEquals(new byte[] {2, 3}, fs.readAllBytes(pathA));
        
        Path pathB = Paths.get("test_file_B");
        assertFalse(fs.exists(pathB));
        
        fs.write(pathB, "abc".getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        assertArrayEquals("abc".getBytes(), fs.readAllBytes(pathB));
        
        Path pathC = Paths.get("test_file_C");
        assertFalse(fs.exists(pathC));
        fs.createDirectory(pathC);
        
        try {
            fs.write(pathC, new byte[3], StandardOpenOption.CREATE);
            fail("writing to a directory should fail");
        } catch (IOException x) {
        }
    }
    
    @Test
    public void testList() throws IOException {
        testing();
        Path dir = Paths.get("test_directory");
        fs.createDirectory(dir);
        
        byte count = 10;
        for (byte i = 0; i < count; ++i) {
            Path file = dir.resolve(Byte.toString(i));
            fs.write(file, new byte[] {i});
            assertTrue(fs.isRegularFile(file));
        }
        
        try (Stream<Path> stream = fs.list(dir)) {
            stream.forEach(file -> {
                try {
                    byte b = Byte.parseByte(file.getFileName().toString());
                    assertArrayEquals(new byte[] {b}, fs.readAllBytes(file));
                } catch (IOException x) {
                    throw new UncheckedIOException(x);
                }
            });
        }
        
        try (Stream<Path> stream = fs.list(dir)) {
            stream.forEach(file -> {
                try {
                    assertTrue(fs.deleteIfExists(file));
                } catch (IOException x) {
                    throw new UncheckedIOException(x);
                }
            });
        }
        
        assertEquals(0, IoMisc.countFiles(dir));
    }
    
    private String newString(Path path) throws IOException {
        return new String(FileSystem.instance().readAllBytes(path));
    }
    
    @Test
    public void testBasicNewOutputStream() throws IOException {
        testing();
        Path path = Paths.get("test_file");
        assertFalse(fs.exists(path));
        
        try (OutputStream out = fs.newOutputStream(path)) {
            out.write("ABC".getBytes());
            out.write("123".getBytes());
        }
        
        assertEquals("ABC123", newString(path));
        
        try (OutputStream out1 = fs.newOutputStream(path)) {
            try (OutputStream out2 = fs.newOutputStream(path)) {
                fail("only one output per file allowed");
            } catch (IOException x) {
            }
        }
        
        try {
            fs.newOutputStream(null);
            fail();
        } catch (NullPointerException x) {
        }
    }
    
    @Test
    public void testNewOutputStreamWriteModes() throws IOException {
        testing();
        Path path = Paths.get("test_file");
        assertFalse(fs.exists(path));
        
        try (OutputStream out = fs.newOutputStream(path)) {
            out.write("ABC".getBytes());
        }
        
        assertEquals("ABC", newString(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.APPEND)) {
            out.write("123".getBytes());
        }
        
        assertEquals("ABC123", newString(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.WRITE)) {
            out.write("XYZ".getBytes());
        }
        
        assertEquals("XYZ123", newString(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.WRITE)) {
            out.write("123456789".getBytes());
        }
        
        assertEquals("123456789", newString(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.TRUNCATE_EXISTING)) {
        }
        
        assertEquals("", newString(path));
    }
    
    @Test
    public void testNewOutputStreamCreate() throws IOException {
        testing();
        Path path = Paths.get("test_file");
        assertFalse(fs.exists(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.WRITE)) {
            fail("file should not be created when no CREATE nor CREATE_NEW specified");
        } catch (IOException x) {
        }
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.CREATE)) {
            out.write(1);
        }
        
        assertTrue(fs.exists(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.CREATE_NEW)) {
            fail("CREATE_NEW requries the file not already exist");
        } catch (FileAlreadyExistsException x) {
        }
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.CREATE)) {
        }
        
        assertArrayEquals("CREATE should only create a new file if it doesn't already exist",
                          new byte[] {1}, fs.readAllBytes(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
            out.write(2);
        }
        
        assertArrayEquals("CREATE should only create a new file if it doesn't already exist",
                          new byte[] {1, 2}, fs.readAllBytes(path));
    }
    
    @Test
    public void testNewOutputStreamClose() throws IOException {
        testing();
        Path path = Paths.get("test_file");
        assertFalse(fs.exists(path));
        
        try (OutputStream out = fs.newOutputStream(path)) {
            out.write(1);
            out.close();
            try {
                out.write(2);
                fail("should not write after close");
            } catch (IOException x) {
            }
        }
        
        assertArrayEquals(new byte[] {1}, fs.readAllBytes(path));
        
        try (OutputStream out = fs.newOutputStream(path, StandardOpenOption.DELETE_ON_CLOSE, StandardOpenOption.APPEND)) {
            out.write(2);
            assertArrayEquals(new byte[] {1, 2}, fs.readAllBytes(path));
        }
        
        assertFalse(fs.exists(path));
    }
}