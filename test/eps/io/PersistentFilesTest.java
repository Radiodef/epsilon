/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import eps.util.*;
import eps.app.*;
import eps.job.*;
import eps.eval.*;
import eps.ui.*;
import eps.json.*;
import static eps.test.Tools.*;
import static eps.util.Literals.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.*;
import java.lang.reflect.*;

import java.io.IOException;
import java.nio.file.Path;

public class PersistentFilesTest {
    public PersistentFilesTest() {
    }
    
    private void setError(PersistentFile file) {
        file.setError();
        assertTrue(file.errorIsSet());
        assertNull(file.getPath());
    }
    
    static void resetConstants() {
        try {
            Field pathCreated = PersistentFile.class.getDeclaredField("pathCreated");
            Field cachedPath  = PersistentFile.class.getDeclaredField("cachedPath");
            Field didError    = PersistentFile.class.getDeclaredField("didError");
            forEach(pathCreated, cachedPath, didError, field -> field.setAccessible(true));
            for (PersistentFile file : PersistentFiles.VALUES) {
                pathCreated.setBoolean(file, false);
                cachedPath.set(file, null);
                didError.setBoolean(file, false);
            }
        } catch (ReflectiveOperationException x) {
            throw new UncheckedReflectiveOperationException(x);
        }
    }
    
    private MockFileSystem fs;
    private MockWorker worker;
    
    @Before
    public void beforeTest() {
        resetConstants();
        fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        worker = new MockWorker();
        PersistentFiles.setWorkerSupplier(action -> action.accept(worker));
    }
    
    @After
    public void afterTest() {
        fs = null;
        FileSystem.setInstance(null);
        worker.closeAndJoin();
        PersistentFiles.setWorkerSupplier(x -> {});
    }
    
    enum TestVariableFunction implements Variable.VariableFunction {
        INSTANCE;
        @Override
        public Operand apply(Variable s, Context c) {
            return c.getMathEnvironment().zero();
        }
    }
    
    private void assertEqualSettings(Settings expectSets, Settings actualSets) {
        for (Setting<?> set : Setting.values()) {
            assertEquals(expectSets.get(set), actualSets.get(set));
        }
    }
    
    private void assertEqualSymbols(Symbols expectSyms, Symbols actualSyms) {
        for (Symbol expectSym : (Iterable<Symbol>) expectSyms.stream()::iterator) {
            Symbol actualSym = actualSyms.get(expectSym.getName(), expectSym.getSymbolKind());
            assertNotNull(actualSym);
            assertTrue(expectSym.isEqualTo(actualSym));
        }
        
        assertEquals(expectSyms.stream().count(), actualSyms.stream().count());
    }
    
    @Test
    public void testConstants() {
        testing();
        
        for (PersistentFile file : PersistentFiles.VALUES) {
            assertFalse(file.errorIsSet());
            assertNotNull(file.getPath());
            Path path = file.getPath();
            assertTrue(fs.exists(path));
            assertEquals(fs.isDirectory(path), file.isDirectory());
            assertEquals(fs.isRegularFile(path), !file.isDirectory());
        }
        
        // test Settings file
        
        for (Object bad : ListOf("not Settings.Concurrent", null)) {
            try {
                PersistentFiles.SETTINGS_FILE.putContents(bad);
                fail("bad = " + bad);
            } catch (IllegalArgumentException ignored) {
            }
        }
        
        Settings.Concurrent expectSets = Settings.Concurrent.ofDefaults();
        Settings.Concurrent actualSets = PersistentFiles.readSettings();
        
        assertEqualSettings(expectSets, actualSets);
        
        expectSets.set(Setting.MAJOR_MARGIN, 61);
        expectSets.set(Setting.MAX_ITEMS,    601);
        expectSets.set(Setting.DECIMAL_MARK, CodePoint.valueOf('x'));
        
        PersistentFiles.writeSettings(expectSets);
        worker.exhaust();
        actualSets = PersistentFiles.readSettings();
        
        assertEquals(61,  (int) actualSets.get(Setting.MAJOR_MARGIN));
        assertEquals(601, (int) actualSets.get(Setting.MAX_ITEMS));
        assertEquals('x', actualSets.get(Setting.DECIMAL_MARK).intValue());
        
        assertEqualSettings(expectSets, actualSets);
        
        setError(PersistentFiles.SETTINGS_FILE);
        assertEqualSettings(Settings.Concurrent.ofDefaults(), PersistentFiles.readSettings());
        
        // test Symbols file
        
        for (Object bad : ListOf("not Symbols.Concurrent", null)) {
            try {
                PersistentFiles.SYMBOLS_FILE.putContents(bad);
                fail("bad = " + bad);
            } catch (IllegalArgumentException ignored) {
            }
        }
        
        Symbols.Concurrent expectSyms = Symbols.Concurrent.ofDefaults();
        // note: readSymbols() does this now
//        Symbols.setLocalSymbols(expectSyms);
        Symbols.Concurrent actualSyms = PersistentFiles.readSymbols();
        
        assertEqualSymbols(expectSyms, actualSyms);
        
        Variable pi = expectSyms.get("pi", Variable.class);
        Variable piAlias = (Variable) pi.createAlias("pi_alias");
        
        Variable testVar =
            new Variable.Builder()
                        .setName("test_var")
                        .setIntrinsic(false)
                        .setFunction(TestVariableFunction.INSTANCE)
                        .build();
        
        expectSyms.remove(pi);
        expectSyms.put(piAlias);
        expectSyms.put(testVar);
        
        assertFalse(expectSyms.contains(pi));
        assertTrue(expectSyms.contains(piAlias));
        assertTrue(expectSyms.contains(testVar));
        
        PersistentFiles.writeSymbols(expectSyms);
        worker.exhaust();
        actualSyms = PersistentFiles.readSymbols();
        
        Variable actualPiAlias = actualSyms.get(piAlias.getName(), Variable.class);
        Variable actualTestVar = actualSyms.get(testVar.getName(), Variable.class);
        
        assertFalse(actualSyms.contains("pi", Variable.class));
        assertNotNull(actualPiAlias);
        assertTrue(piAlias.isEqualTo(actualPiAlias));
        
        assertNotNull(actualTestVar);
        assertTrue(testVar.isEqualTo(actualTestVar));
        
        assertEqualSymbols(expectSyms, actualSyms);
        
        setError(PersistentFiles.SYMBOLS_FILE);
        assertEqualSymbols(Symbols.Concurrent.ofDefaults(), PersistentFiles.readSymbols());
        
        // test Items file
        
        for (Object bad : ListOf("not List", null, ListOf("not Item.Info"), ListOf(null))) {
            try {
                PersistentFiles.ITEMS_FILE.putContents(bad);
                fail("bad = " + bad);
            } catch (IllegalArgumentException ignored) {
            }
        }
        
        List<Item.Info> items = PersistentFiles.readItems();
        assertNotNull(items);
        assertEquals(Collections.emptyList(), items);
        
        // TODO: test write
    }
    
    @Test
    public void testThemes() throws IOException {
        testing();
        
        Theme testThemeA = Theme.BRIGHT.rename("TestThemeA");
        Theme testThemeB = Theme.AURORA.rename("TestThemeB");
        Theme testThemeC = Theme.ICEBERG.rename("TestThemeC");
        List<Theme> testThemes = ListOf(testThemeA, testThemeB, testThemeC);
        
        assertEquals(Collections.emptyList(), PersistentFiles.readThemes());
        
        Path dir = PersistentFiles.THEME_DIRECTORY.getPath();
        
        Path notThemeA = dir.resolve("not_a_theme");
        Path notThemeB = dir.resolve("not_a_theme" + PersistentObjectFile.DOT_EXTENSION);
        fs.write(notThemeA, Json.toBytes(Theme.HELL.rename("X"), false)); // not a theme because no extension
        fs.write(notThemeB, Json.toBytes("not a theme", false));
        
        assertTrue(testThemes.add(Theme.HELL));
        assertEquals(3, PersistentFiles.writeThemes(testThemes));
        worker.exhaust();
        // note: Hell theme which is a preset should not have been written
        assertTrue(testThemes.remove(Theme.HELL));
        
        List<Theme> readThemes = PersistentFiles.readThemes();
        assertEquals(testThemes.size(), readThemes.size());
        assertEquals(new HashSet<>(testThemes), new HashSet<>(readThemes)); // can read in any order
        
        Path pathA = dir.resolve(PersistentObjectFile.toObjectFile(testThemeA.getName()));
        Path pathB = dir.resolve(PersistentObjectFile.toObjectFile(testThemeB.getName()));
        Path pathC = dir.resolve(PersistentObjectFile.toObjectFile(testThemeC.getName()));
        
        assertTrue(fs.isRegularFile(pathA));
        assertTrue(fs.isRegularFile(pathB));
        assertTrue(fs.isRegularFile(pathC));
        
        testThemes = ListOf(testThemeA);
        assertEquals(1, PersistentFiles.writeThemes(testThemes));
        worker.exhaust();
        
        // testing auto deletion of unused themes
        
        assertTrue(fs.isRegularFile(pathA));
        assertFalse(fs.isRegularFile(pathB));
        assertFalse(fs.isRegularFile(pathC));
        
        readThemes = PersistentFiles.readThemes();
        assertEquals(testThemes, readThemes);
        
        assertTrue(fs.exists(notThemeA));
        assertTrue(fs.exists(notThemeB));
        
        setError(PersistentFiles.THEME_DIRECTORY);
        assertEquals(Collections.emptyList(), PersistentFiles.readThemes());
    }
}