/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import static eps.test.Tools.*;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;

public class SuffixerTest {
    public SuffixerTest() {
    }
    
    @Test
    public void test0to9() {
        testing();
        
        Suffixer s = new Suffixer("name", 0, 9, false);
        
        assertEquals(0, s.getStart());
        assertEquals(9, s.getEnd());
        assertFalse(s.isNoZero());
        
        for (int i = 0; i <= 9; ++i) {
            assertTrue(s.hasNext());
            assertEquals(i, s.getCurrent());
            assertEquals("name" + i, s.next());
        }
        
        assertFalse(s.hasNext());
        
        try {
            String next = s.next();
            fail(next);
        } catch (NoSuchElementException ignored) {
        }
    }
    
    @Test
    public void testNoZero() {
        testing();
        
        int i = 0;
        for (String name : Suffixer.each("noz", 100)) {
            assertEquals((i == 0) ? "noz" : ("noz" + i), name);
            ++i;
        }
    }
}