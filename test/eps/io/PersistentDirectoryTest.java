/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.util.function.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;

public class PersistentDirectoryTest {
    public PersistentDirectoryTest() {
    }
    
    @Test
    public void testConstructor() {
        testing();
        
        PersistentDirectory p = new PersistentDirectory("name");
        new PersistentDirectory(p, "name");
        new PersistentDirectory(p, "name.blah");
        
        try {
            new PersistentDirectory(null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            new PersistentDirectory(p, null);
            fail();
        } catch (NullPointerException x) {
        }
        try {
            new PersistentDirectory("");
            fail();
        } catch (IllegalArgumentException x) {
        }
        try {
            new PersistentDirectory(p, "");
            fail();
        } catch (IllegalArgumentException x) {
        }
    }
    
    @Test
    public void testCreation() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        Supplier<PersistentDirectory> supplier = () -> new PersistentDirectory("test_dir_123");
        
        // test initial creation
        // - should create new directory
        
        PersistentDirectory dir = supplier.get();
        
        Path expectedPath = Paths.get(dir.getName());
        assertFalse(fs.isDirectory(expectedPath));
        
        Path path = dir.getPath();
        assertNotNull(path);
        assertEquals(0, IoMisc.countFiles(path));
        
        assertTrue(fs.isDirectory(path));
        assertEquals(expectedPath, path);
        
        assertTrue("test cached", path == dir.getPath());
        
        // test reinitial
        // - should find the previously-created directory
        
        dir = supplier.get();
        path = dir.getPath();
        
        assertNotNull(path);
        assertTrue(fs.isDirectory(path));
        assertEquals(expectedPath, path);
        assertEquals(0, IoMisc.countFiles(path));
        
        assertTrue("test cached", path == dir.getPath());
        
        String file = "hello_world.bytes";
        String contents = "Hello, world!";
        
        fs.write(path.resolve(file), contents.getBytes());
        assertEquals(1, IoMisc.countFiles(path));
        
        // test pre-existing directory contents
        // - should find the previously-created directory and file
        
        dir = supplier.get();
        path = dir.getPath();
        
        assertNotNull(path);
        assertTrue(fs.isDirectory(path));
        assertEquals(expectedPath, path);
        assertEquals(1, IoMisc.countFiles(path));
        
        assertEquals(contents, new String(fs.readAllBytes(path.resolve(file))));
        
        fs.printFileSystem(1);
    }
    
    @Test
    public void testDynamicNaming() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        String name = "test_file_name";
        fs.write(Paths.get(name), new byte[0]);
        fs.write(Paths.get(name + "1"), new byte[0]);
        fs.write(Paths.get(name + "2"), new byte[0]);
        
        PersistentDirectory dir = new PersistentDirectory(name);
        
        Path actual = dir.getPath();
        assertNotNull(actual);
        assertTrue(fs.isDirectory(actual));
        assertEquals(name + "3", actual.getFileName().toString());
        
        fs.printFileSystem(1);
    }
    
    @Test
    public void testFailure() {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        String name = "test_file_name_123";
        PersistentDirectory dir = new PersistentDirectory(name);
        
        fs.putExcept("createDirectory", new IOException("fail"));
        
        Path path = dir.getPath();
        assertNull(path);
        assertTrue(path == dir.getPath());
        
        assertFalse(fs.exists(Paths.get(name)));
        assertFalse(fs.isDirectory(Paths.get(name)));
    }
}