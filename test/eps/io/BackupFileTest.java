/* MIT License
 *
 * Copyright (c) 2017-2018 David Staver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eps.io;

import static eps.test.Tools.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BackupFileTest {
    public BackupFileTest() {
    }
    
    @Test
    public void testSimpleBackup() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        Path path = Paths.get("test_file_123.blah");
        
        try {
            new BackupFile(null);
            fail();
        } catch (NullPointerException x) {
        }
        
        fs.write(path, new byte[] {-1});
        
        BackupFile file = new BackupFile(path, fs);
        assertEquals(fs, file.getFileSystem());
        assertEquals(path, file.getSource());
        
        assertNotNull(file.getBackup());
        assertEquals("test_file_123_backup.blah", file.getBackup().getFileName().toString());
        assertArrayEquals(new byte[] {-1}, fs.readAllBytes(file.getBackup()));
        
        file.close();
        assertFalse(fs.exists(file.getBackup()));
        
        file.close(); // should have no effect
        assertFalse(fs.exists(file.getBackup()));
    }
    
    @Test
    public void testNonExistentBackup() {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        Path path = Paths.get("non_existent_file");
        assertFalse(fs.exists(path));
        
        try (BackupFile file = new BackupFile(path)) {
            assertFalse(file.backupSuccess());
            assertNull(file.getBackup());
        }
    }
    
    @Test
    public void testDynamicNaming() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        fs.write(Paths.get("test_file_backup.blah"), new byte[0]);
        fs.write(Paths.get("test_file_backup1.blah"), new byte[1]);
        fs.createDirectory(Paths.get("test_file_backup2.blah"));
        
        Path path = Paths.get("test_file.blah");
        fs.write(path, new byte[3]);
        
        BackupFile file = new BackupFile(path);
        assertNotNull(file.getBackup());
        
        assertEquals("test_file_backup3.blah", file.getBackup().getFileName().toString());
        assertArrayEquals(new byte[3], fs.readAllBytes(file.getBackup()));
        
        file.close();
        assertFalse(fs.exists(file.getBackup()));
    }
    
    @Test
    public void testFailure() throws IOException {
        testing();
        MockFileSystem fs = new MockFileSystem();
        FileSystem.setInstance(fs);
        
        Path path = Paths.get("test_file_123.blah").toAbsolutePath();
        fs.write(path, new byte[] {-1});
        
        fs.putExcept("copy", new IOException("copy failed"));
        
        BackupFile file = new BackupFile(path);
        
        assertNotNull(file.getBackup());
        assertFalse(fs.exists(file.getBackup()));
        
        file.close();
        assertFalse(fs.exists(file.getBackup()));
        
        file = new BackupFile(path);
        
        assertNotNull(file.getBackup());
        assertEquals(Paths.get("test_file_123_backup.blah").toAbsolutePath(), file.getBackup());
        assertTrue(fs.exists(file.getBackup()));
        assertArrayEquals(new byte[] {-1}, fs.readAllBytes(file.getBackup()));
        
        fs.putExcept("deleteIfExists", new IOException("deletion failed"));
        
        file.close();
        assertTrue(fs.exists(file.getBackup()));
        assertArrayEquals(new byte[] {-1}, fs.readAllBytes(file.getBackup()));
    }
}